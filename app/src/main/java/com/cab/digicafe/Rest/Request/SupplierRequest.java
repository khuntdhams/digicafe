package com.cab.digicafe.Rest.Request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by pc51 on 3/22/2018.
 */

public class SupplierRequest {

    @SerializedName("newdata")
    @Expose
    private List<Object> newdata = null;
    @SerializedName("olddata")
    @Expose
    private List<Object> olddata = null;
    private final static long serialVersionUID = 3398878580068595660L;

    public List<Object> getNewdata() {
        return newdata;
    }

    public void setNewdata(List<Object> newdata) {
        this.newdata = newdata;
    }

    public List<Object> getOlddata() {
        return olddata;
    }

    public void setOlddata(List<Object> olddata) {
        this.olddata = olddata;
    }


}
