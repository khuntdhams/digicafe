
package com.cab.digicafe.Rest.Request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Olddatum implements Serializable {

    @SerializedName("additional_json_data")
    @Expose
    private String additionalJsonData;
    @SerializedName("audio_json_data")
    @Expose
    private String audioJsonData;
    @SerializedName("available_stock")
    @Expose
    private String availableStock;
    @SerializedName("bridge_id")
    @Expose
    private String bridgeId;
    @SerializedName("category")
    @Expose
    private String category;
    @SerializedName("copy_flag")
    @Expose
    private String copyFlag;
    @SerializedName("created_date")
    @Expose
    private String createdDate;
    @SerializedName("cross_reference")
    @Expose
    private String crossReference;
    @SerializedName("currency_code")
    @Expose
    private String currencyCode;
    @SerializedName("currency_code_id")
    @Expose
    private String currencyCodeId;
    @SerializedName("discount_percentage")
    @Expose
    private String discountPercentage;
    @SerializedName("discounted_price")
    @Expose
    private String discountedPrice;
    @SerializedName("field_json_data")
    @Expose
    private String fieldJsonData;
    @SerializedName("image_json_data")
    @Expose
    private String imageJsonData;
    @SerializedName("internal_json_data")
    @Expose
    private String internalJsonData;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("offer")
    @Expose
    private String offer;
    @SerializedName("old_color")
    @Expose
    private String oldColor;
    @SerializedName("old_count")
    @Expose
    private String oldCount;
    @SerializedName("old_geohash")
    @Expose
    private String oldGeohash;
    @SerializedName("old_latitude")
    @Expose
    private String oldLatitude;
    @SerializedName("old_longitude")
    @Expose
    private String oldLongitude;
    @SerializedName("old_make")
    @Expose
    private String oldMake;
    @SerializedName("old_measure")
    @Expose
    private String oldMeasure;
    @SerializedName("old_sales_time")
    @Expose
    private String oldSalesTime;
    @SerializedName("old_total")
    @Expose
    private String oldTotal;
    @SerializedName("old_unit")
    @Expose
    private String oldUnit;
    @SerializedName("out_of_stock")
    @Expose
    private String outOfStock;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("price_type")
    @Expose
    private String priceType;
    @SerializedName("pricelist_id")
    @Expose
    private String pricelistId;
    @SerializedName("section")
    @Expose
    private String section;
    @SerializedName("tax_json_data")
    @Expose
    private String taxJsonData;
    @SerializedName("updated_date")
    @Expose
    private String updatedDate;
    @SerializedName("video_json_data")
    @Expose
    private String videoJsonData;
    private final static long serialVersionUID = -7521517514522657727L;

    public String getAdditionalJsonData() {
        return additionalJsonData;
    }

    public void setAdditionalJsonData(String additionalJsonData) {
        this.additionalJsonData = additionalJsonData;
    }

    public String getAudioJsonData() {
        return audioJsonData;
    }

    public void setAudioJsonData(String audioJsonData) {
        this.audioJsonData = audioJsonData;
    }

    public String getAvailableStock() {
        return availableStock;
    }

    public void setAvailableStock(String availableStock) {
        this.availableStock = availableStock;
    }

    public String getBridgeId() {
        return bridgeId;
    }

    public void setBridgeId(String bridgeId) {
        this.bridgeId = bridgeId;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCopyFlag() {
        return copyFlag;
    }

    public void setCopyFlag(String copyFlag) {
        this.copyFlag = copyFlag;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getCrossReference() {
        return crossReference;
    }

    public void setCrossReference(String crossReference) {
        this.crossReference = crossReference;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getCurrencyCodeId() {
        return currencyCodeId;
    }

    public void setCurrencyCodeId(String currencyCodeId) {
        this.currencyCodeId = currencyCodeId;
    }

    public String getDiscountPercentage() {
        return discountPercentage;
    }

    public void setDiscountPercentage(String discountPercentage) {
        this.discountPercentage = discountPercentage;
    }

    public String getDiscountedPrice() {
        return discountedPrice;
    }

    public void setDiscountedPrice(String discountedPrice) {
        this.discountedPrice = discountedPrice;
    }

    public String getFieldJsonData() {
        return fieldJsonData;
    }

    public void setFieldJsonData(String fieldJsonData) {
        this.fieldJsonData = fieldJsonData;
    }

    public String getImageJsonData() {
        return imageJsonData;
    }

    public void setImageJsonData(String imageJsonData) {
        this.imageJsonData = imageJsonData;
    }

    public String getInternalJsonData() {
        return internalJsonData;
    }

    public void setInternalJsonData(String internalJsonData) {
        this.internalJsonData = internalJsonData;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOffer() {
        return offer;
    }

    public void setOffer(String offer) {
        this.offer = offer;
    }

    public String getOldColor() {
        return oldColor;
    }

    public void setOldColor(String oldColor) {
        this.oldColor = oldColor;
    }

    public String getOldCount() {
        return oldCount;
    }

    public void setOldCount(String oldCount) {
        this.oldCount = oldCount;
    }

    public String getOldGeohash() {
        return oldGeohash;
    }

    public void setOldGeohash(String oldGeohash) {
        this.oldGeohash = oldGeohash;
    }

    public String getOldLatitude() {
        return oldLatitude;
    }

    public void setOldLatitude(String oldLatitude) {
        this.oldLatitude = oldLatitude;
    }

    public String getOldLongitude() {
        return oldLongitude;
    }

    public void setOldLongitude(String oldLongitude) {
        this.oldLongitude = oldLongitude;
    }

    public String getOldMake() {
        return oldMake;
    }

    public void setOldMake(String oldMake) {
        this.oldMake = oldMake;
    }

    public String getOldMeasure() {
        return oldMeasure;
    }

    public void setOldMeasure(String oldMeasure) {
        this.oldMeasure = oldMeasure;
    }

    public String getOldSalesTime() {
        return oldSalesTime;
    }

    public void setOldSalesTime(String oldSalesTime) {
        this.oldSalesTime = oldSalesTime;
    }

    public String getOldTotal() {
        return oldTotal;
    }

    public void setOldTotal(String oldTotal) {
        this.oldTotal = oldTotal;
    }

    public String getOldUnit() {
        return oldUnit;
    }

    public void setOldUnit(String oldUnit) {
        this.oldUnit = oldUnit;
    }

    public String getOutOfStock() {
        return outOfStock;
    }

    public void setOutOfStock(String outOfStock) {
        this.outOfStock = outOfStock;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPriceType() {
        return priceType;
    }

    public void setPriceType(String priceType) {
        this.priceType = priceType;
    }

    public String getPricelistId() {
        return pricelistId;
    }

    public void setPricelistId(String pricelistId) {
        this.pricelistId = pricelistId;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public String getTaxJsonData() {
        return taxJsonData;
    }

    public void setTaxJsonData(String taxJsonData) {
        this.taxJsonData = taxJsonData;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getVideoJsonData() {
        return videoJsonData;
    }

    public void setVideoJsonData(String videoJsonData) {
        this.videoJsonData = videoJsonData;
    }

}
