package com.cab.digicafe.Rest;

import com.cab.digicafe.Rest.Request.CatRequest;
import com.cab.digicafe.Rest.Request.SupplierRequest;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Query;


public interface ApiInterface {
    @GET("movie/top_rated")
    Call<String> getTopRatedMovies(@Query("api_key") String apiKey);


    @FormUrlEncoded
    @POST("LoginApi")
    Call<Object> getUserLogin(@Field("username") String name,
                              @Field("password") String email);


    @FormUrlEncoded
    @POST("RegisterApi")
    Call<Object> getUserRegistration(@Field("actype") String name,
                                     @Field("firstname") String firstname,
                                     @Field("lastname") String lastname,
                                     @Field("latitude") String latitude,
                                     @Field("longitude") String longitude,
                                     @Field("location") String location,
                                     @Field("contact_no") String contact_no,
                                     @Field("email_id") String email_id,
                                     @Field("username") String username,
                                     @Field("password") String password,
                                     @Field("confirm_password") String confirm_password,
                                     @Field("newsletter") String newsletter,
                                     @Field("terms") String terms,
                                     @Field("countrycode") String countrycode,
                                     //@Field("field_json_data") RequestBody field_json_data,
                                     @Field("field_json_data") JSONObject params
    );

    @FormUrlEncoded
    @POST("RegisterApi")
    Call<Object> setRegForAddNw(@Field("actype") String name,
                                @Field("firstname") String firstname,
                                @Field("lastname") String lastname,
                                @Field("latitude") String latitude,
                                @Field("longitude") String longitude,
                                @Field("location") String location,
                                @Field("contact_no") String contact_no,
                                @Field("email_id") String email_id,
                                @Field("username") String username,
                                @Field("password") String password,
                                @Field("confirm_password") String confirm_password,
                                @Field("newsletter") String newsletter,
                                @Field("terms") String terms,
                                @Field("countrycode") String countrycode,
                                @Field("currency_code") String currency_code,
                                @Field("currency_code_id") String currency_code_id,
                                @Field("company_detail_long") String company_detail_long,
                                @Field("business_status") String business_status,
                                @Field("field_json_data") String field_json_data
    );

    @PUT("ProfileApi/changePassword")
    Call<Object> getChangepassword(
            @Header("Cookie") String sessionIdAndToken,
            //  @Part JSONObject userdatas,
            @Body JsonObject userdatas

    );


    @POST("ChitApi")
    Call<Object> createchit(@Header("Cookie") String sessionIdAndToken,
                            @Body JsonObject userdatas,
                            @Query("bridge_id") String apiKey,
                            @Query("chit_hash_id") String chit_hash_id,
                            @Query("favouriteSelected") String favouriteSelected,
                            @Query("currency_code") String currency_code,
                            @Query("currency_code_id") String currency_code_id,
                            @Query("page") String pagecount);



   /* @PUT("ChitApi")
    Call<Object> updatechit(@Header("Cookie") String sessionIdAndToken,
                            @Body JsonObject userdatas,
                            @Query("bridge_id") String apiKey,
                            @Query("chit_hash_id") String chit_hash_id,
                            @Query("favouriteSelected") String favouriteSelected,
                            @Query("page") String pagecount);*/

    //@Headers({"Content-type: application/json", "Accept: */*"})
    @PUT("ChitApi")
    Call<Object> updatechit(@Header("Cookie") String sessionIdAndToken,
                            @Body JsonObject userdatas,
                            @Query("bridge_id") String apiKey,
                            @Query("chit_hash_id") String chit_hash_id,
                            @Query("favouriteSelected") String favouriteSelected,
                            @Query("currency_code") String currency_code,
                            @Query("currency_code_id") String currency_code_id,
                            @Query("page") String pagecount);


    //@Headers({"Content-type: application/json", "Accept: */*"}) // for aggre.
    @GET("ContactApi/networkDownListByBridgeID")
    Call<Object> getCatererlist(@Header("Cookie") String sessionIdAndToken,
                                @Query("bridge_id") String apiKey,
                                @Query("profile_category") String profile_category,
                                @Query("page") int page);

    @GET("profileApi/aggregator")
        // for circle
    Call<Object> getCatererlist(@Header("Cookie") String sessionIdAndToken,
                                @Query("aggregator_bridge_id") String aggregator_bridge_id,
                                @Query("industry_id") String industry_id,
                                @Query("building_id") String building_id,
                                @Query("page") int page);


    @GET("ContactApi/Supplier")
    Call<Object> getMySupplier(@Header("Cookie") String sessionIdAndToken);


    @Headers({"Content-type: application/json", "Accept: */*"})
    @GET("profileApi/industrySupplier")
    Call<Object> getaddress(@Header("Cookie") String sessionIdAndToken, @Query("search") String apiKey);

    // ACT - to_bridge_id - favouriteSelected               INFO - from_bridge_id - chit_hash_id
    @GET("ChitApi/favouritePrice")
    Call<Object> getCatalougelist(@Header("Cookie") String sessionIdAndToken,
                                  @Query("bridge_id") String apiKey,
                                  @Query("chit_hash_id") String chit_hash_id,
                                  @Query("favouriteSelected") String favouriteSelected,
                                  @Query("page") int page,
                                  @Query("search_item") String search_item,
                                  @Query("currency_code") String currency_code,
                                  @Query("category") String category);

    @GET("BridgeApi/sentitem")
    Call<Object> getSenditemsOtherThan(@Header("Cookie") String sessionIdAndToken, @Query("page") int page, @Query("purpose") String purpose, @Query("filter!") String filter);

    @GET("BridgeApi/sentitem")
    Call<Object> getSenditems(@Header("Cookie") String sessionIdAndToken, @Query("page") int page, @Query("purpose") String purpose, @Query("filter") String filter);


    @GET("BridgeApi/chitDetail")
    Call<Object> getchitDetail(@Header("Cookie") String sessionIdAndToken, @Query("chit_hash_id") String chit_hash_id, @Query("chit_id") String chit_id, @Query("currency_code") String currency_code);

    @GET("ProfileApi/user")
    Call<Object> getagregatorProfile(@Query("username") String username);

    @GET("ProfileApi")
        // get employees detail - using this able to get business :  navneet1@henncy.br from this getting henncy
    Call<Object> getProfile(@Header("Cookie") String sessionIdAndToken, @Query("bridge_id") String apiKey);

    //@GET("ProfileApi")  // get Busi detail
    //Call<Object> getProfileFromAllTask(@Header("Cookie") String sessionIdAndToken,@Query("bridge_id") String apiKey); // to bid from emp login in all task assign


    @GET("ProfileApi")
        // get Business detail
    Call<Object> getBusinessProfile(@Header("Cookie") String sessionIdAndToken); // with  currency


    @Multipart
    @POST("ProfileApi/")
        // get Business detail
    Call<Object> updateBusinessProfile(@Header("Cookie") String sessionIdAndToken,
                                       @PartMap Map<String, RequestBody> requestBodyMap);

    @Multipart
    @POST("ProfileApi/")
        // get Business detail
    Call<Object> updateBusinessProfileWithImg(@Header("Cookie") String sessionIdAndToken,
                                              @PartMap Map<String, RequestBody> requestBodyMap,
                                              @Part MultipartBody.Part file);


//    @Headers({"Content-type: application/x-www-form-urlencoded",
//            "Accept: */*"})
//    @FormUrlEncoded
//    @POST("ProfileApi")
//    Call<Object> updateuserprofile(@Field("section") String section,
//                                   @Field("firstname") String firstname,
//                                   @Field("location") String location,
//                                   @Field("contact_no") String contact_no,
//                                   @Field("email_id") String email_id,
//                                   @Field("field_json_data") String field_json_data
//                                   );

    @Multipart
    @POST("ProfileApi")
    Call<Object> updateuserprofile(@PartMap Map<String, RequestBody> requestBodyMap);

    @PUT("BridgeApi/ChitAction")
    Call<Object> removechit(@Header("Cookie") String sessionIdAndToken,
                            @Body JsonObject userdatas);

    @PUT("BridgeApi/ChitComment")
    Call<Object> chitComment(@Header("Cookie") String sessionIdAndToken,
                             @Body JsonObject userdatas);

    @GET("BridgeApi/sentitem")
    Call<Object> getArchiveditems(@Header("Cookie") String sessionIdAndToken, @Query("filter") int filter, @Query("page") int page);

    //@Headers({"Content-type: application/json", "Accept: */*"})
    @GET("BridgeApi/assignToMe")
    Call<Object> getMyTask(@Header("Cookie") String sessionIdAndToken, @Query("page") int page, @Query("filter") String filter, @Query("purpose") String purpose);

    //@Headers({"Content-type: application/json", "Accept: */*"})
    @GET("BridgeApi")
    Call<Object> getFilterByAssign(@Header("Cookie") String sessionIdAndToken, @Query("page") int page,
                                   @Query("filter") String filter,
                                   @Query("purpose") String purpose,
                                   @Query("assign_to_bridge_id") String bridge_id);

    //@Headers({"Content-type: application/json", "Accept: */*"})
    @GET("BridgeApi/unassignTask")
    Call<Object> getMyTaskunassign(@Header("Cookie") String sessionIdAndToken, @Query("page") int page, @Query("filter") String filter, @Query("purpose") String purpose);


    @PUT("BridgeApi/ChitAction")
    Call<Object> removearchived(@Header("Cookie") String sessionIdAndToken,
                                @Body JsonObject userdatas, @Query("filter") int filter);

    @Headers({"Content-type: application/x-www-form-urlencoded",
            "Accept: */*"})
    @FormUrlEncoded
    @POST("LoginApi/forgotPassword")
    Call<Object> forgotpwd(@Field("bridgeid") String bridgeid,
                           @Field("email_id") String email_id);


    @PUT("BridgeApi/ChitAssignee")
    Call<Object> assigntask(@Header("Cookie") String sessionIdAndToken,
                            @Body JsonObject userdatas);


    @GET("ContactApi/searchNetwork")
    Call<Object> searchNw(@Header("Cookie") String sessionIdAndToken, @Query("bridgename") String username);


    @Headers({"Content-type: application/json", "Accept: */*"})
    @POST("ContactApi/network")
    Call<Object> addNetwork(@Header("Cookie") String sessionIdAndToken,
                            @Body JsonObject userdatas);

    @GET("BridgeApi/assignToMe")
    Call<Object> searchMytask(@Header("Cookie") String sessionIdAndToken,
                              @Query("search") String apiKey,
                              @Query("filter") String filter);

    @GET("BridgeApi/unassignTask")
    Call<Object> searchAlltask(@Header("Cookie") String sessionIdAndToken,
                               @Query("search") String search,
                               @Query("filter") String filter,
                               @Query("page") int page);

    @GET("BridgeApi/assignToMe")
    Call<Object> searchMytask(@Header("Cookie") String sessionIdAndToken,
                              @Query("search") String apiKey,
                              @Query("filter") String filter,
                              @Query("page") int page);

    @GET("BridgeApi/sentitem")
    Call<Object> searchOrder(@Header("Cookie") String sessionIdAndToken,
                             @Query("search") String apiKey,
                             @Query("filter") String filter);

    @GET("bridge/miniInbox")
    Call<Object> getReceiptsforcustomer(@Header("Cookie") String sessionIdAndToken, @Query("page") int page);


    @PUT("ProfileApi/consumerDevice")
    Call<Object> DeviceInfo(@Header("Cookie") String sessionIdAndToken, @Body JsonObject userdatas);

    @GET("ProductApi/")
    Call<Object> getCatalouge(@Header("Cookie") String sessionIdAndToken,
                              @Query("page") int page,
                              @Query("out_of_stock") String out_of_stock,
                              @Query("category") String category,
                              @Query("order_by") String order_by, //   // either we can leave it this way, or by default, it can display in asc order
                              @Query("order") String order); // created_date, name,

    @Headers({"Content-type: application/json", "Accept: */*"})
    @POST("ProductApi/")
    Call<Object> SwipeCatalogue(@Header("Cookie") String sessionIdAndToken,
                                @Body CatRequest userdatas);


    @GET("ProductApi/")
    Call<Object> SearchCatalogue(@Header("Cookie") String sessionIdAndToken, @Query("page") int page,
                                 @Query("search_item") String search, @Query("out_of_stock") String out_of_stock,
                                 @Query("category") String category,
                                 @Query("order_by") String order_by, //   // either we can leave it this way, or by default, it can display in asc order
                                 @Query("order") String order);
    // NEWDEVURL --  @Headers({"Content-type: application/json", "Accept: */*"})
    // ContactApi/networkDownListByBridgeID
    // BridgeApi/assignToMe
    // BridgeApi/unassignTask

    @Headers({"Content-type: application/json", "Accept: */*"})
    @POST("ProductApi/")
    Call<Object> PutCatalogue(@Header("Cookie") String sessionIdAndToken,
                              @Body CatRequest userdatas);


    @Headers({"Content-type: application/json", "Accept: */*"})
    @DELETE("ProductApi/")
    Call<Object> DeleteCatalogue(@Header("Cookie") String sessionIdAndToken,
                                 @Query("deleteIds") String deleteIds);


    @PUT("ProfileApi/shop")
    Call<Object> SetBusinessStatus(@Header("Cookie") String sessionIdAndToken, @Body JsonObject shop_status);

    @GET("ProfileApi/employee")
    Call<Object> getEmployee(@Header("Cookie") String sessionIdAndToken);

    @Headers({"Content-type: application/json", "Accept: */*"})
    @DELETE("ContactApi/")
    Call<Object> DelSupplier(@Header("Cookie") String sessionIdAndToken, @Query("deleteIds") String deleteIds);


    @GET("ContactApi/searchSupplier")
    Call<Object> SearchSupplier(@Header("Cookie") String sessionIdAndToken, @Query("bridgename") String bridgename);

    @Headers({"Content-type: application/json", "Accept: */*"})
    @POST("ContactApi/")
    Call<Object> PutSupplier(@Header("Cookie") String sessionIdAndToken,
                             @Body SupplierRequest userdatas);

    @GET("ContactApi/networkDownListByBridgeID")
    Call<Object> GetAggreId(@Header("Cookie") String sessionIdAndToken, @Query("bridge_id") String apiKey);

    @GET("ProductApi/category")
    Call<Object> getCategoryUnderMaster(@Header("Cookie") String sessionIdAndToken, @Query("page") int pagecount);

    @POST("ProductApi/category")
    Call<Object> addCategoryUnderMaster(@Header("Cookie") String sessionIdAndToken, @Body JsonObject jsonObject);

    @DELETE("ProductApi/category")
    Call<Object> deleteCategoryUnderMaster(@Header("Cookie") String sessionIdAndToken, @Query("deleteIds") String deleteIds);

    @GET("ProfileApi/employee")
    Call<Object> getEmployeeUnderMaster(@Header("Cookie") String sessionIdAndToken, @Query("page") int pagecount);

    @POST("ProfileApi/employee")
    Call<Object> addEmployeeUnderMaster(@Header("Cookie") String sessionIdAndToken, @Body JsonObject jsonObject);

    @DELETE("ProfileApi/employee")
    Call<Object> deleteEmpUnderMaster(@Header("Cookie") String sessionIdAndToken, @Query("deleteIds") String deleteIds);

    @GET("BridgeApi/chit_enquiry")
    Call<Object> getPropertyEnq(@Header("Cookie") String sessionIdAndToken,
                                @Query("chit_name") String chit_name, // Xref
                                @Query("purpose") String purpose,
                                @Query("subject") String subject,
                                @Query("filter") String filter,
                                @Query("contact_number") String contact_number,
                                @Query("page") int page);

    @POST("ProfileApi/consumerTraction")
    Call<Object> postCustomerTraction(@Header("Cookie") String sessionIdAndToken, @Body JsonObject jsonObject);

    @GET("ProfileApi/consumerTraction")
    Call<Object> getCustomerTraction(@Header("Cookie") String sessionIdAndToken);

    // External contact

    @GET("ContactApi/external")
    Call<Object> getExternalContactByNo(@Header("Cookie") String sessionIdAndToken,
                                        @Query("contact_number") String contact_number,
                                        @Query("page") int pagecount);

    // walk in insert edit
    @POST("ContactApi/external")
    Call<Object> postExternalContactByNo(@Header("Cookie") String sessionIdAndToken,
                                         @Body JsonObject jo);

    @POST("ContactApi/taskreference")
    Call<Object> posttaskreference(@Header("Cookie") String sessionIdAndToken,
                                   @Body JsonObject jo);


    @GET("ContactApi/taskreference")
    Call<Object> getTaskPreferenceByChitId(@Header("Cookie") String sessionIdAndToken,
                                           @Query("from_chit_id") String to_chit_id, // from all task
                                           @Query("to_chit_hash_id") String to_chit_hash_id, // from order
                                           @Query("task_purpose") String task_purpose,
                                           @Query("page") int pagecount);

    @GET("BridgeApi/unassignTask")
        // SearchByCaseId -- From Order -- Searck All Task
    Call<Object> getChitFromCaseId(@Header("Cookie") String sessionIdAndToken,
                                   @Query("search") String search, // from all task
                                   @Query("page") int pagecount);

  /*  @GET("BridgeApi/sentitem") // SearchByCaseId -- From All Task -- Search Order
    Call<Object> getChitFromCaseIdFromAllTask(@Header("Cookie") String sessionIdAndToken,
                                   @Query("search") String search, // from all task
                                   @Query("page") int pagecount);*/

    @GET("BridgeApi/chitDetail")
        // SearchByCaseId -- From All Task -- Search Order
    Call<Object> getChitFromCaseIdFromAllTask(@Header("Cookie") String sessionIdAndToken,
                                              @Query("chit_hash_id") String chit_hash_id, // from all task
                                              @Query("chit_id") String chit_id, // from all task
                                              @Query("page") int pagecount);


}
