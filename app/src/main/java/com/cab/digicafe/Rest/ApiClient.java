package com.cab.digicafe.Rest;

import android.content.Context;

import com.cab.digicafe.Helper.SessionManager;
import com.cab.digicafe.R;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class ApiClient {

    private static Retrofit retrofit2 = null;
    private static Retrofit retrofit3 = null;
    //  public static final String BASE_URL = "http://dev.chitandbridge.com/prerelease/";  // dev url
    //  public static final String BASE_URL = "http://Demo.chitandbridge.com/";
    //public static final String BASE_URL = "https://Chitandbridge.com/";  // prod url
    Context cc;
    public static String BASE_URL = "";  // prod url
    private static Retrofit retrofit = null;


    static OkHttpClient.Builder httpClient = new OkHttpClient.Builder();


    public static Retrofit getClient(Context c) {

        BASE_URL = c.getString(R.string.BASEURL);
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

       /* HttpLoggingInterceptor logging = new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
            @Override
            public void log(String message) {
               // Log.e("API", "OkHttp: " + message);
            }
        });*/
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();


        final SessionManager sessionManager = new SessionManager(c);
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        httpClient.addInterceptor(logging);

        /*

        CookieManager cookieManager = new CookieManager();
        cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
         httpClient.cookieJar(new JavaNetCookieJar(cookieManager));*/


    /*  if(sessionManager.getIsLogin()){
          Interceptor headerInterceptor = new Interceptor() {
              @Override
              public Response intercept(Chain chain) throws IOException {
                  Request.Builder requestBuilder = chain.request().newBuilder().addHeader("Cookie", sessionManager.getsession());
                  return chain.proceed(requestBuilder.build());
              }
          };
          httpClient.addInterceptor(headerInterceptor);

      }
*/

       /* OkHttpClient client = new OkHttpClient.Builder()
                .cookieJar(new JavaNetCookieJar(cookieManager))
                .readTimeout(30, TimeUnit.SECONDS).connectTimeout(30, TimeUnit.SECONDS)
                .build();*/

      /*  OkHttpClient client = new OkHttpClient().newBuilder()
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        final Request original = chain.request();
                        Request authorized = null;
                        if (!sessionManager.getsession().isEmpty()) {
                            authorized = original.newBuilder()
                                    .addHeader("Cookie", "cab=" + sessionManager.getsession())
                                    .build();
                        } else {
                            authorized = original.newBuilder()
                                    .build();
                        }

                        return chain.proceed(authorized);
                    }
                })
                .build();*/


        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(httpClient.readTimeout(5, TimeUnit.MINUTES).connectTimeout(5, TimeUnit.MINUTES).build())
                .build();

      /*  retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();*/

        return retrofit;
    }


    public static Retrofit getClient2(Context c) {
        BASE_URL = c.getString(R.string.BASEURL);

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();


        final SessionManager sessionManager = new SessionManager(c);
        OkHttpClient client = new OkHttpClient().newBuilder()
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        final Request original = chain.request();

                        final Request authorized = original.newBuilder()
                                .addHeader("Cookie", sessionManager.getsession())
                                .build();

                        return chain.proceed(authorized);
                    }
                })
                .readTimeout(30, TimeUnit.SECONDS).connectTimeout(30, TimeUnit.SECONDS)
                .build();


        if (retrofit2 == null) {

            retrofit2 = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    //.addConverterFactory(ScalarsConverterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .client(client)
                    .build();
        }
        return retrofit2;
    }


}
