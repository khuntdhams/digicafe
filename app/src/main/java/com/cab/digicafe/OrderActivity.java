package com.cab.digicafe;

import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.andremion.counterfab.CounterFab;
import com.cab.digicafe.Activities.ImagePreviewActivity;
import com.cab.digicafe.Activities.RequestApis.MyRequestCall;
import com.cab.digicafe.Activities.delivery.MrpDeliveryDialog;
import com.cab.digicafe.Adapter.OrderAdapter;
import com.cab.digicafe.Database.SettingDB;
import com.cab.digicafe.Database.SettingModel;
import com.cab.digicafe.Database.SqlLiteDbHelper;
import com.cab.digicafe.Dialogbox.DeliveryDialog;
import com.cab.digicafe.Helper.Inad;
import com.cab.digicafe.Helper.SessionManager;
import com.cab.digicafe.Helper.SharedPrefUserDetail;
import com.cab.digicafe.Model.Catelog;
import com.cab.digicafe.Model.Caterer;
import com.cab.digicafe.Model.Metal;
import com.cab.digicafe.Model.ModelFile;
import com.cab.digicafe.Model.ShareModel;
import com.cab.digicafe.Model.SupplierNew;
import com.cab.digicafe.Model.Userprofile;
import com.cab.digicafe.MyCustomClass.JsonObjParse;
import com.cab.digicafe.MyCustomClass.ShowGalleryCamera;
import com.cab.digicafe.Rest.ApiClient;
import com.cab.digicafe.Rest.ApiInterface;
import com.cab.digicafe.photopicker.activity.PickImageActivity;
import com.cab.digicafe.serviceTypeClass.GetServiceTypeFromBusiUserProfile;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import okhttp3.Headers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderActivity extends BaseActivity implements OrderAdapter.OnItemClickListener, DetePickerFragment.OnItemClickListener, TimePicker.OnItemTimerClickListener {
    private List<Caterer> movieList = new ArrayList<>();
    private RecyclerView recyclerView;
    private OrderAdapter mAdapter;
    TextView productcountval, productpriceval;
    String bridgeidval;
    String frombridgeidval;
    static String headernotes;
    private ProgressDialog dialog;
    String sessionString;
    SessionManager sessionManager;
    ArrayList<Catelog> productlist;
    Double pricevalue;
    int productcount;
    Button checkoutbtn;
    String productliststr, purpose = "";
    CounterFab counterFab;
    JSONArray cartdetails;
    JSONObject userdetails;
    Userprofile loggedin;
    ArrayList<String> selectedproductlist;
    ArrayList<Catelog> selectedproduct;
    SqlLiteDbHelper dbHelper;

    Double totalbill = 0.0;
    Double totalstategst = 0.0;
    Double totalVat = 0.0;
    Double totalcentralgst = 0.0;
    Double grandtotal = 0.0;
    String img = "";
    String currency = "";
    String field_json_data_info = "", business_type_info = "", service_type_of = ""; // field_json_data_info - info, business_type - info
    public static boolean isEdit = false;
    String delivery_charge = "";
    public static double total_with_tax = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.content_order, frameLayout);

        field_json_data_info = SharedPrefUserDetail.getString(this, SharedPrefUserDetail.field_json_data_info, "");
        business_type_info = SharedPrefUserDetail.getString(this, SharedPrefUserDetail.business_type, "");

        field_json_data = SharedPrefUserDetail.getString(OrderActivity.this, SharedPrefUserDetail.field_json_data, "");
        service_type_of = JsonObjParse.getValueEmpty(field_json_data, "service_type_of");


        currency = SharedPrefUserDetail.getString(this, SharedPrefUserDetail.chit_symbol_native, "") + " ";
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        dialog = new ProgressDialog(OrderActivity.this);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        tv_progress = (TextView) findViewById(R.id.tv_progress);
        rl_upload = (RelativeLayout) findViewById(R.id.rl_upload);
        dbHelper = new SqlLiteDbHelper(this);
        DividerItemDecoration verticalDecoration = new DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.VERTICAL);
        Drawable verticalDivider = ContextCompat.getDrawable(this, R.drawable.horizontal_divider);
        verticalDecoration.setDrawable(verticalDivider);
        recyclerView.addItemDecoration(verticalDecoration);
        productcountval = (TextView) findViewById(R.id.qtyvalue);
        productpriceval = (TextView) findViewById(R.id.priecvalue);
        checkoutbtn = (Button) findViewById(R.id.btn);
        counterFab = (CounterFab) findViewById(R.id.counter_fab);
        counterFab.setVisibility(View.GONE);
        checkoutbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showBottomSheetFragment();
            }
        });
        sessionManager = new SessionManager(this);
        productlist = new ArrayList<>();
        productlist = dbHelper.getAllcaretproduct();
        productcount = dbHelper.getcartcount();
        pricevalue = 0.0;
        getSupportActionBar().setTitle(sessionManager.getcurrentcatereraliasname());
        selectedproductlist = new ArrayList<>();
        selectedproduct = new ArrayList<>();
        productliststr = sessionManager.getCartproduct();
       /* try {
            JSONArray jsonArray =  new JSONArray(productliststr);
            cartdetails = jsonArray;
            for(int i = 0; i < jsonArray.length(); i++){
                if(jsonArray.get(i) instanceof JSONObject){
                    JSONObject jsnObj = (JSONObject)jsonArray.get(i);
                    Catelog obj = new Gson().fromJson(jsnObj.toString(), Catelog.class);
                    Log.e("alias",obj.getName());
                    pricevalue = pricevalue + (obj.getCartcount() * Double.parseDouble(obj.getdiscountmrp()));
                    productcount = productcount + obj.getCartcount();
                    productlist.add(obj);
                }
            }
            Log.e("cartproduct",jsonArray.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

*/
        try {
            userdetails = new JSONObject(sessionManager.getUserprofile());
            loggedin = new Gson().fromJson(userdetails.toString(), Userprofile.class);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.e("cartproduct", sessionManager.getUserprofile());


        productcountval.setText("#" + dbHelper.getcartcount());
        pricevalue = dbHelper.getcarttotal();
        //final NumberFormat format = NumberFormat.getCurrencyInstance();
        //format.setCurrency();


        Double car_total = dbHelper.getcarttotal();


        for (int i = 0; i < productlist.size(); i++) {
            total_with_tax = total_with_tax + cal_price2(i);
        }

        //productpriceval.setText(getString(R.string.currency) + String.format("%.2f", car_total));
        //productpriceval.setText(currency + String.format("%.2f", total_with_tax));
        productpriceval.setText(currency + Inad.getCurrencyDecimal(total_with_tax, this));
        mAdapter = new OrderAdapter(productlist, this, this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        if (service_type_of.equalsIgnoreCase(getString(R.string.jewel))) {
            new GetServiceTypeFromBusiUserProfile(this, sessionManager.getcurrentcaterername(), new GetServiceTypeFromBusiUserProfile.Apicallback() {
                @Override
                public void onGetResponse(String a, String response, String sms_emp) {
                    try {
                        ShareModel.getI().isCheckStInCatalogue = false;
                        JSONObject Jsonresponse = new JSONObject(response);
                        JSONObject Jsonresponseinfo = Jsonresponse.getJSONObject("response");
                        JSONArray jaData = Jsonresponseinfo.getJSONArray("data");
                        if (jaData.length() > 0) {
                            JSONObject data = jaData.getJSONObject(0);
                            String field_json_data = JsonObjParse.getValueEmpty(data.toString(), "field_json_data");
                            String metal_list = JsonObjParse.getValueEmpty(field_json_data, "metal_list");
                            //SharedPrefUserDetail.setString(CatalougeTabActivity.this, SharedPrefUserDetail.metal_list, metal_list);
                            Metal metal = new Gson().fromJson(metal_list, Metal.class);
                            if (metal == null) metal = new Metal();
                            ShareModel.getI().metal = metal;

                            if (metal.serviceId == 3) {
                                checkoutbtn.setEnabled(false);
                                checkoutbtn.setAlpha(0.5f);
                            }

                        } else {


                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    recyclerView.setAdapter(mAdapter);

                    double total_with_tax = 0;
                    for (int i = 0; i < productlist.size(); i++) {
                        total_with_tax = total_with_tax + cal_price2(i);
                    }
                    productpriceval.setText(currency + Inad.getCurrencyDecimal(total_with_tax, OrderActivity.this));

                }
            });

        } else {
            recyclerView.setAdapter(mAdapter);
        }


        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                sessionString = null;
                bridgeidval = null;
                frombridgeidval = null;
                delivery_charge = null;
            } else {
                sessionString = extras.getString("session");
                bridgeidval = extras.getString("bridgeidval");
                frombridgeidval = extras.getString("frombridgeidval");
                purpose = extras.getString("purpose", "");
                delivery_charge = extras.getString("delivery_charge");
                purpose = SharedPrefUserDetail.getString(this, SharedPrefUserDetail.purpose, "");
                //purpose = getString(R.string.purpose);

                if (extras.containsKey("img")) {
                    img = extras.getString("img");
                }
            }

        } else {
            sessionString = (String) savedInstanceState.getSerializable("session");
            bridgeidval = (String) savedInstanceState.getSerializable("bridgeidval");
            frombridgeidval = (String) savedInstanceState.getSerializable("frombridgeidval");
        }


        Log.e("sessionString", "sessionString" + sessionString);
        checkoutbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* dialog.setMessage("Creating your order ...");
                dialog.show();
                usersignin(sessionString,cartdetails,userdetails);*/
                field_json_data = SharedPrefUserDetail.getString(OrderActivity.this, SharedPrefUserDetail.field_json_data, "");
                service_type_of = JsonObjParse.getValueEmpty(field_json_data, "service_type_of");
                boolean isGo = true;
                if (service_type_of.equalsIgnoreCase(getString(R.string.shopping_cart_cb))) {

                    String min = JsonObjParse.getValueEmpty(field_json_data, "min");
                    int minI = Integer.parseInt(min);
                    if (dbHelper.getcartcount() < minI) {
                        isGo = false;
                        Toast.makeText(OrderActivity.this, "Select at least " + min + " items", Toast.LENGTH_LONG).show();
                    } else {

                    }

                }


                if (!isGo) {
                    return;
                }

                pricevalue = dbHelper.getcarttotal();
                productcount = dbHelper.getcartcount();

                headernotes = "";
                if (purpose.equalsIgnoreCase("service")) {


                    headernotes = sessionManager.getRemark();
                    billamount();

                    final String date_ = sessionManager.getDate();
                    final String value = sessionManager.getAddress();
                    String phone = sessionManager.getT_Phone();

                    sessionManager.setDate(date_);
                    sessionManager.setAddress(value);
                    sessionManager.setRemark(headernotes);
                    sessionManager.setT_Phone(phone);


                    uplaodQueue(sessionString, cartdetails, userdetails, date_, value, null);
                    //usersignin(sessionString, cartdetails, userdetails, date_, value);


                        /*Intent i = new Intent(OrderActivity.this, IssueMgmtActivity.class);
                        i.putExtra("frombridgeidval", frombridgeidval);
                        i.putExtra("purpose", purpose);
                        startActivity(i);*/
                } else {
                    showdeliverydialog();
                }

              /*  if (getString(R.string.isremark).equals("1")) {
                    //showremark();

                } else if (getString(R.string.isdate).equals("1")) {


                    showdeliverydialog();
                    *//*DatePickerFragment mDatePicker = new DatePickerFragment();
                    mDatePicker.setStyle(DialogFragment.STYLE_NORMAL, R.style.CustomDialog);
                    mDatePicker.show(getFragmentManager(), "Select delivery date");*//*

                } else if (getString(R.string.isaddress).equals("1")) {
                    showdeliverydialog();
                   *//* new AddAddress(OrderActivity.this, new AddAddress.OnDialogClickListener() {
                        @Override
                        public void onDialogImageRunClick(int positon,String add) {
                            if (positon == 0) {

                            } else if (positon == 1) {
                                //str_actionid = str_actionid.replaceAll(",$", "");
                                dialog.setMessage("Creating your order ...");
                                dialog.show();
                                billamount();
                                sessionManager.setDate("");
                                sessionManager.setAddress(add);
                                sessionManager.setRemark(headernotes);
                                usersignin(sessionString, cartdetails, userdetails, "", add);
                            }
                        }
                    },sessionManager.getAddress()).show();*//*

                } else {
                    billamount();
                    sessionManager.setDate("");
                    sessionManager.setAddress("");
                    sessionManager.setRemark(headernotes);
                    usersignin(sessionString, cartdetails, userdetails, "", "");
                }*/
            }
        });


    }

    public void showremark() {
        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.AlertDialogCustom));
        builder.setTitle("Enter Your Remark");


// Set up the input

        final EditText input = new EditText(this);

        input.setTextColor(getResources().getColor(R.color.text_color));
        /*input.setHorizontallyScrolling(false);
        input.setMaxLines(Integer.MAX_VALUE);*/
        //input.setMaxLines(3);
// Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        input.setInputType(InputType.TYPE_CLASS_TEXT |
                InputType.TYPE_TEXT_FLAG_MULTI_LINE |
                InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        builder.setView(input);

        input.setText(sessionManager.getRemark());
        input.setSelection(input.getText().length());


// Set up the buttons
        builder.setPositiveButton("Continue", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog1, int which) {
                //    m_Text = input.getText().toString();
                headernotes = input.getText().toString();
              /*  if (getString(R.string.isdate).equals("1")) {

                    showdeliverydialog();

                   *//* DatePickerFragment mDatePicker = new DatePickerFragment();
                    mDatePicker.setStyle(DialogFragment.STYLE_NORMAL, R.style.CustomDialog);
                    mDatePicker.show(getFragmentManager(), "Select delivery date");*//*

                } else if (getString(R.string.isaddress).equals("1")) {
                    showdeliverydialog();
                   *//* new AddAddress(OrderActivity.this, new AddAddress.OnDialogClickListener() {
                        @Override
                        public void onDialogImageRunClick(int positon,String add) {
                            if (positon == 0) {

                            } else if (positon == 1) {
                                //str_actionid = str_actionid.replaceAll(",$", "");
                                dialog.setMessage("Creating your order ...");
                                dialog.show();
                                billamount();
                                sessionManager.setDate("");
                                sessionManager.setAddress(add);
                                sessionManager.setRemark(headernotes);
                                usersignin(sessionString, cartdetails, userdetails, "", add);
                            }
                        }
                    },sessionManager.getAddress()).show();*//*

                } else {
                    billamount();
                    sessionManager.setDate("");
                    sessionManager.setAddress("");
                    sessionManager.setRemark(headernotes);
                    usersignin(sessionString, cartdetails, userdetails, "", "");
                }
*/


               /* dialog.setMessage("Creating your order ...");
                dialog.show();
                billamount();

                sessionManager.setRemark(headernotes);
                usersignin(sessionString, cartdetails, userdetails);*/
            }
        });
        builder.setNegativeButton("Skip", null);
        builder.show();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
        if (rl_upload.getVisibility() == View.VISIBLE) {

        } else {
            super.onBackPressed();
        }
    }

    private void showBottomSheetFragment() {

        CatalogListDialogFragment fragmentModalBottomSheet = new CatalogListDialogFragment();
        fragmentModalBottomSheet.show(getSupportFragmentManager(), "BottomSheet Fragment");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in Androidcreatechit
        //
        //
        // Manifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement


        return super.onOptionsItemSelected(item);
    }

    public void usersignin(final String usersession, JSONArray cartdetails, final JSONObject userdetails, final String dt, final String address, final JSONObject joDelivery) {
        productlist = dbHelper.getAllcaretproduct();

        final JsonObject sendobj = new JsonObject();
        JsonArray a = new JsonArray();
        JsonArray cartproduct = new JsonArray();

        for (int i = 0; i < productlist.size(); i++) {
            Catelog product = productlist.get(i);
            //  JSONObject productobj= null;
            //  try {
            //       productobj = cartdetails.getJSONObject(i);
            JsonObject productsync = new JsonObject();
              /*  productsync.put("entry_id",productobj.getString("entry_id"));
                productsync.put("bridge_id",productobj.getString("bridge_id"));
                productsync.put("flag","add");
                productsync.put("quantity",productobj.getString("cartcount"));
                productsync.put("particulars",productobj.getString("name"));
                productsync.put("price",productobj.getString("mrp"));


*/

             /*    productobj.put("bridge_id",productobj.getString("bridge_id"));
                productobj.put("bridge_id",productobj.getString("bridge_id"));*/
            // cartproduct.put(productsync);

           /* productsync.addProperty("entry_id",product.getEntry_id());
            productsync.addProperty("bridge_id",product.getBridge_id());
            productsync.addProperty("flag","add");
            productsync.addProperty("quantity",product.getCartcount());
            productsync.addProperty("particulars",product.getDefault_name());
            productsync.addProperty("price",product.getMrp());*/

            final SettingDB db = new SettingDB(this);
            SessionManager sessionManager = new SessionManager(this);

            List<SettingModel> list_setting = db.GetItems();
            SettingModel sm = new SettingModel();
            sm = list_setting.get(0);


            String particular = product.getDefault_name();

            if (service_type_of.equalsIgnoreCase(getString(R.string.property))) {
                ShareModel.getI().chit_name = product.getCross_reference();
                ShareModel.getI().parti_nm = product.getDefault_name();
            }

            String cross_reference = product.getCross_reference();
            if (cross_reference != null && !cross_reference.equals("")) {
                particular = particular + "; [" + cross_reference + "]";
            }
            String offer = product.getOffer();
            if (offer != null && !offer.equals("") && sm.getIsconcateoffer().equals("1")) {
                particular = particular + "; " + product.getOffer();
            }
            al_selet = product.getAl_selet();

            if (al_selet.size() > 0) {
                String samples = new Gson().toJson(al_selet);
                particular = particular + ";;; " + samples;
            }

            al_Image = product.getImageArr();


            String samples = new Gson().toJson(al_Image);
            if (al_Image.size() > 0) {
                particular = particular + ";;;; " + samples;
            }

            JSONObject joPart = new JSONObject();
            try {

                cross_reference = product.getCross_reference();
                if (cross_reference != null && !cross_reference.equals("")) {
                    joPart.put("xRef", product.getCross_reference());

                }
                offer = product.getOffer();
                if (offer != null && !offer.equals("") && sm.getIsconcateoffer().equals("1")) {
                    joPart.put("offer", product.getOffer());
                }
                samples = new Gson().toJson(al_Image);
                if (al_Image.size() > 0) {
                    joPart.put("samples", samples);
                }
                joPart.put("name", product.getDefault_name());

                String doctor_template_type = JsonObjParse.getValueEmpty(productlist.get(i).getInternal_json_data(), "doctor_template_type");
                if (!doctor_template_type.isEmpty()) {

                    String internal_json = productlist.get(i).getInternal_json_data();
                    try {
                        joPart.put("internal_json_data", internal_json);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    if (doctor_template_type.equalsIgnoreCase("RPD") || doctor_template_type.equalsIgnoreCase("Precision Attachment")) {
                        joPart.put("additional_price", String.valueOf(cal_Addprice(i)));
                    }
                }

                joPart.put("delivery_service", joDelivery);
                joPart.put("purpose", purpose);


            } catch (JSONException e) {
                e.printStackTrace();
            }
            productsync.addProperty("entry_id", product.getEntry_id());
            productsync.addProperty("bridge_id", product.getBridge_id());
            productsync.addProperty("flag", "add");
            productsync.addProperty("quantity", product.getCartcount());
            productsync.addProperty("particulars", joPart.toString());
            Log.d("part", obj.toString());
            productsync.addProperty("price", String.valueOf(cal_price(i)));
            productsync.addProperty("offer", product.getOffer());
            //productsync.addProperty("reply_particulars", "Sample"); // json

            cartproduct.add(productsync);

          /*  } catch (JSONException e) {
                e.printStackTrace();
            }*/
        }

        try {
            JsonObject a1 = new JsonObject();
         /*  a1.put("email","athi123@gmail.com");
           a1.put("contact_number","22222222222");
           a1.put("location","");
           a1.put("chit_name","testform");*/
            a1.addProperty("email", loggedin.getEmail_id());
            a1.addProperty("contact_number", loggedin.getContact_no());
            a1.addProperty("location", loggedin.getLocation());
            a1.addProperty("chit_name", loggedin.getFirstname());
            a1.addProperty("header_note", headernotes);

            //a1.addProperty("footer_note", jo_footer.toString());
            sendobj.add("newdata", cartproduct);
            sendobj.add("olddata", a);
            sendobj.add("chitHeader", a1);

        } catch (JsonParseException e) {
            e.printStackTrace();
        }

        Log.e("sendobj", sendobj.toString());

        ApiInterface apiService =
                ApiClient.getClient(OrderActivity.this).create(ApiInterface.class);
        String currency_code = SharedPrefUserDetail.getString(OrderActivity.this, SharedPrefUserDetail.chit_currency_code, "");
        String currency_code_id = SharedPrefUserDetail.getString(OrderActivity.this, SharedPrefUserDetail.chit_currency_code_id, "");

        Call call = apiService.createchit(usersession, sendobj, sessionManager.getAggregatorprofile(), frombridgeidval, "d97be841a6d6cbb66bc3ae165b31b85f", currency_code, currency_code_id, "2");
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                int statusCode = response.code();
                Log.e("test", " trdds");

                Headers headerList = response.headers();
                Log.d("test", statusCode + " ");

                //  String cookie = response.;
                //  Log.d("test", cookie+ " ");

                dialog.dismiss();
                if (response.isSuccessful()) {
                    try {
                        String a = new Gson().toJson(response.body());
                        Log.e("response", a);
                        JSONObject responseobj = new JSONObject(a);
                        JSONObject responseval = responseobj.getJSONObject("response");

                        JSONObject data = responseval.getJSONObject("data");
                        String chithashid = data.getString("chitHashId");
                        JSONObject newobj = new JSONObject();
                        try {
                            JsonObject jObj = new JsonObject();
                            jObj.addProperty("email", loggedin.getEmail_id());
                            jObj.addProperty("contact_number", sessionManager.getT_Phone());
                            jObj.addProperty("location", sessionManager.getAddress());
                            jObj.addProperty("act", sessionManager.getcurrentcaterername());
                            jObj.addProperty("chit_name", "");
                            jObj.addProperty("chitHashId", chithashid);
                            jObj.addProperty("latitude", "");
                            jObj.addProperty("longtitude", "");
                            jObj.addProperty("expected_delivery_time", "");
                            jObj.addProperty("for_non_bridge_name", "");
                            jObj.addProperty("subject", sessionManager.getcurrentcatereraliasname());

                            String currency_code = SharedPrefUserDetail.getString(OrderActivity.this, SharedPrefUserDetail.chit_currency_code, "");
                            String symbol_native = SharedPrefUserDetail.getString(OrderActivity.this, SharedPrefUserDetail.chit_symbol_native, "");
                            String currency_code_id = SharedPrefUserDetail.getString(OrderActivity.this, SharedPrefUserDetail.chit_currency_code_id, "");
                            String symbol = SharedPrefUserDetail.getString(OrderActivity.this, SharedPrefUserDetail.chit_symbol, "");

                            jObj.addProperty("currency_code", currency_code);
                            jObj.addProperty("symbol_native", symbol_native);
                            jObj.addProperty("currency_code_id", currency_code_id);
                            jObj.addProperty("symbol", symbol);
                            //jObj.addProperty("info", sessionManager.getAggregator_ID());


                            boolean iss = SharedPrefUserDetail.getBoolean(OrderActivity.this, SharedPrefUserDetail.isfromsupplier, false);
                            if (iss) // for supplier
                            {
                                jObj.addProperty("info", "");
                                StringBuilder stringBuilder = new StringBuilder();
                                if (field_json_data != null) {

                                    String delivery_partner_info = JsonObjParse.getValueEmpty(field_json_data, "delivery_partner_info");
                                    if (!delivery_partner_info.isEmpty()) {
                                        stringBuilder.append(delivery_partner_info + " , ");
                                        jObj.addProperty("info", stringBuilder.toString());
                                    }
                                }
                            } else {

                                // if its aggre / circle
                                // blrmc1(delivery flag of this) under blrmcc(info flag of this)
                                String info = SharedPrefUserDetail.getString(OrderActivity.this, SharedPrefUserDetail.infoMainShop, "");
                                //String act = sessionManager.getAggregator_ID();
                                String act = sessionManager.getcurrentcaterername();
                                //String infoPass = info + " , " + act + " , ";
                                StringBuilder stringBuilder = new StringBuilder(); // old is correct
                                //if(!info.isEmpty())stringBuilder.append(info+" , ");
                                //if(!act.isEmpty())stringBuilder.append(act+" , ");
                                if (field_json_data_info != null) {
                                    String pass_info_circle = JsonObjParse.getValueEmpty(field_json_data_info, "pass_info_circle");
                                    String pass_info_aggregator = JsonObjParse.getValueEmpty(field_json_data_info, "pass_info_aggregator");
                                    if (!pass_info_circle.equalsIgnoreCase("no") && business_type_info.equalsIgnoreCase("circle")) {
                                        if (!info.isEmpty()) stringBuilder.append(info + " , ");
                                    }
                                    if (!pass_info_aggregator.equalsIgnoreCase("no") && business_type_info.equalsIgnoreCase("aggregator")) {
                                        if (!info.isEmpty()) stringBuilder.append(info + " , ");
                                    }
                                }

                                if (!act.isEmpty()) stringBuilder.append(act + " , ");

                                if (field_json_data != null) {

                                    String delivery_partner_info = JsonObjParse.getValueEmpty(field_json_data, "delivery_partner_info");
                                    if (!delivery_partner_info.isEmpty()) {
                                        stringBuilder.append(delivery_partner_info + " , ");
                                    }
                                }

                                Log.d("okhttp info", stringBuilder.toString());
                                jObj.addProperty("info", stringBuilder.toString());
                                //jObj.addProperty("info", sessionManager.getAggregator_ID());


                            }


                            jObj.addProperty("header_note", headernotes);


                            jObj.addProperty("purpose", purpose);

                            jObj.addProperty("chit_item_count", productcount);
                            jObj.addProperty("total_chit_item_value", String.valueOf(grandtotal));
                            JsonObject jo_footer = new JsonObject();
                            jo_footer.addProperty("SGST", String.valueOf(totalstategst));
                            jo_footer.addProperty("VAT", String.valueOf(totalVat));
                            jo_footer.addProperty("CGST", String.valueOf(totalcentralgst));
                            jo_footer.addProperty("TOTALBILL", String.valueOf(totalbill));
                            jo_footer.addProperty("GRANDTOTAL", String.valueOf(grandtotal));
                            jo_footer.addProperty("NAME", loggedin.getFirstname());

                            final SettingDB db = new SettingDB(OrderActivity.this);
                            List<SettingModel> list_setting = db.GetItems();
                            SettingModel sm = new SettingModel();
                            sm = list_setting.get(0);

                            if (sm.getIspayumoney().equals("1")) {
                                jo_footer.addProperty("PAYMENT_MODE", "RAZORPAY");
                            } else {
                                jo_footer.addProperty("PAYMENT_MODE", "COD");
                            }


                            jo_footer.addProperty("DATE", dt);
                            jo_footer.addProperty("ADDRESS", address);
                            jObj.addProperty("footer_note", jo_footer.toString());
                            //jObj.addProperty("total_chit_item_value", pricevalue);
                            // sendobj.put("chitHeader",newobj);
                            Log.e("chitheadervalue", jObj.toString());
                            showorderconfirmation(jObj, chithashid, address, dt, joDelivery);
                            // updatechit(usersession,jObj,chithashid);
                        } catch (JsonParseException e) {
                            e.printStackTrace();
                        }
                    } catch (Exception e) {
                        Log.e("errortest", e.getMessage());

                        Toast.makeText(OrderActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                    //  Intent i = new Intent(MainActivity.this, MainActivity.class);
                    //  startActivity(i);
                } else {
                    Log.e("test", " trdds1");

                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Toast.makeText(OrderActivity.this, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(OrderActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }

                //  List<Movie> movies = response.body().getResults();
                //  recyclerView.setAdapter(new MoviesAdapter(movies, R.layout.list_item_movie, getApplicationContext()));
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // Log error here since request failed
                Log.e("error message", t.toString());
            }
        });
    }

    public void showorderconfirmation(JsonObject hashobj, String hashstring, String add, String dt, JSONObject joDelivery) {
        Intent i = new Intent(OrderActivity.this, InvoicesummaryActivity.class);
        i.putExtra("hashheadervalue", hashobj.toString());
        i.putExtra("hashid", hashstring);
        i.putExtra("add", add);
        i.putExtra("dt", dt);
        i.putExtra("purpose", purpose);
        i.putExtra("img", img);
        i.putExtra("isBk", true);
        i.putExtra("delivery_charge", delivery_charge);
        i.putExtra("delivery_detail", joDelivery.toString());
        startActivity(i);
    }

    public void updatechit(String usersession, JsonObject chitdetails, String hashid) {

        Log.e("response", "test" + chitdetails.toString());
        Log.e("response", "test" + bridgeidval);
        Log.e("response", "test" + hashid);
        // Gson gson = new Gson();
        // gson.toJson(chitdetails.toString());                   // prints 1


        ApiInterface apiService =
                ApiClient.getClient(OrderActivity.this).create(ApiInterface.class);
        Log.e("beforeapical", chitdetails.toString());
        // String json = chitdetails.toString();
        String json = new Gson().toJson(chitdetails);
        Call call = apiService.updatechit(usersession, chitdetails, "d97be841a6d6cbb66bc3ae165b31b85f", hashid, "d97be841a6d6cbb66bc3ae165b31b85f", "", "", "2");
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                int statusCode = response.code();
                Log.e("test", " trddsdadass" + response.code());

                Headers headerList = response.headers();
                Log.d("test", statusCode + " ");

                //  String cookie = response.;
                //  Log.d("test", cookie+ " ");


                if (response.isSuccessful()) {
                    orderplacedsuccess();
                    try {
                        String a = new Gson().toJson(response.body());
                        Log.e("response", a);


                    } catch (Exception e) {
                        Log.e("errortest", e.getMessage());

                        Toast.makeText(OrderActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                    //  Intent i = new Intent(MainActivity.this, MainActivity.class);
                    //  startActivity(i);
                } else {


                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Log.e("test", " trdds1" + jObjErrorresponse.toString());
                        Toast.makeText(OrderActivity.this, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(OrderActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }

                //  List<Movie> movies = response.body().getResults();
                //  recyclerView.setAdapter(new MoviesAdapter(movies, R.layout.list_item_movie, getApplicationContext()));
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // Log error here since request failed
                Log.e("error message", t.toString());
            }
        });
    }

    public int getrandom() {
        int min = 0001;
        int max = 9999;
        Random r = new Random();
        int i1 = r.nextInt(max - min + 1) + min;
        return i1;

    }

    public void orderplacedsuccess() {
        sessionManager.setcurrentcaterer("");
        sessionManager.setcurrentcaterername("");
        sessionManager.setCartproduct("");
        sessionManager.setCartcount(0);
        Intent i = new Intent(OrderActivity.this, PaymentActivity.class);
        startActivity(i);
    }

    boolean isSetFst = false; // bcz of after reset item to 0 going to selct image and add image product list is 0

    @Override
    public void onResume() {
        super.onResume();

        if (isEdit) {
            Intent intent = getIntent();
            finish();
            startActivity(intent);
            isEdit = false;
            Log.d("checkEdit", "yyyyy");
        } else {
            Log.d("checkEdit", "nnnnnnnn");
        }


        if (isSetFst) {
            return;
        }
        isSetFst = true;

        counterFab.setCount(sessionManager.getCartcount());
        String productliststr = sessionManager.getCartproduct();
        productcountval.setText("#" + dbHelper.getcartcount());

        try {
            JSONArray jsonArray = new JSONArray(productliststr);
            for (int i = 0; i < jsonArray.length(); i++) {
                if (jsonArray.get(i) instanceof JSONObject) {
                    JSONObject jsnObj = (JSONObject) jsonArray.get(i);
                    Catelog obj = new Gson().fromJson(jsnObj.toString(), Catelog.class);
                    Log.e("alias", obj.getName());
                    if (selectedproductlist.contains(String.valueOf(obj.getEntry_id()))) {
                        int a = selectedproductlist.indexOf(String.valueOf(obj.getEntry_id()));
                        selectedproduct.set(a, obj);
                        pricevalue = pricevalue + (obj.getCartcount() * Double.parseDouble(obj.getMrp()));

                    } else {
                        selectedproductlist.add(String.valueOf(obj.getEntry_id()));
                        selectedproduct.add(obj);
                        pricevalue = pricevalue + (obj.getCartcount() * Double.parseDouble(obj.getMrp()));
                    }
                }
            }
            Log.e("cartproduct", jsonArray.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        //final NumberFormat format = NumberFormat.getCurrencyInstance();
        //format.setCurrency();
        Log.e("additempricevalue", "" + pricevalue);
        Double car_total = dbHelper.getcarttotal();

        // productpriceval.setText(getString(R.string.currency) + String.format("%.2f", car_total));
        // put your code here...


        String userprofile = sessionManager.getUserprofile();
        try {

            Userprofile obj = new Gson().fromJson(userprofile, Userprofile.class);
            if (purpose.equalsIgnoreCase("service")) {
            } else {
                sessionManager.setAddress(obj.getLocation());
            }


        } catch (Exception e) {

        }


    }


    @Override
    public void onItemClick(Catelog item) {
        if (item.getImg_url() == null || item.getImg_url().equals("[]")) {
            Toast.makeText(this, "No Media found", Toast.LENGTH_LONG).show();
        } else {
          /*  Gson gson = new Gson();
            TypeToken<ArrayList<String>> token = new TypeToken<ArrayList<String>>() {
            };
            GalleryActivity.images = gson.fromJson(item.getImg_url(), token.getType());
            ArrayList<String> a = new ArrayList<>();
            a = item.getVideolArr();

            if (a == null || a.size() == 0) {
                a = new ArrayList<>();
                //Video array testing this way
                //  a.add("https://s3-ap-southeast-1.amazonaws.com/cab-videofiles/ms.mp4");
            }

            GalleryActivity.videos = a;
            Intent i = new Intent(OrderActivity.this, GalleryActivity.class);
            startActivity(i);*/

            ArrayList<ModelFile> al_img = new ArrayList<>();
            try {

                for (int i = 0; i < item.getImageArr().size(); i++) {
                    al_img.add(new ModelFile(item.getImageArr().get(i).toString(), false));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            String imgarray = new Gson().toJson(al_img);


            Intent intent = new Intent(OrderActivity.this, ImagePreviewActivity.class);
            intent.putExtra("imgarray", imgarray);
            intent.putExtra("pos", 0);
            intent.putExtra("img_title", item.getDefault_name());
            startActivity(intent);


        }
    }

    Calendar cal_order = Calendar.getInstance();


    // @SuppressLint("ValidFragment")
   /* public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {
        @Override
        public void onCancel(DialogInterface dialog) {
            super.onCancel(dialog);
            showdeliverydialog();
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar c = Calendar.getInstance();


            String dtStart = sessionManager.getDate();
            SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy hh:mm aaa");
            try {
                if (!dtStart.equals("")) {
                    Date date = format.parse(dtStart);
                    c.setTime(date);
                    System.out.println(date);
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }

            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {


            SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy hh:mm aaa");

            cal_order.set(year, month, day);
            showdeliverydialog();

            //String date = df.format(calendar.getTime());
            //displayCurrentTime.setText("Selected date: " + String.valueOf(year) + " - " + String.valueOf(month) + " - " + String.valueOf(day));
        }
    }

    DatePickerFragment mDatePicker;*/

    // @SuppressLint("ValidFragment")
   /* public class TimePicker extends DialogFragment implements TimePickerDialog.OnTimeSetListener {
        @Override
        public void onCancel(DialogInterface dialog) {
            super.onCancel(dialog);
            showdeliverydialog();
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar c = Calendar.getInstance();

            String dtStart = sessionManager.getDate();
            SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy hh:mm aaa");
            try {
                if (!dtStart.equals("")) {
                    Date date = format.parse(dtStart);
                    c.setTime(date);
                    System.out.println(date);
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }

            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);
            return new TimePickerDialog(getActivity(), this, hour, minute, DateFormat.is24HourFormat(getActivity()));
        }

        @Override
        public void onTimeSet(android.widget.TimePicker view, int hourOfDay, int minute) {

            cal_order.set(cal_order.get(Calendar.YEAR), cal_order.get(Calendar.MONTH), cal_order.get(Calendar.DAY_OF_MONTH), hourOfDay, minute);

            showdeliverydialog();
            SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy hh:mm aaa");
            final String date_ = df.format(cal_order.getTime());
        }
    }*/


    public void showdeliverydialog() {
        Userprofile obj = null;
        String userprofile = sessionManager.getUserprofile();
        try {

            obj = new Gson().fromJson(userprofile, Userprofile.class);

            if (sessionManager.getT_Phone().equals("")) {
                sessionManager.setT_Phone(obj.getContact_no());
            }


        } catch (Exception e) {

        }


        final SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy hh:mm aaa");
        if (purpose.equalsIgnoreCase(getString(R.string.delivery))) {
            new MrpDeliveryDialog(OrderActivity.this, new MrpDeliveryDialog.OnDialogClickListener() {
                @Override
                public void onDialogImageRunClick(int positon, JSONObject joDelivery) {
                    if (positon == 0) {

                        uplaodQueue(sessionString, cartdetails, userdetails, "", "", joDelivery);
                        //usersignin(sessionString, cartdetails, userdetails, date_, value);


                    } else if (positon == 1) {
                        DetePickerFragment mDatePicker = new DetePickerFragment(OrderActivity.this);
                        mDatePicker.setStyle(DialogFragment.STYLE_NORMAL, R.style.CustomDialog);
                        mDatePicker.show(getSupportFragmentManager(), "Select delivery date");

                    } else if (positon == 2) {
                        TimePicker mTimePicker = new TimePicker(OrderActivity.this);
                        mTimePicker.show(getSupportFragmentManager(), "Select delivery time");
                    } else if (positon == 3) {

                   /* new AddAddress(OrderActivity.this, new AddAddress.OnDialogClickListener() {
                        @Override
                        public void onDialogImageRunClick(int positon, String add) {

                            if (positon == 0) {

                            } else if (positon == 1) {
                                sessionManager.setAddress(add);
                                //str_actionid = str_actionid.replaceAll(",$", "");
                            }
                            showdeliverydialog();
                        }
                    }, sessionManager.getAddress()).show();*/
                    }
                }
            }, sessionManager.getAddress(), df.format(cal_order.getTime()), "", sessionManager.getRemark(), sessionManager.getT_Phone()).show();

        } else {
            new DeliveryDialog(OrderActivity.this, new DeliveryDialog.OnDialogClickListener() {
                @Override
                public void onDialogImageRunClick(int positon, final String value, String remark, String phone) {
                    if (positon == 0) {

                        headernotes = remark;
                        final String date_ = df.format(cal_order.getTime());


                        billamount();
                        sessionManager.setDate(date_);
                        sessionManager.setAddress(value);
                        sessionManager.setRemark(headernotes);
                        sessionManager.setT_Phone(phone);

                        uplaodQueue(sessionString, cartdetails, userdetails, date_, value, null);
                        //usersignin(sessionString, cartdetails, userdetails, date_, value);


                    } else if (positon == 1) {
                        DetePickerFragment mDatePicker = new DetePickerFragment(OrderActivity.this);
                        mDatePicker.setStyle(DialogFragment.STYLE_NORMAL, R.style.CustomDialog);
                        mDatePicker.show(getSupportFragmentManager(), "Select delivery date");

                    } else if (positon == 2) {
                        TimePicker mTimePicker = new TimePicker(OrderActivity.this);
                        mTimePicker.show(getSupportFragmentManager(), "Select delivery time");
                    } else if (positon == 3) {

                   /* new AddAddress(OrderActivity.this, new AddAddress.OnDialogClickListener() {
                        @Override
                        public void onDialogImageRunClick(int positon, String add) {

                            if (positon == 0) {

                            } else if (positon == 1) {
                                sessionManager.setAddress(add);
                                //str_actionid = str_actionid.replaceAll(",$", "");
                            }
                            showdeliverydialog();
                        }
                    }, sessionManager.getAddress()).show();*/
                    }
                }
            }, sessionManager.getAddress(), df.format(cal_order.getTime()), "", sessionManager.getRemark(), sessionManager.getT_Phone()).show();

        }

    }


    @Override
    public void onItemClick(Boolean isTrue, int year, int month, int dayOfMonth) {
        if (isTrue) {
            cal_order.set(year, month, dayOfMonth);
            showdeliverydialog();
        } else {
            showdeliverydialog();
        }
    }

    @Override
    public void onItemClick(Boolean isTrue, int hourOfDay, int minute) {
        if (isTrue) {
            cal_order.set(cal_order.get(Calendar.YEAR), cal_order.get(Calendar.MONTH), cal_order.get(Calendar.DAY_OF_MONTH), hourOfDay, minute);
            showdeliverydialog();

        } else {
            showdeliverydialog();
        }
    }


    @Override
    public void onadditem(Catelog product) {


        String wholesale = JsonObjParse.getValueEmpty(product.getInternal_json_data(), "wholesale");
        if (!wholesale.isEmpty()) {

            Apputil.inserttocartWholesale(dbHelper, product, "");
            counterFab.setCount(dbHelper.getcartcount());

        } else {
            counterFab.increase();
            Apputil.inserttocart(dbHelper, product, "");
        }


        productcountval.setText("#" + dbHelper.getcartcount());
        //final NumberFormat format = NumberFormat.getCurrencyInstance();

        Double car_total = dbHelper.getcarttotal();

        //productpriceval.setText(getString(R.string.currency) + String.format("%.2f", car_total));

        double total_with_tax = 0;
        productlist = dbHelper.getAllcaretproduct();
        for (int i = 0; i < productlist.size(); i++) {

            total_with_tax = total_with_tax + cal_price2(i);

        }
        //productpriceval.setText(currency + String.format("%.2f", total_with_tax));
        productpriceval.setText(currency + Inad.getCurrencyDecimal(total_with_tax, this));
    }

    @Override
    public void ondecreasecount(Catelog product) {
        counterFab.decrease();
        if (product.getCartcount() == 0) {
            dbHelper.deleteRecord(product);

        } else {

            ArrayList<Catelog> productlist = new ArrayList<>();
            productlist = dbHelper.getAllcaretproduct();
            //String levelPatternGson = new Gson().toJson(productlist);
            //c = new Gson().fromJson(levelPatternGson,Catelog.class);

            for (int i = 0; i < productlist.size(); i++) {
                if (productlist.get(i).getEntry_id() == product.getEntry_id()) {

                    Gson gson = new Gson();
                    TypeToken<ArrayList<String>> token = new TypeToken<ArrayList<String>>() {
                    };
                    ArrayList<String> pb = gson.fromJson(productlist.get(i).getImg_url(), token.getType());
                    product.setImageArr(pb);
                    product.setImg_url(productlist.get(i).getImg_url());

                    break;

                }
            }


            dbHelper.updateRecord(product);
        }
        final NumberFormat format = NumberFormat.getCurrencyInstance();
        //format.setCurrency();
        Double car_total = dbHelper.getcarttotal();

        //productpriceval.setText(getString(R.string.currency) + String.format("%.2f", car_total));
        productcountval.setText("#" + dbHelper.getcartcount());

        double total_with_tax = 0;
        productlist = dbHelper.getAllcaretproduct();
        for (int i = 0; i < productlist.size(); i++) {

            total_with_tax = total_with_tax + cal_price2(i);

        }
        //productpriceval.setText(currency + String.format("%.2f", total_with_tax));
        productpriceval.setText(currency + Inad.getCurrencyDecimal(total_with_tax, this));

        if (dbHelper.getcartcount() == 0) {
            Inad.alerterInfo("Message", "Cart is empty, please add items..", this);
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    finish();
                }
            }, 2000);
        }
    }

    public void billamount() {
        ArrayList<Catelog> productlist = new ArrayList<>();
        productlist = dbHelper.getAllcaretproduct();

        totalbill = 0.0;
        totalstategst = 0.0;
        totalVat = 0.0;
        totalcentralgst = 0.0;
        for (int i = 0; i < productlist.size(); i++) {
            int qty = productlist.get(i).getCartcount();
            Double bill = Double.valueOf(productlist.get(i).getdiscountmrp()) * qty;
            totalbill = totalbill + bill;
            Log.e("totalbil", productlist.get(i).getdiscountmrp());
            Log.e("qty", String.valueOf(qty));
            Double sgst = 0.0;
            Double cgst = 0.0;
            Double vat = 0.0;
            try {
                String tax = productlist.get(i).getTax_json_data();

                if (tax != null) {
                    JSONObject obj = new JSONObject(tax);
                    if (obj.has("VAT")) {
                        vat = Double.valueOf(obj.getString("VAT"));
                    } else {
                        sgst = Double.valueOf(obj.getString("SGST"));
                        cgst = Double.valueOf(obj.getString("CGST"));
                    }
                }
                Log.e("stategst", String.valueOf(sgst));
                Log.e("stategst", String.valueOf(cgst));
            } catch (JSONException e) {
                e.printStackTrace();
            }


            Double totalamt = bill;
            Double stategst = (totalamt / 100.0f) * sgst;
            Log.e("stategst", String.valueOf(stategst));
            totalstategst = totalstategst + stategst;

            Double centralgst = (totalamt / 100.0f) * cgst;
            Log.e("stategst", String.valueOf(centralgst));
            totalcentralgst = totalcentralgst + centralgst;

            Double valueAt = (totalamt / 100.0f) * vat;
            Log.e("valueAt", String.valueOf(valueAt));
            totalVat = totalVat + valueAt;
        }


        grandtotal = totalbill + totalcentralgst + totalstategst + totalVat;

        /*centralgstamount.setText(""+ String.format("%.2f", totalcentralgst));
        stategstamount.setText(""+ String.format("%.2f", totalstategst));
        billamount.setText(""+ String.format("%.2f", totalbill));
        grandtotalamount.setText(""+String.format("%.2f", grandtotal));
        String.format("%.2f", grandtotal);*/
        Log.e("totalstategst", String.valueOf(totalstategst));
        Log.e("final", String.valueOf(totalbill));
    } // cal price all

    public Double cal_price(int i) {

        Double totalbill_ = 0.0;


        ArrayList<Catelog> productlist = new ArrayList<>();
        productlist = dbHelper.getAllcaretproduct();


        int qty = productlist.get(i).getCartcount();
        Double bill = Double.valueOf(productlist.get(i).getdiscountmrp());
        String doctor_template_type = JsonObjParse.getValueEmpty(productlist.get(i).getInternal_json_data(), "doctor_template_type");

        /*if (doctor_template_type.equalsIgnoreCase("RPD")) {
            Double Amrp = 0.0;
            Log.d("additional", JsonObjParse.getValueEmpty(productlist.get(i).getInternal_json_data(), "additionalPrice_rpd") + "");
            if (!JsonObjParse.getValueEmpty(productlist.get(i).getInternal_json_data(), "additionalPrice_rpd").isEmpty()) {
                Amrp = Double.valueOf(JsonObjParse.getValueEmpty(productlist.get(i).getInternal_json_data(), "additionalPrice_rpd"));
                bill = bill + Amrp;
            }
        }*/
        //Double bill = Double.valueOf(productlist.get(i).getdiscountmrp()) * qty;

        Double ded_price_val = 0.0, addi_price_val = 0.0;
        Double ded_perc_val = 0.0, addi_perc_val = 0.0;
        Double ded_mrp = 0.0, addi_mrp = 0.0;

        Catelog catelog = productlist.get(i);
        String jewel = JsonObjParse.getValueEmpty(productlist.get(i).getInternal_json_data(), "jewel");
        if (!jewel.isEmpty()) {

               /* String weight = JsonObjParse.getValueEmpty(jewel, "weight");

                if (!weight.isEmpty()) {
                    float wt = Float.parseFloat(weight);
                    //dis_mrp = (dis_mrp * wt)/10; // 10 gram
                    mrp = (mrp * wt) / 1; // 1 unit
                }
*/
            try {
                String DeductionJewelPrice = JsonObjParse.getValueEmpty(jewel, "DeductionJewelPrice");
                ded_price_val = catelog.getAdditionalDeductinalInfo(DeductionJewelPrice, bill);
                ded_mrp = Double.valueOf(catelog.getValues(bill, ded_perc_val, ded_price_val));
            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {
                String AdditionalJewelPrice = JsonObjParse.getValueEmpty(jewel, "AdditionalJewelPrice");
                addi_price_val = catelog.getAdditionalDeductinalInfo(AdditionalJewelPrice, bill);
                addi_mrp = Double.valueOf(catelog.getValues(bill, addi_perc_val, addi_price_val));
            } catch (JSONException e) {
                e.printStackTrace();
            }

            bill = bill - ded_mrp + addi_mrp;

        }

        bill = bill;

        Double sgst = 0.0;
        Double cgst = 0.0;
        Double vat = 0.0;
        try {
            String tax = productlist.get(i).getTax_json_data();
            if (tax != null) {

                /*JSONObject obj = new JSONObject(tax);
                if(obj.has("VAT"))
                {
                    vat = Double.valueOf(obj.getString("VAT"));
                }
                else {
                    sgst = Double.valueOf(obj.getString("SGST"));
                    cgst = Double.valueOf(obj.getString("CGST"));
                }*/

                boolean isF1 = true;

                JSONObject joTax = new JSONObject(tax);

                Iterator keys = joTax.keys();

                while (keys.hasNext()) {
                    try {
                        String key = (String) keys.next();

                        String taxVal = JsonObjParse.getValueFromJsonObj(joTax, key);

                        if (isF1) {
                            isF1 = false;
                            sgst = Double.valueOf(taxVal);


                        } else {
                            cgst = Double.valueOf(taxVal);

                        }

                        Log.e("key", key);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }


            }

            Log.e("stategst", String.valueOf(sgst));
            Log.e("stategst", String.valueOf(cgst));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Double totalamt = bill;
        Double stategst = (totalamt / 100.0f) * sgst;
        Double centralgst = (totalamt / 100.0f) * cgst;

        vat = (totalamt / 100.0f) * vat;

        totalbill_ = bill + centralgst + stategst + vat;

        /*centralgstamount.setText(""+ String.format("%.2f", totalcentralgst));
        stategstamount.setText(""+ String.format("%.2f", totalstategst));
        billamount.setText(""+ String.format("%.2f", totalbill));
        grandtotalamount.setText(""+String.format("%.2f", grandtotal));
        String.format("%.2f", grandtotal);*/
        Log.e("final", String.valueOf(totalbill_));

        return totalbill_;
    } //   cal price single

    public Double cal_Addprice(int i) {

        Double totalbill_ = 0.0;


        ArrayList<Catelog> productlist = new ArrayList<>();
        productlist = dbHelper.getAllcaretproduct();


        int qty = productlist.get(i).getCartcount();
        Double bill = Double.valueOf(productlist.get(i).getdiscountmrp());
        String doctor_template_type = JsonObjParse.getValueEmpty(productlist.get(i).getInternal_json_data(), "doctor_template_type");

        /*if (doctor_template_type.equalsIgnoreCase("RPD")) {
            Double Amrp = 0.0;
            Log.d("additional", JsonObjParse.getValueEmpty(productlist.get(i).getInternal_json_data(), "additionalPrice_rpd") + "");
            if (!JsonObjParse.getValueEmpty(productlist.get(i).getInternal_json_data(), "additionalPrice_rpd").isEmpty()) {
                Amrp = Double.valueOf(JsonObjParse.getValueEmpty(productlist.get(i).getInternal_json_data(), "additionalPrice_rpd"));
                bill = bill + Amrp;
            }
        }*/
        //Double bill = Double.valueOf(productlist.get(i).getdiscountmrp()) * qty;

        Double ded_price_val = 0.0, addi_price_val = 0.0;
        Double ded_perc_val = 0.0, addi_perc_val = 0.0;
        Double ded_mrp = 0.0, addi_mrp = 0.0;

        Catelog catelog = productlist.get(i);
        String jewel = JsonObjParse.getValueEmpty(productlist.get(i).getInternal_json_data(), "jewel");
        if (!jewel.isEmpty()) {

               /* String weight = JsonObjParse.getValueEmpty(jewel, "weight");

                if (!weight.isEmpty()) {
                    float wt = Float.parseFloat(weight);
                    //dis_mrp = (dis_mrp * wt)/10; // 10 gram
                    mrp = (mrp * wt) / 1; // 1 unit
                }
*/
            try {
                String DeductionJewelPrice = JsonObjParse.getValueEmpty(jewel, "DeductionJewelPrice");
                ded_price_val = catelog.getAdditionalDeductinalInfo(DeductionJewelPrice, bill);
                ded_mrp = Double.valueOf(catelog.getValues(bill, ded_perc_val, ded_price_val));
            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {
                String AdditionalJewelPrice = JsonObjParse.getValueEmpty(jewel, "AdditionalJewelPrice");
                addi_price_val = catelog.getAdditionalDeductinalInfo(AdditionalJewelPrice, bill);
                addi_mrp = Double.valueOf(catelog.getValues(bill, addi_perc_val, addi_price_val));
            } catch (JSONException e) {
                e.printStackTrace();
            }

            bill = bill - ded_mrp + addi_mrp;

        }

        bill = bill;

        if (doctor_template_type.equalsIgnoreCase("RPD") || doctor_template_type.equalsIgnoreCase("Precision Attachment")) {
            Double Amrp = 0.0;
            Log.d("additional", JsonObjParse.getValueEmpty(productlist.get(i).getInternal_json_data(), "additionalPrice_rpd") + "");
            if (!JsonObjParse.getValueEmpty(productlist.get(i).getInternal_json_data(), "additionalPrice_rpd").isEmpty()) {
                Amrp = Double.valueOf(JsonObjParse.getValueEmpty(productlist.get(i).getInternal_json_data(), "additionalPrice_rpd"));
                bill = bill + Amrp * (qty - 1);

            }
        }

        Double sgst = 0.0;
        Double cgst = 0.0;
        Double vat = 0.0;
        try {
            String tax = productlist.get(i).getTax_json_data();
            if (tax != null) {

                /*JSONObject obj = new JSONObject(tax);
                if(obj.has("VAT"))
                {
                    vat = Double.valueOf(obj.getString("VAT"));
                }
                else {
                    sgst = Double.valueOf(obj.getString("SGST"));
                    cgst = Double.valueOf(obj.getString("CGST"));
                }*/

                boolean isF1 = true;

                JSONObject joTax = new JSONObject(tax);

                Iterator keys = joTax.keys();

                while (keys.hasNext()) {
                    try {
                        String key = (String) keys.next();

                        String taxVal = JsonObjParse.getValueFromJsonObj(joTax, key);

                        if (isF1) {
                            isF1 = false;
                            sgst = Double.valueOf(taxVal);


                        } else {
                            cgst = Double.valueOf(taxVal);

                        }

                        Log.e("key", key);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }


            }

            Log.e("stategst", String.valueOf(sgst));
            Log.e("stategst", String.valueOf(cgst));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Double totalamt = bill;
        Double stategst = (totalamt / 100.0f) * sgst;
        Double centralgst = (totalamt / 100.0f) * cgst;

        vat = (totalamt / 100.0f) * vat;

        totalbill_ = bill + centralgst + stategst + vat;

        /*centralgstamount.setText(""+ String.format("%.2f", totalcentralgst));
        stategstamount.setText(""+ String.format("%.2f", totalstategst));
        billamount.setText(""+ String.format("%.2f", totalbill));
        grandtotalamount.setText(""+String.format("%.2f", grandtotal));
        String.format("%.2f", grandtotal);*/
        Log.e("final", String.valueOf(totalbill_));

        return totalbill_;
    }

    public Double cal_price2(int i) {

        Double totalbill_ = 0.0;


        ArrayList<Catelog> productlist = new ArrayList<>();
        productlist = dbHelper.getAllcaretproduct();


        int qty = productlist.get(i).getCartcount();
        //Double bill = Double.valueOf(productlist.get(i).getdiscountmrp());
        Double bill = Double.valueOf(productlist.get(i).getdiscountmrp());

        String doctor_template_type = JsonObjParse.getValueEmpty(productlist.get(i).getInternal_json_data(), "doctor_template_type");

        Catelog catelog = productlist.get(i);
        String jewel = JsonObjParse.getValueEmpty(productlist.get(i).getInternal_json_data(), "jewel");

        Double ded_price_val = 0.0, addi_price_val = 0.0;
        Double ded_perc_val = 0.0, addi_perc_val = 0.0;
        Double ded_mrp = 0.0, addi_mrp = 0.0;


        if (!jewel.isEmpty()) {

               /* String weight = JsonObjParse.getValueEmpty(jewel, "weight");

                if (!weight.isEmpty()) {
                    float wt = Float.parseFloat(weight);
                    //dis_mrp = (dis_mrp * wt)/10; // 10 gram
                    mrp = (mrp * wt) / 1; // 1 unit
                }
*/
            try {
                String DeductionJewelPrice = JsonObjParse.getValueEmpty(jewel, "DeductionJewelPrice");
                ded_price_val = catelog.getAdditionalDeductinalInfo(DeductionJewelPrice, bill);
                ded_mrp = Double.valueOf(catelog.getValues(bill, ded_perc_val, ded_price_val));
            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {
                String AdditionalJewelPrice = JsonObjParse.getValueEmpty(jewel, "AdditionalJewelPrice");
                addi_price_val = catelog.getAdditionalDeductinalInfo(AdditionalJewelPrice, bill);
                addi_mrp = Double.valueOf(catelog.getValues(bill, addi_perc_val, addi_price_val));
            } catch (JSONException e) {
                e.printStackTrace();
            }

            bill = bill - ded_mrp + addi_mrp;
        }


        if (doctor_template_type.equalsIgnoreCase("RPD") || doctor_template_type.equalsIgnoreCase("Precision Attachment")) {
            Double Amrp = 0.0;
            Log.d("additional", JsonObjParse.getValueEmpty(productlist.get(i).getInternal_json_data(), "additionalPrice_rpd") + "");
            if (!JsonObjParse.getValueEmpty(productlist.get(i).getInternal_json_data(), "additionalPrice_rpd").isEmpty()) {
                Amrp = Double.valueOf(JsonObjParse.getValueEmpty(productlist.get(i).getInternal_json_data(), "additionalPrice_rpd"));
                bill = bill + Amrp * (qty - 1);
            }
        } else {
            bill = bill * qty;
        }

        Double sgst = 0.0;
        Double cgst = 0.0;
        Double vat = 0.0;
        try {
            String tax = productlist.get(i).getTax_json_data();
            if (tax != null) {
               /* JSONObject obj = new JSONObject(tax);
                if(obj.has("VAT"))
                {
                    vat = Double.valueOf(obj.getString("VAT"));
                }
                else {
                    sgst = Double.valueOf(obj.getString("SGST"));
                    cgst = Double.valueOf(obj.getString("CGST"));
                }*/

                boolean isF1 = true;

                JSONObject joTax = new JSONObject(tax);

                Iterator keys = joTax.keys();

                while (keys.hasNext()) {
                    try {
                        String key = (String) keys.next();

                        String taxVal = JsonObjParse.getValueFromJsonObj(joTax, key);

                        if (isF1) {
                            isF1 = false;
                            sgst = Double.valueOf(taxVal);


                        } else {
                            cgst = Double.valueOf(taxVal);

                        }

                        Log.e("key", key);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }

            Log.e("stategst", String.valueOf(sgst));
            Log.e("stategst", String.valueOf(cgst));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Double totalamt = bill;
        Double stategst = (totalamt / 100.0f) * sgst;
        Double centralgst = (totalamt / 100.0f) * cgst;

        vat = (totalamt / 100.0f) * vat;

        //
        //stategst = Math.round(stategst * 100.0) / 100.0; // 0.224 - > 0.22
        //centralgst = Math.round(centralgst * 100.0) / 100.0; // 0.224 - > 0.22

        totalbill_ = bill + centralgst + stategst + vat;



        /*centralgstamount.setText(""+ String.format("%.2f", totalcentralgst));
        stategstamount.setText(""+ String.format("%.2f", totalstategst));
        billamount.setText(""+ String.format("%.2f", totalbill));
        grandtotalamount.setText(""+String.format("%.2f", grandtotal));
        String.format("%.2f", grandtotal);*/
        Log.e("final", String.valueOf(totalbill_));

        return totalbill_;
    } //  cal price - qty


    ShowGalleryCamera showGalleryCamera = new ShowGalleryCamera(this);
    int addSamplePos = 0;

    @Override
    public void onAddSample(int qty) {

        addSamplePos = qty;
        showGalleryCamera.showMultiPictureDialog();


    }

    ArrayList<ModelFile> al_selet = new ArrayList<>();
    ArrayList<String> al_Image = new ArrayList<>();

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to ak aris

        productlist = dbHelper.getAllcaretproduct();

        if (requestCode == 98 && resultCode == -1) {

            al_selet = new ArrayList<>();
            al_selet = productlist.get(addSamplePos).getAl_selet();


            ArrayList<ModelFile> al_temp_selet = new ArrayList<>();
            ArrayList<String> pathList = data.getExtras().getStringArrayList(PickImageActivity.KEY_DATA_RESULT);
            if (pathList != null && !pathList.isEmpty()) {
                for (int i = 0; i < pathList.size(); i++) {
                    al_temp_selet.add(new ModelFile(pathList.get(i), true, addSamplePos));
                }
            }


            al_temp_selet.addAll(al_selet);

            productlist.get(addSamplePos).setAl_selet(al_temp_selet);
            mAdapter.setitem(productlist, addSamplePos);
            //shopImgFileAdapter.assignItem(al_temp_selet);


        } else if (resultCode == RESULT_OK) {
            // Compressing

            showGalleryCamera.onGetResult(requestCode, resultCode, data, new ShowGalleryCamera.CallGetImg() {
                @Override
                public void onGetImg(String str) {
                    // get Compressed
                    al_selet = new ArrayList<>();
                    al_selet = productlist.get(addSamplePos).getAl_selet();
                    String path = str;
                    ArrayList<ModelFile> al_temp_selet = new ArrayList<>();
                    al_temp_selet.add(new ModelFile(path, true, addSamplePos));
                    al_temp_selet.addAll(al_selet);
                    productlist.get(addSamplePos).setAl_selet(al_temp_selet);
                    mAdapter.setitem(productlist, addSamplePos);
                    //shopImgFileAdapter.assignItem(al_temp_selet);
                }
            });
        }


        super.onActivityResult(requestCode, resultCode, data);

    }

    //////////////////////////  Upload Samples

    public interface UploadProgress {
        public void uploadIng();
    }

    int indexOfImg = 0;

    TextView tv_progress;
    RelativeLayout rl_upload;


    ArrayList<ModelFile> alUplodingQ = new ArrayList<>();
    int productQ = 0;

    public void uplaodQueue(final String usersession, final JSONArray cartdetails, final JSONObject userdetails, final String dt, final String address, final JSONObject joDelivery) {

        if (dbHelper.getcartcount() == 0)
            return;

        rl_upload.setVisibility(View.VISIBLE);

        productlist = dbHelper.getAllcaretproduct();

        if (productQ == productlist.size()) {
            rl_upload.setVisibility(View.GONE);

            dialog.setMessage("Creating your order ...");
            dialog.show();

            usersignin(usersession, cartdetails, userdetails, dt, address, joDelivery);

            //callUpdateBuisenssProfile();
        } else {

            indexOfImg = 0;
            al_selet = new ArrayList<>();
            al_selet = productlist.get(productQ).getAl_selet();

            uploadOfflineImg(new UploadProgress() {
                @Override
                public void uploadIng() {
                    productQ++;
                    uplaodQueue(usersession, cartdetails, userdetails, dt, address, joDelivery);
                }
            });

        }
    }


    public void uploadOfflineImg(final UploadProgress uploadProgress) {

        int p = indexOfImg + 1;
        tv_progress.setText("Uploading.. " + productlist.get(productQ).getDefault_name() + " " + p + "/" + al_selet.size());
        if (indexOfImg == al_selet.size()) {
            rl_upload.setVisibility(View.GONE);
            uploadProgress.uploadIng();
            //callUpdateBuisenssProfile();
        } else if (al_selet.get(indexOfImg).getImgpath().contains("http")) {

            indexOfImg++;
            uploadOfflineImg(uploadProgress);

        } else {

            MyRequestCall myRequestCall = new MyRequestCall();

            myRequestCall.uploadSamplesFromCartAwsS3(sessionManager.getcurrentu_nm(), productlist.get(productQ), this, indexOfImg, al_selet.get(indexOfImg).getImgpath(), new MyRequestCall.CallRequest() {
                @Override
                public void onGetResponse(String response) {

                    if (!response.isEmpty()) {
                        al_selet.get(indexOfImg).setImgpath(response);
                        al_selet.get(indexOfImg).setIsoffline(false);
                        productlist.get(productQ).setAl_selet(al_selet);
                        Apputil.updateImg(dbHelper, productlist.get(productQ));

                        indexOfImg++;
                        uploadOfflineImg(uploadProgress);
                        //path = response;

                    } else {
                        rl_upload.setVisibility(View.GONE);
                        Inad.alerter("Alert", "Uplaoding fail", OrderActivity.this);

                    }
                }
            });
        }
    }
}
