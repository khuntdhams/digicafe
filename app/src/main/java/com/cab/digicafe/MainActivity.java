package com.cab.digicafe;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.ajithvgiri.searchdialog.SearchableDialog;
import com.andremion.counterfab.CounterFab;
import com.cab.digicafe.Database.SqlLiteDbHelper;
import com.cab.digicafe.Fragments.TempGoneFragment;
import com.cab.digicafe.Helper.Inad;
import com.cab.digicafe.Helper.SessionManager;
import com.cab.digicafe.Helper.SharedPrefUserDetail;
import com.cab.digicafe.Model.SampleModel;
import com.cab.digicafe.Model.Userprofile;
import com.cab.digicafe.Rest.ApiClient;
import com.cab.digicafe.Rest.ApiInterface;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends BaseActivity {
    String sessionString;
    SessionManager sessionManager;
    CounterFab counterFab;
    SqlLiteDbHelper dbHelper;

    SearchView toolbarSearchView;
    SearchableDialog searchableDialog;
    Userprofile currentuser;

    String purpose = "Invoice";
    boolean isrequsetorissue = false, isfrommyagree = false;
    String to_bridge_id = "", currency_code = "", SupplierTitle = "";
    boolean ismycircle = false;
    boolean isBk = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_main);
        getLayoutInflater().inflate(R.layout.main_content, frameLayout);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getIntent().getExtras() != null) {
            if (getIntent().getExtras().containsKey("isBk")) {
                isBk = getIntent().getExtras().getBoolean("isBk", false);
            }

            if (getIntent().getExtras().containsKey("SupplierTitle")) {
                SupplierTitle = getIntent().getExtras().getString("SupplierTitle", "");
            }
        }

        to_bridge_id = SharedPrefUserDetail.getString(this, SharedPrefUserDetail.isfrommyaggregator, "");
        if (!to_bridge_id.equals("")) {
            getSupportActionBar().setTitle("My Aggregator");
            isfrommyagree = true;
            //getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        } else {  // My Circle

            //shoe circle icon in menu
            ismycircle = true;
            getSupportActionBar().setTitle("Selected Circle");
        }

        if (!SupplierTitle.isEmpty()) {
            getSupportActionBar().setTitle(SupplierTitle.toUpperCase() + "");

        }


        isrequsetorissue = getIntent().getBooleanExtra("isrequset", false);  // back

        if (getIntent() != null && getIntent().getExtras() != null && getIntent().getExtras().containsKey("purpose")) {
            Bundle extras = getIntent().getExtras();
            purpose = extras.getString("purpose", "Invoice");
        }

        dbHelper = new SqlLiteDbHelper(this);
        dbHelper.openDataBase();
        // getSupportActionBar().setTitle("Digicafe");
        sessionManager = new SessionManager(this);
        currentuser = new Gson().fromJson(sessionManager.getUserprofile(), Userprofile.class);
        counterFab = (CounterFab) findViewById(R.id.counter_fab);

        counterFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (counterFab.getCount() > 0) {
                    // if(to_bridge_id.equals("noback")) SharedPrefUserDetail.setString(MainActivity.this, SharedPrefUserDetail.isfrommyaggregator,"ysback");
                    Intent i = new Intent(MainActivity.this, OrderActivity.class);
                    i.putExtra("session", sessionString);
                    //i.putExtra("bridgeidval", currentuser.getBridge_id());
                    i.putExtra("frombridgeidval", sessionManager.getcurrentcaterer());
                    i.putExtra("aliasname", sessionManager.getcurrentcaterername());
                    i.putExtra("purpose", purpose);
                    startActivity(i);
                } else {
                    Apputil.showalert("Empty Cart!", MainActivity.this);
                }
            }
        });

        counterFab.setCount(dbHelper.getcartcount());
        /*  FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
         */


        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                sessionString = null;
            } else {
                sessionString = extras.getString("session");
            }
        } else {
            sessionString = (String) savedInstanceState.getSerializable("session");
        }

        //throw new NullPointerException();


    }

    @Override
    public void onResume() {
        super.onResume();
        //sessionManager.setAggregator_ID(sessionManager.getAggregator_ID());
        counterFab.setCount(dbHelper.getcartcount());
        usersigninbusinessstatus();


        // put your code here...

    }

    private void loadFragment(Fragment fragment) {
// create a FragmentManager
        FragmentManager fm = getSupportFragmentManager();
// create a FragmentTransaction to begin the transaction and replace the Fragment
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
// replace the FrameLayout with new Fragment
        fragmentTransaction.replace(R.id.fragments, fragment);
        fragmentTransaction.commit(); // save the changes
    }

    public void logoutuser() {
        Inad.exitalert(this);

       /* AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.AlertDialogCustom));
        builder
                .setMessage(getString(R.string.quit))
                .setTitle("")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        sessionManager.logoutUser(MainActivity.this);
                        MainActivity.this.finish();
                    }
                })
                .setNegativeButton("No", null)
                .show();*/
    }

    private ArrayList<SampleModel> createSampleData() {
        ArrayList<SampleModel> items = new ArrayList<>();
        items.add(new SampleModel("First item"));
        items.add(new SampleModel("Second item"));
        items.add(new SampleModel("Third item"));
        items.add(new SampleModel("The ultimate item"));
        items.add(new SampleModel("Last item"));
        items.add(new SampleModel("Lorem ipsum"));
        items.add(new SampleModel("Dolor sit"));
        items.add(new SampleModel("Some random word"));
        items.add(new SampleModel("guess who's back"));
        return items;
    }

    @Override
    public void onBackPressed() {

        if (isrequsetorissue || isBk) {
            super.onBackPressed();

        } else if (to_bridge_id.length() > 10) {
            SharedPrefUserDetail.setString(this, SharedPrefUserDetail.isfrommyaggregator, "noback");
            super.onBackPressed();
        } else {
            Inad.exitalert(this);
        }


                        /* AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.AlertDialogCustom));
                         builder
                                 .setMessage(getString(R.string.quit))
                                 .setTitle("")
                                 .setCancelable(false)
                                 .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                     public void onClick(DialogInterface dialog, int id) {

                                         finish();

                                         Intent intent = new Intent(Intent.ACTION_MAIN);
                                         intent.addCategory(Intent.CATEGORY_HOME);
                                         intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                         startActivity(intent);
                                     }
                                 })
                                 .setNegativeButton("No", null)

                                 .show();
*/
    }


    public void usersigninbusinessstatus() {


        ApiInterface apiService =
                ApiClient.getClient(MainActivity.this).create(ApiInterface.class);


        Call call;

        call = apiService.getagregatorProfile(sessionManager.getAggregator_ID());

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                int statusCode = response.code();
                //  String cookie = response.;
                //  Log.d("test", cookie+ " ");

                if (response.isSuccessful()) {

                    try {
                        String a = new Gson().toJson(response.body());
                        JSONObject Jsonresponse = new JSONObject(a);
                        JSONObject Jsonresponseinfo = Jsonresponse.getJSONObject("response");


                        Log.e("test", " trdds" + Jsonresponse.toString());
                        JSONArray aggregatorobj = Jsonresponseinfo.getJSONArray("data");
                        JSONObject aggregator = aggregatorobj.getJSONObject(0);
                        String aggregatorbridgeid = aggregator.getString("bridge_id");
                        String isactive = aggregator.getString("business_status");
                        //sessionManager.setAggregatorprofile(aggregatorbridgeid);
                        //isactive = "";

                        if (isactive.equalsIgnoreCase("open")) {
                            if (!TextUtils.isEmpty(sessionManager.getsession())) {
                                // Create an Intent that will start the Menu-Activity.
                                loadFragment(new CatererFragment().newInstance(sessionString, sessionString, purpose));

                            } else {
                                // Create an Intent that will start the Menu-Activity.


                            }

                        } else {
                            loadFragment(new TempGoneFragment().newInstance());
                            counterFab.setVisibility(View.GONE);
                        }

                    } catch (Exception e) {
                        Log.e("test", " trdds" + e.getLocalizedMessage());

                    }
                } else {
                    Log.e("test", " trdds1");

                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Toast.makeText(MainActivity.this, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        // Toast.makeText(CatelogActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }

                //  List<Movie> movies = response.body().getResults();
                //  recyclerView.setAdapter(new MoviesAdapter(movies, R.layout.list_item_movie, getApplicationContext()));
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // Log error here since request failed
                Log.e("error message", t.toString());
            }
        });
    }


}
