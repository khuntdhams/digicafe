package com.cab.digicafe;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.view.View;
import android.widget.Button;

import com.cab.digicafe.Database.SqlLiteDbHelper;
import com.cab.digicafe.Helper.SessionManager;
import com.craftman.cardform.Card;
import com.craftman.cardform.CardForm;
import com.craftman.cardform.OnPayBtnClickListner;

public class PaymentActivity extends AppCompatActivity {
    SessionManager sessionManager;
    SqlLiteDbHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        sessionManager = new SessionManager(this);
        CardForm cardForm = (CardForm) findViewById(R.id.card_form);
        Button paybtn = (Button) findViewById(R.id.paybtn);
        dbHelper = new SqlLiteDbHelper(this);
        paybtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                orderplacedsuccess();

            }
        });

        cardForm.setPayBtnClickListner(new OnPayBtnClickListner() {
            @Override
            public void onClick(Card card) {
                //Your code here!! use card.getXXX() for get any card property
                //for instance card.getName();

            }
        });
    }

    public void orderplacedsuccess() {

        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(PaymentActivity.this, R.style.AlertDialogCustom));
        builder
                .setMessage("Your order placed successfully")
                .setTitle("")
                .setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dbHelper.deleteallfromcart();
                        finish();
                        Intent i = new Intent(PaymentActivity.this, HistoryActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        i.putExtra("session", sessionManager.getsession());
                        startActivity(i);
                    }
                })


                .show();
    }
}
