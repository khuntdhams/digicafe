package com.cab.digicafe;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.cab.digicafe.Activities.MyAppBaseActivity;
import com.cab.digicafe.Activities.RequestApis.MyRequestCall;
import com.cab.digicafe.Activities.RequestApis.MyRequst;
import com.cab.digicafe.Adapter.taxGroup.TaxGroupListAdapter;
import com.cab.digicafe.Adapter.taxGroup.TaxGroupModel;
import com.cab.digicafe.Database.SettingDB;
import com.cab.digicafe.Database.SettingModel;
import com.cab.digicafe.Database.SqlLiteDbHelper;
import com.cab.digicafe.Dialogbox.RefundStatusDialog;
import com.cab.digicafe.Fragments.SummaryDetailFragmnet;
import com.cab.digicafe.Helper.AppPreference;
import com.cab.digicafe.Helper.Constants;
import com.cab.digicafe.Helper.Inad;
import com.cab.digicafe.Helper.SessionManager;
import com.cab.digicafe.Helper.SharedPrefUserDetail;
import com.cab.digicafe.Model.Catelog;
import com.cab.digicafe.Model.Chit;
import com.cab.digicafe.Model.Coupon;
import com.cab.digicafe.Model.Metal;
import com.cab.digicafe.Model.ModelFile;
import com.cab.digicafe.Model.ProductTotalDiscount;
import com.cab.digicafe.Model.ShareModel;
import com.cab.digicafe.Model.Userprofile;
import com.cab.digicafe.MyCustomClass.Compressor;
import com.cab.digicafe.MyCustomClass.JsonObjParse;
import com.cab.digicafe.MyCustomClass.MyCallBkCommon;
import com.cab.digicafe.MyCustomClass.PlayAnim;
import com.cab.digicafe.MyCustomClass.Utils;
import com.cab.digicafe.Rest.ApiClient;
import com.cab.digicafe.Rest.ApiInterface;
import com.cab.digicafe.Timeline.DateTimeUtils;
import com.cab.digicafe.serviceTypeClass.GetServiceTypeFromBusiUserProfile;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.razorpay.Checkout;
import com.razorpay.Payment;
import com.razorpay.PaymentResultListener;
import com.razorpay.RazorpayClient;
import com.razorpay.RazorpayException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.cab.digicafe.Activities.mrp.MrpFragmnet.vPos;

public class InvoicesummaryActivity extends MyAppBaseActivity implements PaymentResultListener {
    String chithashobj;
    JsonObject chitsummary;
    String hashidval, purpose = "";
    TextView caterername;
    TextView orderdate;
    TextView billamount, stategstamount, centralgstamount, grandtotalamount;
    SqlLiteDbHelper dbHelper;
    private AppPreference mAppPreference;
    Button orderconfirm;
    SessionManager sessionManager;

    TextView tv_date, tv_address, tv_mob;

    ScrollView sv;
    RadioButton rb_payu, rb_cod;
    SettingDB set_db;
    SettingModel sm = new SettingModel();

    @BindView(R.id.rl_upload)
    RelativeLayout rl_upload;

    @BindView(R.id.tv_progress)
    TextView tv_progress;
    @BindView(R.id.tvVat)
    TextView tvVat;
    @BindView(R.id.llVat)
    LinearLayout llVat;
    @BindView(R.id.llSgst)
    LinearLayout llSgst;
    @BindView(R.id.llCgst)
    LinearLayout llCgst;
    @BindView(R.id.tvReqDiscLbl)
    TextView tvReqDiscLbl;

    String currency = "";
    String field_json_data = "", service_type_of = ""; // field_json_data - act
    String field_json_data_info = "", business_type_info = ""; // field_json_data_info - info, business_type - info
    JSONObject joDelivery;

    @BindView(R.id.llMrpDeliveryDetail)
    LinearLayout llMrpDeliveryDetail;
    @BindView(R.id.llDelivery)
    LinearLayout llDelivery;
    @BindView(R.id.tvDate)
    TextView tvDate;
    @BindView(R.id.ivFromMap)
    ImageView ivFromMap;
    @BindView(R.id.tvFromName)
    TextView tvFromName;
    @BindView(R.id.tvFromContact)
    TextView tvFromContact;
    @BindView(R.id.tvFromAddress)
    TextView tvFromAddress;
    @BindView(R.id.tvToName)
    TextView tvToName;
    @BindView(R.id.tvToContact)
    TextView tvToContact;
    @BindView(R.id.tvToAddress)
    TextView tvToAddress;
    @BindView(R.id.totalhint)
    TextView totalhint;
    @BindView(R.id.tvReference)
    TextView tvReference;
    Double fromLatitude = 0.0, fromLongitude = 0.0;
    Double toLatitude = 0.0, toLongitude = 0.0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invoicesummary);
        ButterKnife.bind(this);
        sessionManager = new SessionManager(this);

        checkPromoFromAggre();

        currency = SharedPrefUserDetail.getString(this, SharedPrefUserDetail.chit_symbol_native, "") + " ";
        field_json_data = SharedPrefUserDetail.getString(InvoicesummaryActivity.this, SharedPrefUserDetail.field_json_data, "");
        service_type_of = JsonObjParse.getValueEmpty(field_json_data, "service_type_of");


        field_json_data_info = SharedPrefUserDetail.getString(InvoicesummaryActivity.this, SharedPrefUserDetail.field_json_data_info, "");
        business_type_info = SharedPrefUserDetail.getString(InvoicesummaryActivity.this, SharedPrefUserDetail.business_type, "");

        inits3();

        dbHelper = new SqlLiteDbHelper(this);
        set_db = new SettingDB(this);

        List<SettingModel> list_setting = set_db.GetItems();

        sm = list_setting.get(0);

        mAppPreference = new AppPreference();

        orderconfirm = (Button) findViewById(R.id.confirmorder);
        caterername = (TextView) findViewById(R.id.caterername);

        stategstamount = (TextView) findViewById(R.id.stategstamount);
        sv = (ScrollView) findViewById(R.id.sv);

        sv.pageScroll(View.FOCUS_UP);
        sv.smoothScrollTo(0, 0);
        centralgstamount = (TextView) findViewById(R.id.centralgstamount);
        grandtotalamount = (TextView) findViewById(R.id.grandtotalamount);

        tv_date = (TextView) findViewById(R.id.tv_date);
        tv_address = (TextView) findViewById(R.id.tv_address);
        tv_mob = (TextView) findViewById(R.id.tv_mob);


        billamount = (TextView) findViewById(R.id.totalamount);
        rb_cod = (RadioButton) findViewById(R.id.rb_cod);
        rb_payu = (RadioButton) findViewById(R.id.rb_payu);
        //billamount.setText(""+dbHelper.getcarttotal());


        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy hh:mm aaa");
        String date = df.format(c.getTime());


        //Double taxvalue =  dbHelper.getcarttotal();
        JSONObject invoicesummary = dbHelper.gettotaltaxamount();
        Log.e("Tax_json_data_output", "" + invoicesummary);
        Double taxvalue = null;
        try {

           /* Iterator<String> iter = invoicesummary.keys();

            while (iter.hasNext()) {

                String key = iter.next();
                    Object value = invoicesummary.get(key);
                    Double taxpercent = new Double(value.toString());
                    taxvalue = taxvalue + taxpercent;
                    if (key.equalsIgnoreCase("CGST"))
                    {
                        centralgstamount.setText(""+taxpercent);

                    }else if (key.equalsIgnoreCase("SGST"))
                    {
                        stategstamount.setText(""+taxpercent);

                    }

            }*/
            taxvalue = (dbHelper.getcarttotal() / 100.0f) * 9;
            //centralgstamount.setText(""+taxvalue);
            //stategstamount.setText(""+taxvalue);

        } catch (Exception e) {
            e.printStackTrace();
        }

        double g_total = taxvalue + taxvalue + dbHelper.getcarttotal();

        //grandtotalamount.setText(""+g_total);


        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                chithashobj = null;
                hashidval = null;

              /*  bridgeidval = null;
                frombridgeidval = null;*/
            } else {
                chithashobj = extras.getString("hashheadervalue");
                hashidval = extras.getString("hashid");
                purpose = extras.getString("purpose", "");
                purpose = SharedPrefUserDetail.getString(this, SharedPrefUserDetail.purpose, "");
                try {
                    joDelivery = new JSONObject(getIntent().getStringExtra("delivery_detail"));
                    Log.d("delivery_detail", joDelivery.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                String date_ = extras.getString("dt");
                String add = extras.getString("add");

                if (extras.containsKey("img")) {
                    isisue = true;
                    img = extras.getString("img");
                    if (!img.equalsIgnoreCase("")) {
                        Gson gson = new Gson();
                        TypeToken<ArrayList<ModelFile>> token = new TypeToken<ArrayList<ModelFile>>() {
                        };
                        al_selet = gson.fromJson(img, token.getType());
                    }


                }

                if (add.equalsIgnoreCase(""))
                    findViewById(R.id.rl_address).setVisibility(View.GONE);

                if (!add.equals("") && sm.getIsaddress().equals("1")) {
                    tv_address.setText(add);

                    findViewById(R.id.rl_address).setVisibility(View.VISIBLE);
                } else {
                    findViewById(R.id.rl_address).setVisibility(View.GONE);
                }

                if (sm.getIsdate().equals("1")) {
                    tv_date.setText(date_);
                    findViewById(R.id.rl_del_date).setVisibility(View.VISIBLE);
                } else {
                    findViewById(R.id.rl_del_date).setVisibility(View.GONE);
                }


                tv_mob.setText(sessionManager.getT_Phone());


               /* bridgeidval= extras.getString("bridgeidval");
                frombridgeidval= extras.getString("frombridgeidval");*/

            }
        } else {
            chithashobj = (String) savedInstanceState.getSerializable("hashheadervalue");
            hashidval = (String) savedInstanceState.getSerializable("hashid");
            /*bridgeidval= (String) savedInstanceState.getSerializable("bridgeidval");
            frombridgeidval= (String) savedInstanceState.getSerializable("frombridgeidval");*/
        }
        caterername.setText(sessionManager.getcurrentcatereraliasname());

        JsonParser parser = new JsonParser();
        chitsummary = parser.parse(chithashobj).getAsJsonObject();
        String CurrentString = chitsummary.get("chit_name").getAsString();
        String[] separated = CurrentString.split("/");
        //  if (separated.length >= 2 )

        //  ordernumber.setText(separated[0]);
        // orderotp.setText(separated[1]);
        Log.e("dasdsad", "clicked");
        Log.e("dasdsad", sessionManager.getsession());
        // updatechit(sessionManager.getsession(),chitsummary,hashidval);
        //  orderconfirm.setVisibility(View.GONE);
        orderconfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isisue && al_selet.size() > 0) {
                    str_img = new String[al_selet.size()];
                    new ImageCompress().execute();
                    //uploadimg();
                } else {
                    if (rb_cod.isChecked()) {
                        confirmorder("");
                    } else {
                        startRazorPayment();
                        //launchPayUMoneyFlow();
                    }
                }

            }
        });

       /* if (sm.getIspayumoney().equals("0")) {
            rb_payu.setVisibility(View.GONE);
            rb_cod.setChecked(true);
        }*/


        String chit_currency_code = SharedPrefUserDetail.getString(this, SharedPrefUserDetail.chit_currency_code, "") + "";


        if (!purpose.equalsIgnoreCase("Invoice")) {
            rb_payu.setVisibility(View.GONE);
            rb_cod.setChecked(true);
        }

        if (sm.getIspayumoney().equals("0")) {
            rb_payu.setVisibility(View.GONE);
            rb_cod.setChecked(true);
        }

        if (!chit_currency_code.equalsIgnoreCase("INR")) {
            rb_payu.setVisibility(View.GONE);
            rb_cod.setChecked(true);
        }

        switch (purpose) {

            case "on-line payment only":
                purpose = "invoice";
                rb_cod.setVisibility(View.GONE);
                rb_payu.setVisibility(View.VISIBLE);
                rb_payu.setChecked(true);
                break;

            case "on-line payment & cod":
                rb_cod.setVisibility(View.VISIBLE);
                rb_payu.setVisibility(View.VISIBLE);
                rb_payu.setChecked(true);
                break;

            case "cod only":
                purpose = "request";
                rb_cod.setVisibility(View.VISIBLE);
                rb_payu.setVisibility(View.GONE);
                rb_cod.setChecked(true);
                break;

        }

        if (purpose.equalsIgnoreCase(getString(R.string.delivery))) {
            deliveryDetail();
            llDelivery.setVisibility(View.GONE);
            llMrpDeliveryDetail.setVisibility(View.VISIBLE);
            totalhint.setText("Delivery Charge");
        } else {
            llDelivery.setVisibility(View.VISIBLE);
            llMrpDeliveryDetail.setVisibility(View.GONE);
            totalhint.setText("Product Total");
        }

        billamount();

        //sv.pageScroll(View.FOCUS_UP);

      /*  sv.post(new Runnable() {
            public void run() {
                sv.scrollTo(0, 0);
            }
        });*/
        ivFromMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    String uri = "http://maps.google.com/maps?saddr=" + fromLatitude + "," + fromLongitude + "&daddr=" + toLatitude + "," + toLongitude;
                    Log.d("route", uri);
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                    intent.setPackage("com.google.android.apps.maps");
                    startActivity(intent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
// Result Code is -1 send from Payumoney activity


    }


    public void updatechit(String usersession, JsonObject chitdetails, String hashid) {

        // Gson gson = new Gson();
        // gson.toJson(chitdetails.toString());                   // prints 1
        ApiInterface apiService =
                ApiClient.getClient(InvoicesummaryActivity.this).create(ApiInterface.class);
        Log.e("beforeapical", chitdetails.toString());
        // String json = chitdetails.toString();
        String json = new Gson().toJson(chitdetails);
        String currency_code = SharedPrefUserDetail.getString(InvoicesummaryActivity.this, SharedPrefUserDetail.chit_currency_code, "");
        String currency_code_id = SharedPrefUserDetail.getString(InvoicesummaryActivity.this, SharedPrefUserDetail.chit_currency_code_id, "");

        Call call = apiService.updatechit(usersession, chitdetails, sessionManager.getAggregatorprofile(), hashid, "d97be841a6d6cbb66bc3ae165b31b85f", currency_code, currency_code_id, "2");
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {

                if (response.isSuccessful()) {
                    String res = new Gson().toJson(response.body());
                    try {
                        JSONObject jsonObject = new JSONObject(res);
                        String responseStr = jsonObject.getString("response");
                        String data = JsonObjParse.getValueEmpty(responseStr, "data");
                        String first_chit_header_data_arr = JsonObjParse.getValueEmpty(data, "first_chit_header_data_arr");
                        Chit chit = new Gson().fromJson(first_chit_header_data_arr, Chit.class);
                        orderplacedsuccess(chit);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {

                    if (dialog != null) {
                        dialog.dismiss();
                    }

                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Log.e("test", " trdds1" + jObjErrorresponse.toString());
                        String msg = jObjErrorresponse.getString("errormsg");
                        if (msg.equalsIgnoreCase("To is empty")) {
                            msg = "You cannot order to yourselves.";
                        }
                        Toast.makeText(InvoicesummaryActivity.this, msg, Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(InvoicesummaryActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // Log error here since request failed
                if (dialog != null) {
                    dialog.dismiss();
                }
                Log.e("error message", t.toString());
            }
        });
    }

    public void orderplacedsuccess(final Chit chitOdr) {

        addTaskPreferenceForMrpFromAllTask(chitOdr, new MyCallBkCommon() {
            @Override
            public void myCallBkOnly() {

                ShareModel.getI().isFromAllTask = false;

                if (dialog != null) {
                    dialog.dismiss();
                }

                sessionManager.setcurrentcaterer("");
                sessionManager.setcurrentcaterername("");
                sessionManager.setCartproduct("");
                sessionManager.setCartcount(0);
                sessionManager.setRemark("");
                sessionManager.setDate("");
                sessionManager.setAddress("");
                dbHelper.deleteallfromcart();

                sessionManager.setAggregator_ID(getString(R.string.Aggregator));
                if (iscod) {

                    SharedPrefUserDetail.setString(InvoicesummaryActivity.this, SharedPrefUserDetail.isfrommyaggregator, "");
                    finish();

                    Class<?> cls = null;
                    if (Constants.chit_id_from_mrp == 0) {
                        cls = HistoryActivity.class;

                        Intent i = new Intent(InvoicesummaryActivity.this, cls);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        i.putExtra("session", sessionManager.getsession());
                        startActivity(i);

                    } else {
                        onItemClick();
                    }
                } else {
                    capturepaymnet();
                }

            }
        });


    }

    Double totalWithRoundOff = 0.0;
    Double grandtotal = 0.0;
    Double totalbill = 0.0;
    Double totalReqDisc = 0.0;
    Double ReqDisc = 0.0;
    Double totalProductDisc = 0.0;
    Double totalstategst = 0.0;
    Double totalVat = 0.0;
    Double totalcentralgst = 0.0;

    Double totalDeduction = 0.0;
    Double totalAddition = 0.0;


    public int getExtraPropertyInfo(String extraInfo) throws JSONException {
        int totalInfoVal = 0;
        if (extraInfo != null && !extraInfo.isEmpty()) {

            JSONArray jaExtraInfo = new JSONArray(extraInfo);

            for (int i = 0; i < jaExtraInfo.length(); i++) {
                JSONObject jsonObject = jaExtraInfo.getJSONObject(i);

                Iterator keys = jsonObject.keys();
                while (keys.hasNext()) {
                    try {
                        String key = (String) keys.next();
                        String info = JsonObjParse.getValueFromJsonObj(jsonObject, key);

                        if (!info.isEmpty()) {
                            int infoVal = Integer.parseInt(info);
                            totalInfoVal = totalInfoVal + infoVal;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }
        }

        return totalInfoVal;
    }


    public void billamount() {

        alTax = new ArrayList<>();

        ArrayList<Catelog> productlist = new ArrayList<>();
        productlist = dbHelper.getAllcaretproduct();
        for (int i = 0; i < productlist.size(); i++) {
            int qty = productlist.get(i).getCartcount();
            Double bill = Double.valueOf(productlist.get(i).getdiscountmrp());  // mrp

            String doctor_template_type = JsonObjParse.getValueEmpty(productlist.get(i).getInternal_json_data(), "doctor_template_type");

            // For Calc Disc of property
            Double mrp = Double.valueOf(productlist.get(i).getMrp());  // mrp
            String property = JsonObjParse.getValueEmpty(productlist.get(i).getInternal_json_data(), "property");
            if (!property.isEmpty()) { // property always 1
                String sqFt = JsonObjParse.getValueEmpty(property, "proprty_sqft");
                if (!sqFt.isEmpty()) {
                    int sqFtVal = Integer.parseInt(sqFt);
                    mrp = mrp * sqFtVal;
                }
                try {
                    String ExtraInfoPrice = JsonObjParse.getValueEmpty(property, "ExtraInfoPrice");
                    mrp = mrp + getExtraPropertyInfo(ExtraInfoPrice);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            Double ded_price_val = 0.0, addi_price_val = 0.0;
            Double ded_perc_val = 0.0, addi_perc_val = 0.0;
            Double ded_mrp = 0.0, addi_mrp = 0.0;

            Catelog catelog = productlist.get(i);

            String jewel = JsonObjParse.getValueEmpty(productlist.get(i).getInternal_json_data(), "jewel");
            if (!jewel.isEmpty()) {


                String metalPosId = JsonObjParse.getValueEmpty(jewel, "metalPosId");
                int posId = 0;
                if (!metalPosId.isEmpty()) posId = Integer.parseInt(metalPosId);

                try {
                    if (ShareModel.getI().metal.alMetal.size() > posId) {
                        Metal metal = ShareModel.getI().metal.alMetal.get(posId);
                        if (!metal.metalPrice.isEmpty()) {
                            mrp = Double.valueOf(metal.metalPrice);
                        }
                    } else {
                        Log.d("okhttp jewel", catelog.getName() + "");
                    }

                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }

                String weight = JsonObjParse.getValueEmpty(jewel, "weight");

                if (!weight.isEmpty()) {
                    float wt = Float.parseFloat(weight);
                    //dis_mrp = (dis_mrp * wt)/10; // 10 gram
                    mrp = (mrp * wt) / 1; // 1 unit
                }
                try {
                    String DeductionJewelPrice = JsonObjParse.getValueEmpty(jewel, "DeductionJewelPrice");
                    ded_price_val = catelog.getAdditionalDeductinalInfo(DeductionJewelPrice, mrp);
                    ded_mrp = Double.valueOf(catelog.getValues(mrp, ded_perc_val, ded_price_val));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                try {
                    String AdditionalJewelPrice = JsonObjParse.getValueEmpty(jewel, "AdditionalJewelPrice");
                    addi_price_val = catelog.getAdditionalDeductinalInfo(AdditionalJewelPrice, mrp);
                    addi_mrp = Double.valueOf(catelog.getValues(mrp, addi_perc_val, addi_price_val));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                bill = bill - ded_mrp + addi_mrp;
                mrp = mrp - ded_mrp + addi_mrp;


            }

            if (doctor_template_type.equalsIgnoreCase("RPD") || doctor_template_type.equalsIgnoreCase("Precision Attachment")) {
                Double Amrp = 0.0;
                Log.d("additional", JsonObjParse.getValueEmpty(productlist.get(i).getInternal_json_data(), "additionalPrice_rpd") + "");
                if (!JsonObjParse.getValueEmpty(productlist.get(i).getInternal_json_data(), "additionalPrice_rpd").isEmpty()) {
                    Amrp = Double.valueOf(JsonObjParse.getValueEmpty(productlist.get(i).getInternal_json_data(), "additionalPrice_rpd"));
                    bill = bill + Amrp * (qty - 1);
                    mrp = mrp + Amrp * (qty - 1);
                }
            } else {
                bill = bill * qty;
                mrp = mrp * qty;
            }
            // calc disc of each product
            mrp = mrp - bill;
            totalProductDisc = totalProductDisc + mrp;

            totalbill = totalbill + bill;
            Log.e("totalbil", productlist.get(i).getdiscountmrp());
            Log.e("qty", String.valueOf(qty));

            Double sgst = 0.0;
            Double cgst = 0.0;
            Double vat = 0.0;

            try {
                String tax = productlist.get(i).getTax_json_data();
                if (tax != null) {
                   /* JSONObject obj = new JSONObject(tax);
                    if (obj.has("VAT")) {
                        vat = Double.valueOf(obj.getString("VAT"));
                    } else {
                        sgst = Double.valueOf(obj.getString("SGST"));
                        cgst = Double.valueOf(obj.getString("CGST"));
                    }*/

                    boolean isF1 = true;

                    JSONObject joTax = new JSONObject(tax);


                    Iterator keys = joTax.keys();

                    while (keys.hasNext()) {

                        try {
                            String key = (String) keys.next();

                            String taxVal = JsonObjParse.getValueFromJsonObj(joTax, key);


                            if (isF1) {

                                isF1 = false;
                                sgst = Double.valueOf(taxVal);

                                Double tax1 = (bill / 100.0f) * sgst;
                                boolean isExist = false;
                                for (int k = 0; k < alTax.size(); k++) {
                                    String tmpTaxVal = alTax.get(k).taxValPercentage;
                                    String tmpKey = alTax.get(k).taxKey;
                                    if (tmpKey.equals(key) && taxVal.equals(tmpTaxVal)) {
                                        isExist = true;
                                        alTax.get(k).taxVal = alTax.get(k).taxVal + tax1;
                                        break;
                                    }
                                }
                                if (!isExist && tax1 > 0) {
                                    alTax.add(new TaxGroupModel(tax1, key, taxVal));
                                }


                            } else {
                                cgst = Double.valueOf(taxVal);

                                Double tax2 = (bill / 100.0f) * cgst;
                                boolean isExist = false;
                                for (int k = 0; k < alTax.size(); k++) {
                                    String tmpTaxVal = alTax.get(k).taxValPercentage;
                                    String tmpKey = alTax.get(k).taxKey;
                                    if (tmpKey.equals(key) && taxVal.equals(tmpTaxVal)) {
                                        isExist = true;
                                        alTax.get(k).taxVal = alTax.get(k).taxVal + tax2;
                                        break;
                                    }
                                }
                                if (!isExist && tax2 > 0) {
                                    alTax.add(new TaxGroupModel(tax2, key, taxVal));
                                }

                            }

                            Log.e("key", key);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                }

                Log.e("stategst", String.valueOf(sgst));
                Log.e("stategst", String.valueOf(cgst));
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Double totalamt = bill;
            Double stategst = (totalamt / 100.0f) * sgst;
            Log.e("stategst", String.valueOf(stategst));
            totalstategst = totalstategst + stategst;

            Double centralgst = (totalamt / 100.0f) * cgst;
            Log.e("stategst", String.valueOf(centralgst));
            totalcentralgst = totalcentralgst + centralgst;

            Double valueAt = (totalamt / 100.0f) * vat;
            Log.e("valueAt", String.valueOf(valueAt));
            totalVat = totalVat + valueAt;
        }


        checkDiscount();


        grandtotal = totalbill + totalcentralgst + totalstategst + totalVat - totalReqDisc;

        //centralgstamount.setText(currency + String.format("%.2f", totalcentralgst));
        centralgstamount.setText(currency + Inad.getCurrencyDecimal(totalcentralgst, this));
        //stategstamount.setText(currency + String.format("%.2f", totalstategst));
        stategstamount.setText(currency + Inad.getCurrencyDecimal(totalstategst, this));
        tvVat.setText(currency + Inad.getCurrencyDecimal(totalVat, this));
        //billamount.setText(currency + String.format("%.2f", totalbill));
        billamount.setText(currency + Inad.getCurrencyDecimal(totalbill, this));
        //grandtotalamount.setText(currency + String.format("%.2f", grandtotal));
        grandtotalamount.setText(currency + Inad.getCurrencyDecimal(grandtotal, this));


        llCgst.setVisibility(View.GONE);
        llSgst.setVisibility(View.GONE);
        llVat.setVisibility(View.GONE);

        if (totalVat.doubleValue() == 0.0) {
            llVat.setVisibility(View.GONE);
        }
        if (totalcentralgst.doubleValue() == 0.0) llCgst.setVisibility(View.GONE);
        if (totalstategst.doubleValue() == 0.0) llSgst.setVisibility(View.GONE);

        setCharges();
       /* String.format("%.2f", grandtotal);
        Log.e("totalstategst", String.valueOf(totalstategst));
        Log.e("final", String.valueOf(totalbill));*/

        try {
            Thread.sleep(50);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    Userprofile loggedin;

    public void confirmorder(String payuResponse) {
        dialog = new ProgressDialog(InvoicesummaryActivity.this);
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        dialog.show();
        Userprofile loggedin = null;

        try {
            JSONObject userdetails = new JSONObject(sessionManager.getUserprofile());
            loggedin = new Gson().fromJson(userdetails.toString(), Userprofile.class);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            JsonObject jObj = new JsonObject();
            jObj.addProperty("email", loggedin.getEmail_id());
            jObj.addProperty("contact_number", sessionManager.getT_Phone());
            jObj.addProperty("location", sessionManager.getAddress());
            jObj.addProperty("act", sessionManager.getcurrentcaterername());

            if (service_type_of.equalsIgnoreCase(getString(R.string.property))) {
                String chitnm = ShareModel.getI().chit_name;
                //chitnm = chitnm.replaceAll(" ","-");
                jObj.addProperty("chit_name", chitnm);

            } else {
                jObj.addProperty("chit_name", ""); //o
            }


            jObj.addProperty("chitHashId", hashidval);
            jObj.addProperty("latitude", "");
            jObj.addProperty("longtitude", "");

            String expected_delivery_time = DateTimeUtils.parseDateTime(sessionManager.getDate(), "dd MMM yyyy hh:mm aaa", "yyyy/MM/dd kk:mm");

            jObj.addProperty("expected_delivery_time", expected_delivery_time);
            jObj.addProperty("for_non_bridge_name", "");
            jObj.addProperty("subject", sessionManager.getcurrentcatereraliasname());

            String currency_code = SharedPrefUserDetail.getString(InvoicesummaryActivity.this, SharedPrefUserDetail.chit_currency_code, "");
            String symbol_native = SharedPrefUserDetail.getString(InvoicesummaryActivity.this, SharedPrefUserDetail.chit_symbol_native, "");
            String currency_code_id = SharedPrefUserDetail.getString(InvoicesummaryActivity.this, SharedPrefUserDetail.chit_currency_code_id, "");
            String symbol = SharedPrefUserDetail.getString(InvoicesummaryActivity.this, SharedPrefUserDetail.chit_symbol, "");

            jObj.addProperty("currency_code", currency_code);
            jObj.addProperty("symbol_native", symbol_native);
            jObj.addProperty("currency_code_id", currency_code_id);
            jObj.addProperty("symbol", symbol);


            // The order is, first we hit blrmcc (info ) then we hit blrmc1
            //In blrmcc , we check info flag
            //In blrmc1 , we check delivery flag


            boolean iss = SharedPrefUserDetail.getBoolean(InvoicesummaryActivity.this, SharedPrefUserDetail.isfromsupplier, false);
            if (iss) { // if its business
                jObj.addProperty("info", "");

                StringBuilder stringBuilder = new StringBuilder();

                if (field_json_data != null) {

                    String delivery_partner_info = JsonObjParse.getValueEmpty(field_json_data, "delivery_partner_info");
                    if (!delivery_partner_info.isEmpty()) {
                        stringBuilder.append(delivery_partner_info + " , ");
                        jObj.addProperty("info", stringBuilder.toString());
                    }
                }

                Log.d("okhttp info iss", stringBuilder.toString());


            } else { // if its aggre / circle
                // blrmc1(delivery flag of this) under blrmcc(info flag of this)
                String info = SharedPrefUserDetail.getString(InvoicesummaryActivity.this, SharedPrefUserDetail.infoMainShop, "");
                //String act = sessionManager.getAggregator_ID();
                String act = sessionManager.getcurrentcaterername();
                //String infoPass = info + " , " + act + " , ";
                StringBuilder stringBuilder = new StringBuilder(); // old is correct
                //if(!info.isEmpty())stringBuilder.append(info+" , ");
                //if(!act.isEmpty())stringBuilder.append(act+" , ");
                if (field_json_data_info != null) {
                    String pass_info_circle = JsonObjParse.getValueEmpty(field_json_data_info, "pass_info_circle");
                    String pass_info_aggregator = JsonObjParse.getValueEmpty(field_json_data_info, "pass_info_aggregator");
                    if (!pass_info_circle.equalsIgnoreCase("no") && business_type_info.equalsIgnoreCase("circle")) {
                        if (!info.isEmpty()) stringBuilder.append(info + " , ");
                    }
                    if (!pass_info_aggregator.equalsIgnoreCase("no") && business_type_info.equalsIgnoreCase("aggregator")) {
                        if (!info.isEmpty()) stringBuilder.append(info + " , ");
                    }
                }

                if (!act.isEmpty()) stringBuilder.append(act + " , ");

                if (field_json_data != null) {

                    String delivery_partner_info = JsonObjParse.getValueEmpty(field_json_data, "delivery_partner_info");
                    if (!delivery_partner_info.isEmpty()) {
                        stringBuilder.append(delivery_partner_info + " , ");
                    }
                }

                Log.d("okhttp info", stringBuilder.toString());
                jObj.addProperty("info", stringBuilder.toString());
                //jObj.addProperty("info", sessionManager.getAggregator_ID());
            }


            jObj.addProperty("header_note", sessionManager.getRemark());

            service_type_of = JsonObjParse.getValueEmpty(field_json_data, "service_type_of");
            if (service_type_of.equalsIgnoreCase(getString(R.string.property))) {
                purpose = "property";
                jObj.addProperty("purpose", "property");
                // jObj.addProperty("purpose", "request");
            } else if (service_type_of.equalsIgnoreCase(getString(R.string.jewel))) {
                purpose = "Material";
                jObj.addProperty("purpose", "Material");
                // jObj.addProperty("purpose", "request");
            } else if (purpose.equalsIgnoreCase("on-line payment & cod")) {
                if (rb_payu.isChecked()) {
                    purpose = "invoice";
                    jObj.addProperty("purpose", "invoice");
                } else {
                    purpose = "request";
                    jObj.addProperty("purpose", "request");
                }
            } else {
                jObj.addProperty("purpose", purpose);
            }

            jObj.addProperty("chit_item_count", dbHelper.getcartcount());
            jObj.addProperty("total_chit_item_value", String.valueOf(grandtotal));
            JsonObject jo_footer = new JsonObject();
            jo_footer.addProperty("app_name", getString(R.string.app_name));
            jo_footer.addProperty("SGST", String.valueOf(totalstategst));
            jo_footer.addProperty("CGST", String.valueOf(totalcentralgst));
            jo_footer.addProperty("VAT", String.valueOf(totalVat));
            jo_footer.addProperty("TOTALBILL", String.valueOf(totalbill));
            jo_footer.addProperty("TOTLREQDISC", String.valueOf(totalReqDisc));
            jo_footer.addProperty("REQ_DISC", String.valueOf(ReqDisc));
            jo_footer.addProperty("TOTLPRODISC", String.valueOf(totalProductDisc));

            jo_footer.addProperty("COUPONDISC", String.valueOf(couponDiscount));
            jo_footer.addProperty("COUPONDISCLBL", tvCouponValOrModulo.getText().toString().trim());
            jo_footer.addProperty("GRANDTOTAL", String.valueOf(grandtotal));
            jo_footer.addProperty("businessId", String.valueOf(grandtotal));
            if (service_type_of.equalsIgnoreCase(getString(R.string.property))) {
                jo_footer.addProperty("property_name", ShareModel.getI().parti_nm);
            }

            if (totalWithRoundOff > 0) {
                jObj.addProperty("total_chit_item_value", String.valueOf(totalWithRoundOff));
                jo_footer.addProperty("totalWithRoundOff", String.valueOf(totalWithRoundOff));
            }

            jo_footer.addProperty("NAME", loggedin.getFirstname());
            boolean isMrp = SharedPrefUserDetail.getBoolean(this, SharedPrefUserDetail.isMrp, false);
            if (isMrp) { // from all task only pass flage

                Chit chit = SummaryDetailFragmnet.item;
                //jaMrp = new JSONArray();
                joFrom = new JSONObject();
                joTo = new JSONObject();

                // Ex, from henncy(task) customer athi, supplier navnit
                // Henncy buying something from Nitin for Athi

                try {

                   /* joFrom.put("from_bridge_id", chit.getFrom_bridge_id());
                    String ConsumerInfo = JsonObjParse.getValueEmpty(chit.getFooter_note(), "ConsumerInfo");
                    if (!ConsumerInfo.isEmpty()) {
                        JSONArray jaConsumerInfo = new JSONArray(ConsumerInfo);
                        if (jaConsumerInfo.length() > 0) {
                            JSONObject joConsInfo = jaConsumerInfo.getJSONObject(0);

                            joFrom.put("ConsumerInfo", joConsInfo.toString());
                        }
                    }

                    joFrom.put("ref_id", chit.getRef_id());
                    joFrom.put("case_id", chit.getCase_id());
                    joFrom.put("purpose", chit.getPurpose());
                    joFrom.put("chit_hash_id", chit.getChit_hash_id());
                    joFrom.put("chit_item_count", chit.getChit_item_count());
                    joFrom.put("transaction_status", chit.getTransaction_status());
                    joFrom.put("currency_code", chit.getCurrency_code());
                    joFrom.put("currency_code_id", chit.getCurrency_code_id());
                    joFrom.put("from_chit_id", chit.getChit_id());*/

                    try {
                        JSONObject jo_summary = new JSONObject(chit.getFooter_note());
                        joFrom.put("DATE", jo_summary.getString("DATE"));
                        joFrom.put("EDIT_DATE", jo_summary.getString("DATE"));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    joFrom.put("symbol_native", chit.getSymbol_native());
                    joFrom.put("total_chit_item_value", chit.getTotal_chit_item_value());
                    joFrom.put("edit_total_chit_item_value", chit.getTotal_chit_item_value());
                    joFrom.put("subject", chit.getSubject());
                    joFrom.put("location", chit.getLocation());


                    /****************************************************************************/

                   /* joTo.put("to_bridge_id", chit.getTo_bridge_id());
                    joTo.put("chit_item_count", dbHelper.getcartcount());
                    joTo.put("chit_hash_id", hashidval);
                    joTo.put("currency_code", currency_code);
                    joTo.put("currency_code_id", currency_code_id);
                    joTo.put("to_chit_id", chit.getChit_id());

                    JSONObject joBusiness = new JSONObject();
                    String sms_business = JsonObjParse.getValueEmpty(field_json_data, "sms_status");
                    joBusiness.put("sms", sms_business);
                    String mobile_business = JsonObjParse.getValueEmpty(field_json_data, "mobile_business");
                    joBusiness.put("mobile", mobile_business);
                    joBusiness.put("customer_nm", sessionManager.getcurrentcatereraliasname());
                    joTo.put("BusinessInfo", joBusiness.toString());*/

                    joTo.put("DATE", sessionManager.getDate());
                    joTo.put("EDIT_DATE", sessionManager.getDate());
                    joTo.put("symbol_native", currency.trim());
                    joTo.put("subject", sessionManager.getcurrentcatereraliasname());
                    joTo.put("location", sessionManager.getAddress());
                    joTo.put("sort", Constants.SORT_MAT);


                    if (totalWithRoundOff > 0) {
                        joTo.put("total_chit_item_value", totalWithRoundOff + "");
                        joTo.put("edit_total_chit_item_value", totalWithRoundOff);
                    } else {
                        joTo.put("total_chit_item_value", grandtotal + "");
                        joTo.put("edit_total_chit_item_value", grandtotal);
                    }


                    /****************************************************************************/


                    //jaMrp.put(joMrp);


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                //jo_footer.addProperty("MRP_TAB", jaMrp.toString());
                jo_footer.addProperty("MRP_TAB", "Ys");
            }


            String taxArray = new Gson().toJson(alTax);
            jo_footer.addProperty("taxArray", taxArray);

            if (al_uploadedimg.size() > 0) {
                String imgjson = new Gson().toJson(al_uploadedimg);
                jo_footer.addProperty("img", imgjson);
            } else {

                jo_footer.addProperty("img", "");

            }

            if (rb_cod.isChecked()) {
                iscod = true;
                jo_footer.addProperty("PAYMENT_MODE", "COD");
            } else {
                iscod = false;
                jo_footer.addProperty("PAYMENT_MODE", "RAZORPAY");
            }

            // Consumer Array

            JsonArray jaConsumerContact = new JsonArray();
            JsonObject joConsumer = new JsonObject();
            String sms_customer = JsonObjParse.getValueEmpty(field_json_data, "sms_customer");
            joConsumer.addProperty("sms", sms_customer);
            joConsumer.addProperty("customer_nm", loggedin.getFirstname());
            joConsumer.addProperty("mobile", sessionManager.getT_Phone());
            jaConsumerContact.add(joConsumer);
            jo_footer.add("ConsumerInfo", jaConsumerContact);

            // Business Array

            JsonArray jaBusinessContact = new JsonArray();
            JsonObject joBusiness = new JsonObject();
            String sms_business = JsonObjParse.getValueEmpty(field_json_data, "sms_status");
            joBusiness.addProperty("sms", sms_business);
            String mobile_business = JsonObjParse.getValueEmpty(field_json_data, "mobile_business");
            joBusiness.addProperty("mobile", mobile_business);
            joBusiness.addProperty("customer_nm", sessionManager.getcurrentcatereraliasname());
            jaBusinessContact.add(joBusiness);

            jo_footer.add("BusinessInfo", jaBusinessContact);

            // Delivery Array

            JsonArray jaDeliveryContact = new JsonArray();
            JsonObject joDelivery = new JsonObject();

            String mobile_delivery = JsonObjParse.getValueEmpty(field_json_data, "mobile_delivery");
            joDelivery.addProperty("mobile", mobile_delivery);
            String companynm = JsonObjParse.getValueEmpty(field_json_data, "customer_nm_delivery");
            joDelivery.addProperty("customer_nm", companynm + "");
            //String pick_up_only = JsonObjParse.getValueEmpty(field_json_data, "pick_up_only");
            if (cbIsPickUpOnly.isChecked()) {
                joDelivery.addProperty("sms", "no");
                joDelivery.addProperty("pick_up_only", "Yes");
            } else {
                String sms_delivery = JsonObjParse.getValueEmpty(field_json_data, "sms_delivery");
                joDelivery.addProperty("sms", sms_delivery);
                joDelivery.addProperty("pick_up_only", "No");
            }
            jaDeliveryContact.add(joDelivery);
            jo_footer.add("DeliveryInfo", jaDeliveryContact);

            jo_footer.addProperty("DATE", sessionManager.getDate());
            jo_footer.addProperty("ADDRESS", sessionManager.getAddress());

            if (service_type_of.equalsIgnoreCase(getString(R.string.property))) {
                //  jo_footer.addProperty("Cust_Nm", sessionManager.getCustNmForProperty());
            }
            jo_footer.addProperty("Cust_Nm", sessionManager.getCustNmForProperty());

            jo_footer.addProperty("payuResponse", payuResponse);
            jo_footer.addProperty("chithashid", hashidval);


            jo_footer.addProperty("field_json_data", field_json_data);


            jObj.addProperty("footer_note", jo_footer.toString());
            Log.i("okhttp", jo_footer.toString());
            updatechit(sessionManager.getsession(), jObj, hashidval);

        } catch (JsonParseException e) {
            e.printStackTrace();
        }
    }

    private ProgressDialog dialog;


    public void startRazorPayment() {
        /*
          You need to pass current activity in order to let Razorpay create CheckoutActivity
         */
        final Activity activity = this;

        final Checkout co = new Checkout();

        try {
            Userprofile loggedin = null;

            try {
                JSONObject userdetails = new JSONObject(sessionManager.getUserprofile());
                loggedin = new Gson().fromJson(userdetails.toString(), Userprofile.class);

            } catch (JSONException e) {
                e.printStackTrace();
            }


            JSONObject options = new JSONObject();
            options.put("name", sessionManager.getcurrentu_nm());


            JSONArray ja_notes = new JSONArray();

            String from = "From : " + sessionManager.getUserWithAggregator(); // Customer id
            String chithashid = "Chit hash id : " + hashidval; // Customer id
            String to = "To : " + sessionManager.getcurrentcaterername(); //
            String info = "Info : " + sessionManager.getAggregator_ID();

            String url_ = "Url : ";

            if (getString(R.string.BASEURL).equalsIgnoreCase("http://Dev.chitandbridge.com/prerelease/")) {
                url_ = url_ + "DEV";
            } else {
                url_ = url_ + "PROD";
            }
            ja_notes.put(chithashid);
            ja_notes.put(from);
            ja_notes.put(to);
            ja_notes.put(info);
            ja_notes.put(url_);

            options.put("notes", ja_notes);

            options.put("description", sessionManager.getcurrentcatereraliasname());


            //options.put("notes", {});
            //You can omit the image option to fetch the image from dashboard

            try {
                String company_image = sessionManager.getCompImg();

                String endurl = "assets/uploadimages/";

                String url = getString(R.string.BASEURL) + endurl + company_image;

                if (company_image.equalsIgnoreCase("")) {

                } else {

                    //options.put("image", url);

                }
            } catch (Exception e) {

            }
            options.put("image", "https://rzp-mobile.s3.amazonaws.com/images/rzp.png");

            String currency_code = SharedPrefUserDetail.getString(InvoicesummaryActivity.this, SharedPrefUserDetail.currency_code, "") + " ";

            options.put("currency", "INR");

            double amount = 0;
            try {
                //amount = Double.parseDouble("1");
                grandtotal = Math.round(grandtotal * 100.0) / 100.0;  // for 2 decimal valuee
                amount = grandtotal * 100;  // paisa
                amount = Math.round(amount);

                if (totalWithRoundOff > 0) {
                    totalWithRoundOff = Math.round(totalWithRoundOff * 100.0) / 100.0;  // for 2 decimal valuee
                    amount = totalWithRoundOff * 100;  // paisa
                    amount = Math.round(amount);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }


            options.put("amount", amount);

            JSONObject preFill = new JSONObject();
            preFill.put("email", loggedin.getEmail_id());
            preFill.put("contact", sessionManager.getT_Phone());

            options.put("prefill", preFill);

            co.open(activity, options);
        } catch (Exception e) {
            Toast.makeText(activity, "Error in payment: " + e.getMessage(), Toast.LENGTH_SHORT)
                    .show();
            e.printStackTrace();
        }
    }

    @Override
    public void onPaymentSuccess(String s) {
        try {

            confirmorder(s);

            pay_id = s;

            Toast.makeText(this, "Payment Successful", Toast.LENGTH_SHORT).show();

        } catch (Exception e) {
            Log.e("Razor", "Exception in onPaymentSuccess", e);
        }
    }

    @Override
    public void onPaymentError(int code, String response) {
        try {
            Toast.makeText(this, "Payment failed: " + code + " " + response, Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Log.e("Razor", "Exception in onPaymentError", e);
        }
    }

    String pay_id = "";

    public void capturepaymnet() {
        RazorpayClient razorpay = null;
        try {
            razorpay = new RazorpayClient(getString(R.string.api_key), getString(R.string.key_secret));

            JSONObject options = new JSONObject();

            double amount = 0;
            try {
                //amount = Double.parseDouble("1");
                amount = grandtotal * 100;  // paisa

            } catch (Exception e) {
                e.printStackTrace();
            }

            options.put("amount", amount);
            Payment capture = razorpay.Payments.capture(pay_id, options);

            try {

                new RefundStatusDialog(InvoicesummaryActivity.this, new RefundStatusDialog.OnDialogClickListener() {
                    @Override
                    public void onDialogImageRunClick(int pos) {

                        SharedPrefUserDetail.setString(InvoicesummaryActivity.this, SharedPrefUserDetail.isfrommyaggregator, "");
                        finish();

                        Class<?> cls = null;
                        if (Constants.chit_id_from_mrp == 0) {
                            cls = HistoryActivity.class;

                            Intent i = new Intent(InvoicesummaryActivity.this, cls);
                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            i.putExtra("session", sessionManager.getsession());
                            startActivity(i);
                        } else {


                            onItemClick();

                        }


                    }

                }, capture.toString(), true).show();

            } catch (Exception e) {

            }

        } catch (RazorpayException e) {
            e.printStackTrace();
        } catch (Exception e) {

        }

    }

    boolean iscod = true;
    String img = "";
    ArrayList<ModelFile> al_selet = new ArrayList<>();
    ArrayList<String> al_uploadedimg = new ArrayList<>();
    boolean isisue = false;

    AmazonS3 s3;
    TransferUtility transferUtility;

    public void inits3() {
        s3 = new AmazonS3Client(new BasicAWSCredentials(getString(R.string.accesskey), getString(R.string.secretkey)));
        transferUtility = new TransferUtility(s3, getApplicationContext());

    }

    String namegsxsax = "";

    public void uploadimg() {
        rl_upload.setVisibility(View.VISIBLE);
        int cc = c_img + 1;
        int total = CompressItemSelect.size();
        tv_progress.setText("Uploading " + cc + "/" + total);


        File file = new File(CompressItemSelect.get(c_img));
        namegsxsax = "ISSUEIMG_" + System.currentTimeMillis() + ".jpg";


        TransferObserver transferObserver = transferUtility.upload("cab-videofiles", namegsxsax, file);
        transferObserverListener(transferObserver);


    }

    int c_img = 0;
    String[] str_img = new String[]{};

    public void transferObserverListener(final TransferObserver transferObserver) {

        transferObserver.setTransferListener(new TransferListener() {
            @Override
            public void onStateChanged(int id, TransferState state) {
                if (state.name().equals("COMPLETED")) {


                    str_img[c_img] = getString(R.string.baseaws) + namegsxsax;
                    al_uploadedimg.add(str_img[c_img]);

                    c_img++;
                    if (c_img < al_selet.size()) {
                        uploadimg();
                    } else {
                        rl_upload.setVisibility(View.GONE);
                        c_img = 0;

                        if (rb_cod.isChecked()) {
                            confirmorder("");
                        } else {
                            startRazorPayment();
                            //launchPayUMoneyFlow();
                        }

                        //putdata();

                    }
                } else if (state.name().equals("FAILED")) {
                    rl_upload.setVisibility(View.GONE);
                    Toast.makeText(InvoicesummaryActivity.this, "Failed Uploading !", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                try {
                    int percentage = (int) (bytesCurrent / bytesTotal * 100);


                    //mProgressDialog.setProgress(percentage);


                    //Log.e("percentage ", " : " + percentage);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(int id, Exception ex) {

                Log.e("error", "error");
            }

        });
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if (findViewById(R.id.rl_pb_cat).getVisibility() == View.VISIBLE || findViewById(R.id.rl_upload).getVisibility() == View.VISIBLE) {

        } else {
            super.onBackPressed();
        }
    }

    ArrayList<String> CompressItemSelect;

    public class ImageCompress extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            CompressItemSelect = new ArrayList();
            findViewById(R.id.rl_pb_cat).setVisibility(View.VISIBLE);

        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                for (int i = 0; i < al_selet.size(); i++) {

                    File ImageFile = new File(al_selet.get(i).getImgpath());
                    File compressedImageFile = null;
                    String strFile = "";
                    try {
                        compressedImageFile = new Compressor(InvoicesummaryActivity.this).compressToFile(ImageFile);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    strFile = String.valueOf(compressedImageFile);

                    CompressItemSelect.add(strFile);

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            findViewById(R.id.rl_pb_cat).setVisibility(View.GONE);

            uploadimg();
        }
    }

    /// Charges

    @BindView(R.id.llCharge1)
    LinearLayout llCharge1;
    @BindView(R.id.tvChargeNm1)
    TextView tvChargeNm1;
    @BindView(R.id.tvCharegVal1)
    TextView tvCharegVal1;

    @BindView(R.id.llCharge2)
    LinearLayout llCharge2;
    @BindView(R.id.tvChargeNm2)
    TextView tvChargeNm2;
    @BindView(R.id.tvCharegVal2)
    TextView tvCharegVal2;

    Double charge1 = 0.0, charge2 = 0.0;

    public void setCharges() {

        try {

            llCharge1.setVisibility(View.GONE);
            llCharge2.setVisibility(View.GONE);
            //cbIsPickUpOnly.setVisibility(View.GONE);


            if (field_json_data != null && !field_json_data.isEmpty()) {

                JSONObject joField = new JSONObject(field_json_data);
                JSONArray arr = joField.getJSONArray("charges");

                JSONObject element;

                tvChargeNm1.setText("");
                tvCharegVal1.setText("");

                for (int i = 0; i < arr.length(); i++) {
                    element = arr.getJSONObject(i); // which for example will be Types,TotalPoints,ExpiringToday in the case of the first array(All_Details)

                    Iterator keys = element.keys();
                    while (keys.hasNext()) {
                        try {
                            String key = (String) keys.next();

                            String taxVal = JsonObjParse.getValueFromJsonObj(element, key);

                            if (tvChargeNm1.getText().toString().isEmpty()) {
                                if (taxVal.isEmpty()) taxVal = "0";
                                charge1 = Double.valueOf(taxVal);
                                tvChargeNm1.setText(key);
                                if (charge1 > 0) llCharge1.setVisibility(View.VISIBLE);

                                cbIsPickUpOnly.setVisibility(View.VISIBLE);

                            } else {
                                if (taxVal.isEmpty()) taxVal = "0";
                                charge2 = Double.valueOf(taxVal);
                                tvChargeNm2.setText(key);
                                if (charge2 > 0) llCharge2.setVisibility(View.VISIBLE);
                            }

                            Log.e("key", key);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

                //grandtotal = grandtotal + charge1 + charge2;
                tvCharegVal1.setText("+ " + currency + Inad.getCurrencyDecimal(charge1, this));
                tvCharegVal2.setText("+ " + currency + Inad.getCurrencyDecimal(charge2, this));
                //grandtotalamount.setText(currency + Inad.getCurrencyDecimal(grandtotal, this));
                setIsPickUp();
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

        setTaxGroupRv();
        checkConditions();
        checkProperty();


    }

    @BindView(R.id.cbIsPickUpOnly)
    CheckBox cbIsPickUpOnly;

    @BindView(R.id.txt_add)
    TextView txt_add;


    public void setIsPickUp() {

        String pick_up_only = JsonObjParse.getValueEmpty(field_json_data, "pick_up_only");

        if (pick_up_only.equalsIgnoreCase("Yes")) { // from business profile

            findViewById(R.id.rl_address).setVisibility(View.GONE);
            llCharge1.setVisibility(View.GONE);
            llCharge2.setVisibility(View.GONE);

            txt_add.setText("Pickup Only : "); // from company
            //grandtotal = grandtotal;
            grandtotalamount.setText(currency + Inad.getCurrencyDecimal(grandtotal, InvoicesummaryActivity.this));

            cbIsPickUpOnly.setChecked(true);
            cbIsPickUpOnly.setVisibility(View.GONE);
            llCharge1.setVisibility(View.GONE);
            llCharge2.setVisibility(View.GONE);
        } else {

            grandtotal = grandtotal + charge1 + charge2;
            grandtotalamount.setText(currency + Inad.getCurrencyDecimal(grandtotal, InvoicesummaryActivity.this));

            cbIsPickUpOnly.setChecked(false);
        }


        cbIsPickUpOnly.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if (b) { // from user
                    txt_add.setText("Delivery address : ");
                    strikeLineIfPickUpOnly(tvChargeNm1);
                    strikeLineIfPickUpOnly(tvChargeNm2);
                    strikeLineIfPickUpOnly(tvCharegVal1);
                    strikeLineIfPickUpOnly(tvCharegVal2);
                    strikeLineIfPickUpOnly(txt_add);
                    strikeLineIfPickUpOnly(tv_address);

                    grandtotal = grandtotal - charge1 - charge2;
                    grandtotalamount.setText(currency + Inad.getCurrencyDecimal(grandtotal, InvoicesummaryActivity.this));
                } else {
                    txt_add.setText("Delivery address : ");
                    removeStrikeLineIfPickUpOnly(tvChargeNm1);
                    removeStrikeLineIfPickUpOnly(tvChargeNm2);
                    removeStrikeLineIfPickUpOnly(tvCharegVal1);
                    removeStrikeLineIfPickUpOnly(tvCharegVal2);
                    removeStrikeLineIfPickUpOnly(txt_add);
                    removeStrikeLineIfPickUpOnly(tv_address);
                    grandtotal = grandtotal + charge1 + charge2;
                    grandtotalamount.setText(currency + Inad.getCurrencyDecimal(grandtotal, InvoicesummaryActivity.this));
                }

                checkConditions();


            }
        });
    }

    public void strikeLineIfPickUpOnly(TextView textView) {
        textView.setPaintFlags(textView.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
    }

    public void removeStrikeLineIfPickUpOnly(TextView textView) {
        textView.setPaintFlags(textView.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
    }

    @BindView(R.id.rvTaxGroup)
    RecyclerView rvTaxGroup;

    TaxGroupListAdapter taxGroupListAdapter;
    ArrayList<TaxGroupModel> alTax = new ArrayList<>();

    public void setTaxGroupRv() {

        taxGroupListAdapter = new TaxGroupListAdapter(alTax, this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        rvTaxGroup.setLayoutManager(mLayoutManager);
        rvTaxGroup.setAdapter(taxGroupListAdapter);

    }

    // Conditions

    @BindView(R.id.tvConditions)
    TextView tvConditions;

    @BindView(R.id.llConditions)
    LinearLayout llConditions;

    @BindView(R.id.tvTotalRoundVal)
    TextView tvTotalRoundVal;

    @BindView(R.id.llRoundOff)
    LinearLayout llRoundOff;


    public void checkConditions() {

        if (field_json_data != null) {
            String conditions = JsonObjParse.getValueEmpty(field_json_data, "conditions");
            llConditions.setVisibility(View.GONE);
            if (!conditions.isEmpty()) {
                tvConditions.setText(conditions + "");
                llConditions.setVisibility(View.VISIBLE);
            }

            totalWithRoundOff = 0.0;
            llRoundOff.setVisibility(View.GONE);

            String is_roundoff_near = JsonObjParse.getValueEmpty(field_json_data, "is_roundoff_near");

            if (is_roundoff_near.equalsIgnoreCase("yes")) {
                llRoundOff.setVisibility(View.VISIBLE);
                totalWithRoundOff = (double) Math.round(grandtotal);
                tvTotalRoundVal.setText(currency + Inad.getCurrencyDecimal(totalWithRoundOff, InvoicesummaryActivity.this));

                if (totalWithRoundOff.doubleValue() == grandtotal.doubleValue()) {
                    llRoundOff.setVisibility(View.GONE);
                    totalWithRoundOff = 0.0;
                }
            }
        }
    }

    @BindView(R.id.llDiscProduct)
    LinearLayout llDiscProduct;

    @BindView(R.id.tvDiscTotalProduct)
    TextView tvDiscTotalProduct;

    @BindView(R.id.tvSavings)
    TextView tvSavings;


    public void checkDiscount() {
        String str_productTotalDiscount = JsonObjParse.getValueEmpty(field_json_data, "productTotalDiscount");
        ProductTotalDiscount productTotalDiscount = new Gson().fromJson(str_productTotalDiscount, ProductTotalDiscount.class);

        if (productTotalDiscount == null)
            productTotalDiscount = new ProductTotalDiscount();

        llDiscProduct.setVisibility(View.GONE);

        if (productTotalDiscount.product_total_discount_type_of.equalsIgnoreCase("range")) {

            ArrayList<ProductTotalDiscount> alRange = new ArrayList<>();
            alRange = productTotalDiscount.alRange;
            totalReqDisc = 0.0;
            for (int i = 0; i < alRange.size(); i++) {

                productTotalDiscount = alRange.get(i);
                String min_range = productTotalDiscount.min_range;
                String max_range = productTotalDiscount.max_range;
                String disc_range = productTotalDiscount.disc_range;

                try {
                    Double minR = Double.parseDouble(min_range);
                    Double maxR = Double.parseDouble(max_range);
                    Double disR = Double.parseDouble(disc_range);


                    if (totalbill.doubleValue() >= minR.doubleValue() && totalbill.doubleValue() <= maxR.doubleValue()) {
                        llDiscProduct.setVisibility(View.VISIBLE);
                        totalReqDisc = (totalbill / 100.0f) * disR;
                        setReqDiscLbl(disR);
                        break;
                    }
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }

            }

            tvDiscTotalProduct.setText("- " + currency + Inad.getCurrencyDecimal(totalReqDisc, InvoicesummaryActivity.this));


        } else { // defauult check

            String disc_perc = productTotalDiscount.disc_perc;
            String max_disc_val = productTotalDiscount.max_disc_val;
            String min_purchase_val = productTotalDiscount.min_purchase_val;

            boolean isDiscountPossible = true;
            if (disc_perc.isEmpty()) {
                isDiscountPossible = false;
            } else {
                Double discVal = Double.parseDouble(disc_perc);
                Double minPurcVal = 0.0, maxDiscVal = 0.0;
                if (!min_purchase_val.isEmpty()) {
                    minPurcVal = Double.parseDouble(min_purchase_val);
                }
                if (!max_disc_val.isEmpty()) {
                    maxDiscVal = Double.parseDouble(max_disc_val);
                }
                if (minPurcVal >= 0) {

                    if (totalbill.doubleValue() < minPurcVal.doubleValue()) {
                        isDiscountPossible = false;
                    } else {
                        if (isDiscountPossible) {
                            llDiscProduct.setVisibility(View.VISIBLE);
                            totalReqDisc = (totalbill / 100.0f) * discVal;
                            if (maxDiscVal.doubleValue() > 0 && totalReqDisc.doubleValue() > maxDiscVal.doubleValue()) {

                                totalReqDisc = maxDiscVal;
                                setReqDiscLbl(-1);

                            } else {
                                setReqDiscLbl(discVal);
                                totalReqDisc = totalReqDisc;
                            }
                            tvDiscTotalProduct.setText("- " + currency + Inad.getCurrencyDecimal(totalReqDisc, InvoicesummaryActivity.this));
                        }
                    }
                }
            }
        }

        Double totalSavings = totalProductDisc + totalReqDisc;

        tvSavings.setVisibility(View.GONE);
        if (totalSavings > 0) tvSavings.setVisibility(View.VISIBLE);
        tvSavings.setText("You have saved " + currency + "" + Inad.getCurrencyDecimal(totalSavings, InvoicesummaryActivity.this));
        //tvSavings.setText("You have saved " + currency + "" + totalSavings);

    }

    public void setReqDiscLbl(double disc) {
        if (disc <= 0) {
            tvReqDiscLbl.setText("Discount" + " (" + "MAX." + ")");
        } else {
            tvReqDiscLbl.setText("Discount" + " @ " + disc + " %");
        }
        ReqDisc = disc;
    }

    @BindView(R.id.txtDelInfo)
    TextView txtDelInfo;

    @BindView(R.id.rgPayment)
    RadioGroup rgPayment;

    public void checkProperty() {

        if (service_type_of.equalsIgnoreCase(getString(R.string.property))) {
            cbIsPickUpOnly.setVisibility(View.GONE);
            rgPayment.setVisibility(View.GONE);
            rb_cod.setChecked(true);
            txtDelInfo.setText("Customer details");
            txt_add.setText("Customer address : ");
            orderconfirm.setText("Confirm enquiry");
        }

    }

    // Material

    @BindView(R.id.llCoupon)
    LinearLayout llCoupon;

    @BindView(R.id.llAppCoupon)
    LinearLayout llAppCoupon;

    @BindView(R.id.llEnterCoupon)
    LinearLayout llEnterCoupon;

    @BindView(R.id.llCouponSuccess)
    LinearLayout llCouponSuccess;

    @BindView(R.id.etCouponCode)
    EditText etCouponCode;

    @BindView(R.id.tvApplyCoupon)
    TextView tvApplyCoupon;

    PlayAnim playAnim = new PlayAnim();

    @OnClick(R.id.llAppCoupon)
    public void llAppCoupon(View v) {
        llAppCoupon.setVisibility(View.GONE);
        llEnterCoupon.setVisibility(View.VISIBLE);
        playAnim.slideLeftLl(llEnterCoupon);

    }

    @OnClick(R.id.tvTc)
    public void tvTc(View v) {

        Inad.showAnimDialogue(this, "Terms & conditions", couponApply.terms);
    }

    String couponCode = "";
    Calendar today = Calendar.getInstance();
    Coupon couponApply;

    @OnClick(R.id.tvApplyCoupon)
    public void tvApplyCoupon(View v) {

        couponCode = etCouponCode.getText().toString().trim();
        if (couponCode.isEmpty()) {
            Inad.alerterInfo("Alert", "Please enter coupon code", this);
        } else {

            couponApply = null;
            ArrayList<Coupon> alCoupon = ShareModel.getI().alPromo;
            for (int i = 0; i < alCoupon.size(); i++) {
                Coupon coupon = alCoupon.get(i);
                if (coupon.couponCode.equals(couponCode)) {
                    couponApply = coupon;
                    break;
                }
            }

            if (couponApply != null) {

                today.set(Calendar.HOUR_OF_DAY, 0);
                today.set(Calendar.MINUTE, 0);
                today.set(Calendar.SECOND, 0);
                today.set(Calendar.MILLISECOND, 0);

                Date dt = today.getTime();


                if (dt.equals(couponApply.getStartDt()) || dt.equals(couponApply.getEndDt()) ||
                        (dt.after(couponApply.getStartDt()) && dt.before(couponApply.getEndDt()))) {


                    try {
                        Utils.hideSoftKeyboard(InvoicesummaryActivity.this, etCouponCode);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                    checkCoupon();

                    //tvTotalCouponDiscount.setText("- Total discount");


                } else {
                    Inad.alerter("Coupon", "Coupon code is expired", this);
                }


                //Inad.alerterGrnInfo("Coupon","Coupon code applied successfully",this);
            } else {
                Inad.alerter("Coupon", "Coupon code is not valid", this);
            }
        }
    }

    String couponCurrency = "";

    public void checkPromoFromAggre() {

        tvApplyCoupon.setAlpha(0.6f);
        tvApplyCoupon.setEnabled(false);

        etCouponCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    tvApplyCoupon.setAlpha(1f);
                    tvApplyCoupon.setEnabled(true);
                } else {
                    tvApplyCoupon.setAlpha(0.6f);
                    tvApplyCoupon.setEnabled(false);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        llCoupon.setVisibility(View.GONE);
        llCouponDiscount.setVisibility(View.GONE);

        String businessUserNm = getString(R.string.Aggregator);
        businessUserNm = sessionManager.getcurrentu_nm();
        if (businessUserNm.contains("@")) {
            int indexOfannot = businessUserNm.indexOf("@") + 1;
            businessUserNm = businessUserNm.substring(indexOfannot, businessUserNm.indexOf("."));
        }

        new GetServiceTypeFromBusiUserProfile(this, businessUserNm, new GetServiceTypeFromBusiUserProfile.Apicallback() {
            @Override
            public void onGetResponse(String a, String response, String sms_emp) {

                try {

                    JSONObject Jsonresponse = new JSONObject(response);
                    JSONObject Jsonresponseinfo = Jsonresponse.getJSONObject("response");
                    JSONArray jaData = Jsonresponseinfo.getJSONArray("data");
                    if (jaData.length() > 0) {
                        JSONObject data = jaData.getJSONObject(0);
                        String field_json_data = JsonObjParse.getValueEmpty(data.toString(), "field_json_data");
                        String is_promo = JsonObjParse.getValueEmpty(field_json_data, "is_promo");
                        if (is_promo.equalsIgnoreCase("yes")) {
                            llAppCoupon.setVisibility(View.VISIBLE);
                            llEnterCoupon.setVisibility(View.GONE);
                            llCouponSuccess.setVisibility(View.GONE);
                            llCoupon.setVisibility(View.VISIBLE);

                        }
                        couponCurrency = JsonObjParse.getValueEmpty(data.toString(), "symbol_native");

                    } else {

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @BindView(R.id.llCouponDiscount)
    LinearLayout llCouponDiscount;

    @BindView(R.id.tvCouponValOrModulo)
    TextView tvCouponValOrModulo;

    @BindView(R.id.tvTotalCouponDiscount)
    TextView tvTotalCouponDiscount;

    Double couponDiscount = 0.0;

    public void checkCoupon() {

        couponDiscount = 0.0;

        if (couponApply.promo_modulo > 0) {
            couponDiscount = (grandtotal / 100.0f) * couponApply.promo_modulo;
        } else if (couponApply.promo_value > 0) {
            couponDiscount = couponApply.promo_value;
            couponApply.min_purchase = couponApply.promo_value;
        }

        if (couponApply.min_purchase > 0) {

            if (grandtotal >= couponApply.min_purchase) {

            } else { // if total less then min purchase
                couponDiscount = -1d;
            }

        }

        if (couponDiscount.doubleValue() == -1) {
            Inad.alerterInfo("Message", "Coupon is applicable on minimum order of " + currency + " " + couponApply.min_purchase, this);
        } else if (couponDiscount > 0) {

            String coupon = "" + couponApply.couponCode + "";

            if (couponApply.promo_modulo > 0) {
                coupon = coupon + " @ " + couponApply.promo_modulo + " % ";
            } else if (couponApply.promo_value > 0) {
                coupon = coupon + " discount";
            }

            if (couponApply.max_discount > 0 && couponApply.promo_modulo > 0) {
                if (couponDiscount > couponApply.max_discount) {
                    couponDiscount = couponApply.max_discount;
                    coupon = coupon + " discount" + " (" + "MAX." + ")";
                }
            }

            tvCouponValOrModulo.setText(coupon);

            llEnterCoupon.setVisibility(View.GONE);
            llCouponSuccess.setVisibility(View.VISIBLE);
            playAnim.slideLeftLl(llCouponSuccess);

            llCouponDiscount.setVisibility(View.VISIBLE);


            tvTotalCouponDiscount.setText("- " + currency + Inad.getCurrencyDecimal(couponDiscount, this));

            grandtotal = grandtotal - couponDiscount;

            grandtotalamount.setText(currency + Inad.getCurrencyDecimal(grandtotal, InvoicesummaryActivity.this));

            Double totalSavings = totalProductDisc + totalReqDisc + couponDiscount;

            tvSavings.setVisibility(View.GONE);
            if (totalSavings > 0) tvSavings.setVisibility(View.VISIBLE);
            tvSavings.setText("You have saved " + currency + "" + Inad.getCurrencyDecimal(totalSavings, InvoicesummaryActivity.this));


            checkConditions();

            Inad.alerterGrnInfo("Coupon", "Coupon code is applied successfully", this);

        }

        //tvTotalCouponDiscount.setText("- Total discount");

    }


    MyRequestCall myRequestCall = new MyRequestCall();
    MyRequst myRequst = new MyRequst();
    JsonObject joAddEdit = new JsonObject();

    JSONObject joFrom = new JSONObject(); // From should be his customer info
    JSONObject joTo = new JSONObject(); // supplier ; In To, supplier , name address etc

    public void addTaskPreferenceForMrpFromAllTask(final Chit chitOdr, final MyCallBkCommon myCallBkCommon) {

        boolean isMrp = SharedPrefUserDetail.getBoolean(this, SharedPrefUserDetail.isMrp, false);
        if (isMrp) { // from all task only pass flage

            final Chit chit = SummaryDetailFragmnet.item; // from
            joAddEdit.addProperty("flag", "add");
            joAddEdit.addProperty("from_chit_hash_id", chit.getChit_hash_id());
            joAddEdit.addProperty("from_chit_id", chit.getChit_id());
            joAddEdit.addProperty("from_ref_id", chit.getRef_id());
            joAddEdit.addProperty("from_case_id", chit.getCase_id());
            joAddEdit.addProperty("from_purpose", chit.getPurpose());
            joAddEdit.addProperty("from_field_json_data", joFrom.toString());

            joAddEdit.addProperty("to_bridge_id", chitOdr.getTo_bridge_id());
            joAddEdit.addProperty("to_chit_hash_id", chitOdr.getChit_hash_id());
            joAddEdit.addProperty("to_chit_id", chitOdr.getChit_id());
            joAddEdit.addProperty("to_ref_id", chitOdr.getRef_id());
            joAddEdit.addProperty("to_case_id", chitOdr.getCase_id());
            joAddEdit.addProperty("to_purpose", chitOdr.getPurpose());

            joAddEdit.addProperty("to_field_json_data", joTo.toString());
            if (vPos == 1) {
                joAddEdit.addProperty("task_purpose", "Material");
            } else if (vPos == 4) {
                joAddEdit.addProperty("task_purpose", "Delivery");
            }
            if (vPos == 1) {
                joAddEdit.addProperty("task_purpose", "Material");
            } else if (vPos == 4) {
                joAddEdit.addProperty("task_purpose", "Delivery");
            } else {
                joAddEdit.addProperty("task_purpose", "Material");
            }

            JsonObject joMrp = new JsonObject();
            JsonArray jsonElements = new JsonArray();
            jsonElements.add(joAddEdit);

            joMrp.add("newdata", jsonElements);

            myRequst.cookie = sessionManager.getsession();

            myRequestCall.addTaskPreferencr(this, myRequst, joMrp, new MyRequestCall.CallRequest() {
                @Override
                public void onGetResponse(String response) {
                    Constants.chit_id_from_mrp = chit.getChit_id();

                    myCallBkCommon.myCallBkOnly();
                }
            });

        } else {
            myCallBkCommon.myCallBkOnly();
        }
    }

    public void onItemClick() {

        Chit item = SummaryDetailFragmnet.item;

        Intent i = new Intent(this, ChitlogActivity.class);
        i.putExtra("chit_hash_id", item.getChit_hash_id());
        i.putExtra("chit_id", item.getChit_id() + "");
        i.putExtra("transtactionid", item.getChit_name() + "");
        i.putExtra("productqty", item.getTotal_chit_item_value() + "");
        i.putExtra("productprice", item.getChit_item_count() + "");
        i.putExtra("purpose", item.getPurpose());
        i.putExtra("Refid", item.getRef_id());
        i.putExtra("Caseid", item.getCase_id());
        i.putExtra("isprint", true);
        //if (tab.equalsIgnoreCase("new_all"))
        i.putExtra("isMrp", true);

        i.putExtra("iscomment", true);

        ShareModel.getI().isFromAllTask = true;

        ShareModel.getI().isBackFromChitLog = true;

        //Constants.chit_id_to = Constants.chit_id_to;
        DetailsFragment.chititemcount = "" + item.getChit_item_count();
        DetailsFragment.chititemprice = "" + item.getTotal_chit_item_value();
        SummaryDetailFragmnet.item = item;
        DetailsFragment.item_detail = item;
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
        finish();
    }

    public void deliveryDetail() {
        String fromName = JsonObjParse.getValueEmpty(joDelivery.toString(), "fromName");
        String fromContact = JsonObjParse.getValueEmpty(joDelivery.toString(), "fromContact");
        String fromAddress = JsonObjParse.getValueEmpty(joDelivery.toString(), "fromAddress");
        String toName = JsonObjParse.getValueEmpty(joDelivery.toString(), "toName");
        String toContact = JsonObjParse.getValueEmpty(joDelivery.toString(), "toContact");
        String toAddress = JsonObjParse.getValueEmpty(joDelivery.toString(), "toAddress");
        String date = JsonObjParse.getValueEmpty(joDelivery.toString(), "delivery_date");
        String time = JsonObjParse.getValueEmpty(joDelivery.toString(), "delivery_time");
        String reference = JsonObjParse.getValueEmpty(joDelivery.toString(), "reference");
        fromLatitude = Double.parseDouble(JsonObjParse.getValueEmpty(joDelivery.toString(), "fromLatitude"));
        fromLongitude = Double.parseDouble(JsonObjParse.getValueEmpty(joDelivery.toString(), "fromLongitude"));
        toLatitude = Double.parseDouble(JsonObjParse.getValueEmpty(joDelivery.toString(), "toLatitude"));
        toLongitude = Double.parseDouble(JsonObjParse.getValueEmpty(joDelivery.toString(), "toLongitude"));

        tvFromName.setText(fromName + "");
        tvFromContact.setText(fromContact + "");
        tvFromAddress.setText(fromAddress + "");
        tvToName.setText(toName + "");
        tvToContact.setText(toContact + "");
        tvToAddress.setText(toAddress + "");
        tvDate.setText(date + " " + time);
        tvReference.setText(reference + "");
    }
}