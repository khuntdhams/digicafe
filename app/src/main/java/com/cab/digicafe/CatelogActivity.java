package com.cab.digicafe;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.andremion.counterfab.CounterFab;
import com.cab.digicafe.Activities.CatalogeEditDataActivity;
import com.cab.digicafe.Activities.ImagePreviewActivity;
import com.cab.digicafe.Activities.RequestApis.MyRequestCall;
import com.cab.digicafe.Adapter.CatalogAdapter;
import com.cab.digicafe.Adapter.CategoryListAdapter;
import com.cab.digicafe.Database.SqlLiteDbHelper;
import com.cab.digicafe.Dialogbox.DialogLoading;
import com.cab.digicafe.Helper.Inad;
import com.cab.digicafe.Helper.LoadImg;
import com.cab.digicafe.Helper.SessionManager;
import com.cab.digicafe.Helper.SharedPrefUserDetail;
import com.cab.digicafe.Model.Catelog;
import com.cab.digicafe.Model.ModelFile;
import com.cab.digicafe.Model.ShareModel;
import com.cab.digicafe.Model.SupplierCategory;
import com.cab.digicafe.Model.SupplierNew;
import com.cab.digicafe.MyCustomClass.JsonObjParse;
import com.cab.digicafe.MyCustomClass.PlayAnim;
import com.cab.digicafe.MyCustomClass.RefreshSession;
import com.cab.digicafe.MyCustomClass.Utils;
import com.cab.digicafe.Rest.ApiClient;
import com.cab.digicafe.Rest.ApiInterface;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.xiaofeng.flowlayoutmanager.Alignment;
import com.xiaofeng.flowlayoutmanager.FlowLayoutManager;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Headers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CatelogActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener, CatalogAdapter.OnItemClickListener, SwipyRefreshLayout.OnRefreshListener, SearchView.OnQueryTextListener {
    private List<Catelog> productlist = new ArrayList<>();
    private RecyclerView recyclerView;
    private CatalogAdapter mAdapter;
    String sessionString;
    String bridgeidval;
    String frombridgeidval;
    public static String aliasname;
    public static String company_name = "";
    public static String username;
    String purpose = "";
    int pagecount;
    int pagesize;
    boolean isdataavailable;
    String querystringval;
    SwipyRefreshLayout mSwipyRefreshLayout;
    CounterFab counterFab;
    SessionManager sessionManager;
    ArrayList<Catelog> selectedproduct;
    ArrayList<String> selectedproductlist;
    SqlLiteDbHelper dbHelper;

    String currency_code = "";
    String category = "";

    boolean isstoreopen = false;
    String img = "";

    boolean isfrommyagree = false;
    String to_bridge_id = "";

    RelativeLayout rlSubmitSurvey;
    String delivery_charge = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.content_catelog, frameLayout);

        ButterKnife.bind(this);
        ShareModel.getI().isImgPrev = false;

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        rvCategory = (RecyclerView) findViewById(R.id.rvCategory);
        tv_empty = (TextView) findViewById(R.id.tv_empty);
        ivDown = (ImageView) findViewById(R.id.ivDown);
        ivOfr = (ImageView) findViewById(R.id.ivOfr);
        rlSubmitSurvey = (RelativeLayout) findViewById(R.id.rlSubmitSurvey);

        setSupportActionBar(toolbar);
        mSwipyRefreshLayout = (SwipyRefreshLayout) findViewById(R.id.swipyrefreshlayout);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        to_bridge_id = SharedPrefUserDetail.getString(this, SharedPrefUserDetail.isfrommyaggregator, "");
        if (!to_bridge_id.equals("")) {
            isfrommyagree = true;
        }

        pagesize = 25;
        querystringval = "";
        isdataavailable = true;
        // listen refresh event
        mSwipyRefreshLayout.setOnRefreshListener(this);
        sessionManager = new SessionManager(this);
        mAdapter = new CatalogAdapter(productlist, this, this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        DividerItemDecoration verticalDecoration = new DividerItemDecoration(recyclerView.getContext(),
                DividerItemDecoration.VERTICAL);
        Drawable verticalDivider = ContextCompat.getDrawable(this, R.drawable.horizontal_divider);
        verticalDecoration.setDrawable(verticalDivider);
        recyclerView.addItemDecoration(verticalDecoration);
        LinearLayoutManager manager = new LinearLayoutManager(this);
        recyclerView.setHasFixedSize(true);
        dbHelper = new SqlLiteDbHelper(this);
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(mAdapter);

        counterFab = (CounterFab) findViewById(R.id.counter_fab);
        pagecount = 1;
        counterFab.setCount(0); // Set the count value to show on badge
        // Decrease the current count value by 1
        if (sessionManager.getcurrentcatererstatus().equalsIgnoreCase("open")) {

        } else {
            //counterFab.setVisibility(View.GONE);
        }

        counterFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (counterFab.getCount() > 0) {

                    boolean isGo = true;
                    if (service_type_of.equalsIgnoreCase(getString(R.string.shopping_cart_cb))) {

                        String min = JsonObjParse.getValueEmpty(field_json_data, "min");
                        int minI = Integer.parseInt(min);
                        if (dbHelper.getcartcount() < minI) {
                            isGo = false;
                            Toast.makeText(CatelogActivity.this, "Select at least " + min + " items", Toast.LENGTH_LONG).show();
                        } else {

                            isGo = true;
                        }

                    } else {

                    }
                    if (isGo) {
                        Intent i = new Intent(CatelogActivity.this, OrderActivity.class);
                        i.putExtra("session", sessionString);
                        i.putExtra("bridgeidval", bridgeidval);
                        i.putExtra("frombridgeidval", frombridgeidval);
                        i.putExtra("purpose", purpose);
                        i.putExtra("img", img);
                        i.putExtra("delivery_charge", delivery_charge);
                        i.putExtra("isBk", true);
                        startActivity(i);
                    }


                } else {
                    if (isstoreopen) {
                        Apputil.showalert("Empty Cart!", CatelogActivity.this);
                    } else {
                        Apputil.showalert("Shop Closed!", CatelogActivity.this);
                    }


                }
            }
        });
        selectedproduct = new ArrayList<>();
        selectedproductlist = new ArrayList<>();
        /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/


        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                sessionString = null;
                bridgeidval = null;
                frombridgeidval = null;
                aliasname = null;
            } else {
                sessionString = extras.getString("session");
                bridgeidval = extras.getString("bridgeidval");
                frombridgeidval = extras.getString("frombridgeidval");
                aliasname = extras.getString("aliasname");
                username = extras.getString("username");
                currency_code = extras.getString("currency_code");

                purpose = extras.getString("purpose");
                delivery_charge = extras.getString("delivery_charge");
                if (extras.containsKey("img")) {
                    img = extras.getString("img");
                }

                company_name = extras.getString("company_name", "");
                isFromSupplier = extras.getBoolean("isFromSupplier", false);
                isFromSupplier = true;


            }
        } else {
            sessionString = (String) savedInstanceState.getSerializable("session");
            bridgeidval = (String) savedInstanceState.getSerializable("bridgeidval");
            frombridgeidval = (String) savedInstanceState.getSerializable("frombridgeidval");
            aliasname = (String) savedInstanceState.getSerializable("aliasname");
        }
        getSupportActionBar().setTitle(company_name);
        // tv_cat_title.setText("5828");

        store_businessstatus(username);
        setCategoryRv();


    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (rlAddCategory.getVisibility() == View.VISIBLE) {
            hideAddCat();
        } else {
            super.onBackPressed();
        }
    }

    Menu menu;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);

        this.menu = menu;
        //final MenuItem action_filter = menu.findItem(R.id.action_filter);
        //if(!isfrommyagree)action_filter.setVisible(true);

        final MenuItem item = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setOnQueryTextListener(this);

        MenuItemCompat.setOnActionExpandListener(item,
                new MenuItemCompat.OnActionExpandListener() {
                    @Override
                    public boolean onMenuItemActionCollapse(MenuItem item) {
// Do something when collapsed
                        mAdapter.setFilter(productlist);
                        return true; // Return true to collapse action view
                    }

                    @Override
                    public boolean onMenuItemActionExpand(MenuItem item) {
// Do something when expanded
                        return true; // Return true to expand action view
                    }
                });
        return true;
    }


    public boolean onQueryTextChange(String newText) {
        Log.e("aliassearchname", newText);

        if (newText.length() > 2 || newText.length() == 0) {
            productlist = new ArrayList<>();
            pagecount = 1;
            usersignin(sessionString, 1, newText.toLowerCase());
        }

        return true;
    }

    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    private List<Catelog> filter(List<Catelog> models, String query) {
        final List<Catelog> filteredModelList = new ArrayList<>();


        return filteredModelList;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.


        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
        }

        int id = item.getItemId();
        switch (id) {
            case R.id.action_filter:

                return true;
        }
        //noinspection SimplifiableIfStatement

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onResume() {
        super.onResume();
        counterFab.setCount(dbHelper.getcartcount());

        if (!ShareModel.getI().isImgPrev && ShareModel.getI().dbUnderMaster.isEmpty()) {


            mAdapter = new CatalogAdapter(productlist, this, this);
            recyclerView.setAdapter(mAdapter);
            if (mAdapter != null) {

            }
        }
        ShareModel.getI().isImgPrev = false;
        timer = new CountDownTimer(60 * 60 * 1000, 1000) {

            public void onTick(long millisUntilFinished) {
                //Some code
                long sec = millisUntilFinished / 1000;
                Log.e("sec", String.valueOf(sec));

            }

            public void onFinish() {

                isdataavailable = true;
                pagecount = 1;

                querystringval = "";
                usersignin(sessionString, pagecount, "");


            }
        };

        timer.start();

        setShoppingCart();

        setSubmitSurveyBtn();

        // put your code here...
    }

    private void loadFragment(Fragment fragment) {
// create a FragmentManager
        FragmentManager fm = getSupportFragmentManager();
// create a FragmentTransaction to begin the transaction and replace the Fragment
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
// replace the FrameLayout with new Fragment
        fragmentTransaction.replace(R.id.fragments, fragment);
        fragmentTransaction.commit(); // save the changes
    }

    public void usersignin(String usersession, final int pagecount, final String searchquery) {
        ApiInterface apiService =
                ApiClient.getClient(CatelogActivity.this).create(ApiInterface.class);
        Log.e("sess", sessionManager.getAggregatorprofile());

        Call call = null;

        call = apiService.getCatalougelist(usersession, sessionManager.getAggregatorprofile(), frombridgeidval, bridgeidval, pagecount, searchquery, currency_code, category);

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                int statusCode = response.code();
                Log.e("test", " trdds");

                Headers headerList = response.headers();
                Log.d("testcatelogue", statusCode + " ");

                DialogLoading.dialogLoading_Dismiss();
                //  String cookie = response.;
                //  Log.d("test", cookie+ " ");

                mSwipyRefreshLayout.setRefreshing(false);
                if (response.isSuccessful()) {
                    try {
                        String a = new Gson().toJson(response.body());
                        Log.e("catalogresponse", a);
                        JSONObject jObjRes = new JSONObject(a);
                        JSONObject jObjdata = jObjRes.getJSONObject("response");
                        JSONObject jObjResponse = jObjdata.getJSONObject("data");
                        String totalRecord = jObjResponse.getString("totalRecord");
                        JSONArray jObjcontacts = jObjResponse.getJSONArray("content");
                        String displayEndRecord = jObjResponse.getString("displayEndRecord");
                        if (totalRecord.equals(displayEndRecord)) {
                            isdataavailable = false;
                        } else {
                            isdataavailable = true;
                        }

                        if (pagecount == 1 && searchquery.length() > 0) {
                            productlist = new ArrayList<Catelog>();
                        }

                        boolean isSurvey = false;
                        if (service_type_of.equalsIgnoreCase(getString(R.string.survey))) {
                            isSurvey = true;
                        }

                        for (int i = 0; i < jObjcontacts.length(); i++) {
                            if (jObjcontacts.get(i) instanceof JSONObject) {
                                JSONObject jsnObj = (JSONObject) jObjcontacts.get(i);
                                Catelog obj = new Gson().fromJson(jsnObj.toString(), Catelog.class);
                                if (selectedproductlist.contains(String.valueOf(obj.getEntry_id()))) {
                                    int a1 = selectedproductlist.indexOf(String.valueOf(obj.getEntry_id()));
                                    Catelog objval = selectedproduct.get(a1);
                                    obj.setCartcount(objval.getCartcount());
                                }

                                if (isSurvey) {
                                    try {
                                        //String survey = JsonObjParse.getValueEmpty(obj.getInternal_json_data(), "survey");
                                        //String surveyQPos = JsonObjParse.getValueEmpty(survey, "SurveyQPos");
                                        if (obj.getCross_reference().isEmpty()) obj.sortOrder = 0;
                                        else
                                            obj.sortOrder = Integer.parseInt(obj.getCross_reference());
                                    } catch (NumberFormatException e) {
                                        e.printStackTrace();
                                    }

                                }
                                Log.e("alias", obj.getName());
                                productlist.add(obj);
                            }
                        }

                        if (isSurvey && productlist.size() > 0) {

                            Collections.sort(productlist, new Comparator<Catelog>() {
                                @Override
                                public int compare(Catelog p1, Catelog p2) {
                                    return p1.getSortOrder() - p2.getSortOrder(); // Ascending
                                }
                            });

                        }

                        Log.e("aliassearch", productlist.size() + "");

                        int ctr = productlist.size();
                        getSupportActionBar().setTitle("[ " + totalRecord + " ] " + company_name);

                        if (productlist.size() == 0) {
                            tv_empty.setVisibility(View.VISIBLE);
                        } else tv_empty.setVisibility(View.GONE);


                        mAdapter.setitem(productlist, isstoreopen);

                        if (alCategory.size() == 0) {
                            alCategory = new ArrayList<>();
                            if (isFromSupplier && jObjResponse.has("category")) {
                                JSONArray ja_category = jObjResponse.getJSONArray("category");
                                if (ja_category.length() > 0)
                                    alCategory.add(new SupplierCategory("All"));
                                for (int i = 0; i < ja_category.length(); i++) {
                                    if (ja_category.get(i) instanceof JSONObject) {
                                        JSONObject jsnObj = (JSONObject) ja_category.get(i);
                                        SupplierCategory obj = new Gson().fromJson(jsnObj.toString(), SupplierCategory.class);
                                        alCategory.add(obj);
                                    }
                                }
                                if (alCategory == null) alCategory = new ArrayList<>();
                                if (alCategory.size() > 0) {
                                    ivDown.setVisibility(View.GONE);
                                    if (alCategory.size() > 5) ivDown.setVisibility(View.VISIBLE);

                                    rvCategory.setVisibility(View.VISIBLE);
                                    alCategory.get(0).setChecked(true);

                                    if (template_type.equalsIgnoreCase("image")) {
                                        categoryImageTemplateListAdapter.setitem(alCategory);

                                    } else {

                                        categoryListAdapter.setitem(alCategory);
                                    }


                                } else {
                                    rvCategory.setVisibility(View.GONE);
                                }
                            } else {
                                rvCategory.setVisibility(View.GONE);
                            }
                        }

                        setSubmitSurveyBtn();


                        //mAdapter.notifyDataSetChanged();
                    } catch (Exception e) {
                        Toast.makeText(CatelogActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                    //  Intent i = new Intent(MainActivity.this, MainActivity.class);
                    //  startActivity(i);
                } else {
                    Log.e("test", " trdds1");

                    switch (response.code()) {
                        case 401:
                            new RefreshSession(CatelogActivity.this, sessionManager);  // session expired
                            break;
                        case 404:
                            Toast.makeText(CatelogActivity.this, "not found", Toast.LENGTH_SHORT).show();
                            break;
                        case 500:
                            Toast.makeText(CatelogActivity.this, "server broken", Toast.LENGTH_SHORT).show();
                            break;
                        default:
                            Toast.makeText(CatelogActivity.this, "unknown error", Toast.LENGTH_SHORT).show();
                            break;
                    }


                    try {
                       /* JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Toast.makeText(MainActivity.this, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();*/
                    } catch (Exception e) {
                        Toast.makeText(CatelogActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }

                //  List<Movie> movies = response.body().getResults();
                //  recyclerView.setAdapter(new MoviesAdapter(movies, R.layout.list_item_movie, getApplicationContext()));
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // Log error here since request failed
                DialogLoading.dialogLoading_Dismiss();
                Log.e("error message", t.toString());
            }
        });
    }


    @Override
    public void onItemClick(Catelog item) {
        if (item.getImageArr().size() == 0) {
            Toast.makeText(this, "No Media found", Toast.LENGTH_LONG).show();
        } else {
            ShareModel.getI().isImgPrev = true;

            /*GalleryActivity.images = item.getImageArr();
            ArrayList<String> a = new ArrayList<>();
            a = item.getVideolArr();

            if (a == null || a.size() == 0) {
                a = new ArrayList<>();
            }
            GalleryActivity.videos = a;
            Intent i = new Intent(CatelogActivity.this, GalleryActivity.class);
            startActivity(i);*/

            ArrayList<ModelFile> al_img = new ArrayList<>();
            try {

                for (int i = 0; i < item.getImageArr().size(); i++) {
                    al_img.add(new ModelFile(item.getImageArr().get(i).toString(), false));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            String imgarray = new Gson().toJson(al_img);


            Intent intent = new Intent(CatelogActivity.this, ImagePreviewActivity.class);
            intent.putExtra("imgarray", imgarray);
            intent.putExtra("pos", 0);
            intent.putExtra("img_title", item.getDefault_name());
            startActivity(intent);
        }
    }

    @Override
    public void onShowPropComment(Catelog item, int pos) {

        showInfoDialogue = "comment";

        String name = item.getDefault_name();

        String surveyoption[] = item.getDefault_name().split("///");

        if (surveyoption.length > 1) {
            name = surveyoption[0];
            //holder.txt_serialno.setText("  "+surveyoption[1]);
        }


        String property = JsonObjParse.getValueEmpty(item.getInternal_json_data(), "property");
        String status_commnet = JsonObjParse.getValueEmpty(property, "status_commnet");

        if (!status_commnet.isEmpty()) showInfo("" + name, "" + status_commnet + "");

    }


    @Override
    public void onadditem(Catelog product) {

        String wholesale = JsonObjParse.getValueEmpty(product.getInternal_json_data(), "wholesale");
        String survey = JsonObjParse.getValueEmpty(product.getInternal_json_data(), "survey");
        if (!wholesale.isEmpty()) {
            //int ctr = counterFab.getCount() + product.getCartcount();
            //counterFab.setCount(ctr);
            Apputil.inserttocartWholesale(dbHelper, product, aliasname);
            //counterFab.setCount(dbHelper.getcartcount());

        }
        //  if (!survey.isEmpty()) {
        else if (!survey.isEmpty()) {
            //int ctr = counterFab.getCount() + product.getCartcount();
            //counterFab.setCount(ctr);
            Apputil.inserttocartWholesale(dbHelper, product, aliasname);
            //counterFab.setCount(dbHelper.getcartcount());
        } else {

            if (service_type_of.equalsIgnoreCase(getString(R.string.jewel))) {

               /* Metal metal = ShareModel.getI().metal;
                if (metal == null) metal = new Metal();
                int serviceId = metal.serviceId;
                if(serviceId==1)counterFab.setCount(1);*/
            } else if (service_type_of.equalsIgnoreCase(getString(R.string.property))) {
                //counterFab.setCount(1);
            } else {
                //counterFab.increase();
            }
            Apputil.inserttocart(dbHelper, product, aliasname);
        }

        counterFab.setCount(dbHelper.getcartcount());
        isSubmtProgress = true;
        setSubmitSurveyBtn();
    }

    @Override
    public void onPause() {
        super.onPause();

        timer.cancel();

        Gson gson = new Gson();

        String listString = gson.toJson(
                selectedproduct,
                new TypeToken<ArrayList<Catelog>>() {
                }.getType());
        sessionManager.setCartproduct(listString);
        /*try {
            JSONArray jsonArray =  new JSONArray(listString);
            Log.e("cartproduct",jsonArray.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }*/
        // put your code here...
    }

    @Override
    public void ondecreasecount(Catelog product) {

        String wholesale = JsonObjParse.getValueEmpty(product.getInternal_json_data(), "wholesale");
        if (!wholesale.isEmpty()) {
            //int ctr = counterFab.getCount() + product.getCartcount();
            //counterFab.setCount(ctr);

            dbHelper.deleteRecord(product);
            counterFab.setCount(dbHelper.getcartcount());

        } else {
            counterFab.decrease();
            if (product.getCartcount() == 0) {
                dbHelper.deleteRecord(product);
            } else {
                dbHelper.updateRecord(product);
            }
        }


    }

    @Override
    public void onSurveyComment(Catelog qty, final int p) {

        showInfoDialogue = "comment";

        String q = qty.getDefault_name();

        if (Apputil.isCatExist(dbHelper, qty)) {
            Catelog c = Apputil.getCatelog(dbHelper, qty);
            q = c.getDefault_name();
        }

        etCommentForSurvey.setText("");

        String surveyoption[] = q.split("///");
        if (surveyoption.length > 1) q = surveyoption[0];
        if (surveyoption.length > 2) {
            etCommentForSurvey.setText(surveyoption[2] + "");
        }

        showInfo("" + company_name, "" + q + "");

        pos = p;

    }

    @Override
    public void onRefresh(SwipyRefreshLayoutDirection direction) {
        Log.d("MainActivity", "Refresh triggered at "
                + (direction == SwipyRefreshLayoutDirection.TOP ? "top" : "bottom"));
        mSwipyRefreshLayout.setRefreshing(false);

        Log.e("scroll direction", "Refresh triggered at "
                + (direction == SwipyRefreshLayoutDirection.TOP ? "top" : "bottom"));
        if (direction == SwipyRefreshLayoutDirection.TOP) {
            isdataavailable = true;
            pagecount = 1;
            Log.e("scroll direction", "top");
            productlist = new ArrayList<>();

            usersignin(sessionString, pagecount, querystringval);
        } else if (direction == SwipyRefreshLayoutDirection.BOTTOM) {
            Log.e("scroll direction", "bottom");
            if (isdataavailable) {
                pagecount++;
                usersignin(sessionString, pagecount, querystringval);
            } else {
                Toast.makeText(this, "No more data available", Toast.LENGTH_LONG).show();
            }
        }
    }

    public void store_businessstatus(String username) {

        ApiInterface apiService =
                ApiClient.getClient(CatelogActivity.this).create(ApiInterface.class);
        Call call;
        call = apiService.getagregatorProfile(username);

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                int statusCode = response.code();
                //  String cookie = response.;
                //  Log.d("test", cookie+ " ");

                if (response.isSuccessful()) {

                    try {
                        String a = new Gson().toJson(response.body());
                        JSONObject Jsonresponse = new JSONObject(a);
                        JSONObject Jsonresponseinfo = Jsonresponse.getJSONObject("response");


                        Log.e("test", " trdds" + Jsonresponse.toString());
                        JSONArray aggregatorobj = Jsonresponseinfo.getJSONArray("data");
                        JSONObject aggregator = aggregatorobj.getJSONObject(0);
                        String aggregatorbridgeid = aggregator.getString("bridge_id");
                        String isactive = aggregator.getString("business_status");
                        //sessionManager.setAggregatorprofile(aggregatorbridgeid);
                        //isactive = "";


                        if (isactive.equalsIgnoreCase("open")) {
                            Drawable vectorDrawable = ResourcesCompat.getDrawable(getResources(), R.drawable.shoppingcart, null);
                            isstoreopen = true;
                            counterFab.setImageDrawable(null);
                            counterFab.setImageDrawable(vectorDrawable);

                        } else {
                            Drawable vectorDrawable = ResourcesCompat.getDrawable(getResources(), R.drawable.locked, null);
                            isstoreopen = false;
                            counterFab.setImageDrawable(null);
                            counterFab.setImageDrawable(vectorDrawable);
                        }


                        usersignin(sessionString, pagecount, querystringval);

                    } catch (Exception e) {
                        Log.e("test", " trdds" + e.getLocalizedMessage());

                    }
                } else {
                    Log.e("test", " trdds1");

                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Toast.makeText(CatelogActivity.this, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        // Toast.makeText(CatelogActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }

                //  List<Movie> movies = response.body().getResults();
                //  recyclerView.setAdapter(new MoviesAdapter(movies, R.layout.list_item_movie, getApplicationContext()));
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // Log error here since request failed
                Log.e("error message", t.toString());
            }
        });
    }

    CountDownTimer timer;

    boolean isFromSupplier = false;
    RecyclerView rvCategory;
    TextView tv_empty;
    ImageView ivDown, ivOfr;
    ArrayList<SupplierCategory> alCategory = new ArrayList<>();
    CategoryListAdapter categoryListAdapter;
    CategoryImageTemplateListAdapter categoryImageTemplateListAdapter;
    boolean isLinear = true;


    public void setCategoryRv() {


        if (isLinear) {
            ivDown.setRotation(0);
        } else {
            ivDown.setRotation(180);
        }

        field_json_data = SharedPrefUserDetail.getString(this, SharedPrefUserDetail.field_json_data, "");
        template_type = JsonObjParse.getValueEmpty(field_json_data, "template_type");
        template_type = "";
        if (template_type.equalsIgnoreCase("image")) {

            categoryImageTemplateListAdapter = new CategoryImageTemplateListAdapter(alCategory, CatelogActivity.this, new CategoryImageTemplateListAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(SupplierCategory item, int p) {


                    isLinear = false;
                    ivDown.performClick();

                    category = item.getPricelistCategoryName();
                    if (category.equalsIgnoreCase("all")) category = "";

                    rvCategory.scrollToPosition(p);

                    pagecount = 1;
                    productlist = new ArrayList<>();
                    DialogLoading.dialogLoading(CatelogActivity.this);

                    usersignin(sessionString, pagecount, querystringval);
                }
            });


        } else {

            categoryListAdapter = new CategoryListAdapter(alCategory, CatelogActivity.this, new CategoryListAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(SupplierCategory item) {
                    callCategory(item);
                }
            });

        }

        if (isLinear) {
            rvCategory.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        } else {
            FlowLayoutManager layoutManager = new FlowLayoutManager();
            layoutManager.setAutoMeasureEnabled(true);
            layoutManager.setAlignment(Alignment.LEFT);
            rvCategory.setLayoutManager(layoutManager);
        }
        //

        rvCategory.setItemAnimator(new DefaultItemAnimator());

        if (template_type.equalsIgnoreCase("image")) {
            rvCategory.setAdapter(categoryImageTemplateListAdapter);

        } else {
            rvCategory.setAdapter(categoryListAdapter);

        }
        rvCategory.setNestedScrollingEnabled(false);


        ivDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isLinear = !isLinear;
                setCategoryRv();
            }
        });

        ivOfr.setVisibility(View.GONE);
        String disc_info = JsonObjParse.getValueEmpty(field_json_data, "disc_info");
        if (disc_info.length() > 2) ivOfr.setVisibility(View.VISIBLE);
        ivOfr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String disc_info = JsonObjParse.getValueEmpty(field_json_data, "disc_info");
                showInfo("" + company_name, "Discount : " + disc_info + "");

            }
        });


    }

    boolean isDefaultFstForSurvey = true;

    public void callCategory(SupplierCategory item) {
        category = item.getPricelistCategoryName();
        if (category.equalsIgnoreCase("all")) category = "";

        pagecount = 1;
        productlist = new ArrayList<>();
        DialogLoading.dialogLoading(CatelogActivity.this);
        usersignin(sessionString, pagecount, querystringval);
    }

    @Override
    public void onViewClick(Catelog item) { // for Database under master only

        item.setName(item.getDefault_name());

        Intent i = new Intent(this, CatalogeEditDataActivity.class);
        Gson gson = new Gson();
        String json = gson.toJson(item);
        i.putExtra("CatalougeContent", json);
        i.putExtra("isadd", false);

        ShareModel.getI().alCategory = new Gson().toJson(alCategory, new TypeToken<ArrayList<SupplierCategory>>() {
        }.getType());


        startActivityForResult(i, 21);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 21) {

            if (resultCode == RESULT_OK) {

                isdataavailable = true;
                pagecount = 1;
                Log.e("scroll direction", "top");
                productlist = new ArrayList<>();
                usersignin(sessionString, pagecount, querystringval);

            }

        }
    }

    String field_json_data = "", service_type_of = "", template_type = "";

    public void setShoppingCart() {

        counterFab.setVisibility(View.VISIBLE);
        if (!ShareModel.getI().dbUnderMaster.isEmpty()) counterFab.setVisibility(View.GONE);


        field_json_data = SharedPrefUserDetail.getString(this, SharedPrefUserDetail.field_json_data, "");
        service_type_of = JsonObjParse.getValueEmpty(field_json_data, "service_type_of");

        if (service_type_of.equalsIgnoreCase(getString(R.string.catalogue_only))) {
            counterFab.setVisibility(View.GONE);
        }
        if (service_type_of.equalsIgnoreCase(getString(R.string.survey))) {
            counterFab.setVisibility(View.GONE);
        }
    }

    // show dislogue

    PlayAnim playAnim = new PlayAnim();

    @BindView(R.id.llAnimAddcat)
    LinearLayout llAnimAddcat;

    @OnClick(R.id.rlAddCategory)
    public void rlAddCategory(View v) {
        onBackPressed();
    }

    @BindView(R.id.rlAddCategory)
    RelativeLayout rlAddCategory;

    @BindView(R.id.tilHint)
    TextInputLayout tilHint;

    @BindView(R.id.tilMobile)
    TextInputLayout tilMobile;


    @BindView(R.id.tvAddEditTitle)
    TextView tvAddEditTitle;

    @BindView(R.id.tvAdd)
    TextView tvAdd;

    @BindView(R.id.tvCancel)
    TextView tvCancel;

    @BindView(R.id.tvMsg)
    TextView tvMsg;

    @BindView(R.id.tilComment)
    TextInputLayout tilComment;

    @BindView(R.id.etCommentForSurvey)
    EditText etCommentForSurvey;


    int pos = 0;

    @OnClick(R.id.tvAdd)
    public void tvAdd(View v) {

        if (tilComment.getVisibility() == View.VISIBLE) {
            if (!etCommentForSurvey.getText().toString().trim().isEmpty()) {
                showInfoDialogue = "";
                mAdapter.setitem(pos, etCommentForSurvey.getText().toString().trim());
                hideAddCat();
            } else {
                //etCommentForSurvey.setError("Please enter comment");
                Toast.makeText(CatelogActivity.this, "Please enter comment", Toast.LENGTH_LONG).show();
            }
        } else {
            hideAddCat();
        }

    }


    @OnClick(R.id.tvCancel)
    public void tvCancel(View v) {

        if (tilComment.getVisibility() == View.VISIBLE) {
            showInfoDialogue = "";
        }


        hideAddCat();

    }

    String showInfoDialogue = "";

    public void showInfo(String title, String msg) {


        //etCommentForSurvey.setError("");
        tilMobile.setVisibility(View.GONE);
        tilHint.setVisibility(View.GONE);
        playAnim.slideDownRl(llAnimAddcat);
        rlAddCategory.setVisibility(View.VISIBLE);

        //tvAdd.setText("Yes");
        tvMsg.setVisibility(View.GONE);

        tvAddEditTitle.setText(title);
        tvAdd.setText("Ok");
        tvCancel.setVisibility(View.GONE);
        tvMsg.setText(msg);
        tvMsg.setVisibility(View.VISIBLE);
        tilComment.setVisibility(View.GONE);

        if (showInfoDialogue.equals("comment")) {
            Utils.showSoftKeyboard(this);
            tilComment.setVisibility(View.VISIBLE);
            tvCancel.setVisibility(View.VISIBLE);
            tvAdd.setVisibility(View.VISIBLE);
            etCommentForSurvey.requestFocus();

        }
    }


    public void hideAddCat() {
        playAnim.slideUpRl(llAnimAddcat);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    Utils.hideSoftKeyboard(CatelogActivity.this, etCommentForSurvey);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                rlAddCategory.setVisibility(View.GONE);
            }
        }, 700);
    }

    @BindView(R.id.tvSubmitSurvey)
    TextView tvSubmitSurvey;


    LoadImg loadImg = new LoadImg();

    boolean isSubmtProgress = false;

    int outOfTotal = 0;
    int totalSurveying = 0;

    public void setSubmitSurveyBtn() {

        field_json_data = SharedPrefUserDetail.getString(this, SharedPrefUserDetail.field_json_data, "");
        service_type_of = JsonObjParse.getValueEmpty(field_json_data, "service_type_of");

        if (dbHelper.getcartcount() > 0) {
            rlSubmitSurvey.setAlpha(1f);
            rlSubmitSurvey.setEnabled(true);
        }

        //if(category.isEmpty())outOfTotal = productlist.size();
        outOfTotal = productlist.size();
        totalSurveying = 0;

        for (int i = 0; i < productlist.size(); i++) {
            if (Apputil.isCatExist(dbHelper, productlist.get(i))) {
                totalSurveying++;
            }
        }

        //if (service_type_of.equalsIgnoreCase(getString(R.string.survey))&&(category.isEmpty()||isSubmtProgress)) {
        if (service_type_of.equalsIgnoreCase(getString(R.string.survey))) {
            isSubmtProgress = false;
            rlSubmitSurvey.setVisibility(View.VISIBLE);
            tvSubmitSurvey.setText("Submit survey [" + totalSurveying + "/" + outOfTotal + "]");
        }

        if (service_type_of.equalsIgnoreCase(getString(R.string.survey)) && alCategory.size() > 1 && isDefaultFstForSurvey) {
            alCategory.get(0).setChecked(false);
            alCategory.get(1).setChecked(true);
            categoryListAdapter.setitem(alCategory);
            callCategory(alCategory.get(1));
            isDefaultFstForSurvey = false;
        }

        rlSubmitSurvey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dbHelper.getcartcount() > 0) {
                    //loadImg.loadFitImg(CatelogActivity.this, tvSubmitSurvey);

                    MyRequestCall myRequestCall = new MyRequestCall();
                    myRequestCall.callCreateChitFoSurvey(CatelogActivity.this, new MyRequestCall.CallRequest() {
                        @Override
                        public void onGetResponse(String response) {
                            //tvAdd.setBackground(ContextCompat.getDrawable(CatelogActivity.this, R.drawable.fill_sq_blue));
                            Inad.alerterInfo("Survey", "Thanks for surveying..", CatelogActivity.this);

                            final Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {

                                    if (sessionManager.getIsEmployess()) {
                                        //sessionManager.setcurrentcaterer("");
                                        //sessionManager.setcurrentcaterername("");
                                        sessionManager.setCartproduct("");
                                        sessionManager.setCartcount(0);
                                        sessionManager.setRemark("");
                                        sessionManager.setDate("");
                                        sessionManager.setAddress("");
                                        sessionManager.setDobForSurvey("");
                                        sessionManager.setMobile("");
                                        dbHelper.deleteallfromcart();
                                        //sessionManager.setAggregator_ID(getString(R.string.Aggregator));
                                        SharedPrefUserDetail.setString(CatelogActivity.this, SharedPrefUserDetail.isfrommyaggregator, "");
                                        mAdapter.setitem(productlist, isstoreopen);

                                        onRefresh(SwipyRefreshLayoutDirection.TOP);
                                    } else { // for consumer - means business also
                                        sessionManager.setcurrentcaterer("");
                                        sessionManager.setcurrentcaterername("");
                                        sessionManager.setCartproduct("");
                                        sessionManager.setCartcount(0);
                                        sessionManager.setRemark("");
                                        sessionManager.setDate("");
                                        sessionManager.setDobForSurvey("");
                                        sessionManager.setMobile("");
                                        sessionManager.setAddress("");
                                        dbHelper.deleteallfromcart();
                                        sessionManager.setAggregator_ID(getString(R.string.Aggregator));
                                        SharedPrefUserDetail.setString(CatelogActivity.this, SharedPrefUserDetail.isfrommyaggregator, "");


                                        finish();
                                        Intent i = new Intent(CatelogActivity.this, HistoryActivity.class);
                                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        i.putExtra("session", sessionManager.getsession());
                                        startActivity(i);

                                    }


                                   /* finish();
                                    startActivity(getIntent());*/
                                }
                            }, 2000);

                        }
                    }, frombridgeidval);

                } else {
                    Inad.alerter("Survey", "Please choose any options", CatelogActivity.this);
                }
            }
        });

    }
}
