package com.cab.digicafe.serviceTypeClass;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.cab.digicafe.Activities.EditCurrencyForBusinessActivity;
import com.cab.digicafe.Activities.MySupplierActivity;
import com.cab.digicafe.Activities.master.AddShopsUnderMasterActivity;
import com.cab.digicafe.ChitlogActivity;
import com.cab.digicafe.MyCustomClass.JsonObjParse;
import com.cab.digicafe.Rest.ApiClient;
import com.cab.digicafe.Rest.ApiInterface;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GetServiceTypeFromBusiUserProfile {


    ProgressDialog dialog;


    public GetServiceTypeFromBusiUserProfile(final Context c, String aggre, final Apicallback callback) {

        dialog = new ProgressDialog(c);
        if (dialog != null && !dialog.isShowing()) {
            dialog.setMessage("Loading...");
            dialog.setCancelable(false);
            if (c instanceof EditCurrencyForBusinessActivity) {

            } else if (c instanceof AddShopsUnderMasterActivity) {

            } else if (c instanceof MySupplierActivity) {

            } else if (c instanceof ChitlogActivity) {

            } else {
                dialog.show();
            }

        }

        ApiInterface apiService = ApiClient.getClient(c).create(ApiInterface.class);
        Call call;
        call = apiService.getagregatorProfile(aggre);


        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {

                if (dialog != null) {
                    dialog.dismiss();
                }

                if (response.isSuccessful()) {

                    try {
                        String a = new Gson().toJson(response.body());

                        JSONObject Jsonresponse = new JSONObject(a);
                        JSONObject Jsonresponseinfo = Jsonresponse.getJSONObject("response");
                        JSONArray jaData = Jsonresponseinfo.getJSONArray("data");
                        if (jaData.length() > 0) {
                            JSONObject data = jaData.getJSONObject(0);
                            String field_json_data = JsonObjParse.getValueEmpty(data.toString(), "field_json_data");
                            String sms_employee = JsonObjParse.getValueEmpty(field_json_data, "sms_employee");
                            String service_type = JsonObjParse.getValueEmpty(field_json_data, "service_type");
                            callback.onGetResponse(service_type, a, sms_employee);
                        } else {
                            callback.onGetResponse("", "", "");

                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {


                    try {

                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Toast.makeText(c, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        // Toast.makeText(CatelogActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }


            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // Log error here since request failed
                if (dialog != null) {
                    dialog.dismiss();
                }
                Log.e("error message", t.toString());
            }
        });
    }

    public void getBusinessProfileFromUserId(final Context c, String cookie, final Apicallback callback) {


        ApiInterface apiService = ApiClient.getClient(c).create(ApiInterface.class);
        Call call;
        call = apiService.getBusinessProfile(cookie);

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {


                if (response.isSuccessful()) {
                    String a = new Gson().toJson(response.body());
                    callback.onGetResponse("", a, "");

                }

            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // Log error here since request failed
                if (dialog != null) {
                    dialog.dismiss();
                }
                Log.e("error message", t.toString());
            }
        });
    }

    public GetServiceTypeFromBusiUserProfile() {

    }

    public GetServiceTypeFromBusiUserProfile(final Context c, String cookie, String aggre, final Apicallback callback) {

        dialog = new ProgressDialog(c);
        if (dialog != null && !dialog.isShowing()) {
            dialog.setMessage("Loading...");
            dialog.setCancelable(false);

            dialog.show();


        }

        ApiInterface apiService = ApiClient.getClient(c).create(ApiInterface.class);

        Call call;

        call = apiService.searchNw(cookie, aggre);


        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {

                if (dialog != null) {
                    dialog.dismiss();
                }

                if (response.isSuccessful()) {

                    try {
                        String a = new Gson().toJson(response.body());


                        JSONObject Jsonresponse = new JSONObject(a);
                        JSONObject Jsonresponseinfo = Jsonresponse.getJSONObject("response");
                        JSONObject joData = Jsonresponseinfo.getJSONObject("data");
                        JSONArray jaData = joData.getJSONArray("content");
                        if (jaData.length() > 0) {
                            JSONObject data = jaData.getJSONObject(0);
                            callback.onGetResponse("", a, "");
                        } else {
                            callback.onGetResponse("", "", "");

                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {


                    try {

                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        callback.onGetResponse(jObjErrorresponse.getString("errormsg"), "", "");

                        Toast.makeText(c, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        // Toast.makeText(CatelogActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }


            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // Log error here since request failed
                if (dialog != null) {
                    dialog.dismiss();
                }
                Log.e("error message", t.toString());
            }
        });
    }


    public interface Apicallback {
        void onGetResponse(String a, String response, String sms_emp);
    }

}
