package com.cab.digicafe.Activities.mrp;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TextInputEditText;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.CircularProgressDrawable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.cab.digicafe.Activities.RequestApis.MyRequestCall;
import com.cab.digicafe.Activities.RequestApis.MyRequst;
import com.cab.digicafe.Adapter.mrp.TaskPrefViewModel;
import com.cab.digicafe.Fragments.SummaryDetailFragmnet;
import com.cab.digicafe.Helper.Constants;
import com.cab.digicafe.Helper.SessionManager;
import com.cab.digicafe.Model.Chit;
import com.cab.digicafe.Model.ShareModel;
import com.cab.digicafe.MyCustomClass.AutoCompleteTv;
import com.cab.digicafe.MyCustomClass.DatePickerNew;
import com.cab.digicafe.MyCustomClass.JsonObjParse;
import com.cab.digicafe.MyCustomClass.PlayAnim;
import com.cab.digicafe.R;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class PlanningDialogue extends AlertDialog implements
        View.OnClickListener {
    private final OnDialogClickListener listener;
    public AppCompatActivity c;
    public Dialog d;


    @BindView(R.id.llAnim)
    LinearLayout llAnim;


    @BindView(R.id.tvAdd)
    TextView tvAdd;

    @BindView(R.id.tvCancel)
    TextView tvCancel;

    @BindView(R.id.tvStatus)
    TextView tvStatus;

    @BindView(R.id.rlHide)
    RelativeLayout rlHide;

    String title = "", status = "";
    TaskPrefViewModel taskPrefViewModel;

    public PlanningDialogue(Context a, OnDialogClickListener listener, String title, String view) {
        super(a);
        // TODO Auto-generated constructor stub
        this.c = (AppCompatActivity) a;
        this.listener = listener;
        this.title = title;
        this.status = view;
    }

    String symbol_native = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_planning_dialogue);
        ButterKnife.bind(this);

        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        getWindow().setAttributes(lp);


        tvCancel.setOnClickListener(this);
        rlHide.setOnClickListener(this);

        setCancelable(true);

        tvStatus.setText(title);
        /*setOnKeyListener(new OnKeyListener() {

            @Override
            public boolean onKey(DialogInterface arg0, int keyCode,
                                 KeyEvent event) {
                // TODO Auto-generated method stub
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    dismissDialogue(0);
                }
                return true;
            }
        });*/
        rlHide.setOnClickListener(this);


        playAnim.slideDownRl(llAnim);

        taskPrefViewModel = ShareModel.getI().taskPrefViewModel;
        //showHideLl(llTaskDetails);
        setTaskList();

        rgStatus.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.rbPlanned:
                        task_status = "Planned";
                        break;

                    case R.id.rbDoing:
                        task_status = "Doing";
                        break;

                    case R.id.rbDone:
                        task_status = "Done";
                        break;
                }
            }
        });


        setSortBySpinner();
        setData();


    }

    @BindView(R.id.llTask)
    LinearLayout llTask;


    @BindView(R.id.etTaskNm)
    TextInputEditText etTaskNm;

    @BindView(R.id.etTaskAmount)
    TextInputEditText etTaskAmount;

    @BindView(R.id.etActAmount)
    TextInputEditText etActAmount;


    @OnClick(R.id.tvSaveTask)
    public void tvSaveTask(View v) {

    }

    public void showToast(String msg) {
        Toast.makeText(c, msg + "", Toast.LENGTH_SHORT).show();
    }

    @BindView(R.id.ivRotate)
    ImageView ivRotate;

    @OnClick(R.id.llTask)
    public void llTask(View v) {
        showHideLl(llTaskDetails);
    }

    @BindView(R.id.llTaskDetails)
    LinearLayout llTaskDetails;

    @BindView(R.id.etActTitle)
    EditText etActTitle;

    public void showHideLl(LinearLayout linearLayout) {
        if (linearLayout.getVisibility() == View.VISIBLE) {
            linearLayout.setVisibility(View.GONE);
            ivRotate.setRotation(270);
        } else {
            linearLayout.setVisibility(View.VISIBLE);
            ivRotate.setRotation(0);

        }
    }


    public void dismissDialogue(final int call) {
        playAnim.slideUpRl(llAnim);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                listener.onDialogImageRunClick(call);
                dismiss();

            }
        }, 700);

    }

    PlayAnim playAnim = new PlayAnim();
    SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy");
    SimpleDateFormat dfEdit = new SimpleDateFormat("dd MMM yyyy hh:mm aaa");


    public interface OnDialogClickListener {
        void onDialogImageRunClick(int jsonObject);
    }


    public void showLoader(TextView textView) {
        textView.setText("");
        CircularProgressDrawable circularProgressDrawable = new CircularProgressDrawable(c);
        circularProgressDrawable.setStrokeWidth(5f);
        circularProgressDrawable.setCenterRadius(30f);
        circularProgressDrawable.start();
        textView.setBackground(circularProgressDrawable);
    }

    //TaskForPlanningListAdapter taskForPlanningListAdapter;

   /* @BindView(R.id.rvTask)
    RecyclerView rvTask;
    ArrayList<Plan> alTask = new ArrayList<>();
    int editdelPos = 0;*/

    public void setTaskList() {


/*        taskForPlanningListAdapter = new TaskForPlanningListAdapter(alTask, c, new TaskForPlanningListAdapter.OnItemClickListener() {
            @Override
            public void onEdit(int pos) {
                editdelPos = pos;
                setTextFieldValues(alTask.get(pos));
                tvSaveTask.setText("Update");
            }

            @Override
            public void onDelete(int pos) {
                editdelPos = pos;
                setTextFieldValues(alTask.get(pos));
                tvSaveTask.setText("Delete");

            }
        })*/
        ;


       /* FlowLayoutManager layoutManager = new FlowLayoutManager();
        layoutManager.setAutoMeasureEnabled(true);
        layoutManager.setAlignment(Alignment.LEFT);
        rvTask.setLayoutManager(layoutManager);

        rvTask.setItemAnimator(new DefaultItemAnimator());
        rvTask.setAdapter(taskForPlanningListAdapter);
        rvTask.setNestedScrollingEnabled(false);

        rvTask.setVisibility(View.VISIBLE);
*/
    }

    @BindView(R.id.rgStatus)
    RadioGroup rgStatus;
    @BindView(R.id.rbPlanned)
    RadioButton rbPlanned;
    @BindView(R.id.rbDoing)
    RadioButton rbDoing;
    @BindView(R.id.rbDone)
    RadioButton rbDone;

    public void setTextFieldValues(Plan plan) {
        etTaskNm.setText(plan.getTask_title());
        etTaskAmount.setText(plan.getTask_amount());
        //tvTaskDueDt.setText(plan.getTask_due_date());

        switch (plan.getTask_status().toLowerCase().trim()) {
            case "planned":
                rbPlanned.setChecked(true);
                break;
            case "doing":
                rbDoing.setChecked(true);
                break;
            case "done":
                rbDone.setChecked(true);
                break;
            default:
                rbPlanned.setChecked(true);
                break;

        }

    }

    //Calendar calActDue = Calendar.getInstance();
    //@BindView(R.id.tvActDueDt)
    //TextView tvActDueDt;

  /*  @OnClick(R.id.rlDelDt)
    public void rlDelDt(View v) {
        openDateView(tvActDueDt, calActDue);
    }*/


    public void openDateView(final TextView textView, final Calendar calendar) {

        DatePickerNew mDatePicker = new DatePickerNew(c, new DatePickerNew.PickDob() {
            @Override
            public void onSelect(Date date) {
                calendar.setTime(date);

                String date_ = df.format(date);
                textView.setText(date_);
            }
        }, textView.getText().toString().trim());
        mDatePicker.setStyle(DialogFragment.STYLE_NORMAL, R.style.CustomDialog);
        mDatePicker.show(c.getFragmentManager(), "Select date");
    }


    public void setTime(String plan_start_date, final Calendar calendar) {


        try {
            if (!plan_start_date.equals("")) {
                Date date_ = df.parse(plan_start_date);
                calendar.setTime(date_);

              /*  SimpleDateFormat timeformat = new SimpleDateFormat("hh:mm aaa");
                String str_time = timeformat.format(date_);
*/


            }
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvCancel:
                dismissDialogue(0);
                break;

            case R.id.tvAdd:

/*
                String actTitle = etActTitle.getText().toString().trim();
                String actDueDt = tvActDueDt.getText().toString().trim();

                if (actTitle.isEmpty()) {
                    showToast("Enter title field is required.");
                } else if (actDueDt.isEmpty()) {
                    showToast("Enter due date field is required");
                } else {
                    try {
                        joActivity.put("activity_title",actTitle);
                        joActivity.put("activity_due_date",actDueDt);
                        String task = new Gson().toJson(alTask);
                        joActivity.put("task_content",task);

                        if(tvAdd.getText().toString().equalsIgnoreCase("Edit"))
                        {

                        }
                        else {
                            addPlanning();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }


                break;
*/
            case R.id.rlHide:
                dismissDialogue(1);
                break;
            default:
                break;
        }
    }


    MyRequestCall myRequestCall = new MyRequestCall();
    MyRequst myRequst = new MyRequst();
    JsonObject joAddEdit = new JsonObject();
    JSONObject joFrom = new JSONObject(); // From should be his customer info
    JSONObject joActivity = new JSONObject(); // From should be his customer info
    //JSONObject joTo = new JSONObject(); // supplier ; In To, supplier , name address etc

    public void addPlanning() {
        showLoader(tvAdd);

        Chit chit = SummaryDetailFragmnet.item;
        try {

            SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy");
            joFrom.put("symbol_native", chit.getSymbol_native());
            joFrom.put("planning", joActivity);

            String date_ = "";
            switch (spn_sortby.getSelectedItemPosition()) {

                case 0:

                    date_ = dfEdit.format(calPlanStartDt.getTime());
                    if (!tvPlanStartDt.getText().toString().isEmpty()) {
                        joFrom.put("EDIT_DATE", date_); // // current date if plan start date is empty
                    } else {
                        joFrom.put("EDIT_DATE", "");
                    }

                    break;

                case 1:

                    date_ = dfEdit.format(calPlanEndDt.getTime());
                    if (!tvPlanEndDt.getText().toString().isEmpty()) {
                        joFrom.put("EDIT_DATE", date_); // // current date if plan start date is empty
                    } else {
                        joFrom.put("EDIT_DATE", "");
                    }

                    break;

                case 2:

                    date_ = dfEdit.format(calActStartDt.getTime());
                    if (!tvActStartDt.getText().toString().isEmpty()) {
                        joFrom.put("EDIT_DATE", date_); // // current date if plan start date is empty
                    } else {
                        joFrom.put("EDIT_DATE", "");
                    }


                    break;

                case 3:

                    date_ = dfEdit.format(calActEndDt.getTime());
                    if (!tvActEndDt.getText().toString().isEmpty()) {
                        joFrom.put("EDIT_DATE", date_); // // current date if plan start date is empty
                    } else {
                        joFrom.put("EDIT_DATE", "");
                    }

                    break;
            }


            joFrom.put("sort", Constants.SORT_PLAN); // Res
        } catch (JSONException e) {
            e.printStackTrace();
        }

        joAddEdit.addProperty("flag", "add");

        joAddEdit.addProperty("from_chit_hash_id", chit.getChit_hash_id());
        joAddEdit.addProperty("from_chit_id", chit.getChit_id());
        joAddEdit.addProperty("from_ref_id", chit.getRef_id());
        joAddEdit.addProperty("from_case_id", chit.getCase_id());
        joAddEdit.addProperty("from_purpose", chit.getPurpose());
        joAddEdit.addProperty("from_field_json_data", "{}");

        joAddEdit.addProperty("to_bridge_id", "0");
        joAddEdit.addProperty("to_chit_hash_id", "0");
        joAddEdit.addProperty("to_chit_id", "0");
        joAddEdit.addProperty("to_ref_id", "0");
        joAddEdit.addProperty("to_case_id", "0");

        joAddEdit.addProperty("to_field_json_data", joFrom.toString());


        joAddEdit.addProperty("task_purpose", "Planning");
        joAddEdit.addProperty("to_purpose", "");


        JsonObject joMrpObj = new JsonObject();
        JsonArray jsonElements = new JsonArray();
        jsonElements.add(joAddEdit);

        joMrpObj.add("newdata", jsonElements);

        myRequst.cookie = new SessionManager().getsession();

        myRequestCall.addTaskPreferencr(c, myRequst, joMrpObj, new MyRequestCall.CallRequest() {
            @Override
            public void onGetResponse(String response) {

                if (!response.isEmpty()) {

                } else {

                }

                tvAdd.setBackground(null);
                tvAdd.setBackgroundColor(ContextCompat.getColor(c, R.color.colorPrimary));
                tvAdd.setBackground(ContextCompat.getDrawable(c, R.drawable.roundcorner_color));
                tvAdd.setText("Add");

                dismissDialogue(2);

            }
        });


    }

    public void setData() {

        if (title.contains("Update")) {

            String forWhich = taskPrefViewModel.getToFieldJsonData();


            String planning = JsonObjParse.getValueEmpty(forWhich, "planning");
            symbol_native = JsonObjParse.getValueEmpty(forWhich, "symbol_native");
            String plan_start_date = JsonObjParse.getValueEmpty(planning, "plan_start_date");

            int sortby = JsonObjParse.getValueEmptyInt(planning, "sortby");

            spn_sortby.setSelection(sortby);


            if (!plan_start_date.isEmpty())
                setTime(plan_start_date, calPlanStartDt); // For sort used with time

            tvPlanStartDt.setText(plan_start_date);

            String plan_end_date = JsonObjParse.getValueEmpty(planning, "plan_end_date");
            tvPlanEndDt.setText(plan_end_date);

            String act_end_date = JsonObjParse.getValueEmpty(planning, "act_end_date");
            tvActEndDt.setText(act_end_date);

            String act_start_date = JsonObjParse.getValueEmpty(planning, "act_start_date");
            tvActStartDt.setText(act_start_date);

            String activity_title = JsonObjParse.getValueEmpty(planning, "task_title");
            String task_amount = JsonObjParse.getValueEmpty(planning, "plan_amount");
            etTaskNm.setText(activity_title + "");
            etTaskAmount.setText(task_amount + "");
            String act_amount = JsonObjParse.getValueEmpty(planning, "act_amount");
            etActAmount.setText(act_amount);


            tvAdd.setText("Edit");

            task_status = JsonObjParse.getValueEmpty(planning, "task_status");

            switch (task_status.toLowerCase()) {
                case "planned":
                    rbPlanned.setChecked(true);
                    break;
                case "doing":
                    rbDoing.setChecked(true);
                    break;
                case "done":
                    rbDone.setChecked(true);
                    break;
                default:
                    rbPlanned.setChecked(true);
                    break;

            }



            /*String task_content = JsonObjParse.getValueEmpty(planning, "task_content");

            Gson gson = new Gson();
            TypeToken<ArrayList<Plan>> token = new TypeToken<ArrayList<Plan>>() {
            };
            alTask = gson.fromJson(task_content, token.getType());

            if (alTask == null) alTask = new ArrayList<>();
            if (taskForPlanningListAdapter != null) taskForPlanningListAdapter.setitem(alTask);*/

        } else {
        }


    }

    String task_status = "Planned";

    @OnClick(R.id.tvAdd)
    public void tvAdd(View v) {
      /*  tvSaveTask.setText("Save");
        setTextFieldValues(new Plan("", "", "", ""));
        taskForPlanningListAdapter.setitem(alTask);*/


        String tsakNm = etTaskNm.getText().toString().trim();
        String tsakAmt = etTaskAmount.getText().toString().trim();
        String actAmt = etActAmount.getText().toString().trim();

        if (tsakNm.isEmpty()) {
            showToast("Enter task name field is required.");
        } else {

            /*if (tvSaveTask.getText().toString().equalsIgnoreCase("save")) {
                alTask.add(new Plan(tsakNm, tsakAmt, task_status, tsakDueDate));

            } else if (tvSaveTask.getText().toString().equalsIgnoreCase("update")) {
                alTask.set(editdelPos, new Plan(tsakNm, tsakAmt, task_status, tsakDueDate));

            } else if (tvSaveTask.getText().toString().equalsIgnoreCase("delete")) {
                alTask.remove(editdelPos);
            }*/

            //taskForPlanningListAdapter.setitem(alTask);
            //tvSaveTask.setText("Save");
            //setTextFieldValues(new Plan("", "", "", ""));

            try {
                joActivity.put("task_title", tsakNm);
                joActivity.put("plan_start_date", tvPlanStartDt.getText().toString().trim());
                joActivity.put("plan_end_date", tvPlanEndDt.getText().toString().trim());
                joActivity.put("act_start_date", tvActStartDt.getText().toString().trim());
                joActivity.put("act_end_date", tvActEndDt.getText().toString().trim());
                joActivity.put("plan_amount", tsakAmt);
                joActivity.put("act_amount", actAmt);
                joActivity.put("task_status", task_status);
                joActivity.put("sortby", spn_sortby.getSelectedItemPosition());

                if (title.contains("Update")) {
                    editPlanning();
                } else {
                    addPlanning(); // edit_date - plan start date -- current if empty
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

    }

    public void editPlanning() {

        final JsonObject joMrp = new JsonObject();
        JsonArray jsonElements = new JsonArray();

        try {
            joFrom.put("symbol_native", symbol_native);
            joFrom.put("planning", joActivity);

            /*final String date_ = dfEdit.format(calPlanStartDt.getTime()); // current date if plan start date is empty
            if (!tvPlanStartDt.getText().toString().isEmpty()) {
                joFrom.put("EDIT_DATE", date_); // // current date if plan start date is empty
            } else {
                joFrom.put("EDIT_DATE", "");
            }*/

            String date_ = "";
            switch (spn_sortby.getSelectedItemPosition()) {
                case 0:

                    date_ = dfEdit.format(calPlanStartDt.getTime());
                    if (!tvPlanStartDt.getText().toString().isEmpty()) {
                        joFrom.put("EDIT_DATE", date_); // // current date if plan start date is empty
                    } else {
                        joFrom.put("EDIT_DATE", "");
                    }

                    break;

                case 1:

                    date_ = dfEdit.format(calPlanEndDt.getTime());
                    if (!tvPlanEndDt.getText().toString().isEmpty()) {
                        joFrom.put("EDIT_DATE", date_); // // current date if plan start date is empty
                    } else {
                        joFrom.put("EDIT_DATE", "");
                    }

                    break;

                case 2:

                    date_ = dfEdit.format(calActStartDt.getTime());
                    if (!tvActStartDt.getText().toString().isEmpty()) {
                        joFrom.put("EDIT_DATE", date_); // // current date if plan start date is empty
                    } else {
                        joFrom.put("EDIT_DATE", "");
                    }


                    break;

                case 3:

                    date_ = dfEdit.format(calActEndDt.getTime());
                    if (!tvActEndDt.getText().toString().isEmpty()) {
                        joFrom.put("EDIT_DATE", date_); // // current date if plan start date is empty
                    } else {
                        joFrom.put("EDIT_DATE", "");
                    }

                    break;
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

        joAddEdit = new JsonObject();
        joAddEdit.addProperty("task_reference_id", taskPrefViewModel.getTaskReferenceId());
        joAddEdit.addProperty("to_field_json_data", joFrom.toString());

        jsonElements.add(joAddEdit);

        joMrp.add("olddata", jsonElements);

        MyRequestCall myRequestCall = new MyRequestCall();
        MyRequst myRequst = new MyRequst();
        myRequst.cookie = new SessionManager().getsession();

        myRequestCall.editTaskPreferencr(c, myRequst, joMrp, new MyRequestCall.CallRequest() {
            @Override
            public void onGetResponse(String response) {
                if (!response.isEmpty()) {

                    tvAdd.setBackground(null);
                    tvAdd.setBackgroundColor(ContextCompat.getColor(c, R.color.colorPrimary));
                    tvAdd.setBackground(ContextCompat.getDrawable(c, R.drawable.roundcorner_color));
                    tvAdd.setText("Edit");

                    dismissDialogue(2);


                }
            }
        });
    }

    Calendar calPlanStartDt = Calendar.getInstance();
    Calendar calPlanEndDt = Calendar.getInstance();
    Calendar calActStartDt = Calendar.getInstance();
    Calendar calActEndDt = Calendar.getInstance();
    @BindView(R.id.tvPlanStartDt)
    TextView tvPlanStartDt;
    @BindView(R.id.tvPlanEndDt)
    TextView tvPlanEndDt;
    @BindView(R.id.tvActStartDt)
    TextView tvActStartDt;
    @BindView(R.id.tvActEndDt)
    TextView tvActEndDt;

    @OnClick(R.id.rlPlanStartDt)
    public void rlPlanStartDt(View v) {
        openDateView(tvPlanStartDt, calPlanStartDt);
    }

    @OnClick(R.id.rlPlanEndDt)
    public void rlPlanEndDt(View v) {
        openDateView(tvPlanEndDt, calPlanEndDt);
    }

    @OnClick(R.id.rlActStartDt)
    public void rlActStartDt(View v) {
        openDateView(tvActStartDt, calActStartDt);
    }

    @OnClick(R.id.rlActEndDt)
    public void rlActEndDt(View v) {
        openDateView(tvActEndDt, calActEndDt);
    }


    @BindView(R.id.spn_sortby)
    Spinner spn_sortby;
    AutoCompleteTv autoCompleteTv = new AutoCompleteTv();
    ArrayList<AutoCompleteTv> alTractions = new ArrayList<>();
    int tractionId = 0;
    String type = "";

    public void setSortBySpinner() {

        alTractions = new ArrayList<>();
        alTractions.add(new AutoCompleteTv("Plan start date", 0));
        alTractions.add(new AutoCompleteTv("Plan end date", 1));
        alTractions.add(new AutoCompleteTv("Actual start date", 2));
        alTractions.add(new AutoCompleteTv("Actual end date", 3));


        autoCompleteTv.setautofill(c, spn_sortby, alTractions);
        spn_sortby.setSelection(tractionId);

        spn_sortby.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
}


