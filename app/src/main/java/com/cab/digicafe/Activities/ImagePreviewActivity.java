package com.cab.digicafe.Activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.cab.digicafe.Activities.player.PlayerActivity;
import com.cab.digicafe.Adapter.ImgPrevAdapter;
import com.cab.digicafe.Fragments.CatalougeContent;
import com.cab.digicafe.Helper.DragTouchHelper;
import com.cab.digicafe.Helper.SessionManager;
import com.cab.digicafe.Model.ModelFile;
import com.cab.digicafe.Model.ShareModel;
import com.cab.digicafe.R;
import com.cab.digicafe.Rest.ApiClient;
import com.cab.digicafe.Rest.ApiInterface;
import com.cab.digicafe.Rest.Request.CatRequest;
import com.cab.digicafe.collageviews.MultiTouchListener;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.lang.reflect.Type;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ImagePreviewActivity extends MyAppBaseActivity {

    ViewPager viewPager;
    int item = 0;
    ArrayList<ModelFile> al_img = new ArrayList<>();


    @BindView(R.id.rv_selectfile)
    RecyclerView rv_selectfile;
    @BindView(R.id.tvImgTitle)
    TextView tvImgTitle;

    ImgPrevAdapter imgPrevAdapter;

    ModelFile modelFile;
    int prevPos = 0;
    boolean isdrag = false;
    CatalougeContent catalougeContent;
    String str_model = "";
    String img_title = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_preview);

        ButterKnife.bind(this);

        findViewById(R.id.iv_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        sessionManager = new SessionManager(this);

        if (getIntent().getExtras().containsKey("CatalougeContent")) {
            str_model = getIntent().getExtras().getString("CatalougeContent");
            catalougeContent = new Gson().fromJson(str_model, CatalougeContent.class);
        }

        if (getIntent().getExtras().containsKey("img_title")) {
            img_title = getIntent().getExtras().getString("img_title");
            tvImgTitle.setText(img_title);
            tvImgTitle.setText(1 + "/" + al_img.size() + "  " + img_title);
        } else {
            img_title = getString(R.string.app_name);
            tvImgTitle.setText(1 + "/" + al_img.size() + "  " + img_title);
        }


        item = getIntent().getIntExtra("pos", 0);
        String imgarray = getIntent().getStringExtra("imgarray");
        isdrag = getIntent().getBooleanExtra("isdrag", false);

        Type type = new TypeToken<ArrayList<ModelFile>>() {
        }.getType();

        al_img = new Gson().fromJson(imgarray, type);


        viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.setAdapter(new ViewPagerAdapter(this, al_img));
        viewPager.setCurrentItem(item);
        tvImgTitle.setText(viewPager.getCurrentItem() + 1 + "/" + al_img.size() + "  " + img_title);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rv_selectfile.setLayoutManager(mLayoutManager);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {
                tvImgTitle.setText(viewPager.getCurrentItem() + 1 + "/" + al_img.size() + "  " + img_title);
            }

            @Override
            public void onPageSelected(int i) {

            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });


        imgPrevAdapter = new ImgPrevAdapter(al_img, this, new ImgPrevAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int pos) {
                viewPager.setCurrentItem(pos);
                tvImgTitle.setText(viewPager.getCurrentItem() + 1 + "/" + al_img.size() + "  " + img_title);
            }

            @Override
            public void onDelete(int pos) {


            }

            @Override
            public void onLongClick(View v) {

            }

        }, isdrag);


        rv_selectfile.setAdapter(imgPrevAdapter);
        rv_selectfile.scrollToPosition(item);
        findViewById(R.id.iv_save_img).setVisibility(View.GONE);
        if (isdrag) {
            ItemTouchHelper.Callback callback = new DragTouchHelper(imgPrevAdapter);
            ItemTouchHelper helper = new ItemTouchHelper(callback);
            helper.attachToRecyclerView(rv_selectfile);
            findViewById(R.id.iv_save_img).setVisibility(View.VISIBLE);
        }


        findViewById(R.id.iv_save_img).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Gson gson = new GsonBuilder().create();
                String json = gson.toJson(catalougeContent);// obj is your object


                try {

                    JSONObject jo = new JSONObject();
                    JSONArray ja_new = new JSONArray();
                    jo.put("newdata", ja_new);
                    JSONArray ja_old = new JSONArray();


                    JSONObject jsonObj = new JSONObject(json);

                    JsonArray ja_img = new JsonArray();
                    for (int i = 0; i < al_img.size(); i++) {
                        ja_img.add(al_img.get(i).getImgpath());
                    }

                    jsonObj.put("image_json_data", ja_img);


                    //jsonObj.put("field_json_data",new JSONObject());
                    jsonObj.put("flag", "edit");
                    ja_old.put(jsonObj);
                    jo.put("olddata", ja_old);

                    CatRequest catalougeContent = new CatRequest();

                    catalougeContent = new Gson().fromJson(jo.toString(), CatRequest.class);

                    putData(catalougeContent);
                } catch (JSONException e) {
                    e.printStackTrace();
                    findViewById(R.id.rl_pb_cat).setVisibility(View.GONE);
                }


            }
        });


    }


    public class ViewPagerAdapter extends PagerAdapter {
        private ArrayList<ModelFile> data;
        private LayoutInflater inflater = null;
        Context context;

        public ViewPagerAdapter(Context context, ArrayList<ModelFile> d) {
            this.context = context;
            data = d;
        }

        @Override
        public int getCount() {
            return data.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((RelativeLayout) object);
        }


        @Override
        public Object instantiateItem(ViewGroup container, final int position) {
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View itemView = inflater.inflate(R.layout.zoom_item, container, false);

            ImageView photoView = (ImageView) itemView.findViewById(R.id.collageView);
            //ImageView ivVdo = (ImageView) itemView.findViewById(R.id.ivVdo);
            RelativeLayout rlVdo = (RelativeLayout) itemView.findViewById(R.id.rlVdo);
            LinearLayout llLocInfo = (LinearLayout) itemView.findViewById(R.id.llLocInfo);
            TextView tvLocInfo = (TextView) itemView.findViewById(R.id.tvLocInfo);

            ModelFile modelFile = data.get(position);

            photoView.setOnTouchListener(new MultiTouchListener());
            rlVdo.setVisibility(View.GONE);
            if (modelFile.getImgpath().contains("youtube")) {

                Uri uri = Uri.parse(modelFile.getImgpath());
                //Set<String> args = uri.getQueryParameterNames();
                String v = uri.getQueryParameter("v");

                rlVdo.setVisibility(View.VISIBLE);
                String url = "" + getString(R.string.utube_base) + v + getString(R.string.utube_end);
                Log.e("okhttp utube", url + "");
                Glide.with(context).load(url).asBitmap().placeholder(R.drawable.placeholder_image).diskCacheStrategy(DiskCacheStrategy.ALL).into(photoView);

            } else if (modelFile.isIsoffline()) {
                Glide.with(context).load(new File(modelFile.getImgpath())).asBitmap().placeholder(R.drawable.placeholder_image).diskCacheStrategy(DiskCacheStrategy.ALL).into(photoView);
            } else {
                Glide.with(context).load(modelFile.getImgpath()).asBitmap().placeholder(R.drawable.placeholder_image).diskCacheStrategy(DiskCacheStrategy.ALL).into(photoView);
            }

            tvLocInfo.setText("");
            llLocInfo.setVisibility(View.GONE);
            if (modelFile.getImgInfo() != null && modelFile.getImgInfo().length() > 0) {
                tvLocInfo.setText(modelFile.getImgInfo() + "");
                llLocInfo.setVisibility(View.VISIBLE);
            }

            rlVdo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ModelFile modelFile = data.get(position);
                    if (modelFile.getImgpath().contains("youtube")) {
                        Uri uri = Uri.parse(modelFile.getImgpath());
                        String v = uri.getQueryParameter("v");
                        ShareModel.getI().utubeId = v;
                        context.startActivity(new Intent(context, PlayerActivity.class));

                    }
                }
            });


            // Add viewpager_item.xml to ViewPager
            ((ViewPager) container).addView(itemView);

            return itemView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            // Remove viewpager_item.xml from ViewPager
            ((ViewPager) container).removeView((RelativeLayout) object);

        }
    }


    @Override
    public void onBackPressed() {


        if (isdrag) {
           /* Intent i = new Intent();
            String paths = new Gson().toJson(al_img);
            i.putExtra("al_selet",paths);
            setResult(RESULT_OK,i);
            finish();*/
        } else {
            //  super.onBackPressed();
        }

        super.onBackPressed();

    }

    SessionManager sessionManager;

    public void putData(CatRequest jo_cat) {

        findViewById(R.id.rl_pb_cat).setVisibility(View.VISIBLE);

        ApiInterface apiService =
                ApiClient.getClient(ImagePreviewActivity.this).create(ApiInterface.class);

        Call call = apiService.PutCatalogue(sessionManager.getsession(), jo_cat);

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                findViewById(R.id.rl_pb_cat).setVisibility(View.GONE);

                if (response.isSuccessful()) {
                    try {
                        //ll_edit.setVisibility(View.VISIBLE);
                        //ll_save.setVisibility(View.GONE);
                        //edit();

                        Intent intent = new Intent();
                        setResult(Activity.RESULT_OK, intent);

                        Toast.makeText(ImagePreviewActivity.this, "Saved !", Toast.LENGTH_LONG).show();

                    } catch (Exception e) {


                    }

                } else {

                    try {
                    } catch (Exception e) {
                        Toast.makeText(ImagePreviewActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }

            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // Log error here since request failed
                findViewById(R.id.rl_pb_cat).setVisibility(View.GONE);

            }
        });
    }


}
