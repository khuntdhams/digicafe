package com.cab.digicafe.Activities.healthTemplate;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ExifInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.cab.digicafe.Activities.CatalougeTabActivity;
import com.cab.digicafe.Activities.ImagePreviewActivity;
import com.cab.digicafe.Adapter.TemplateAdapter;
import com.cab.digicafe.CatelogActivity;
import com.cab.digicafe.Dialogbox.DialogLoading;
import com.cab.digicafe.Helper.SessionManager;
import com.cab.digicafe.Model.Catelog;
import com.cab.digicafe.Model.ModelFile;
import com.cab.digicafe.Model.SupplierCategory;
import com.cab.digicafe.Model.templateModel;
import com.cab.digicafe.MyCustomClass.JsonObjParse;
import com.cab.digicafe.MyCustomClass.RefreshSession;
import com.cab.digicafe.R;
import com.cab.digicafe.Rest.ApiClient;
import com.cab.digicafe.Rest.ApiInterface;
import com.cab.digicafe.photopicker.activity.PickImageActivity;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Headers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.media.MediaRecorder.VideoSource.CAMERA;

public class SelectTemplateActivity extends AppCompatActivity implements SwipyRefreshLayout.OnRefreshListener, TemplateAdapter.OnItemClickListener {

    @BindView(R.id.rvTemplateList)
    RecyclerView rvTemplateList;

    ArrayList<templateModel> templateList = new ArrayList();
    TemplateAdapter adapter;

    @BindView(R.id.iv_back)
    ImageView iv_back;

    MyDoctorData myDoctorData;
    SessionManager sessionManager;
    String bridgeidval;
    String frombridgeidval;
    SwipyRefreshLayout mSwipyRefreshLayout;
    boolean isdataavailable;
    private List<Catelog> productlist = new ArrayList<>();
    ArrayList<String> selectedproductlist;
    ArrayList<Catelog> selectedproduct;
    ArrayList<SupplierCategory> alCategory = new ArrayList<>();
    String sessionString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_templet);
        ButterKnife.bind(this);

        sessionManager = new SessionManager(this);

        myDoctorData = new MyDoctorData();
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        mSwipyRefreshLayout = (SwipyRefreshLayout) findViewById(R.id.swipyrefreshlayout);
        mSwipyRefreshLayout.setOnRefreshListener(this);

        sessionString = sessionManager.getsession();

        isdataavailable = true;
        if (getString(R.string.BASEURL).equalsIgnoreCase(getString(R.string.PRODURL))) {
            frombridgeidval = "ac3ae4180e8dd2405fac7ed8f6863184";
            bridgeidval = "ac3ae4180e8dd2405fac7ed8f6863184";
        } else {
            frombridgeidval = "cbe1289de7323892a2786c910014d41b";
            bridgeidval = "cbe1289de7323892a2786c910014d41b";
        }

        selectedproduct = new ArrayList<>();
        selectedproductlist = new ArrayList<>();

        adapter = new TemplateAdapter(myDoctorData.getTemplateList(), this, this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        rvTemplateList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        rvTemplateList.setAdapter(adapter);


        usersignin(sessionString, 1, "");

    }

    public void usersignin(String usersession, final int pagecount, final String searchquery) {
        ApiInterface apiService =
                ApiClient.getClient(SelectTemplateActivity.this).create(ApiInterface.class);
        Log.e("sess", sessionManager.getAggregatorprofile());

        Call call = null;

        call = apiService.getCatalougelist(usersession, sessionManager.getAggregatorprofile(), frombridgeidval, bridgeidval, pagecount, searchquery, "INR", "");

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                int statusCode = response.code();
                Log.e("test", " trdds");

                Headers headerList = response.headers();
                Log.d("testcatelogue", statusCode + " ");

                DialogLoading.dialogLoading_Dismiss();
                //  String cookie = response.;
                //  Log.d("test", cookie+ " ");

                mSwipyRefreshLayout.setRefreshing(false);
                if (response.isSuccessful()) {
                    try {
                        String a = new Gson().toJson(response.body());
                        Log.e("catalogresponse", a);
                        JSONObject jObjRes = new JSONObject(a);
                        JSONObject jObjdata = jObjRes.getJSONObject("response");
                        JSONObject jObjResponse = jObjdata.getJSONObject("data");
                        String totalRecord = jObjResponse.getString("totalRecord");
                        JSONArray jObjcontacts = jObjResponse.getJSONArray("content");
                        String displayEndRecord = jObjResponse.getString("displayEndRecord");
                        if (totalRecord.equals(displayEndRecord)) {
                            isdataavailable = false;
                        } else {
                            isdataavailable = true;
                        }

                        if (pagecount == 1 && searchquery.length() > 0) {
                            productlist = new ArrayList<Catelog>();
                        }

                        boolean isSurvey = false;

                        for (int i = 0; i < jObjcontacts.length(); i++) {
                            if (jObjcontacts.get(i) instanceof JSONObject) {
                                JSONObject jsnObj = (JSONObject) jObjcontacts.get(i);
                                Catelog obj = new Gson().fromJson(jsnObj.toString(), Catelog.class);
                                if (selectedproductlist.contains(String.valueOf(obj.getEntry_id()))) {
                                    int a1 = selectedproductlist.indexOf(String.valueOf(obj.getEntry_id()));
                                    Catelog objval = selectedproduct.get(a1);
                                    obj.setCartcount(objval.getCartcount());
                                }

                                if (isSurvey) {
                                    try {
                                        //String survey = JsonObjParse.getValueEmpty(obj.getInternal_json_data(), "survey");
                                        //String surveyQPos = JsonObjParse.getValueEmpty(survey, "SurveyQPos");
                                        if (obj.getCross_reference().isEmpty()) obj.sortOrder = 0;
                                        else
                                            obj.sortOrder = Integer.parseInt(obj.getCross_reference());
                                    } catch (NumberFormatException e) {
                                        e.printStackTrace();
                                    }

                                }
                                Log.e("alias", obj.getName());
                                productlist.add(obj);

                            }
                        }
                        for (int j = 0; j < myDoctorData.templateList.size(); j++) {

                            JSONObject job = new JSONObject();
                            job.put("doctor_template_type", myDoctorData.templateList.get(j).getTemplateName());
                            if (productlist.get(productlist.indexOf(new Catelog(job.toString()))).getImageArr().size() != 0) {
                                myDoctorData.templateList.get(j).setImage(productlist.get(productlist.indexOf(new Catelog(job.toString()))).getImageArr().get(0));
                            }

                        }
                        adapter.notifyDataSetChanged();


                        //mAdapter.notifyDataSetChanged();
                    } catch (Exception e) {
                        Toast.makeText(SelectTemplateActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                    //  Intent i = new Intent(MainActivity.this, MainActivity.class);
                    //  startActivity(i);
                } else {
                    Log.e("test", " trdds1");

                    switch (response.code()) {
                        case 401:
                            new RefreshSession(SelectTemplateActivity.this, sessionManager);  // session expired
                            break;
                        case 404:
                            Toast.makeText(SelectTemplateActivity.this, "not found", Toast.LENGTH_SHORT).show();
                            break;
                        case 500:
                            Toast.makeText(SelectTemplateActivity.this, "server broken", Toast.LENGTH_SHORT).show();
                            break;
                        default:
                            Toast.makeText(SelectTemplateActivity.this, "unknown error", Toast.LENGTH_SHORT).show();
                            break;
                    }


                    try {
                       /* JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Toast.makeText(MainActivity.this, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();*/
                    } catch (Exception e) {
                        Toast.makeText(SelectTemplateActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }

                //  List<Movie> movies = response.body().getResults();
                //  recyclerView.setAdapter(new MoviesAdapter(movies, R.layout.list_item_movie, getApplicationContext()));
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // Log error here since request failed
                DialogLoading.dialogLoading_Dismiss();
                Log.e("error message", t.toString());
            }
        });
    }

    @Override
    public void onRefresh(SwipyRefreshLayoutDirection direction) {
        mSwipyRefreshLayout.setRefreshing(false);

        if (direction == SwipyRefreshLayoutDirection.TOP) {
            isdataavailable = true;
            Log.e("scroll direction", "top");
            productlist = new ArrayList<>();

            usersignin(sessionString, 1, "");
        } else if (direction == SwipyRefreshLayoutDirection.BOTTOM) {
            Log.e("scroll direction", "bottom");
            if (isdataavailable) {

                usersignin(sessionString, 1, "");
            } else {
                Toast.makeText(this, "No more data available", Toast.LENGTH_LONG).show();
            }

        }
    }

    @Override
    public void onImgClick(int pos) {

        ArrayList<ModelFile> al_img = new ArrayList<>();
        try {
            JSONObject job = new JSONObject();
            job.put("doctor_template_type", myDoctorData.templateList.get(pos).getTemplateName());
            if (productlist.get(productlist.indexOf(new Catelog(job.toString()))).getImageArr().size() != 0) {
                //  imgarray = new Gson().toJson(productlist.get(productlist.indexOf(new Catelog(job.toString()))).getImageArr());
                try {
                    String imageJsonData = productlist.get(productlist.indexOf(new Catelog(job.toString()))).getImage_json_data();
                    JSONArray ja_img = new JSONArray(imageJsonData);
                    for (int i = 0; i < ja_img.length(); i++) {
                        al_img.add(new ModelFile(ja_img.get(i).toString(), false));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String imgarray = new Gson().toJson(al_img);

        String json = "";

        try {
            Gson gson = new Gson();
            JSONObject job = new JSONObject();
            job.put("doctor_template_type", myDoctorData.templateList.get(pos).getTemplateName());
            json = gson.toJson(productlist.get(productlist.indexOf(new Catelog(job.toString()))));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Intent intent = new Intent(SelectTemplateActivity.this, ImagePreviewActivity.class);
        intent.putExtra("imgarray", imgarray);
        intent.putExtra("pos", 0);
        intent.putExtra("isdrag", true);
        intent.putExtra("CatalougeContent", json);

        startActivityForResult(intent, 21);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to ak aris
        if (requestCode == 21) {
           /* String picturePath = data.getExtras().getString("al_selet");
            ArrayList<ModelFile> al_temp_selet = new Gson().fromJson(picturePath, new TypeToken<ArrayList<ModelFile>>() {
            }.getType());
            al_temp_selet.addAll(al_selet);
            selectFileAdapter.setItem(al_temp_selet);
            al_selet = new ArrayList<>();
            al_selet.addAll(al_temp_selet);*/

            if (resultCode == RESULT_OK) {
                /*findViewById(R.id.rl_pb_cat).setVisibility(View.VISIBLE);
                pagecount = 1;
                usersignin(pagecount);*/
            }

        }

        super.onActivityResult(requestCode, resultCode, data);

    }


}
