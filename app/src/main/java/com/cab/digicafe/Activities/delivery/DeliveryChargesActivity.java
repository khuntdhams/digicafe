package com.cab.digicafe.Activities.delivery;

import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cab.digicafe.Activities.MyAppBaseActivity;
import com.cab.digicafe.Activities.RequestApis.MyRequestCall;
import com.cab.digicafe.Activities.RequestApis.MyRequst;
import com.cab.digicafe.Adapter.PlaceArrayAdapter;
import com.cab.digicafe.Adapter.taxGroup.TaxGroupModel;
import com.cab.digicafe.ChitlogActivity;
import com.cab.digicafe.Database.SqlLiteDbHelper;
import com.cab.digicafe.DetailsFragment;
import com.cab.digicafe.DetePickerFragment;
import com.cab.digicafe.Fragments.SummaryDetailFragmnet;
import com.cab.digicafe.Helper.Constants;
import com.cab.digicafe.Helper.Inad;
import com.cab.digicafe.Helper.SessionManager;
import com.cab.digicafe.Helper.SharedPrefUserDetail;
import com.cab.digicafe.Model.Catelog;
import com.cab.digicafe.Model.Chit;
import com.cab.digicafe.Model.Metal;
import com.cab.digicafe.Model.ShareModel;
import com.cab.digicafe.Model.SupplierNew;
import com.cab.digicafe.Model.Userprofile;
import com.cab.digicafe.MyCustomClass.JsonObjParse;
import com.cab.digicafe.MyCustomClass.MyCallBkCommon;
import com.cab.digicafe.R;
import com.cab.digicafe.Rest.ApiClient;
import com.cab.digicafe.Rest.ApiInterface;
import com.cab.digicafe.TimePicker;
import com.cab.digicafe.Timeline.DateTimeUtils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.kofigyan.stateprogressbar.StateProgressBar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.cab.digicafe.Activities.mrp.MrpFragmnet.vPos;

public class DeliveryChargesActivity extends MyAppBaseActivity implements View.OnClickListener, DetePickerFragment.OnItemClickListener, TimePicker.OnItemTimerClickListener {

    @BindView(R.id.iv_back)
    ImageView iv_back;
    String currency = "";

    @BindView(R.id.tvTitle)
    TextView tvTitle;

    String purpose = "";
    SupplierNew item;

    @BindView(R.id.etFromName)
    TextInputEditText etFromName;
    @BindView(R.id.etFromAddress)
    TextInputEditText etFromAddress;
    @BindView(R.id.etFromContactNo)
    TextInputEditText etFromContactNo;
    //etFromContactNo
    @BindView(R.id.etToName)
    TextInputEditText etToName;
    @BindView(R.id.etToAddress)
    TextInputEditText etToAddress;
    @BindView(R.id.etToContactNo)
    TextInputEditText etToContactNo;

    @BindView(R.id.tv_date)
    TextView tv_date;
    @BindView(R.id.tv_time)
    TextView tv_time;
    @BindView(R.id.iv_date)
    ImageView iv_date;
    @BindView(R.id.iv_time)
    ImageView iv_time;

    @BindView(R.id.etCalDistance)
    TextView etCalDistance;
    @BindView(R.id.tvSubmit)
    TextView tvSubmit;
    @BindView(R.id.etCharges)
    TextView etCharges;

    String sessionString;
    String delivery_charge;
    double charge;

    @BindView(R.id.llStepOne)
    LinearLayout llStepOne;
    @BindView(R.id.llStepSecond)
    LinearLayout llStepSecond;
    @BindView(R.id.llStepThird)
    LinearLayout llStepThird;
    @BindView(R.id.llStepFour)
    LinearLayout llStepFour;
    @BindView(R.id.llStepFive)
    RelativeLayout llStepFive;

    @BindView(R.id.tvNextOne)
    TextView tvNextOne;
    @BindView(R.id.tvNextSecond)
    TextView tvNextSecond;
    @BindView(R.id.tvNextThird)
    TextView tvNextThird;
    @BindView(R.id.tvNextFour)
    TextView tvNextFour;

    @BindView(R.id.cbToAddress)
    CheckBox cbToAddress;

    String ref_id = "";
    String case_id = "";

    @BindView(R.id.usage_stateprogressbar)
    StateProgressBar usage_stateprogressbar;

    @BindView(R.id.tvFromName)
    TextView tvFromName;
    @BindView(R.id.tvFromContact)
    TextView tvFromContact;
    @BindView(R.id.tvFromAddress)
    TextView tvFromAddress;
    @BindView(R.id.tvToName)
    TextView tvToName;
    @BindView(R.id.tvToContact)
    TextView tvToContact;
    @BindView(R.id.tvToAddress)
    TextView tvToAddress;
    @BindView(R.id.tvRefId)
    TextView tvRefId;
    @BindView(R.id.tvDate)
    TextView tvDate;
    @BindView(R.id.tvTime)
    TextView tvTime;
    @BindView(R.id.tvDistance)
    TextView tvDistance;
    @BindView(R.id.tvCharge)
    TextView tvCharge;
    @BindView(R.id.tvFrom)
    TextView tvFrom;
    @BindView(R.id.tvTo)
    TextView tvTo;
    @BindView(R.id.ivFromMap)
    ImageView ivFromMap;


    @BindView(R.id.tv_p_cur)
    TextView tv_p_cur;
    @BindView(R.id.et_mrp)
    EditText et_mrp;
    @BindView(R.id.tv_calc_dis)
    TextView tv_calc_dis;
    @BindView(R.id.llDiscBlck)
    LinearLayout llDiscBlck;
    @BindView(R.id.tv_dp_cur)
    TextView tv_dp_cur;
    @BindView(R.id.et_dis_val)
    EditText et_dis_val;
    @BindView(R.id.et_dis_per)
    EditText et_dis_per;
    @BindView(R.id.tvTotalDiscPerc)
    TextView tvTotalDiscPerc;
    @BindView(R.id.tvTotalWoTax)
    TextView tvTotalWoTax;
    @BindView(R.id.llCgst)
    LinearLayout llCgst;
    @BindView(R.id.tvTaxField1)
    TextView tvTaxField1;
    @BindView(R.id.et_cgst)
    EditText et_cgst;
    @BindView(R.id.tv_calc_ctax)
    TextView tv_calc_ctax;
    @BindView(R.id.llSgst)
    LinearLayout llSgst;
    @BindView(R.id.tvTaxField2)
    TextView tvTaxField2;
    @BindView(R.id.et_sgst)
    EditText et_sgst;
    @BindView(R.id.tv_calc_stax)
    TextView tv_calc_stax;
    @BindView(R.id.tv_total)
    TextView tv_total;
    ArrayList<TaxGroupModel> alTax = new ArrayList<>();
    SqlLiteDbHelper dbHelper;
    @BindView(R.id.rvAllDetail)
    RelativeLayout rvAllDetail;

    boolean isFromMrp = false;

    AutoCompleteTextView tv_google_loc;
    AutoCompleteTextView tv_google_loc_to;

    Double fromLatitude = 0.0, fromLongitude = 0.0;
    Double toLatitude = 0.0, toLongitude = 0.0;
    String locDistance = "";

    @BindView(R.id.etReference)
    TextInputEditText etReference;

    @BindView(R.id.etRecAmount)
    TextView etRecAmount;

    @BindView(R.id.llRecAmount)
    LinearLayout llRecAmount;
    @BindView(R.id.tvRecAmount)
    TextView tvRecAmount;
    @BindView(R.id.llAmount)
    RelativeLayout llAmount;
    @BindView(R.id.tvFinalTotal)
    TextView tvFinalTotal;
    @BindView(R.id.cbAmount)
    CheckBox cbAmount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivery_charges);
        ButterKnife.bind(this);

        sessionManager = new SessionManager(this);

        currency = SharedPrefUserDetail.getString(this, SharedPrefUserDetail.chit_symbol_native, "") + " ";

        tv_google_loc = (AutoCompleteTextView) findViewById(R.id.tv_google_loc);
        tv_google_loc_to = (AutoCompleteTextView) findViewById(R.id.tv_google_loc_to);

        if (vPos == 1) {
            //joAddEdit.addProperty("task_purpose", "Material");
        } else if (vPos == 4) {
            //joAddEdit.addProperty("task_purpose", "Delivery");
        }

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        dbHelper = new SqlLiteDbHelper(this);

        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {

            } else {
                sessionString = extras.getString("session");
                purpose = extras.getString("purpose");
                delivery_charge = extras.getString("delivery_charge");
                frombridgeidval = extras.getString("frombridgeidval");
                String supplier = extras.getString("supplier");
                isFromMrp = extras.getBoolean("isFromMrp");
                item = new Gson().fromJson(supplier, SupplierNew.class);
                tvTitle.setText("Service");
                if (purpose.equalsIgnoreCase(getString(R.string.delivery))) {
                    tvTitle.setText(getString(R.string.delivery));
                }
            }
        }
        tvFrom.setText("From");
        tvTo.setText("To");

        iv_date.setOnClickListener(this);
        iv_time.setOnClickListener(this);

        tvSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                callCreateChit(DeliveryChargesActivity.this, new CallRequest() {
                    @Override
                    public void onGetResponse(String response) {

                    }
                });
            }
        });

        if (isFromMrp) {
            fillDetails();
            llRecAmount.setVisibility(View.VISIBLE);
            llAmount.setVisibility(View.VISIBLE);

        } else {
            llRecAmount.setVisibility(View.GONE);
            llAmount.setVisibility(View.GONE);
            tvSubmit.setVisibility(View.GONE);
        }
        tvNextOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(etFromName.getText().toString())) {
                    Toast.makeText(DeliveryChargesActivity.this, "Enter Name", Toast.LENGTH_SHORT).show();
                } else if (TextUtils.isEmpty(etFromContactNo.getText().toString())) {
                    Toast.makeText(DeliveryChargesActivity.this, "Enter Contact No.", Toast.LENGTH_SHORT).show();
                } else if (TextUtils.isEmpty(tv_google_loc.getText().toString())) {
                    Toast.makeText(DeliveryChargesActivity.this, "Enter Address", Toast.LENGTH_SHORT).show();
                } else {
                    Geocoder coder = new Geocoder(DeliveryChargesActivity.this);
                    List<Address> address;
                    LatLng p1 = null;

                    try {
                        // May throw an IOException
                        address = coder.getFromLocationName(tv_google_loc.getText().toString(), 1);
                        if (address.size() != 0) {
                            Address location = address.get(0);
                            fromLatitude = location.getLatitude();
                            fromLongitude = location.getLongitude();
                            p1 = new LatLng(location.getLatitude(), location.getLongitude());

                            if (fromLatitude == 0.0 || fromLongitude == 0.0) {
                                Toast.makeText(DeliveryChargesActivity.this, "Please Enter Valid Address", Toast.LENGTH_SHORT).show();
                            } else {
                                llStepOne.setVisibility(View.GONE);
                                llStepSecond.setVisibility(View.VISIBLE);
                                llStepThird.setVisibility(View.GONE);
                                llStepFour.setVisibility(View.GONE);
                                llStepFive.setVisibility(View.GONE);
                                usage_stateprogressbar.setCurrentStateNumber(StateProgressBar.StateNumber.TWO);
                            }
                        } else {
                            Toast.makeText(DeliveryChargesActivity.this, "Please Enter Valid Address", Toast.LENGTH_SHORT).show();
                        }

                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                    //getLocationFromAddress(DeliveryChargesActivity.this, tv_google_loc.getText().toString());
                }
            }
        });

        tvNextSecond.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(etToName.getText().toString())) {
                    Toast.makeText(DeliveryChargesActivity.this, "Enter Name", Toast.LENGTH_SHORT).show();
                } else if (TextUtils.isEmpty(etToContactNo.getText().toString())) {
                    Toast.makeText(DeliveryChargesActivity.this, "Enter Contact No.", Toast.LENGTH_SHORT).show();
                } else if (TextUtils.isEmpty(tv_google_loc_to.getText().toString())) {
                    Toast.makeText(DeliveryChargesActivity.this, "Enter Address", Toast.LENGTH_SHORT).show();
                } else {
                    Geocoder coder = new Geocoder(DeliveryChargesActivity.this);
                    List<Address> address;
                    LatLng p1 = null;

                    try {
                        // May throw an IOException
                        address = coder.getFromLocationName(tv_google_loc_to.getText().toString(), 1);
                        if (address.size() != 0) {
                            Address location = address.get(0);
                            toLatitude = location.getLatitude();
                            toLongitude = location.getLongitude();
                            p1 = new LatLng(location.getLatitude(), location.getLongitude());

                            if (toLatitude == 0.0 || toLatitude == 0.0) {
                                Toast.makeText(DeliveryChargesActivity.this, "Please Enter Valid Address", Toast.LENGTH_SHORT).show();
                            } else {
                                llStepOne.setVisibility(View.GONE);
                                llStepSecond.setVisibility(View.GONE);
                                llStepThird.setVisibility(View.VISIBLE);
                                llStepFour.setVisibility(View.GONE);
                                llStepFive.setVisibility(View.GONE);
                                usage_stateprogressbar.setCurrentStateNumber(StateProgressBar.StateNumber.THREE);
                                ;
                            }
                        } else {
                            Toast.makeText(DeliveryChargesActivity.this, "Please Enter Valid Address", Toast.LENGTH_SHORT).show();
                        }

                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                    //  getLocationToAddress(DeliveryChargesActivity.this, tv_google_loc_to.getText().toString());
                }
            }
        });
        tvNextThird.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(tv_date.getText().toString())) {
                    Toast.makeText(DeliveryChargesActivity.this, "Enter Delivery Date", Toast.LENGTH_SHORT).show();
                } else if (TextUtils.isEmpty(tv_time.getText().toString())) {
                    Toast.makeText(DeliveryChargesActivity.this, "Enter Delivery Time", Toast.LENGTH_SHORT).show();
                } else {
                    llStepOne.setVisibility(View.GONE);
                    llStepSecond.setVisibility(View.GONE);
                    llStepThird.setVisibility(View.GONE);
                    llStepFour.setVisibility(View.VISIBLE);
                    llStepFive.setVisibility(View.GONE);
                    usage_stateprogressbar.setCurrentStateNumber(StateProgressBar.StateNumber.FOUR);
                    Location locationA = new Location("point A");

                    locationA.setLatitude(fromLatitude);
                    locationA.setLongitude(fromLongitude);

                    Location locationB = new Location("point B");

                    locationB.setLatitude(toLatitude);
                    locationB.setLongitude(toLongitude);

                    float distance = locationA.distanceTo(locationB) / 1000;
                    locDistance = String.format("%.1f", distance);
                    Log.d("distance", locDistance + "");
                    etCalDistance.setText(Float.parseFloat(locDistance) + " km");
                    charge = Double.parseDouble(delivery_charge) * Float.parseFloat(locDistance);
                    etCharges.setText(currency + "" + charge + "");
                    setTaxFromProfile();
                    calc_tax_value();
                }
            }
        });
        tvNextFour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finalTotal = total;
                detail();
                llStepOne.setVisibility(View.GONE);
                llStepSecond.setVisibility(View.GONE);
                llStepThird.setVisibility(View.GONE);
                llStepFour.setVisibility(View.GONE);
                llStepFive.setVisibility(View.VISIBLE);
                usage_stateprogressbar.setCurrentStateNumber(StateProgressBar.StateNumber.FIVE);

            }
        });

        cbToAddress.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    tvFrom.setText("To");
                    tvTo.setText("From");
                } else {
                    tvFrom.setText("From");
                    tvTo.setText("To");
                }
            }
        });

        ivFromMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                   /* if (tvFrom.getText().equals("From")) {
                        double lati = fromLatitude;
                        double longi = fromLongitude;
                        String q = tvFromAddress.getText().toString();
                       *//* String uri = String.format(Locale.ENGLISH, "geo:%f,%f", lati, longi);
                        uri = uri + "?q=" + q;*//*

                        String uri = "http://maps.google.com/maps?saddr=" + fromLatitude + "," + fromLongitude + "&daddr=" + toLatitude + "," + toLongitude;
                        Log.d("route", uri);
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                        intent.setPackage("com.google.android.apps.maps");
                        startActivity(intent);
                    } else {
                        double lati = toLatitude;
                        double longi = toLongitude;
                        String q = tvFromAddress.getText().toString();
                        String uri = String.format(Locale.ENGLISH, "geo:%f,%f", lati, longi);
                        uri = uri + "?q=" + q;
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                        intent.setPackage("com.google.android.apps.maps");
                        startActivity(intent);
                    }
*/
                    double lati = fromLatitude;
                    double longi = fromLongitude;
                    String q = tvFromAddress.getText().toString();
                       /* String uri = String.format(Locale.ENGLISH, "geo:%f,%f", lati, longi);
                        uri = uri + "?q=" + q;*/

                    String uri = "http://maps.google.com/maps?saddr=" + fromLatitude + "," + fromLongitude + "&daddr=" + toLatitude + "," + toLongitude;
                    Log.d("route", uri);
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                    intent.setPackage("com.google.android.apps.maps");
                    startActivity(intent);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        /*ivToMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (tvFrom.getText().equals("From")) {
                        double lati = toLatitude;d
                        double longi = toLongitude;
                        String q = tvToAddress.getText().toString();
                        String uri = String.format(Locale.ENGLISH, "geo:%f,%f", lati, longi);
                        uri = uri + "?q=" + q;
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                        intent.setPackage("com.google.android.apps.maps");
                        startActivity(intent);
                    } else {
                        double lati = fromLatitude;
                        double longi = fromLongitude;
                        String q = tvToAddress.getText().toString();
                        String uri = String.format(Locale.ENGLISH, "geo:%f,%f", lati, longi);
                        uri = uri + "?q=" + q;
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                        intent.setPackage("com.google.android.apps.maps");
                        startActivity(intent);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });*/

        String userprofile = sessionManager.getUserprofile();
        userProfileData = new Gson().fromJson(userprofile, Userprofile.class);

        tv_p_cur.setText(currency);
        tv_dp_cur.setText(currency);

        et_mrp.setEnabled(false);
        et_sgst.setEnabled(false);
        et_dis_val.setEnabled(false);
        et_dis_per.setEnabled(false);
        et_cgst.setEnabled(false);
        et_sgst.setEnabled(false);

        findViewById(R.id.iv_clear).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv_google_loc.setText("");
            }
        });
        findViewById(R.id.iv_clear_to).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv_google_loc_to.setText("");
            }
        });

        tv_google_loc.setThreshold(1);
        tv_google_loc_to.setThreshold(1);
        tv_google_loc.setOnItemClickListener(mAutocompleteClickListener);
        tv_google_loc_to.setOnItemClickListener(mAutocompleteClickListener);
        locationinitialized();
        strikeLineIfDiscValuew(tvRecAmount);
        cbAmount.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    finalTotal = total + formatedprice;
                    removeStrikeLineIfNoDiscValuew(tvRecAmount);
                } else {
                    finalTotal = total;
                    strikeLineIfDiscValuew(tvRecAmount);
                }
                detail();
            }
        });
    }

    public LatLng getLocationFromAddress(Context context, String strAddress) {
        Geocoder coder = new Geocoder(context);
        List<Address> address;
        LatLng p1 = null;

        try {
            // May throw an IOException
            address = coder.getFromLocationName(strAddress, 5);
            if (address == null) {
                return null;
            }
            if (address.size() != 0) {
                Address location = address.get(0);
                fromLatitude = location.getLatitude();
                fromLongitude = location.getLatitude();
                p1 = new LatLng(location.getLatitude(), location.getLongitude());
            }

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return p1;
    }

    public LatLng getLocationToAddress(Context context, String strAddress) {
        Geocoder coder = new Geocoder(context);
        List<Address> address;
        LatLng p1 = null;

        try {
            // May throw an IOException
            address = coder.getFromLocationName(strAddress, 5);
            if (address == null) {
                return null;
            }
            if (address.size() != 0) {
                Address location = address.get(0);
                toLatitude = location.getLatitude();
                toLongitude = location.getLatitude();
                p1 = new LatLng(location.getLatitude(), location.getLongitude());
            }

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return p1;
    }

    Calendar cal_order = Calendar.getInstance();

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_date:
                DetePickerFragment mDatePicker = new DetePickerFragment(DeliveryChargesActivity.this);
                mDatePicker.setStyle(DialogFragment.STYLE_NORMAL, R.style.CustomDialog);
                mDatePicker.show(getSupportFragmentManager(), "Select delivery date");

                break;
            case R.id.iv_time:
                TimePicker mTimePicker = new TimePicker(DeliveryChargesActivity.this);
                mTimePicker.show(getSupportFragmentManager(), "Select delivery time");
                break;
            case R.id.iv_address:

                break;
            default:
                break;
        }
    }

    @Override
    public void onItemClick(Boolean isTrue, int year, int month, int dayOfMonth) {
        if (isTrue) {
            cal_order.set(year, month, dayOfMonth);
            settime();
        } else {
        }
    }

    @Override
    public void onItemClick(Boolean isTrue, int hourOfDay, int minute) {
        if (isTrue) {
            cal_order.set(cal_order.get(Calendar.YEAR), cal_order.get(Calendar.MONTH), cal_order.get(Calendar.DAY_OF_MONTH), hourOfDay, minute);

            settime();

        } else {
        }
    }

    public void settime() {
        SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy hh:mm aaa");
        String time = df.format(cal_order.getTime());

        SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy hh:mm aaa");
        try {
            if (!time.equals("")) {
                Date date_ = format.parse(time);


                SimpleDateFormat timeformat = new SimpleDateFormat("hh:mm aaa");
                String str_time = timeformat.format(date_);

                SimpleDateFormat dtformat = new SimpleDateFormat("dd MMM yyyy");

                String str_date = dtformat.format(date_);
                tv_date.setText(str_date);
                tv_time.setText(str_time);

            }
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    Double finalTotal = 0.0;

    public void detail() {
        if (tvFrom.getText().equals("From")) {
            tvFromName.setText(etFromName.getText().toString());
            tvFromContact.setText(etFromContactNo.getText().toString());
            tvFromAddress.setText(tv_google_loc.getText().toString());
            tvToName.setText(etToName.getText().toString());
            tvToContact.setText(etToContactNo.getText().toString());
            tvToAddress.setText(tv_google_loc_to.getText().toString());
        } else {
            tvFromName.setText(etToName.getText().toString());
            tvFromContact.setText(etToContactNo.getText().toString());
            tvFromAddress.setText(tv_google_loc_to.getText().toString());
            tvToName.setText(etFromName.getText().toString());
            tvToContact.setText(etFromContactNo.getText().toString());
            tvToAddress.setText(tv_google_loc.getText().toString());
        }

       /* if (ref_id.equalsIgnoreCase("")) {
            tvRefId.setText("From");
        } else {
            tvRefId.setText("From / " + ref_id + " / " + case_id);
        }*/
        tvDate.setText(tv_date.getText().toString());
        tvTime.setText(tv_time.getText().toString());
        tvDistance.setText(etCalDistance.getText().toString());
        tvCharge.setText(currency + Inad.getCurrencyDecimal(total, this));
        tvRecAmount.setText(currency + Inad.getCurrencyDecimal(formatedprice, this));


        tvFinalTotal.setText(currency + Inad.getCurrencyDecimal(finalTotal, this));
        ivFromMap.setVisibility(View.VISIBLE);
       /* ivToMap.setVisibility(View.VISIBLE);
        if (tvFromAddress.getText().equals("")) {
            ivFromMap.setVisibility(View.GONE);
        } else if (tvToAddress.getText().equals("")) {
            ivToMap.setVisibility(View.GONE);
        }*/
    }

    Double formatedprice = 0.00;

    void fillDetails() {

        try {
            String BusinessInfo = JsonObjParse.getValueEmpty(SummaryDetailFragmnet.item.getFooter_note(), "BusinessInfo");
            String ADDRESS = JsonObjParse.getValueEmpty(SummaryDetailFragmnet.item.getFooter_note(), "ADDRESS");
            ref_id = SummaryDetailFragmnet.item.getRef_id();
            case_id = SummaryDetailFragmnet.item.getCase_id();
            tv_google_loc_to.setText(ADDRESS);
            etReference.setText(ref_id + " / " + case_id);

            if (tv_google_loc_to.getText().equals("")) {
                findViewById(R.id.iv_clear_to).setVisibility(View.GONE);
            } else {
                findViewById(R.id.iv_clear_to).setVisibility(View.VISIBLE);
            }

            if (!ADDRESS.equalsIgnoreCase("")) {
                getLocationFromAddress(this, ADDRESS);
            }

            if (!BusinessInfo.isEmpty()) {
                JSONArray jaBusinessInfo = new JSONArray(BusinessInfo);
                if (jaBusinessInfo.length() > 0) {
                    JSONObject joConsInfo = jaBusinessInfo.getJSONObject(0);
                    etFromName.setText(joConsInfo.getString("customer_nm"));
                    etFromContactNo.setText(joConsInfo.getString("mobile"));
                }
            }


            DecimalFormat twoDForm = new DecimalFormat("#.##");
            Double price = Double.parseDouble(SummaryDetailFragmnet.item.getTotal_chit_item_value());
            formatedprice = Double.valueOf(twoDForm.format(price));
            etRecAmount.setText(currency + Inad.getCurrencyDecimal(formatedprice, this));
            String DATE = JsonObjParse.getValueEmpty(SummaryDetailFragmnet.item.getFooter_note(), "DATE");

            String date = DateTimeUtils.parseDateTime(DATE, "dd MMM yyyy hh:mm aaa", "dd MMM yyyy");
            String time = DateTimeUtils.parseDateTime(DATE, "dd MMM yyyy hh:mm aaa", "hh:mm aaa");
            tv_date.setText(date);
            tv_time.setText(time);

            // jsonObject = new JSONObject(SummaryDetailFragmnet.item.getFooter_note());

            /*String pick_up_only = "";
            if (jsonObject.has("field_json_data")) {
                String field_json_data = JsonObjParse.getValueEmpty(SummaryDetailFragmnet.item.getFooter_note(), "field_json_data");
                pick_up_only = JsonObjParse.getValueEmpty(field_json_data, "pick_up_only");
            }
*/
            String ConsumerInfo = JsonObjParse.getValueEmpty(SummaryDetailFragmnet.item.getFooter_note(), "ConsumerInfo");
            if (!ConsumerInfo.isEmpty()) {
                JSONArray jaConsumerInfo = new JSONArray(ConsumerInfo);
                if (jaConsumerInfo.length() > 0) {
                    JSONObject joConsInfo = jaConsumerInfo.getJSONObject(0);
                    etToName.setText(joConsInfo.getString("customer_nm"));
                    etToContactNo.setText(joConsInfo.getString("mobile"));
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    // Create chit

    public interface CallRequest {
        public void onGetResponse(String response);
    }

    String headernotes = "", frombridgeidval = "";
    CallRequest callRequestForSurvey;

    private ProgressDialog dialog;

    public void callCreateChit(final AppCompatActivity c, final CallRequest callRequest) {

        callRequestForSurvey = callRequest;


        final SessionManager sessionManager = new SessionManager(c);
        final SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy hh:mm aaa");
        final Calendar cal_order = Calendar.getInstance();

        //headernotes = remark;
        headernotes = "";
        final String date_ = df.format(cal_order.getTime());

        sessionManager.setDate(date_);
        sessionManager.setAddress("###");
        sessionManager.setRemark(headernotes);
        sessionManager.setT_Phone("###");

        try {
            userdetails = new JSONObject(sessionManager.getUserprofile());
            loggedin = new Gson().fromJson(userdetails.toString(), Userprofile.class);

        } catch (JSONException e) {
            e.printStackTrace();
        }


        usersignin(c, sessionManager.getsession(), date_, "###");

    }

    Userprofile loggedin;
    JSONObject userdetails;
    int productcount;

    String field_json_data = "", field_json_data_info = "", business_type_info = "";

    Double price = 0.0;

    public void usersignin(final Context c, final String usersession, final String dt, final String address) {

        dialog = new ProgressDialog(c);
        dialog.setMessage("Sending...");
        dialog.show();

        final SessionManager sessionManager = new SessionManager(c);

        field_json_data = SharedPrefUserDetail.getString(c, SharedPrefUserDetail.field_json_data, "");
        field_json_data_info = SharedPrefUserDetail.getString(c, SharedPrefUserDetail.field_json_data_info, "");
        business_type_info = SharedPrefUserDetail.getString(c, SharedPrefUserDetail.business_type, "");


        productcount = 1;

        final JsonObject sendobj = new JsonObject();
        JsonArray a = new JsonArray();
        JsonArray cartproduct = new JsonArray();

        JsonObject productsync = new JsonObject();

        JSONObject joPart = new JSONObject();
        try {

            joPart.put("fromName", "" + tvFromName.getText().toString());
            joPart.put("fromContact", "" + tvFromContact.getText().toString());
            joPart.put("fromAddress", "" + tvFromAddress.getText().toString());
            joPart.put("toName", "" + tvToName.getText().toString());
            joPart.put("toContact", "" + tvFromContact.getText().toString());
            joPart.put("toAddress", "" + tvToAddress.getText().toString());
            joPart.put("delivery_distance", "" + tvDistance.getText().toString());
            joPart.put("delivery_charge", "" + String.valueOf(finalTotal));
            joPart.put("delivery_date", "" + tv_date.getText().toString());
            joPart.put("delivery_time", "" + tv_time.getText().toString());
            joPart.put("ref_id", "" + ref_id);
            joPart.put("case_id", "" + case_id);
            joPart.put("purpose", "" + purpose);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        price = charge;
        price = total;

        productsync.addProperty("entry_id", "");
        productsync.addProperty("bridge_id", "");
        productsync.addProperty("flag", "add");

        productsync.addProperty("quantity", 1);

        productsync.addProperty("particulars", joPart.toString());
        //productsync.addProperty("price", String.valueOf(cal_price(i)));
        productsync.addProperty("price", "" + price);
        productsync.addProperty("offer", "");
        cartproduct.add(productsync);

        try {
            JsonObject a1 = new JsonObject();

            a1.addProperty("email", loggedin.getEmail_id());
            a1.addProperty("contact_number", loggedin.getContact_no());
            a1.addProperty("location", loggedin.getLocation());
            a1.addProperty("chit_name", loggedin.getFirstname());
            a1.addProperty("header_note", headernotes);

            //a1.addProperty("footer_note", jo_footer.toString());
            sendobj.add("newdata", cartproduct);
            sendobj.add("olddata", a);
            sendobj.add("chitHeader", a1);

        } catch (JsonParseException e) {
            e.printStackTrace();
        }
        //price = price / dbHelper.getcartcount();

        ApiInterface apiService =
                ApiClient.getClient(c).create(ApiInterface.class);
        String currency_code = SharedPrefUserDetail.getString(c, SharedPrefUserDetail.chit_currency_code, "");
        String currency_code_id = SharedPrefUserDetail.getString(c, SharedPrefUserDetail.chit_currency_code_id, "");

        Call call = apiService.createchit(usersession, sendobj, sessionManager.getAggregatorprofile(), frombridgeidval, "d97be841a6d6cbb66bc3ae165b31b85f", currency_code, currency_code_id, "2");
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {

                if (response.isSuccessful()) {
                    try {
                        String a = new Gson().toJson(response.body());
                        Log.e("response", a);
                        JSONObject responseobj = new JSONObject(a);
                        JSONObject responseval = responseobj.getJSONObject("response");

                        JSONObject data = responseval.getJSONObject("data");
                        String chithashid = data.getString("chitHashId");
                        try {
                            JsonObject jObj = new JsonObject();
                            jObj.addProperty("email", loggedin.getEmail_id());
                            jObj.addProperty("contact_number", sessionManager.getT_Phone());
                            jObj.addProperty("location", sessionManager.getAddress());
                            jObj.addProperty("act", sessionManager.getcurrentcaterername());
                            jObj.addProperty("chit_name", "");
                            jObj.addProperty("chitHashId", chithashid);
                            jObj.addProperty("latitude", "");
                            jObj.addProperty("longtitude", "");
                            jObj.addProperty("expected_delivery_time", "");
                            jObj.addProperty("for_non_bridge_name", "");
                            jObj.addProperty("subject", sessionManager.getcurrentcatereraliasname());

                            String currency_code = SharedPrefUserDetail.getString(c, SharedPrefUserDetail.chit_currency_code, "");
                            String symbol_native = SharedPrefUserDetail.getString(c, SharedPrefUserDetail.chit_symbol_native, "");
                            String currency_code_id = SharedPrefUserDetail.getString(c, SharedPrefUserDetail.chit_currency_code_id, "");
                            String symbol = SharedPrefUserDetail.getString(c, SharedPrefUserDetail.chit_symbol, "");

                            jObj.addProperty("currency_code", currency_code);
                            jObj.addProperty("symbol_native", symbol_native);
                            jObj.addProperty("currency_code_id", currency_code_id);
                            jObj.addProperty("symbol", symbol);


                            boolean iss = SharedPrefUserDetail.getBoolean(c, SharedPrefUserDetail.isfromsupplier, false);
                            if (iss) {
                                jObj.addProperty("info", "");
                                StringBuilder stringBuilder = new StringBuilder();
                                if (field_json_data != null) {

                                    String delivery_partner_info = JsonObjParse.getValueEmpty(field_json_data, "delivery_partner_info");
                                    if (!delivery_partner_info.isEmpty()) {
                                        stringBuilder.append(delivery_partner_info + " , ");
                                        jObj.addProperty("info", stringBuilder.toString());
                                    }
                                }
                            } else {

                                String info = SharedPrefUserDetail.getString(c, SharedPrefUserDetail.infoMainShop, "");
                                String act = sessionManager.getcurrentcaterername();
                                StringBuilder stringBuilder = new StringBuilder(); // old is correct

                                if (field_json_data_info != null) {
                                    String pass_info_circle = JsonObjParse.getValueEmpty(field_json_data_info, "pass_info_circle");
                                    String pass_info_aggregator = JsonObjParse.getValueEmpty(field_json_data_info, "pass_info_aggregator");
                                    if (!pass_info_circle.equalsIgnoreCase("no") && business_type_info.equalsIgnoreCase("circle")) {
                                        if (!info.isEmpty()) stringBuilder.append(info + " , ");
                                    }
                                    if (!pass_info_aggregator.equalsIgnoreCase("no") && business_type_info.equalsIgnoreCase("aggregator")) {
                                        if (!info.isEmpty()) stringBuilder.append(info + " , ");
                                    }
                                }

                                if (!act.isEmpty()) stringBuilder.append(act + " , ");

                                if (field_json_data != null) {
                                    String delivery_partner_info = JsonObjParse.getValueEmpty(field_json_data, "delivery_partner_info");
                                    if (!delivery_partner_info.isEmpty()) {
                                        stringBuilder.append(delivery_partner_info + " , ");
                                    }
                                }

                                Log.d("okhttp info", stringBuilder.toString());
                                jObj.addProperty("info", stringBuilder.toString());
                                //jObj.addProperty("info", sessionManager.getAggregator_ID());

                            }


                            jObj.addProperty("header_note", headernotes);

                            jObj.addProperty("purpose", purpose);

                            jObj.addProperty("chit_item_count", productcount);


                            jObj.addProperty("total_chit_item_value", price + "");
                            JsonObject jo_footer = new JsonObject();
                          /*  jo_footer.addProperty("SGST", String.valueOf(totalstategst));
                            jo_footer.addProperty("VAT", String.valueOf(totalVat));
                            jo_footer.addProperty("CGST", String.valueOf(totalcentralgst));
                            jo_footer.addProperty("TOTALBILL", String.valueOf(totalbill));
                            jo_footer.addProperty("GRANDTOTAL", String.valueOf(grandtotal));*/
                            jo_footer.addProperty("NAME", loggedin.getFirstname());
                            jo_footer.addProperty("PAYMENT_MODE", "NONE");

                            jo_footer.addProperty("app_name", getString(R.string.app_name));

                            jo_footer.addProperty("DATE", dt);
                            jo_footer.addProperty("ADDRESS", "");
                            jo_footer.addProperty("DOB", "");
                            jObj.addProperty("footer_note", jo_footer.toString());

                            Log.e("chitheadervalue", jObj.toString());
                            confirmorder("", chithashid);

                        } catch (JsonParseException e) {
                            e.printStackTrace();
                        }
                    } catch (Exception e) {
                        Toast.makeText(c, e.getMessage(), Toast.LENGTH_LONG).show();
                    }

                } else {
                    if (dialog != null) dialog.dismiss();

                    Log.e("test", " trdds1");

                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Toast.makeText(c, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(c, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                if (dialog != null) dialog.dismiss();

                Log.e("error message", t.toString());
            }
        });
    }

    /*public void confirmorder(Context context, String hashidval) {

        Userprofile loggedin = null;

        try {
            JSONObject userdetails = new JSONObject(sessionManager.getUserprofile());
            loggedin = new Gson().fromJson(userdetails.toString(), Userprofile.class);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            JsonObject jObj = new JsonObject();
            jObj.addProperty("email", loggedin.getEmail_id());
            jObj.addProperty("contact_number", sessionManager.getT_Phone());
            jObj.addProperty("location", sessionManager.getAddress());
            jObj.addProperty("act", sessionManager.getcurrentcaterername());


            jObj.addProperty("chit_name", ""); //o


            jObj.addProperty("chitHashId", hashidval);
            jObj.addProperty("latitude", "");
            jObj.addProperty("longtitude", "");

            String expected_delivery_time = DateTimeUtils.parseDateTime(sessionManager.getDate(), "dd MMM yyyy hh:mm aaa", "yyyy/MM/dd kk:mm");

            jObj.addProperty("expected_delivery_time", expected_delivery_time);
            jObj.addProperty("for_non_bridge_name", "");
            jObj.addProperty("subject", sessionManager.getcurrentcatereraliasname());

            String currency_code = SharedPrefUserDetail.getString(context, SharedPrefUserDetail.chit_currency_code, "");
            String symbol_native = SharedPrefUserDetail.getString(context, SharedPrefUserDetail.chit_symbol_native, "");
            String currency_code_id = SharedPrefUserDetail.getString(context, SharedPrefUserDetail.chit_currency_code_id, "");
            String symbol = SharedPrefUserDetail.getString(context, SharedPrefUserDetail.chit_symbol, "");

            jObj.addProperty("currency_code", currency_code);
            jObj.addProperty("symbol_native", symbol_native);
            jObj.addProperty("currency_code_id", currency_code_id);
            jObj.addProperty("symbol", symbol);

            // The order is, first we hit blrmcc (info ) then we hit blrmc1
            //In blrmcc , we check info flag
            //In blrmc1 , we check delivery flag

            boolean iss = SharedPrefUserDetail.getBoolean(context, SharedPrefUserDetail.isfromsupplier, false);
            if (iss) { // if its business
                jObj.addProperty("info", "");

                StringBuilder stringBuilder = new StringBuilder();

                if (field_json_data != null) {

                    String delivery_partner_info = JsonObjParse.getValueEmpty(field_json_data, "delivery_partner_info");
                    if (!delivery_partner_info.isEmpty()) {
                        stringBuilder.append(delivery_partner_info + " , ");
                        jObj.addProperty("info", stringBuilder.toString());
                    }
                }
                Log.d("okhttp info iss", stringBuilder.toString());

            } else { // if its aggre / circle
                // blrmc1(delivery flag of this) under blrmcc(info flag of this)
                String info = SharedPrefUserDetail.getString(context, SharedPrefUserDetail.infoMainShop, "");
                //String act = sessionManager.getAggregator_ID();
                String act = sessionManager.getcurrentcaterername();
                //String infoPass = info + " , " + act + " , ";
                StringBuilder stringBuilder = new StringBuilder(); // old is correct
                //if(!info.isEmpty())stringBuilder.append(info+" , ");
                //if(!act.isEmpty())stringBuilder.append(act+" , ");
                if (field_json_data_info != null) {
                    String pass_info_circle = JsonObjParse.getValueEmpty(field_json_data_info, "pass_info_circle");
                    String pass_info_aggregator = JsonObjParse.getValueEmpty(field_json_data_info, "pass_info_aggregator");
                    if (!pass_info_circle.equalsIgnoreCase("no") && business_type_info.equalsIgnoreCase("circle")) {
                        if (!info.isEmpty()) stringBuilder.append(info + " , ");
                    }
                    if (!pass_info_aggregator.equalsIgnoreCase("no") && business_type_info.equalsIgnoreCase("aggregator")) {
                        if (!info.isEmpty()) stringBuilder.append(info + " , ");
                    }
                }

                if (!act.isEmpty()) stringBuilder.append(act + " , ");

                if (field_json_data != null) {

                    String delivery_partner_info = JsonObjParse.getValueEmpty(field_json_data, "delivery_partner_info");
                    if (!delivery_partner_info.isEmpty()) {
                        stringBuilder.append(delivery_partner_info + " , ");
                    }
                }

                Log.d("okhttp info", stringBuilder.toString());
                jObj.addProperty("info", stringBuilder.toString());
                //jObj.addProperty("info", sessionManager.getAggregator_ID());
            }

            jObj.addProperty("header_note", sessionManager.getRemark());

            jObj.addProperty("purpose", purpose);

            jObj.addProperty("chit_item_count", productcount);
            jObj.addProperty("total_chit_item_value", String.valueOf(price));
            JsonObject jo_footer = new JsonObject();
            jo_footer.addProperty("app_name", getString(R.string.app_name));
            jo_footer.addProperty("SGST", "0.0");
            jo_footer.addProperty("CGST", "0.0");
            jo_footer.addProperty("VAT", "0.0");
            jo_footer.addProperty("TOTALBILL", "0.0");
            jo_footer.addProperty("TOTLREQDISC", "0.0");
            jo_footer.addProperty("REQ_DISC", "");
            jo_footer.addProperty("TOTLPRODISC", "");

            jo_footer.addProperty("COUPONDISC", "");
            jo_footer.addProperty("COUPONDISCLBL", "");
            jo_footer.addProperty("GRANDTOTAL", "");
            //jo_footer.addProperty("businessId", String.valueOf(grandtotal));

            jObj.addProperty("total_chit_item_value", String.valueOf(price));

            jo_footer.addProperty("NAME", loggedin.getFirstname());
            boolean isMrp = SharedPrefUserDetail.getBoolean(this, SharedPrefUserDetail.isMrp, false);
            if (isMrp) { // from all task only pass flage

                Chit chit = SummaryDetailFragmnet.item;
                //jaMrp = new JSONArray();
                joFrom = new JSONObject();
                joTo = new JSONObject();

                // Ex, from henncy(task) customer athi, supplier navnit
                // Henncy buying something from Nitin for Athi

                try {

                   *//* joFrom.put("from_bridge_id", chit.getFrom_bridge_id());
                    String ConsumerInfo = JsonObjParse.getValueEmpty(chit.getFooter_note(), "ConsumerInfo");
                    if (!ConsumerInfo.isEmpty()) {
                        JSONArray jaConsumerInfo = new JSONArray(ConsumerInfo);
                        if (jaConsumerInfo.length() > 0) {
                            JSONObject joConsInfo = jaConsumerInfo.getJSONObject(0);

                            joFrom.put("ConsumerInfo", joConsInfo.toString());
                        }
                    }

                    joFrom.put("ref_id", chit.getRef_id());
                    joFrom.put("case_id", chit.getCase_id());
                    joFrom.put("purpose", chit.getPurpose());
                    joFrom.put("chit_hash_id", chit.getChit_hash_id());
                    joFrom.put("chit_item_count", chit.getChit_item_count());
                    joFrom.put("transaction_status", chit.getTransaction_status());
                    joFrom.put("currency_code", chit.getCurrency_code());
                    joFrom.put("currency_code_id", chit.getCurrency_code_id());
                    joFrom.put("from_chit_id", chit.getChit_id());*//*

                    try {
                        JSONObject jo_summary = new JSONObject(chit.getFooter_note());
                        joFrom.put("DATE", jo_summary.getString("DATE"));
                        joFrom.put("EDIT_DATE", jo_summary.getString("DATE"));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    joFrom.put("symbol_native", chit.getSymbol_native());
                    joFrom.put("total_chit_item_value", chit.getTotal_chit_item_value());
                    joFrom.put("edit_total_chit_item_value", chit.getTotal_chit_item_value());
                    joFrom.put("subject", chit.getSubject());
                    joFrom.put("location", chit.getLocation());

                    *//****************************************************************************//*

     *//* joTo.put("to_bridge_id", chit.getTo_bridge_id());
                    joTo.put("chit_item_count", dbHelper.getcartcount());
                    joTo.put("chit_hash_id", hashidval);
                    joTo.put("currency_code", currency_code);
                    joTo.put("currency_code_id", currency_code_id);
                    joTo.put("to_chit_id", chit.getChit_id());

                    JSONObject joBusiness = new JSONObject();
                    String sms_business = JsonObjParse.getValueEmpty(field_json_data, "sms_status");
                    joBusiness.put("sms", sms_business);
                    String mobile_business = JsonObjParse.getValueEmpty(field_json_data, "mobile_business");
                    joBusiness.put("mobile", mobile_business);
                    joBusiness.put("customer_nm", sessionManager.getcurrentcatereraliasname());
                    joTo.put("BusinessInfo", joBusiness.toString());*//*

                    joTo.put("DATE", sessionManager.getDate());
                    joTo.put("EDIT_DATE", sessionManager.getDate());
                    joTo.put("symbol_native", currency.trim());
                    joTo.put("subject", sessionManager.getcurrentcatereraliasname());
                    joTo.put("location", sessionManager.getAddress());
                    joTo.put("sort", Constants.SORT_MAT);

                    joTo.put("total_chit_item_value", price + "");
                    joTo.put("edit_total_chit_item_value", price);

                    *//****************************************************************************//*

                    //jaMrp.put(joMrp);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                //jo_footer.addProperty("MRP_TAB", jaMrp.toString());
                jo_footer.addProperty("MRP_TAB", "Ys");
            }

            String taxArray = new Gson().toJson(alTax);
            jo_footer.addProperty("taxArray", taxArray);

            jo_footer.addProperty("img", "");

            jo_footer.addProperty("PAYMENT_MODE", "COD");

            // Consumer Array

            JsonArray jaConsumerContact = new JsonArray();
            JsonObject joConsumer = new JsonObject();
            String sms_customer = JsonObjParse.getValueEmpty(field_json_data, "sms_customer");
            joConsumer.addProperty("sms", sms_customer);
            joConsumer.addProperty("customer_nm", loggedin.getFirstname());
            joConsumer.addProperty("mobile", sessionManager.getT_Phone());
            jaConsumerContact.add(joConsumer);
            jo_footer.add("ConsumerInfo", jaConsumerContact);

            // Business Array

            JsonArray jaBusinessContact = new JsonArray();
            JsonObject joBusiness = new JsonObject();
            String sms_business = JsonObjParse.getValueEmpty(field_json_data, "sms_status");
            joBusiness.addProperty("sms", sms_business);
            String mobile_business = JsonObjParse.getValueEmpty(field_json_data, "mobile_business");
            joBusiness.addProperty("mobile", mobile_business);
            joBusiness.addProperty("customer_nm", sessionManager.getcurrentcatereraliasname());
            jaBusinessContact.add(joBusiness);

            jo_footer.add("BusinessInfo", jaBusinessContact);

            // Delivery Array

            JsonArray jaDeliveryContact = new JsonArray();
            JsonObject joDelivery = new JsonObject();

            String mobile_delivery = JsonObjParse.getValueEmpty(field_json_data, "mobile_delivery");
            joDelivery.addProperty("mobile", mobile_delivery);
            String companynm = JsonObjParse.getValueEmpty(field_json_data, "customer_nm_delivery");
            joDelivery.addProperty("customer_nm", companynm + "");
            //String pick_up_only = JsonObjParse.getValueEmpty(field_json_data, "pick_up_only");

            String sms_delivery = JsonObjParse.getValueEmpty(field_json_data, "sms_delivery");
            joDelivery.addProperty("sms", sms_delivery);
            joDelivery.addProperty("pick_up_only", "No");

            jaDeliveryContact.add(joDelivery);
            jo_footer.add("DeliveryInfo", jaDeliveryContact);

            jo_footer.addProperty("DATE", sessionManager.getDate());
            jo_footer.addProperty("ADDRESS", sessionManager.getAddress());

            jo_footer.addProperty("Cust_Nm", sessionManager.getCustNmForProperty());

            jo_footer.addProperty("payuResponse", "");
            jo_footer.addProperty("chithashid", hashidval);

            jo_footer.addProperty("field_json_data", field_json_data);

            jObj.addProperty("footer_note", jo_footer.toString());
            Log.i("okhttp", jo_footer.toString());
            updatechit(context, sessionManager.getsession(), jObj, hashidval);

        } catch (JsonParseException e) {
            e.printStackTrace();
        }
    }*/

    Double totalWithRoundOff = 0.0;
    Double grandtotal = 0.0;
    Double totalbill = 0.0;
    Double totalReqDisc = 0.0;
    Double ReqDisc = 0.0;
    Double totalProductDisc = 0.0;
    Double totalstategst = 0.0;
    Double totalVat = 0.0;
    Double totalcentralgst = 0.0;
    Double couponDiscount = 0.0;


    public void confirmorder(String payuResponse, String hashidval) {

        Userprofile loggedin = null;

        try {
            JSONObject userdetails = new JSONObject(sessionManager.getUserprofile());
            loggedin = new Gson().fromJson(userdetails.toString(), Userprofile.class);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            JsonObject jObj = new JsonObject();
            jObj.addProperty("email", loggedin.getEmail_id());
            jObj.addProperty("contact_number", sessionManager.getT_Phone());
            jObj.addProperty("location", sessionManager.getAddress());
            jObj.addProperty("act", sessionManager.getcurrentcaterername());


            jObj.addProperty("chit_name", ""); //o



            jObj.addProperty("chitHashId", hashidval);
            jObj.addProperty("latitude", "");
            jObj.addProperty("longtitude", "");

            String expected_delivery_time = DateTimeUtils.parseDateTime(sessionManager.getDate(), "dd MMM yyyy hh:mm aaa", "yyyy/MM/dd kk:mm");

            jObj.addProperty("expected_delivery_time", expected_delivery_time);
            jObj.addProperty("for_non_bridge_name", "");
            jObj.addProperty("subject", sessionManager.getcurrentcatereraliasname());

            String currency_code = SharedPrefUserDetail.getString(DeliveryChargesActivity.this, SharedPrefUserDetail.chit_currency_code, "");
            String symbol_native = SharedPrefUserDetail.getString(DeliveryChargesActivity.this, SharedPrefUserDetail.chit_symbol_native, "");
            String currency_code_id = SharedPrefUserDetail.getString(DeliveryChargesActivity.this, SharedPrefUserDetail.chit_currency_code_id, "");
            String symbol = SharedPrefUserDetail.getString(DeliveryChargesActivity.this, SharedPrefUserDetail.chit_symbol, "");

            jObj.addProperty("currency_code", currency_code);
            jObj.addProperty("symbol_native", symbol_native);
            jObj.addProperty("currency_code_id", currency_code_id);
            jObj.addProperty("symbol", symbol);


            // The order is, first we hit blrmcc (info ) then we hit blrmc1
            //In blrmcc , we check info flag
            //In blrmc1 , we check delivery flag


            boolean iss = SharedPrefUserDetail.getBoolean(DeliveryChargesActivity.this, SharedPrefUserDetail.isfromsupplier, false);
            if (iss) { // if its business
                jObj.addProperty("info", "");

                StringBuilder stringBuilder = new StringBuilder();

                if (field_json_data != null) {

                    String delivery_partner_info = JsonObjParse.getValueEmpty(field_json_data, "delivery_partner_info");
                    if (!delivery_partner_info.isEmpty()) {
                        stringBuilder.append(delivery_partner_info + " , ");
                        jObj.addProperty("info", stringBuilder.toString());
                    }
                }

                Log.d("okhttp info iss", stringBuilder.toString());


            } else { // if its aggre / circle
                // blrmc1(delivery flag of this) under blrmcc(info flag of this)
                String info = SharedPrefUserDetail.getString(DeliveryChargesActivity.this, SharedPrefUserDetail.infoMainShop, "");
                //String act = sessionManager.getAggregator_ID();
                String act = sessionManager.getcurrentcaterername();
                //String infoPass = info + " , " + act + " , ";
                StringBuilder stringBuilder = new StringBuilder(); // old is correct
                //if(!info.isEmpty())stringBuilder.append(info+" , ");
                //if(!act.isEmpty())stringBuilder.append(act+" , ");
                if (field_json_data_info != null) {
                    String pass_info_circle = JsonObjParse.getValueEmpty(field_json_data_info, "pass_info_circle");
                    String pass_info_aggregator = JsonObjParse.getValueEmpty(field_json_data_info, "pass_info_aggregator");
                    if (!pass_info_circle.equalsIgnoreCase("no") && business_type_info.equalsIgnoreCase("circle")) {
                        if (!info.isEmpty()) stringBuilder.append(info + " , ");
                    }
                    if (!pass_info_aggregator.equalsIgnoreCase("no") && business_type_info.equalsIgnoreCase("aggregator")) {
                        if (!info.isEmpty()) stringBuilder.append(info + " , ");
                    }
                }

                if (!act.isEmpty()) stringBuilder.append(act + " , ");

                if (field_json_data != null) {

                    String delivery_partner_info = JsonObjParse.getValueEmpty(field_json_data, "delivery_partner_info");
                    if (!delivery_partner_info.isEmpty()) {
                        stringBuilder.append(delivery_partner_info + " , ");
                    }
                }

                Log.d("okhttp info", stringBuilder.toString());
                jObj.addProperty("info", stringBuilder.toString());
                //jObj.addProperty("info", sessionManager.getAggregator_ID());
            }


            jObj.addProperty("header_note", sessionManager.getRemark());

            String service_type_of = JsonObjParse.getValueEmpty(field_json_data, "service_type_of");

            jObj.addProperty("purpose", purpose);

            jObj.addProperty("chit_item_count", dbHelper.getcartcount());
            //jObj.addProperty("total_chit_item_value", String.valueOf(grandtotal));
            jObj.addProperty("total_chit_item_value", String.valueOf(finalTotal));
            JsonObject jo_footer = new JsonObject();
            jo_footer.addProperty("app_name", getString(R.string.app_name));
            jo_footer.addProperty("SGST", String.valueOf(totalstategst));
            jo_footer.addProperty("CGST", String.valueOf(totalcentralgst));
            jo_footer.addProperty("VAT", String.valueOf(totalVat));
            jo_footer.addProperty("TOTALBILL", String.valueOf(totalbill));
            jo_footer.addProperty("TOTLREQDISC", String.valueOf(totalReqDisc));
            jo_footer.addProperty("REQ_DISC", String.valueOf(ReqDisc));
            jo_footer.addProperty("TOTLPRODISC", String.valueOf(totalProductDisc));

            jo_footer.addProperty("COUPONDISC", String.valueOf(couponDiscount));
            jo_footer.addProperty("COUPONDISCLBL", "");
            jo_footer.addProperty("GRANDTOTAL", String.valueOf(grandtotal));
            jo_footer.addProperty("businessId", String.valueOf(grandtotal));


            if (totalWithRoundOff > 0) {
                jObj.addProperty("total_chit_item_value", String.valueOf(totalWithRoundOff));
                jo_footer.addProperty("totalWithRoundOff", String.valueOf(totalWithRoundOff));
            }

            jo_footer.addProperty("NAME", loggedin.getFirstname());
            boolean isMrp = SharedPrefUserDetail.getBoolean(this, SharedPrefUserDetail.isMrp, false);
            if (isMrp) { // from all task only pass flage

                Chit chit = SummaryDetailFragmnet.item;
                //jaMrp = new JSONArray();
                joFrom = new JSONObject();
                joTo = new JSONObject();

                // Ex, from henncy(task) customer athi, supplier navnit
                // Henncy buying something from Nitin for Athi

                try {

                   /* joFrom.put("from_bridge_id", chit.getFrom_bridge_id());
                    String ConsumerInfo = JsonObjParse.getValueEmpty(chit.getFooter_note(), "ConsumerInfo");
                    if (!ConsumerInfo.isEmpty()) {
                        JSONArray jaConsumerInfo = new JSONArray(ConsumerInfo);
                        if (jaConsumerInfo.length() > 0) {
                            JSONObject joConsInfo = jaConsumerInfo.getJSONObject(0);

                            joFrom.put("ConsumerInfo", joConsInfo.toString());
                        }
                    }

                    joFrom.put("ref_id", chit.getRef_id());
                    joFrom.put("case_id", chit.getCase_id());
                    joFrom.put("purpose", chit.getPurpose());
                    joFrom.put("chit_hash_id", chit.getChit_hash_id());
                    joFrom.put("chit_item_count", chit.getChit_item_count());
                    joFrom.put("transaction_status", chit.getTransaction_status());
                    joFrom.put("currency_code", chit.getCurrency_code());
                    joFrom.put("currency_code_id", chit.getCurrency_code_id());
                    joFrom.put("from_chit_id", chit.getChit_id());*/

                    try {
                        JSONObject jo_summary = new JSONObject(chit.getFooter_note());
                        joFrom.put("DATE", jo_summary.getString("DATE"));
                        joFrom.put("EDIT_DATE", jo_summary.getString("DATE"));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    joFrom.put("symbol_native", chit.getSymbol_native());
                    joFrom.put("total_chit_item_value", chit.getTotal_chit_item_value());
                    joFrom.put("edit_total_chit_item_value", chit.getTotal_chit_item_value());
                    joFrom.put("subject", chit.getSubject());
                    joFrom.put("location", chit.getLocation());


                    /****************************************************************************/

                   /* joTo.put("to_bridge_id", chit.getTo_bridge_id());
                    joTo.put("chit_item_count", dbHelper.getcartcount());
                    joTo.put("chit_hash_id", hashidval);
                    joTo.put("currency_code", currency_code);
                    joTo.put("currency_code_id", currency_code_id);
                    joTo.put("to_chit_id", chit.getChit_id());

                    JSONObject joBusiness = new JSONObject();
                    String sms_business = JsonObjParse.getValueEmpty(field_json_data, "sms_status");
                    joBusiness.put("sms", sms_business);
                    String mobile_business = JsonObjParse.getValueEmpty(field_json_data, "mobile_business");
                    joBusiness.put("mobile", mobile_business);
                    joBusiness.put("customer_nm", sessionManager.getcurrentcatereraliasname());
                    joTo.put("BusinessInfo", joBusiness.toString());*/

                    joTo.put("DATE", sessionManager.getDate());
                    joTo.put("EDIT_DATE", sessionManager.getDate());
                    joTo.put("symbol_native", currency.trim());
                    joTo.put("subject", sessionManager.getcurrentcatereraliasname());
                    joTo.put("location", sessionManager.getAddress());
                    joTo.put("sort", Constants.SORT_MAT);


                    if (totalWithRoundOff > 0) {
                        joTo.put("total_chit_item_value", totalWithRoundOff + "");
                        joTo.put("edit_total_chit_item_value", totalWithRoundOff);
                    } else {
                        joTo.put("total_chit_item_value", grandtotal + "");
                        joTo.put("edit_total_chit_item_value", grandtotal);
                    }


                    /****************************************************************************/


                    //jaMrp.put(joMrp);


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                //jo_footer.addProperty("MRP_TAB", jaMrp.toString());
                jo_footer.addProperty("MRP_TAB", "Ys");
            }


            String taxArray = new Gson().toJson(alTax);
            jo_footer.addProperty("taxArray", taxArray);



            jo_footer.addProperty("img", "");



           /* if (rb_cod.isChecked()) {
                iscod = true;
                jo_footer.addProperty("PAYMENT_MODE", "COD");
            } else {
                iscod = false;
                jo_footer.addProperty("PAYMENT_MODE", "RAZORPAY");
            }*/
            jo_footer.addProperty("PAYMENT_MODE", "COD");

            // Consumer Array

            JsonArray jaConsumerContact = new JsonArray();
            JsonObject joConsumer = new JsonObject();
            String sms_customer = JsonObjParse.getValueEmpty(field_json_data, "sms_customer");
            joConsumer.addProperty("sms", sms_customer);
            joConsumer.addProperty("customer_nm", loggedin.getFirstname());
            joConsumer.addProperty("mobile", sessionManager.getT_Phone());
            jaConsumerContact.add(joConsumer);
            jo_footer.add("ConsumerInfo", jaConsumerContact);

            // Business Array

            JsonArray jaBusinessContact = new JsonArray();
            JsonObject joBusiness = new JsonObject();
            String sms_business = JsonObjParse.getValueEmpty(field_json_data, "sms_status");
            joBusiness.addProperty("sms", sms_business);
            String mobile_business = JsonObjParse.getValueEmpty(field_json_data, "mobile_business");
            joBusiness.addProperty("mobile", mobile_business);
            joBusiness.addProperty("customer_nm", sessionManager.getcurrentcatereraliasname());
            jaBusinessContact.add(joBusiness);

            jo_footer.add("BusinessInfo", jaBusinessContact);

            // Delivery Array

            JsonArray jaDeliveryContact = new JsonArray();
            JsonObject joDelivery = new JsonObject();

            String mobile_delivery = JsonObjParse.getValueEmpty(field_json_data, "mobile_delivery");
            joDelivery.addProperty("mobile", mobile_delivery);
            String companynm = JsonObjParse.getValueEmpty(field_json_data, "customer_nm_delivery");
            joDelivery.addProperty("customer_nm", companynm + "");
            //String pick_up_only = JsonObjParse.getValueEmpty(field_json_data, "pick_up_only");
           /* if (cbIsPickUpOnly.isChecked()) {
                joDelivery.addProperty("sms", "no");
                joDelivery.addProperty("pick_up_only", "Yes");
            } else {
                String sms_delivery = JsonObjParse.getValueEmpty(field_json_data, "sms_delivery");
                joDelivery.addProperty("sms", sms_delivery);
                joDelivery.addProperty("pick_up_only", "No");
            }*/



            String sms_delivery = JsonObjParse.getValueEmpty(field_json_data, "sms_delivery");
            joDelivery.addProperty("sms", sms_delivery);
            joDelivery.addProperty("pick_up_only", "No");

            jaDeliveryContact.add(joDelivery);
            jo_footer.add("DeliveryInfo", jaDeliveryContact);

            jo_footer.addProperty("DATE", sessionManager.getDate());
            jo_footer.addProperty("ADDRESS", sessionManager.getAddress());


            jo_footer.addProperty("Cust_Nm", sessionManager.getCustNmForProperty());

            jo_footer.addProperty("payuResponse", payuResponse);
            jo_footer.addProperty("chithashid", hashidval);


            jo_footer.addProperty("field_json_data", field_json_data);


            jObj.addProperty("footer_note", jo_footer.toString());
            Log.i("okhttp", jo_footer.toString());
            updatechit(DeliveryChargesActivity.this, sessionManager.getsession(), jObj, hashidval);

        } catch (JsonParseException e) {
            e.printStackTrace();
        }
    }



    public void updatechit(final Context c, String usersession, JsonObject chitdetails, String hashid) {

        SessionManager sessionManager = new SessionManager(c);

        ApiInterface apiService = ApiClient.getClient(c).create(ApiInterface.class);


        String currency_code = SharedPrefUserDetail.getString(c, SharedPrefUserDetail.chit_currency_code, "");
        String currency_code_id = SharedPrefUserDetail.getString(c, SharedPrefUserDetail.chit_currency_code_id, "");

        Call call = apiService.updatechit(usersession, chitdetails, sessionManager.getAggregatorprofile(), hashid, "d97be841a6d6cbb66bc3ae165b31b85f", currency_code, currency_code_id, "2");
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {


                if (response.isSuccessful()) {

                    String res = new Gson().toJson(response.body());
                    try {
                        JSONObject jsonObject = new JSONObject(res);
                        String responseStr = jsonObject.getString("response");
                        String data = JsonObjParse.getValueEmpty(responseStr, "data");
                        String first_chit_header_data_arr = JsonObjParse.getValueEmpty(data, "first_chit_header_data_arr");
                        Chit chit = new Gson().fromJson(first_chit_header_data_arr, Chit.class);
                        orderplacedsuccess(chit);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {

                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Log.e("test", " trdds1" + jObjErrorresponse.toString());
                        String msg = jObjErrorresponse.getString("errormsg");
                        if (msg.equalsIgnoreCase("To is empty")) {
                            msg = "You cannot reporting to yourselves.";
                        }
                        Toast.makeText(c, msg, Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(c, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }

            }

            @Override
            public void onFailure(Call call, Throwable t) {
                if (dialog != null) dialog.dismiss();

            }
        });
    }

    SessionManager sessionManager;

    public void orderplacedsuccess(final Chit chitOdr) {

        addTaskPreferenceForMrpFromAllTask(chitOdr, new MyCallBkCommon() {
            @Override
            public void myCallBkOnly() {

                ShareModel.getI().isFromAllTask = false;

                if (dialog != null) {
                    dialog.dismiss();
                }

                sessionManager.setcurrentcaterer("");
                sessionManager.setcurrentcaterername("");
                sessionManager.setCartproduct("");
                sessionManager.setCartcount(0);
                sessionManager.setRemark("");
                sessionManager.setDate("");
                sessionManager.setAddress("");

                sessionManager.setAggregator_ID(getString(R.string.Aggregator));

                onItemClick();


            }
        });


    }

    JsonObject joAddEdit = new JsonObject();
    MyRequestCall myRequestCall = new MyRequestCall();
    MyRequst myRequst = new MyRequst();
    JSONObject joFrom = new JSONObject(); // From should be his customer info
    JSONObject joTo = new JSONObject(); // supplier ; In To, supplier , name address etc

    public void addTaskPreferenceForMrpFromAllTask(final Chit chitOdr, final MyCallBkCommon myCallBkCommon) {

        boolean isMrp = SharedPrefUserDetail.getBoolean(this, SharedPrefUserDetail.isMrp, false);
        if (isMrp) {

        } else {
            //myCallBkCommon.myCallBkOnly();
        }

        // from all task only pass flage

        final Chit chit = SummaryDetailFragmnet.item; // from
        joAddEdit.addProperty("flag", "add");
        joAddEdit.addProperty("from_chit_hash_id", chit.getChit_hash_id());
        joAddEdit.addProperty("from_chit_id", chit.getChit_id());
        joAddEdit.addProperty("from_ref_id", chit.getRef_id());
        joAddEdit.addProperty("from_case_id", chit.getCase_id());
        joAddEdit.addProperty("from_purpose", chit.getPurpose());
        joAddEdit.addProperty("from_field_json_data", joFrom.toString());

        joAddEdit.addProperty("to_bridge_id", chitOdr.getTo_bridge_id());
        joAddEdit.addProperty("to_chit_hash_id", chitOdr.getChit_hash_id());
        joAddEdit.addProperty("to_chit_id", chitOdr.getChit_id());
        joAddEdit.addProperty("to_ref_id", chitOdr.getRef_id());
        joAddEdit.addProperty("to_case_id", chitOdr.getCase_id());
        joAddEdit.addProperty("to_purpose", chitOdr.getPurpose());

        joAddEdit.addProperty("to_field_json_data", joTo.toString());
        joAddEdit.addProperty("task_purpose", purpose);


        JsonObject joMrp = new JsonObject();
        JsonArray jsonElements = new JsonArray();
        jsonElements.add(joAddEdit);

        joMrp.add("newdata", jsonElements);

        myRequst.cookie = sessionManager.getsession();

        myRequestCall.addTaskPreferencr(this, myRequst, joMrp, new MyRequestCall.CallRequest() {
            @Override
            public void onGetResponse(String response) {
                Constants.chit_id_from_mrp = chit.getChit_id();

                myCallBkCommon.myCallBkOnly();
            }
        });
    }

    public void onItemClick() {

        Chit item = SummaryDetailFragmnet.item;

        Intent i = new Intent(this, ChitlogActivity.class);
        i.putExtra("chit_hash_id", item.getChit_hash_id());
        i.putExtra("chit_id", item.getChit_id() + "");
        i.putExtra("transtactionid", item.getChit_name() + "");
        i.putExtra("productqty", item.getTotal_chit_item_value() + "");
        i.putExtra("productprice", item.getChit_item_count() + "");
        i.putExtra("purpose", item.getPurpose());
        i.putExtra("Refid", item.getRef_id());
        i.putExtra("Caseid", item.getCase_id());
        i.putExtra("isprint", true);
        //if (tab.equalsIgnoreCase("new_all"))
        i.putExtra("isMrp", true);

        i.putExtra("iscomment", true);

        ShareModel.getI().isFromAllTask = true;

        ShareModel.getI().isBackFromChitLog = true;

        //Constants.chit_id_to = Constants.chit_id_to;
        DetailsFragment.chititemcount = "" + item.getChit_item_count();
        DetailsFragment.chititemprice = "" + item.getTotal_chit_item_value();
        SummaryDetailFragmnet.item = item;
        DetailsFragment.item_detail = item;
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
        finish();
    }

    @Override
    public void onBackPressed() {
        if (llStepOne.getVisibility() == View.VISIBLE) {
            super.onBackPressed();
        } else if (llStepSecond.getVisibility() == View.VISIBLE) {
            llStepOne.setVisibility(View.VISIBLE);
            llStepSecond.setVisibility(View.GONE);
            llStepThird.setVisibility(View.GONE);
            llStepFour.setVisibility(View.GONE);
            llStepFive.setVisibility(View.GONE);
            usage_stateprogressbar.setCurrentStateNumber(StateProgressBar.StateNumber.ONE);
        } else if (llStepThird.getVisibility() == View.VISIBLE) {
            llStepOne.setVisibility(View.GONE);
            llStepSecond.setVisibility(View.VISIBLE);
            llStepThird.setVisibility(View.GONE);
            llStepFour.setVisibility(View.GONE);
            llStepFive.setVisibility(View.GONE);
            usage_stateprogressbar.setCurrentStateNumber(StateProgressBar.StateNumber.TWO);
        } else if (llStepFour.getVisibility() == View.VISIBLE) {
            llStepOne.setVisibility(View.GONE);
            llStepSecond.setVisibility(View.GONE);
            llStepThird.setVisibility(View.VISIBLE);
            llStepFour.setVisibility(View.GONE);
            llStepFive.setVisibility(View.GONE);
            usage_stateprogressbar.setCurrentStateNumber(StateProgressBar.StateNumber.THREE);
        } else if (llStepFive.getVisibility() == View.VISIBLE) {
            llStepOne.setVisibility(View.GONE);
            llStepSecond.setVisibility(View.GONE);
            llStepThird.setVisibility(View.GONE);
            llStepFour.setVisibility(View.VISIBLE);
            llStepFive.setVisibility(View.GONE);
            usage_stateprogressbar.setCurrentStateNumber(StateProgressBar.StateNumber.FOUR);
        }

    }

    Userprofile userProfileData;

    public void setTaxFromProfile() {

        try {
            setTax(userProfileData.field_json_data);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    Double stategst = 0.0;
    Double centralgst = 0.0;

    public void setTax(String field_json_data) throws JSONException {

        Double dis_price = 0.0;
        Double dis_perc = 0.0;

       /* if (!obj.getDiscountedPrice().equals(""))
            dis_price = Double.valueOf(obj.getDiscountedPrice());
        if (!obj.getDiscountPercentage().equals(""))
            dis_perc = Double.valueOf(obj.getDiscountPercentage());*/

        if (dis_perc == 0) {
            et_dis_per.setText("");
        } else {
            et_dis_per.setText(Inad.getCurrencyDecimal(dis_perc, this) + "");
        }

        if (dis_price == 0) {
            et_dis_val.setText("");
        } else {
            et_dis_val.setText(Inad.getCurrencyDecimal(dis_price, this) + "");
        }

        Double sgst = 0.0;
        Double cgst = 0.0;

        if (field_json_data != null) {

            String Tax = JsonObjParse.getValueEmpty(field_json_data, "Tax");
            String dis_perc_from_field = JsonObjParse.getValueEmpty(field_json_data, "dis_perc");
            if (!dis_perc_from_field.isEmpty()) // if update then empty
            {
                et_dis_per.setText(dis_perc_from_field);
            }

            if (!Tax.isEmpty()) { // Add New

                JSONObject joTax = new JSONObject(field_json_data);

                JSONArray arr = joTax.getJSONArray("Tax");

                JSONObject element;

                et_cgst.setText("");
                tvTaxField1.setText("");
                et_sgst.setText("");
                tvTaxField2.setText("");

                llCgst.setVisibility(View.GONE);
                llSgst.setVisibility(View.GONE);

                for (int i = 0; i < arr.length(); i++) {
                    element = arr.getJSONObject(i); // which for example will be Types,TotalPoints,ExpiringToday in the case of the first array(All_Details)

                    Iterator keys = element.keys();

                    while (keys.hasNext()) {
                        try {
                            String key = (String) keys.next();

                            String taxVal = JsonObjParse.getValueFromJsonObj(element, key);

                            if (et_cgst.getText().toString().isEmpty()) {
                                //sgst = Double.valueOf(jo_tax.getString("SGST"));
                                //cgst = Double.valueOf(jo_tax.getString("CGST"));
                                cgst = Double.valueOf(taxVal);
                                et_cgst.setText(taxVal);
                                tvTaxField1.setText(key);
                                llCgst.setVisibility(View.VISIBLE);

                            } else {
                                sgst = Double.valueOf(taxVal);
                                et_sgst.setText(taxVal);
                                tvTaxField2.setText(key);
                                llSgst.setVisibility(View.VISIBLE);
                            }

                            Log.e("key", key);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            } else { // Update
                et_cgst.setText("");
                tvTaxField1.setText("");
                et_sgst.setText("");
                tvTaxField2.setText("");

                llCgst.setVisibility(View.GONE);
                llSgst.setVisibility(View.GONE);

                if (!field_json_data.equalsIgnoreCase("[]")) {

                    JSONObject joTax = new JSONObject(field_json_data); // :. field and tax is same
                    Iterator keys = joTax.keys();

                    while (keys.hasNext()) {
                        try {
                            String key = (String) keys.next();

                            String taxVal = JsonObjParse.getValueFromJsonObj(joTax, key);

                            if (et_cgst.getText().toString().isEmpty()) {
                                //sgst = Double.valueOf(jo_tax.getString("SGST"));
                                //cgst = Double.valueOf(jo_tax.getString("CGST"));
                                cgst = Double.valueOf(taxVal);
                                et_cgst.setText(taxVal);
                                tvTaxField1.setText(key);
                                llCgst.setVisibility(View.VISIBLE);

                            } else {
                                sgst = Double.valueOf(taxVal);
                                et_sgst.setText(taxVal);
                                tvTaxField2.setText(key);
                                llSgst.setVisibility(View.VISIBLE);
                            }

                            Log.e("key", key);

                        } catch (Exception e) {

                            e.printStackTrace();
                        }
                    }
                }
            }
        }

        if (cgst == 0) {
            et_cgst.setText("");
        } else {
            et_cgst.setText(Inad.getCurrencyDecimal(cgst, this) + "");
        }
        if (sgst == 0) {
            et_sgst.setText("");
        } else {
            et_sgst.setText(Inad.getCurrencyDecimal(sgst, this) + "");
        }


        Double mrp = 0.0;
        mrp = charge;

        Double dis_mrp = Double.valueOf(getdiscountmrp(mrp, dis_perc, dis_price).toString());
        if (dis_mrp.doubleValue() != mrp.doubleValue()) {
            // tv_calc_dis.setText(getString(R.string.currency) + String.format("%.2f", dis_mrp));
            tv_calc_dis.setText(currency + Inad.getCurrencyDecimal(dis_mrp, this));
        } else {
            //tv_calc_dis.setVisibility(View.GONE);
        }
        // tv_calc_dis.setText(getString(R.string.currency) + String.format("%.2f", dis_mrp));
        tv_calc_dis.setText(currency + Inad.getCurrencyDecimal(dis_mrp, this));

        stategst = (dis_mrp / 100.0f) * sgst;
        centralgst = (dis_mrp / 100.0f) * cgst;

        if (mrp == 0) {
            et_mrp.setText("");
        } else {
            et_mrp.setText(Inad.getCurrencyDecimal(mrp, this) + "");
        }

        Double total = dis_mrp + stategst + centralgst;
        if (total == 0) {
            tv_total.setText("");
        } else {
            tv_total.setText("Total : " + currency + Inad.getCurrencyDecimal(total, this) + "" + "");
        }

    }

    public String getdiscountmrp(Double mrpprice, Double discount_percentage, Double discounted_price) {
        Double discountprice = mrpprice;
        //  tvTotalDiscPerc.setText("");

        if (discount_percentage > 0) {
            if (mrpprice > 0) {
                Double totalDisc = (mrpprice * (discount_percentage / 100));
                // tvTotalDiscPerc.setText("- " + currency + Inad.getCurrencyDecimal(totalDisc, this));
                discountprice = mrpprice - totalDisc;
            }
        } else if (discounted_price > 0) {
            Double totalDisc = mrpprice - discounted_price;
            //  tvTotalDiscPerc.setText("- " + currency + Inad.getCurrencyDecimal(totalDisc, this));

            discountprice = discounted_price;
        }

        return String.valueOf(discountprice);
    }

    Double total = 0.0;

    public void calc_tax_value() {
        try {
            String price = et_mrp.getText().toString().trim();

            Double mrp = 0.0;
            Double addMrp = 0.0;
            if (!price.isEmpty()) mrp = Double.valueOf(price);

            Double extraInfoPrice = 0.0, dedPriceForJewel = 0.0, additionalPriceForJewel = 0.0;
            Double ded_mrp = 0.0, additional_mrp = 0.0;


            mrp = mrp + extraInfoPrice;

            Double dis_price = 0.0;
            Double dis_perc = 0.0;

            String dis_val = et_dis_val.getText().toString();
            if (!dis_val.isEmpty()) {
                dis_price = Double.parseDouble(et_dis_val.getText().toString());
            }

            String dis_per = et_dis_per.getText().toString();
            if (!dis_per.isEmpty()) {
                dis_perc = Double.parseDouble(et_dis_per.getText().toString());
            }

            Double dis_mrp = Double.valueOf(getdiscountmrp(mrp, dis_perc, dis_price).toString());


            if (dis_mrp.doubleValue() != mrp.doubleValue()) {
                tv_calc_dis.setVisibility(View.VISIBLE);
                //tv_calc_dis.setText(currency + Inad.getCurrencyDecimal(dis_mrp, this));
            } else {

                //tv_calc_dis.setVisibility(View.GONE);
            }

           /* if (myDoctorData.getList().get(spn_template.getSelectedItemPosition()).getTemplateName().equalsIgnoreCase("RPD")) {
                dis_mrp = dis_mrp - ded_mrp + additional_mrp + addMrp;
            } else {
                dis_mrp = dis_mrp - ded_mrp + additional_mrp;
            }*/
            dis_mrp = dis_mrp - ded_mrp + additional_mrp;

            tvTotalWoTax.setText("Sub Total : " + currency + Inad.getCurrencyDecimal(dis_mrp, this));
            removeStrikeLineIfNoDiscValuew(tv_calc_dis);
            if (dis_perc == 0 && dis_price > 0) {
                strikeLineIfDiscValuew(tv_calc_dis);
                tvTotalDiscPerc.setText("+ " + currency + Inad.getCurrencyDecimal(dis_price, this));
            }

            tv_calc_dis.setText(currency + Inad.getCurrencyDecimal(mrp, this));

            /******************** Tax from Discount *************************/

            Double sgst = 0.0;
            Double cgst = 0.0;
            String s_cgst = et_cgst.getText().toString();
            if (!s_cgst.isEmpty()) cgst = Double.parseDouble(et_cgst.getText().toString());

            String s_sgst = et_sgst.getText().toString();
            if (!s_sgst.isEmpty()) sgst = Double.parseDouble(et_sgst.getText().toString());

            Double stategst = (dis_mrp / 100.0f) * sgst;
            Double centralgst = (dis_mrp / 100.0f) * cgst;

            if (centralgst == 0) {
                tv_calc_ctax.setText("");
            } else {
                tv_calc_ctax.setText("+ " + currency + Inad.getCurrencyDecimal(centralgst, this));
            }
            if (stategst == 0) {
                tv_calc_stax.setText("");
            } else {
                tv_calc_stax.setText("+ " + currency + Inad.getCurrencyDecimal(stategst, this));
            }

            if (dis_mrp > 0) {
                total = dis_mrp + stategst + centralgst;
                if (total == 0) {
                    tv_total.setText("");
                } else {
                    tv_total.setText("Total : " + currency + Inad.getCurrencyDecimal(total, this) + "" + "");
                }
            } else {
                total = mrp + stategst + centralgst;
                if (total == 0) {
                    tv_total.setText("");
                } else {
                    tv_total.setText("Total : " + currency + Inad.getCurrencyDecimal(total, this) + "" + "");
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        billamount();
    }

    public void strikeLineIfDiscValuew(TextView textView) {
        textView.setPaintFlags(textView.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
    }

    public void removeStrikeLineIfNoDiscValuew(TextView textView) {
        textView.setPaintFlags(textView.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
    }

    public void billamount() {

        alTax = new ArrayList<>();

        ArrayList<Catelog> productlist = new ArrayList<>();
        productlist = dbHelper.getAllcaretproduct();
        for (int i = 0; i < productlist.size(); i++) {
            int qty = productlist.get(i).getCartcount();
            Double bill = Double.valueOf(productlist.get(i).getdiscountmrp());  // mrp

            String doctor_template_type = JsonObjParse.getValueEmpty(productlist.get(i).getInternal_json_data(), "doctor_template_type");

            // For Calc Disc of property
            Double mrp = Double.valueOf(productlist.get(i).getMrp());  // mrp
            String property = JsonObjParse.getValueEmpty(productlist.get(i).getInternal_json_data(), "property");
            if (!property.isEmpty()) { // property always 1
                String sqFt = JsonObjParse.getValueEmpty(property, "proprty_sqft");
                if (!sqFt.isEmpty()) {
                    int sqFtVal = Integer.parseInt(sqFt);
                    mrp = mrp * sqFtVal;
                }
                try {
                    String ExtraInfoPrice = JsonObjParse.getValueEmpty(property, "ExtraInfoPrice");
                    mrp = mrp + getExtraPropertyInfo(ExtraInfoPrice);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            Double ded_price_val = 0.0, addi_price_val = 0.0;
            Double ded_perc_val = 0.0, addi_perc_val = 0.0;
            Double ded_mrp = 0.0, addi_mrp = 0.0;

            Catelog catelog = productlist.get(i);

            String jewel = JsonObjParse.getValueEmpty(productlist.get(i).getInternal_json_data(), "jewel");
            if (!jewel.isEmpty()) {


                String metalPosId = JsonObjParse.getValueEmpty(jewel, "metalPosId");
                int posId = 0;
                if (!metalPosId.isEmpty()) posId = Integer.parseInt(metalPosId);

                try {
                    if (ShareModel.getI().metal.alMetal.size() > posId) {
                        Metal metal = ShareModel.getI().metal.alMetal.get(posId);
                        if (!metal.metalPrice.isEmpty()) {
                            mrp = Double.valueOf(metal.metalPrice);
                        }
                    } else {
                        Log.d("okhttp jewel", catelog.getName() + "");
                    }

                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }

                String weight = JsonObjParse.getValueEmpty(jewel, "weight");

                if (!weight.isEmpty()) {
                    float wt = Float.parseFloat(weight);
                    //dis_mrp = (dis_mrp * wt)/10; // 10 gram
                    mrp = (mrp * wt) / 1; // 1 unit
                }
                try {
                    String DeductionJewelPrice = JsonObjParse.getValueEmpty(jewel, "DeductionJewelPrice");
                    ded_price_val = catelog.getAdditionalDeductinalInfo(DeductionJewelPrice, mrp);
                    ded_mrp = Double.valueOf(catelog.getValues(mrp, ded_perc_val, ded_price_val));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                try {
                    String AdditionalJewelPrice = JsonObjParse.getValueEmpty(jewel, "AdditionalJewelPrice");
                    addi_price_val = catelog.getAdditionalDeductinalInfo(AdditionalJewelPrice, mrp);
                    addi_mrp = Double.valueOf(catelog.getValues(mrp, addi_perc_val, addi_price_val));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                bill = bill - ded_mrp + addi_mrp;
                mrp = mrp - ded_mrp + addi_mrp;


            }

            if (doctor_template_type.equalsIgnoreCase("RPD") || doctor_template_type.equalsIgnoreCase("Precision Attachment")) {
                Double Amrp = 0.0;
                Log.d("additional", JsonObjParse.getValueEmpty(productlist.get(i).getInternal_json_data(), "additionalPrice_rpd") + "");
                if (!JsonObjParse.getValueEmpty(productlist.get(i).getInternal_json_data(), "additionalPrice_rpd").isEmpty()) {
                    Amrp = Double.valueOf(JsonObjParse.getValueEmpty(productlist.get(i).getInternal_json_data(), "additionalPrice_rpd"));
                    bill = bill + Amrp * (qty - 1);
                    mrp = mrp + Amrp * (qty - 1);
                }
            } else {
                bill = bill * qty;
                mrp = mrp * qty;
            }

            // calc disc of each product
            mrp = mrp - bill;
            // totalProductDisc = totalProductDisc + mrp;

            // totalbill = totalbill + bill;
            Log.e("totalbil", productlist.get(i).getdiscountmrp());
            Log.e("qty", String.valueOf(qty));

            Double sgst = 0.0;
            Double cgst = 0.0;
            Double vat = 0.0;

            try {
                String tax = productlist.get(i).getTax_json_data();
                if (tax != null) {
                   /* JSONObject obj = new JSONObject(tax);
                    if (obj.has("VAT")) {
                        vat = Double.valueOf(obj.getString("VAT"));
                    } else {
                        sgst = Double.valueOf(obj.getString("SGST"));
                        cgst = Double.valueOf(obj.getString("CGST"));
                    }*/

                    boolean isF1 = true;

                    JSONObject joTax = new JSONObject(tax);

                    Iterator keys = joTax.keys();

                    while (keys.hasNext()) {

                        try {
                            String key = (String) keys.next();

                            String taxVal = JsonObjParse.getValueFromJsonObj(joTax, key);


                            if (isF1) {

                                isF1 = false;
                                sgst = Double.valueOf(taxVal);

                                Double tax1 = (bill / 100.0f) * sgst;
                                boolean isExist = false;
                                for (int k = 0; k < alTax.size(); k++) {
                                    String tmpTaxVal = alTax.get(k).taxValPercentage;
                                    String tmpKey = alTax.get(k).taxKey;
                                    if (tmpKey.equals(key) && taxVal.equals(tmpTaxVal)) {
                                        isExist = true;
                                        alTax.get(k).taxVal = alTax.get(k).taxVal + tax1;
                                        break;
                                    }
                                }
                                if (!isExist && tax1 > 0) {
                                    alTax.add(new TaxGroupModel(tax1, key, taxVal));
                                }


                            } else {
                                cgst = Double.valueOf(taxVal);

                                Double tax2 = (bill / 100.0f) * cgst;
                                boolean isExist = false;
                                for (int k = 0; k < alTax.size(); k++) {
                                    String tmpTaxVal = alTax.get(k).taxValPercentage;
                                    String tmpKey = alTax.get(k).taxKey;
                                    if (tmpKey.equals(key) && taxVal.equals(tmpTaxVal)) {
                                        isExist = true;
                                        alTax.get(k).taxVal = alTax.get(k).taxVal + tax2;
                                        break;
                                    }
                                }
                                if (!isExist && tax2 > 0) {
                                    alTax.add(new TaxGroupModel(tax2, key, taxVal));
                                }

                            }

                            Log.e("key", key);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                }

                Log.e("stategst", String.valueOf(sgst));
                Log.e("stategst", String.valueOf(cgst));
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Double totalamt = bill;
            Double stategst = (totalamt / 100.0f) * sgst;
            Log.e("stategst", String.valueOf(stategst));
            //  totalstategst = totalstategst + stategst;

            Double centralgst = (totalamt / 100.0f) * cgst;
            Log.e("stategst", String.valueOf(centralgst));
            // totalcentralgst = totalcentralgst + centralgst;

            Double valueAt = (totalamt / 100.0f) * vat;
            Log.e("valueAt", String.valueOf(valueAt));
            //  totalVat = totalVat + valueAt;
        }

       /* checkDiscount();

        grandtotal = totalbill + totalcentralgst + totalstategst + totalVat - totalReqDisc;

        //centralgstamount.setText(currency + String.format("%.2f", totalcentralgst));
        centralgstamount.setText(currency + Inad.getCurrencyDecimal(totalcentralgst, this));
        //stategstamount.setText(currency + String.format("%.2f", totalstategst));
        stategstamount.setText(currency + Inad.getCurrencyDecimal(totalstategst, this));
        tvVat.setText(currency + Inad.getCurrencyDecimal(totalVat, this));
        //billamount.setText(currency + String.format("%.2f", totalbill));
        billamount.setText(currency + Inad.getCurrencyDecimal(totalbill, this));
        //grandtotalamount.setText(currency + String.format("%.2f", grandtotal));
        grandtotalamount.setText(currency + Inad.getCurrencyDecimal(grandtotal, this));

        llCgst.setVisibility(View.GONE);
        llSgst.setVisibility(View.GONE);
        llVat.setVisibility(View.GONE);

        if (totalVat.doubleValue() == 0.0) {
            llVat.setVisibility(View.GONE);
        }
        if (totalcentralgst.doubleValue() == 0.0) llCgst.setVisibility(View.GONE);
        if (totalstategst.doubleValue() == 0.0) llSgst.setVisibility(View.GONE);

        setCharges();
       *//* String.format("%.2f", grandtotal);
        Log.e("totalstategst", String.valueOf(totalstategst));
        Log.e("final", String.valueOf(totalbill));*//*
         */
        try {
            Thread.sleep(50);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public int getExtraPropertyInfo(String extraInfo) throws JSONException {
        int totalInfoVal = 0;
        if (extraInfo != null && !extraInfo.isEmpty()) {

            JSONArray jaExtraInfo = new JSONArray(extraInfo);

            for (int i = 0; i < jaExtraInfo.length(); i++) {
                JSONObject jsonObject = jaExtraInfo.getJSONObject(i);

                Iterator keys = jsonObject.keys();
                while (keys.hasNext()) {
                    try {
                        String key = (String) keys.next();
                        String info = JsonObjParse.getValueFromJsonObj(jsonObject, key);

                        if (!info.isEmpty()) {
                            int infoVal = Integer.parseInt(info);
                            totalInfoVal = totalInfoVal + infoVal;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }
        }

        return totalInfoVal;
    }

    public void locationinitialized() {

        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(Places.GEO_DATA_API)
                    .enableAutoManage(this, GOOGLE_API_CLIENT_ID, new GoogleApiClient.OnConnectionFailedListener() {
                        @Override
                        public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

                        }
                    })
                    .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                        @Override
                        public void onConnected(@Nullable Bundle bundle) {
                            mPlaceArrayAdapter.setGoogleApiClient(mGoogleApiClient);
                        }

                        @Override
                        public void onConnectionSuspended(int i) {
                            mPlaceArrayAdapter.setGoogleApiClient(null);
                        }
                    })
                    .build();
        }

        tv_google_loc.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count > 0) {
                    findViewById(R.id.iv_clear).setVisibility(View.VISIBLE);
                } else {
                    findViewById(R.id.iv_clear).setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        tv_google_loc_to.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count > 0) {
                    findViewById(R.id.iv_clear_to).setVisibility(View.VISIBLE);
                } else {
                    findViewById(R.id.iv_clear_to).setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mPlaceArrayAdapter = new PlaceArrayAdapter(this, android.R.layout.simple_list_item_1,
                BOUNDS_MOUNTAIN_VIEW, null);

        tv_google_loc.setAdapter(mPlaceArrayAdapter);
        tv_google_loc_to.setAdapter(mPlaceArrayAdapter);
    }

    private GoogleApiClient mGoogleApiClient;

    private PlaceArrayAdapter mPlaceArrayAdapter;
    private static final LatLngBounds BOUNDS_MOUNTAIN_VIEW = new LatLngBounds(
            new LatLng(37.398160, -122.180831), new LatLng(37.430610, -121.972090));
    private static final int GOOGLE_API_CLIENT_ID = 0;

    private AdapterView.OnItemClickListener mAutocompleteClickListener
            = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            final PlaceArrayAdapter.PlaceAutocomplete item = mPlaceArrayAdapter.getItem(position);
            final String placeId = String.valueOf(item.placeId);
            //Log.i(LOG_TAG, "Selected: " + item.description);
            PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                    .getPlaceById(mGoogleApiClient, placeId);
            placeResult.setResultCallback(mUpdatePlaceDetailsCallback);
            //Log.i(LOG_TAG, "Fetching details for ID: " + item.placeId);
        }
    };

    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback
            = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {

                return;
            }
            final Place place = places.get(0);
            CharSequence attributions = places.getAttributions();
        }
    };

}