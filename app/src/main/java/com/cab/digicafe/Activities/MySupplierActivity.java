package com.cab.digicafe.Activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TextInputLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.andremion.counterfab.CounterFab;
import com.cab.digicafe.Activities.RequestApis.MyRequestCall;
import com.cab.digicafe.Activities.RequestApis.MyRequst;
import com.cab.digicafe.Activities.delivery.DeliveryChargesActivity;
import com.cab.digicafe.Adapter.MySupplierAdapter;
import com.cab.digicafe.Apputil;
import com.cab.digicafe.BaseActivity;
import com.cab.digicafe.CatelogActivity;
import com.cab.digicafe.ChangepasswordActivity;
import com.cab.digicafe.Database.SqlLiteDbHelper;
import com.cab.digicafe.Dialogbox.CustomDialogClass;
import com.cab.digicafe.Dialogbox.FilterPurposeDialog;
import com.cab.digicafe.Dialogbox.SearchSupplier;
import com.cab.digicafe.Dialogbox.SentSmsConfirmDialogue;
import com.cab.digicafe.Dialogbox.UpdateDialogClass;
import com.cab.digicafe.Helper.Constants;
import com.cab.digicafe.Helper.Inad;
import com.cab.digicafe.Helper.SessionManager;
import com.cab.digicafe.Helper.SharedPrefUserDetail;
import com.cab.digicafe.Model.Metal;
import com.cab.digicafe.Model.ModelFile;
import com.cab.digicafe.Model.ShareModel;
import com.cab.digicafe.Model.SupplierNew;
import com.cab.digicafe.Model.Userprofile;
import com.cab.digicafe.MyCustomClass.CheckVersion;
import com.cab.digicafe.MyCustomClass.JsonObjParse;
import com.cab.digicafe.MyCustomClass.ModelDevice;
import com.cab.digicafe.MyCustomClass.PlayAnim;
import com.cab.digicafe.MyCustomClass.RefreshSession;
import com.cab.digicafe.OrderActivity;
import com.cab.digicafe.ProfileActivity;
import com.cab.digicafe.R;
import com.cab.digicafe.Rest.ApiClient;
import com.cab.digicafe.Rest.ApiInterface;
import com.cab.digicafe.Rest.Request.SupplierRequest;
import com.cab.digicafe.Timeline.DateTimeUtils;
import com.cab.digicafe.serviceTypeClass.GetServiceTypeFromBusiUserProfile;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.cab.digicafe.MyCustomClass.Constants.share_shop_node;

public class MySupplierActivity extends BaseActivity implements SearchView.OnQueryTextListener {

    @BindView(R.id.counter_fab)
    CounterFab counterFab;

    private List<SupplierNew> catererlist = new ArrayList<>();
    private RecyclerView recyclerView;
    private MySupplierAdapter mAdapter;
    private ProgressBar loadingview;
    private SessionManager sessionManager;

    @BindView(R.id.tv_dummy)
    TextView tv_dummy;

    @BindView(R.id.tv_empty)
    TextView tv_empty;
    boolean fromAT = false;
   public static boolean fromMrp = false;

    boolean isClk = false;

    @BindView(R.id.llMore)
    RelativeLayout llMore;
    @BindView(R.id.tvEdit)
    TextView tvEdit;
    @BindView(R.id.tvChangePass)
    TextView tvChangePass;
    @BindView(R.id.tvLogout)
    TextView tvLogout;

    public static boolean isAppHyperlocalMode = true;
    public static boolean isFavourite = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.activity_my_supplier, frameLayout);
        ButterKnife.bind(this);

        tv_empty.setText("Loading..");

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            fromAT = extras.getBoolean("fromAT", false);
            fromMrp = extras.getBoolean("isFromMrp", false);
            isFavourite = extras.getBoolean("isFavourite", false);
        }

        isAppHyperlocalMode = SharedPrefUserDetail.getBoolean(this, SharedPrefUserDetail.isAppHyperlocalMode, true); // pass flag

        SharedPrefUserDetail.setBoolean(this, SharedPrefUserDetail.isMrp, fromAT);

        rlAddCategory.setVisibility(View.GONE);

        SharedPrefUserDetail.setBoolean(this, SharedPrefUserDetail.isfromsupplier, true);

        dbHelper = new SqlLiteDbHelper(this);

        GetAddressBase("");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        sessionManager = new SessionManager(this);

        if (sessionManager.getIsConsumer() && !isAppHyperlocalMode) {
            if (isFavourite) {
                getSupportActionBar().setTitle("My Favourite");
            } else {
                getSupportActionBar().setTitle("Shop");
            }

        } else {
            getSupportActionBar().setTitle("My Supplier");
        }

        if (fromAT) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        //counterFab.setVisibility(View.GONE);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        loadingview = (ProgressBar) findViewById(R.id.loadingview);
        loadingview.setVisibility(View.VISIBLE);

        mAdapter = new MySupplierAdapter(catererlist, new MySupplierAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(SupplierNew item, int pos) {

                String a = new Gson().toJson(item);

                Intent i = new Intent(MySupplierActivity.this, AddSupplierActivity.class);
                i.putExtra("bridge", a);
                i.putExtra("isadd", false);
                startActivityForResult(i, 10);
            }

            @Override
            public void onShopClick(SupplierNew item, int pos) {
                if (!isClk) {
                    isClk = true;
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            isClk = false;
                        }
                    }, 500);
                    checkversioncode(item);
                }
            }

            @Override
            public void onItemLongClick(SupplierNew item, int pos) {
                if (sessionManager.getIsConsumer() && !isAppHyperlocalMode) {
                    return;
                } else if (sessionManager.getIsConsumer() && !isAppHyperlocalMode) {
                    return;
                } else {

                }

                if (catererlist.get(pos).isIslongpress()) {
                    catererlist.get(pos).setIslongpress(false);
                } else {
                    catererlist.get(pos).setIslongpress(true);
                }

                mAdapter.notifyDataSetChanged();

                str_actionid = "";
                totalcount = 0;
                for (int i = 0; i < catererlist.size(); i++) {
                    if (catererlist.get(i).isIslongpress()) {
                        totalcount++;
                        str_actionid = str_actionid + String.valueOf(catererlist.get(i).getUser_bridge_contacts_id()) + ",";
                    }
                }

                if (totalcount > 0) {
                    showdel(false);
                } else {
                    showdel(true);
                }
            }

            @Override
            public void onDisplyLongDesc(SupplierNew item) {

                if (!item.getLocation().trim().isEmpty()) {
                    String open_time = JsonObjParse.getValueEmpty(item.getField_json_data(), "open_time");
                    String close_time = JsonObjParse.getValueEmpty(item.getField_json_data(), "close_time");
                    String work_hr_msg = JsonObjParse.getValueEmpty(item.getField_json_data(), "work_hr_msg");
                    String msg = "";
                    if (!open_time.isEmpty()) msg = "Open time : " + open_time + "\n";
                    if (!close_time.isEmpty()) msg = msg + "Close time : " + close_time + "\n";
                    if (!work_hr_msg.isEmpty()) msg = msg + "Note : " + work_hr_msg;
                    showInfo(item.getFirstname(), "Location : " + item.getLocation() + "\n" + msg);
                }
            }

            @Override
            public void onDisplyInfo(SupplierNew item) {
                String disc_info = JsonObjParse.getValueEmpty(item.getField_json_data(), "disc_info");
                showInfo("" + item.getFirstname(), "Discount : " + disc_info + "");
            }

            @Override
            public void onDisplayRefer(SupplierNew item) {
                showInfo("" + item.getFirstname() + " - share link open by :", item.getMyReferal());
            }
        }, this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView.setAdapter(mAdapter);

        mSwipyRefreshLayout = (SwipyRefreshLayout) findViewById(R.id.swipyrefreshlayout);
        mSwipyRefreshLayout.setOnRefreshListener(new SwipyRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(SwipyRefreshLayoutDirection direction) {
                Log.e("scroll direction", "Refresh triggered at "
                        + (direction == SwipyRefreshLayoutDirection.TOP ? "top" : "bottom"));
                if (direction == SwipyRefreshLayoutDirection.TOP) {
                    isdataavailable = true;
                    pagecount = 1;
                    loadfirstpage();
                } else if (direction == SwipyRefreshLayoutDirection.BOTTOM) {
                    if (isdataavailable) {
                        pagecount++;
                        usersignin();
                    } else {
                        mSwipyRefreshLayout.setRefreshing(false);
                        Toast.makeText(MySupplierActivity.this, "No more data available", Toast.LENGTH_LONG).show();
                    }

                }
            }
        });

        counterFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (counterFab.getCount() > 0) {

                    boolean isGo = true;
                    if (service_type_of.equalsIgnoreCase(getString(R.string.shopping_cart_cb))) {

                        String min = JsonObjParse.getValueEmpty(field_json_data, "min");
                        int minI = Integer.parseInt(min);
                        if (dbHelper.getcartcount() < minI) {
                            isGo = false;
                            Toast.makeText(MySupplierActivity.this, "Select at least " + min + " items", Toast.LENGTH_LONG).show();
                        } else {
                            isGo = true;
                        }

                    } else {
                    }

                    if (isGo) {
                        Intent i = new Intent(MySupplierActivity.this, OrderActivity.class);
                        i.putExtra("session", sessionManager.getsession());
                        //i.putExtra("bridgeidval", currentuser.getBridge_id());
                        i.putExtra("frombridgeidval", sessionManager.getcurrentcaterer());
                        i.putExtra("aliasname", sessionManager.getcurrentcaterername());
                        startActivity(i);
                    }

                } else {
                    Apputil.showalert("Empty Cart!", MySupplierActivity.this);
                }
            }
        });

        tvLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShareModel.getI().dbUnderMaster = "";

                ShareModel.getI().metal = new Metal();
                Constants.chit_id_from_mrp = 0;

                sessionManager.setAggregator_ID(getString(R.string.Aggregator));
                SharedPrefUserDetail.setBoolean(MySupplierActivity.this, SharedPrefUserDetail.isfromsupplier, false);
                SharedPrefUserDetail.setString(MySupplierActivity.this, SharedPrefUserDetail.isfrommyaggregator, "");
                SharedPrefUserDetail.setBoolean(MySupplierActivity.this, SharedPrefUserDetail.isMrp, false); // pass flag
                logoutuser();

                llMore.setVisibility(View.GONE);
            }
        });

        tvChangePass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShareModel.getI().dbUnderMaster = "";

                ShareModel.getI().metal = new Metal();
                Constants.chit_id_from_mrp = 0;

                sessionManager.setAggregator_ID(MySupplierActivity.this.getString(R.string.Aggregator));
                SharedPrefUserDetail.setBoolean(MySupplierActivity.this, SharedPrefUserDetail.isfromsupplier, false);
                SharedPrefUserDetail.setString(MySupplierActivity.this, SharedPrefUserDetail.isfrommyaggregator, "");
                SharedPrefUserDetail.setBoolean(MySupplierActivity.this, SharedPrefUserDetail.isMrp, false); // pass flag

                Intent i = new Intent(MySupplierActivity.this, ChangepasswordActivity.class);
                i.putExtra("session", sessionManager.getsession());
                startActivity(i);
                llMore.setVisibility(View.GONE);
            }
        });

        tvEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MySupplierActivity.this, ProfileActivity.class);
                i.putExtra("session", sessionManager.getsession());
                startActivity(i);
                llMore.setVisibility(View.GONE);
            }
        });

        llMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (llMore.getVisibility() == View.VISIBLE) {
                    llMore.setVisibility(View.GONE);
                }
            }
        });

        purpose = SharedPrefUserDetail.getString(MySupplierActivity.this, SharedPrefUserDetail.purpose_mysupplier, "request");
        SharedPrefUserDetail.setString(MySupplierActivity.this, SharedPrefUserDetail.purpose, purpose);

        String notes = getString(R.string.dummy);
        {
            notes = notes.replaceAll("Notes:", "<font color='#DA482F'><b>" + "Notes: " + "</b></font>");
        }
        tv_dummy.setText(Html.fromHtml(notes));

        ref = getReferer(this);


        if (ref != null && !ref.isEmpty()) {
            MyRequst myRequst = new MyRequst();
            myRequst.cookie = sessionManager.getsession();

            String[] ref_list = ref.split("-");
            String user = "";
            if (ref_list.length > 0) {
                ref = ref_list[0];
            }

            if (ref_list.length > 1) {
                user = ref_list[1];
                user = user.replace("@", "_");
                user = user.replace(".", "_");

            }
            if (user.isEmpty()) user = ref; // share qwn from menu
            myRequst.userid_share = user;

            myRequst.share_detail =
                    " " + ref + ":" + user + "->" + sessionManager.getcurrentu_nm() +
                            " :.Share " + ref + " by " + user + " for add to " + sessionManager.getcurrentu_nm() + " suppliers at " + DateTimeUtils.formatCurrentDateToStringWithTime() +
                            "";
            myRequst.share_detail = "Click by " + sessionManager.getUserWithAggregator() + ", add at " + DateTimeUtils.formatCurrentDateToStringWithTime();

            share_shop_node = ref;


            if (!isAppHyperlocalMode && sessionManager.getIsConsumer() && !ref.equalsIgnoreCase(getString(R.string.app_name_condition))) {
                ref = "";
            }

            new MyRequestCall().searchSupplier(this, ref, myRequst);
        }
    }

    String ref = "";

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (loadingview.getVisibility() == View.VISIBLE) {

        } else if (llMore.getVisibility() == View.VISIBLE) {
            llMore.setVisibility(View.GONE);
        } else if (rlAddCategory.getVisibility() == View.VISIBLE) {
            hideAddCat();
        } else if (totalcount > 0) {
            try {
                for (int i = 0; i < catererlist.size(); i++) {
                    if (catererlist.get(i).isIslongpress()) {
                        catererlist.get(i).setIslongpress(false);
                    }
                }
                totalcount = 0;
                str_actionid = "";
                showdel(true);
                mAdapter.notifyDataSetChanged();

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (fromAT) {
            super.onBackPressed();
        } else {
            Inad.exitalert(this);
        }
    }

    SwipyRefreshLayout mSwipyRefreshLayout;
    int pagecount = 1;
    boolean isdataavailable = true;
    int pagesize = 25;

    public void usersignin() {

        ApiInterface apiService = ApiClient.getClient(this).create(ApiInterface.class);

        Log.e("sessionM", sessionManager.getsession());
        Call call = apiService.getMySupplier(sessionManager.getsession());

        // Call call = apiService.getCatererlist(usersession,sessionManager.getAggregatorprofile());
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                //  String cookie = response.;
                //  Log.d("test", cookie+ " ");
                loadingview.setVisibility(View.GONE);
                mSwipyRefreshLayout.setRefreshing(false);
                if (response.isSuccessful()) {
                    try {

                        Log.e("caterrelist", response.body().toString());
                        String a = new Gson().toJson(response.body());

                        JSONObject jObjRes = new JSONObject(a);
                        JSONObject jObjdata = jObjRes.getJSONObject("response");
                        JSONObject jObjResponse = jObjdata.getJSONObject("data");
                        JSONArray jObjcontacts = jObjResponse.getJSONArray("content");

                        try {

                            String totalRecord = jObjResponse.getString("totalRecord");
                            String displayEndRecord = jObjResponse.getString("displayEndRecord");

                            if (totalRecord.equals(displayEndRecord)) {
                                isdataavailable = false;
                            } else {
                                isdataavailable = true;
                            }

                        } catch (Exception e) {
                            isdataavailable = false;
                        }

                        if (pagecount == 1) {
                            catererlist.clear();
                        }

                        for (int i = 0; i < jObjcontacts.length(); i++) {
                            if (jObjcontacts.get(i) instanceof JSONObject) {
                                JSONObject jsnObj = (JSONObject) jObjcontacts.get(i);
                                Log.e("aliasjsnObj", jsnObj.toString());

                                SupplierNew obj = new Gson().fromJson(jsnObj.toString(), SupplierNew.class);
                                catererlist.add(obj);
                            }
                        }

                        tv_empty.setVisibility(View.GONE);
                        mAdapter.notifyDataSetChanged();
                        tv_dummy.setVisibility(View.GONE);
                        if (catererlist.size() == 1 && isloaded) {
                            isloaded = false;
                            SupplierNew item = catererlist.get(0);
                            //  checkversioncode(item);
                        } else if (catererlist.size() == 0) {
                            tv_empty.setVisibility(View.VISIBLE);
                            tv_empty.setText("No shop found.");

                           /* if (sessionManager.getcurrentu_nm().matches("[0-9.]*"))  // Only For Consumer
                            {
                                chechdummy();
                            }*/

                            if (ShareModel.getI().isAddShopAuto.equals("ys") && !isAppHyperlocalMode) {
                                SearchSupplier(getString(R.string.consumer).toLowerCase());
                            } else if (ShareModel.getI().isAddShopAuto.equals("ys") && !isAppHyperlocalMode) {
                                SearchSupplier(getString(R.string.consumer).toLowerCase());
                            } else {
                                ShareModel.getI().isAddShopAuto = "";
                            }

                            if (sessionManager.getisBusinesslogic())  // Only For Consumer
                            {
                                chechdummy();
                            }
                        }
                        if (catererlist.size() > 0) {
                            setdummy();
                        }

                        if (catererlist.size() == 1) {
                            SupplierNew item = catererlist.get(0);
                            if (item.getUsername().equalsIgnoreCase("dummys")) {
                                tv_dummy.setVisibility(View.GONE);
                                String notes = getString(R.string.notes);
                                {
                                    notes = notes.replaceAll("Notes:", "<font color='#DA482F'><b>" + "Notes: " + "</b></font>");

                                }
                                tv_dummy.setText(Html.fromHtml(notes));
                            } else {
                                tv_dummy.setVisibility(View.GONE);
                            }
                        }

                    } catch (Exception e) {
                        Toast.makeText(MySupplierActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                    //  Intent i = new Intent(MainActivity.this, MainActivity.class);
                    //  startActivity(i);
                } else {

                    switch (response.code()) {
                        case 401:
                            new RefreshSession(MySupplierActivity.this, sessionManager);  // session expired
                            break;
                        case 404:
                            Toast.makeText(MySupplierActivity.this, "not found", Toast.LENGTH_SHORT).show();
                            break;
                        case 500:
                            Toast.makeText(MySupplierActivity.this, "server broken", Toast.LENGTH_SHORT).show();
                            break;
                        default:
                            Toast.makeText(MySupplierActivity.this, "unknown error", Toast.LENGTH_SHORT).show();
                            break;
                    }
                    try {
                       /* JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Toast.makeText(MainActivity.this, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();*/
                    } catch (Exception e) {
                        Toast.makeText(MySupplierActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
                //  List<Movie> movies = response.body().getResults();
                //  recyclerView.setAdapter(new MoviesAdapter(movies, R.layout.list_item_movie, getApplicationContext()));
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // Log error here since request failed
                loadingview.setVisibility(View.GONE);

                mSwipyRefreshLayout.setRefreshing(false);
                Log.e("error message", t.toString());
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);

        final MenuItem item_del = menu.findItem(R.id.action_add);
        final MenuItem item_more = menu.findItem(R.id.action_more);

        if (sessionManager.getIsConsumer() && !isAppHyperlocalMode) {
            if (isFavourite) {
                item_del.setVisible(false);
                item_more.setVisible(false);
            } else {
                item_del.setVisible(false);
                item_more.setVisible(true);
            }
        } else {
            item_del.setVisible(true);
            item_more.setVisible(false);
        }

        MenuItem action_search = menu.findItem(R.id.action_search);
        if (sessionManager.getIsConsumer() && !isAppHyperlocalMode) {
            if (isFavourite) {
                getSupportActionBar().setTitle("My Favourite");
                action_search.setVisible(true);
            } else {
                action_search.setVisible(false);
            }
        } else {
            getSupportActionBar().setTitle("My Supplier");
            action_search.setVisible(true);
        }
        item_more.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                if (llMore.getVisibility() == View.VISIBLE) {
                    llMore.setVisibility(View.GONE);
                } else {
                    llMore.setVisibility(View.VISIBLE);
                }

                return false;
            }
        });

        MenuItem action_purpose_char = menu.findItem(R.id.action_purpose_char);
        action_purpose_char.setVisible(false);

        setmenuchar();

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        if (id == R.id.action_delete) {


            new CustomDialogClass(MySupplierActivity.this, new CustomDialogClass.OnDialogClickListener() {
                @Override
                public void onDialogImageRunClick(int positon) {
                    if (positon == 0) {

                    } else if (positon == 1) {
                        //str_actionid = str_actionid.replaceAll(",$", "");
                        DelSupplier();
                    }
                }
            }, "DELETE ?").show();
        }


        if (id == R.id.action_add) {
            searchSupplier = new SearchSupplier(this, new SearchSupplier.OnDialogClickListener() {
                @Override
                public void onDialogImageRunClick(int positon, String add) {

                    if (positon == 1) {
                        SearchSupplier(add);
                    }
                }

                @Override
                public void onImgClick(List<ModelFile> data, int pos) {
                }

                @Override
                public void onAddImg() {
                }

            }, et_supplier, false);

            searchSupplier.show();
        }


        if (id == R.id.action_purpose_char) {
            new FilterPurposeDialog(MySupplierActivity.this, new FilterPurposeDialog.OnDialogClickListener() {

                @Override
                public void onDialogImageRunClick(int pos, String add) {

                    if (!purpose.equals(add) && pos == 1) {

                        purpose = add;
                        SharedPrefUserDetail.setString(MySupplierActivity.this, SharedPrefUserDetail.purpose, purpose);
                        SharedPrefUserDetail.setString(MySupplierActivity.this, SharedPrefUserDetail.purpose_mysupplier, purpose);
                        setmenuchar();
                    }

                }
            }, purpose, false).show();
        }
        return super.onOptionsItemSelected(item);
    }

    String et_supplier = "";

    String purpose = "request";
    SearchSupplier searchSupplier;

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return true;
    }

    Menu menu;

    public void showdel(boolean isshow) {

        if (menu != null) {
            MenuItem action_add = menu.findItem(R.id.action_add);
            action_add.setVisible(isshow);

        /*    if (sessionManager.getIsConsumer() && getString(R.string.app_name_condition).equalsIgnoreCase(getString(R.string.c1_omantex))) {
                action_add.setVisible(false);
            } else if (sessionManager.getIsConsumer() && getString(R.string.app_name_condition).equalsIgnoreCase(getString(R.string.c3_texvalley))) {
                action_add.setVisible(false);
            } else {
                action_add.setVisible(true);
            }*/

            if (isAppHyperlocalMode) {
                action_add.setVisible(true);
            } else {
                action_add.setVisible(false);
            }

            MenuItem action_search = menu.findItem(R.id.action_search);
            if (sessionManager.getIsConsumer() && !isAppHyperlocalMode) {
                if (isFavourite) {
                    action_search.setVisible(isshow);
                } else {
                    action_search.setVisible(false);
                }
            } else {
                action_search.setVisible(isshow);
            }

            MenuItem action_purpose_char = menu.findItem(R.id.action_purpose_char);
            //action_purpose_char.setVisible(isshow);
            action_purpose_char.setVisible(false);

            isshow = !isshow;
            MenuItem action_delete = menu.findItem(R.id.action_delete);
            action_delete.setVisible(isshow);
        }
    }

    int totalcount = 0;
    String str_actionid = "";

    public void DelSupplier() {

        ApiInterface apiService =
                ApiClient.getClient(this).create(ApiInterface.class);

        Call call;
        call = apiService.DelSupplier(sessionManager.getsession(), str_actionid);
        loadingview.setVisibility(View.VISIBLE);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                try {
                    if (response.isSuccessful()) {

                        Toast.makeText(MySupplierActivity.this, "DELETED !", Toast.LENGTH_LONG).show();

                        loadfirstpage();

                    } else {
                        try {
                            loadingview.setVisibility(View.GONE);
                            JSONObject jObjError = new JSONObject(response.errorBody().string());
                            JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                            Toast.makeText(MySupplierActivity.this, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();
                        } catch (Exception e) {
                            Toast.makeText(MySupplierActivity.this, "ERROR !", Toast.LENGTH_LONG).show();
                        }
                    }
                } catch (Exception e) {
                    loadingview.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // Log error here since request failed
                try {
                    loadingview.setVisibility(View.GONE);
                } catch (Exception e) {

                }
            }
        });
    }

    public void loadfirstpage() {
        pagecount = 1;
        isdataavailable = true;
        str_actionid = "";
        totalcount = 0;
        showdel(true);
        usersignin();
    }

    public void SearchSupplier(final String searchs) {

        ApiInterface apiService =
                ApiClient.getClient(this).create(ApiInterface.class);

        Call call;
        call = apiService.SearchSupplier(sessionManager.getsession(), searchs);

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {

                try {
                    if (response.isSuccessful()) {

                        if (searchSupplier != null) searchSupplier.dismiss();
                        String a = new Gson().toJson(response.body());

                        if (ShareModel.getI().isAddShopAuto.equals("ys")) {
                            ShareModel.getI().isAddShopAuto = "";
                            JSONObject jObjRes = new JSONObject(a);
                            JSONObject jObjdata = jObjRes.getJSONObject("response");
                            JSONObject jObjResponse = jObjdata.getJSONObject("data");
                            JSONArray ja_con = jObjResponse.getJSONArray("content");
                            SupplierNew supplierNew = new Gson().fromJson(ja_con.get(0).toString(), SupplierNew.class);
                            addAutoSupplierForConsumer(supplierNew, searchs);
                        } else {
                            Intent i = new Intent(MySupplierActivity.this, AddSupplierActivity.class);
                            i.putExtra("bridge", a);
                            i.putExtra("username", searchs);
                            i.putExtra("isadd", true);
                            startActivityForResult(i, 10);
                        }
                        //Toast.makeText(MySupplierActivity.this, "DELETED !", Toast.LENGTH_LONG).show();
                        // loadfirstpage();
                    } else {
                        try {

                            JSONObject jObjError = new JSONObject(response.errorBody().string());
                            JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                            String jo_error = jObjErrorresponse.getString("errormsg");
                            searchSupplier.getsupplier(jo_error);
                            // Toast.makeText(MySupplierActivity.this, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();
                        } catch (Exception e) {
                            searchSupplier.getsupplier("ERROR !");
                            Toast.makeText(MySupplierActivity.this, "ERROR !", Toast.LENGTH_LONG).show();

                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // Log error here since request failed
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (ref.isEmpty()) loadfirstpage();
        counterFab.setCount(dbHelper.getcartcount());
        setmenuchar();

        getUserProfile();
        setShoppingCart();
    }

    public void checkversioncode(final SupplierNew item) {

        loadingview.setVisibility(View.VISIBLE);

        CheckVersion checkVersion = new CheckVersion(this, sessionManager.getAggregator_ID(), new CheckVersion.Apicallback() {
            @Override
            public void onGetResponse(String a, boolean issuccess) {

            }

            @Override
            public void onUpdate(boolean isupdateforce, String ischeck) {

                if (ischeck.equalsIgnoreCase("ys")) {

                    loadingview.setVisibility(View.GONE);

                    new UpdateDialogClass(MySupplierActivity.this, new UpdateDialogClass.OnDialogClickListener() {
                        @Override
                        public void onDialogImageRunClick(int positon) {
                            if (positon == 0) {
                                shopselect(item);
                            } else if (positon == 1) {
                                final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                                try {
                                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                                } catch (android.content.ActivityNotFoundException anfe) {
                                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                                }
                            }
                        }
                    }, "Update is available !", isupdateforce).show();
                } else {
                    shopselect(item);
                }
            }
        });
    }

    SqlLiteDbHelper dbHelper;

    public void shopselect(final SupplierNew item) {

        ShareModel.getI().dbUnderMaster = "";

        if (Apputil.isanothercateror(dbHelper, item)) {

            loadingview.setVisibility(View.GONE);

            new CustomDialogClass(this, new CustomDialogClass.OnDialogClickListener() {
                @Override
                public void onDialogImageRunClick(int positon) {
                    if (positon == 0) {

                    } else if (positon == 1) {

                        sessionManager.setcurrentcaterer("");
                        sessionManager.setcurrentcaterername(item.getUsername());
                        sessionManager.setcurrentcaterer(item.getBridgeId());  // from
                        sessionManager.setcurrentcatererstatus(item.getBusinessStatus());
                        sessionManager.setRemark("");
                        sessionManager.setDate("");
                        sessionManager.setAddress("");
                        //   sessionManager.setCartproduct("");
                        //   sessionManager.setCartcount(0);
                        dbHelper.deleteallfromcart();
                        nextpage(item);


                    }
                }
            }, "You are moving out of " + sessionManager.getcurrentcatereraliasname() + ".").show();
        } else {
            sessionManager.setcurrentcatererstatus(item.getBusinessStatus());
            sessionManager.setcurrentcaterername(item.getUsername());
            sessionManager.setcurrentcaterer(item.getBridgeId());  // from
            nextpage(item);
        }
    }

    boolean isloaded = true;

    public void setmenuchar() {

        if (menu != null) {
            MenuItem action_purpose_char = menu.findItem(R.id.action_purpose_char);

            switch (purpose) {

                case "invoice":
                    action_purpose_char.setTitle("I");
                    break;
                case "request":
                    action_purpose_char.setTitle("R");
                    break;
                case "service":
                    action_purpose_char.setTitle("S");
                    break;
            }
        }
    }

    ArrayList<ModelDevice> al_user = new ArrayList<>();

    public void chechdummy() {

        final Gson gson = new Gson();

        boolean isdummy = true;

        String json = SharedPrefUserDetail.getString(MySupplierActivity.this, SharedPrefUserDetail.loginuser, "");

        Type type = new TypeToken<ArrayList<ModelDevice>>() {
        }.getType();

        al_user = gson.fromJson(json, type);

        if (al_user == null) {
            al_user = new ArrayList<>();
        }

        String user = sessionManager.getcurrentu_nm();

        String agree = "";

        for (int i = 0; i < al_user.size(); i++) {
            String alluser = al_user.get(i).getUsernm();
            if (user.equals(alluser)) {
                isdummy = al_user.get(i).isIsdummy();
                break;
            }
        }

        if (isdummy) {
            et_supplier = "dummys";
            et_supplier = "";
            tv_dummy.setVisibility(View.GONE);
        } else {
        }
    }

    public void setdummy() {

        et_supplier = "";

        final Gson gson = new Gson();

        boolean isdummy = true;

        String json = SharedPrefUserDetail.getString(MySupplierActivity.this, SharedPrefUserDetail.loginuser, "");

        Type type = new TypeToken<ArrayList<ModelDevice>>() {
        }.getType();

        al_user = gson.fromJson(json, type);

        if (al_user == null) {
            al_user = new ArrayList<>();
        }

        String user = sessionManager.getcurrentu_nm();

        for (int i = 0; i < al_user.size(); i++) {
            String alluser = al_user.get(i).getUsernm();
            if (user.equals(alluser)) {
                al_user.get(i).setIsdummy(false);
                break;
            }
        }

        String str_array = new Gson().toJson(al_user);
        SharedPrefUserDetail.setString(MySupplierActivity.this, SharedPrefUserDetail.loginuser, str_array);
        sessionManager.setTLoginuser(str_array);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 10) {
            if (resultCode == RESULT_OK) {

            }
        }
    }

    Userprofile userProfileData;

    public void getUserProfile() {
        String userprofile = sessionManager.getUserprofile();
        try {
            userProfileData = new Gson().fromJson(userprofile, Userprofile.class);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (userProfileData != null) {
            String is_g_plus_sign = JsonObjParse.getValueEmpty(userProfileData.field_json_data, "is_g_plus_sign");
            if (is_g_plus_sign.equalsIgnoreCase("yes")) {
                tvChangePass.setVisibility(View.GONE);
            } else {
                tvChangePass.setVisibility(View.VISIBLE);
            }
        }
    }

    //  Display Long Desc Dialogue
    PlayAnim playAnim = new PlayAnim();

    @BindView(R.id.llAnimAddcat)
    LinearLayout llAnimAddcat;

    @OnClick(R.id.rlAddCategory)
    public void rlAddCategory(View v) {
        onBackPressed();
    }

    @BindView(R.id.rlAddCategory)
    RelativeLayout rlAddCategory;

    @BindView(R.id.tilHint)
    TextInputLayout tilHint;

    @BindView(R.id.tilMobile)
    TextInputLayout tilMobile;


    @BindView(R.id.tvAddEditTitle)
    TextView tvAddEditTitle;

    @BindView(R.id.tvAdd)
    TextView tvAdd;

    @BindView(R.id.tvCancel)
    TextView tvCancel;

    @BindView(R.id.tvMsg)
    TextView tvMsg;


    @OnClick(R.id.tvCancel)
    public void tvCancel(View v) {
        hideAddCat();
    }

    @OnClick(R.id.tvAdd)
    public void tvAdd(View v) {
        hideAddCat();
    }

    public void showInfo(String title, String msg) {
        tilMobile.setVisibility(View.GONE);
        tilHint.setVisibility(View.GONE);
        playAnim.slideDownRl(llAnimAddcat);
        rlAddCategory.setVisibility(View.VISIBLE);

        tvAdd.setText("Yes");
        tvMsg.setVisibility(View.GONE);

        tvAddEditTitle.setSelected(true);
        tvAddEditTitle.setText(title);
        tvAdd.setText("Ok");
        tvCancel.setVisibility(View.GONE);
        tvMsg.setText(msg);
        tvMsg.setVisibility(View.VISIBLE);
    }

    public void hideAddCat() {
        playAnim.slideUpRl(llAnimAddcat);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                rlAddCategory.setVisibility(View.GONE);
            }
        }, 700);
    }

    String field_json_data = "", service_type_of = "";

    public void setShoppingCart() {

        if (sessionManager.getIsConsumer() && !isAppHyperlocalMode) {
            if (isFavourite) {
                getSupportActionBar().setTitle("My Favourite");
                counterFab.show();
            } else {
                getSupportActionBar().setTitle("Shop");
                counterFab.hide();
            }
        } else {
            getSupportActionBar().setTitle("My Supplier");
            counterFab.show();
        }

        field_json_data = SharedPrefUserDetail.getString(this, SharedPrefUserDetail.field_json_data, "");
        service_type_of = JsonObjParse.getValueEmpty(field_json_data, "service_type_of");

        if (fromAT) counterFab.hide();

        if (service_type_of.equalsIgnoreCase(getString(R.string.survey))) {
            counterFab.hide();
        }
    }

    SentSmsConfirmDialogue sentSmsConfirmDialogue;

    public void showSmsCnfm() {

        sentSmsConfirmDialogue = new SentSmsConfirmDialogue(MySupplierActivity.this, new SentSmsConfirmDialogue.OnDialogClickListener() {
            @Override
            public void onDialogImageRunClick(int pos, String add) {

            }
        });
        sentSmsConfirmDialogue.show();
    }

    public void addAutoSupplierForConsumer(SupplierNew supplierNew, String unm) {
        try {

            JSONObject jo = new JSONObject();
            JSONArray ja_new = new JSONArray();

            JSONObject jsonObj;
            jsonObj = new JSONObject();
            jsonObj.put("preference", "Yes");
            jsonObj.put("firstname", supplierNew.getFirstname());
            jsonObj.put("lastname", supplierNew.getLastname());
            jsonObj.put("contact_no", supplierNew.getContactNo());
            jsonObj.put("email_id", supplierNew.getEmailId());
            jsonObj.put("location", supplierNew.getLocation());
            jsonObj.put("role", "");

            jsonObj.put("contact_bridge_id", supplierNew.getBridgeId());
            jsonObj.put("account_type", supplierNew.getAccountType());
            jsonObj.put("username", unm);
            jsonObj.put("flag", "add");

            ja_new.put(jsonObj);
            jo.put("newdata", ja_new);
            JSONArray ja_old = new JSONArray();
            jo.put("olddata", ja_old);

            SupplierRequest catRequest = new SupplierRequest();
            catRequest = new Gson().fromJson(jo.toString(), SupplierRequest.class);
            ApiInterface apiService = ApiClient.getClient(this).create(ApiInterface.class);
            Call call = apiService.PutSupplier(sessionManager.getsession(), catRequest);
            call.enqueue(new Callback() {
                @Override
                public void onResponse(Call call, Response response) {

                    if (response.isSuccessful()) {
                        loadfirstpage();
                    } else {
                        loadingview.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onFailure(Call call, Throwable t) {
                    // Log error here since request failed
                    loadingview.setVisibility(View.GONE);
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
            findViewById(R.id.rl_pb_cat).setVisibility(View.GONE);
        }
    }

    private void nextpage(final SupplierNew item) {

        SharedPrefUserDetail.setString(MySupplierActivity.this, SharedPrefUserDetail.field_json_data, item.getField_json_data());
        SharedPrefUserDetail.setString(MySupplierActivity.this, SharedPrefUserDetail.field_json_data_info, item.getField_json_data());
        SharedPrefUserDetail.setString(this, SharedPrefUserDetail.business_type, item.getBusiness_type());

        new GetServiceTypeFromBusiUserProfile(MySupplierActivity.this, item.getUsername(), new GetServiceTypeFromBusiUserProfile.Apicallback() {
            @Override
            public void onGetResponse(String a, String response, String sms_emp) {
                redirectToUnderShopOrCatalogue(a, response, item);
            }
        });
    }

    public void redirectToUnderShopOrCatalogue(String a, String res, SupplierNew item) {

        loadingview.setVisibility(View.GONE);

        String service_type_of = "";
        String field_json_data = "";

        try {
            JSONObject Jsonresponse = new JSONObject(res);
            JSONObject Jsonresponseinfo = Jsonresponse.getJSONObject("response");
            JSONArray jaData = Jsonresponseinfo.getJSONArray("data");
            if (jaData.length() > 0) {
                JSONObject data = jaData.getJSONObject(0);
                field_json_data = JsonObjParse.getValueEmpty(data.toString(), "field_json_data");
                service_type_of = JsonObjParse.getValueEmpty(field_json_data, "service_type_of").toLowerCase();

                String is_user_traction = JsonObjParse.getValueEmpty(field_json_data, "is_user_traction");
                if (is_user_traction.equalsIgnoreCase("yes")) {
                    MyRequestCall myRequestCall = new MyRequestCall();
                    myRequestCall.setTraction(MySupplierActivity.this, item, field_json_data);
                }
            } else {
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (item.getAccountType() != null && item.getBusiness_type() != null &&
                item.getAccountType().equalsIgnoreCase("Business") &&
                item.getBusiness_type().equalsIgnoreCase("Aggregator")) {

            sessionManager.setAggregator_ID(getString(R.string.Aggregator)); // From Base Click
            SharedPrefUserDetail.setBoolean(MySupplierActivity.this, SharedPrefUserDetail.isfromsupplier, false);

            String agree = item.getUsername();
            sessionManager.setAggregator_ID(agree);

            SharedPrefUserDetail.setString(MySupplierActivity.this, SharedPrefUserDetail.isfrommyaggregator, item.getBridgeId());

            SharedPrefUserDetail.setString(MySupplierActivity.this, SharedPrefUserDetail.infoMainShop, item.getUsername());

            Intent mainIntent = new Intent(MySupplierActivity.this, MySupplierUnderSubSHopActivity.class);
            mainIntent.putExtra("session", sessionManager.getsession());
            mainIntent.putExtra("SupplierTitle", item.getFirstname());
            mainIntent.putExtra("purpose", purpose);

            startActivity(mainIntent);


        } else if (item.getAccountType() != null && item.getBusiness_type() != null &&
                item.getAccountType().equalsIgnoreCase("Business") &&
                item.getBusiness_type().equalsIgnoreCase("Circle")) {

            SharedPrefUserDetail.setString(MySupplierActivity.this, SharedPrefUserDetail.infoMainShop, item.getUsername());

            sessionManager.setAggregator_ID(getString(R.string.Aggregator)); // From Base Click
            SharedPrefUserDetail.setBoolean(MySupplierActivity.this, SharedPrefUserDetail.isfromsupplier, false);
            SharedPrefUserDetail.setString(MySupplierActivity.this, SharedPrefUserDetail.isfrommyaggregator, "");
            String companyDetailShort = item.getCompany_detail_short().trim();

            try {
                String data = sessionManager.getIndustrySupplier();
                JSONObject jo_data = new JSONObject(data);
                JSONArray ja_buildingArr = jo_data.getJSONArray("buildingArr");

                sessionManager.setBuilding_id("");
                sessionManager.setIndustry_id("");

                for (int i = 0; i < ja_buildingArr.length(); i++) {
                    JSONObject jo_res = ja_buildingArr.getJSONObject(i);
                    String building_name = jo_res.getString("building_name").trim();
                    String building_id = jo_res.getString("building_id");

                    if (companyDetailShort.equalsIgnoreCase(building_name)) {
                        sessionManager.setBuilding_id(building_id);
                        break;
                    }
                }

                JSONArray industryArr = jo_data.getJSONArray("industryArr");
                for (int i = 0; i < industryArr.length(); i++) {
                    JSONObject jo_res = industryArr.getJSONObject(i);
                    String industry_name = jo_res.getString("industry_name");
                    String industry_id = jo_res.getString("industry_id");
                    sessionManager.setIndustry_id(industry_id);
                    break;
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            Intent mainIntent = new Intent(this, MySupplierUnderSubSHopActivity.class);
            mainIntent.putExtra("session", sessionManager.getsession());
            mainIntent.putExtra("isBk", true);
            mainIntent.putExtra("SupplierTitle", item.getFirstname());
            mainIntent.putExtra("purpose", purpose);

            startActivity(mainIntent);

        } else {
            String metal_list = JsonObjParse.getValueEmpty(field_json_data, "metal_list");
            Metal metal = new Gson().fromJson(metal_list, Metal.class);
            if (metal == null) metal = new Metal();
            ShareModel.getI().metal = metal;

            if (service_type_of.equalsIgnoreCase(getString(R.string.none))) {
                showInfo("Message", "Service is not available.");
            } else if (service_type_of.equalsIgnoreCase(getString(R.string.upload_pdf))) {

                String picturePath = JsonObjParse.getValueEmpty(field_json_data, "picturePath");

                if (!picturePath.isEmpty()) {
                    Intent i = new Intent(MySupplierActivity.this, WebViewActivity.class);
                    i.putExtra("url", picturePath);

                    int pos = picturePath.lastIndexOf("/");
                    if (pos > 0) {
                        picturePath = picturePath.substring(pos + 1);
                    }

                    i.putExtra("title", picturePath);
                    startActivity(i);
                }

            } else if (service_type_of.equalsIgnoreCase(getString(R.string.link_to_web))) {

                String webLink = JsonObjParse.getValueEmpty(field_json_data, "webLink").toLowerCase();
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(webLink));
                startActivity(i);

            } else {

                purpose = a;
                if (a.isEmpty()) purpose = "request";

                purpose = purpose.toLowerCase();

                SharedPrefUserDetail.setString(MySupplierActivity.this, SharedPrefUserDetail.purpose, purpose);
                SharedPrefUserDetail.setString(MySupplierActivity.this, SharedPrefUserDetail.purpose_mysupplier, purpose);
                setmenuchar();

                /* set purpose  */
                sessionManager.setAggregator_ID(getString(R.string.Aggregator)); // From Base Click
                SharedPrefUserDetail.setString(MySupplierActivity.this, SharedPrefUserDetail.isfrommyaggregator, "");

                SharedPrefUserDetail.setBoolean(MySupplierActivity.this, SharedPrefUserDetail.isfromsupplier, true);

                SharedPrefUserDetail.setString(MySupplierActivity.this, SharedPrefUserDetail.chit_currency_code, item.getCurrencyCode());
                SharedPrefUserDetail.setString(MySupplierActivity.this, SharedPrefUserDetail.chit_symbol_native, item.getSymbol_native());
                SharedPrefUserDetail.setString(MySupplierActivity.this, SharedPrefUserDetail.chit_currency_code_id, item.getCurrencyCodeId());
                SharedPrefUserDetail.setString(MySupplierActivity.this, SharedPrefUserDetail.chit_symbol, item.getSymbol());

                if (purpose.equalsIgnoreCase("service") || purpose.equalsIgnoreCase(getString(R.string.sms_only))) {
                    sessionManager.setcurrentcatereraliasname(item.getFirstname());

                    String supl = new Gson().toJson(item);
                    Intent i = new Intent(MySupplierActivity.this, IssueMgmtActivity.class);
                    i.putExtra("purpose", purpose);
                    i.putExtra("supplier", supl);

                    startActivity(i);
                }/* else if (purpose.equalsIgnoreCase(getString(R.string.delivery))) {
                    if (fromMrp) {
                        Intent i = new Intent(MySupplierActivity.this, DeliveryChargesActivity.class);
                        String supl = new Gson().toJson(item);
                        String delivery_charge = JsonObjParse.getValueEmpty(field_json_data, "delivery_charge").toLowerCase();
                        i.putExtra("session", sessionManager.getsession());
                        i.putExtra("purpose", purpose);
                        i.putExtra("frombridgeidval", sessionManager.getcurrentcaterer());
                        i.putExtra("delivery_charge", delivery_charge);
                        i.putExtra("supplier", supl);
                        i.putExtra("isFromMrp", true);
                        startActivity(i);
                    } else {
                        Intent i = new Intent(MySupplierActivity.this, DeliveryChargesActivity.class);
                        String delivery_charge = JsonObjParse.getValueEmpty(field_json_data, "delivery_charge").toLowerCase();
                        i.putExtra("session", sessionManager.getsession());
                        i.putExtra("purpose", purpose);
                        i.putExtra("frombridgeidval", sessionManager.getcurrentcaterer());
                        i.putExtra("delivery_charge", delivery_charge);
                        i.putExtra("isFromMrp", false);

                        String supl = new Gson().toJson(item);
                        i.putExtra("supplier", supl);
                        startActivity(i);
                    }
                }*/
                    /*else if (purpose.equalsIgnoreCase(getString(R.string.dental))) {
                    sessionManager.setcurrentcatereraliasname(item.getFirstname());

                    String supl = new Gson().toJson(item);
                    Intent i = new Intent(MySupplierActivity.this, Template_DentalActivity.class);
                    i.putExtra("frombridgeidval", item.getBridgeId());
                    i.putExtra("purpose", purpose);
                    i.putExtra("supplier", supl);

                    startActivity(i);
                } */
                else {
                    boolean isRet = false;
                    /*if (service_type_of.equalsIgnoreCase(getString(R.string.jewel))) {
                        String silver_today_price = JsonObjParse.getValueEmpty(field_json_data, "silver_today_price");
                        String gold_today_price = JsonObjParse.getValueEmpty(field_json_data, "gold_today_price");
                        if (silver_today_price.isEmpty() || gold_today_price.isEmpty()) {
                            isRet = true;

                        } else {
                            ShareModel.getI().silver_today_price = silver_today_price;
                            ShareModel.getI().gold_today_price = gold_today_price;
                        }
                    }

                    if (isRet) {
                        Inad.alerterInfo("Message", "Please try after some time...", MySupplierActivity.this);
                        return;
                    }*/

                    sessionManager.setcurrentcatereraliasname(item.getFirstname());
                    String delivery_charge = JsonObjParse.getValueEmpty(field_json_data, "delivery_charge").toLowerCase();
                    SharedPrefUserDetail.setString(MySupplierActivity.this, SharedPrefUserDetail.delivery_charge, delivery_charge);
                    Intent i = new Intent(MySupplierActivity.this, CatelogActivity.class);
                    i.putExtra("session", sessionManager.getsession());
                    i.putExtra("isFromSupplier", true);
                    i.putExtra("bridgeidval", item.getContact_bridge_id());
                    i.putExtra("frombridgeidval", item.getBridgeId());
                    i.putExtra("aliasname", item.getUsername());
                    i.putExtra("username", item.getUsername());
                    i.putExtra("currency_code", item.getCurrencyCode());
                    i.putExtra("company_name", item.getFirstname());
                    i.putExtra("delivery_charge", delivery_charge);
                    i.putExtra("isBk", true);
                    if (fromMrp) {
                        i.putExtra("isFromMrp", true);
                    } else {
                        i.putExtra("isFromMrp", false);
                    }
                    startActivity(i);
                }
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe()
    public void onMessageEvent(String st) {
        ref = "";
        isdataavailable = true;
        pagecount = 1;
        loadfirstpage();
    }
}