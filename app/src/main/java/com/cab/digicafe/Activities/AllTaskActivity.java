package com.cab.digicafe.Activities;


import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.andremion.counterfab.CounterFab;
import com.cab.digicafe.Adapter.MyTaskAdapter;
import com.cab.digicafe.BaseActivity;
import com.cab.digicafe.ChitlogActivity;
import com.cab.digicafe.DetailsFragment;
import com.cab.digicafe.Dialogbox.CustomDialogClass;
import com.cab.digicafe.Dialogbox.EmployeeListDialog;
import com.cab.digicafe.Dialogbox.FilterPurposeDialog;
import com.cab.digicafe.Fragments.SummaryDetailFragmnet;
import com.cab.digicafe.Helper.Constants;
import com.cab.digicafe.Helper.Inad;
import com.cab.digicafe.Helper.RecyclerItemTouchHelper3;
import com.cab.digicafe.Helper.SessionManager;
import com.cab.digicafe.Model.Chit;
import com.cab.digicafe.Model.Employee;
import com.cab.digicafe.Model.ShareModel;
import com.cab.digicafe.Model.Userprofile;
import com.cab.digicafe.MyCustomClass.CanelOrder_Refund;
import com.cab.digicafe.MyCustomClass.JsonObjParse;
import com.cab.digicafe.MyCustomClass.RefreshSession;
import com.cab.digicafe.R;
import com.cab.digicafe.Rest.ApiClient;
import com.cab.digicafe.Rest.ApiInterface;
import com.cab.digicafe.serviceTypeClass.GetServiceTypeFromBusiUserProfile;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import info.hoang8f.android.segmented.SegmentedGroup;
import okhttp3.Headers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AllTaskActivity extends BaseActivity implements SearchView.OnQueryTextListener, MyTaskAdapter.OnItemClickListener, SwipyRefreshLayout.OnRefreshListener, RecyclerItemTouchHelper3.RecyclerItemTouchHelperListener {

    private RecyclerView recyclerView;
    private MyTaskAdapter mAdapter;
    CounterFab counterFab;
    private List<Chit> orderlist = new ArrayList<>();
    SessionManager sessionManager;
    String sessionString;
    MenuItem item;
    Menu menu;
    int totalcount = 0;
    String str_actionid = ""; // to  is business for empl login
    RadioButton openstatus, Close_, Archive_;
    SegmentedGroup s_group;
    String tab = "new_all";

    List<Chit> temp_opentask = new ArrayList<>();
    List<Chit> temp_closetask = new ArrayList<>();

    boolean isbusiness = false;

    ArrayList<String> al_refund = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.content_mytask, frameLayout);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        refreshlist();

        Constants.chit_id_to = 1;


        sessionManager = new SessionManager(this);
        if (sessionManager.getcurrentu_nm().contains("@")) // Employee
        {
            getSupportActionBar().setTitle("All Task");
        } else {
            // getSupportActionBar().setTitle("Receipts");
        }
        getSupportActionBar().setTitle("All Task");

        isbusiness = sessionManager.getisBusinesslogic();

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        s_group = (SegmentedGroup) findViewById(R.id.segmented2);
        sessionManager = new SessionManager(this);
        // listen refresh event
        counterFab = (CounterFab) findViewById(R.id.counter_fab);
        counterFab.hide();

        openstatus = (RadioButton) findViewById(R.id.openstatus);
        Close_ = (RadioButton) findViewById(R.id.closestatus);

        Archive_ = (RadioButton) findViewById(R.id.inprogressstatus);

        Archive_.setVisibility(View.GONE);


        openstatus.setText("Open");
        Close_.setText("Close");


        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                // bridgeidval = null;
            } else {
                sessionString = extras.getString("session");
                //  bridgeidval= extras.getString("bridgeidval");

            }
        } else {
            sessionString = (String) savedInstanceState.getSerializable("session");
            // bridgeidval= (String) savedInstanceState.getSerializable("bridgeidval");
        }


        s_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {


                findViewById(R.id.rl_pb_ctask).setVisibility(View.VISIBLE);

                switch (checkedId) {
                    case R.id.openstatus:
                        tab = "new_all";
                        break;
                    case R.id.closestatus:
                        tab = "close_all";
                        break;
                    default:
                        break;
                }


                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(AllTaskActivity.this);
                recyclerView.setLayoutManager(mLayoutManager);
                //recyclerView.setItemAnimator(new DefaultItemAnimator());

                mAdapter = new MyTaskAdapter(orderlist, AllTaskActivity.this, AllTaskActivity.this, tab, isbusiness);
                recyclerView.setAdapter(mAdapter);
                new ItemTouchHelper(new RecyclerItemTouchHelper3(0, 0, AllTaskActivity.this)).attachToRecyclerView(recyclerView);

                recyclerView.setNestedScrollingEnabled(false);
                isdataavailable = true;
                pagecount = 1;

                usersignin(sessionString, 1, false);

               /* if (totalcount > 0) {
                    try {
                        for (int i = 0; i < orderlist.size(); i++) {
                            if (orderlist.get(i).isIslongpress()) {
                                orderlist.get(i).setIslongpress(false);
                            }
                        }

                        totalcount = 0;
                        str_actionid = "";

                        MenuItem item1 = menu.findItem(R.id.action_delete);
                        item1.setVisible(false);
                        mAdapter.notifyDataSetChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }


                setcte();*/


            }
        });

        openstatus.setChecked(true);


    }

    @Override
    public void onItemClick(Chit item) {
        Intent i = new Intent(AllTaskActivity.this, ChitlogActivity.class);
        i.putExtra("chit_hash_id", item.getChit_hash_id());
        i.putExtra("chit_id", item.getChit_id() + "");
        i.putExtra("transtactionid", item.getChit_name() + "");
        i.putExtra("productqty", item.getTotal_chit_item_value() + "");
        i.putExtra("productprice", item.getChit_item_count() + "");
        i.putExtra("purpose", item.getPurpose());
        i.putExtra("Refid", item.getRef_id());
        i.putExtra("Caseid", item.getCase_id());
        i.putExtra("isprint", true);
        //if (tab.equalsIgnoreCase("new_all"))
        i.putExtra("isMrp", true);

        if (tab.equalsIgnoreCase("new_all")) {  // open task
            if (sessionManager.getisBusinesslogic()) {
                i.putExtra("iscomment", true);  // business
                i.putExtra("isdiaries", true);  // if open & business then diary for comment
            } else {
                i.putExtra("iscomment", false);  // dont show for employee
            }

        } else {
            i.putExtra("iscomment", true);
        }

        ShareModel.getI().isFromAllTask = true;


        DetailsFragment.chititemcount = "" + item.getChit_item_count();
        DetailsFragment.chititemprice = "" + item.getTotal_chit_item_value();
        SummaryDetailFragmnet.item = item;
        DetailsFragment.item_detail = item;


        startActivity(i);
    }

    @Override
    public void onItemLongClick(Chit item, int pos) {

        if (orderlist.get(pos).isIslongpress()) {
            orderlist.get(pos).setIslongpress(false);
        } else {
            orderlist.get(pos).setIslongpress(true);
        }

        mAdapter.notifyDataSetChanged();


        boolean isanyitem = false;
        str_actionid = "";
        totalcount = 0;
        for (int i = 0; i < orderlist.size(); i++) {
            if (orderlist.get(i).isIslongpress()) {
                totalcount++;
                isanyitem = true;
                str_actionid = str_actionid + String.valueOf(orderlist.get(i).getChit_id()) + ",";

            }
        }


        Log.e("totalcount", String.valueOf(totalcount));
        MenuItem item1 = menu.findItem(R.id.action_delete);
        MenuItem action_employee = menu.findItem(R.id.action_employee);
        if (totalcount > 0) {
            item1.setVisible(true);
            action_employee.setVisible(true);
            showsearch(false);

        } else {
            item1.setVisible(false);
            action_employee.setVisible(false);
            showsearch(true);
        }


        /****************  Multiple Refund  *********************/

        try {
            JSONObject jo_summary = new JSONObject(item.getFooter_note());

            if (jo_summary.getString("PAYMENT_MODE").equals("RAZORPAY")) {
                boolean isadded = false;
                for (int i = 0; i < al_refund.size(); i++) {
                    String payuResponse = al_refund.get(i);
                    if (jo_summary.getString("payuResponse").equals(payuResponse)) {
                        isadded = true;
                        if (isadded) {
                            al_refund.remove(i);
                            break;
                        }
                    }
                }
                if (!isadded) {
                    al_refund.add(jo_summary.getString("payuResponse"));
                }


            } else {

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onRefresh(SwipyRefreshLayoutDirection direction) {
        //mSwipyRefreshLayout.setRefreshing(false);

        Log.e("scroll direction", "Refresh triggered at "
                + (direction == SwipyRefreshLayoutDirection.TOP ? "top" : "bottom"));
        if (direction == SwipyRefreshLayoutDirection.TOP) {
            isdataavailable = true;
            pagecount = 1;
            usersignin(sessionString, pagecount, false);
        } else if (direction == SwipyRefreshLayoutDirection.BOTTOM) {
            if (isdataavailable) {
                pagecount++;
                usersignin(sessionString, pagecount, false);
            } else {
                mSwipyRefreshLayout.setRefreshing(false);
                Toast.makeText(this, "No more data available", Toast.LENGTH_LONG).show();
            }

        }
    }

    SwipyRefreshLayout mSwipyRefreshLayout;
    int pagecount = 1;
    boolean isdataavailable = true;
    int pagesize = 50;

    public void refreshlist() {
        mSwipyRefreshLayout = (SwipyRefreshLayout) findViewById(R.id.swipyrefreshlayout);
        mSwipyRefreshLayout.setDirection(SwipyRefreshLayoutDirection.BOTH);
        mSwipyRefreshLayout.setOnRefreshListener(this);

    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {


        if (viewHolder instanceof MyTaskAdapter.MyViewHolder) {


            final Chit deletedItem = orderlist.get(viewHolder.getAdapterPosition());
            final int deletedIndex = viewHolder.getAdapterPosition();
            mAdapter.removeItem(viewHolder.getAdapterPosition());

            String actionid = String.valueOf(deletedItem.getChit_id());
            actionid = actionid + ",";

            String actionCode = "";

            String msg = "";

            isSmsForWhat = "";

            if (tab.equalsIgnoreCase("new_all")) {

                if (direction == ItemTouchHelper.LEFT) {

                    findViewById(R.id.rl_pb_ctask).setVisibility(View.VISIBLE);
                    // new ass
                    actionCode = "";
                    msg = "MY TASK";
                } else if (direction == ItemTouchHelper.RIGHT) {

                    actionCode = "5";
                    isSmsForWhat = "sms_customer"; // for cancel task

                }

            } else if (tab.equalsIgnoreCase("close_all")) {

                findViewById(R.id.rl_pb_ctask).setVisibility(View.VISIBLE);


                if (direction == ItemTouchHelper.LEFT) {

                    // new ass
                    // actionCode = "4";
                    actionCode = "2";
                } else if (direction == ItemTouchHelper.RIGHT) {

                }

            }

            if (actionCode.equalsIgnoreCase("")) {

                assignarchived(sessionString, actionid, sessionManager.getE_bridgeid(), msg, new MyCallback() {
                    @Override
                    public void callbackCall(List<Chit> filteredModelList) {

                    }

                    @Override
                    public void callback_remove() {

                        totalcount = 0;
                        str_actionid = "";
                        MenuItem item1 = menu.findItem(R.id.action_delete);
                        MenuItem action_employee = menu.findItem(R.id.action_employee);
                        item1.setVisible(false);
                        action_employee.setVisible(false);
                        showsearch(true);
                        pagecount = 1;
                        isdataavailable = true;
                        usersignin(sessionString, pagecount, false);

                    }
                });
            } else {

                String employee_bridge_id = "";
                final String f_actionid = actionid;
                final String f_actionCode = actionCode;

                if (actionCode.equalsIgnoreCase("5")) { // for send sms to customer for act close with flag

                    employee_bridge_id = sessionManager.getE_bridgeid();

                    if (!isSmsForWhat.isEmpty()) {

                        if (sessionManager.getisBusinesslogic()) { // only right swipe no need left

                            String userprofile = sessionManager.getUserprofile();
                            try {
                                Userprofile userProfileData = new Gson().fromJson(userprofile, Userprofile.class);
                                smsFlagForOpen = JsonObjParse.getValueEmpty(userProfileData.field_json_data, isSmsForWhat);
                                // left - employee, right - customer
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            loadData(sessionString, actionid, employee_bridge_id, msg, f_actionCode);

                        } else {

                            String empUseName = sessionManager.getcurrentu_nm();
                            int indexOfannot = empUseName.indexOf("@") + 1;
                            empUseName = empUseName.substring(indexOfannot, empUseName.indexOf("."));

                            final String finalActionid1 = actionid;
                            final String finalEmployee_bridge_id1 = employee_bridge_id;
                            final String finalMsg1 = msg;

                            new GetServiceTypeFromBusiUserProfile(AllTaskActivity.this, empUseName, new GetServiceTypeFromBusiUserProfile.Apicallback() {
                                @Override
                                public void onGetResponse(String a, String response, String sms_emp) {

                                    try {
                                        JSONObject Jsonresponse = new JSONObject(response);
                                        JSONObject Jsonresponseinfo = Jsonresponse.getJSONObject("response");
                                        JSONArray jaData = Jsonresponseinfo.getJSONArray("data");
                                        if (jaData.length() > 0) {
                                            JSONObject data = jaData.getJSONObject(0);
                                            String field_json_data = JsonObjParse.getValueNull(data.toString(), "field_json_data");
                                            smsFlagForOpen = JsonObjParse.getValueNull(field_json_data, isSmsForWhat);

                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    loadData(sessionString, finalActionid1, finalEmployee_bridge_id1, finalMsg1, f_actionCode);


                                }
                            });


                        }


                    } else {

                        loadData(sessionString, actionid, employee_bridge_id, msg, f_actionCode);

                    }


                } else if (actionCode.equalsIgnoreCase("2")) {
                    employee_bridge_id = "";


                    final String finalActionid = actionid;
                    final String finalEmployee_bridge_id = employee_bridge_id;
                    final String finalMsg = msg;

                    removearchived(sessionString, f_actionid, true, f_actionCode, new ChitActionCallback() {
                        @Override
                        public void callback_chit() {


                            assignarchived(sessionString, finalActionid, finalEmployee_bridge_id, finalMsg, new MyCallback() {
                                @Override
                                public void callbackCall(List<Chit> filteredModelList) {

                                }

                                @Override
                                public void callback_remove() {
                                    findViewById(R.id.rl_pb_ctask).setVisibility(View.GONE);
                                    totalcount = 0;
                                    str_actionid = "";
                                    MenuItem item1 = menu.findItem(R.id.action_delete);
                                    MenuItem action_employee = menu.findItem(R.id.action_employee);
                                    item1.setVisible(false);
                                    action_employee.setVisible(false);
                                    showsearch(true);
                                    pagecount = 1;
                                    isdataavailable = true;
                                    usersignin(sessionString, pagecount, false);

                                }
                            });


                        }
                    });


                }


            }


        }
    }

    public void loadData(final String sessionString, final String actionid, String employee_bridge_id, String msg, final String f_actionCode) {


        findViewById(R.id.rl_pb_ctask).setVisibility(View.VISIBLE);


        assignarchived(sessionString, actionid, employee_bridge_id, msg, new MyCallback() {
            @Override
            public void callbackCall(List<Chit> filteredModelList) {

            }

            @Override
            public void callback_remove() {

                removearchived(sessionString, actionid, true, f_actionCode, new ChitActionCallback() {
                    @Override
                    public void callback_chit() {

                        totalcount = 0;
                        str_actionid = "";
                        MenuItem item1 = menu.findItem(R.id.action_delete);
                        MenuItem action_employee = menu.findItem(R.id.action_employee);
                        item1.setVisible(false);
                        action_employee.setVisible(false);
                        showsearch(true);
                        pagecount = 1;
                        isdataavailable = true;
                        usersignin(sessionString, pagecount, false);

                    }
                });
            }
        });


    }


    @Override
    public void onTouchSwipe(boolean isSwipeEnable) {
        mSwipyRefreshLayout.setEnabled(isSwipeEnable);
    }


    String isSmsForWhat = "";
    String smsFlagForOpen = "";


    public void removearchived(final String usersession, String chitIds, final boolean isswipe, String actionCode, final ChitActionCallback callback) {

        ApiInterface apiService =
                ApiClient.getClient(AllTaskActivity.this).create(ApiInterface.class);
        JsonObject a1 = new JsonObject();
        try {

            a1.addProperty("chitIds", chitIds);
            a1.addProperty("actionCode", actionCode);

            if (!isSmsForWhat.isEmpty() && smsFlagForOpen.equalsIgnoreCase("yes")) {
                isSmsForWhat = "";
                a1.addProperty("sms", "true");
            }


        } catch (JsonParseException e) {
            e.printStackTrace();
        }
        // Toast.makeText(AllTaskActivity.this, actionCode, Toast.LENGTH_LONG).show();

        //Call call = apiService.removechit(usersession,chitIds+",","10");
        Call call = apiService.removechit(usersession, a1);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                int statusCode = response.code();
                Log.e("test", sessionString + "sessionstring");

                Headers headerList = response.headers();
                Log.d("testhistory", statusCode + " ");

                if (response.isSuccessful()) {
                    try {

                        callback.callback_chit();

                        Log.e("response.body()", response.body().toString());
                        String a = new Gson().toJson(response.body());

                        JSONObject jo_res = new JSONObject(a);
                        String response_ = jo_res.getString("response");
                        JSONObject jo_txt = new JSONObject(response_);
                        String actionTxt = jo_txt.getString("actionTxt");


                        Toast.makeText(AllTaskActivity.this, actionTxt, Toast.LENGTH_LONG).show();


                    } catch (Exception e) {
                        Log.e("errortest", e.getMessage());

                    }

                } else {
                    Log.e("test", " trdds1");

                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Toast.makeText(AllTaskActivity.this, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(AllTaskActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }


            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // Log error here since request failed
                findViewById(R.id.rl_pb_ctask).setVisibility(View.GONE);
                Log.e("error message", t.toString());
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (totalcount > 0) {
            try {
                for (int i = 0; i < orderlist.size(); i++) {
                    if (orderlist.get(i).isIslongpress()) {
                        orderlist.get(i).setIslongpress(false);
                    }
                }

                totalcount = 0;
                str_actionid = "";

                MenuItem item1 = menu.findItem(R.id.action_delete);
                MenuItem action_employee = menu.findItem(R.id.action_employee);
                item1.setVisible(false);
                action_employee.setVisible(false);
                showsearch(true);
                mAdapter.notifyDataSetChanged();


                if (al_refund != null && al_refund.size() > 0) {
                    al_refund.clear();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
           /* AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.AlertDialogCustom));
            builder
                    .setMessage(getString(R.string.quit))
                    .setTitle("")
                    .setCancelable(false)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            finish();
                            Intent intent = new Intent(Intent.ACTION_MAIN);
                            intent.addCategory(Intent.CATEGORY_HOME);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        }
                    })
                    .setNegativeButton("No", null)

                    .show();*/

            Inad.exitalert(this);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);


        final MenuItem item = menu.findItem(R.id.action_search);

        MenuItem item1 = menu.findItem(R.id.action_filter);
        item1.setVisible(true);

        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setInputType(InputType.TYPE_CLASS_NUMBER);
        searchView.setOnQueryTextListener(this);

        MenuItemCompat.setOnActionExpandListener(item,
                new MenuItemCompat.OnActionExpandListener() {
                    @Override
                    public boolean onMenuItemActionCollapse(MenuItem item) {
// Do something when collapsed
                        mAdapter.setFilter(orderlist);
                        return true; // Return true to collapse action view
                    }

                    @Override
                    public boolean onMenuItemActionExpand(MenuItem item) {
// Do something when expanded
                        return true; // Return true to expand action view
                    }
                });


        return true;
    }

    String msg_alert = "";
    String actionCode_multi = "";

    GetServiceTypeFromBusiUserProfile getBusinessProfile = new GetServiceTypeFromBusiUserProfile();

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.


        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        if (id == R.id.action_employee) {
            new EmployeeListDialog(AllTaskActivity.this, new EmployeeListDialog.OnDialogClickListener() {
                @Override
                public void onDialogImageRunClick(int pos, final Employee add) {

                    sms_toemployee = "";
                    if (pos == 1 && add != null && add.getUsername().equalsIgnoreCase(sessionManager.getcurrentu_nm())) // its for employee login only
                    {
                        Inad.alerter("Alert !", "Can't assign yourself.", AllTaskActivity.this);

                    } else if (pos == 1 && add != null) {

                        isSmsEmpWhileAssign = true;

                        // for business - sms from getUserprofile
                        // for employee - sms frrom businessprofile - - from username substring

                        String empUseName = add.getUsername();
                        int indexOfannot = empUseName.indexOf("@") + 1;
                        empUseName = empUseName.substring(indexOfannot, empUseName.indexOf("."));


                        new GetServiceTypeFromBusiUserProfile(AllTaskActivity.this, empUseName, new GetServiceTypeFromBusiUserProfile.Apicallback() {
                            @Override
                            public void onGetResponse(String serviecType, String response, String sms_emp) {

                                findViewById(R.id.rl_pb_ctask).setVisibility(View.VISIBLE);


                                if (sessionManager.getisBusinesslogic()) { // for business user(employer)

                                    String userprofile = sessionManager.getUserprofile();
                                    try {
                                        userProfileData = new Gson().fromJson(userprofile, Userprofile.class);
                                        sms_toemployee = JsonObjParse.getValueEmpty(userProfileData.field_json_data, "sms_employee");

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                } else { // for employee

                                    sms_toemployee = sms_emp;


                                }


                                assignarchived(sessionString, str_actionid, add.getEmployee_bridge_id(), getString(R.string.str_assign), new MyCallback() {
                                    @Override
                                    public void callbackCall(List<Chit> filteredModelList) {

                                    }

                                    @Override
                                    public void callback_remove() {

                                        totalcount = 0;
                                        str_actionid = "";


                                        MenuItem item1 = menu.findItem(R.id.action_delete);
                                        MenuItem action_employee = menu.findItem(R.id.action_employee);
                                        item1.setVisible(false);
                                        action_employee.setVisible(false);
                                        showsearch(true);


                                        pagecount = 1;
                                        isdataavailable = true;
                                        usersignin(sessionString, pagecount, false);

                                    }
                                });
                            }
                        });


                    }
                }
            }).show();

        }

        if (id == R.id.action_filter) {
            new FilterPurposeDialog(AllTaskActivity.this, new FilterPurposeDialog.OnDialogClickListener() {

                @Override
                public void onDialogImageRunClick(int pos, String add) {


                    if (!purpose.equals(add) && pos == 1) {
                        findViewById(R.id.rl_pb_ctask).setVisibility(View.VISIBLE);
                        isdataavailable = true;
                        purpose = add;

                        pagecount = 1;

                       /* switch (add) {
                            case "all":
                                out_of_stock = "";
                                break;
                            case "avail":
                                out_of_stock = "No";
                                break;
                            case "notavail":
                                out_of_stock = "Yes";
                                break;
                            default:
                                out_of_stock = "";
                                break;
                        }*/

                        //usersignin(pagecount);
                        usersignin(sessionString, 1, false);
                        //mAdapter.setitem(orderlist,filter);
                    }


                }
            }, purpose, true).show();
        }


        if (id == R.id.action_delete) {

            findViewById(R.id.rl_pb_ctask).setVisibility(View.VISIBLE);
            if (tab.equalsIgnoreCase("new_all")) {

                msg_alert = "Closing";
                actionCode_multi = "7";


            } else if (tab.equalsIgnoreCase("close_all")) {
                msg_alert = "Active";
                actionCode_multi = "7";

            }

            new CustomDialogClass(AllTaskActivity.this, new CustomDialogClass.OnDialogClickListener() {
                @Override
                public void onDialogImageRunClick(int positon) {
                    if (positon == 0) {
                        findViewById(R.id.rl_pb_ctask).setVisibility(View.GONE);
                    } else if (positon == 1) {
                        //str_actionid = str_actionid.replaceAll(",$", "");
                        Log.e("str_actionid", str_actionid);

                        isSmsForWhat = "cancellation";
                        smsFlagForOpen = "yes";


                        String employee_bridge_id = "";

                        if (tab.equalsIgnoreCase("new_all")) {

                            employee_bridge_id = sessionManager.getE_bridgeid();

                            assignarchived(sessionString, str_actionid, employee_bridge_id, "", new MyCallback() {
                                @Override
                                public void callbackCall(List<Chit> filteredModelList) {

                                }

                                @Override
                                public void callback_remove() {
                                    removearchived(sessionString, str_actionid, true, actionCode_multi, new ChitActionCallback() {
                                        @Override
                                        public void callback_chit() {

                                            totalcount = 0;
                                            str_actionid = "";
                                            MenuItem item1 = menu.findItem(R.id.action_delete);
                                            item1.setVisible(false);
                                            pagecount = 1;
                                            isdataavailable = true;
                                            usersignin(sessionString, pagecount, true);
                                            refundafterwithdrawn();
                                        }
                                    });
                                }
                            });

                        } else if (tab.equalsIgnoreCase("close_all")) {

                            removearchived(sessionString, str_actionid, true, actionCode_multi, new ChitActionCallback() {
                                @Override
                                public void callback_chit() {

                                    totalcount = 0;
                                    str_actionid = "";
                                    MenuItem item1 = menu.findItem(R.id.action_delete);
                                    item1.setVisible(false);
                                    pagecount = 1;
                                    isdataavailable = true;
                                    usersignin(sessionString, pagecount, true);

                                    refundafterwithdrawn();

                                }
                            });
                        }


                    }
                }
            }, "CANCEL ?").show();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    private List<Chit> filter(List<Chit> models, String query) {
        query = query.toLowerCase();
        final List<Chit> filteredModelList = new ArrayList<>();
        for (Chit model : models) {
            final String text = model.getSubject().toLowerCase();
            if (text.contains(query)) {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;
    }

    List<Chit> filteredModelList = new ArrayList<>();

    @Override
    public boolean onQueryTextChange(String newText) {
       /* final List<Chit> filteredModelList = filter(orderlist, newText);

        mAdapter.setFilter(filteredModelList);*/


        if (newText.equalsIgnoreCase("")) {
            // final List<Chit> filteredModelList = filter(orderlist, newText);
            // mAdapter.setFilter(filteredModelList);
            // setcte(null,tab,String.valueOf(filteredModelList.size()));
            // return true;
        }
        String filter = "";
        if (tab.equalsIgnoreCase("new_all")) {
            filter = "3,4,8,99,";
        } else if (tab.equalsIgnoreCase("close_all")) {  // finished - 5     ,  close - filter = "5,6,7,9,";
            filter = "5,";
        }

        if (newText.length() == 4 || newText.length() == 0) {

        } else {
            return true;
        }

        filteredModelList = searchMytask(sessionString, newText, filter, new MyCallback() {
            @Override
            public void callbackCall(List<Chit> filteredModelList) {
                mAdapter.setFilter(filteredModelList);


            }

            @Override
            public void callback_remove() {

            }
        });
        return true;
    }

    Userprofile userProfileData;
    String sms_toemployee = ""; // for employee - get from busine profile of chit - to bridge id  and for business - from user profile
    boolean isSmsEmpWhileAssign = false;


    public void assignarchived(final String usersession, String chitIds, String employee_bridge_id, final String msg, final MyCallback listener) {

        ApiInterface apiService =
                ApiClient.getClient(AllTaskActivity.this).create(ApiInterface.class);
        JsonObject a1 = new JsonObject();
        try {

            a1.addProperty("chitIds", chitIds);
            a1.addProperty("employee_bridge_id", employee_bridge_id);


            // for business - sms from getUserprofile
            // for employee - sms frrom businessprofile - - to_bridge_id (get business profile) from bridge id

            if (isSmsEmpWhileAssign) {
                isSmsEmpWhileAssign = false;
                if (sms_toemployee.equalsIgnoreCase("yes"))
                    a1.addProperty("sms", "true");
            }


        } catch (JsonParseException e) {
            e.printStackTrace();
        }
        //   Toast.makeText(MyTaskActivity.this, employee_bridge_id, Toast.LENGTH_LONG).show();

        //Call call = apiService.removechit(usersession,chitIds+",","10");


        Call call = apiService.assigntask(usersession, a1);


        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                int statusCode = response.code();
                Log.e("test", sessionString + "sessionstring");

                Headers headerList = response.headers();
                Log.d("testhistory", statusCode + " ");

                if (response.isSuccessful()) {
                    try {
                        Log.e("response.body()", response.body().toString());
                        String a = new Gson().toJson(response.body());

                        if (!msg.equalsIgnoreCase("")) {
                            Toast.makeText(AllTaskActivity.this, msg, Toast.LENGTH_LONG).show();
                        }

                        listener.callback_remove();


                    } catch (Exception e) {
                        Log.e("errortest", e.getMessage());

                    }

                } else {
                    Log.e("test", " trdds1");

                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Toast.makeText(AllTaskActivity.this, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(AllTaskActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }


            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // Log error here since request failed
                findViewById(R.id.rl_pb_ctask).setVisibility(View.GONE);
                Log.e("error message", t.toString());
            }
        });
    }

    public List<Chit> searchMytask(String usersession, String q, String filter, final MyCallback listener) {

        final List<Chit> filteredModelList = new ArrayList<>();

        ApiInterface apiService =
                ApiClient.getClient(AllTaskActivity.this).create(ApiInterface.class);
        Call call = null;
        if (tab.equalsIgnoreCase("new_all")) {
            call = apiService.searchAlltask(usersession, q, filter, 1);
        } else if (tab.equalsIgnoreCase("close_all")) {
            call = apiService.searchMytask(usersession, q, filter);
        }


        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {


                if (response.isSuccessful()) {
                    try {
                        String a = new Gson().toJson(response.body());

                        JSONObject jObjRes = new JSONObject(a);
                        JSONObject jObjdata = jObjRes.getJSONObject("response");
                        JSONObject jObjResponse = jObjdata.getJSONObject("data");
                        String totalRecord = jObjResponse.getString("totalRecord");
                        JSONArray jObjcontacts = jObjResponse.getJSONArray("content");


                        for (int i = 0; i < jObjcontacts.length(); i++) {
                            if (jObjcontacts.get(i) instanceof JSONObject) {
                                JSONObject jsnObj = (JSONObject) jObjcontacts.get(i);
                                Chit obj = new Gson().fromJson(jsnObj.toString(), Chit.class);

                                filteredModelList.add(obj);
                            }
                        }

                        listener.callbackCall(filteredModelList);

                        setcte(filteredModelList, tab, totalRecord);

                    } catch (Exception e) {

                        Toast.makeText(AllTaskActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                } else {


                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Toast.makeText(AllTaskActivity.this, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(AllTaskActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }


            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // Log error here since request failed  mSwipyRefreshLayout.setRefreshing(false);
                Log.e("error message", t.toString());
            }
        });
        return filteredModelList;

    }

    interface MyCallback {
        void callbackCall(List<Chit> filteredModelList);

        void callback_remove();
    }

    private MyCallback listener;

    public void setcte(List<Chit> orderlist, String tab, String tt) {

        int newtask = 0, closetask = 0;


        try {
           /* for (int i = 0; i < orderlist.size(); i++) {
                String t_status = orderlist.get(i).getTransaction_status();


                if (t_status.equalsIgnoreCase("ACTIVE") || t_status.equalsIgnoreCase("ACCEPTED") || t_status.equalsIgnoreCase("HOLD") || t_status.equalsIgnoreCase("INPROGRESS")) {
                    newtask++;
                }

                if (t_status.equalsIgnoreCase("FINISHED")) {
                    closetask++;
                }
            }

*/
            if (tab.equalsIgnoreCase("new_all")) {

                openstatus.setText("Open [ " + tt + " ]");
                Close_.setText("Close");
            } else {
                openstatus.setText("Open");
                Close_.setText("Close [ " + tt + " ]");

                if (tt.equalsIgnoreCase("0")) {
                    recyclerView.setVisibility(View.GONE);
                    findViewById(R.id.tv_nodata).setVisibility(View.VISIBLE);
                } else {
                    recyclerView.setVisibility(View.VISIBLE);
                    findViewById(R.id.tv_nodata).setVisibility(View.GONE);
                }

            }

        /*    if (tab.equalsIgnoreCase("new")) {

                rb_newtask.setText("Open (" + tt + ")");
            } else if (tab.equalsIgnoreCase("inprogress")) {
                rb_inprogtask.setText("Progress (" + tt + ")");
            } else if (tab.equalsIgnoreCase("close")) {
                rb_close.setText("Close (" + tt + ")");
            }*/


        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    int redirectToChitPos = -1;

    public void usersignin(String usersession, final int pagecount, final boolean isrefund) {


        ApiInterface apiService = ApiClient.getClient(AllTaskActivity.this).create(ApiInterface.class);

        Call call = null;

        if (tab.equals("new_all")) {

            filter_ = "3,4,8,99,";

            call = apiService.getMyTaskunassign(usersession, pagecount, filter_, purpose);
        } else {  // close
            filter_ = "5,";
            call = apiService.getMyTask(usersession, pagecount, filter_, purpose);
        }



        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                findViewById(R.id.rl_pb_ctask).setVisibility(View.GONE);
                mSwipyRefreshLayout.setRefreshing(false);
                int statusCode = response.code();

                if (response.isSuccessful()) {
                    try {
                        String a = new Gson().toJson(response.body());

                        JSONObject jObjRes = new JSONObject(a);
                        JSONObject jObjdata = jObjRes.getJSONObject("response");
                        JSONObject jObjResponse = jObjdata.getJSONObject("data");
                        String totalRecord = jObjResponse.getString("totalRecord");
                        JSONArray jObjcontacts = jObjResponse.getJSONArray("content");

                        String displayEndRecord = jObjResponse.getString("displayEndRecord");
                        if (totalRecord.equals(displayEndRecord)) {
                            isdataavailable = false;
                        } else {
                            isdataavailable = true;
                        }
                        /*if (jObjcontacts.length() == pagesize) {
                            isdataavailable = true;
                        } else {
                            isdataavailable = false;
                        }*/

                        List<Chit> temp_orderlist = new ArrayList<>();
                        for (int i = 0; i < jObjcontacts.length(); i++) {
                            if (jObjcontacts.get(i) instanceof JSONObject) {
                                JSONObject jsnObj = (JSONObject) jObjcontacts.get(i);
                                Chit obj = new Gson().fromJson(jsnObj.toString(), Chit.class);
                                /*if (Constants.chit_id_from_mrp != 0 && obj.getChit_id() == Constants.chit_id_from_mrp) {
                                    redirectToChitPos = i;
                                }*/
                                temp_orderlist.add(obj);
                            }
                        }

                        if (pagecount == 1) {
                            orderlist.clear();
                        }
                        orderlist.addAll(temp_orderlist);
                        mAdapter.notifyDataSetChanged();

                        if (orderlist.size() == 0) {
                            recyclerView.setVisibility(View.GONE);
                            findViewById(R.id.tv_nodata).setVisibility(View.VISIBLE);
                        } else {
                            recyclerView.setVisibility(View.VISIBLE);
                            findViewById(R.id.tv_nodata).setVisibility(View.GONE);

                           /* if (Constants.chit_id_from_mrp != 0 && orderlist.size() > redirectToChitPos) {
                                int chitId = orderlist.get(redirectToChitPos).getChit_id();
                                if (redirectToChitPos>=0 && chitId == Constants.chit_id_from_mrp) {
                                    onItemClick(orderlist.get(redirectToChitPos));
                                    recyclerView.smoothScrollToPosition(redirectToChitPos);
                                    redirectToChitPos = -1;
                                }
                            }*/

                        }
                        setcte(orderlist, tab, totalRecord);
                        if (al_refund != null && al_refund.size() > 0 && !isrefund) {
                            al_refund.clear();
                        }

                    } catch (Exception e) {

                        Toast.makeText(AllTaskActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                } else {


                    // error case
                    switch (response.code()) {
                        case 401:
                            new RefreshSession(AllTaskActivity.this, sessionManager);
                            break;
                        case 404:
                            Toast.makeText(AllTaskActivity.this, "not found", Toast.LENGTH_SHORT).show();
                            break;
                        case 500:
                            Toast.makeText(AllTaskActivity.this, "server broken", Toast.LENGTH_SHORT).show();
                            break;
                        default:
                            Toast.makeText(AllTaskActivity.this, "unknown error", Toast.LENGTH_SHORT).show();
                            break;
                    }


                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Toast.makeText(AllTaskActivity.this, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(AllTaskActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }

                try {
                    totalcount = 0;
                    str_actionid = "";
                    MenuItem item1 = menu.findItem(R.id.action_delete);
                    MenuItem action_employee = menu.findItem(R.id.action_employee);
                    item1.setVisible(false);
                    action_employee.setVisible(false);
                    showsearch(true);

                } catch (Exception e) {

                }

            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // Log error here since request failed
                findViewById(R.id.rl_pb_ctask).setVisibility(View.GONE);
                mSwipyRefreshLayout.setRefreshing(false);
            }
        });
    }

    public void Mytaskctr(String usersession, final int pagecount, final boolean isremoved) {


        ApiInterface apiService =
                ApiClient.getClient(AllTaskActivity.this).create(ApiInterface.class);

        Call call = null;

        filter_ = "5,6,7,9,";
        call = apiService.getMyTask(usersession, pagecount, filter_, purpose);


        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {

                if (response.isSuccessful()) {
                    try {
                        String a = new Gson().toJson(response.body());

                        JSONObject jObjRes = new JSONObject(a);
                        JSONObject jObjdata = jObjRes.getJSONObject("response");
                        JSONObject jObjResponse = jObjdata.getJSONObject("data");

                        JSONArray jObjcontacts = jObjResponse.getJSONArray("content");

                        List<Chit> temp_orderlist = new ArrayList<>();
                        for (int i = 0; i < jObjcontacts.length(); i++) {
                            if (jObjcontacts.get(i) instanceof JSONObject) {
                                JSONObject jsnObj = (JSONObject) jObjcontacts.get(i);
                                Chit obj = new Gson().fromJson(jsnObj.toString(), Chit.class);
                                temp_orderlist.add(obj);
                            }
                        }

                        //setcte(temp_orderlist, "");
                    } catch (Exception e) {

                        Toast.makeText(AllTaskActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                } else {

                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Toast.makeText(AllTaskActivity.this, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(AllTaskActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // Log error here since request failed
                try {
                    Log.e("Throwable", t.getMessage());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        timer = new CountDownTimer(15 * 60 * 1000, 1000) {

            public void onTick(long millisUntilFinished) {
                //Some code

                long sec = millisUntilFinished / 1000;
                Log.e("sec", String.valueOf(sec));
            }

            public void onFinish() {

                isdataavailable = true;
                pagecount = 1;
                usersignin(sessionString, 1, false);


            }
        };

        timer.start();

        findViewById(R.id.rl_pb_ctask).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

    }

    @Override
    protected void onPause() {
        super.onPause();
        timer.cancel();

    }

    CountDownTimer timer;
    String filter_ = "";

    interface ChitActionCallback {
        void callback_chit();
    }

    private ChitActionCallback chitlistener;

    public void refundafterwithdrawn() {
        if (al_refund != null && al_refund.size() > 0) {
            new CanelOrder_Refund(AllTaskActivity.this, al_refund.get(0), new CanelOrder_Refund.RefundCallback() {
                @Override
                public void showconfirmcancel() {

                }

                @Override
                public void showdescription() {

                }

                @Override
                public void refunded() {
                    al_refund.remove(0);
                    refundafterwithdrawn();
                }
            });
        }

    }


    String purpose = "";

    public void showsearch(boolean isshow) {
        MenuItem action_search = menu.findItem(R.id.action_search);
        action_search.setVisible(isshow);

        MenuItem action_filter = menu.findItem(R.id.action_filter);
        //action_filter.setVisible(isshow);
    }


}
