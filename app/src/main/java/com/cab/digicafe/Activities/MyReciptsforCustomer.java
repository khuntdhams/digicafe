package com.cab.digicafe.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.andremion.counterfab.CounterFab;
import com.cab.digicafe.Adapter.HistoryAdapter;
import com.cab.digicafe.BaseActivity;
import com.cab.digicafe.ChitlogActivity;
import com.cab.digicafe.DetailsFragment;
import com.cab.digicafe.Dialogbox.CustomDialogClass;
import com.cab.digicafe.Fragments.SummaryDetailFragmnet;
import com.cab.digicafe.Helper.Inad;
import com.cab.digicafe.Helper.RecyclerItemTouchHelper;
import com.cab.digicafe.Helper.SessionManager;
import com.cab.digicafe.Model.Chit;
import com.cab.digicafe.R;
import com.cab.digicafe.Rest.ApiClient;
import com.cab.digicafe.Rest.ApiInterface;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import info.hoang8f.android.segmented.SegmentedGroup;
import okhttp3.Headers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyReciptsforCustomer extends BaseActivity implements RadioGroup.OnCheckedChangeListener, SearchView.OnQueryTextListener, HistoryAdapter.OnItemClickListener, RecyclerItemTouchHelper.RecyclerItemTouchHelperListener, SwipyRefreshLayout.OnRefreshListener {
    private RecyclerView recyclerView;
    private HistoryAdapter mAdapter;
    CounterFab counterFab;
    private List<Chit> orderlist = new ArrayList<>();
    SessionManager sessionManager;
    String sessionString;
    MenuItem item;
    Menu menu;
    String status_code = "4";
    int totalcount = 0;
    String str_actionid = "";
    SegmentedGroup ststussegmented;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.content_history, frameLayout);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("My Receipt");
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        sessionManager = new SessionManager(this);
        // listen refresh event
        counterFab = (CounterFab) findViewById(R.id.counter_fab);
        counterFab.setVisibility(View.GONE);
        sessionManager = new SessionManager(this);

        ststussegmented = (SegmentedGroup) findViewById(R.id.segmented2);
        ststussegmented.setOnCheckedChangeListener(this);

        Log.e("i am here", "create");
        mAdapter = new HistoryAdapter(orderlist, this, this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new RecyclerItemTouchHelper(0, 0, this);
        new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(recyclerView);


        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                // bridgeidval = null;
            } else {
                sessionString = extras.getString("session");
                //  bridgeidval= extras.getString("bridgeidval");

            }
        } else {
            sessionString = (String) savedInstanceState.getSerializable("session");
            // bridgeidval= (String) savedInstanceState.getSerializable("bridgeidval");
        }

        refreshlist();
        usersignin(sessionString, 1, false);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (totalcount > 0) {
            try {
                for (int i = 0; i < orderlist.size(); i++) {
                    if (orderlist.get(i).isIslongpress()) {
                        orderlist.get(i).setIslongpress(false);
                    }
                }

                totalcount = 0;
                str_actionid = "";

                MenuItem item1 = menu.findItem(R.id.action_delete);
                item1.setVisible(false);
                mAdapter.notifyDataSetChanged();
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            /*AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.AlertDialogCustom));
            builder
                    .setMessage(getString(R.string.quit))
                    .setTitle("")
                    .setCancelable(false)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            finish();
                            Intent intent = new Intent(Intent.ACTION_MAIN);
                            intent.addCategory(Intent.CATEGORY_HOME);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        }
                    })
                    .setNegativeButton("No", null)

                    .show();*/

            Inad.exitalert(this);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);


        final MenuItem item = menu.findItem(R.id.action_search);

        /*MenuItem item1 = menu.findItem(R.id.action_filter);
        item1.setVisible(false);*/

        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setInputType(InputType.TYPE_CLASS_NUMBER);
        searchView.setOnQueryTextListener(this);

        MenuItemCompat.setOnActionExpandListener(item,
                new MenuItemCompat.OnActionExpandListener() {
                    @Override
                    public boolean onMenuItemActionCollapse(MenuItem item) {
// Do something when collapsed
                        mAdapter.setFilter(orderlist);
                        return true; // Return true to collapse action view
                    }

                    @Override
                    public boolean onMenuItemActionExpand(MenuItem item) {
// Do something when expanded
                        return true; // Return true to expand action view
                    }
                });


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        if (id == R.id.action_delete) {
            new CustomDialogClass(MyReciptsforCustomer.this, new CustomDialogClass.OnDialogClickListener() {
                @Override
                public void onDialogImageRunClick(int positon) {
                    if (positon == 0) {

                    } else if (positon == 1) {
                        //str_actionid = str_actionid.replaceAll(",$", "");
                        Log.e("str_actionid", str_actionid);
                        removechit(sessionString, str_actionid, false);
                    }
                }
            }, "Are you sure you want to add " + totalcount + " chit in archived?").show();
        }

        return super.onOptionsItemSelected(item);
    }


    public void usersignin(String usersession, final int pagecount, final boolean isremoved) {


        ApiInterface apiService =
                ApiClient.getClient(MyReciptsforCustomer.this).create(ApiInterface.class);
        Call call = apiService.getReceiptsforcustomer(usersession, pagecount);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                mSwipyRefreshLayout.setRefreshing(false);
                int statusCode = response.code();

                if (response.isSuccessful()) {
                    try {
                        String a = new Gson().toJson(response.body());

                        JSONObject jObjRes = new JSONObject(a);
                        JSONObject jObjdata = jObjRes.getJSONObject("response");
                        JSONObject jObjResponse = jObjdata.getJSONObject("data");
                        String totalRecord = jObjResponse.getString("totalRecord");
                        JSONArray jObjcontacts = jObjResponse.getJSONArray("content");
                        /*if (jObjcontacts.length() < pagesize) {
                            isdataavailable = false;
                        } else {
                            isdataavailable = true;
                        }*/

                        String displayEndRecord = jObjResponse.getString("displayEndRecord");
                        if (totalRecord.equals(displayEndRecord)) {
                            isdataavailable = false;
                        } else {
                            isdataavailable = true;
                        }


                        List<Chit> temp_orderlist = new ArrayList<>();
                        for (int i = 0; i < jObjcontacts.length(); i++) {
                            if (jObjcontacts.get(i) instanceof JSONObject) {
                                JSONObject jsnObj = (JSONObject) jObjcontacts.get(i);
                                Chit obj = new Gson().fromJson(jsnObj.toString(), Chit.class);
                                Log.e("alias", obj.getChit_name());
                                temp_orderlist.add(obj);
                            }
                        }

                        if (pagecount == 1) {
                            orderlist.clear();
                        }
                        orderlist.addAll(temp_orderlist);
                        mAdapter.notifyDataSetChanged();

                        if (orderlist.size() == 0) {
                            getSupportActionBar().setTitle("My Receipt");
                            recyclerView.setVisibility(View.GONE);
                            findViewById(R.id.tv_nodata).setVisibility(View.VISIBLE);
                        } else {
                            getSupportActionBar().setTitle("My Receipt [" + totalRecord + "]");
                            recyclerView.setVisibility(View.VISIBLE);
                            findViewById(R.id.tv_nodata).setVisibility(View.GONE);
                        }
                    } catch (Exception e) {
                        Log.e("errortest", e.getMessage());

                        Toast.makeText(MyReciptsforCustomer.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                } else {
                    Log.e("test", " trdds1");

                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Toast.makeText(MyReciptsforCustomer.this, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(MyReciptsforCustomer.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // Log error here since request failed
                mSwipyRefreshLayout.setRefreshing(false);
                Log.e("error message", t.toString());
            }
        });
    }


    @Override
    public void onItemClick(Chit item) {
        Intent i = new Intent(MyReciptsforCustomer.this, ChitlogActivity.class);
        i.putExtra("chit_hash_id", item.getChit_hash_id());
        i.putExtra("chit_id", item.getChit_id() + "");
        i.putExtra("transtactionid", item.getChit_name() + "");
        i.putExtra("productqty", item.getTotal_chit_item_value() + "");
        i.putExtra("productprice", item.getChit_item_count() + "");
        i.putExtra("purpose", item.getPurpose());
        i.putExtra("Refid", item.getRef_id());
        i.putExtra("Caseid", item.getCase_id());
        DetailsFragment.chititemcount = "" + item.getChit_item_count();
        DetailsFragment.chititemprice = "" + item.getTotal_chit_item_value();
        SummaryDetailFragmnet.item = item;
        DetailsFragment.item_detail = item;
        startActivity(i);

    }

    @Override
    public void onItemLongClick(Chit item, int pos) {
        Log.e("test", " trdds1");
        if (orderlist.get(pos).isIslongpress()) {
            orderlist.get(pos).setIslongpress(false);
        } else {
            orderlist.get(pos).setIslongpress(true);
        }

        boolean isanyitem = false;
        str_actionid = "";
        totalcount = 0;
        for (int i = 0; i < orderlist.size(); i++) {
            if (orderlist.get(i).isIslongpress()) {
                totalcount++;
                isanyitem = true;
                str_actionid = str_actionid + String.valueOf(orderlist.get(i).getChit_id()) + ",";
            }
        }

        mAdapter.notifyDataSetChanged();


        Log.e("totalcount", String.valueOf(totalcount));
        MenuItem item1 = menu.findItem(R.id.action_delete);
        if (totalcount > 0) {
            item1.setVisible(true);
        } else {
            item1.setVisible(false);
        }

    }


    public void removechit(final String usersession, String chitIds, final boolean isswipe) {

        ApiInterface apiService =
                ApiClient.getClient(MyReciptsforCustomer.this).create(ApiInterface.class);
        JsonObject a1 = new JsonObject();
        try {

            a1.addProperty("chitIds", chitIds);

            a1.addProperty("actionCode", status_code);


        } catch (JsonParseException e) {
            e.printStackTrace();
        }


        //Call call = apiService.removechit(usersession,chitIds+",","10");
        Call call = apiService.removechit(usersession, a1);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                int statusCode = response.code();

                Headers headerList = response.headers();
                if (response.isSuccessful()) {
                    try {
                        String a = new Gson().toJson(response.body());
                        Toast.makeText(MyReciptsforCustomer.this, "Order Archived!", Toast.LENGTH_LONG).show();
                        //Inad.alerter("Order Removed!",HistoryActivity.this);

                    } catch (Exception e) {
                        Log.e("errortest", e.getMessage());
                        //Toast.makeText(HistoryActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }

                } else {
                    Log.e("test", " trdds1");

                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Toast.makeText(MyReciptsforCustomer.this, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(MyReciptsforCustomer.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }

                totalcount = 0;
                str_actionid = "";
                MenuItem item1 = menu.findItem(R.id.action_delete);
                item1.setVisible(false);
                pagecount = 1;
                isdataavailable = true;
                usersignin(usersession, pagecount, true);

            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // Log error here since request failed

            }
        });
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {
        if (viewHolder instanceof HistoryAdapter.MyViewHolder) {
            final Chit deletedItem = orderlist.get(viewHolder.getAdapterPosition());
            final int deletedIndex = viewHolder.getAdapterPosition();
            mAdapter.removeItem(viewHolder.getAdapterPosition());

            String actionid = String.valueOf(deletedItem.getChit_id());
            actionid = actionid + ",";
            removechit(sessionString, actionid, true);

        }


    }


    @Override
    public void onRefresh(SwipyRefreshLayoutDirection direction) {
        //mSwipyRefreshLayout.setRefreshing(false);

        Log.e("scroll direction", "Refresh triggered at "
                + (direction == SwipyRefreshLayoutDirection.TOP ? "top" : "bottom"));
        if (direction == SwipyRefreshLayoutDirection.TOP) {
            isdataavailable = true;
            pagecount = 1;
            usersignin(sessionString, pagecount, false);
        } else if (direction == SwipyRefreshLayoutDirection.BOTTOM) {
            if (isdataavailable) {
                pagecount++;
                usersignin(sessionString, pagecount, false);
            } else {
                mSwipyRefreshLayout.setRefreshing(false);
                Toast.makeText(this, "No more data available", Toast.LENGTH_LONG).show();
            }

        }
    }

    SwipyRefreshLayout mSwipyRefreshLayout;
    int pagecount = 1;
    boolean isdataavailable = true;
    int pagesize = 25;

    public void refreshlist() {
        mSwipyRefreshLayout = (SwipyRefreshLayout) findViewById(R.id.swipyrefreshlayout);
        mSwipyRefreshLayout.setOnRefreshListener(this);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    private List<Chit> filter(List<Chit> models, String query) {
        query = query.toLowerCase();
        final List<Chit> filteredModelList = new ArrayList<>();
        for (Chit model : models) {
            final String text = model.getSubject().toLowerCase();
            if (text.contains(query)) {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;
    }


    @Override
    public boolean onQueryTextChange(String newText) {
       /* final List<Chit> filteredModelList = filter(orderlist, newText);

        mAdapter.setFilter(filteredModelList);*/

        if (newText.equalsIgnoreCase("")) {
            final List<Chit> filteredModelList = filter(orderlist, newText);
            mAdapter.setFilter(filteredModelList);
            return true;
        }


        searchMytask(sessionString, newText, "", new MyCallback() {
            @Override
            public void callbackCall(List<Chit> filteredModelList) {
                mAdapter.setFilter(filteredModelList);
            }
        });

        return true;
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.openstatus:
                status_code = "4";
                break;
            case R.id.inprogressstatus:
                status_code = "9";
                break;
            case R.id.closestatus:
                status_code = "4";
                break;

            default:
                // Nothing to do
        }
    }


    public List<Chit> searchMytask(String usersession, String q, String filter, final MyCallback listener) {

        final List<Chit> filteredModelList = new ArrayList<>();

        ApiInterface apiService =
                ApiClient.getClient(MyReciptsforCustomer.this).create(ApiInterface.class);
        Call call = apiService.searchOrder(usersession, q, filter);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {


                if (response.isSuccessful()) {
                    try {
                        String a = new Gson().toJson(response.body());

                        JSONObject jObjRes = new JSONObject(a);
                        JSONObject jObjdata = jObjRes.getJSONObject("response");
                        JSONObject jObjResponse = jObjdata.getJSONObject("data");
                        String totalRecord = jObjResponse.getString("totalRecord");
                        JSONArray jObjcontacts = jObjResponse.getJSONArray("content");


                        for (int i = 0; i < jObjcontacts.length(); i++) {
                            if (jObjcontacts.get(i) instanceof JSONObject) {
                                JSONObject jsnObj = (JSONObject) jObjcontacts.get(i);
                                Chit obj = new Gson().fromJson(jsnObj.toString(), Chit.class);

                                filteredModelList.add(obj);
                            }
                        }

                        listener.callbackCall(filteredModelList);

                        // getSupportActionBar().setTitle("My Order [" + totalRecord + "]");


                    } catch (Exception e) {

                        Toast.makeText(MyReciptsforCustomer.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                } else {


                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Toast.makeText(MyReciptsforCustomer.this, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(MyReciptsforCustomer.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }


            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // Log error here since request failed  mSwipyRefreshLayout.setRefreshing(false);
                Log.e("error message", t.toString());
            }
        });
        return filteredModelList;

    }

    interface MyCallback {
        void callbackCall(List<Chit> filteredModelList);
    }

    private MyCallback listener;

}
