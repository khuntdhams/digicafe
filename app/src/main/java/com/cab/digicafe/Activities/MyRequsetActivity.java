package com.cab.digicafe.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.andremion.counterfab.CounterFab;
import com.cab.digicafe.Adapter.MyRequsetAdapter;
import com.cab.digicafe.BaseActivity;
import com.cab.digicafe.Dialogbox.CustomDialogClass;
import com.cab.digicafe.Dialogbox.EmployeeListDialog;
import com.cab.digicafe.Helper.Inad;
import com.cab.digicafe.Helper.Req_Issue_ItemTouchHelper;
import com.cab.digicafe.Helper.SessionManager;
import com.cab.digicafe.MainActivity;
import com.cab.digicafe.Model.Chit;
import com.cab.digicafe.Model.Employee;
import com.cab.digicafe.MyCustomClass.CanelOrder_Refund;
import com.cab.digicafe.R;
import com.cab.digicafe.Rest.ApiClient;
import com.cab.digicafe.Rest.ApiInterface;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import info.hoang8f.android.segmented.SegmentedGroup;
import okhttp3.Headers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyRequsetActivity extends BaseActivity implements SearchView.OnQueryTextListener, SwipyRefreshLayout.OnRefreshListener, Req_Issue_ItemTouchHelper.RecyclerItemTouchHelperListener {

    private RecyclerView recyclerView;
    private MyRequsetAdapter mAdapter;
    CounterFab counterFab;
    private List<Chit> orderlist = new ArrayList<>();
    SessionManager sessionManager;
    String sessionString;
    MenuItem item;
    Menu menu;
    SegmentedGroup s_group;
    String purpose = "";

    int totalcount = 0;
    String str_actionid = "";

    String tab = "new_all";
    RadioButton openstatus, Close_, Archive_;


    @Override
    public void onTouchSwipe(boolean isSwipeEnable) {
        mSwipyRefreshLayout.setEnabled(isSwipeEnable);

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.content_mytask, frameLayout);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        refreshlist();

        sessionManager = new SessionManager(this);


        s_group = (SegmentedGroup) findViewById(R.id.segmented2);


        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        counterFab = (CounterFab) findViewById(R.id.counter_fab);
        counterFab.setVisibility(View.GONE);

        openstatus = (RadioButton) findViewById(R.id.openstatus);
        Close_ = (RadioButton) findViewById(R.id.closestatus);

        Archive_ = (RadioButton) findViewById(R.id.inprogressstatus);

        Archive_.setVisibility(View.GONE);


        openstatus.setText("Open");
        Close_.setText("Close");

        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                // bridgeidval = null;
            } else {
                sessionString = extras.getString("session");
                purpose = extras.getString("purpose", "");
                //  bridgeidval= extras.getString("bridgeidval");

            }
        } else {
            sessionString = (String) savedInstanceState.getSerializable("session");
            // bridgeidval= (String) savedInstanceState.getSerializable("bridgeidval");
        }
        getSupportActionBar().setTitle(purpose);

        if (purpose.equalsIgnoreCase("issue")) {
            s_group.setVisibility(View.VISIBLE);
        } else {
            s_group.setVisibility(View.GONE);
        }


        s_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {


                findViewById(R.id.rl_pb_ctask).setVisibility(View.VISIBLE);

                switch (checkedId) {
                    case R.id.openstatus:
                        tab = "new_all";
                        break;
                    case R.id.closestatus:
                        tab = "close_all";
                        break;
                    default:
                        break;
                }


                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(MyRequsetActivity.this);
                recyclerView.setLayoutManager(mLayoutManager);
                //recyclerView.setItemAnimator(new DefaultItemAnimator());

                mAdapter = new MyRequsetAdapter(false, orderlist, MyRequsetActivity.this, new MyRequsetAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(Chit item) {
                    }

                    @Override
                    public void onItemLongClick(Chit item, int pos) {

                        if (purpose.equalsIgnoreCase("issue")) {

                            if (orderlist.get(pos).isIslongpress()) {
                                orderlist.get(pos).setIslongpress(false);
                            } else {
                                orderlist.get(pos).setIslongpress(true);
                            }

                            boolean isanyitem = false;
                            str_actionid = "";
                            totalcount = 0;
                            for (int i = 0; i < orderlist.size(); i++) {
                                if (orderlist.get(i).isIslongpress()) {
                                    totalcount++;
                                    isanyitem = true;
                                    str_actionid = str_actionid + String.valueOf(orderlist.get(i).getChit_id()) + ",";
                                }
                            }

                            mAdapter.notifyDataSetChanged();


                            Log.e("totalcount", String.valueOf(totalcount));
                            MenuItem item1 = menu.findItem(R.id.action_delete);
                            if (totalcount > 0) {

                                showsearch(false);
                            } else {
                                showsearch(true);

                            }


                            /****************  Multiple Refund  *********************/

                            try {
                                JSONObject jo_summary = new JSONObject(item.getFooter_note());

                                if (jo_summary.getString("PAYMENT_MODE").equals("RAZORPAY")) {
                                    boolean isadded = false;
                                    for (int i = 0; i < al_refund.size(); i++) {
                                        String payuResponse = al_refund.get(i);
                                        if (jo_summary.getString("payuResponse").equals(payuResponse)) {
                                            isadded = true;
                                            if (isadded) {
                                                al_refund.remove(i);
                                                break;
                                            }
                                        }
                                    }
                                    if (!isadded) {
                                        al_refund.add(jo_summary.getString("payuResponse"));
                                    }


                                } else {

                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                        } else {

                        }


                    }
                }, purpose, tab);

                recyclerView.setAdapter(mAdapter);
                recyclerView.setNestedScrollingEnabled(false);

                ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new Req_Issue_ItemTouchHelper(0, 0, MyRequsetActivity.this);
                if (purpose.equalsIgnoreCase("issue"))
                    new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(recyclerView);

                pagecount = 1;
                usersignin(sessionString, 1, false);

            }
        });

        openstatus.setChecked(true);


    }


    SwipyRefreshLayout mSwipyRefreshLayout;
    int pagecount = 1;
    boolean isdataavailable = true;
    int pagesize = 50;

    public void refreshlist() {
        mSwipyRefreshLayout = (SwipyRefreshLayout) findViewById(R.id.swipyrefreshlayout);
        mSwipyRefreshLayout.setDirection(SwipyRefreshLayoutDirection.BOTH);
        mSwipyRefreshLayout.setOnRefreshListener(this);
    }


    String filter_ = "";

    public void usersignin(String usersession, final int pagecount, final boolean isrefund) {


        ApiInterface apiService =
                ApiClient.getClient(MyRequsetActivity.this).create(ApiInterface.class);

        if (purpose.equalsIgnoreCase("issue")) {
            if (tab.equals("new_all")) {
                filter_ = "3,4,8,99,";
            } else {  // close
                filter_ = "5,";
            }
        } else {
            filter_ = "";
        }

        Call call = apiService.getSenditems(usersession, pagecount, purpose.toLowerCase(), filter_);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                findViewById(R.id.rl_pb_ctask).setVisibility(View.GONE);
                mSwipyRefreshLayout.setRefreshing(false);
                int statusCode = response.code();

                if (response.isSuccessful()) {
                    try {
                        String a = new Gson().toJson(response.body());

                        JSONObject jObjRes = new JSONObject(a);
                        JSONObject jObjdata = jObjRes.getJSONObject("response");
                        JSONObject jObjResponse = jObjdata.getJSONObject("data");
                        String totalRecord = jObjResponse.getString("totalRecord");
                        JSONArray jObjcontacts = jObjResponse.getJSONArray("content");
                        /*if (jObjcontacts.length() < pagesize) {
                            isdataavailable = false;
                        } else {
                            isdataavailable = true;
                        }*/
                        String displayEndRecord = jObjResponse.getString("displayEndRecord");

                        if (totalRecord.equals(displayEndRecord)) {
                            isdataavailable = false;
                        } else {
                            isdataavailable = true;
                        }



                      /*  if (jObjcontacts.length() == pagesize) {
                            isdataavailable = true;
                        } else {
                            isdataavailable = false;
                        }*/


                        List<Chit> temp_orderlist = new ArrayList<>();
                        for (int i = 0; i < jObjcontacts.length(); i++) {
                            if (jObjcontacts.get(i) instanceof JSONObject) {
                                JSONObject jsnObj = (JSONObject) jObjcontacts.get(i);
                                Chit obj = new Gson().fromJson(jsnObj.toString(), Chit.class);
                                Log.e("alias", obj.getChit_name());
                                temp_orderlist.add(obj);
                            }
                        }

                        if (pagecount == 1) {
                            orderlist.clear();
                        }
                        orderlist.addAll(temp_orderlist);
                        mAdapter.notifyDataSetChanged();

                        if (orderlist.size() == 0) {
                            getSupportActionBar().setTitle(purpose);
                            recyclerView.setVisibility(View.GONE);
                            findViewById(R.id.tv_nodata).setVisibility(View.VISIBLE);
                        } else {
                            if (purpose.equalsIgnoreCase("issue")) {
                                getSupportActionBar().setTitle(purpose);
                            } else {
                                getSupportActionBar().setTitle(purpose + " [ " + totalRecord + " ]");
                            }

                            recyclerView.setVisibility(View.VISIBLE);
                            findViewById(R.id.tv_nodata).setVisibility(View.GONE);
                        }

                        setcte(orderlist, tab, totalRecord);

                        if (al_refund != null && al_refund.size() > 0 && !isrefund) {
                            al_refund.clear();
                        }

                    } catch (Exception e) {
                        Log.e("errortest", e.getMessage());

                        Toast.makeText(MyRequsetActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                } else {
                    Log.e("test", " trdds1");

                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Toast.makeText(MyRequsetActivity.this, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(MyRequsetActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // Log error here since request failed
                findViewById(R.id.rl_pb_ctask).setVisibility(View.GONE);
                mSwipyRefreshLayout.setRefreshing(false);
                Log.e("error message", t.toString());
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);


        final MenuItem item = menu.findItem(R.id.action_search);
        final MenuItem item_del = menu.findItem(R.id.action_add);
        item_del.setVisible(true);
        /*MenuItem item1 = menu.findItem(R.id.action_filter);
        item1.setVisible(false);*/

        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setInputType(InputType.TYPE_CLASS_NUMBER);
        searchView.setOnQueryTextListener(this);

        MenuItemCompat.setOnActionExpandListener(item,
                new MenuItemCompat.OnActionExpandListener() {
                    @Override
                    public boolean onMenuItemActionCollapse(MenuItem item) {
// Do something when collapsed
                        mAdapter.setFilter(orderlist);
                        return true; // Return true to collapse action view
                    }

                    @Override
                    public boolean onMenuItemActionExpand(MenuItem item) {
// Do something when expanded
                        return true; // Return true to expand action view
                    }
                });


        return true;
    }

    String msg_alert = "";
    String actionCode_multi = "";

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.


        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }


        if (id == R.id.action_employee) {
            new EmployeeListDialog(MyRequsetActivity.this, new EmployeeListDialog.OnDialogClickListener() {
                @Override
                public void onDialogImageRunClick(int pos, Employee add) {


                    if (pos == 1 && add != null) {
                        findViewById(R.id.rl_pb_ctask).setVisibility(View.VISIBLE);

                        assignarchived(sessionString, str_actionid, add.getEmployee_bridge_id(), "ASSIGN", new MyCallback() {
                            @Override
                            public void callbackCall(List<Chit> filteredModelList) {

                            }

                            @Override
                            public void callback_remove() {

                                totalcount = 0;
                                str_actionid = "";


                                MenuItem item1 = menu.findItem(R.id.action_delete);
                                MenuItem action_employee = menu.findItem(R.id.action_employee);
                                item1.setVisible(false);
                                action_employee.setVisible(false);
                                showsearch(true);


                                pagecount = 1;
                                isdataavailable = true;
                                usersignin(sessionString, pagecount, false);

                            }
                        });


                    }
                }
            }).show();

        }

        if (id == R.id.action_delete) {


            if (tab.equalsIgnoreCase("new_all")) {
                actionCode_multi = "7";


            } else if (tab.equalsIgnoreCase("close_all")) {
                actionCode_multi = "7";

            }


            new CustomDialogClass(MyRequsetActivity.this, new CustomDialogClass.OnDialogClickListener() {
                @Override
                public void onDialogImageRunClick(int positon) {
                    if (positon == 0) {

                    } else if (positon == 1) {
                        findViewById(R.id.rl_pb_ctask).setVisibility(View.VISIBLE);
                        //str_actionid = str_actionid.replaceAll(",$", "");
                        Log.e("str_actionid", str_actionid);
                        // removechit(sessionString, str_actionid, false);


                        String employee_bridge_id = "";

                        if (tab.equalsIgnoreCase("new_all")) {

                            employee_bridge_id = sessionManager.getE_bridgeid();

                            assignarchived(sessionString, str_actionid, employee_bridge_id, "", new MyCallback() {
                                @Override
                                public void callbackCall(List<Chit> filteredModelList) {

                                }

                                @Override
                                public void callback_remove() {
                                    removearchived(sessionString, str_actionid, true, actionCode_multi, new ChitActionCallback() {
                                        @Override
                                        public void callback_chit() {

                                            totalcount = 0;
                                            str_actionid = "";
                                            MenuItem item1 = menu.findItem(R.id.action_delete);
                                            item1.setVisible(false);
                                            pagecount = 1;
                                            isdataavailable = true;
                                            usersignin(sessionString, pagecount, true);

                                            refundafterwithdrawn();

                                        }
                                    });
                                }
                            });

                        } else if (tab.equalsIgnoreCase("close_all")) {

                            removearchived(sessionString, str_actionid, true, actionCode_multi, new ChitActionCallback() {
                                @Override
                                public void callback_chit() {

                                    totalcount = 0;
                                    str_actionid = "";
                                    MenuItem item1 = menu.findItem(R.id.action_delete);
                                    item1.setVisible(false);
                                    pagecount = 1;
                                    isdataavailable = true;
                                    usersignin(sessionString, pagecount, true);

                                    refundafterwithdrawn();

                                }
                            });
                        }


                    }
                }
            }, "CANCEL ?").show();

        }

        if (id == R.id.action_add) {

            Intent mainIntent = new Intent(this, MainActivity.class);
            mainIntent.putExtra("session", sessionManager.getsession());
            mainIntent.putExtra("isrequset", true);
            mainIntent.putExtra("purpose", purpose);

            startActivity(mainIntent);

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (newText.equalsIgnoreCase("")) {
            final List<Chit> filteredModelList = filter(orderlist, newText);
            mAdapter.setFilter(filteredModelList);
            return true;
        }

        if (newText.length() == 4 || newText.length() == 0) {

        } else {
            return true;
        }


        searchMytask(sessionString, newText, "", new MyCallback() {
            @Override
            public void callbackCall(List<Chit> filteredModelList) {
                mAdapter.setFilter(filteredModelList);
            }

            @Override
            public void callback_remove() {

            }
        });

        return true;
    }

    private List<Chit> filter(List<Chit> models, String query) {
        query = query.toLowerCase();
        final List<Chit> filteredModelList = new ArrayList<>();
        for (Chit model : models) {
            final String text = model.getSubject().toLowerCase();
            if (text.contains(query)) {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;
    }


    @Override
    public void onRefresh(SwipyRefreshLayoutDirection direction) {

        if (direction == SwipyRefreshLayoutDirection.TOP) {
            isdataavailable = true;
            pagecount = 1;
            usersignin(sessionString, pagecount, false);
        } else if (direction == SwipyRefreshLayoutDirection.BOTTOM) {
            if (isdataavailable) {
                pagecount++;
                usersignin(sessionString, pagecount, false);
            } else {
                mSwipyRefreshLayout.setRefreshing(false);
                Toast.makeText(this, "No more data available", Toast.LENGTH_LONG).show();
            }
        }

    }


    public List<Chit> searchMytask(String usersession, String q, String filter, final MyCallback listener) {

        final List<Chit> filteredModelList = new ArrayList<>();

        ApiInterface apiService =
                ApiClient.getClient(MyRequsetActivity.this).create(ApiInterface.class);
        Call call = apiService.searchOrder(usersession, q, filter);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {


                if (response.isSuccessful()) {
                    try {
                        String a = new Gson().toJson(response.body());

                        JSONObject jObjRes = new JSONObject(a);
                        JSONObject jObjdata = jObjRes.getJSONObject("response");
                        JSONObject jObjResponse = jObjdata.getJSONObject("data");
                        String totalRecord = jObjResponse.getString("totalRecord");
                        JSONArray jObjcontacts = jObjResponse.getJSONArray("content");


                        for (int i = 0; i < jObjcontacts.length(); i++) {
                            if (jObjcontacts.get(i) instanceof JSONObject) {
                                JSONObject jsnObj = (JSONObject) jObjcontacts.get(i);
                                Chit obj = new Gson().fromJson(jsnObj.toString(), Chit.class);

                                filteredModelList.add(obj);
                            }
                        }

                        listener.callbackCall(filteredModelList);

                        // getSupportActionBar().setTitle("My Order [" + totalRecord + "]");


                    } catch (Exception e) {

                        Toast.makeText(MyRequsetActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                } else {


                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Toast.makeText(MyRequsetActivity.this, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(MyRequsetActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }


            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // Log error here since request failed  mSwipyRefreshLayout.setRefreshing(false);
                Log.e("error message", t.toString());
            }
        });
        return filteredModelList;

    }


    interface MyCallback {
        void callbackCall(List<Chit> filteredModelList);

        void callback_remove();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (totalcount > 0) {
            try {
                for (int i = 0; i < orderlist.size(); i++) {
                    if (orderlist.get(i).isIslongpress()) {
                        orderlist.get(i).setIslongpress(false);
                    }
                }

                totalcount = 0;
                str_actionid = "";

                MenuItem item1 = menu.findItem(R.id.action_delete);
                //item1.setVisible(false);
                showsearch(true);
                mAdapter.notifyDataSetChanged();

                if (al_refund != null && al_refund.size() > 0) {
                    al_refund.clear();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            Inad.exitalert(this);

        }
    }

    public void showsearch(boolean isshow) {
        MenuItem action_search = menu.findItem(R.id.action_search);
        action_search.setVisible(isshow);

        MenuItem action_filter = menu.findItem(R.id.action_add);
        action_filter.setVisible(isshow);

        isshow = !isshow;
        MenuItem action_delete = menu.findItem(R.id.action_delete);
        action_delete.setVisible(isshow);

        MenuItem action_employee = menu.findItem(R.id.action_employee);
        action_employee.setVisible(isshow);
    }


    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {


        if (viewHolder instanceof MyRequsetAdapter.MyViewHolder) {

            findViewById(R.id.rl_pb_ctask).setVisibility(View.VISIBLE);

            final Chit deletedItem = orderlist.get(viewHolder.getAdapterPosition());
            final int deletedIndex = viewHolder.getAdapterPosition();
            mAdapter.removeItem(viewHolder.getAdapterPosition());

            String actionid = String.valueOf(deletedItem.getChit_id());
            actionid = actionid + ",";

            String actionCode = "";

            String msg = "";

            if (tab.equalsIgnoreCase("new_all")) {

                if (direction == ItemTouchHelper.LEFT) {

                    // new ass
                    actionCode = "";
                    msg = "MY TASK";
                } else if (direction == ItemTouchHelper.RIGHT) {

                    actionCode = "5";

                }

            } else if (tab.equalsIgnoreCase("close_all")) {
                if (direction == ItemTouchHelper.LEFT) {

                    // new ass
                    // actionCode = "4";
                    actionCode = "2";
                } else if (direction == ItemTouchHelper.RIGHT) {

                }

            }

            if (actionCode.equalsIgnoreCase("")) {

                assignarchived(sessionString, actionid, sessionManager.getE_bridgeid(), msg, new MyCallback() {
                    @Override
                    public void callbackCall(List<Chit> filteredModelList) {

                    }

                    @Override
                    public void callback_remove() {

                        totalcount = 0;
                        str_actionid = "";
                        MenuItem item1 = menu.findItem(R.id.action_delete);
                        MenuItem action_employee = menu.findItem(R.id.action_employee);
                        item1.setVisible(false);
                        action_employee.setVisible(false);
                        showsearch(true);
                        pagecount = 1;
                        isdataavailable = true;
                        usersignin(sessionString, pagecount, false);

                    }
                });
            } else {

                String employee_bridge_id = "";
                final String f_actionid = actionid;
                final String f_actionCode = actionCode;

                if (actionCode.equalsIgnoreCase("5")) {
                    employee_bridge_id = sessionManager.getE_bridgeid();

                    assignarchived(sessionString, actionid, employee_bridge_id, msg, new MyCallback() {
                        @Override
                        public void callbackCall(List<Chit> filteredModelList) {

                        }

                        @Override
                        public void callback_remove() {

                            removearchived(sessionString, f_actionid, true, f_actionCode, new ChitActionCallback() {
                                @Override
                                public void callback_chit() {

                                    totalcount = 0;
                                    str_actionid = "";
                                    MenuItem item1 = menu.findItem(R.id.action_delete);
                                    MenuItem action_employee = menu.findItem(R.id.action_employee);
                                    item1.setVisible(false);
                                    action_employee.setVisible(false);
                                    showsearch(true);
                                    pagecount = 1;
                                    isdataavailable = true;
                                    usersignin(sessionString, pagecount, false);

                                }
                            });
                        }
                    });

                } else if (actionCode.equalsIgnoreCase("2")) {
                    employee_bridge_id = "";


                    final String finalActionid = actionid;
                    final String finalEmployee_bridge_id = employee_bridge_id;
                    final String finalMsg = msg;

                    removearchived(sessionString, f_actionid, true, f_actionCode, new ChitActionCallback() {
                        @Override
                        public void callback_chit() {

                            if (sessionManager.getisBusinesslogic()) {

                            } else {

                            }


                            findViewById(R.id.rl_pb_ctask).setVisibility(View.GONE);
                            totalcount = 0;
                            str_actionid = "";
                            MenuItem item1 = menu.findItem(R.id.action_delete);
                            MenuItem action_employee = menu.findItem(R.id.action_employee);
                            item1.setVisible(false);
                            action_employee.setVisible(false);
                            showsearch(true);
                            pagecount = 1;
                            isdataavailable = true;
                            usersignin(sessionString, pagecount, false);


                            /*assignarchived(sessionString, finalActionid, finalEmployee_bridge_id, finalMsg, new MyCallback() {
                                @Override
                                public void callbackCall(List<Chit> filteredModelList) {

                                }

                                @Override
                                public void callback_remove() {
                                    findViewById(R.id.rl_pb_ctask).setVisibility(View.GONE);
                                    totalcount = 0;
                                    str_actionid = "";
                                    MenuItem item1 = menu.findItem(R.id.action_delete);
                                    MenuItem action_employee = menu.findItem(R.id.action_employee);
                                    item1.setVisible(false);
                                    action_employee.setVisible(false);
                                    showsearch(true);
                                    pagecount = 1;
                                    isdataavailable = true;
                                    usersignin(sessionString, pagecount, false);

                                }
                            });*/


                        }
                    });


                }


            }


        }
    }


    public void assignarchived(final String usersession, String chitIds, String employee_bridge_id, final String msg, final MyCallback listener) {

        ApiInterface apiService =
                ApiClient.getClient(MyRequsetActivity.this).create(ApiInterface.class);
        JsonObject a1 = new JsonObject();
        try {

            a1.addProperty("chitIds", chitIds);
            a1.addProperty("employee_bridge_id", employee_bridge_id);


        } catch (JsonParseException e) {
            e.printStackTrace();
        }
        //   Toast.makeText(MyTaskActivity.this, employee_bridge_id, Toast.LENGTH_LONG).show();

        //Call call = apiService.removechit(usersession,chitIds+",","10");


        Call call = apiService.assigntask(usersession, a1);


        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                int statusCode = response.code();
                Log.e("test", sessionString + "sessionstring");

                Headers headerList = response.headers();
                Log.d("testhistory", statusCode + " ");

                if (response.isSuccessful()) {
                    try {
                        Log.e("response.body()", response.body().toString());
                        String a = new Gson().toJson(response.body());

                        if (!msg.equalsIgnoreCase("")) {
                            Toast.makeText(MyRequsetActivity.this, msg, Toast.LENGTH_LONG).show();
                        }

                        listener.callback_remove();


                    } catch (Exception e) {
                        Log.e("errortest", e.getMessage());

                    }

                } else {
                    Log.e("test", " trdds1");

                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Toast.makeText(MyRequsetActivity.this, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(MyRequsetActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }


            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // Log error here since request failed
                findViewById(R.id.rl_pb_ctask).setVisibility(View.GONE);
                Log.e("error message", t.toString());
            }
        });
    }

    public void removearchived(final String usersession, String chitIds, final boolean isswipe, String actionCode, final ChitActionCallback callback) {

        ApiInterface apiService =
                ApiClient.getClient(MyRequsetActivity.this).create(ApiInterface.class);
        JsonObject a1 = new JsonObject();
        try {

            a1.addProperty("chitIds", chitIds);
            a1.addProperty("actionCode", actionCode);


        } catch (JsonParseException e) {
            e.printStackTrace();
        }
        // Toast.makeText(AllTaskActivity.this, actionCode, Toast.LENGTH_LONG).show();

        //Call call = apiService.removechit(usersession,chitIds+",","10");
        Call call = apiService.removechit(usersession, a1);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                int statusCode = response.code();
                Log.e("test", sessionString + "sessionstring");

                Headers headerList = response.headers();
                Log.d("testhistory", statusCode + " ");

                if (response.isSuccessful()) {
                    try {

                        callback.callback_chit();

                        Log.e("response.body()", response.body().toString());
                        String a = new Gson().toJson(response.body());

                        JSONObject jo_res = new JSONObject(a);
                        String response_ = jo_res.getString("response");
                        JSONObject jo_txt = new JSONObject(response_);
                        String actionTxt = jo_txt.getString("actionTxt");


                        Toast.makeText(MyRequsetActivity.this, actionTxt, Toast.LENGTH_LONG).show();


                    } catch (Exception e) {
                        Log.e("errortest", e.getMessage());

                    }

                } else {
                    Log.e("test", " trdds1");

                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Toast.makeText(MyRequsetActivity.this, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(MyRequsetActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }


            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // Log error here since request failed
                findViewById(R.id.rl_pb_ctask).setVisibility(View.GONE);
                Log.e("error message", t.toString());
            }
        });
    }


    public void setcte(List<Chit> orderlist, String tab, String tt) {

        int newtask = 0, closetask = 0;


        try {
           /* for (int i = 0; i < orderlist.size(); i++) {
                String t_status = orderlist.get(i).getTransaction_status();


                if (t_status.equalsIgnoreCase("ACTIVE") || t_status.equalsIgnoreCase("ACCEPTED") || t_status.equalsIgnoreCase("HOLD") || t_status.equalsIgnoreCase("INPROGRESS")) {
                    newtask++;
                }

                if (t_status.equalsIgnoreCase("FINISHED")) {
                    closetask++;
                }
            }

*/
            if (tab.equalsIgnoreCase("new_all")) {

                openstatus.setText("Open [ " + tt + " ]");
                Close_.setText("Close");
            } else {
                openstatus.setText("Open");
                Close_.setText("Close [ " + tt + " ]");


            }

        /*    if (tab.equalsIgnoreCase("new")) {

                rb_newtask.setText("Open (" + tt + ")");
            } else if (tab.equalsIgnoreCase("inprogress")) {
                rb_inprogtask.setText("Progress (" + tt + ")");
            } else if (tab.equalsIgnoreCase("close")) {
                rb_close.setText("Close (" + tt + ")");
            }*/


        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    interface ChitActionCallback {
        void callback_chit();
    }

    private ChitActionCallback chitlistener;


    public void refundafterwithdrawn() {
        if (al_refund != null && al_refund.size() > 0) {
            new CanelOrder_Refund(MyRequsetActivity.this, al_refund.get(0), new CanelOrder_Refund.RefundCallback() {
                @Override
                public void showconfirmcancel() {

                }

                @Override
                public void showdescription() {

                }

                @Override
                public void refunded() {
                    al_refund.remove(0);
                    refundafterwithdrawn();
                }
            });
        }

    }

    ArrayList<String> al_refund = new ArrayList<>();

}
