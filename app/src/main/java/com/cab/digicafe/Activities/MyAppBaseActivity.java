package com.cab.digicafe.Activities;

import android.os.Bundle;
import android.support.v4.widget.CircularProgressDrawable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cab.digicafe.Helper.SessionManager;
import com.cab.digicafe.Model.ShareModel;
import com.cab.digicafe.Model.Userprofile;
import com.cab.digicafe.MyCustomClass.JsonObjParse;
import com.cab.digicafe.R;
import com.cab.digicafe.serviceTypeClass.GetServiceTypeFromBusiUserProfile;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

public class MyAppBaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_app_base);
        overridePendingTransition(R.anim.fadein, R.anim.fadeout);
        //overridePendingTransition(R.anim.animation_fade_in_fast,R.anim.animation_fade_out_fast);

       /* TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        tm.listen(new PhoneStateListener(), PhoneStateListener.LISTEN_DATA_CONNECTION_STATE);
*/

    }

    public void showLoader(TextView textView) {
        textView.setText("");
        CircularProgressDrawable circularProgressDrawable = new CircularProgressDrawable(this);
        circularProgressDrawable.setStrokeWidth(5f);
        circularProgressDrawable.setCenterRadius(30f);
        circularProgressDrawable.start();
        textView.setBackground(circularProgressDrawable);
    }

    public void showLoader(ImageView textView) {
        textView.setImageResource(0);
        textView.setBackground(null);
        CircularProgressDrawable circularProgressDrawable = new CircularProgressDrawable(this);
        circularProgressDrawable.setStrokeWidth(5f);
        circularProgressDrawable.setCenterRadius(30f);
        circularProgressDrawable.start();
        textView.setBackground(circularProgressDrawable);
    }

    SessionManager sessionManager;

    public String getValueFromFieldJson(String key) { //  Used For Business Only
        String service_type_of = "";
        if (sessionManager == null) sessionManager = new SessionManager(this);
        String userprofile = sessionManager.getUserprofile();
        Userprofile obj = new Gson().fromJson(userprofile, Userprofile.class);
        if (obj != null)
            service_type_of = JsonObjParse.getValueEmpty(obj.field_json_data, key);
        return service_type_of;
    }


    public String getValueFromFieldJsonForEmployee(String key) {
        String service_type_of = "";
        if (sessionManager == null) sessionManager = new SessionManager(this);
        String userprofile = sessionManager.getUserprofile();
        Userprofile obj = new Gson().fromJson(userprofile, Userprofile.class);
        if (obj != null)
            service_type_of = JsonObjParse.getValueEmpty(obj.field_json_data, key);
        return service_type_of;
    }


    public void showMsg(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }

    public interface MyCallForFieldJson {
        public void call(String field_json);
    }


    public void getBusinessProfileForEmployee(final MyCallForFieldJson myCallForFieldJson) {


        if (!ShareModel.getI().field_json_data_of_bus_for_emp.isEmpty()) {
            myCallForFieldJson.call(ShareModel.getI().field_json_data_of_bus_for_emp);
            return;
        }

        String username = "";

        if (sessionManager == null) sessionManager = new SessionManager(this);

        if (sessionManager.getIsEmployess()) {
            String empUseName = sessionManager.getcurrentu_nm();
            int indexOfannot = empUseName.indexOf("@") + 1;
            username = empUseName.substring(indexOfannot, empUseName.indexOf("."));
        }

        if (username.isEmpty()) {
            return;
        }

        new GetServiceTypeFromBusiUserProfile(this, username, new GetServiceTypeFromBusiUserProfile.Apicallback() {
            @Override
            public void onGetResponse(String a, String response, String sms_emp) {

                String field_json_data = "";
                try {

                    JSONObject Jsonresponse = new JSONObject(response);
                    JSONObject Jsonresponseinfo = Jsonresponse.getJSONObject("response");
                    JSONArray jaData = Jsonresponseinfo.getJSONArray("data");
                    if (jaData.length() > 0) {
                        JSONObject data = jaData.getJSONObject(0);
                        field_json_data = JsonObjParse.getValueEmpty(data.toString(), "field_json_data");
                        ShareModel.getI().field_json_data_of_bus_for_emp = field_json_data;
                        myCallForFieldJson.call(field_json_data);

                    } else {

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
