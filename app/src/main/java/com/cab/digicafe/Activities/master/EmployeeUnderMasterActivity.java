package com.cab.digicafe.Activities.master;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cab.digicafe.Activities.MyAppBaseActivity;
import com.cab.digicafe.Activities.RequestApis.MyRequestCall;
import com.cab.digicafe.Activities.RequestApis.MyRequst;
import com.cab.digicafe.Activities.RequestApis.MyResponse.EmployeeViewModel;
import com.cab.digicafe.Adapter.master.EmployeeUnderMasterListAdapter;
import com.cab.digicafe.Helper.SessionManager;
import com.cab.digicafe.MyCustomClass.JsonObjParse;
import com.cab.digicafe.MyCustomClass.PlayAnim;
import com.cab.digicafe.MyCustomClass.Utils;
import com.cab.digicafe.R;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EmployeeUnderMasterActivity extends MyAppBaseActivity implements SwipyRefreshLayout.OnRefreshListener {

    PlayAnim playAnim = new PlayAnim();

    @OnClick(R.id.iv_back)
    public void iv_back(View v) {
        onBackPressed();
    }

    @BindView(R.id.ivDelEmployee)
    ImageView ivDelEmployee;

    @BindView(R.id.rvEmployee)
    RecyclerView rvEmployee;

    @BindView(R.id.tv_empty)
    TextView tv_empty;

    @BindView(R.id.tvTitle)
    TextView tvTitle;

    @BindView(R.id.rl_pb)
    RelativeLayout rl_pb;

    Context context;
    SessionManager sessionManager;
    MyRequestCall myRequestCall = new MyRequestCall();
    MyRequst myRequst = new MyRequst();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employee_under_master);
        ButterKnife.bind(this);

        ivDelEmployee.setVisibility(View.GONE);
        tv_empty.setVisibility(View.GONE);

        context = this;
        sessionManager = new SessionManager(context);
        myRequst.cookie = sessionManager.getsession();
        myRequst.page = pagecount;

        setEmployee();
        refreshlist();
        callEmployee();

        rlAddEmployee.setVisibility(View.GONE);


    }


    SwipyRefreshLayout mSwipyRefreshLayout;
    int pagecount = 1;
    boolean isdataavailable = true;

    public void refreshlist() {
        mSwipyRefreshLayout = (SwipyRefreshLayout) findViewById(R.id.swipyrefreshlayout);
        mSwipyRefreshLayout.setDirection(SwipyRefreshLayoutDirection.BOTH);
        mSwipyRefreshLayout.setOnRefreshListener(this);
    }

    @Override
    public void onRefresh(SwipyRefreshLayoutDirection direction) {

        if (direction == SwipyRefreshLayoutDirection.BOTTOM) {
            if (isdataavailable) {
                pagecount++;
                myRequst.page = pagecount;
                callEmployee();
                //callCat();
            } else {
                mSwipyRefreshLayout.setRefreshing(false);
                Toast.makeText(this, "No more data available", Toast.LENGTH_LONG).show();
            }

        }

        if (direction == SwipyRefreshLayoutDirection.TOP) {
            pagecount = 1;
            myRequst.page = pagecount;
            alEmplopyee = new ArrayList<>();
            callEmployee();

        }
    }

    EmployeeUnderMasterListAdapter employeeUnderMasterActivity;
    private ArrayList<EmployeeViewModel> alEmplopyee = new ArrayList<>();

    public void setEmployee() {
        employeeUnderMasterActivity = new EmployeeUnderMasterListAdapter(alEmplopyee, context, new EmployeeUnderMasterListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(EmployeeViewModel item) {
                crudStatus = "edit";
                employeeViewModel = item;
                showAddCat();
            }

            @Override
            public void onEditRate(EmployeeViewModel item) {
                crudStatus = "editUnit";
                employeeViewModel = item;
                showAddCat();
            }

            @Override
            public void onCheckChange(int pos, boolean isChecked) {

                //alCategory.get(pos).isSelect = isChecked;

                for (int i = 0; i < alEmplopyee.size(); i++) {
                    ivDelEmployee.setVisibility(View.GONE);
                    if (alEmplopyee.get(i).isSelect) {
                        ivDelEmployee.setVisibility(View.VISIBLE);
                        break;
                    }
                }
            }

            @Override
            public void onDrag() {

            }

        });

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rvEmployee.setLayoutManager(mLayoutManager);
        rvEmployee.setItemAnimator(new DefaultItemAnimator());
        rvEmployee.setAdapter(employeeUnderMasterActivity);
        rvEmployee.setNestedScrollingEnabled(false);

        //ItemTouchHelper.Callback callback = new DragTouchTopBottomHelperForCategory(categoryUnderMasterListAdapter);
        //ItemTouchHelper helper = new ItemTouchHelper(callback);
        //helper.attachToRecyclerView(rvCat);

    }

    public void callEmployee() {
        myRequestCall.getEmployeeUnderMaster(context, myRequst, new MyRequestCall.CallRequest() {
            @Override
            public void onGetResponse(String response) {
                rl_pb.setVisibility(View.GONE);
                mSwipyRefreshLayout.setRefreshing(false);
                try {
                    JSONObject jObjRes = new JSONObject(response);

                    JSONObject jObjdata = jObjRes.getJSONObject("response");
                    JSONObject jObjResponse = jObjdata.getJSONObject("data");
                    String totalRecord = jObjResponse.getString("totalRecord");
                    tvTitle.setText("Employee [" + totalRecord + "]");

                    JSONArray jObjcontacts = jObjResponse.getJSONArray("content");

                    String displayEndRecord = jObjResponse.getString("displayEndRecord");
                    if (totalRecord.equals(displayEndRecord)) {
                        isdataavailable = false;
                    } else {
                        isdataavailable = true;
                    }

                    ArrayList<EmployeeViewModel> tmpAlCategory = new ArrayList<>();


                    for (int i = 0; i < jObjcontacts.length(); i++) {
                        if (jObjcontacts.get(i) instanceof JSONObject) {
                            JSONObject jsnObj = (JSONObject) jObjcontacts.get(i);
                            EmployeeViewModel obj = new Gson().fromJson(jsnObj.toString(), EmployeeViewModel.class);
                            tmpAlCategory.add(obj);
                        }
                    }


                    alEmplopyee.addAll(tmpAlCategory);

                    employeeUnderMasterActivity.setitem(alEmplopyee);


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                tv_empty.setVisibility(View.GONE);
                if (alEmplopyee.size() == 0) {
                    tv_empty.setVisibility(View.VISIBLE);
                }

            }
        });

    }


    @BindView(R.id.rlAddEmployee)
    RelativeLayout rlAddEmployee;

    @BindView(R.id.llAnimAddemp)
    LinearLayout llAnimAddemp;

    @BindView(R.id.llAddEditEmpInfo)
    LinearLayout llAddEditEmpInfo;

    @BindView(R.id.etUsrNm)
    EditText etUsrNm;

    @BindView(R.id.tilUsrNm)
    TextInputLayout tilUsrNm;

    @BindView(R.id.tilContact)
    TextInputLayout tilContact;

    @BindView(R.id.etPwd)
    EditText etPwd;

    @BindView(R.id.etCnfmPwd)
    EditText etCnfmPwd;

    @BindView(R.id.etEmpNm)
    EditText etEmpNm;

    @BindView(R.id.etContactNo)
    EditText etContactNo;

    @BindView(R.id.tvAddEditTitle)
    TextView tvAddEditTitle;

    @BindView(R.id.tvAdd)
    TextView tvAdd;

    @BindView(R.id.tvMsg)
    TextView tvMsg;

    @BindView(R.id.rbActive)
    RadioButton rbActive;

    @BindView(R.id.rbInActive)
    RadioButton rbInActive;

    String crudStatus = ""; // "" - Add

    @OnClick(R.id.tvCancel)
    public void tvCancel(View v) {
        hideAddCat();
    }

    @OnClick(R.id.ivAddEmployee)
    public void ivAddEmployee(View v) {
        employeeViewModel = new EmployeeViewModel();
        showAddCat();
    }

    @OnClick(R.id.ivDelEmployee)
    public void ivDelCat(View v) {
        crudStatus = "delete";
        employeeViewModel = new EmployeeViewModel();
        showAddCat();
        //deleteCategory();
    }

    public void hideAddCat() {

        Utils.hideSoftKeyboard(context, etEmpNm);


        playAnim.slideUpRl(llAnimAddemp);
        crudStatus = "";
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                rlAddEmployee.setVisibility(View.GONE);

            }
        }, 700);


    }

    public void showAddCat() {

        etResUnitOrQuantity.setText("");
        //etResAmount.setText("");
        etUnitNm.setText("");

        playAnim.slideDownRl(llAnimAddemp);
        rlAddEmployee.setVisibility(View.VISIBLE);
        etUsrNm.setCursorVisible(true);

        etPwd.setText("");
        etCnfmPwd.setText("");
        etEmpNm.setText(employeeViewModel.employee_name);
        etUsrNm.setText(employeeViewModel.username);

        String unit = JsonObjParse.getValueEmpty(employeeViewModel.field_json_data, "unit");
        //String amount = JsonObjParse.getValueEmpty(employeeViewModel.field_json_data,"amount");
        String type = JsonObjParse.getValueEmpty(employeeViewModel.field_json_data, "type");
        etResUnitOrQuantity.setText(unit);
        //etResAmount.setText(amount);
        etUnitNm.setText(type);

        tvAddEditTitle.setText("Add Employee");
        tvAdd.setText("Add");

        llAddEditEmpInfo.setVisibility(View.VISIBLE);
        //etSortNm.setVisibility(View.VISIBLE);
        tvMsg.setVisibility(View.GONE);
        rbActive.setChecked(true);
        etUsrNm.setVisibility(View.VISIBLE);
        tilUsrNm.setVisibility(View.VISIBLE);
        tilContact.setVisibility(View.VISIBLE);
        etContactNo.setVisibility(View.VISIBLE);


        if (crudStatus.equalsIgnoreCase("edit")) {

            Utils.showSoftKeyboard(context);
            etUsrNm.setText(employeeViewModel.username);
            etEmpNm.setText(employeeViewModel.employee_name);
            rbInActive.setChecked(true);
            if (employeeViewModel.status.equalsIgnoreCase("active")) rbActive.setChecked(true);
            etUsrNm.setVisibility(View.GONE);
            tilUsrNm.setVisibility(View.GONE);
            tilContact.setVisibility(View.GONE);
            etContactNo.setVisibility(View.GONE);
            tvAddEditTitle.setText("Edit Category");
            tvAdd.setText("Update");

            showHideUnitDetails(false);
        } else if (crudStatus.equalsIgnoreCase("editUnit")) {

            Utils.showSoftKeyboard(context);
            tvAddEditTitle.setText("Edit Unit");
            tvAdd.setText("Update");

            llAddEditEmpInfo.setVisibility(View.GONE);
            showHideUnitDetails(true);
        } else if (crudStatus.equalsIgnoreCase("delete")) {
            llAddEditEmpInfo.setVisibility(View.GONE);
            tvAddEditTitle.setText("Delete Category");
            tvAdd.setText("Delete");
            tvMsg.setText("Are you sure you want to delete?");
            tvMsg.setVisibility(View.VISIBLE);

            showHideUnitDetails(false);
        } else { // Add
            Utils.showSoftKeyboard(context);
            showHideUnitDetails(false);
        }

    }


    EmployeeViewModel employeeViewModel;

    @Override
    public void onBackPressed() {
        if (rl_pb.getVisibility() == View.VISIBLE) {
        } else if (rlAddEmployee.getVisibility() == View.VISIBLE) {
            hideAddCat();
        } else {
            super.onBackPressed();
        }
    }

    @OnClick(R.id.tvAdd)
    public void tvAdd(View v) {


        if (crudStatus.equalsIgnoreCase("delete")) {
            deleteCategory();
        } else if (crudStatus.equalsIgnoreCase("editUnit")) {
            addEmployee();
        } else {

            String empNm = etEmpNm.getText().toString().trim();
            String usrNm = etUsrNm.getText().toString().trim();
            String cnfmPwd = etCnfmPwd.getText().toString().trim();
            String pwd = etPwd.getText().toString().trim();


            if (empNm.isEmpty()) {
                Toast.makeText(context, "Enter employee name", Toast.LENGTH_SHORT).show();
                //etEmpNm.setError("Enter employee name");
            } else if (usrNm.isEmpty()) {
                Toast.makeText(context, "Enter user name", Toast.LENGTH_SHORT).show();
                //etUsrNm.setError("Enter user name");
            } else if (!crudStatus.equalsIgnoreCase("edit") && etContactNo.getText().toString().isEmpty()) {
                Toast.makeText(context, "Enter contact number", Toast.LENGTH_SHORT).show();
                //etUsrNm.setError("Enter user name");
            } else if (pwd.isEmpty()) {
                Toast.makeText(context, "Enter password", Toast.LENGTH_SHORT).show();
                //etPwd.setError("Enter password");
            } else if (cnfmPwd.isEmpty()) {
                Toast.makeText(context, "Enter confirm password", Toast.LENGTH_SHORT).show();
                //etCnfmPwd.setError("Enter confirm password");
            } else if (!cnfmPwd.equals(pwd)) {
                Toast.makeText(context, "Confirm password mismatch", Toast.LENGTH_SHORT).show();
                //etCnfmPwd.setError("Confirm password mismatch");
            } else {
                addEmployee();
            }


        }
    }


    public void addEmployee() {

        try {
            JSONObject sendobj = new JSONObject();


            if (crudStatus.equalsIgnoreCase("edit")) {
                Gson gson = new GsonBuilder().create();
                String json = gson.toJson(employeeViewModel);// obj is your object


                try {


                    JSONArray ja_new = new JSONArray();
                    sendobj.put("newdata", ja_new);

                    JSONArray ja_old = new JSONArray();

                    JSONObject jsonObj = new JSONObject(json);
                    jsonObj.put("employee_name", etEmpNm.getText().toString().trim());
                    //jsonObj.put("username", etUsrNm.getText().toString().trim());
                    jsonObj.put("password", etPwd.getText().toString().trim());
                    jsonObj.put("confirm_password", etCnfmPwd.getText().toString().trim());

                    /*JSONObject joField = new JSONObject();
                    joField.put("unit",etResAmount.getText().toString().trim());
                    joField.put("amount",etResAmount.getText().toString().trim());
                    jsonObj.put("field_json_data", joField.toString());*/


                    if (rbActive.isChecked()) jsonObj.put("status", "Active");
                    else jsonObj.put("status", "InActive");

                    //jsonObj.put("field_json_data",new JSONObject());
                    jsonObj.put("flag", "edit");
                    ja_old.put(jsonObj);
                    sendobj.put("olddata", ja_old);


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            } else if (crudStatus.equalsIgnoreCase("editUnit")) {
                Gson gson = new GsonBuilder().create();
                String json = gson.toJson(employeeViewModel);// obj is your object


                try {


                    JSONArray ja_new = new JSONArray();
                    sendobj.put("newdata", ja_new);

                    JSONArray ja_old = new JSONArray();

                    JSONObject jsonObj = new JSONObject(json);

                    /*JSONObject joField = new JSONObject();
                    joField.put("unit",etResAmount.getText().toString().trim());
                    joField.put("amount",etResAmount.getText().toString().trim());
                    jsonObj.put("field_json_data", joField.toString());*/

                    JSONObject joField = new JSONObject();
                    joField.put("unit", etResUnitOrQuantity.getText().toString().trim());
                    //joField.put("amount",etResAmount.getText().toString().trim());
                    joField.put("type", etUnitNm.getText().toString().trim());
                    jsonObj.put("field_json_data", joField.toString());

                    //jsonObj.put("field_json_data",new JSONObject());
                    jsonObj.put("flag", "edit");
                    ja_old.put(jsonObj);
                    sendobj.put("olddata", ja_old);


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            } else {


                JSONArray jaNewData = new JSONArray();
                JSONObject a1 = new JSONObject();
                try {
                    a1.put("flag", "add");


                    a1.put("employee_name", etEmpNm.getText().toString().trim());
                    a1.put("username", etUsrNm.getText().toString().trim());
                    a1.put("password", etPwd.getText().toString().trim());
                    a1.put("contact_no", etContactNo.getText().toString().trim());


                    JSONObject joField = new JSONObject();
                    joField.put("unit", etResUnitOrQuantity.getText().toString().trim());
                    //joField.put("amount",etResAmount.getText().toString().trim());
                    joField.put("type", etUnitNm.getText().toString().trim());

                    a1.put("field_json_data", joField.toString());

                    a1.put("confirm_password", etCnfmPwd.getText().toString().trim());

                    if (rbActive.isChecked()) a1.put("status", "Active");
                    else a1.put("status", "InActive");

                    jaNewData.put(a1);
                    sendobj.put("newdata", jaNewData);

                    JSONArray a = new JSONArray();
                    sendobj.put("olddata", a);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }


            rl_pb.setVisibility(View.VISIBLE);

            JSONObject object = sendobj;
            JsonParser jsonParser = new JsonParser();
            JsonObject gsonObject = (JsonObject) jsonParser.parse(object.toString());

            myRequestCall.addEmployeeUnderMaster(context, myRequst, gsonObject, new MyRequestCall.CallRequest() {
                @Override
                public void onGetResponse(String response) {

                    if (!response.isEmpty()) {
                        Utils.hideSoftKeyboard(context, etUsrNm);
                        finish();
                        startActivity(getIntent());
                    } else {
                        rl_pb.setVisibility(View.GONE);
                    }

                }
            });

        } catch (JsonParseException e) {
            e.printStackTrace();
        }
    }

    public void deleteCategory() {
        rl_pb.setVisibility(View.VISIBLE);

        String dltId = "";
        for (int i = 0; i < alEmplopyee.size(); i++) {
            if (alEmplopyee.get(i).isSelect)
                dltId = dltId + "" + alEmplopyee.get(i).employee_bridge_id + ",";
        }

        myRequst.deleteIds = dltId;
        myRequestCall.deleteEmployeeUnderMaster(context, myRequst, new MyRequestCall.CallRequest() {
            @Override
            public void onGetResponse(String response) {
                if (!response.isEmpty()) {
                    Utils.hideSoftKeyboard(context, etUsrNm);
                    finish();
                    startActivity(getIntent());
                } else {
                    rl_pb.setVisibility(View.GONE);
                }

            }
        });

    }


    @BindView(R.id.etResUnitOrQuantity)
    EditText etResUnitOrQuantity;

    @BindView(R.id.etUnitNm)
    EditText etUnitNm;

    //@BindView(R.id.etResAmount)
    //EditText etResAmount;

    @BindView(R.id.tilUnitNm)
    TextInputLayout tilUnitNm;

    @BindView(R.id.tilAmount)
    TextInputLayout tilAmount;

    @BindView(R.id.tilUnit)
    TextInputLayout tilUnit;

    @BindView(R.id.llEditUnit)
    LinearLayout llEditUnit;


    public void showHideUnitDetails(boolean isShow) {
        if (isShow) {
            llEditUnit.setVisibility(View.VISIBLE);
        } else {
            llEditUnit.setVisibility(View.GONE);
        }
    }
}
