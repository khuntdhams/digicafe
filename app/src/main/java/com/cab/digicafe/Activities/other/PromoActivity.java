package com.cab.digicafe.Activities.other;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cab.digicafe.Activities.MyAppBaseActivity;
import com.cab.digicafe.Adapter.other.CouponListAdapter;
import com.cab.digicafe.Helper.Inad;
import com.cab.digicafe.Model.Coupon;
import com.cab.digicafe.Model.ShareModel;
import com.cab.digicafe.MyCustomClass.DatePicker;
import com.cab.digicafe.MyCustomClass.JsonObjParse;
import com.cab.digicafe.MyCustomClass.PlayAnim;
import com.cab.digicafe.MyCustomClass.Utils;
import com.cab.digicafe.R;
import com.cab.digicafe.serviceTypeClass.GetServiceTypeFromBusiUserProfile;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PromoActivity extends MyAppBaseActivity {

    @BindView(R.id.etPromoCode)
    EditText etPromoCode;

    @BindView(R.id.etPromoPerc)
    EditText etPromoPerc;

    @BindView(R.id.etMaxPromoValue)
    EditText etMaxPromoValue;

    @BindView(R.id.etMinPurchaseValue)
    EditText etMinPurchaseValue;

    @BindView(R.id.etPromoValue)
    EditText etPromoValue;

    @BindView(R.id.etTc)
    EditText etTc;

    @BindView(R.id.tvStartDate)
    TextView tvStartDate;

    @BindView(R.id.tvEndDate)
    TextView tvEndDate;

    @BindView(R.id.rvPromo)
    RecyclerView rvPromo;

    @OnClick(R.id.iv_back)
    public void iv_back(View v) {
        onBackPressed();
    }

    CouponListAdapter couponListAdapter;
    ArrayList<Coupon> alMetal = new ArrayList<>();

    Date startdt = new Date();
    Date endDate = new Date();

    @OnClick(R.id.tvStartDate)
    public void tvStartDate(View v) {

        DatePicker mDatePicker = new DatePicker(context, new DatePicker.PickDob() {
            @Override
            public void onSelect(Date date) {

                startdt = date;

                SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy");
                String date_ = format.format(startdt);

                tvStartDate.setText(date_);

            }
        }, tvStartDate.getText().toString().trim());
        mDatePicker.setStyle(DialogFragment.STYLE_NORMAL, R.style.CustomDialog);
        mDatePicker.show(getFragmentManager(), "Select starting date");


    }

    @OnClick(R.id.tvEndDate)
    public void tvEndDate(View v) {

        DatePicker mDatePicker = new DatePicker(context, new DatePicker.PickDob() {
            @Override
            public void onSelect(Date date) {
                endDate = date;
                SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy");
                String date_ = format.format(endDate);
                tvEndDate.setText(date_);

            }
        }, tvEndDate.getText().toString().trim());
        mDatePicker.setStyle(DialogFragment.STYLE_NORMAL, R.style.CustomDialog);
        mDatePicker.show(getFragmentManager(), "Select starting date");

    }

    @OnClick(R.id.tvAddPromo)
    public void tvAddPromo(View v) {

        String promo = etPromoCode.getText().toString().trim();
        String maxPromo = etMaxPromoValue.getText().toString().trim();
        String minPurchase = etMinPurchaseValue.getText().toString().trim();
        String modulo = etPromoPerc.getText().toString().trim();
        String value = etPromoValue.getText().toString().trim();
        String sDate = tvStartDate.getText().toString().trim();
        String eDate = tvEndDate.getText().toString().trim();
        String tc = etTc.getText().toString().trim();


        if (promo.isEmpty()) {
            Inad.alerterInfo("Alert", "Please enter promo code", (Activity) context);
        } else if (modulo.isEmpty() && value.isEmpty()) {
            Inad.alerterInfo("Alert", "Please enter promo amount or percentage", (Activity) context);
        } else if (sDate.isEmpty()) {
            Inad.alerterInfo("Alert", "Please enter start date", (Activity) context);
        } else if (eDate.isEmpty()) {
            Inad.alerterInfo("Alert", "Please enter end date", (Activity) context);
        } else if (tc.isEmpty()) {
            Inad.alerterInfo("Alert", "Please enter terms", (Activity) context);
        } else if (startdt.after(endDate)) {
            Inad.alerterInfo("Alert", "Please enter valid date", (Activity) context);
        } else {

            double val = 0.0, moduloVal = 0.0, maxPromoVal = 0.0, minPurchaseVal = 0.0;

            if (!value.isEmpty()) {
                val = Double.parseDouble(value);
            }

            if (!modulo.isEmpty()) {
                moduloVal = Double.parseDouble(modulo);
            }

            if (!maxPromo.isEmpty()) {
                maxPromoVal = Double.parseDouble(maxPromo);
            }

            if (!minPurchase.isEmpty()) {
                minPurchaseVal = Double.parseDouble(minPurchase);
            }

            alMetal.add(new Coupon(promo, val, moduloVal, maxPromoVal, minPurchaseVal, startdt, endDate, tc));
            couponListAdapter.setItem(alMetal);

            clearEt(etPromoCode);
            clearEt(etPromoPerc);
            clearEt(etPromoValue);
            clearEt(etMaxPromoValue);
            clearEt(etMinPurchaseValue);
            clearEt(etTc);
            tvStartDate.setText("");
            tvEndDate.setText("");
            rvPromo.scrollToPosition(alMetal.size() - 1);


            ShareModel.getI().alPromo = alMetal;


        }

    }

    Context context;

    public void clearEt(EditText editText) {
        editText.setText("");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_promo);
        ButterKnife.bind(this);
        context = this;


        new GetServiceTypeFromBusiUserProfile(this, getString(R.string.Aggregator), new GetServiceTypeFromBusiUserProfile.Apicallback() {
            @Override
            public void onGetResponse(String a, String response, String sms_emp) {

                try {

                    JSONObject Jsonresponse = new JSONObject(response);
                    JSONObject Jsonresponseinfo = Jsonresponse.getJSONObject("response");
                    JSONArray jaData = Jsonresponseinfo.getJSONArray("data");
                    if (jaData.length() > 0) {
                        JSONObject data = jaData.getJSONObject(0);
                        //String field_json_data = JsonObjParse.getValueEmpty(data.toString(), "field_json_data");
                        currency = JsonObjParse.getValueEmpty(data.toString(), "symbol_native");
                        setList();

                    } else {


                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


    }

    String currency = "";

    public void setList() {

        alMetal = new ArrayList<>();

        alMetal = ShareModel.getI().alPromo;

        couponListAdapter = new CouponListAdapter(alMetal, this, new CouponListAdapter.OnItemClickListener() {
            @Override
            public void onDelete(int item) {

            }

            @Override
            public void onEdit(int item) {

            }

            @Override
            public void onShowTc(int item) {
                String status_commnet = alMetal.get(item).terms;
                if (!status_commnet.isEmpty())
                    showInfo("" + "Terms & conditions", "" + status_commnet + "");
            }
        }, currency);

        rvPromo.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        //rvMetals.setLayoutManager(new GridLayoutManager(this, 2));
        rvPromo.setAdapter(couponListAdapter);
        rvPromo.setNestedScrollingEnabled(false);

    }


    // show dislogue

    PlayAnim playAnim = new PlayAnim();

    @BindView(R.id.llAnimAddcat)
    LinearLayout llAnimAddcat;

    @OnClick(R.id.rlAddCategory)
    public void rlAddCategory(View v) {
        onBackPressed();
    }

    @BindView(R.id.rlAddCategory)
    RelativeLayout rlAddCategory;

    @BindView(R.id.tilHint)
    TextInputLayout tilHint;

    @BindView(R.id.tilMobile)
    TextInputLayout tilMobile;

    @BindView(R.id.tvAddEditTitle)
    TextView tvAddEditTitle;

    @BindView(R.id.tvAdd)
    TextView tvAdd;

    @BindView(R.id.tvCancel)
    TextView tvCancel;

    @BindView(R.id.tvMsg)
    TextView tvMsg;

    @BindView(R.id.tilComment)
    TextInputLayout tilComment;

    @BindView(R.id.etCommentForSurvey)
    EditText etCommentForSurvey;

    int pos = 0;


    @OnClick(R.id.tvCancel)
    public void tvCancel(View v) {

        if (tilComment.getVisibility() == View.VISIBLE) {
            showInfoDialogue = "";
        }
        hideAddCat();

    }

    @Override
    public void onBackPressed() {
        if (rlAddCategory.getVisibility() == View.VISIBLE) {
            hideAddCat();
        } else {
            super.onBackPressed();
        }
    }


    String showInfoDialogue = "";

    public void showInfo(String title, String msg) {


        //etCommentForSurvey.setError("");
        tilMobile.setVisibility(View.GONE);
        tilHint.setVisibility(View.GONE);
        playAnim.slideDownRl(llAnimAddcat);
        rlAddCategory.setVisibility(View.VISIBLE);

        //tvAdd.setText("Yes");
        tvMsg.setVisibility(View.GONE);

        tvAddEditTitle.setText(title);
        tvAdd.setText("Ok");
        tvCancel.setVisibility(View.GONE);
        tvMsg.setText(msg);
        tvMsg.setVisibility(View.VISIBLE);
        tilComment.setVisibility(View.GONE);

        if (showInfoDialogue.equals("comment")) {
            Utils.showSoftKeyboard(this);
            tilComment.setVisibility(View.VISIBLE);
            tvCancel.setVisibility(View.VISIBLE);
            tvAdd.setVisibility(View.VISIBLE);
            etCommentForSurvey.requestFocus();

        }
    }

    @OnClick(R.id.tvAdd)
    public void tvAdd(View v) {
        hideAddCat();
    }


    public void hideAddCat() {
        playAnim.slideUpRl(llAnimAddcat);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                rlAddCategory.setVisibility(View.GONE);
            }
        }, 700);
    }


}
