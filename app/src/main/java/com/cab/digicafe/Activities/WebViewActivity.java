package com.cab.digicafe.Activities;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.cab.digicafe.R;

public class WebViewActivity extends MyAppBaseActivity {

    String url = "", title = "";
    private WebView webView;
    ImageView ivImg;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        ivImg = (ImageView) findViewById(R.id.ivImg);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        Bundle extras = getIntent().getExtras();
        if (extras == null) {

        } else {
            if (extras.containsKey("url")) {
                url = extras.getString("url");
                title = extras.getString("title");
            }
        }

        webView = (WebView) findViewById(R.id.wv);
        webView.getSettings().setJavaScriptEnabled(true);
        if (url.endsWith("pdf")) {
            webView.loadUrl("https://docs.google.com/viewer?url=" + url);

        } else {

            findViewById(R.id.pb).setVisibility(View.GONE);

            Glide.with(this).load(url).diskCacheStrategy(DiskCacheStrategy.ALL).placeholder(R.drawable.defaultplace).into(ivImg);

            //webView.loadUrl(url);

        }

        getSupportActionBar().setTitle(title);

        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);

                // findViewById(R.id.pb).setVisibility(View.GONE);

            }

            @Override
            public void onPageCommitVisible(WebView view, String url) {
                super.onPageCommitVisible(view, url);
                findViewById(R.id.pb).setVisibility(View.GONE);
            }
        });


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
