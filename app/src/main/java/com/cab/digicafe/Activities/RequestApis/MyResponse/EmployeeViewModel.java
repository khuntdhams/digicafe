package com.cab.digicafe.Activities.RequestApis.MyResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class EmployeeViewModel implements Serializable {

    @SerializedName("username")
    @Expose
    public String username = "";
    @SerializedName("status")
    @Expose
    public String status = "";
    @SerializedName("employee_name")
    @Expose
    public String employee_name = "";
    @SerializedName("employee_bridge_id")
    @Expose
    public String employee_bridge_id;
    @SerializedName("field_json_data")
    @Expose
    public String field_json_data = "";

    public boolean isSelect;


}
