package com.cab.digicafe.Activities.healthTemplate;

import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.TextInputEditText;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cab.digicafe.Activities.ImagePreviewActivity;
import com.cab.digicafe.Activities.RequestApis.MyRequestCall;
import com.cab.digicafe.Adapter.SelectFileBiteAdapter;
import com.cab.digicafe.Adapter.SelectFileModelAdapter;
import com.cab.digicafe.Adapter.SelectFileTrayAdapter;
import com.cab.digicafe.Adapter.TeethImageAdapter;
import com.cab.digicafe.Adapter.healthAdapter.SelectFileFrontalAdapter;
import com.cab.digicafe.Adapter.healthAdapter.SelectFileFrontalSmileAdapter;
import com.cab.digicafe.Adapter.healthAdapter.SelectFileIntraFrontalAdapter;
import com.cab.digicafe.Adapter.healthAdapter.SelectFileIntraLeftLateralAdapter;
import com.cab.digicafe.Adapter.healthAdapter.SelectFileIntraLeftOcclusalAdapter;
import com.cab.digicafe.Adapter.healthAdapter.SelectFileIntraRightLateralAdapter;
import com.cab.digicafe.Adapter.healthAdapter.SelectFileIntraUpperOcclusalAdapter;
import com.cab.digicafe.Adapter.healthAdapter.SelectFileLeftLateralAdapter;
import com.cab.digicafe.Adapter.healthAdapter.SelectFileLeftLateralSmileAdapter;
import com.cab.digicafe.Adapter.healthAdapter.SelectFileRightLateralAdapter;
import com.cab.digicafe.Adapter.healthAdapter.SelectFileRightLateralSmileAdapter;
import com.cab.digicafe.Apputil;
import com.cab.digicafe.CatelogActivity;
import com.cab.digicafe.Database.SettingModel;
import com.cab.digicafe.Database.SqlLiteDbHelper;
import com.cab.digicafe.Helper.Inad;
import com.cab.digicafe.Helper.SessionManager;
import com.cab.digicafe.Helper.SharedPrefUserDetail;
import com.cab.digicafe.Model.Catelog;
import com.cab.digicafe.Model.Dental;
import com.cab.digicafe.Model.ModelFile;
import com.cab.digicafe.Model.Teeth;
import com.cab.digicafe.MyCustomClass.JsonObjParse;
import com.cab.digicafe.R;
import com.cab.digicafe.photopicker.activity.PickImageActivity;
import com.google.api.client.extensions.android.json.AndroidJsonFactory;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.services.vision.v1.Vision;
import com.google.api.services.vision.v1.VisionRequestInitializer;
import com.google.api.services.vision.v1.model.AnnotateImageRequest;
import com.google.api.services.vision.v1.model.BatchAnnotateImagesRequest;
import com.google.api.services.vision.v1.model.BatchAnnotateImagesResponse;
import com.google.api.services.vision.v1.model.EntityAnnotation;
import com.google.api.services.vision.v1.model.Feature;
import com.google.api.services.vision.v1.model.WebDetection;
import com.google.api.services.vision.v1.model.WebEntity;
import com.google.gson.Gson;
import com.kofigyan.stateprogressbar.StateProgressBar;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.media.MediaRecorder.VideoSource.CAMERA;

public class Template_AlignersActivity extends HealthTemplateBaseActivity implements View.OnClickListener {

    @BindView(R.id.iv_back)
    ImageView iv_back;

    @BindView(R.id.rlStepOne)
    LinearLayout rlStepOne;

    @BindView(R.id.rlStepThree)
    LinearLayout rlStepThree;

    @BindView(R.id.rlStepFour)
    LinearLayout rlStepFour;

    @BindView(R.id.rlStepFive)
    RelativeLayout rlStepFive;

    @BindView(R.id.nextStepOne)
    TextView nextStepOne;

    @BindView(R.id.nextStepThree)
    TextView nextStepThree;

    @BindView(R.id.nextStepFour)
    TextView nextStepFour;

    @BindView(R.id.nextStepFive)
    TextView nextStepFive;

    @BindView(R.id.usage_stateprogressbar)
    StateProgressBar usage_stateprogressbar;

    @BindView(R.id.rgSex)
    RadioGroup rgSex;

    @BindView(R.id.rbMale)
    RadioButton rbMale;

    @BindView(R.id.rbFemale)
    RadioButton rbFemale;

    @BindView(R.id.et_name)
    TextInputEditText et_name;

    @BindView(R.id.et_age)
    TextInputEditText et_age;

    ArrayList<Teeth> teethList = new ArrayList<>();

    ArrayList<Dental> teethDetail = new ArrayList<>();

    boolean isdetecting = false;
    SettingModel sm;
    SelectFileTrayAdapter selectFileTrayAdapter;
    SelectFileModelAdapter selectFileModelAdapter;
    SelectFileBiteAdapter selectFileBiteAdapter;

    StringBuilder teeth = new StringBuilder();

    @BindView(R.id.cb_trays)
    CheckBox cb_trays;
    @BindView(R.id.iv_add_tray)
    ImageView iv_add_tray;
    @BindView(R.id.rv_selectTray)
    RecyclerView rv_selectTray;

    @BindView(R.id.cb_model)
    CheckBox cb_model;
    @BindView(R.id.iv_add_model)
    ImageView iv_add_model;
    @BindView(R.id.rv_selectModel)
    RecyclerView rv_selectModel;

    @BindView(R.id.cb_bite)
    CheckBox cb_bite;
    @BindView(R.id.iv_add_bite)
    ImageView iv_add_bite;
    @BindView(R.id.rv_selectBite)
    RecyclerView rv_selectBite;


    ExifInterface exif;
    File f3f;
    int screen_height;
    int screen_width;
    private Uri imageUri;
    ArrayList<ModelFile> al_selet = new ArrayList<>();
    ArrayList<ModelFile> al_selet_tray = new ArrayList<>();
    ArrayList<ModelFile> al_selet_model = new ArrayList<>();
    ArrayList<ModelFile> al_selet_bite = new ArrayList<>();
    ArrayList<ModelFile> al_selet_temp = new ArrayList<>();
    Vision vision;
    int code = 0;

    @BindView(R.id.tvName)
    TextView tvName;
    @BindView(R.id.tvAge)
    TextView tvAge;
    @BindView(R.id.tvSex)
    TextView tvSex;

    @BindView(R.id.rvTraysImage)
    RecyclerView rvTraysImage;
    TeethImageAdapter adapter;
    @BindView(R.id.llTray)
    LinearLayout llTray;
    @BindView(R.id.llModel)
    LinearLayout llModel;
    @BindView(R.id.rvModelsImage)
    RecyclerView rvModelsImage;
    @BindView(R.id.llBite)
    LinearLayout llBite;
    @BindView(R.id.rvBiteImage)
    RecyclerView rvBiteImage;

    String sex = "Male";
    StringBuilder preference;

    @BindView(R.id.rl_upload)
    RelativeLayout rl_upload;
    @BindView(R.id.tv_progress)
    TextView tv_progress;
    String frombridgeidval;
    SessionManager sessionManager;
    boolean isOnlyShow = false;

    Catelog catelog;

    @BindView(R.id.et_Chief)
    TextInputEditText et_Chief;
    @BindView(R.id.et_Treatment)
    TextInputEditText et_Treatment;

    @BindView(R.id.tvChief)
    TextView tvChief;
    @BindView(R.id.tvTreatment)
    TextView tvTreatment;

    @BindView(R.id.cb_Frontal)
    CheckBox cb_Frontal;
    @BindView(R.id.iv_add_frontal)
    ImageView iv_add_frontal;
    @BindView(R.id.rv_selectFrontal)
    RecyclerView rv_selectFrontal;
    ArrayList<ModelFile> al_selet_frontal = new ArrayList<>();
    SelectFileFrontalAdapter selectFileFrontalAdapter;
    @BindView(R.id.llFrontal)
    LinearLayout llFrontal;
    @BindView(R.id.rvFrontalImage)
    RecyclerView rvFrontalImage;

    @BindView(R.id.cb_FrontalSmile)
    CheckBox cb_FrontalSmile;
    @BindView(R.id.iv_add_frontalSmile)
    ImageView iv_add_frontalSmile;
    @BindView(R.id.rv_selectFrontalSmile)
    RecyclerView rv_selectFrontalSmile;
    ArrayList<ModelFile> al_selet_frontalSmile = new ArrayList<>();
    SelectFileFrontalSmileAdapter selectFileFrontalSmileAdapter;
    @BindView(R.id.llFrontalSmile)
    LinearLayout llFrontalSmile;
    @BindView(R.id.rvFrontalSmileImage)
    RecyclerView rvFrontalSmileImage;

    @BindView(R.id.cb_LeftLateral)
    CheckBox cb_LeftLateral;
    @BindView(R.id.iv_add_leftLateral)
    ImageView iv_add_leftLateral;
    @BindView(R.id.rv_selectLeftLateral)
    RecyclerView rv_selectLeftLateral;
    ArrayList<ModelFile> al_selet_leftLateral = new ArrayList<>();
    SelectFileLeftLateralAdapter selectFileLeftLateralAdapter;
    @BindView(R.id.llLeftLateral)
    LinearLayout llLeftLateral;
    @BindView(R.id.rvLeftLateralImage)
    RecyclerView rvLeftLateralImage;

    @BindView(R.id.cb_LeftLateralSmile)
    CheckBox cb_LeftLateralSmile;
    @BindView(R.id.iv_add_leftLateralSmile)
    ImageView iv_add_leftLateralSmile;
    @BindView(R.id.rv_selectLeftLateralSmile)
    RecyclerView rv_selectLeftLateralSmile;
    ArrayList<ModelFile> al_selet_leftLateralSmile = new ArrayList<>();
    SelectFileLeftLateralSmileAdapter selectFileLeftLateralSmileAdapter;
    @BindView(R.id.llLeftLateralSmile)
    LinearLayout llLeftLateralSmile;
    @BindView(R.id.rvLeftLateralSmileImage)
    RecyclerView rvLeftLateralSmileImage;

    @BindView(R.id.cb_RightLateral)
    CheckBox cb_RightLateral;
    @BindView(R.id.iv_add_rightLateral)
    ImageView iv_add_rightLateral;
    @BindView(R.id.rv_selectRightLateral)
    RecyclerView rv_selectRightLateral;
    ArrayList<ModelFile> al_selet_rightLateral = new ArrayList<>();
    SelectFileRightLateralAdapter selectFileRightLateralAdapter;
    @BindView(R.id.llRightLateral)
    LinearLayout llRightLateral;
    @BindView(R.id.rvRightLateralImage)
    RecyclerView rvRightLateralImage;

    @BindView(R.id.cb_RightLateralSmile)
    CheckBox cb_RightLateralSmile;
    @BindView(R.id.iv_add_rightLateralSmile)
    ImageView iv_add_rightLateralSmile;
    @BindView(R.id.rv_selectRightLateralSmile)
    RecyclerView rv_selectRightLateralSmile;
    ArrayList<ModelFile> al_selet_rightLateralSmile = new ArrayList<>();
    SelectFileRightLateralSmileAdapter selectFileRightLateralSmileAdapter;
    @BindView(R.id.llRightLateralSmile)
    LinearLayout llRightLateralSmile;
    @BindView(R.id.rvRightLateralSmileImage)
    RecyclerView rvRightLateralSmileImage;

    @BindView(R.id.cb_IntraFrontal)
    CheckBox cb_IntraFrontal;
    @BindView(R.id.iv_add_intraFrontal)
    ImageView iv_add_intraFrontal;
    @BindView(R.id.rv_selectIntraFrontal)
    RecyclerView rv_selectIntraFrontal;
    ArrayList<ModelFile> al_selet_intraFrontal = new ArrayList<>();
    SelectFileIntraFrontalAdapter selectFileIntraFrontalAdapter;
    @BindView(R.id.llIntraFrontal)
    LinearLayout llIntraFrontal;
    @BindView(R.id.rvIntraFrontalImage)
    RecyclerView rvIntraFrontalImage;

    @BindView(R.id.cb_IntraRightLateral)
    CheckBox cb_IntraRightLateral;
    @BindView(R.id.iv_add_intraRightLateral)
    ImageView iv_add_intraRightLateral;
    @BindView(R.id.rv_selectIntraRightLateral)
    RecyclerView rv_selectIntraRightLateral;
    ArrayList<ModelFile> al_selet_intraRightLateral = new ArrayList<>();
    SelectFileIntraRightLateralAdapter selectFileIntraRightLateralAdapter;
    @BindView(R.id.llIntraRightLateral)
    LinearLayout llIntraRightLateral;
    @BindView(R.id.rvIntraRightLateralImage)
    RecyclerView rvIntraRightLateralImage;

    @BindView(R.id.cb_IntraLeftLateral)
    CheckBox cb_IntraLeftLateral;
    @BindView(R.id.iv_add_intraLeftLateral)
    ImageView iv_add_intraLeftLateral;
    @BindView(R.id.rv_selectIntraLeftLateral)
    RecyclerView rv_selectIntraLeftLateral;
    ArrayList<ModelFile> al_selet_intraLeftLateral = new ArrayList<>();
    SelectFileIntraLeftLateralAdapter selectFileIntraLeftLateralAdapter;
    @BindView(R.id.llIntraLeftLateral)
    LinearLayout llIntraLeftLateral;
    @BindView(R.id.rvIntraLeftLateralImage)
    RecyclerView rvIntraLeftLateralImage;

    @BindView(R.id.cb_IntraUpperOcclusal)
    CheckBox cb_IntraUpperOcclusal;
    @BindView(R.id.iv_add_intraUpperOcclusal)
    ImageView iv_add_intraUpperOcclusal;
    @BindView(R.id.rv_selectIntraUpperOcclusal)
    RecyclerView rv_selectIntraUpperOcclusal;
    ArrayList<ModelFile> al_selet_intraUpperOcclusal = new ArrayList<>();
    SelectFileIntraUpperOcclusalAdapter selectFileIntraUpperOcclusalAdapter;
    @BindView(R.id.llIntraUpperOcclusal)
    LinearLayout llIntraUpperOcclusal;
    @BindView(R.id.rvIntraUpperOcclusalImage)
    RecyclerView rvIntraUpperOcclusalImage;

    @BindView(R.id.cb_IntraLeftOcclusal)
    CheckBox cb_IntraLeftOcclusal;
    @BindView(R.id.iv_add_intraLeftOcclusal)
    ImageView iv_add_intraLeftOcclusal;
    @BindView(R.id.rv_selectIntraLeftOcclusal)
    RecyclerView rv_selectIntraLeftOcclusal;
    ArrayList<ModelFile> al_selet_intraLeftOcclusal = new ArrayList<>();
    SelectFileIntraLeftOcclusalAdapter selectFileIntraLeftOcclusalAdapter;
    @BindView(R.id.llIntraLeftOcclusal)
    LinearLayout llIntraLeftOcclusal;
    @BindView(R.id.rvIntraLeftOcclusalImage)
    RecyclerView rvIntraLeftOcclusalImage;

    @BindView(R.id.serialno)
    TextView serialno;
    @BindView(R.id.productqty)
    TextView productqty;
    @BindView(R.id.productprice)
    TextView productprice;
    String currency = "";
    @BindView(R.id.llDetail)
    LinearLayout llDetail;

    @BindView(R.id.tvProdName)
    TextView tvProdName;
    @BindView(R.id.llProduct)
    LinearLayout llProduct;
    boolean isFromDatabse = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_template__aligners);
        ButterKnife.bind(this);

        isOnlyShow = getIntent().getBooleanExtra("isOnlyShow", false);
        isFromDatabse = getIntent().getBooleanExtra("Database", false);
        if (isOnlyShow) {
            nextStepFive.setVisibility(View.GONE);
        } else {

            if (isFromDatabse) {
                nextStepFive.setVisibility(View.GONE);
            }
            if (getIntent().getExtras().containsKey("Catelog")) {
                String c = getIntent().getExtras().getString("Catelog", "");
                catelog = new Gson().fromJson(c, Catelog.class);
                Log.d("catelog", c.toString());

                if (catelog.getCartcount() > 0) {
                    nextStepFive.setText("UPDATE");

                    SqlLiteDbHelper dbHelper = new SqlLiteDbHelper(Template_AlignersActivity.this);
                    String internal_json = Apputil.getCatelog(dbHelper, catelog).getInternal_json_data();
                    String d = JsonObjParse.getValueEmpty(internal_json, "dental");

                    dental = new Gson().fromJson(d, Dental.class);
                    if (dental == null) dental = new Dental();

                    et_name.setText(dental.getName());
                    et_age.setText(dental.getAge());
                    sex = dental.getSex();
                    teeth = dental.getTeeth();
                    teethList = dental.getTeethList();

                    al_selet = dental.getAl_selet();
                    al_selet_bite = dental.getAl_selet_bite();
                    al_selet_model = dental.getAl_selet_model();
                    al_selet_temp = dental.getAl_selet_temp();
                    al_selet_tray = dental.getAl_selet_tray();

                    al_selet_frontal = dental.getAl_selet_frontal();
                    al_selet_frontalSmile = dental.getAl_selet_frontalSmile();
                    al_selet_leftLateral = dental.getAl_selet_leftLateral();
                    al_selet_leftLateralSmile = dental.getAl_selet_leftLateralSmile();
                    al_selet_rightLateral = dental.getAl_selet_rightLateral();
                    al_selet_rightLateralSmile = dental.getAl_selet_rightLateralSmile();
                    al_selet_intraFrontal = dental.getAl_selet_intraFrontal();
                    al_selet_intraLeftLateral = dental.getAl_selet_intraLeftLateral();
                    al_selet_intraRightLateral = dental.getAl_selet_intraRightLateral();
                    al_selet_intraUpperOcclusal = dental.getAl_selet_intraUpperOcclusal();
                    al_selet_intraLeftOcclusal = dental.getAl_selet_intraLeftOcclusal();

                    sex = dental.getSex();

                    cb_trays.setChecked(dental.isTray);
                    cb_model.setChecked(dental.isModel);
                    cb_bite.setChecked(dental.isBite);
                    cb_Frontal.setChecked(dental.isFrontal);
                    cb_FrontalSmile.setChecked(dental.isFrontalSmile);
                    cb_LeftLateral.setChecked(dental.isLeftLateral);
                    cb_LeftLateralSmile.setChecked(dental.isLeftLateralSmile);
                    cb_RightLateral.setChecked(dental.isRightLateral);
                    cb_RightLateralSmile.setChecked(dental.isRightLateralSmile);
                    cb_IntraFrontal.setChecked(dental.isIntraFrontal);
                    cb_IntraRightLateral.setChecked(dental.isIntraRightLateral);
                    cb_IntraLeftLateral.setChecked(dental.isIntraLeftLateral);
                    cb_IntraUpperOcclusal.setChecked(dental.isIntraUpperOcclusal);
                    cb_IntraLeftOcclusal.setChecked(dental.isIntraLeftOcclusal);

                    et_Chief.setText(dental.getChief_complaint());
                    et_Treatment.setText(dental.getTreatment());

                    Detail();

                } else {

                }
            }
        }

        sessionManager = new SessionManager(this);
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                frombridgeidval = null;
            } else {
                frombridgeidval = extras.getString("frombridgeidval");
            }
        } else {
            frombridgeidval = (String) savedInstanceState.getSerializable("frombridgeidval");
        }

        rbMale.setSelected(true);

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (rl_upload.getVisibility() == View.VISIBLE) {

                } else {
                    if (rlStepOne.getVisibility() == View.VISIBLE) {
                        finish();
                    } else if (rlStepThree.getVisibility() == View.VISIBLE) {
                        rlStepOne.setVisibility(View.VISIBLE);
                        rlStepThree.setVisibility(View.GONE);
                        rlStepFour.setVisibility(View.GONE);
                        rlStepFive.setVisibility(View.GONE);
                        usage_stateprogressbar.setCurrentStateNumber(StateProgressBar.StateNumber.ONE);
                    } else if (rlStepFour.getVisibility() == View.VISIBLE) {
                        rlStepOne.setVisibility(View.GONE);
                        rlStepThree.setVisibility(View.VISIBLE);
                        rlStepFour.setVisibility(View.GONE);
                        rlStepFive.setVisibility(View.GONE);
                        usage_stateprogressbar.setCurrentStateNumber(StateProgressBar.StateNumber.TWO);
                    } else if (rlStepFive.getVisibility() == View.VISIBLE) {
                        rlStepOne.setVisibility(View.GONE);
                        rlStepThree.setVisibility(View.GONE);
                        rlStepFour.setVisibility(View.VISIBLE);
                        rlStepFive.setVisibility(View.GONE);
                        usage_stateprogressbar.setCurrentStateNumber(StateProgressBar.StateNumber.THREE);
                    } else {
                        finish();
                    }
                }
            }
        });

        nextStepOne.setOnClickListener(this);
        nextStepThree.setOnClickListener(this);
        nextStepFour.setOnClickListener(this);
        nextStepFive.setOnClickListener(this);

        rgSex.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (R.id.rbMale == checkedId) {
                    sex = "Male";
                } else {
                    sex = "Female";
                }
            }
        });

        iv_add_tray.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isdetecting) {
                    showPictureDialog();
                    code = 1;
                    //showGalleryCamera.showMultiPictureDialog();

                } else {
                    Toast.makeText(Template_AlignersActivity.this, "Please wait while detecting images...", Toast.LENGTH_LONG).show();
                }
            }
        });
        iv_add_model.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isdetecting) {
                    showPictureDialog();
                    code = 2;
                    //showGalleryCamera.showMultiPictureDialog();

                } else {
                    Toast.makeText(Template_AlignersActivity.this, "Please wait while detecting images...", Toast.LENGTH_LONG).show();
                }
            }
        });

        iv_add_bite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isdetecting) {
                    showPictureDialog();
                    code = 3;
                    //showGalleryCamera.showMultiPictureDialog();

                } else {
                    Toast.makeText(Template_AlignersActivity.this, "Please wait while detecting images...", Toast.LENGTH_LONG).show();
                }
            }
        });
        iv_add_frontal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isdetecting) {
                    showPictureDialog();
                    code = 5;
                    //showGalleryCamera.showMultiPictureDialog();

                } else {
                    Toast.makeText(Template_AlignersActivity.this, "Please wait while detecting images...", Toast.LENGTH_LONG).show();
                }
            }
        });
        iv_add_frontalSmile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isdetecting) {
                    showPictureDialog();
                    code = 6;
                    //showGalleryCamera.showMultiPictureDialog();

                } else {
                    Toast.makeText(Template_AlignersActivity.this, "Please wait while detecting images...", Toast.LENGTH_LONG).show();
                }
            }
        });
        iv_add_leftLateral.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isdetecting) {
                    showPictureDialog();
                    code = 7;
                    //showGalleryCamera.showMultiPictureDialog();

                } else {
                    Toast.makeText(Template_AlignersActivity.this, "Please wait while detecting images...", Toast.LENGTH_LONG).show();
                }
            }
        });
        iv_add_leftLateralSmile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isdetecting) {
                    showPictureDialog();
                    code = 8;
                    //showGalleryCamera.showMultiPictureDialog();

                } else {
                    Toast.makeText(Template_AlignersActivity.this, "Please wait while detecting images...", Toast.LENGTH_LONG).show();
                }
            }
        });

        iv_add_rightLateral.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isdetecting) {
                    showPictureDialog();
                    code = 9;
                    //showGalleryCamera.showMultiPictureDialog();

                } else {
                    Toast.makeText(Template_AlignersActivity.this, "Please wait while detecting images...", Toast.LENGTH_LONG).show();
                }
            }
        });
        iv_add_rightLateralSmile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isdetecting) {
                    showPictureDialog();
                    code = 10;
                    //showGalleryCamera.showMultiPictureDialog();

                } else {
                    Toast.makeText(Template_AlignersActivity.this, "Please wait while detecting images...", Toast.LENGTH_LONG).show();
                }
            }
        });
        iv_add_intraFrontal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isdetecting) {
                    showPictureDialog();
                    code = 11;
                    //showGalleryCamera.showMultiPictureDialog();

                } else {
                    Toast.makeText(Template_AlignersActivity.this, "Please wait while detecting images...", Toast.LENGTH_LONG).show();
                }
            }
        });
        iv_add_intraRightLateral.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isdetecting) {
                    showPictureDialog();
                    code = 12;
                    //showGalleryCamera.showMultiPictureDialog();

                } else {
                    Toast.makeText(Template_AlignersActivity.this, "Please wait while detecting images...", Toast.LENGTH_LONG).show();
                }
            }
        });
        iv_add_intraLeftLateral.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isdetecting) {
                    showPictureDialog();
                    code = 13;
                    //showGalleryCamera.showMultiPictureDialog();

                } else {
                    Toast.makeText(Template_AlignersActivity.this, "Please wait while detecting images...", Toast.LENGTH_LONG).show();
                }
            }
        });
        iv_add_intraUpperOcclusal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isdetecting) {
                    showPictureDialog();
                    code = 14;
                    //showGalleryCamera.showMultiPictureDialog();

                } else {
                    Toast.makeText(Template_AlignersActivity.this, "Please wait while detecting images...", Toast.LENGTH_LONG).show();
                }
            }
        });
        iv_add_intraLeftOcclusal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isdetecting) {
                    showPictureDialog();
                    code = 15;
                    //showGalleryCamera.showMultiPictureDialog();

                } else {
                    Toast.makeText(Template_AlignersActivity.this, "Please wait while detecting images...", Toast.LENGTH_LONG).show();
                }
            }
        });

        LinearLayoutManager mLayoutManager2 = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rv_selectTray.setLayoutManager(mLayoutManager2);
        selectFileTrayAdapter = new SelectFileTrayAdapter(al_selet_tray, this, new SelectFileTrayAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(String item) {
            }

            @Override
            public void onDelete(int pos) {
                al_selet_tray.remove(pos);
                selectFileTrayAdapter.setItem(al_selet_tray);
            }

            @Override
            public void onClickString(String pos) {
            }

            @Override
            public void onImgClick(List<ModelFile> data, int pos) {

                if (isdetecting) {
                    Toast.makeText(Template_AlignersActivity.this, "Please wait while detecting images...", Toast.LENGTH_SHORT).show();
                } else {
                    ArrayList<ModelFile> al_img = new ArrayList<>();
                    al_img.addAll(data);
                    String imgarray = new Gson().toJson(al_img);
                    Intent intent = new Intent(Template_AlignersActivity.this, ImagePreviewActivity.class);
                    intent.putExtra("imgarray", imgarray);
                    intent.putExtra("pos", pos);
                    intent.putExtra("isdrag", false);
                    startActivityForResult(intent, 11);
                }

            }
        }, false, true, false);
        rv_selectTray.setAdapter(selectFileTrayAdapter);

        LinearLayoutManager mLayoutManager3 = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rv_selectModel.setLayoutManager(mLayoutManager3);
        selectFileModelAdapter = new SelectFileModelAdapter(al_selet_model, this, new SelectFileModelAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(String item) {
            }

            @Override
            public void onDelete(int pos) {
                al_selet_model.remove(pos);
                selectFileModelAdapter.setItem(al_selet_model);
            }

            @Override
            public void onClickString(String pos) {
            }

            @Override
            public void onImgClick(List<ModelFile> data, int pos) {

                if (isdetecting) {
                    Toast.makeText(Template_AlignersActivity.this, "Please wait while detecting images...", Toast.LENGTH_SHORT).show();
                } else {
                    ArrayList<ModelFile> al_img = new ArrayList<>();
                    al_img.addAll(data);
                    String imgarray = new Gson().toJson(al_img);
                    Intent intent = new Intent(Template_AlignersActivity.this, ImagePreviewActivity.class);
                    intent.putExtra("imgarray", imgarray);
                    intent.putExtra("pos", pos);
                    intent.putExtra("isdrag", false);
                    startActivityForResult(intent, 11);
                }

            }
        }, false, true, false);
        rv_selectModel.setAdapter(selectFileModelAdapter);

        LinearLayoutManager mLayoutManager4 = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rv_selectBite.setLayoutManager(mLayoutManager4);
        selectFileBiteAdapter = new SelectFileBiteAdapter(al_selet_bite, this, new SelectFileBiteAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(String item) {
            }

            @Override
            public void onDelete(int pos) {
                al_selet_bite.remove(pos);
                selectFileBiteAdapter.setItem(al_selet_bite);
            }

            @Override
            public void onClickString(String pos) {
            }

            @Override
            public void onImgClick(List<ModelFile> data, int pos) {

                if (isdetecting) {
                    Toast.makeText(Template_AlignersActivity.this, "Please wait while detecting images...", Toast.LENGTH_SHORT).show();
                } else {
                    ArrayList<ModelFile> al_img = new ArrayList<>();
                    al_img.addAll(data);
                    String imgarray = new Gson().toJson(al_img);
                    Intent intent = new Intent(Template_AlignersActivity.this, ImagePreviewActivity.class);
                    intent.putExtra("imgarray", imgarray);
                    intent.putExtra("pos", pos);
                    intent.putExtra("isdrag", false);
                    startActivityForResult(intent, 11);
                }

            }
        }, false, true, false);
        rv_selectBite.setAdapter(selectFileBiteAdapter);

        LinearLayoutManager mLayoutManager5 = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rv_selectFrontal.setLayoutManager(mLayoutManager5);
        selectFileFrontalAdapter = new SelectFileFrontalAdapter(al_selet_frontal, this, new SelectFileFrontalAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(String item) {
            }

            @Override
            public void onDelete(int pos) {
                al_selet_frontal.remove(pos);
                selectFileFrontalAdapter.setItem(al_selet_frontal);
            }

            @Override
            public void onClickString(String pos) {
            }

            @Override
            public void onImgClick(List<ModelFile> data, int pos) {

                if (isdetecting) {
                    Toast.makeText(Template_AlignersActivity.this, "Please wait while detecting images...", Toast.LENGTH_SHORT).show();
                } else {
                    ArrayList<ModelFile> al_img = new ArrayList<>();
                    al_img.addAll(data);
                    String imgarray = new Gson().toJson(al_img);
                    Intent intent = new Intent(Template_AlignersActivity.this, ImagePreviewActivity.class);
                    intent.putExtra("imgarray", imgarray);
                    intent.putExtra("pos", pos);
                    intent.putExtra("isdrag", false);
                    startActivityForResult(intent, 11);
                }

            }
        }, false, true, false);
        rv_selectFrontal.setAdapter(selectFileFrontalAdapter);

        LinearLayoutManager mLayoutManager6 = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rv_selectFrontalSmile.setLayoutManager(mLayoutManager6);
        selectFileFrontalSmileAdapter = new SelectFileFrontalSmileAdapter(al_selet_frontalSmile, this, new SelectFileFrontalSmileAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(String item) {
            }

            @Override
            public void onDelete(int pos) {
                al_selet_frontalSmile.remove(pos);
                selectFileFrontalSmileAdapter.setItem(al_selet_frontalSmile);
            }

            @Override
            public void onClickString(String pos) {
            }

            @Override
            public void onImgClick(List<ModelFile> data, int pos) {

                if (isdetecting) {
                    Toast.makeText(Template_AlignersActivity.this, "Please wait while detecting images...", Toast.LENGTH_SHORT).show();
                } else {
                    ArrayList<ModelFile> al_img = new ArrayList<>();
                    al_img.addAll(data);
                    String imgarray = new Gson().toJson(al_img);
                    Intent intent = new Intent(Template_AlignersActivity.this, ImagePreviewActivity.class);
                    intent.putExtra("imgarray", imgarray);
                    intent.putExtra("pos", pos);
                    intent.putExtra("isdrag", false);
                    startActivityForResult(intent, 11);
                }

            }
        }, false, true, false);
        rv_selectFrontalSmile.setAdapter(selectFileFrontalSmileAdapter);

        LinearLayoutManager mLayoutManager7 = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rv_selectLeftLateral.setLayoutManager(mLayoutManager7);
        selectFileLeftLateralAdapter = new SelectFileLeftLateralAdapter(al_selet_leftLateral, this, new SelectFileLeftLateralAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(String item) {
            }

            @Override
            public void onDelete(int pos) {
                al_selet_leftLateral.remove(pos);
                selectFileLeftLateralAdapter.setItem(al_selet_leftLateral);
            }

            @Override
            public void onClickString(String pos) {
            }

            @Override
            public void onImgClick(List<ModelFile> data, int pos) {

                if (isdetecting) {
                    Toast.makeText(Template_AlignersActivity.this, "Please wait while detecting images...", Toast.LENGTH_SHORT).show();
                } else {
                    ArrayList<ModelFile> al_img = new ArrayList<>();
                    al_img.addAll(data);
                    String imgarray = new Gson().toJson(al_img);
                    Intent intent = new Intent(Template_AlignersActivity.this, ImagePreviewActivity.class);
                    intent.putExtra("imgarray", imgarray);
                    intent.putExtra("pos", pos);
                    intent.putExtra("isdrag", false);
                    startActivityForResult(intent, 11);
                }

            }
        }, false, true, false);
        rv_selectLeftLateral.setAdapter(selectFileLeftLateralAdapter);

        LinearLayoutManager mLayoutManager8 = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rv_selectLeftLateralSmile.setLayoutManager(mLayoutManager8);
        selectFileLeftLateralSmileAdapter = new SelectFileLeftLateralSmileAdapter(al_selet_leftLateralSmile, this, new SelectFileLeftLateralSmileAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(String item) {
            }

            @Override
            public void onDelete(int pos) {
                al_selet_leftLateralSmile.remove(pos);
                selectFileLeftLateralSmileAdapter.setItem(al_selet_leftLateralSmile);
            }

            @Override
            public void onClickString(String pos) {
            }

            @Override
            public void onImgClick(List<ModelFile> data, int pos) {

                if (isdetecting) {
                    Toast.makeText(Template_AlignersActivity.this, "Please wait while detecting images...", Toast.LENGTH_SHORT).show();
                } else {
                    ArrayList<ModelFile> al_img = new ArrayList<>();
                    al_img.addAll(data);
                    String imgarray = new Gson().toJson(al_img);
                    Intent intent = new Intent(Template_AlignersActivity.this, ImagePreviewActivity.class);
                    intent.putExtra("imgarray", imgarray);
                    intent.putExtra("pos", pos);
                    intent.putExtra("isdrag", false);
                    startActivityForResult(intent, 11);
                }

            }
        }, false, true, false);
        rv_selectLeftLateralSmile.setAdapter(selectFileLeftLateralSmileAdapter);

        LinearLayoutManager mLayoutManager9 = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rv_selectRightLateral.setLayoutManager(mLayoutManager9);
        selectFileRightLateralAdapter = new SelectFileRightLateralAdapter(al_selet_rightLateral, this, new SelectFileRightLateralAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(String item) {
            }

            @Override
            public void onDelete(int pos) {
                al_selet_rightLateral.remove(pos);
                selectFileRightLateralAdapter.setItem(al_selet_rightLateral);
            }

            @Override
            public void onClickString(String pos) {
            }

            @Override
            public void onImgClick(List<ModelFile> data, int pos) {

                if (isdetecting) {
                    Toast.makeText(Template_AlignersActivity.this, "Please wait while detecting images...", Toast.LENGTH_SHORT).show();
                } else {
                    ArrayList<ModelFile> al_img = new ArrayList<>();
                    al_img.addAll(data);
                    String imgarray = new Gson().toJson(al_img);
                    Intent intent = new Intent(Template_AlignersActivity.this, ImagePreviewActivity.class);
                    intent.putExtra("imgarray", imgarray);
                    intent.putExtra("pos", pos);
                    intent.putExtra("isdrag", false);
                    startActivityForResult(intent, 11);
                }

            }
        }, false, true, false);
        rv_selectRightLateral.setAdapter(selectFileRightLateralAdapter);

        LinearLayoutManager mLayoutManager10 = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rv_selectRightLateralSmile.setLayoutManager(mLayoutManager10);
        selectFileRightLateralSmileAdapter = new SelectFileRightLateralSmileAdapter(al_selet_rightLateralSmile, this, new SelectFileRightLateralSmileAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(String item) {
            }

            @Override
            public void onDelete(int pos) {
                al_selet_rightLateralSmile.remove(pos);
                selectFileRightLateralSmileAdapter.setItem(al_selet_rightLateralSmile);
            }

            @Override
            public void onClickString(String pos) {
            }

            @Override
            public void onImgClick(List<ModelFile> data, int pos) {

                if (isdetecting) {
                    Toast.makeText(Template_AlignersActivity.this, "Please wait while detecting images...", Toast.LENGTH_SHORT).show();
                } else {
                    ArrayList<ModelFile> al_img = new ArrayList<>();
                    al_img.addAll(data);
                    String imgarray = new Gson().toJson(al_img);
                    Intent intent = new Intent(Template_AlignersActivity.this, ImagePreviewActivity.class);
                    intent.putExtra("imgarray", imgarray);
                    intent.putExtra("pos", pos);
                    intent.putExtra("isdrag", false);
                    startActivityForResult(intent, 11);
                }

            }
        }, false, true, false);
        rv_selectRightLateralSmile.setAdapter(selectFileRightLateralSmileAdapter);

        LinearLayoutManager mLayoutManager11 = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rv_selectIntraFrontal.setLayoutManager(mLayoutManager11);
        selectFileIntraFrontalAdapter = new SelectFileIntraFrontalAdapter(al_selet_intraFrontal, this, new SelectFileIntraFrontalAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(String item) {
            }

            @Override
            public void onDelete(int pos) {
                al_selet_intraFrontal.remove(pos);
                selectFileIntraFrontalAdapter.setItem(al_selet_intraFrontal);
            }

            @Override
            public void onClickString(String pos) {
            }

            @Override
            public void onImgClick(List<ModelFile> data, int pos) {

                if (isdetecting) {
                    Toast.makeText(Template_AlignersActivity.this, "Please wait while detecting images...", Toast.LENGTH_SHORT).show();
                } else {
                    ArrayList<ModelFile> al_img = new ArrayList<>();
                    al_img.addAll(data);
                    String imgarray = new Gson().toJson(al_img);
                    Intent intent = new Intent(Template_AlignersActivity.this, ImagePreviewActivity.class);
                    intent.putExtra("imgarray", imgarray);
                    intent.putExtra("pos", pos);
                    intent.putExtra("isdrag", false);
                    startActivityForResult(intent, 11);
                }

            }
        }, false, true, false);
        rv_selectIntraFrontal.setAdapter(selectFileIntraFrontalAdapter);

        LinearLayoutManager mLayoutManager12 = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rv_selectIntraRightLateral.setLayoutManager(mLayoutManager12);
        selectFileIntraRightLateralAdapter = new SelectFileIntraRightLateralAdapter(al_selet_intraRightLateral, this, new SelectFileIntraRightLateralAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(String item) {
            }

            @Override
            public void onDelete(int pos) {
                al_selet_intraRightLateral.remove(pos);
                selectFileIntraRightLateralAdapter.setItem(al_selet_intraRightLateral);
            }

            @Override
            public void onClickString(String pos) {
            }

            @Override
            public void onImgClick(List<ModelFile> data, int pos) {

                if (isdetecting) {
                    Toast.makeText(Template_AlignersActivity.this, "Please wait while detecting images...", Toast.LENGTH_SHORT).show();
                } else {
                    ArrayList<ModelFile> al_img = new ArrayList<>();
                    al_img.addAll(data);
                    String imgarray = new Gson().toJson(al_img);
                    Intent intent = new Intent(Template_AlignersActivity.this, ImagePreviewActivity.class);
                    intent.putExtra("imgarray", imgarray);
                    intent.putExtra("pos", pos);
                    intent.putExtra("isdrag", false);
                    startActivityForResult(intent, 11);
                }

            }
        }, false, true, false);
        rv_selectIntraRightLateral.setAdapter(selectFileIntraRightLateralAdapter);

        LinearLayoutManager mLayoutManager13 = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rv_selectIntraLeftLateral.setLayoutManager(mLayoutManager13);
        selectFileIntraLeftLateralAdapter = new SelectFileIntraLeftLateralAdapter(al_selet_intraLeftLateral, this, new SelectFileIntraLeftLateralAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(String item) {
            }

            @Override
            public void onDelete(int pos) {
                al_selet_intraLeftLateral.remove(pos);
                selectFileIntraLeftLateralAdapter.setItem(al_selet_intraLeftLateral);
            }

            @Override
            public void onClickString(String pos) {
            }

            @Override
            public void onImgClick(List<ModelFile> data, int pos) {

                if (isdetecting) {
                    Toast.makeText(Template_AlignersActivity.this, "Please wait while detecting images...", Toast.LENGTH_SHORT).show();
                } else {
                    ArrayList<ModelFile> al_img = new ArrayList<>();
                    al_img.addAll(data);
                    String imgarray = new Gson().toJson(al_img);
                    Intent intent = new Intent(Template_AlignersActivity.this, ImagePreviewActivity.class);
                    intent.putExtra("imgarray", imgarray);
                    intent.putExtra("pos", pos);
                    intent.putExtra("isdrag", false);
                    startActivityForResult(intent, 11);
                }

            }
        }, false, true, false);
        rv_selectIntraLeftLateral.setAdapter(selectFileIntraLeftLateralAdapter);

        LinearLayoutManager mLayoutManager14 = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rv_selectIntraUpperOcclusal.setLayoutManager(mLayoutManager14);
        selectFileIntraUpperOcclusalAdapter = new SelectFileIntraUpperOcclusalAdapter(al_selet_intraUpperOcclusal, this, new SelectFileIntraUpperOcclusalAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(String item) {
            }

            @Override
            public void onDelete(int pos) {
                al_selet_intraUpperOcclusal.remove(pos);
                selectFileIntraUpperOcclusalAdapter.setItem(al_selet_intraUpperOcclusal);
            }

            @Override
            public void onClickString(String pos) {
            }

            @Override
            public void onImgClick(List<ModelFile> data, int pos) {

                if (isdetecting) {
                    Toast.makeText(Template_AlignersActivity.this, "Please wait while detecting images...", Toast.LENGTH_SHORT).show();
                } else {
                    ArrayList<ModelFile> al_img = new ArrayList<>();
                    al_img.addAll(data);
                    String imgarray = new Gson().toJson(al_img);
                    Intent intent = new Intent(Template_AlignersActivity.this, ImagePreviewActivity.class);
                    intent.putExtra("imgarray", imgarray);
                    intent.putExtra("pos", pos);
                    intent.putExtra("isdrag", false);
                    startActivityForResult(intent, 11);
                }

            }
        }, false, true, false);
        rv_selectIntraUpperOcclusal.setAdapter(selectFileIntraUpperOcclusalAdapter);

        LinearLayoutManager mLayoutManager15 = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rv_selectIntraLeftOcclusal.setLayoutManager(mLayoutManager15);
        selectFileIntraLeftOcclusalAdapter = new SelectFileIntraLeftOcclusalAdapter(al_selet_intraLeftOcclusal, this, new SelectFileIntraLeftOcclusalAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(String item) {
            }

            @Override
            public void onDelete(int pos) {
                al_selet_intraLeftOcclusal.remove(pos);
                selectFileIntraLeftOcclusalAdapter.setItem(al_selet_intraLeftOcclusal);
            }

            @Override
            public void onClickString(String pos) {
            }

            @Override
            public void onImgClick(List<ModelFile> data, int pos) {

                if (isdetecting) {
                    Toast.makeText(Template_AlignersActivity.this, "Please wait while detecting images...", Toast.LENGTH_SHORT).show();
                } else {
                    ArrayList<ModelFile> al_img = new ArrayList<>();
                    al_img.addAll(data);
                    String imgarray = new Gson().toJson(al_img);
                    Intent intent = new Intent(Template_AlignersActivity.this, ImagePreviewActivity.class);
                    intent.putExtra("imgarray", imgarray);
                    intent.putExtra("pos", pos);
                    intent.putExtra("isdrag", false);
                    startActivityForResult(intent, 11);
                }

            }
        }, false, true, false);
        rv_selectIntraLeftOcclusal.setAdapter(selectFileIntraLeftOcclusalAdapter);
        getPrice();
    }

    Double sgst = 0.0;
    Double cgst = 0.0;

    public void getPrice() {
        if (isOnlyShow) {
            nextStepFive.setVisibility(View.GONE);
            llDetail.setVisibility(View.GONE);
            llProduct.setVisibility(View.GONE);

        } else {
            if (getIntent().getExtras().containsKey("Catelog")) {
                try {
                    llDetail.setVisibility(View.VISIBLE);
                    llProduct.setVisibility(View.VISIBLE);
                    try {
                        tvProdName.setText(catelog.getDefault_name());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    if (currency.equalsIgnoreCase("null")) {
                        currency = catelog.getSymbol_native() + " ";
                    } else {
                        currency = SharedPrefUserDetail.getString(this, SharedPrefUserDetail.chit_symbol_native, "") + " ";
                    }

                    DecimalFormat twoDForm = new DecimalFormat("#.##");
                    Double price = Double.parseDouble(catelog.getMrp());

                    Double dis_price = 0.0;
                    Double dis_perc = 0.0;

                    if (!catelog.getDiscounted_price().equals(""))
                        dis_price = Double.valueOf(catelog.getDiscounted_price());
                    if (!catelog.getDiscount_percentage().equals(""))
                        dis_perc = Double.valueOf(catelog.getDiscount_percentage());


                    Double dis_mrp = Double.valueOf(getdiscountmrp(price, dis_perc, dis_price).toString());
                    sgst = 0.0;
                    cgst = 0.0;
                    try {
                        String tax = catelog.getTax_json_data();
                        if (tax != null) {
                            setTax(tax);
                        }

                        Log.e("stategst", String.valueOf(sgst));
                        Log.e("stategst", String.valueOf(cgst));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    Double stategst = (dis_mrp / 100.0f) * sgst;
                    Double centralgst = (dis_mrp / 100.0f) * cgst;
                    Double total = dis_mrp + stategst + centralgst;

                    serialno.setText("# " + 1);
                    productqty.setText(currency + Inad.getCurrencyDecimal(total, this));
                    productprice.setText(currency + Inad.getCurrencyDecimal(total, this));

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void setTax(String field_json_data) throws JSONException {

        if (field_json_data != null) {

            boolean isF1 = true;

            JSONObject joTax = new JSONObject(field_json_data);

            Iterator keys = joTax.keys();

            while (keys.hasNext()) {
                try {
                    String key = (String) keys.next();

                    String taxVal = JsonObjParse.getValueFromJsonObj(joTax, key);

                    if (isF1) {
                        isF1 = false;
                        sgst = Double.valueOf(taxVal);


                    } else {
                        cgst = Double.valueOf(taxVal);

                    }

                    Log.e("key", key);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public String getdiscountmrp(Double mrpprice, Double discount_percentage, Double discounted_price) {
        Double discountprice = mrpprice;


        if (discount_percentage > 0) {
            if (mrpprice > 0) {
                Double totalDisc = (mrpprice * (discount_percentage / 100));
                discountprice = mrpprice - totalDisc;
            }
        } else if (discounted_price > 0) {
            Double totalDisc = mrpprice - discounted_price;

            discountprice = discounted_price;
        }

        return String.valueOf(discountprice);
    }


    private void showPictureDialog() {
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(Template_AlignersActivity.this);
        String[] pictureDialogItems = {"Photo Gallery", "Camera"};
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:

                              /*  Intent i = new Intent(CatalogeEditDataActivity.this, FileActivity.class);
                                i.putExtra("isdoc", false);
                                startActivityForResult(i, 8);*/

                                Intent mIntent = new Intent(Template_AlignersActivity.this, PickImageActivity.class);
                                mIntent.putExtra(PickImageActivity.KEY_LIMIT_MAX_IMAGE, 30);
                                mIntent.putExtra(PickImageActivity.KEY_LIMIT_MIN_IMAGE, 1);
                                if (code == 0) {
                                    startActivityForResult(mIntent, 98);
                                } else if (code == 1) {
                                    startActivityForResult(mIntent, 99);
                                } else if (code == 2) {
                                    startActivityForResult(mIntent, 97);
                                } else if (code == 3) {
                                    startActivityForResult(mIntent, 96);
                                } else if (code == 4) {
                                    startActivityForResult(mIntent, 95);
                                } else if (code == 5) {
                                    startActivityForResult(mIntent, 94);
                                } else if (code == 6) {
                                    startActivityForResult(mIntent, 93);
                                } else if (code == 7) {
                                    startActivityForResult(mIntent, 92);
                                } else if (code == 8) {
                                    startActivityForResult(mIntent, 91);
                                } else if (code == 9) {
                                    startActivityForResult(mIntent, 90);
                                } else if (code == 10) {
                                    startActivityForResult(mIntent, 89);
                                } else if (code == 11) {
                                    startActivityForResult(mIntent, 88);
                                } else if (code == 12) {
                                    startActivityForResult(mIntent, 87);
                                } else if (code == 13) {
                                    startActivityForResult(mIntent, 86);
                                } else if (code == 14) {
                                    startActivityForResult(mIntent, 85);
                                } else if (code == 15) {
                                    startActivityForResult(mIntent, 84);
                                }

                                break;
                            case 1:
                                cameraIntent();
                                break;
                        }
                    }
                });
        pictureDialog.show();
    }

    private void cameraIntent() {

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        this.screen_width = size.x;
        this.screen_height = size.y;

        File root = new File(Environment.getExternalStorageDirectory()
                + File.separator + getString(R.string.app_name) + File.separator + "Capture" + File.separator);
        root.mkdirs();
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        f3f = new File(root, "IMG" + timeStamp + ".jpg");

        try {
            imageUri = FileProvider.getUriForFile(
                    Template_AlignersActivity.this, getApplicationContext()
                            .getPackageName() + ".provider", this.f3f);
        } catch (Exception e) {
            Toast.makeText(this, "Please check SD card! Image shot is impossible!", Toast.LENGTH_SHORT).show();
        }

        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        intent.putExtra("output", imageUri);
        try {
            if (code == 0) {
                startActivityForResult(intent, CAMERA);
            } else if (code == 1) {
                startActivityForResult(intent, 101);
            } else if (code == 2) {
                startActivityForResult(intent, 102);
            } else if (code == 3) {
                startActivityForResult(intent, 103);
            } else if (code == 4) {
                startActivityForResult(intent, 104);
            } else if (code == 5) {
                startActivityForResult(intent, 105);
            } else if (code == 6) {
                startActivityForResult(intent, 106);
            } else if (code == 7) {
                startActivityForResult(intent, 107);
            } else if (code == 8) {
                startActivityForResult(intent, 108);
            } else if (code == 9) {
                startActivityForResult(intent, 109);
            } else if (code == 10) {
                startActivityForResult(intent, 110);
            } else if (code == 11) {
                startActivityForResult(intent, 111);
            } else if (code == 12) {
                startActivityForResult(intent, 112);
            } else if (code == 13) {
                startActivityForResult(intent, 113);
            } else if (code == 14) {
                startActivityForResult(intent, 114);
            } else if (code == 15) {
                startActivityForResult(intent, 115);
            }
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to ak aris
        if (resultCode == -1 && requestCode == 101) {
            Bitmap bitmap;
            try {
                bitmap = BitmapFactory.decodeFile(this.f3f.getPath());
                this.exif = new ExifInterface(this.f3f.getPath());
                if (bitmap != null) {
                    // f3f.getAbsolutePath(

                    String picturePath = f3f.getPath();
                    ArrayList<ModelFile> al_temp_selet = new ArrayList<>();
                    al_temp_selet.add(new ModelFile(picturePath, true));
                    al_temp_selet.addAll(al_selet_tray);
                    selectFileTrayAdapter.setItem(al_temp_selet);
                    al_selet_tray = new ArrayList<>();
                    al_selet_tray.addAll(al_temp_selet);
                    Detection1(0);

                } else {
                    Toast.makeText(getApplicationContext(), "Picture not taken !", Toast.LENGTH_SHORT).show();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (requestCode == 99 && resultCode == -1) {
            ArrayList<ModelFile> al_temp_selet = new ArrayList<>();
            ArrayList<String> pathList = data.getExtras().getStringArrayList(PickImageActivity.KEY_DATA_RESULT);
            if (pathList != null && !pathList.isEmpty()) {
                for (int i = 0; i < pathList.size(); i++) {
                    al_temp_selet.add(new ModelFile(pathList.get(i), true));
                }
            }

            al_temp_selet.addAll(al_selet_tray);
            selectFileTrayAdapter.setItem(al_temp_selet);
            al_selet_tray = new ArrayList<>();
            al_selet_tray.addAll(al_temp_selet);
            Detection1(0);

        } else if (resultCode == -1 && requestCode == 102) {
            Bitmap bitmap;
            try {
                bitmap = BitmapFactory.decodeFile(this.f3f.getPath());
                this.exif = new ExifInterface(this.f3f.getPath());
                if (bitmap != null) {
                    // f3f.getAbsolutePath(

                    String picturePath = f3f.getPath();
                    ArrayList<ModelFile> al_temp_selet = new ArrayList<>();
                    al_temp_selet.add(new ModelFile(picturePath, true));
                    al_temp_selet.addAll(al_selet_model);
                    selectFileModelAdapter.setItem(al_temp_selet);
                    al_selet_model = new ArrayList<>();
                    al_selet_model.addAll(al_temp_selet);
                    Detection2(0);

                } else {
                    Toast.makeText(getApplicationContext(), "Picture not taken !", Toast.LENGTH_SHORT).show();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (requestCode == 97 && resultCode == -1) {
            ArrayList<ModelFile> al_temp_selet = new ArrayList<>();
            ArrayList<String> pathList = data.getExtras().getStringArrayList(PickImageActivity.KEY_DATA_RESULT);
            if (pathList != null && !pathList.isEmpty()) {
                for (int i = 0; i < pathList.size(); i++) {
                    al_temp_selet.add(new ModelFile(pathList.get(i), true));
                }
            }

            al_temp_selet.addAll(al_selet_model);
            selectFileModelAdapter.setItem(al_temp_selet);
            al_selet_model = new ArrayList<>();
            al_selet_model.addAll(al_temp_selet);
            Detection2(0);

        } else if (resultCode == -1 && requestCode == 103) {
            Bitmap bitmap;
            try {
                bitmap = BitmapFactory.decodeFile(this.f3f.getPath());
                this.exif = new ExifInterface(this.f3f.getPath());
                if (bitmap != null) {
                    // f3f.getAbsolutePath(

                    String picturePath = f3f.getPath();
                    ArrayList<ModelFile> al_temp_selet = new ArrayList<>();
                    al_temp_selet.add(new ModelFile(picturePath, true));
                    al_temp_selet.addAll(al_selet_bite);
                    selectFileBiteAdapter.setItem(al_temp_selet);
                    al_selet_bite = new ArrayList<>();
                    al_selet_bite.addAll(al_temp_selet);
                    Detection3(0);

                } else {
                    Toast.makeText(getApplicationContext(), "Picture not taken !", Toast.LENGTH_SHORT).show();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (requestCode == 96 && resultCode == -1) {
            ArrayList<ModelFile> al_temp_selet = new ArrayList<>();
            ArrayList<String> pathList = data.getExtras().getStringArrayList(PickImageActivity.KEY_DATA_RESULT);
            if (pathList != null && !pathList.isEmpty()) {
                for (int i = 0; i < pathList.size(); i++) {
                    al_temp_selet.add(new ModelFile(pathList.get(i), true));
                }
            }

            al_temp_selet.addAll(al_selet_bite);
            selectFileBiteAdapter.setItem(al_temp_selet);
            al_selet_bite = new ArrayList<>();
            al_selet_bite.addAll(al_temp_selet);
            Detection3(0);
        } else if (resultCode == -1 && requestCode == 105) {
            Bitmap bitmap;
            try {
                bitmap = BitmapFactory.decodeFile(this.f3f.getPath());
                this.exif = new ExifInterface(this.f3f.getPath());
                if (bitmap != null) {
                    // f3f.getAbsolutePath(

                    String picturePath = f3f.getPath();
                    ArrayList<ModelFile> al_temp_selet = new ArrayList<>();
                    al_temp_selet.add(new ModelFile(picturePath, true));
                    al_temp_selet.addAll(al_selet_frontal);
                    selectFileFrontalAdapter.setItem(al_temp_selet);
                    al_selet_frontal = new ArrayList<>();
                    al_selet_frontal.addAll(al_temp_selet);
                    Detection5(0);

                } else {
                    Toast.makeText(getApplicationContext(), "Picture not taken !", Toast.LENGTH_SHORT).show();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (requestCode == 94 && resultCode == -1) {
            ArrayList<ModelFile> al_temp_selet = new ArrayList<>();
            ArrayList<String> pathList = data.getExtras().getStringArrayList(PickImageActivity.KEY_DATA_RESULT);
            if (pathList != null && !pathList.isEmpty()) {
                for (int i = 0; i < pathList.size(); i++) {
                    al_temp_selet.add(new ModelFile(pathList.get(i), true));
                }
            }

            al_temp_selet.addAll(al_selet_frontal);
            selectFileFrontalAdapter.setItem(al_temp_selet);
            al_selet_frontal = new ArrayList<>();
            al_selet_frontal.addAll(al_temp_selet);
            Detection5(0);
        } else if (resultCode == -1 && requestCode == 106) {
            Bitmap bitmap;
            try {
                bitmap = BitmapFactory.decodeFile(this.f3f.getPath());
                this.exif = new ExifInterface(this.f3f.getPath());
                if (bitmap != null) {
                    // f3f.getAbsolutePath(

                    String picturePath = f3f.getPath();
                    ArrayList<ModelFile> al_temp_selet = new ArrayList<>();
                    al_temp_selet.add(new ModelFile(picturePath, true));
                    al_temp_selet.addAll(al_selet_frontalSmile);
                    selectFileFrontalSmileAdapter.setItem(al_temp_selet);
                    al_selet_frontalSmile = new ArrayList<>();
                    al_selet_frontalSmile.addAll(al_temp_selet);
                    Detection6(0);

                } else {
                    Toast.makeText(getApplicationContext(), "Picture not taken !", Toast.LENGTH_SHORT).show();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (requestCode == 93 && resultCode == -1) {
            ArrayList<ModelFile> al_temp_selet = new ArrayList<>();
            ArrayList<String> pathList = data.getExtras().getStringArrayList(PickImageActivity.KEY_DATA_RESULT);
            if (pathList != null && !pathList.isEmpty()) {
                for (int i = 0; i < pathList.size(); i++) {
                    al_temp_selet.add(new ModelFile(pathList.get(i), true));
                }
            }

            al_temp_selet.addAll(al_selet_frontalSmile);
            selectFileFrontalSmileAdapter.setItem(al_temp_selet);
            al_selet_frontalSmile = new ArrayList<>();
            al_selet_frontalSmile.addAll(al_temp_selet);
            Detection6(0);
        } else if (resultCode == -1 && requestCode == 107) {
            Bitmap bitmap;
            try {
                bitmap = BitmapFactory.decodeFile(this.f3f.getPath());
                this.exif = new ExifInterface(this.f3f.getPath());
                if (bitmap != null) {
                    // f3f.getAbsolutePath(

                    String picturePath = f3f.getPath();
                    ArrayList<ModelFile> al_temp_selet = new ArrayList<>();
                    al_temp_selet.add(new ModelFile(picturePath, true));
                    al_temp_selet.addAll(al_selet_leftLateral);
                    selectFileLeftLateralAdapter.setItem(al_temp_selet);
                    al_selet_leftLateral = new ArrayList<>();
                    al_selet_leftLateral.addAll(al_temp_selet);
                    Detection7(0);

                } else {
                    Toast.makeText(getApplicationContext(), "Picture not taken !", Toast.LENGTH_SHORT).show();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (requestCode == 92 && resultCode == -1) {
            ArrayList<ModelFile> al_temp_selet = new ArrayList<>();
            ArrayList<String> pathList = data.getExtras().getStringArrayList(PickImageActivity.KEY_DATA_RESULT);
            if (pathList != null && !pathList.isEmpty()) {
                for (int i = 0; i < pathList.size(); i++) {
                    al_temp_selet.add(new ModelFile(pathList.get(i), true));
                }
            }

            al_temp_selet.addAll(al_selet_leftLateral);
            selectFileLeftLateralAdapter.setItem(al_temp_selet);
            al_selet_leftLateral = new ArrayList<>();
            al_selet_leftLateral.addAll(al_temp_selet);
            Detection7(0);
        } else if (resultCode == -1 && requestCode == 108) {
            Bitmap bitmap;
            try {
                bitmap = BitmapFactory.decodeFile(this.f3f.getPath());
                this.exif = new ExifInterface(this.f3f.getPath());
                if (bitmap != null) {
                    // f3f.getAbsolutePath(

                    String picturePath = f3f.getPath();
                    ArrayList<ModelFile> al_temp_selet = new ArrayList<>();
                    al_temp_selet.add(new ModelFile(picturePath, true));
                    al_temp_selet.addAll(al_selet_leftLateralSmile);
                    selectFileLeftLateralSmileAdapter.setItem(al_temp_selet);
                    al_selet_leftLateralSmile = new ArrayList<>();
                    al_selet_leftLateralSmile.addAll(al_temp_selet);
                    Detection8(0);

                } else {
                    Toast.makeText(getApplicationContext(), "Picture not taken !", Toast.LENGTH_SHORT).show();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (requestCode == 91 && resultCode == -1) {
            ArrayList<ModelFile> al_temp_selet = new ArrayList<>();
            ArrayList<String> pathList = data.getExtras().getStringArrayList(PickImageActivity.KEY_DATA_RESULT);
            if (pathList != null && !pathList.isEmpty()) {
                for (int i = 0; i < pathList.size(); i++) {
                    al_temp_selet.add(new ModelFile(pathList.get(i), true));
                }
            }

            al_temp_selet.addAll(al_selet_leftLateralSmile);
            selectFileLeftLateralSmileAdapter.setItem(al_temp_selet);
            al_selet_leftLateralSmile = new ArrayList<>();
            al_selet_leftLateralSmile.addAll(al_temp_selet);
            Detection8(0);
        } else if (resultCode == -1 && requestCode == 109) {
            Bitmap bitmap;
            try {
                bitmap = BitmapFactory.decodeFile(this.f3f.getPath());
                this.exif = new ExifInterface(this.f3f.getPath());
                if (bitmap != null) {
                    // f3f.getAbsolutePath(

                    String picturePath = f3f.getPath();
                    ArrayList<ModelFile> al_temp_selet = new ArrayList<>();
                    al_temp_selet.add(new ModelFile(picturePath, true));
                    al_temp_selet.addAll(al_selet_rightLateral);
                    selectFileRightLateralAdapter.setItem(al_temp_selet);
                    al_selet_rightLateral = new ArrayList<>();
                    al_selet_rightLateral.addAll(al_temp_selet);
                    Detection9(0);

                } else {
                    Toast.makeText(getApplicationContext(), "Picture not taken !", Toast.LENGTH_SHORT).show();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (requestCode == 90 && resultCode == -1) {
            ArrayList<ModelFile> al_temp_selet = new ArrayList<>();
            ArrayList<String> pathList = data.getExtras().getStringArrayList(PickImageActivity.KEY_DATA_RESULT);
            if (pathList != null && !pathList.isEmpty()) {
                for (int i = 0; i < pathList.size(); i++) {
                    al_temp_selet.add(new ModelFile(pathList.get(i), true));
                }
            }

            al_temp_selet.addAll(al_selet_rightLateral);
            selectFileRightLateralAdapter.setItem(al_temp_selet);
            al_selet_rightLateral = new ArrayList<>();
            al_selet_rightLateral.addAll(al_temp_selet);
            Detection9(0);
        } else if (resultCode == -1 && requestCode == 110) {
            Bitmap bitmap;
            try {
                bitmap = BitmapFactory.decodeFile(this.f3f.getPath());
                this.exif = new ExifInterface(this.f3f.getPath());
                if (bitmap != null) {
                    // f3f.getAbsolutePath(

                    String picturePath = f3f.getPath();
                    ArrayList<ModelFile> al_temp_selet = new ArrayList<>();
                    al_temp_selet.add(new ModelFile(picturePath, true));
                    al_temp_selet.addAll(al_selet_rightLateralSmile);
                    selectFileRightLateralSmileAdapter.setItem(al_temp_selet);
                    al_selet_rightLateralSmile = new ArrayList<>();
                    al_selet_rightLateralSmile.addAll(al_temp_selet);
                    Detection10(0);

                } else {
                    Toast.makeText(getApplicationContext(), "Picture not taken !", Toast.LENGTH_SHORT).show();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (requestCode == 89 && resultCode == -1) {
            ArrayList<ModelFile> al_temp_selet = new ArrayList<>();
            ArrayList<String> pathList = data.getExtras().getStringArrayList(PickImageActivity.KEY_DATA_RESULT);
            if (pathList != null && !pathList.isEmpty()) {
                for (int i = 0; i < pathList.size(); i++) {
                    al_temp_selet.add(new ModelFile(pathList.get(i), true));
                }
            }

            al_temp_selet.addAll(al_selet_rightLateralSmile);
            selectFileRightLateralSmileAdapter.setItem(al_temp_selet);
            al_selet_rightLateralSmile = new ArrayList<>();
            al_selet_rightLateralSmile.addAll(al_temp_selet);
            Detection10(0);
        } else if (resultCode == -1 && requestCode == 111) {
            Bitmap bitmap;
            try {
                bitmap = BitmapFactory.decodeFile(this.f3f.getPath());
                this.exif = new ExifInterface(this.f3f.getPath());
                if (bitmap != null) {
                    // f3f.getAbsolutePath(

                    String picturePath = f3f.getPath();
                    ArrayList<ModelFile> al_temp_selet = new ArrayList<>();
                    al_temp_selet.add(new ModelFile(picturePath, true));
                    al_temp_selet.addAll(al_selet_intraFrontal);
                    selectFileIntraFrontalAdapter.setItem(al_temp_selet);
                    al_selet_intraFrontal = new ArrayList<>();
                    al_selet_intraFrontal.addAll(al_temp_selet);
                    Detection11(0);

                } else {
                    Toast.makeText(getApplicationContext(), "Picture not taken !", Toast.LENGTH_SHORT).show();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (requestCode == 88 && resultCode == -1) {
            ArrayList<ModelFile> al_temp_selet = new ArrayList<>();
            ArrayList<String> pathList = data.getExtras().getStringArrayList(PickImageActivity.KEY_DATA_RESULT);
            if (pathList != null && !pathList.isEmpty()) {
                for (int i = 0; i < pathList.size(); i++) {
                    al_temp_selet.add(new ModelFile(pathList.get(i), true));
                }
            }

            al_temp_selet.addAll(al_selet_intraFrontal);
            selectFileIntraFrontalAdapter.setItem(al_temp_selet);
            al_selet_intraFrontal = new ArrayList<>();
            al_selet_intraFrontal.addAll(al_temp_selet);
            Detection11(0);
        } else if (resultCode == -1 && requestCode == 112) {
            Bitmap bitmap;
            try {
                bitmap = BitmapFactory.decodeFile(this.f3f.getPath());
                this.exif = new ExifInterface(this.f3f.getPath());
                if (bitmap != null) {
                    // f3f.getAbsolutePath(

                    String picturePath = f3f.getPath();
                    ArrayList<ModelFile> al_temp_selet = new ArrayList<>();
                    al_temp_selet.add(new ModelFile(picturePath, true));
                    al_temp_selet.addAll(al_selet_intraRightLateral);
                    selectFileIntraRightLateralAdapter.setItem(al_temp_selet);
                    al_selet_intraRightLateral = new ArrayList<>();
                    al_selet_intraRightLateral.addAll(al_temp_selet);
                    Detection12(0);

                } else {
                    Toast.makeText(getApplicationContext(), "Picture not taken !", Toast.LENGTH_SHORT).show();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (requestCode == 87 && resultCode == -1) {
            ArrayList<ModelFile> al_temp_selet = new ArrayList<>();
            ArrayList<String> pathList = data.getExtras().getStringArrayList(PickImageActivity.KEY_DATA_RESULT);
            if (pathList != null && !pathList.isEmpty()) {
                for (int i = 0; i < pathList.size(); i++) {
                    al_temp_selet.add(new ModelFile(pathList.get(i), true));
                }
            }

            al_temp_selet.addAll(al_selet_intraRightLateral);
            selectFileIntraRightLateralAdapter.setItem(al_temp_selet);
            al_selet_intraRightLateral = new ArrayList<>();
            al_selet_intraRightLateral.addAll(al_temp_selet);
            Detection12(0);
        } else if (resultCode == -1 && requestCode == 113) {
            Bitmap bitmap;
            try {
                bitmap = BitmapFactory.decodeFile(this.f3f.getPath());
                this.exif = new ExifInterface(this.f3f.getPath());
                if (bitmap != null) {
                    // f3f.getAbsolutePath(

                    String picturePath = f3f.getPath();
                    ArrayList<ModelFile> al_temp_selet = new ArrayList<>();
                    al_temp_selet.add(new ModelFile(picturePath, true));
                    al_temp_selet.addAll(al_selet_intraLeftLateral);
                    selectFileIntraLeftLateralAdapter.setItem(al_temp_selet);
                    al_selet_intraLeftLateral = new ArrayList<>();
                    al_selet_intraLeftLateral.addAll(al_temp_selet);
                    Detection13(0);

                } else {
                    Toast.makeText(getApplicationContext(), "Picture not taken !", Toast.LENGTH_SHORT).show();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (requestCode == 86 && resultCode == -1) {
            ArrayList<ModelFile> al_temp_selet = new ArrayList<>();
            ArrayList<String> pathList = data.getExtras().getStringArrayList(PickImageActivity.KEY_DATA_RESULT);
            if (pathList != null && !pathList.isEmpty()) {
                for (int i = 0; i < pathList.size(); i++) {
                    al_temp_selet.add(new ModelFile(pathList.get(i), true));
                }
            }

            al_temp_selet.addAll(al_selet_intraLeftLateral);
            selectFileIntraLeftLateralAdapter.setItem(al_temp_selet);
            al_selet_intraLeftLateral = new ArrayList<>();
            al_selet_intraLeftLateral.addAll(al_temp_selet);
            Detection13(0);
        } else if (resultCode == -1 && requestCode == 114) {
            Bitmap bitmap;
            try {
                bitmap = BitmapFactory.decodeFile(this.f3f.getPath());
                this.exif = new ExifInterface(this.f3f.getPath());
                if (bitmap != null) {
                    // f3f.getAbsolutePath(

                    String picturePath = f3f.getPath();
                    ArrayList<ModelFile> al_temp_selet = new ArrayList<>();
                    al_temp_selet.add(new ModelFile(picturePath, true));
                    al_temp_selet.addAll(al_selet_intraUpperOcclusal);
                    selectFileIntraUpperOcclusalAdapter.setItem(al_temp_selet);
                    al_selet_intraUpperOcclusal = new ArrayList<>();
                    al_selet_intraUpperOcclusal.addAll(al_temp_selet);
                    Detection14(0);

                } else {
                    Toast.makeText(getApplicationContext(), "Picture not taken !", Toast.LENGTH_SHORT).show();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (requestCode == 85 && resultCode == -1) {
            ArrayList<ModelFile> al_temp_selet = new ArrayList<>();
            ArrayList<String> pathList = data.getExtras().getStringArrayList(PickImageActivity.KEY_DATA_RESULT);
            if (pathList != null && !pathList.isEmpty()) {
                for (int i = 0; i < pathList.size(); i++) {
                    al_temp_selet.add(new ModelFile(pathList.get(i), true));
                }
            }

            al_temp_selet.addAll(al_selet_intraUpperOcclusal);
            selectFileIntraUpperOcclusalAdapter.setItem(al_temp_selet);
            al_selet_intraUpperOcclusal = new ArrayList<>();
            al_selet_intraUpperOcclusal.addAll(al_temp_selet);
            Detection14(0);
        } else if (resultCode == -1 && requestCode == 115) {
            Bitmap bitmap;
            try {
                bitmap = BitmapFactory.decodeFile(this.f3f.getPath());
                this.exif = new ExifInterface(this.f3f.getPath());
                if (bitmap != null) {
                    // f3f.getAbsolutePath(

                    String picturePath = f3f.getPath();
                    ArrayList<ModelFile> al_temp_selet = new ArrayList<>();
                    al_temp_selet.add(new ModelFile(picturePath, true));
                    al_temp_selet.addAll(al_selet_intraLeftOcclusal);
                    selectFileIntraLeftOcclusalAdapter.setItem(al_temp_selet);
                    al_selet_intraLeftOcclusal = new ArrayList<>();
                    al_selet_intraLeftOcclusal.addAll(al_temp_selet);
                    Detection15(0);

                } else {
                    Toast.makeText(getApplicationContext(), "Picture not taken !", Toast.LENGTH_SHORT).show();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (requestCode == 84 && resultCode == -1) {
            ArrayList<ModelFile> al_temp_selet = new ArrayList<>();
            ArrayList<String> pathList = data.getExtras().getStringArrayList(PickImageActivity.KEY_DATA_RESULT);
            if (pathList != null && !pathList.isEmpty()) {
                for (int i = 0; i < pathList.size(); i++) {
                    al_temp_selet.add(new ModelFile(pathList.get(i), true));
                }
            }

            al_temp_selet.addAll(al_selet_intraLeftOcclusal);
            selectFileIntraLeftOcclusalAdapter.setItem(al_temp_selet);
            al_selet_intraLeftOcclusal = new ArrayList<>();
            al_selet_intraLeftOcclusal.addAll(al_temp_selet);
            Detection15(0);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void initvision() {
        Vision.Builder visionBuilder = new Vision.Builder(
                new NetHttpTransport(),
                new AndroidJsonFactory(),
                null);

        visionBuilder.setVisionRequestInitializer(
                new VisionRequestInitializer("AIzaSyBljKEMCnXTENIltyt8_EzpIHRgKoNLbcI"));

        vision = visionBuilder.build();

    }

    private void Detection1(final int pos) {
        AsyncTask.execute(new Runnable() {

            @Override
            public void run() {
                try {

                    if (!sm.getIsvisiondetect().equals("1")) return;

                    isdetecting = true;
                    //InputStream inputStream = new FileInputStream("");;
                    if (al_selet_tray.get(pos).isIsdetect()) {

                        runOnUiThread(new Runnable() {
                            public void run() {

                                int c_p = pos;
                                c_p++;
                                if (c_p < al_selet_tray.size()) {
                                    Detection1(c_p);
                                } else {
                                    isdetecting = false;
                                    Toast.makeText(Template_AlignersActivity.this, "Image Detected !", Toast.LENGTH_LONG).show();
                                }

                            }
                        });
                        return;

                    }
                    InputStream inputStream = null;
                    if (al_selet_tray.get(pos).isIsoffline()) {
                        inputStream = new FileInputStream(al_selet_tray.get(pos).getImgpath());
                    } else {
                        inputStream = new URL(al_selet_tray.get(pos).getImgpath()).openStream();
                    }

                    byte[] photoData = org.apache.commons.io.IOUtils.toByteArray(inputStream);

                    com.google.api.services.vision.v1.model.Image inputImage = new com.google.api.services.vision.v1.model.Image();
                    inputImage.encodeContent(photoData);

                   /* Feature landFeature = new Feature();
                    landFeature.setType("LANDMARK_DETECTION");*/


                    Feature Webdetection = new Feature();
                    Webdetection.setType("WEB_DETECTION");
                    //Webdetection.setMaxResults(2);

                    Feature desiredFeature = new Feature();
                    desiredFeature.setType("TEXT_DETECTION");

                    Feature landdetect = new Feature();
                    landdetect.setType("LANDMARK_DETECTION");

                    Feature facedetect = new Feature();
                    facedetect.setType("FACE_DETECTION");


                    AnnotateImageRequest request = new AnnotateImageRequest();
                    request.setImage(inputImage);
                    request.setFeatures(Arrays.asList(Webdetection, desiredFeature, landdetect));

                    BatchAnnotateImagesRequest batchRequest = new BatchAnnotateImagesRequest();
                    batchRequest.setRequests(Arrays.asList(request));

                    BatchAnnotateImagesResponse batchResponse =
                            vision.images().annotate(batchRequest).execute();

                    ArrayList<ModelFile> al_detect = new ArrayList<>();

                    WebDetection web = batchResponse.getResponses().get(0).getWebDetection();
                    //TextAnnotation tax = batchResponse.getResponses().get(0).getFullTextAnnotation();
                    List<EntityAnnotation> tax = batchResponse.getResponses().get(0).getTextAnnotations();
                    List<EntityAnnotation> landmark = batchResponse.getResponses().get(0).getLandmarkAnnotations();

                    al_detect.add(new ModelFile(true, "Web Detect", pos));
                    if (web != null) {
                        List<WebEntity> webEntities = web.getWebEntities();
                        for (int a = 0; a < webEntities.size(); a++) {
                            String det_str = webEntities.get(a).getDescription();
                            al_detect.add(new ModelFile(false, det_str, pos));
                        }
                    }

                   /* if (tax != null) {
                        al_detect.add(new ModelFile(true, "Full Text Detect"));
                        al_detect.add(new ModelFile(false, tax.toString()));
                    }*/
                    al_detect.add(new ModelFile(true, "Text Detect", pos));
                    if (tax != null) {

                        for (int a = 0; a < tax.size(); a++) {
                            String det_str = tax.get(a).getDescription();
                            al_detect.add(new ModelFile(false, det_str, pos));
                        }
                    }

                    al_detect.add(new ModelFile(true, "LandMark Detect", pos));
                    if (landmark != null) {
                        for (int a = 0; a < landmark.size(); a++) {
                            String det_str = landmark.get(a).getDescription();
                            al_detect.add(new ModelFile(false, det_str, pos));
                        }
                    }


                    al_selet_tray.get(pos).setAl_detect(al_detect);
                    //al_selet.get(pos).setDescription(description);
                    al_selet_tray.get(pos).setIsdetect(true);


                    runOnUiThread(new Runnable() {
                        public void run() {
                            selectFileTrayAdapter.setItem(al_selet_tray);
                            int c_p = pos;
                            c_p++;
                            if (c_p < al_selet_tray.size()) {
                                Detection1(c_p);
                            } else {
                                isdetecting = false;
                                Toast.makeText(Template_AlignersActivity.this, "Image Detected !", Toast.LENGTH_LONG).show();
                            }
                        }
                    });

                } catch (Exception e) {
                    Log.d("ERROR", e.getMessage());
                }
            }
        });
    }

    private void Detection2(final int pos) {
        AsyncTask.execute(new Runnable() {

            @Override
            public void run() {
                try {

                    if (!sm.getIsvisiondetect().equals("1")) return;

                    isdetecting = true;
                    //InputStream inputStream = new FileInputStream("");;
                    if (al_selet_model.get(pos).isIsdetect()) {

                        runOnUiThread(new Runnable() {
                            public void run() {

                                int c_p = pos;
                                c_p++;
                                if (c_p < al_selet_model.size()) {
                                    Detection2(c_p);
                                } else {
                                    isdetecting = false;
                                    Toast.makeText(Template_AlignersActivity.this, "Image Detected !", Toast.LENGTH_LONG).show();
                                }

                            }
                        });
                        return;

                    }
                    InputStream inputStream = null;
                    if (al_selet_model.get(pos).isIsoffline()) {
                        inputStream = new FileInputStream(al_selet_model.get(pos).getImgpath());
                    } else {
                        inputStream = new URL(al_selet_model.get(pos).getImgpath()).openStream();
                    }

                    byte[] photoData = org.apache.commons.io.IOUtils.toByteArray(inputStream);

                    com.google.api.services.vision.v1.model.Image inputImage = new com.google.api.services.vision.v1.model.Image();
                    inputImage.encodeContent(photoData);

                   /* Feature landFeature = new Feature();
                    landFeature.setType("LANDMARK_DETECTION");*/


                    Feature Webdetection = new Feature();
                    Webdetection.setType("WEB_DETECTION");
                    //Webdetection.setMaxResults(2);

                    Feature desiredFeature = new Feature();
                    desiredFeature.setType("TEXT_DETECTION");

                    Feature landdetect = new Feature();
                    landdetect.setType("LANDMARK_DETECTION");

                    Feature facedetect = new Feature();
                    facedetect.setType("FACE_DETECTION");


                    AnnotateImageRequest request = new AnnotateImageRequest();
                    request.setImage(inputImage);
                    request.setFeatures(Arrays.asList(Webdetection, desiredFeature, landdetect));

                    BatchAnnotateImagesRequest batchRequest = new BatchAnnotateImagesRequest();
                    batchRequest.setRequests(Arrays.asList(request));

                    BatchAnnotateImagesResponse batchResponse =
                            vision.images().annotate(batchRequest).execute();

                    ArrayList<ModelFile> al_detect = new ArrayList<>();

                    WebDetection web = batchResponse.getResponses().get(0).getWebDetection();
                    //TextAnnotation tax = batchResponse.getResponses().get(0).getFullTextAnnotation();
                    List<EntityAnnotation> tax = batchResponse.getResponses().get(0).getTextAnnotations();
                    List<EntityAnnotation> landmark = batchResponse.getResponses().get(0).getLandmarkAnnotations();

                    al_detect.add(new ModelFile(true, "Web Detect", pos));
                    if (web != null) {
                        List<WebEntity> webEntities = web.getWebEntities();
                        for (int a = 0; a < webEntities.size(); a++) {
                            String det_str = webEntities.get(a).getDescription();
                            al_detect.add(new ModelFile(false, det_str, pos));
                        }
                    }

                   /* if (tax != null) {
                        al_detect.add(new ModelFile(true, "Full Text Detect"));
                        al_detect.add(new ModelFile(false, tax.toString()));
                    }*/
                    al_detect.add(new ModelFile(true, "Text Detect", pos));
                    if (tax != null) {

                        for (int a = 0; a < tax.size(); a++) {
                            String det_str = tax.get(a).getDescription();
                            al_detect.add(new ModelFile(false, det_str, pos));
                        }
                    }

                    al_detect.add(new ModelFile(true, "LandMark Detect", pos));
                    if (landmark != null) {
                        for (int a = 0; a < landmark.size(); a++) {
                            String det_str = landmark.get(a).getDescription();
                            al_detect.add(new ModelFile(false, det_str, pos));
                        }
                    }


                    al_selet_model.get(pos).setAl_detect(al_detect);
                    //al_selet.get(pos).setDescription(description);
                    al_selet_model.get(pos).setIsdetect(true);


                    runOnUiThread(new Runnable() {
                        public void run() {
                            selectFileModelAdapter.setItem(al_selet_model);
                            int c_p = pos;
                            c_p++;
                            if (c_p < al_selet_model.size()) {
                                Detection2(c_p);
                            } else {
                                isdetecting = false;
                                Toast.makeText(Template_AlignersActivity.this, "Image Detected !", Toast.LENGTH_LONG).show();
                            }
                        }
                    });

                } catch (Exception e) {
                    Log.d("ERROR", e.getMessage());
                }
            }
        });
    }

    private void Detection3(final int pos) {
        AsyncTask.execute(new Runnable() {

            @Override
            public void run() {
                try {

                    if (!sm.getIsvisiondetect().equals("1")) return;

                    isdetecting = true;
                    //InputStream inputStream = new FileInputStream("");;
                    if (al_selet_bite.get(pos).isIsdetect()) {

                        runOnUiThread(new Runnable() {
                            public void run() {

                                int c_p = pos;
                                c_p++;
                                if (c_p < al_selet_bite.size()) {
                                    Detection2(c_p);
                                } else {
                                    isdetecting = false;
                                    Toast.makeText(Template_AlignersActivity.this, "Image Detected !", Toast.LENGTH_LONG).show();
                                }

                            }
                        });
                        return;

                    }
                    InputStream inputStream = null;
                    if (al_selet_bite.get(pos).isIsoffline()) {
                        inputStream = new FileInputStream(al_selet_bite.get(pos).getImgpath());
                    } else {
                        inputStream = new URL(al_selet_bite.get(pos).getImgpath()).openStream();
                    }

                    byte[] photoData = org.apache.commons.io.IOUtils.toByteArray(inputStream);

                    com.google.api.services.vision.v1.model.Image inputImage = new com.google.api.services.vision.v1.model.Image();
                    inputImage.encodeContent(photoData);

                   /* Feature landFeature = new Feature();
                    landFeature.setType("LANDMARK_DETECTION");*/


                    Feature Webdetection = new Feature();
                    Webdetection.setType("WEB_DETECTION");
                    //Webdetection.setMaxResults(2);

                    Feature desiredFeature = new Feature();
                    desiredFeature.setType("TEXT_DETECTION");

                    Feature landdetect = new Feature();
                    landdetect.setType("LANDMARK_DETECTION");

                    Feature facedetect = new Feature();
                    facedetect.setType("FACE_DETECTION");


                    AnnotateImageRequest request = new AnnotateImageRequest();
                    request.setImage(inputImage);
                    request.setFeatures(Arrays.asList(Webdetection, desiredFeature, landdetect));

                    BatchAnnotateImagesRequest batchRequest = new BatchAnnotateImagesRequest();
                    batchRequest.setRequests(Arrays.asList(request));

                    BatchAnnotateImagesResponse batchResponse =
                            vision.images().annotate(batchRequest).execute();

                    ArrayList<ModelFile> al_detect = new ArrayList<>();

                    WebDetection web = batchResponse.getResponses().get(0).getWebDetection();
                    //TextAnnotation tax = batchResponse.getResponses().get(0).getFullTextAnnotation();
                    List<EntityAnnotation> tax = batchResponse.getResponses().get(0).getTextAnnotations();
                    List<EntityAnnotation> landmark = batchResponse.getResponses().get(0).getLandmarkAnnotations();

                    al_detect.add(new ModelFile(true, "Web Detect", pos));
                    if (web != null) {
                        List<WebEntity> webEntities = web.getWebEntities();
                        for (int a = 0; a < webEntities.size(); a++) {
                            String det_str = webEntities.get(a).getDescription();
                            al_detect.add(new ModelFile(false, det_str, pos));
                        }
                    }

                   /* if (tax != null) {
                        al_detect.add(new ModelFile(true, "Full Text Detect"));
                        al_detect.add(new ModelFile(false, tax.toString()));
                    }*/
                    al_detect.add(new ModelFile(true, "Text Detect", pos));
                    if (tax != null) {

                        for (int a = 0; a < tax.size(); a++) {
                            String det_str = tax.get(a).getDescription();
                            al_detect.add(new ModelFile(false, det_str, pos));
                        }
                    }

                    al_detect.add(new ModelFile(true, "LandMark Detect", pos));
                    if (landmark != null) {
                        for (int a = 0; a < landmark.size(); a++) {
                            String det_str = landmark.get(a).getDescription();
                            al_detect.add(new ModelFile(false, det_str, pos));
                        }
                    }


                    al_selet_bite.get(pos).setAl_detect(al_detect);
                    //al_selet.get(pos).setDescription(description);
                    al_selet_bite.get(pos).setIsdetect(true);


                    runOnUiThread(new Runnable() {
                        public void run() {
                            selectFileBiteAdapter.setItem(al_selet_bite);
                            int c_p = pos;
                            c_p++;
                            if (c_p < al_selet_bite.size()) {
                                Detection2(c_p);
                            } else {
                                isdetecting = false;
                                Toast.makeText(Template_AlignersActivity.this, "Image Detected !", Toast.LENGTH_LONG).show();
                            }
                        }
                    });

                } catch (Exception e) {
                    Log.d("ERROR", e.getMessage());
                }
            }
        });
    }

    private void Detection5(final int pos) {
        AsyncTask.execute(new Runnable() {

            @Override
            public void run() {
                try {

                    if (!sm.getIsvisiondetect().equals("1")) return;

                    isdetecting = true;
                    //InputStream inputStream = new FileInputStream("");;
                    if (al_selet_frontal.get(pos).isIsdetect()) {

                        runOnUiThread(new Runnable() {
                            public void run() {

                                int c_p = pos;
                                c_p++;
                                if (c_p < al_selet_frontal.size()) {
                                    Detection2(c_p);
                                } else {
                                    isdetecting = false;
                                    Toast.makeText(Template_AlignersActivity.this, "Image Detected !", Toast.LENGTH_LONG).show();
                                }

                            }
                        });
                        return;

                    }
                    InputStream inputStream = null;
                    if (al_selet_frontal.get(pos).isIsoffline()) {
                        inputStream = new FileInputStream(al_selet_frontal.get(pos).getImgpath());
                    } else {
                        inputStream = new URL(al_selet_frontal.get(pos).getImgpath()).openStream();
                    }

                    byte[] photoData = org.apache.commons.io.IOUtils.toByteArray(inputStream);

                    com.google.api.services.vision.v1.model.Image inputImage = new com.google.api.services.vision.v1.model.Image();
                    inputImage.encodeContent(photoData);

                   /* Feature landFeature = new Feature();
                    landFeature.setType("LANDMARK_DETECTION");*/


                    Feature Webdetection = new Feature();
                    Webdetection.setType("WEB_DETECTION");
                    //Webdetection.setMaxResults(2);

                    Feature desiredFeature = new Feature();
                    desiredFeature.setType("TEXT_DETECTION");

                    Feature landdetect = new Feature();
                    landdetect.setType("LANDMARK_DETECTION");

                    Feature facedetect = new Feature();
                    facedetect.setType("FACE_DETECTION");


                    AnnotateImageRequest request = new AnnotateImageRequest();
                    request.setImage(inputImage);
                    request.setFeatures(Arrays.asList(Webdetection, desiredFeature, landdetect));

                    BatchAnnotateImagesRequest batchRequest = new BatchAnnotateImagesRequest();
                    batchRequest.setRequests(Arrays.asList(request));

                    BatchAnnotateImagesResponse batchResponse =
                            vision.images().annotate(batchRequest).execute();

                    ArrayList<ModelFile> al_detect = new ArrayList<>();

                    WebDetection web = batchResponse.getResponses().get(0).getWebDetection();
                    //TextAnnotation tax = batchResponse.getResponses().get(0).getFullTextAnnotation();
                    List<EntityAnnotation> tax = batchResponse.getResponses().get(0).getTextAnnotations();
                    List<EntityAnnotation> landmark = batchResponse.getResponses().get(0).getLandmarkAnnotations();

                    al_detect.add(new ModelFile(true, "Web Detect", pos));
                    if (web != null) {
                        List<WebEntity> webEntities = web.getWebEntities();
                        for (int a = 0; a < webEntities.size(); a++) {
                            String det_str = webEntities.get(a).getDescription();
                            al_detect.add(new ModelFile(false, det_str, pos));
                        }
                    }

                   /* if (tax != null) {
                        al_detect.add(new ModelFile(true, "Full Text Detect"));
                        al_detect.add(new ModelFile(false, tax.toString()));
                    }*/
                    al_detect.add(new ModelFile(true, "Text Detect", pos));
                    if (tax != null) {

                        for (int a = 0; a < tax.size(); a++) {
                            String det_str = tax.get(a).getDescription();
                            al_detect.add(new ModelFile(false, det_str, pos));
                        }
                    }

                    al_detect.add(new ModelFile(true, "LandMark Detect", pos));
                    if (landmark != null) {
                        for (int a = 0; a < landmark.size(); a++) {
                            String det_str = landmark.get(a).getDescription();
                            al_detect.add(new ModelFile(false, det_str, pos));
                        }
                    }


                    al_selet_frontal.get(pos).setAl_detect(al_detect);
                    //al_selet.get(pos).setDescription(description);
                    al_selet_frontal.get(pos).setIsdetect(true);


                    runOnUiThread(new Runnable() {
                        public void run() {
                            selectFileFrontalAdapter.setItem(al_selet_frontal);
                            int c_p = pos;
                            c_p++;
                            if (c_p < al_selet_frontal.size()) {
                                Detection2(c_p);
                            } else {
                                isdetecting = false;
                                Toast.makeText(Template_AlignersActivity.this, "Image Detected !", Toast.LENGTH_LONG).show();
                            }
                        }
                    });

                } catch (Exception e) {
                    Log.d("ERROR", e.getMessage());
                }
            }
        });
    }

    private void Detection6(final int pos) {
        AsyncTask.execute(new Runnable() {

            @Override
            public void run() {
                try {

                    if (!sm.getIsvisiondetect().equals("1")) return;

                    isdetecting = true;
                    //InputStream inputStream = new FileInputStream("");;
                    if (al_selet_frontalSmile.get(pos).isIsdetect()) {

                        runOnUiThread(new Runnable() {
                            public void run() {

                                int c_p = pos;
                                c_p++;
                                if (c_p < al_selet_frontalSmile.size()) {
                                    Detection2(c_p);
                                } else {
                                    isdetecting = false;
                                    Toast.makeText(Template_AlignersActivity.this, "Image Detected !", Toast.LENGTH_LONG).show();
                                }

                            }
                        });
                        return;

                    }
                    InputStream inputStream = null;
                    if (al_selet_frontalSmile.get(pos).isIsoffline()) {
                        inputStream = new FileInputStream(al_selet_frontalSmile.get(pos).getImgpath());
                    } else {
                        inputStream = new URL(al_selet_frontalSmile.get(pos).getImgpath()).openStream();
                    }

                    byte[] photoData = org.apache.commons.io.IOUtils.toByteArray(inputStream);

                    com.google.api.services.vision.v1.model.Image inputImage = new com.google.api.services.vision.v1.model.Image();
                    inputImage.encodeContent(photoData);

                   /* Feature landFeature = new Feature();
                    landFeature.setType("LANDMARK_DETECTION");*/


                    Feature Webdetection = new Feature();
                    Webdetection.setType("WEB_DETECTION");
                    //Webdetection.setMaxResults(2);

                    Feature desiredFeature = new Feature();
                    desiredFeature.setType("TEXT_DETECTION");

                    Feature landdetect = new Feature();
                    landdetect.setType("LANDMARK_DETECTION");

                    Feature facedetect = new Feature();
                    facedetect.setType("FACE_DETECTION");


                    AnnotateImageRequest request = new AnnotateImageRequest();
                    request.setImage(inputImage);
                    request.setFeatures(Arrays.asList(Webdetection, desiredFeature, landdetect));

                    BatchAnnotateImagesRequest batchRequest = new BatchAnnotateImagesRequest();
                    batchRequest.setRequests(Arrays.asList(request));

                    BatchAnnotateImagesResponse batchResponse =
                            vision.images().annotate(batchRequest).execute();

                    ArrayList<ModelFile> al_detect = new ArrayList<>();

                    WebDetection web = batchResponse.getResponses().get(0).getWebDetection();
                    //TextAnnotation tax = batchResponse.getResponses().get(0).getFullTextAnnotation();
                    List<EntityAnnotation> tax = batchResponse.getResponses().get(0).getTextAnnotations();
                    List<EntityAnnotation> landmark = batchResponse.getResponses().get(0).getLandmarkAnnotations();

                    al_detect.add(new ModelFile(true, "Web Detect", pos));
                    if (web != null) {
                        List<WebEntity> webEntities = web.getWebEntities();
                        for (int a = 0; a < webEntities.size(); a++) {
                            String det_str = webEntities.get(a).getDescription();
                            al_detect.add(new ModelFile(false, det_str, pos));
                        }
                    }

                   /* if (tax != null) {
                        al_detect.add(new ModelFile(true, "Full Text Detect"));
                        al_detect.add(new ModelFile(false, tax.toString()));
                    }*/
                    al_detect.add(new ModelFile(true, "Text Detect", pos));
                    if (tax != null) {

                        for (int a = 0; a < tax.size(); a++) {
                            String det_str = tax.get(a).getDescription();
                            al_detect.add(new ModelFile(false, det_str, pos));
                        }
                    }

                    al_detect.add(new ModelFile(true, "LandMark Detect", pos));
                    if (landmark != null) {
                        for (int a = 0; a < landmark.size(); a++) {
                            String det_str = landmark.get(a).getDescription();
                            al_detect.add(new ModelFile(false, det_str, pos));
                        }
                    }


                    al_selet_frontalSmile.get(pos).setAl_detect(al_detect);
                    //al_selet.get(pos).setDescription(description);
                    al_selet_frontalSmile.get(pos).setIsdetect(true);


                    runOnUiThread(new Runnable() {
                        public void run() {
                            selectFileFrontalSmileAdapter.setItem(al_selet_frontalSmile);
                            int c_p = pos;
                            c_p++;
                            if (c_p < al_selet_frontalSmile.size()) {
                                Detection2(c_p);
                            } else {
                                isdetecting = false;
                                Toast.makeText(Template_AlignersActivity.this, "Image Detected !", Toast.LENGTH_LONG).show();
                            }
                        }
                    });

                } catch (Exception e) {
                    Log.d("ERROR", e.getMessage());
                }
            }
        });
    }

    private void Detection7(final int pos) {
        AsyncTask.execute(new Runnable() {

            @Override
            public void run() {
                try {

                    if (!sm.getIsvisiondetect().equals("1")) return;

                    isdetecting = true;
                    //InputStream inputStream = new FileInputStream("");;
                    if (al_selet_leftLateral.get(pos).isIsdetect()) {

                        runOnUiThread(new Runnable() {
                            public void run() {

                                int c_p = pos;
                                c_p++;
                                if (c_p < al_selet_leftLateral.size()) {
                                    Detection2(c_p);
                                } else {
                                    isdetecting = false;
                                    Toast.makeText(Template_AlignersActivity.this, "Image Detected !", Toast.LENGTH_LONG).show();
                                }

                            }
                        });
                        return;

                    }
                    InputStream inputStream = null;
                    if (al_selet_leftLateral.get(pos).isIsoffline()) {
                        inputStream = new FileInputStream(al_selet_leftLateral.get(pos).getImgpath());
                    } else {
                        inputStream = new URL(al_selet_leftLateral.get(pos).getImgpath()).openStream();
                    }

                    byte[] photoData = org.apache.commons.io.IOUtils.toByteArray(inputStream);

                    com.google.api.services.vision.v1.model.Image inputImage = new com.google.api.services.vision.v1.model.Image();
                    inputImage.encodeContent(photoData);

                   /* Feature landFeature = new Feature();
                    landFeature.setType("LANDMARK_DETECTION");*/


                    Feature Webdetection = new Feature();
                    Webdetection.setType("WEB_DETECTION");
                    //Webdetection.setMaxResults(2);

                    Feature desiredFeature = new Feature();
                    desiredFeature.setType("TEXT_DETECTION");

                    Feature landdetect = new Feature();
                    landdetect.setType("LANDMARK_DETECTION");

                    Feature facedetect = new Feature();
                    facedetect.setType("FACE_DETECTION");


                    AnnotateImageRequest request = new AnnotateImageRequest();
                    request.setImage(inputImage);
                    request.setFeatures(Arrays.asList(Webdetection, desiredFeature, landdetect));

                    BatchAnnotateImagesRequest batchRequest = new BatchAnnotateImagesRequest();
                    batchRequest.setRequests(Arrays.asList(request));

                    BatchAnnotateImagesResponse batchResponse =
                            vision.images().annotate(batchRequest).execute();

                    ArrayList<ModelFile> al_detect = new ArrayList<>();

                    WebDetection web = batchResponse.getResponses().get(0).getWebDetection();
                    //TextAnnotation tax = batchResponse.getResponses().get(0).getFullTextAnnotation();
                    List<EntityAnnotation> tax = batchResponse.getResponses().get(0).getTextAnnotations();
                    List<EntityAnnotation> landmark = batchResponse.getResponses().get(0).getLandmarkAnnotations();

                    al_detect.add(new ModelFile(true, "Web Detect", pos));
                    if (web != null) {
                        List<WebEntity> webEntities = web.getWebEntities();
                        for (int a = 0; a < webEntities.size(); a++) {
                            String det_str = webEntities.get(a).getDescription();
                            al_detect.add(new ModelFile(false, det_str, pos));
                        }
                    }

                   /* if (tax != null) {
                        al_detect.add(new ModelFile(true, "Full Text Detect"));
                        al_detect.add(new ModelFile(false, tax.toString()));
                    }*/
                    al_detect.add(new ModelFile(true, "Text Detect", pos));
                    if (tax != null) {

                        for (int a = 0; a < tax.size(); a++) {
                            String det_str = tax.get(a).getDescription();
                            al_detect.add(new ModelFile(false, det_str, pos));
                        }
                    }

                    al_detect.add(new ModelFile(true, "LandMark Detect", pos));
                    if (landmark != null) {
                        for (int a = 0; a < landmark.size(); a++) {
                            String det_str = landmark.get(a).getDescription();
                            al_detect.add(new ModelFile(false, det_str, pos));
                        }
                    }


                    al_selet_leftLateral.get(pos).setAl_detect(al_detect);
                    //al_selet.get(pos).setDescription(description);
                    al_selet_leftLateral.get(pos).setIsdetect(true);


                    runOnUiThread(new Runnable() {
                        public void run() {
                            selectFileLeftLateralAdapter.setItem(al_selet_leftLateral);
                            int c_p = pos;
                            c_p++;
                            if (c_p < al_selet_leftLateral.size()) {
                                Detection2(c_p);
                            } else {
                                isdetecting = false;
                                Toast.makeText(Template_AlignersActivity.this, "Image Detected !", Toast.LENGTH_LONG).show();
                            }
                        }
                    });

                } catch (Exception e) {
                    Log.d("ERROR", e.getMessage());
                }
            }
        });
    }

    private void Detection8(final int pos) {
        AsyncTask.execute(new Runnable() {

            @Override
            public void run() {
                try {

                    if (!sm.getIsvisiondetect().equals("1")) return;

                    isdetecting = true;
                    //InputStream inputStream = new FileInputStream("");;
                    if (al_selet_leftLateralSmile.get(pos).isIsdetect()) {

                        runOnUiThread(new Runnable() {
                            public void run() {

                                int c_p = pos;
                                c_p++;
                                if (c_p < al_selet_leftLateralSmile.size()) {
                                    Detection2(c_p);
                                } else {
                                    isdetecting = false;
                                    Toast.makeText(Template_AlignersActivity.this, "Image Detected !", Toast.LENGTH_LONG).show();
                                }

                            }
                        });
                        return;

                    }
                    InputStream inputStream = null;
                    if (al_selet_leftLateralSmile.get(pos).isIsoffline()) {
                        inputStream = new FileInputStream(al_selet_leftLateralSmile.get(pos).getImgpath());
                    } else {
                        inputStream = new URL(al_selet_leftLateralSmile.get(pos).getImgpath()).openStream();
                    }

                    byte[] photoData = org.apache.commons.io.IOUtils.toByteArray(inputStream);

                    com.google.api.services.vision.v1.model.Image inputImage = new com.google.api.services.vision.v1.model.Image();
                    inputImage.encodeContent(photoData);

                   /* Feature landFeature = new Feature();
                    landFeature.setType("LANDMARK_DETECTION");*/


                    Feature Webdetection = new Feature();
                    Webdetection.setType("WEB_DETECTION");
                    //Webdetection.setMaxResults(2);

                    Feature desiredFeature = new Feature();
                    desiredFeature.setType("TEXT_DETECTION");

                    Feature landdetect = new Feature();
                    landdetect.setType("LANDMARK_DETECTION");

                    Feature facedetect = new Feature();
                    facedetect.setType("FACE_DETECTION");


                    AnnotateImageRequest request = new AnnotateImageRequest();
                    request.setImage(inputImage);
                    request.setFeatures(Arrays.asList(Webdetection, desiredFeature, landdetect));

                    BatchAnnotateImagesRequest batchRequest = new BatchAnnotateImagesRequest();
                    batchRequest.setRequests(Arrays.asList(request));

                    BatchAnnotateImagesResponse batchResponse =
                            vision.images().annotate(batchRequest).execute();

                    ArrayList<ModelFile> al_detect = new ArrayList<>();

                    WebDetection web = batchResponse.getResponses().get(0).getWebDetection();
                    //TextAnnotation tax = batchResponse.getResponses().get(0).getFullTextAnnotation();
                    List<EntityAnnotation> tax = batchResponse.getResponses().get(0).getTextAnnotations();
                    List<EntityAnnotation> landmark = batchResponse.getResponses().get(0).getLandmarkAnnotations();

                    al_detect.add(new ModelFile(true, "Web Detect", pos));
                    if (web != null) {
                        List<WebEntity> webEntities = web.getWebEntities();
                        for (int a = 0; a < webEntities.size(); a++) {
                            String det_str = webEntities.get(a).getDescription();
                            al_detect.add(new ModelFile(false, det_str, pos));
                        }
                    }

                   /* if (tax != null) {
                        al_detect.add(new ModelFile(true, "Full Text Detect"));
                        al_detect.add(new ModelFile(false, tax.toString()));
                    }*/
                    al_detect.add(new ModelFile(true, "Text Detect", pos));
                    if (tax != null) {

                        for (int a = 0; a < tax.size(); a++) {
                            String det_str = tax.get(a).getDescription();
                            al_detect.add(new ModelFile(false, det_str, pos));
                        }
                    }

                    al_detect.add(new ModelFile(true, "LandMark Detect", pos));
                    if (landmark != null) {
                        for (int a = 0; a < landmark.size(); a++) {
                            String det_str = landmark.get(a).getDescription();
                            al_detect.add(new ModelFile(false, det_str, pos));
                        }
                    }


                    al_selet_leftLateralSmile.get(pos).setAl_detect(al_detect);
                    //al_selet.get(pos).setDescription(description);
                    al_selet_leftLateralSmile.get(pos).setIsdetect(true);


                    runOnUiThread(new Runnable() {
                        public void run() {
                            selectFileLeftLateralSmileAdapter.setItem(al_selet_leftLateralSmile);
                            int c_p = pos;
                            c_p++;
                            if (c_p < al_selet_leftLateralSmile.size()) {
                                Detection2(c_p);
                            } else {
                                isdetecting = false;
                                Toast.makeText(Template_AlignersActivity.this, "Image Detected !", Toast.LENGTH_LONG).show();
                            }
                        }
                    });

                } catch (Exception e) {
                    Log.d("ERROR", e.getMessage());
                }
            }
        });
    }

    private void Detection9(final int pos) {
        AsyncTask.execute(new Runnable() {

            @Override
            public void run() {
                try {

                    if (!sm.getIsvisiondetect().equals("1")) return;

                    isdetecting = true;
                    //InputStream inputStream = new FileInputStream("");;
                    if (al_selet_rightLateral.get(pos).isIsdetect()) {

                        runOnUiThread(new Runnable() {
                            public void run() {

                                int c_p = pos;
                                c_p++;
                                if (c_p < al_selet_rightLateral.size()) {
                                    Detection2(c_p);
                                } else {
                                    isdetecting = false;
                                    Toast.makeText(Template_AlignersActivity.this, "Image Detected !", Toast.LENGTH_LONG).show();
                                }

                            }
                        });
                        return;

                    }
                    InputStream inputStream = null;
                    if (al_selet_rightLateral.get(pos).isIsoffline()) {
                        inputStream = new FileInputStream(al_selet_rightLateral.get(pos).getImgpath());
                    } else {
                        inputStream = new URL(al_selet_rightLateral.get(pos).getImgpath()).openStream();
                    }

                    byte[] photoData = org.apache.commons.io.IOUtils.toByteArray(inputStream);

                    com.google.api.services.vision.v1.model.Image inputImage = new com.google.api.services.vision.v1.model.Image();
                    inputImage.encodeContent(photoData);

                   /* Feature landFeature = new Feature();
                    landFeature.setType("LANDMARK_DETECTION");*/


                    Feature Webdetection = new Feature();
                    Webdetection.setType("WEB_DETECTION");
                    //Webdetection.setMaxResults(2);

                    Feature desiredFeature = new Feature();
                    desiredFeature.setType("TEXT_DETECTION");

                    Feature landdetect = new Feature();
                    landdetect.setType("LANDMARK_DETECTION");

                    Feature facedetect = new Feature();
                    facedetect.setType("FACE_DETECTION");


                    AnnotateImageRequest request = new AnnotateImageRequest();
                    request.setImage(inputImage);
                    request.setFeatures(Arrays.asList(Webdetection, desiredFeature, landdetect));

                    BatchAnnotateImagesRequest batchRequest = new BatchAnnotateImagesRequest();
                    batchRequest.setRequests(Arrays.asList(request));

                    BatchAnnotateImagesResponse batchResponse =
                            vision.images().annotate(batchRequest).execute();

                    ArrayList<ModelFile> al_detect = new ArrayList<>();

                    WebDetection web = batchResponse.getResponses().get(0).getWebDetection();
                    //TextAnnotation tax = batchResponse.getResponses().get(0).getFullTextAnnotation();
                    List<EntityAnnotation> tax = batchResponse.getResponses().get(0).getTextAnnotations();
                    List<EntityAnnotation> landmark = batchResponse.getResponses().get(0).getLandmarkAnnotations();

                    al_detect.add(new ModelFile(true, "Web Detect", pos));
                    if (web != null) {
                        List<WebEntity> webEntities = web.getWebEntities();
                        for (int a = 0; a < webEntities.size(); a++) {
                            String det_str = webEntities.get(a).getDescription();
                            al_detect.add(new ModelFile(false, det_str, pos));
                        }
                    }

                   /* if (tax != null) {
                        al_detect.add(new ModelFile(true, "Full Text Detect"));
                        al_detect.add(new ModelFile(false, tax.toString()));
                    }*/
                    al_detect.add(new ModelFile(true, "Text Detect", pos));
                    if (tax != null) {

                        for (int a = 0; a < tax.size(); a++) {
                            String det_str = tax.get(a).getDescription();
                            al_detect.add(new ModelFile(false, det_str, pos));
                        }
                    }

                    al_detect.add(new ModelFile(true, "LandMark Detect", pos));
                    if (landmark != null) {
                        for (int a = 0; a < landmark.size(); a++) {
                            String det_str = landmark.get(a).getDescription();
                            al_detect.add(new ModelFile(false, det_str, pos));
                        }
                    }


                    al_selet_rightLateral.get(pos).setAl_detect(al_detect);
                    //al_selet.get(pos).setDescription(description);
                    al_selet_rightLateral.get(pos).setIsdetect(true);


                    runOnUiThread(new Runnable() {
                        public void run() {
                            selectFileRightLateralAdapter.setItem(al_selet_rightLateral);
                            int c_p = pos;
                            c_p++;
                            if (c_p < al_selet_rightLateral.size()) {
                                Detection2(c_p);
                            } else {
                                isdetecting = false;
                                Toast.makeText(Template_AlignersActivity.this, "Image Detected !", Toast.LENGTH_LONG).show();
                            }
                        }
                    });

                } catch (Exception e) {
                    Log.d("ERROR", e.getMessage());
                }
            }
        });
    }

    private void Detection10(final int pos) {
        AsyncTask.execute(new Runnable() {

            @Override
            public void run() {
                try {

                    if (!sm.getIsvisiondetect().equals("1")) return;

                    isdetecting = true;
                    //InputStream inputStream = new FileInputStream("");;
                    if (al_selet_rightLateralSmile.get(pos).isIsdetect()) {

                        runOnUiThread(new Runnable() {
                            public void run() {

                                int c_p = pos;
                                c_p++;
                                if (c_p < al_selet_rightLateralSmile.size()) {
                                    Detection2(c_p);
                                } else {
                                    isdetecting = false;
                                    Toast.makeText(Template_AlignersActivity.this, "Image Detected !", Toast.LENGTH_LONG).show();
                                }

                            }
                        });
                        return;

                    }
                    InputStream inputStream = null;
                    if (al_selet_rightLateralSmile.get(pos).isIsoffline()) {
                        inputStream = new FileInputStream(al_selet_rightLateralSmile.get(pos).getImgpath());
                    } else {
                        inputStream = new URL(al_selet_rightLateralSmile.get(pos).getImgpath()).openStream();
                    }

                    byte[] photoData = org.apache.commons.io.IOUtils.toByteArray(inputStream);

                    com.google.api.services.vision.v1.model.Image inputImage = new com.google.api.services.vision.v1.model.Image();
                    inputImage.encodeContent(photoData);

                   /* Feature landFeature = new Feature();
                    landFeature.setType("LANDMARK_DETECTION");*/


                    Feature Webdetection = new Feature();
                    Webdetection.setType("WEB_DETECTION");
                    //Webdetection.setMaxResults(2);

                    Feature desiredFeature = new Feature();
                    desiredFeature.setType("TEXT_DETECTION");

                    Feature landdetect = new Feature();
                    landdetect.setType("LANDMARK_DETECTION");

                    Feature facedetect = new Feature();
                    facedetect.setType("FACE_DETECTION");


                    AnnotateImageRequest request = new AnnotateImageRequest();
                    request.setImage(inputImage);
                    request.setFeatures(Arrays.asList(Webdetection, desiredFeature, landdetect));

                    BatchAnnotateImagesRequest batchRequest = new BatchAnnotateImagesRequest();
                    batchRequest.setRequests(Arrays.asList(request));

                    BatchAnnotateImagesResponse batchResponse =
                            vision.images().annotate(batchRequest).execute();

                    ArrayList<ModelFile> al_detect = new ArrayList<>();

                    WebDetection web = batchResponse.getResponses().get(0).getWebDetection();
                    //TextAnnotation tax = batchResponse.getResponses().get(0).getFullTextAnnotation();
                    List<EntityAnnotation> tax = batchResponse.getResponses().get(0).getTextAnnotations();
                    List<EntityAnnotation> landmark = batchResponse.getResponses().get(0).getLandmarkAnnotations();

                    al_detect.add(new ModelFile(true, "Web Detect", pos));
                    if (web != null) {
                        List<WebEntity> webEntities = web.getWebEntities();
                        for (int a = 0; a < webEntities.size(); a++) {
                            String det_str = webEntities.get(a).getDescription();
                            al_detect.add(new ModelFile(false, det_str, pos));
                        }
                    }

                   /* if (tax != null) {
                        al_detect.add(new ModelFile(true, "Full Text Detect"));
                        al_detect.add(new ModelFile(false, tax.toString()));
                    }*/
                    al_detect.add(new ModelFile(true, "Text Detect", pos));
                    if (tax != null) {

                        for (int a = 0; a < tax.size(); a++) {
                            String det_str = tax.get(a).getDescription();
                            al_detect.add(new ModelFile(false, det_str, pos));
                        }
                    }

                    al_detect.add(new ModelFile(true, "LandMark Detect", pos));
                    if (landmark != null) {
                        for (int a = 0; a < landmark.size(); a++) {
                            String det_str = landmark.get(a).getDescription();
                            al_detect.add(new ModelFile(false, det_str, pos));
                        }
                    }


                    al_selet_rightLateralSmile.get(pos).setAl_detect(al_detect);
                    //al_selet.get(pos).setDescription(description);
                    al_selet_rightLateralSmile.get(pos).setIsdetect(true);


                    runOnUiThread(new Runnable() {
                        public void run() {
                            selectFileRightLateralSmileAdapter.setItem(al_selet_rightLateralSmile);
                            int c_p = pos;
                            c_p++;
                            if (c_p < al_selet_rightLateralSmile.size()) {
                                Detection2(c_p);
                            } else {
                                isdetecting = false;
                                Toast.makeText(Template_AlignersActivity.this, "Image Detected !", Toast.LENGTH_LONG).show();
                            }
                        }
                    });

                } catch (Exception e) {
                    Log.d("ERROR", e.getMessage());
                }
            }
        });
    }

    private void Detection11(final int pos) {
        AsyncTask.execute(new Runnable() {

            @Override
            public void run() {
                try {

                    if (!sm.getIsvisiondetect().equals("1")) return;

                    isdetecting = true;
                    //InputStream inputStream = new FileInputStream("");;
                    if (al_selet_intraFrontal.get(pos).isIsdetect()) {

                        runOnUiThread(new Runnable() {
                            public void run() {

                                int c_p = pos;
                                c_p++;
                                if (c_p < al_selet_intraFrontal.size()) {
                                    Detection2(c_p);
                                } else {
                                    isdetecting = false;
                                    Toast.makeText(Template_AlignersActivity.this, "Image Detected !", Toast.LENGTH_LONG).show();
                                }

                            }
                        });
                        return;

                    }
                    InputStream inputStream = null;
                    if (al_selet_intraFrontal.get(pos).isIsoffline()) {
                        inputStream = new FileInputStream(al_selet_intraFrontal.get(pos).getImgpath());
                    } else {
                        inputStream = new URL(al_selet_intraFrontal.get(pos).getImgpath()).openStream();
                    }

                    byte[] photoData = org.apache.commons.io.IOUtils.toByteArray(inputStream);

                    com.google.api.services.vision.v1.model.Image inputImage = new com.google.api.services.vision.v1.model.Image();
                    inputImage.encodeContent(photoData);

                   /* Feature landFeature = new Feature();
                    landFeature.setType("LANDMARK_DETECTION");*/


                    Feature Webdetection = new Feature();
                    Webdetection.setType("WEB_DETECTION");
                    //Webdetection.setMaxResults(2);

                    Feature desiredFeature = new Feature();
                    desiredFeature.setType("TEXT_DETECTION");

                    Feature landdetect = new Feature();
                    landdetect.setType("LANDMARK_DETECTION");

                    Feature facedetect = new Feature();
                    facedetect.setType("FACE_DETECTION");


                    AnnotateImageRequest request = new AnnotateImageRequest();
                    request.setImage(inputImage);
                    request.setFeatures(Arrays.asList(Webdetection, desiredFeature, landdetect));

                    BatchAnnotateImagesRequest batchRequest = new BatchAnnotateImagesRequest();
                    batchRequest.setRequests(Arrays.asList(request));

                    BatchAnnotateImagesResponse batchResponse =
                            vision.images().annotate(batchRequest).execute();

                    ArrayList<ModelFile> al_detect = new ArrayList<>();

                    WebDetection web = batchResponse.getResponses().get(0).getWebDetection();
                    //TextAnnotation tax = batchResponse.getResponses().get(0).getFullTextAnnotation();
                    List<EntityAnnotation> tax = batchResponse.getResponses().get(0).getTextAnnotations();
                    List<EntityAnnotation> landmark = batchResponse.getResponses().get(0).getLandmarkAnnotations();

                    al_detect.add(new ModelFile(true, "Web Detect", pos));
                    if (web != null) {
                        List<WebEntity> webEntities = web.getWebEntities();
                        for (int a = 0; a < webEntities.size(); a++) {
                            String det_str = webEntities.get(a).getDescription();
                            al_detect.add(new ModelFile(false, det_str, pos));
                        }
                    }

                   /* if (tax != null) {
                        al_detect.add(new ModelFile(true, "Full Text Detect"));
                        al_detect.add(new ModelFile(false, tax.toString()));
                    }*/
                    al_detect.add(new ModelFile(true, "Text Detect", pos));
                    if (tax != null) {

                        for (int a = 0; a < tax.size(); a++) {
                            String det_str = tax.get(a).getDescription();
                            al_detect.add(new ModelFile(false, det_str, pos));
                        }
                    }

                    al_detect.add(new ModelFile(true, "LandMark Detect", pos));
                    if (landmark != null) {
                        for (int a = 0; a < landmark.size(); a++) {
                            String det_str = landmark.get(a).getDescription();
                            al_detect.add(new ModelFile(false, det_str, pos));
                        }
                    }


                    al_selet_intraFrontal.get(pos).setAl_detect(al_detect);
                    //al_selet.get(pos).setDescription(description);
                    al_selet_intraFrontal.get(pos).setIsdetect(true);


                    runOnUiThread(new Runnable() {
                        public void run() {
                            selectFileIntraFrontalAdapter.setItem(al_selet_intraFrontal);
                            int c_p = pos;
                            c_p++;
                            if (c_p < al_selet_intraFrontal.size()) {
                                Detection2(c_p);
                            } else {
                                isdetecting = false;
                                Toast.makeText(Template_AlignersActivity.this, "Image Detected !", Toast.LENGTH_LONG).show();
                            }
                        }
                    });

                } catch (Exception e) {
                    Log.d("ERROR", e.getMessage());
                }
            }
        });
    }

    private void Detection12(final int pos) {
        AsyncTask.execute(new Runnable() {

            @Override
            public void run() {
                try {

                    if (!sm.getIsvisiondetect().equals("1")) return;

                    isdetecting = true;
                    //InputStream inputStream = new FileInputStream("");;
                    if (al_selet_intraRightLateral.get(pos).isIsdetect()) {

                        runOnUiThread(new Runnable() {
                            public void run() {

                                int c_p = pos;
                                c_p++;
                                if (c_p < al_selet_intraRightLateral.size()) {
                                    Detection2(c_p);
                                } else {
                                    isdetecting = false;
                                    Toast.makeText(Template_AlignersActivity.this, "Image Detected !", Toast.LENGTH_LONG).show();
                                }

                            }
                        });
                        return;

                    }
                    InputStream inputStream = null;
                    if (al_selet_intraRightLateral.get(pos).isIsoffline()) {
                        inputStream = new FileInputStream(al_selet_intraRightLateral.get(pos).getImgpath());
                    } else {
                        inputStream = new URL(al_selet_intraRightLateral.get(pos).getImgpath()).openStream();
                    }

                    byte[] photoData = org.apache.commons.io.IOUtils.toByteArray(inputStream);

                    com.google.api.services.vision.v1.model.Image inputImage = new com.google.api.services.vision.v1.model.Image();
                    inputImage.encodeContent(photoData);

                   /* Feature landFeature = new Feature();
                    landFeature.setType("LANDMARK_DETECTION");*/


                    Feature Webdetection = new Feature();
                    Webdetection.setType("WEB_DETECTION");
                    //Webdetection.setMaxResults(2);

                    Feature desiredFeature = new Feature();
                    desiredFeature.setType("TEXT_DETECTION");

                    Feature landdetect = new Feature();
                    landdetect.setType("LANDMARK_DETECTION");

                    Feature facedetect = new Feature();
                    facedetect.setType("FACE_DETECTION");


                    AnnotateImageRequest request = new AnnotateImageRequest();
                    request.setImage(inputImage);
                    request.setFeatures(Arrays.asList(Webdetection, desiredFeature, landdetect));

                    BatchAnnotateImagesRequest batchRequest = new BatchAnnotateImagesRequest();
                    batchRequest.setRequests(Arrays.asList(request));

                    BatchAnnotateImagesResponse batchResponse =
                            vision.images().annotate(batchRequest).execute();

                    ArrayList<ModelFile> al_detect = new ArrayList<>();

                    WebDetection web = batchResponse.getResponses().get(0).getWebDetection();
                    //TextAnnotation tax = batchResponse.getResponses().get(0).getFullTextAnnotation();
                    List<EntityAnnotation> tax = batchResponse.getResponses().get(0).getTextAnnotations();
                    List<EntityAnnotation> landmark = batchResponse.getResponses().get(0).getLandmarkAnnotations();

                    al_detect.add(new ModelFile(true, "Web Detect", pos));
                    if (web != null) {
                        List<WebEntity> webEntities = web.getWebEntities();
                        for (int a = 0; a < webEntities.size(); a++) {
                            String det_str = webEntities.get(a).getDescription();
                            al_detect.add(new ModelFile(false, det_str, pos));
                        }
                    }

                   /* if (tax != null) {
                        al_detect.add(new ModelFile(true, "Full Text Detect"));
                        al_detect.add(new ModelFile(false, tax.toString()));
                    }*/
                    al_detect.add(new ModelFile(true, "Text Detect", pos));
                    if (tax != null) {

                        for (int a = 0; a < tax.size(); a++) {
                            String det_str = tax.get(a).getDescription();
                            al_detect.add(new ModelFile(false, det_str, pos));
                        }
                    }

                    al_detect.add(new ModelFile(true, "LandMark Detect", pos));
                    if (landmark != null) {
                        for (int a = 0; a < landmark.size(); a++) {
                            String det_str = landmark.get(a).getDescription();
                            al_detect.add(new ModelFile(false, det_str, pos));
                        }
                    }


                    al_selet_intraRightLateral.get(pos).setAl_detect(al_detect);
                    //al_selet.get(pos).setDescription(description);
                    al_selet_intraRightLateral.get(pos).setIsdetect(true);


                    runOnUiThread(new Runnable() {
                        public void run() {
                            selectFileIntraRightLateralAdapter.setItem(al_selet_intraRightLateral);
                            int c_p = pos;
                            c_p++;
                            if (c_p < al_selet_intraRightLateral.size()) {
                                Detection2(c_p);
                            } else {
                                isdetecting = false;
                                Toast.makeText(Template_AlignersActivity.this, "Image Detected !", Toast.LENGTH_LONG).show();
                            }
                        }
                    });

                } catch (Exception e) {
                    Log.d("ERROR", e.getMessage());
                }
            }
        });
    }

    private void Detection13(final int pos) {
        AsyncTask.execute(new Runnable() {

            @Override
            public void run() {
                try {

                    if (!sm.getIsvisiondetect().equals("1")) return;

                    isdetecting = true;
                    //InputStream inputStream = new FileInputStream("");;
                    if (al_selet_intraLeftLateral.get(pos).isIsdetect()) {

                        runOnUiThread(new Runnable() {
                            public void run() {

                                int c_p = pos;
                                c_p++;
                                if (c_p < al_selet_intraLeftLateral.size()) {
                                    Detection2(c_p);
                                } else {
                                    isdetecting = false;
                                    Toast.makeText(Template_AlignersActivity.this, "Image Detected !", Toast.LENGTH_LONG).show();
                                }

                            }
                        });
                        return;

                    }
                    InputStream inputStream = null;
                    if (al_selet_intraLeftLateral.get(pos).isIsoffline()) {
                        inputStream = new FileInputStream(al_selet_intraLeftLateral.get(pos).getImgpath());
                    } else {
                        inputStream = new URL(al_selet_intraLeftLateral.get(pos).getImgpath()).openStream();
                    }

                    byte[] photoData = org.apache.commons.io.IOUtils.toByteArray(inputStream);

                    com.google.api.services.vision.v1.model.Image inputImage = new com.google.api.services.vision.v1.model.Image();
                    inputImage.encodeContent(photoData);

                   /* Feature landFeature = new Feature();
                    landFeature.setType("LANDMARK_DETECTION");*/


                    Feature Webdetection = new Feature();
                    Webdetection.setType("WEB_DETECTION");
                    //Webdetection.setMaxResults(2);

                    Feature desiredFeature = new Feature();
                    desiredFeature.setType("TEXT_DETECTION");

                    Feature landdetect = new Feature();
                    landdetect.setType("LANDMARK_DETECTION");

                    Feature facedetect = new Feature();
                    facedetect.setType("FACE_DETECTION");


                    AnnotateImageRequest request = new AnnotateImageRequest();
                    request.setImage(inputImage);
                    request.setFeatures(Arrays.asList(Webdetection, desiredFeature, landdetect));

                    BatchAnnotateImagesRequest batchRequest = new BatchAnnotateImagesRequest();
                    batchRequest.setRequests(Arrays.asList(request));

                    BatchAnnotateImagesResponse batchResponse =
                            vision.images().annotate(batchRequest).execute();

                    ArrayList<ModelFile> al_detect = new ArrayList<>();

                    WebDetection web = batchResponse.getResponses().get(0).getWebDetection();
                    //TextAnnotation tax = batchResponse.getResponses().get(0).getFullTextAnnotation();
                    List<EntityAnnotation> tax = batchResponse.getResponses().get(0).getTextAnnotations();
                    List<EntityAnnotation> landmark = batchResponse.getResponses().get(0).getLandmarkAnnotations();

                    al_detect.add(new ModelFile(true, "Web Detect", pos));
                    if (web != null) {
                        List<WebEntity> webEntities = web.getWebEntities();
                        for (int a = 0; a < webEntities.size(); a++) {
                            String det_str = webEntities.get(a).getDescription();
                            al_detect.add(new ModelFile(false, det_str, pos));
                        }
                    }

                   /* if (tax != null) {
                        al_detect.add(new ModelFile(true, "Full Text Detect"));
                        al_detect.add(new ModelFile(false, tax.toString()));
                    }*/
                    al_detect.add(new ModelFile(true, "Text Detect", pos));
                    if (tax != null) {

                        for (int a = 0; a < tax.size(); a++) {
                            String det_str = tax.get(a).getDescription();
                            al_detect.add(new ModelFile(false, det_str, pos));
                        }
                    }

                    al_detect.add(new ModelFile(true, "LandMark Detect", pos));
                    if (landmark != null) {
                        for (int a = 0; a < landmark.size(); a++) {
                            String det_str = landmark.get(a).getDescription();
                            al_detect.add(new ModelFile(false, det_str, pos));
                        }
                    }


                    al_selet_intraLeftLateral.get(pos).setAl_detect(al_detect);
                    //al_selet.get(pos).setDescription(description);
                    al_selet_intraLeftLateral.get(pos).setIsdetect(true);


                    runOnUiThread(new Runnable() {
                        public void run() {
                            selectFileIntraLeftLateralAdapter.setItem(al_selet_intraLeftLateral);
                            int c_p = pos;
                            c_p++;
                            if (c_p < al_selet_intraLeftLateral.size()) {
                                Detection2(c_p);
                            } else {
                                isdetecting = false;
                                Toast.makeText(Template_AlignersActivity.this, "Image Detected !", Toast.LENGTH_LONG).show();
                            }
                        }
                    });

                } catch (Exception e) {
                    Log.d("ERROR", e.getMessage());
                }
            }
        });
    }

    private void Detection14(final int pos) {
        AsyncTask.execute(new Runnable() {

            @Override
            public void run() {
                try {

                    if (!sm.getIsvisiondetect().equals("1")) return;

                    isdetecting = true;
                    //InputStream inputStream = new FileInputStream("");;
                    if (al_selet_intraUpperOcclusal.get(pos).isIsdetect()) {

                        runOnUiThread(new Runnable() {
                            public void run() {

                                int c_p = pos;
                                c_p++;
                                if (c_p < al_selet_intraUpperOcclusal.size()) {
                                    Detection2(c_p);
                                } else {
                                    isdetecting = false;
                                    Toast.makeText(Template_AlignersActivity.this, "Image Detected !", Toast.LENGTH_LONG).show();
                                }

                            }
                        });
                        return;

                    }
                    InputStream inputStream = null;
                    if (al_selet_intraUpperOcclusal.get(pos).isIsoffline()) {
                        inputStream = new FileInputStream(al_selet_intraUpperOcclusal.get(pos).getImgpath());
                    } else {
                        inputStream = new URL(al_selet_intraUpperOcclusal.get(pos).getImgpath()).openStream();
                    }

                    byte[] photoData = org.apache.commons.io.IOUtils.toByteArray(inputStream);

                    com.google.api.services.vision.v1.model.Image inputImage = new com.google.api.services.vision.v1.model.Image();
                    inputImage.encodeContent(photoData);

                   /* Feature landFeature = new Feature();
                    landFeature.setType("LANDMARK_DETECTION");*/


                    Feature Webdetection = new Feature();
                    Webdetection.setType("WEB_DETECTION");
                    //Webdetection.setMaxResults(2);

                    Feature desiredFeature = new Feature();
                    desiredFeature.setType("TEXT_DETECTION");

                    Feature landdetect = new Feature();
                    landdetect.setType("LANDMARK_DETECTION");

                    Feature facedetect = new Feature();
                    facedetect.setType("FACE_DETECTION");


                    AnnotateImageRequest request = new AnnotateImageRequest();
                    request.setImage(inputImage);
                    request.setFeatures(Arrays.asList(Webdetection, desiredFeature, landdetect));

                    BatchAnnotateImagesRequest batchRequest = new BatchAnnotateImagesRequest();
                    batchRequest.setRequests(Arrays.asList(request));

                    BatchAnnotateImagesResponse batchResponse =
                            vision.images().annotate(batchRequest).execute();

                    ArrayList<ModelFile> al_detect = new ArrayList<>();

                    WebDetection web = batchResponse.getResponses().get(0).getWebDetection();
                    //TextAnnotation tax = batchResponse.getResponses().get(0).getFullTextAnnotation();
                    List<EntityAnnotation> tax = batchResponse.getResponses().get(0).getTextAnnotations();
                    List<EntityAnnotation> landmark = batchResponse.getResponses().get(0).getLandmarkAnnotations();

                    al_detect.add(new ModelFile(true, "Web Detect", pos));
                    if (web != null) {
                        List<WebEntity> webEntities = web.getWebEntities();
                        for (int a = 0; a < webEntities.size(); a++) {
                            String det_str = webEntities.get(a).getDescription();
                            al_detect.add(new ModelFile(false, det_str, pos));
                        }
                    }

                   /* if (tax != null) {
                        al_detect.add(new ModelFile(true, "Full Text Detect"));
                        al_detect.add(new ModelFile(false, tax.toString()));
                    }*/
                    al_detect.add(new ModelFile(true, "Text Detect", pos));
                    if (tax != null) {

                        for (int a = 0; a < tax.size(); a++) {
                            String det_str = tax.get(a).getDescription();
                            al_detect.add(new ModelFile(false, det_str, pos));
                        }
                    }

                    al_detect.add(new ModelFile(true, "LandMark Detect", pos));
                    if (landmark != null) {
                        for (int a = 0; a < landmark.size(); a++) {
                            String det_str = landmark.get(a).getDescription();
                            al_detect.add(new ModelFile(false, det_str, pos));
                        }
                    }


                    al_selet_intraUpperOcclusal.get(pos).setAl_detect(al_detect);
                    //al_selet.get(pos).setDescription(description);
                    al_selet_intraUpperOcclusal.get(pos).setIsdetect(true);


                    runOnUiThread(new Runnable() {
                        public void run() {
                            selectFileIntraUpperOcclusalAdapter.setItem(al_selet_intraUpperOcclusal);
                            int c_p = pos;
                            c_p++;
                            if (c_p < al_selet_intraUpperOcclusal.size()) {
                                Detection2(c_p);
                            } else {
                                isdetecting = false;
                                Toast.makeText(Template_AlignersActivity.this, "Image Detected !", Toast.LENGTH_LONG).show();
                            }
                        }
                    });

                } catch (Exception e) {
                    Log.d("ERROR", e.getMessage());
                }
            }
        });
    }

    private void Detection15(final int pos) {
        AsyncTask.execute(new Runnable() {

            @Override
            public void run() {
                try {

                    if (!sm.getIsvisiondetect().equals("1")) return;

                    isdetecting = true;
                    //InputStream inputStream = new FileInputStream("");;
                    if (al_selet_intraLeftOcclusal.get(pos).isIsdetect()) {

                        runOnUiThread(new Runnable() {
                            public void run() {

                                int c_p = pos;
                                c_p++;
                                if (c_p < al_selet_intraLeftOcclusal.size()) {
                                    Detection2(c_p);
                                } else {
                                    isdetecting = false;
                                    Toast.makeText(Template_AlignersActivity.this, "Image Detected !", Toast.LENGTH_LONG).show();
                                }

                            }
                        });
                        return;

                    }
                    InputStream inputStream = null;
                    if (al_selet_intraLeftOcclusal.get(pos).isIsoffline()) {
                        inputStream = new FileInputStream(al_selet_intraLeftOcclusal.get(pos).getImgpath());
                    } else {
                        inputStream = new URL(al_selet_intraLeftOcclusal.get(pos).getImgpath()).openStream();
                    }

                    byte[] photoData = org.apache.commons.io.IOUtils.toByteArray(inputStream);

                    com.google.api.services.vision.v1.model.Image inputImage = new com.google.api.services.vision.v1.model.Image();
                    inputImage.encodeContent(photoData);

                   /* Feature landFeature = new Feature();
                    landFeature.setType("LANDMARK_DETECTION");*/


                    Feature Webdetection = new Feature();
                    Webdetection.setType("WEB_DETECTION");
                    //Webdetection.setMaxResults(2);

                    Feature desiredFeature = new Feature();
                    desiredFeature.setType("TEXT_DETECTION");

                    Feature landdetect = new Feature();
                    landdetect.setType("LANDMARK_DETECTION");

                    Feature facedetect = new Feature();
                    facedetect.setType("FACE_DETECTION");


                    AnnotateImageRequest request = new AnnotateImageRequest();
                    request.setImage(inputImage);
                    request.setFeatures(Arrays.asList(Webdetection, desiredFeature, landdetect));

                    BatchAnnotateImagesRequest batchRequest = new BatchAnnotateImagesRequest();
                    batchRequest.setRequests(Arrays.asList(request));

                    BatchAnnotateImagesResponse batchResponse =
                            vision.images().annotate(batchRequest).execute();

                    ArrayList<ModelFile> al_detect = new ArrayList<>();

                    WebDetection web = batchResponse.getResponses().get(0).getWebDetection();
                    //TextAnnotation tax = batchResponse.getResponses().get(0).getFullTextAnnotation();
                    List<EntityAnnotation> tax = batchResponse.getResponses().get(0).getTextAnnotations();
                    List<EntityAnnotation> landmark = batchResponse.getResponses().get(0).getLandmarkAnnotations();

                    al_detect.add(new ModelFile(true, "Web Detect", pos));
                    if (web != null) {
                        List<WebEntity> webEntities = web.getWebEntities();
                        for (int a = 0; a < webEntities.size(); a++) {
                            String det_str = webEntities.get(a).getDescription();
                            al_detect.add(new ModelFile(false, det_str, pos));
                        }
                    }

                   /* if (tax != null) {
                        al_detect.add(new ModelFile(true, "Full Text Detect"));
                        al_detect.add(new ModelFile(false, tax.toString()));
                    }*/
                    al_detect.add(new ModelFile(true, "Text Detect", pos));
                    if (tax != null) {

                        for (int a = 0; a < tax.size(); a++) {
                            String det_str = tax.get(a).getDescription();
                            al_detect.add(new ModelFile(false, det_str, pos));
                        }
                    }

                    al_detect.add(new ModelFile(true, "LandMark Detect", pos));
                    if (landmark != null) {
                        for (int a = 0; a < landmark.size(); a++) {
                            String det_str = landmark.get(a).getDescription();
                            al_detect.add(new ModelFile(false, det_str, pos));
                        }
                    }


                    al_selet_intraLeftOcclusal.get(pos).setAl_detect(al_detect);
                    //al_selet.get(pos).setDescription(description);
                    al_selet_intraLeftOcclusal.get(pos).setIsdetect(true);


                    runOnUiThread(new Runnable() {
                        public void run() {
                            selectFileIntraLeftOcclusalAdapter.setItem(al_selet_intraLeftOcclusal);
                            int c_p = pos;
                            c_p++;
                            if (c_p < al_selet_intraLeftOcclusal.size()) {
                                Detection2(c_p);
                            } else {
                                isdetecting = false;
                                Toast.makeText(Template_AlignersActivity.this, "Image Detected !", Toast.LENGTH_LONG).show();
                            }
                        }
                    });

                } catch (Exception e) {
                    Log.d("ERROR", e.getMessage());
                }
            }
        });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.nextStepOne:
                if (TextUtils.isEmpty(et_name.getText().toString())) {
                    Toast.makeText(this, "Enter Patient Name", Toast.LENGTH_SHORT).show();
                } else if (TextUtils.isEmpty(et_age.getText().toString())) {
                    Toast.makeText(this, "Enter Patient Age", Toast.LENGTH_SHORT).show();
                } else {
                    rlStepOne.setVisibility(View.GONE);
                    rlStepThree.setVisibility(View.VISIBLE);
                    rlStepFour.setVisibility(View.GONE);
                    rlStepFive.setVisibility(View.GONE);
                    usage_stateprogressbar.setCurrentStateNumber(StateProgressBar.StateNumber.TWO);
                }
                break;

            case R.id.nextStepThree:

                rlStepOne.setVisibility(View.GONE);
                rlStepThree.setVisibility(View.GONE);
                rlStepFour.setVisibility(View.VISIBLE);
                rlStepFive.setVisibility(View.GONE);
                usage_stateprogressbar.setCurrentStateNumber(StateProgressBar.StateNumber.THREE);

                break;

            case R.id.nextStepFour:
                rlStepOne.setVisibility(View.GONE);
                rlStepThree.setVisibility(View.GONE);
                rlStepFour.setVisibility(View.GONE);
                rlStepFive.setVisibility(View.VISIBLE);
                usage_stateprogressbar.setCurrentStateNumber(StateProgressBar.StateNumber.FOUR);
                Detail();
                break;

            case R.id.nextStepFive:
                imageUpload(0, 0);
                // Log.d("teethDetail", new Gson().toJson(teethDetail));
                break;
        }
    }

    @Override
    public void onBackPressed() {

        if (rl_upload.getVisibility() == View.VISIBLE) {

        } else {
            if (rlStepOne.getVisibility() == View.VISIBLE) {
                finish();
            } else if (rlStepThree.getVisibility() == View.VISIBLE) {
                rlStepOne.setVisibility(View.VISIBLE);
                rlStepThree.setVisibility(View.GONE);
                rlStepFour.setVisibility(View.GONE);
                rlStepFive.setVisibility(View.GONE);
                usage_stateprogressbar.setCurrentStateNumber(StateProgressBar.StateNumber.ONE);
            } else if (rlStepFour.getVisibility() == View.VISIBLE) {
                rlStepOne.setVisibility(View.GONE);
                rlStepThree.setVisibility(View.VISIBLE);
                rlStepFour.setVisibility(View.GONE);
                rlStepFive.setVisibility(View.GONE);
                usage_stateprogressbar.setCurrentStateNumber(StateProgressBar.StateNumber.TWO);
            } else if (rlStepFive.getVisibility() == View.VISIBLE) {
                rlStepOne.setVisibility(View.GONE);
                rlStepThree.setVisibility(View.GONE);
                rlStepFour.setVisibility(View.VISIBLE);
                rlStepFive.setVisibility(View.GONE);
                usage_stateprogressbar.setCurrentStateNumber(StateProgressBar.StateNumber.THREE);
            } else {
                finish();
            }
        }
    }


    Dental dental = new Dental();

    public void Detail() {

        if (sex.equals("Male")) {
            rbMale.setChecked(true);
        } else {
            rbFemale.setChecked(true);
        }

        tvName.setText(et_name.getText().toString());
        dental.setName(tvName.getText().toString());

        tvAge.setText(et_age.getText().toString());
        dental.setAge(tvAge.getText().toString());

        tvSex.setText(sex);
        dental.setSex(sex);

        dental.setTeethList(teethList);
        dental.setTeeth(teeth);

        preference = new StringBuilder();

        dental.setPreference(preference.toString());


        // dental.setShade("");


        if (cb_trays.isChecked() || al_selet_tray.size() > 0) {
            dental.isTray = true;
            llTray.setVisibility(View.VISIBLE);
            adapter = new TeethImageAdapter(al_selet_tray, this);
            rvTraysImage.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
            rvTraysImage.setAdapter(adapter);
        } else {
            dental.isTray = false;
            llTray.setVisibility(View.GONE);
        }
        dental.setAl_selet_tray(al_selet_tray);


        if (cb_model.isChecked() || al_selet_model.size() > 0) {
            dental.isModel = true;
            llModel.setVisibility(View.VISIBLE);
            adapter = new TeethImageAdapter(al_selet_model, this);
            rvModelsImage.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
            rvModelsImage.setAdapter(adapter);
        } else {
            dental.isModel = false;
            llModel.setVisibility(View.GONE);
        }
        dental.setAl_selet_model(al_selet_model);

        if (cb_bite.isChecked() || al_selet_bite.size() > 0) {
            dental.isBite = true;
            llBite.setVisibility(View.VISIBLE);
            adapter = new TeethImageAdapter(al_selet_bite, this);
            rvBiteImage.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
            rvBiteImage.setAdapter(adapter);
        } else {
            dental.isBite = false;
            llBite.setVisibility(View.GONE);
        }
        dental.setAl_selet_bite(al_selet_bite);

        if (cb_Frontal.isChecked() || al_selet_frontal.size() > 0) {
            dental.isFrontal = true;
            llFrontal.setVisibility(View.VISIBLE);
            adapter = new TeethImageAdapter(al_selet_frontal, this);
            rvFrontalImage.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
            rvFrontalImage.setAdapter(adapter);
        } else {
            dental.isFrontal = false;
            llFrontal.setVisibility(View.GONE);
        }
        dental.setAl_selet_frontal(al_selet_frontal);

        if (cb_FrontalSmile.isChecked() || al_selet_frontalSmile.size() > 0) {
            dental.isFrontalSmile = true;
            llFrontalSmile.setVisibility(View.VISIBLE);
            adapter = new TeethImageAdapter(al_selet_frontalSmile, this);
            rvFrontalSmileImage.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
            rvFrontalSmileImage.setAdapter(adapter);
        } else {
            dental.isFrontalSmile = false;
            llFrontalSmile.setVisibility(View.GONE);
        }
        dental.setAl_selet_frontalSmile(al_selet_frontalSmile);

        if (cb_LeftLateral.isChecked() || al_selet_leftLateral.size() > 0) {
            dental.isLeftLateral = true;
            llLeftLateral.setVisibility(View.VISIBLE);
            adapter = new TeethImageAdapter(al_selet_leftLateral, this);
            rvLeftLateralImage.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
            rvLeftLateralImage.setAdapter(adapter);
        } else {
            dental.isLeftLateral = false;
            llLeftLateral.setVisibility(View.GONE);
        }
        dental.setAl_selet_leftLateral(al_selet_leftLateral);

        if (cb_LeftLateralSmile.isChecked() || al_selet_leftLateralSmile.size() > 0) {
            dental.isLeftLateralSmile = true;
            llLeftLateralSmile.setVisibility(View.VISIBLE);
            adapter = new TeethImageAdapter(al_selet_leftLateralSmile, this);
            rvLeftLateralSmileImage.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
            rvLeftLateralSmileImage.setAdapter(adapter);
        } else {
            dental.isLeftLateralSmile = false;
            llLeftLateralSmile.setVisibility(View.GONE);
        }
        dental.setAl_selet_leftLateralSmile(al_selet_leftLateralSmile);

        if (cb_RightLateral.isChecked() || al_selet_rightLateral.size() > 0) {
            dental.isRightLateral = true;
            llRightLateral.setVisibility(View.VISIBLE);
            adapter = new TeethImageAdapter(al_selet_rightLateral, this);
            rvRightLateralImage.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
            rvRightLateralImage.setAdapter(adapter);
        } else {
            dental.isRightLateral = false;
            llRightLateral.setVisibility(View.GONE);
        }
        dental.setAl_selet_rightLateral(al_selet_rightLateral);

        if (cb_RightLateralSmile.isChecked() || al_selet_rightLateralSmile.size() > 0) {
            dental.isRightLateralSmile = true;
            llRightLateralSmile.setVisibility(View.VISIBLE);
            adapter = new TeethImageAdapter(al_selet_rightLateralSmile, this);
            rvRightLateralSmileImage.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
            rvRightLateralSmileImage.setAdapter(adapter);
        } else {
            dental.isRightLateralSmile = false;
            llRightLateralSmile.setVisibility(View.GONE);
        }
        dental.setAl_selet_rightLateralSmile(al_selet_rightLateralSmile);

        if (cb_IntraFrontal.isChecked() || al_selet_intraFrontal.size() > 0) {
            dental.isIntraFrontal = true;
            llIntraFrontal.setVisibility(View.VISIBLE);
            adapter = new TeethImageAdapter(al_selet_intraFrontal, this);
            rvIntraFrontalImage.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
            rvIntraFrontalImage.setAdapter(adapter);
        } else {
            dental.isIntraFrontal = false;
            llIntraFrontal.setVisibility(View.GONE);
        }
        dental.setAl_selet_intraFrontal(al_selet_intraFrontal);

        if (cb_IntraRightLateral.isChecked() || al_selet_intraRightLateral.size() > 0) {
            dental.isIntraRightLateral = true;
            llIntraRightLateral.setVisibility(View.VISIBLE);
            adapter = new TeethImageAdapter(al_selet_intraRightLateral, this);
            rvIntraRightLateralImage.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
            rvIntraRightLateralImage.setAdapter(adapter);
        } else {
            dental.isIntraRightLateral = false;
            llIntraRightLateral.setVisibility(View.GONE);
        }
        dental.setAl_selet_intraRightLateral(al_selet_intraRightLateral);

        if (cb_IntraLeftLateral.isChecked() || al_selet_intraLeftLateral.size() > 0) {
            dental.isIntraLeftLateral = true;
            llIntraLeftLateral.setVisibility(View.VISIBLE);
            adapter = new TeethImageAdapter(al_selet_intraLeftLateral, this);
            rvIntraLeftLateralImage.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
            rvIntraLeftLateralImage.setAdapter(adapter);
        } else {
            dental.isIntraLeftLateral = false;
            llIntraLeftLateral.setVisibility(View.GONE);
        }
        dental.setAl_selet_intraLeftLateral(al_selet_intraLeftLateral);

        if (cb_IntraUpperOcclusal.isChecked() || al_selet_intraUpperOcclusal.size() > 0) {
            dental.isIntraUpperOcclusal = true;
            llIntraUpperOcclusal.setVisibility(View.VISIBLE);
            adapter = new TeethImageAdapter(al_selet_intraUpperOcclusal, this);
            rvIntraUpperOcclusalImage.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
            rvIntraUpperOcclusalImage.setAdapter(adapter);
        } else {
            dental.isIntraUpperOcclusal = false;
            llIntraUpperOcclusal.setVisibility(View.GONE);
        }
        dental.setAl_selet_intraUpperOcclusal(al_selet_intraUpperOcclusal);

        if (cb_IntraLeftOcclusal.isChecked() || al_selet_intraLeftOcclusal.size() > 0) {
            dental.isIntraLeftOcclusal = true;
            llIntraLeftOcclusal.setVisibility(View.VISIBLE);
            adapter = new TeethImageAdapter(al_selet_intraLeftOcclusal, this);
            rvIntraLeftOcclusalImage.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
            rvIntraLeftOcclusalImage.setAdapter(adapter);
        } else {
            dental.isIntraLeftOcclusal = false;
            llIntraLeftOcclusal.setVisibility(View.GONE);
        }
        dental.setAl_selet_intraLeftOcclusal(al_selet_intraLeftOcclusal);

        dental.setAl_selet_temp(al_selet_temp);

        tvChief.setText(et_Chief.getText().toString());
        dental.setChief_complaint(tvChief.getText().toString());

        tvTreatment.setText(et_Treatment.getText().toString());
        dental.setTreatment(tvTreatment.getText().toString());

        teethDetail.add(dental);
    }

    ArrayList<ModelFile> imageList = new ArrayList<>();
    String msg;

    public void imageUpload(final int arrayPos, final int uploadIndexPos) {

        rl_upload.setVisibility(View.VISIBLE);
        imageList = new ArrayList<>();
        msg = "";
        switch (arrayPos) {
            case 0:
                msg = "Uploading Shade ";
                imageList = al_selet;
                break;
            case 1:
                msg = "Uploading Tray ";
                imageList = al_selet_tray;
                break;
            case 2:
                msg = "Uploading Model ";
                imageList = al_selet_model;
                break;
            case 3:
                msg = "Uploading Bite";
                imageList = al_selet_bite;
                break;
            case 5:
                msg = "Uploading Frontal";
                imageList = al_selet_frontal;
                break;
            case 6:
                msg = "Uploading Frontal Smile";
                imageList = al_selet_frontalSmile;
                break;
            case 7:
                msg = "Uploading Left Lateral";
                imageList = al_selet_leftLateral;
                break;
            case 8:
                msg = "Uploading Left Lateral Smile";
                imageList = al_selet_leftLateralSmile;
                break;
            case 9:
                msg = "Uploading Right Lateral";
                imageList = al_selet_rightLateral;
                break;
            case 10:
                msg = "Uploading Right Lateral Smile";
                imageList = al_selet_rightLateralSmile;
                break;
            case 11:
                msg = "Uploading Intra Frontal";
                imageList = al_selet_intraFrontal;
                break;
            case 12:
                msg = "Uploading Intra Right lateral";
                imageList = al_selet_intraRightLateral;
                break;
            case 13:
                msg = "Uploading Intra Left Lateral";
                imageList = al_selet_intraLeftLateral;
                break;
            case 14:
                msg = "Uploading Upper Occlusal";
                imageList = al_selet_intraUpperOcclusal;
                break;
            case 15:
                msg = "Uploading Left Occlusal";
                imageList = al_selet_intraLeftOcclusal;
                break;

        }

        if (uploadIndexPos < imageList.size()) {

            int k = uploadIndexPos + 1;
            msg = msg + " " + k + "/" + imageList.size();
            tv_progress.setText(msg);
            MyRequestCall myRequestCall = new MyRequestCall();

            if (!imageList.get(uploadIndexPos).isIsoffline()) {

                imageUpload(arrayPos, k);
                return;
            } else {

                myRequestCall.uploadAwsS3(sessionManager.getcurrentu_nm(), Template_AlignersActivity.this, imageList.get(uploadIndexPos).getImgpath(), new MyRequestCall.CallRequest() {
                    @Override
                    public void onGetResponse(String response) {

                        int t = uploadIndexPos;

                        if (!response.isEmpty()) {

                            imageList.get(uploadIndexPos).setImgpath(response);
                            imageList.get(uploadIndexPos).setIsoffline(false);

                            switch (arrayPos) {
                                case 0:
                                    al_selet = imageList;
                                    break;
                                case 1:
                                    al_selet_tray = imageList;
                                    break;
                                case 2:
                                    al_selet_model = imageList;
                                    break;
                                case 3:
                                    al_selet_bite = imageList;
                                    break;
                                case 5:
                                    al_selet_frontal = imageList;
                                    break;
                                case 6:
                                    al_selet_frontalSmile = imageList;
                                    break;
                                case 7:
                                    al_selet_leftLateral = imageList;
                                    break;
                                case 8:
                                    al_selet_leftLateralSmile = imageList;
                                    break;
                                case 9:
                                    al_selet_rightLateral = imageList;
                                    break;
                                case 10:
                                    al_selet_rightLateralSmile = imageList;
                                    break;
                                case 11:
                                    al_selet_intraFrontal = imageList;
                                    break;
                                case 12:
                                    al_selet_intraRightLateral = imageList;
                                    break;
                                case 13:
                                    al_selet_intraLeftLateral = imageList;
                                    break;
                                case 14:
                                    al_selet_intraUpperOcclusal = imageList;
                                    break;
                                case 15:
                                    al_selet_intraLeftOcclusal = imageList;
                                    break;
                            }

                            t++;
                            imageUpload(arrayPos, t);
                            //  callUpdateBuisenssProfile();
                        }
                    }
                });
            }

        } else {
            int t = arrayPos;
            t++;

            if (t < 16) {
                imageUpload(t, 0);
            } else {

                dental.setAl_selet(al_selet);
                dental.setAl_selet_bite(al_selet_bite);
                dental.setAl_selet_model(al_selet_model);
                dental.setAl_selet_temp(al_selet_temp);
                dental.setAl_selet_tray(al_selet_tray);
                dental.setAl_selet_frontal(al_selet_frontal);
                dental.setAl_selet_frontalSmile(al_selet_frontalSmile);
                dental.setAl_selet_leftLateral(al_selet_leftLateral);
                dental.setAl_selet_leftLateralSmile(al_selet_leftLateralSmile);
                dental.setAl_selet_rightLateral(al_selet_rightLateral);
                dental.setAl_selet_rightLateralSmile(al_selet_rightLateralSmile);
                dental.setAl_selet_intraFrontal(al_selet_intraFrontal);
                dental.setAl_selet_intraRightLateral(al_selet_intraRightLateral);
                dental.setAl_selet_intraLeftLateral(al_selet_intraLeftLateral);
                dental.setAl_selet_intraUpperOcclusal(al_selet_intraUpperOcclusal);
                dental.setAl_selet_intraLeftOcclusal(al_selet_intraLeftOcclusal);

                rl_upload.setVisibility(View.GONE);

                insertCart();
                //Toast.makeText(this, "All Image Are Has Been Uploaded", Toast.LENGTH_SHORT).show();

               /* callCreateChit(this, new CallRequest() {
                    @Override
                    public void onGetResponse(String response) {

                        sessionManager.setcurrentcaterer("");
                        sessionManager.setcurrentcaterername("");
                        sessionManager.setCartproduct("");
                        sessionManager.setCartcount(0);
                        sessionManager.setRemark("");
                        sessionManager.setDate("");
                        sessionManager.setDobForSurvey("");
                        sessionManager.setMobile("");
                        sessionManager.setAddress("");
                        SqlLiteDbHelper dbHelper = new SqlLiteDbHelper(Template_AlignersActivity.this);
                        dbHelper.deleteallfromcart();
                        sessionManager.setAggregator_ID(getString(R.string.Aggregator));
                        SharedPrefUserDetail.setString(Template_AlignersActivity.this, SharedPrefUserDetail.isfrommyaggregator, "");

                        finish();
                        Intent i = new Intent(Template_AlignersActivity.this, HistoryActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        i.putExtra("session", sessionManager.getsession());
                        startActivity(i);

                    }
                }, frombridgeidval, dental);*/
            }
        }
    }

    SqlLiteDbHelper dbHelper;

    public void insertCart() {
        dbHelper = new SqlLiteDbHelper(this);

        String internal_json = catelog.getInternal_json_data();

        try {
            JSONObject joInternalJson = new JSONObject(internal_json);
            joInternalJson.put("dental", new Gson().toJson(dental));
            catelog.setInternal_json_data(joInternalJson.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (catelog.getCartcount() == 0) {
            catelog.setCartcount(1);
            Toast.makeText(Template_AlignersActivity.this, "Report added", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(Template_AlignersActivity.this, "Report updated", Toast.LENGTH_SHORT).show();

        }

        String[] teethList = dental.getTeeth().toString().split(",");
        productcount = teethList.length;
        catelog.setCartcount(productcount);

        Apputil.inserttocart(dbHelper, catelog, CatelogActivity.aliasname);
        nextStepFive.setText("UPDATE");

        finish();
    }
}