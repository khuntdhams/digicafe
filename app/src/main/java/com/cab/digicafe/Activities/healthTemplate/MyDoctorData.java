package com.cab.digicafe.Activities.healthTemplate;

import com.cab.digicafe.Model.templateModel;
import com.cab.digicafe.R;

import java.util.ArrayList;

public class MyDoctorData {

    public ArrayList<templateModel> getTemplateList() {
        templateModel template = new templateModel(1, "Crown/FPD", Template_DentalActivity.class, R.drawable.place, "", true);
        templateList.add(template);

        template = new templateModel(2, "CD", Template_CdActivity.class, R.drawable.place, "", false);
        templateList.add(template);

        template = new templateModel(3, "Precision Attachment", Template_precisionActivity.class, R.drawable.place, "", false);
        templateList.add(template);

        template = new templateModel(4, "Ortho Appliance", Template_OrthoActivity.class, R.drawable.place, "", false);
        templateList.add(template);

        template = new templateModel(5, "Implant", Template_ImplantActivity.class, R.drawable.place, "", false);
        templateList.add(template);

        template = new templateModel(6, "Aligners", Template_AlignersActivity.class, R.drawable.place, "", false);
        templateList.add(template);

        template = new templateModel(7, "Sports Guard", Template_SportActivity.class, R.drawable.place, "", false);
        templateList.add(template);

        template = new templateModel(8, "OSA Appliance", Template_OsaActivity.class, R.drawable.place, "", false);
        templateList.add(template);

        template = new templateModel(9, "RPD", Template_RpdActivity.class, R.drawable.place, "", false);
        templateList.add(template);

        template = new templateModel(10, "None", null, R.drawable.place, "", false);
        templateList.add(template);

        return templateList;
    }

    public ArrayList<templateModel> getList() {

        return templateList;
    }

    public void setTemplateList(ArrayList<templateModel> templateList) {


        this.templateList = templateList;
    }

    ArrayList<templateModel> templateList = new ArrayList<>();

}
