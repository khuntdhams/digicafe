package com.cab.digicafe.Activities.healthTemplate;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.cab.digicafe.Helper.SessionManager;
import com.cab.digicafe.Helper.SharedPrefUserDetail;
import com.cab.digicafe.Model.Dental;
import com.cab.digicafe.Model.Userprofile;
import com.cab.digicafe.MyCustomClass.JsonObjParse;
import com.cab.digicafe.R;
import com.cab.digicafe.Rest.ApiClient;
import com.cab.digicafe.Rest.ApiInterface;
import com.cab.digicafe.Timeline.DateTimeUtils;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HealthTemplateBaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_health_template_base);
        overridePendingTransition(R.anim.fadein, R.anim.fadeout);
    }

    public interface CallRequest {
        public void onGetResponse(String response);
    }

    String headernotes = "", frombridgeidval = "";
    CallRequest callRequestForSurvey;

    private ProgressDialog dialog;


    Dental dental;
    public void callCreateChit(final AppCompatActivity c, final CallRequest callRequest, String frombridgeidval, Dental d) {


        this.dental = d;
        callRequestForSurvey = callRequest;

        this.frombridgeidval = frombridgeidval;

        final SessionManager sessionManager = new SessionManager(c);
        final SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy hh:mm aaa");
        final Calendar cal_order = Calendar.getInstance();

        //headernotes = remark;
        headernotes = dental.getInstructioon();
        final String date_ = df.format(cal_order.getTime());

        sessionManager.setDate(date_);
        sessionManager.setAddress("###");
        sessionManager.setRemark(headernotes);
        sessionManager.setT_Phone("###");

        try {
            userdetails = new JSONObject(sessionManager.getUserprofile());
            loggedin = new Gson().fromJson(userdetails.toString(), Userprofile.class);

        } catch (JSONException e) {
            e.printStackTrace();
        }


        usersignin(c, sessionManager.getsession(), date_, "###");

    }


    Userprofile loggedin;
    JSONObject userdetails;
    int productcount;


    String field_json_data = "", field_json_data_info = "", business_type_info = "";

    Double price = 0.0;
    String purpose = "Dental";

    public void usersignin(final Context c, final String usersession, final String dt, final String address) {

        dialog = new ProgressDialog(c);
        dialog.setMessage("Sending...");
        dialog.show();

        final SessionManager sessionManager = new SessionManager(c);

        field_json_data = SharedPrefUserDetail.getString(c, SharedPrefUserDetail.field_json_data, "");
        field_json_data_info = SharedPrefUserDetail.getString(c, SharedPrefUserDetail.field_json_data_info, "");
        business_type_info = SharedPrefUserDetail.getString(c, SharedPrefUserDetail.business_type, "");


        String[] teethList = dental.getTeeth().toString().split(",");
        productcount = teethList.length;

        final JsonObject sendobj = new JsonObject();
        JsonArray a = new JsonArray();
        JsonArray cartproduct = new JsonArray();

        JsonObject productsync = new JsonObject();

        String particular = "Dental; "+dental.getTeeth().toString();

        price = 0.0;

        productsync.addProperty("entry_id", "#");
        productsync.addProperty("bridge_id", "#");
        productsync.addProperty("flag", "add");

        productsync.addProperty("quantity", productcount);

        productsync.addProperty("particulars", particular);
        //productsync.addProperty("price", String.valueOf(cal_price(i)));
        productsync.addProperty("price", "###");
        productsync.addProperty("offer", "");
        cartproduct.add(productsync);


        try {
            JsonObject a1 = new JsonObject();

            a1.addProperty("email", loggedin.getEmail_id());
            a1.addProperty("contact_number", loggedin.getContact_no());
            a1.addProperty("location", loggedin.getLocation());
            a1.addProperty("chit_name", loggedin.getFirstname());
            a1.addProperty("header_note", headernotes);

            //a1.addProperty("footer_note", jo_footer.toString());
            sendobj.add("newdata", cartproduct);
            sendobj.add("olddata", a);
            sendobj.add("chitHeader", a1);

        } catch (JsonParseException e) {
            e.printStackTrace();
        }

        //price = price / dbHelper.getcartcount();

        ApiInterface apiService =
                ApiClient.getClient(c).create(ApiInterface.class);
        String currency_code = SharedPrefUserDetail.getString(c, SharedPrefUserDetail.chit_currency_code, "");
        String currency_code_id = SharedPrefUserDetail.getString(c, SharedPrefUserDetail.chit_currency_code_id, "");

        Call call = apiService.createchit(usersession, sendobj, sessionManager.getAggregatorprofile(), frombridgeidval, "d97be841a6d6cbb66bc3ae165b31b85f", currency_code, currency_code_id, "2");
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {

                if (response.isSuccessful()) {
                    try {
                        String a = new Gson().toJson(response.body());
                        Log.e("response", a);
                        JSONObject responseobj = new JSONObject(a);
                        JSONObject responseval = responseobj.getJSONObject("response");

                        JSONObject data = responseval.getJSONObject("data");
                        String chithashid = data.getString("chitHashId");
                        try {
                            JsonObject jObj = new JsonObject();
                            jObj.addProperty("email", loggedin.getEmail_id());
                            jObj.addProperty("contact_number", sessionManager.getT_Phone());
                            jObj.addProperty("location", sessionManager.getAddress());
                            jObj.addProperty("act", sessionManager.getcurrentcaterername());
                            jObj.addProperty("chit_name", "");
                            jObj.addProperty("chitHashId", chithashid);
                            jObj.addProperty("latitude", "");
                            jObj.addProperty("longtitude", "");
                            jObj.addProperty("expected_delivery_time", "");
                            jObj.addProperty("for_non_bridge_name", "");
                            jObj.addProperty("subject", sessionManager.getcurrentcatereraliasname());

                            String currency_code = SharedPrefUserDetail.getString(c, SharedPrefUserDetail.chit_currency_code, "");
                            String symbol_native = SharedPrefUserDetail.getString(c, SharedPrefUserDetail.chit_symbol_native, "");
                            String currency_code_id = SharedPrefUserDetail.getString(c, SharedPrefUserDetail.chit_currency_code_id, "");
                            String symbol = SharedPrefUserDetail.getString(c, SharedPrefUserDetail.chit_symbol, "");

                            jObj.addProperty("currency_code", currency_code);
                            jObj.addProperty("symbol_native", symbol_native);
                            jObj.addProperty("currency_code_id", currency_code_id);
                            jObj.addProperty("symbol", symbol);


                            boolean iss = SharedPrefUserDetail.getBoolean(c, SharedPrefUserDetail.isfromsupplier, false);
                            if (iss) {
                                jObj.addProperty("info", "");
                                StringBuilder stringBuilder = new StringBuilder();
                                if (field_json_data != null) {

                                    String delivery_partner_info = JsonObjParse.getValueEmpty(field_json_data, "delivery_partner_info");
                                    if (!delivery_partner_info.isEmpty()) {
                                        stringBuilder.append(delivery_partner_info + " , ");
                                        jObj.addProperty("info", stringBuilder.toString());
                                    }
                                }
                            } else {

                                String info = SharedPrefUserDetail.getString(c, SharedPrefUserDetail.infoMainShop, "");
                                String act = sessionManager.getcurrentcaterername();
                                StringBuilder stringBuilder = new StringBuilder(); // old is correct

                                if (field_json_data_info != null) {
                                    String pass_info_circle = JsonObjParse.getValueEmpty(field_json_data_info, "pass_info_circle");
                                    String pass_info_aggregator = JsonObjParse.getValueEmpty(field_json_data_info, "pass_info_aggregator");
                                    if (!pass_info_circle.equalsIgnoreCase("no") && business_type_info.equalsIgnoreCase("circle")) {
                                        if (!info.isEmpty()) stringBuilder.append(info + " , ");
                                    }
                                    if (!pass_info_aggregator.equalsIgnoreCase("no") && business_type_info.equalsIgnoreCase("aggregator")) {
                                        if (!info.isEmpty()) stringBuilder.append(info + " , ");
                                    }
                                }

                                if (!act.isEmpty()) stringBuilder.append(act + " , ");

                                if (field_json_data != null) {
                                    String delivery_partner_info = JsonObjParse.getValueEmpty(field_json_data, "delivery_partner_info");
                                    if (!delivery_partner_info.isEmpty()) {
                                        stringBuilder.append(delivery_partner_info + " , ");
                                    }
                                }

                                Log.d("okhttp info", stringBuilder.toString());
                                jObj.addProperty("info", stringBuilder.toString());
                                //jObj.addProperty("info", sessionManager.getAggregator_ID());

                            }


                            jObj.addProperty("header_note", headernotes);

                            jObj.addProperty("purpose", purpose);

                            jObj.addProperty("chit_item_count", productcount);


                            jObj.addProperty("total_chit_item_value", price + "");
                            JsonObject jo_footer = new JsonObject();
                          /*  jo_footer.addProperty("SGST", String.valueOf(totalstategst));
                            jo_footer.addProperty("VAT", String.valueOf(totalVat));
                            jo_footer.addProperty("CGST", String.valueOf(totalcentralgst));
                            jo_footer.addProperty("TOTALBILL", String.valueOf(totalbill));
                            jo_footer.addProperty("GRANDTOTAL", String.valueOf(grandtotal));*/
                            jo_footer.addProperty("NAME", loggedin.getFirstname());
                            jo_footer.addProperty("PAYMENT_MODE", "NONE");


                            jo_footer.addProperty("DATE", dt);
                            jo_footer.addProperty("ADDRESS", "");
                            jo_footer.addProperty("DOB", "");
                            jObj.addProperty("footer_note", jo_footer.toString());

                            Log.e("chitheadervalue", jObj.toString());
                            confirmorder(c, chithashid);

                        } catch (JsonParseException e) {
                            e.printStackTrace();
                        }
                    } catch (Exception e) {
                        Toast.makeText(c, e.getMessage(), Toast.LENGTH_LONG).show();
                    }

                } else {
                    if (dialog != null) dialog.dismiss();

                    Log.e("test", " trdds1");

                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Toast.makeText(c, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(c, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }

            }

            @Override
            public void onFailure(Call call, Throwable t) {
                if (dialog != null) dialog.dismiss();

                Log.e("error message", t.toString());
            }
        });
    }

    public void confirmorder(Context context, String hashidval) {

        final SessionManager sessionManager = new SessionManager(context);


        try {
            JsonObject jObj = new JsonObject();
            jObj.addProperty("email", loggedin.getEmail_id());
            jObj.addProperty("contact_number", sessionManager.getT_Phone());
            jObj.addProperty("location", sessionManager.getAddress());
            jObj.addProperty("act", sessionManager.getcurrentcaterername());


            jObj.addProperty("chit_name", ""); //


            jObj.addProperty("chitHashId", hashidval);
            jObj.addProperty("latitude", "");
            jObj.addProperty("longtitude", "");

            String expected_delivery_time = DateTimeUtils.parseDateTime(sessionManager.getDate(), "dd MMM yyyy hh:mm aaa", "yyyy/MM/dd kk:mm");

            jObj.addProperty("expected_delivery_time", expected_delivery_time);
            jObj.addProperty("for_non_bridge_name", "");
            jObj.addProperty("subject", sessionManager.getcurrentcatereraliasname());

            String currency_code = SharedPrefUserDetail.getString(context, SharedPrefUserDetail.chit_currency_code, "");
            String symbol_native = SharedPrefUserDetail.getString(context, SharedPrefUserDetail.chit_symbol_native, "");
            String currency_code_id = SharedPrefUserDetail.getString(context, SharedPrefUserDetail.chit_currency_code_id, "");
            String symbol = SharedPrefUserDetail.getString(context, SharedPrefUserDetail.chit_symbol, "");

            jObj.addProperty("currency_code", currency_code);
            jObj.addProperty("symbol_native", symbol_native);
            jObj.addProperty("currency_code_id", currency_code_id);
            jObj.addProperty("symbol", symbol);


            boolean iss = SharedPrefUserDetail.getBoolean(context, SharedPrefUserDetail.isfromsupplier, false);
            if (iss) { // if its business
                jObj.addProperty("info", "");

                StringBuilder stringBuilder = new StringBuilder();

                if (field_json_data != null) {

                    String delivery_partner_info = JsonObjParse.getValueEmpty(field_json_data, "delivery_partner_info");
                    if (!delivery_partner_info.isEmpty()) {
                        stringBuilder.append(delivery_partner_info + " , ");
                        jObj.addProperty("info", stringBuilder.toString());
                    }
                }


            } else { // if its aggre / circle
                // blrmc1(delivery flag of this) under blrmcc(info flag of this)
                String info = SharedPrefUserDetail.getString(context, SharedPrefUserDetail.infoMainShop, "");
                //String act = sessionManager.getAggregator_ID();
                String act = sessionManager.getcurrentcaterername();
                //String infoPass = info + " , " + act + " , ";
                StringBuilder stringBuilder = new StringBuilder(); // old is correct
                //if(!info.isEmpty())stringBuilder.append(info+" , ");
                //if(!act.isEmpty())stringBuilder.append(act+" , ");
                if (field_json_data_info != null) {
                    String pass_info_circle = JsonObjParse.getValueEmpty(field_json_data_info, "pass_info_circle");
                    String pass_info_aggregator = JsonObjParse.getValueEmpty(field_json_data_info, "pass_info_aggregator");
                    if (!pass_info_circle.equalsIgnoreCase("no") && business_type_info.equalsIgnoreCase("circle")) {
                        if (!info.isEmpty()) stringBuilder.append(info + " , ");
                    }
                    if (!pass_info_aggregator.equalsIgnoreCase("no") && business_type_info.equalsIgnoreCase("aggregator")) {
                        if (!info.isEmpty()) stringBuilder.append(info + " , ");
                    }
                }

                if (!act.isEmpty()) stringBuilder.append(act + " , ");

                if (field_json_data != null) {

                    String delivery_partner_info = JsonObjParse.getValueEmpty(field_json_data, "delivery_partner_info");
                    if (!delivery_partner_info.isEmpty()) {
                        stringBuilder.append(delivery_partner_info + " , ");
                    }
                }

                Log.d("okhttp info", stringBuilder.toString());
                jObj.addProperty("info", stringBuilder.toString());
                //jObj.addProperty("info", sessionManager.getAggregator_ID());
            }


            jObj.addProperty("header_note", sessionManager.getRemark());
            jObj.addProperty("purpose", purpose);

            jObj.addProperty("chit_item_count", productcount);
            jObj.addProperty("total_chit_item_value", price + "");
            JsonObject jo_footer = new JsonObject();
           /* jo_footer.addProperty("SGST", String.valueOf(totalstategst));
            jo_footer.addProperty("CGST", String.valueOf(totalcentralgst));
            jo_footer.addProperty("VAT", String.valueOf(totalVat));
            jo_footer.addProperty("TOTALBILL", String.valueOf(totalbill));
            jo_footer.addProperty("TOTLREQDISC", String.valueOf(totalReqDisc));
            jo_footer.addProperty("REQ_DISC", String.valueOf(ReqDisc));
            jo_footer.addProperty("TOTLPRODISC", String.valueOf(totalProductDisc));
            jo_footer.addProperty("GRANDTOTAL", String.valueOf(grandtotal));
            jo_footer.addProperty("businessId", String.valueOf(grandtotal));*/


            jo_footer.addProperty("NAME", loggedin.getFirstname());
            jo_footer.addProperty("DOB", "###");


            /*String taxArray = new Gson().toJson(alTax);
            jo_footer.addProperty("taxArray", taxArray);
*/
            /*if (al_uploadedimg.size() > 0) {
                String imgjson = new Gson().toJson(al_uploadedimg);
                jo_footer.addProperty("img", imgjson);
            } else {

                jo_footer.addProperty("img", "");

            }
*/
           /* if (rb_cod.isChecked()) {
                iscod = true;
                jo_footer.addProperty("PAYMENT_MODE", "COD");
            } else {
                iscod = false;
            }*/

            jo_footer.addProperty("PAYMENT_MODE", "NONE");

            // Consumer Array

            JsonArray jaConsumerContact = new JsonArray();
            JsonObject joConsumer = new JsonObject();
            String sms_customer = JsonObjParse.getValueEmpty(field_json_data, "sms_customer");
            joConsumer.addProperty("sms", sms_customer);
            joConsumer.addProperty("customer_nm", loggedin.getFirstname());
            //joConsumer.addProperty("mobile", sessionManager.getT_Phone());
            joConsumer.addProperty("mobile", loggedin.getContact_no());
            jaConsumerContact.add(joConsumer);
            jo_footer.add("ConsumerInfo", jaConsumerContact);

            // Business Array

            JsonArray jaBusinessContact = new JsonArray();
            JsonObject joBusiness = new JsonObject();
            String sms_business = JsonObjParse.getValueEmpty(field_json_data, "sms_status");
            joBusiness.addProperty("sms", sms_business);
            String mobile_business = JsonObjParse.getValueEmpty(field_json_data, "mobile_business");
            joBusiness.addProperty("mobile", mobile_business);
            joBusiness.addProperty("customer_nm", sessionManager.getcurrentcatereraliasname());
            jaBusinessContact.add(joBusiness);

            jo_footer.add("BusinessInfo", jaBusinessContact);

            // Delivery Array

           /* JsonArray jaDeliveryContact = new JsonArray();
            JsonObject joDelivery = new JsonObject();

            String mobile_delivery = JsonObjParse.getValueEmpty(field_json_data, "mobile_delivery");
            joDelivery.addProperty("mobile", mobile_delivery);
            String companynm = JsonObjParse.getValueEmpty(field_json_data, "customer_nm_delivery");
            joDelivery.addProperty("customer_nm", companynm + "");
            //String pick_up_only = JsonObjParse.getValueEmpty(field_json_data, "pick_up_only");

            String sms_delivery = JsonObjParse.getValueEmpty(field_json_data, "sms_delivery");
            joDelivery.addProperty("sms", sms_delivery);
            joDelivery.addProperty("pick_up_only", "No");

            jaDeliveryContact.add(joDelivery);
            jo_footer.add("DeliveryInfo", jaDeliveryContact);*/

            jo_footer.addProperty("DATE", sessionManager.getDate());
            jo_footer.addProperty("ADDRESS", sessionManager.getAddress());


            jo_footer.addProperty("Cust_Nm", "");


            String dentalobj = new Gson().toJson(dental, Dental.class);

            jo_footer.addProperty("dental_obj", dentalobj);
            jo_footer.addProperty("payuResponse", "");
            jo_footer.addProperty("chithashid", hashidval);


            jo_footer.addProperty("field_json_data", field_json_data);


            jObj.addProperty("footer_note", jo_footer.toString());
            Log.i("okhttp", jo_footer.toString());
            updatechit(context, sessionManager.getsession(), jObj, hashidval);

        } catch (JsonParseException e) {
            if (dialog != null) dialog.dismiss();

            e.printStackTrace();
        }
    }

    public void updatechit(final Context c, String usersession, JsonObject chitdetails, String hashid) {

        SessionManager sessionManager = new SessionManager(c);

        ApiInterface apiService = ApiClient.getClient(c).create(ApiInterface.class);


        String currency_code = SharedPrefUserDetail.getString(c, SharedPrefUserDetail.chit_currency_code, "");
        String currency_code_id = SharedPrefUserDetail.getString(c, SharedPrefUserDetail.chit_currency_code_id, "");

        Call call = apiService.updatechit(usersession, chitdetails, sessionManager.getAggregatorprofile(), hashid, "d97be841a6d6cbb66bc3ae165b31b85f", currency_code, currency_code_id, "2");
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {

                if (dialog != null) dialog.dismiss();


                if (response.isSuccessful()) {

                    try {
                        String a = new Gson().toJson(response.body());
                        callRequestForSurvey.onGetResponse("");

                    } catch (Exception e) {

                        Toast.makeText(c, e.getMessage(), Toast.LENGTH_LONG).show();
                    }

                } else {

                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Log.e("test", " trdds1" + jObjErrorresponse.toString());
                        String msg = jObjErrorresponse.getString("errormsg");
                        if (msg.equalsIgnoreCase("To is empty")) {
                            msg = "You cannot reporting to yourselves.";
                        }
                        Toast.makeText(c, msg, Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(c, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }

            }

            @Override
            public void onFailure(Call call, Throwable t) {
                if (dialog != null) dialog.dismiss();

            }
        });
    }




}
