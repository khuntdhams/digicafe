package com.cab.digicafe.Activities.other.walkin;

import android.content.Context;
import android.widget.Toast;

import com.cab.digicafe.Activities.RequestApis.MyRequst;
import com.cab.digicafe.Helper.SessionManager;
import com.cab.digicafe.Rest.ApiClient;
import com.cab.digicafe.Rest.ApiInterface;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * An implementation of the Presenter.
 *
 * @author Quang Nguyen.
 */

public class WalkInPresenter implements WalkInContarct.Presenter {


    private WalkInContarct.View myView;
    Context context;
    SessionManager sessionManager;

    public WalkInPresenter(WalkInContarct.View view, Context context) {
        this.myView = view;
        this.context = context;
        sessionManager = new SessionManager(context);
    }


    @Override
    public void findExternalContactsByContactNo(JsonObject jsonObject, MyRequst myRequst) {
        myView.showProgress();

        ApiInterface apiService = ApiClient.getClient(context).create(ApiInterface.class);
        Call call;
        call = apiService.getExternalContactByNo(sessionManager.getsession(), myRequst.contact_number, myRequst.page);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                myView.hideProgress();
                if (response != null && response.isSuccessful()) {
                    myView.onGetContact(response);
                } else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Toast.makeText(context, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        try {
                            Toast.makeText(context, "something went wrong " + context.getPackageResourcePath(), Toast.LENGTH_LONG).show();
                        } catch (Exception ee) {
                            ee.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                myView.hideProgress();

                try {
                    Toast.makeText(context, "Failed " + context.getPackageResourcePath(), Toast.LENGTH_LONG).show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


    }

    @Override
    public void insertExternalContactsByContactNo(JsonObject jsonObject, MyRequst myRequst) {

        myView.showProgress();

        ApiInterface apiService = ApiClient.getClient(context).create(ApiInterface.class);
        Call call;
        call = apiService.postExternalContactByNo(sessionManager.getsession(), jsonObject);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                myView.hideProgress();
                if (response != null && response.isSuccessful()) {
                    myView.onGetInsertedContact(response);
                } else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Toast.makeText(context, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        try {
                            Toast.makeText(context, "something went wrong " + context.getPackageResourcePath(), Toast.LENGTH_LONG).show();
                        } catch (Exception ee) {
                            ee.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                myView.hideProgress();

                try {
                    Toast.makeText(context, "Failed " + context.getPackageResourcePath(), Toast.LENGTH_LONG).show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


    }

}
