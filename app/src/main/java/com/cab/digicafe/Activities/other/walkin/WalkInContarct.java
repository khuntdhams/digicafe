package com.cab.digicafe.Activities.other.walkin;


import com.cab.digicafe.Activities.RequestApis.MyRequst;
import com.google.gson.JsonObject;

import retrofit2.Response;

public interface WalkInContarct {

    /**
     * Represents the View in MVP.
     */
    public interface View {
        void showMessage(String message);

        void showError(String error);

        void showProgress();

        void hideProgress();

        void onGetContact(Response response);

        void onGetInsertedContact(Response response);
    }

    /**
     * Represents the Presenter in MVP.
     */
    public interface Presenter {
        void findExternalContactsByContactNo(JsonObject jsonObject, MyRequst myRequst);

        void insertExternalContactsByContactNo(JsonObject jsonObject, MyRequst myRequst);
    }
}
