package com.cab.digicafe.Activities.other;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.cab.digicafe.Activities.MyAppBaseActivity;
import com.cab.digicafe.Activities.RequestApis.MyRequestCall;
import com.cab.digicafe.Adapter.traction.ModelUserTraction;
import com.cab.digicafe.Adapter.traction.UserTractionListListAdapter;
import com.cab.digicafe.R;
import com.google.gson.Gson;
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class UserTractionActivity extends MyAppBaseActivity {


    @BindView(R.id.tvTitle)
    TextView tvTitle;

    @OnClick(R.id.iv_back)
    public void iv_back(View v) {
        onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_traction);
        context = this;
        //getLayoutInflater().inflate(R.layout.activity_user_traction, frameLayout);
        ButterKnife.bind(this);
        refreshlist();
        //Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);
        //getSupportActionBar().setTitle("User Traction");
        setClickList();
    }

    @BindView(R.id.rvClicks)
    RecyclerView rvClicks;

    UserTractionListListAdapter userTractionListListAdapter;
    ArrayList<ModelUserTraction> alUserTraction = new ArrayList<>();

    MyRequestCall myRequestCall = new MyRequestCall();

    public void setClickList() {

        /*String json = SharedPrefUserDetail.getString(this, SharedPrefUserDetail.user_traction_list, "");
        Type type = new TypeToken<ArrayList<ModelUserTraction>>() {
        }.getType();

        alUserTraction = new Gson().fromJson(json, type);

        if (alUserTraction == null) {
            alUserTraction = new ArrayList<>();
        }*/

        //alUserTraction = myRequestCall.getTraction(this);

        userTractionListListAdapter = new UserTractionListListAdapter(alUserTraction, this, new UserTractionListListAdapter.OnItemClickListener() {
            @Override
            public void onDelete(int item) {
            }

            @Override
            public void onEdit(int item) {
            }
        });

        rvClicks.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        rvClicks.setAdapter(userTractionListListAdapter);
        rvClicks.setNestedScrollingEnabled(false);

        fillList();

    }

    Context context;

    public void fillList() {
        if (pagecount == 1) alUserTraction = new ArrayList<>();

        mSwipyRefreshLayout.setRefreshing(true);

        myRequestCall.getTractionData(context, new MyRequestCall.CallRequest() {
            @Override
            public void onGetResponse(String response) {

                mSwipyRefreshLayout.setRefreshing(false);

                try {
                    JSONObject jObjRes = new JSONObject(response);

                    JSONObject jObjdata = jObjRes.getJSONObject("response");
                    JSONObject jObjResponse = jObjdata.getJSONObject("data");
                    String totalRecord = jObjResponse.getString("totalRecord");

                    tvTitle.setText("User Traction [" + totalRecord + "]");
                    JSONArray jObjcontacts = jObjResponse.getJSONArray("content");

                    for (int i = 0; i < jObjcontacts.length(); i++) {
                        if (jObjcontacts.get(i) instanceof JSONObject) {
                            JSONObject jsnObj = (JSONObject) jObjcontacts.get(i);
                            ModelUserTraction modelUserTraction = new Gson().fromJson(jsnObj.toString(), ModelUserTraction.class);
                            alUserTraction.add(modelUserTraction);
                        }
                    }


                    int totalVal = Integer.parseInt(totalRecord);
                    int displayEndRecord = jObjResponse.getInt("displayEndRecord");
                    if (totalVal == displayEndRecord) {
                        isdataavailable = false;
                    } else {
                        isdataavailable = true;
                    }

                    userTractionListListAdapter.setItem(alUserTraction);

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    SwipyRefreshLayout mSwipyRefreshLayout;
    int pagecount = 1;
    boolean isdataavailable = true;

    public void refreshlist() {
        mSwipyRefreshLayout = (SwipyRefreshLayout) findViewById(R.id.swipyrefreshlayout);
        mSwipyRefreshLayout.setDirection(SwipyRefreshLayoutDirection.BOTH);
        mSwipyRefreshLayout.setOnRefreshListener(new SwipyRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(SwipyRefreshLayoutDirection direction) {

                if (direction == SwipyRefreshLayoutDirection.BOTTOM) {
                    if (isdataavailable) {
                        pagecount++;
                        fillList();
                    } else {
                        mSwipyRefreshLayout.setRefreshing(false);
                        Toast.makeText(context, "No more data available", Toast.LENGTH_LONG).show();
                    }
                }

                if (direction == SwipyRefreshLayoutDirection.TOP) {
                    isdataavailable = true;
                    pagecount = 1;
                    fillList();
                }
            }
        });
    }


}
