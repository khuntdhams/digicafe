package com.cab.digicafe.Activities;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.cab.digicafe.Activities.RequestApis.MyRequestCall;
import com.cab.digicafe.Activities.healthTemplate.SelectTemplateActivity;
import com.cab.digicafe.Adapter.AddressAdapter;
import com.cab.digicafe.Adapter.PlaceArrayAdapter;
import com.cab.digicafe.Adapter.ctlg.MetalListAdapter;
import com.cab.digicafe.Adapter.master.ImagePagerAdapter;
import com.cab.digicafe.Adapter.settings.RangeInDiscountListAdapter;
import com.cab.digicafe.Adapter.settings.ShopDragTouchHelperIssue;
import com.cab.digicafe.Adapter.settings.ShopImgFileAdapter;
import com.cab.digicafe.Dialogbox.SaveProfile;
import com.cab.digicafe.Helper.FileHelper;
import com.cab.digicafe.Helper.Inad;
import com.cab.digicafe.Helper.LoadImg;
import com.cab.digicafe.Helper.SessionManager;
import com.cab.digicafe.Helper.SharedPrefUserDetail;
import com.cab.digicafe.Model.AddAddress;
import com.cab.digicafe.Model.Metal;
import com.cab.digicafe.Model.ModelFile;
import com.cab.digicafe.Model.ProductTotalDiscount;
import com.cab.digicafe.Model.ShareModel;
import com.cab.digicafe.Model.SpinModel;
import com.cab.digicafe.Model.Userprofile;
import com.cab.digicafe.MyCustomClass.AutoCompleteTv;
import com.cab.digicafe.MyCustomClass.JsonObjParse;
import com.cab.digicafe.MyCustomClass.PlayAnim;
import com.cab.digicafe.MyCustomClass.SearchableSpinner;
import com.cab.digicafe.MyCustomClass.ShowGalleryCamera;
import com.cab.digicafe.MyCustomClass.Utils;
import com.cab.digicafe.MyCustomClass.dialogeffect.Effectstype;
import com.cab.digicafe.MyCustomClass.dialogeffect.effects.BaseEffects;
import com.cab.digicafe.R;
import com.cab.digicafe.Rest.ApiClient;
import com.cab.digicafe.Rest.ApiInterface;
import com.cab.digicafe.photopicker.activity.PickImageActivity;
import com.cab.digicafe.serviceTypeClass.GetServiceTypeFromBusiUserProfile;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnLongClick;
import me.relex.circleindicator.CircleIndicator;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditCurrencyForBusinessActivity extends MyAppBaseActivity implements AddressAdapter.OnItemClickListener {


    @BindView(R.id.spinCurrency)
    SearchableSpinner spinCurrency;

    @BindView(R.id.serviceType)
    SearchableSpinner serviceType;

    @BindView(R.id.rlPb)
    RelativeLayout rlPb;

    @BindView(R.id.cbSmsBus)
    CheckBox cbSms;

    @BindView(R.id.cbSmsCust)
    CheckBox cbSmsCust;

    @BindView(R.id.cbSmsEmp)
    CheckBox cbSmsEmp;

    @BindView(R.id.cbSmsDelivery)
    CheckBox cbSmsDelivery;

    @BindView(R.id.btnUpdate)
    Button btnUpdate;

    @BindView(R.id.tvAddImg)
    TextView tvAddImg;

    @BindView(R.id.llAddImg)
    LinearLayout llAddImg;

    @BindView(R.id.rlImg)
    LinearLayout rlImg;

    @BindView(R.id.llBusSetInfo)
    LinearLayout llBusSetInfo;

    @BindView(R.id.name)
    TextView name;

    @BindView(R.id.mobile)
    TextView mobile;

    String mobile_business = "";
    @BindView(R.id.rgServiceType)
    RadioGroup rgServiceType;

    @BindView(R.id.rbShoppingCart)
    RadioButton rbShoppingCart;

    @BindView(R.id.rbService)
    RadioButton rbService;

    @BindView(R.id.rbCatalogueOnly)
    RadioButton rbCatalogueOnly;

    @BindView(R.id.rbShoppingCartWithCb)
    RadioButton rbShoppingCartWithCb;

    @BindView(R.id.rgTemplate)
    RadioGroup rgTemplate;

    @BindView(R.id.rbNormalTemp)
    RadioButton rbNormalTemp;

    @BindView(R.id.sv)
    ScrollView sv;

    @BindView(R.id.rbImageTemp)
    RadioButton rbImageTemp;

    @BindView(R.id.tvShoppingCartWithCb)
    TextView tvShoppingCartWithCb;

    @BindView(R.id.llMinMax)
    LinearLayout llMinMax;

    @BindView(R.id.etMinVal)
    EditText etMinVal;

    @BindView(R.id.etMaxVal)
    EditText etMaxVal;

    @BindView(R.id.etEntrLicenceInfo)
    EditText etEntrLicenceInfo;

    @BindView(R.id.etFullDesc)
    EditText etFullDesc;

    @BindView(R.id.tvLatLongInfo)
    TextView tvLatLongInfo;

    @BindView(R.id.tvAggreBT)
    TextView tvAggreBT;
    @BindView(R.id.tvCircleBT)
    TextView tvCircleBT;
    @BindView(R.id.tvBusinessBT)
    TextView tvBusinessBT;

    @BindView(R.id.llBusinessType)
    LinearLayout llBusinessType;

    @BindView(R.id.llAnimAddcat2)
    LinearLayout llAnimAddcat2;

    @BindView(R.id.rlAddCategory2)
    RelativeLayout rlAddCategory2;

    @BindView(R.id.tvDentalLink)
    TextView tvDentalLink;

    @BindView(R.id.llCustomerTraction)
    LinearLayout llCustomerTraction;
    @BindView(R.id.llPromo)
    LinearLayout llPromo;
    @BindView(R.id.llWalkIn)
    LinearLayout llWalkIn;
    @BindView(R.id.llWorking)
    LinearLayout llWorking;
    @BindView(R.id.llShareLink)
    LinearLayout llShareLink;

    ArrayList<AddAddress> addressList = new ArrayList<>();
    AddressAdapter mAdapter;
    @BindView(R.id.llAddressList)
    LinearLayout llAddressList;
    @BindView(R.id.ivHideList)
    ImageView ivHideList;
    @BindView(R.id.rvAddressList)
    RecyclerView rvAddressList;
    @BindView(R.id.ivAddList)
    ImageView ivAddList;


    @OnLongClick(R.id.llBusinessType)
    public boolean llBusinessType(View v) {
        showBusinessType();
        return true;
    }

    @OnClick(R.id.rlAddCategory2)
    public void rlAddCategory2(View v) {
        onBackPressed();
    }

    @OnClick(R.id.tvBusinessBT)
    public void tvBusinessBT(View v) {
        business_type = "Business";
        setAct();
        hideBT();
    }

    @OnClick(R.id.tvAggreBT)
    public void tvAggreBT(View v) {
        business_type = "Aggregator";
        setAct();
        hideBT();
    }

    @OnClick(R.id.tvCircleBT)
    public void tvCircleBT(View v) {
        business_type = "Circle";
        setAct();
        hideBT();

    }

    public void setAct() {
        tvBusinessBT.setTextColor(ContextCompat.getColor(this, R.color.black));
        tvAggreBT.setTextColor(ContextCompat.getColor(this, R.color.black));
        tvCircleBT.setTextColor(ContextCompat.getColor(this, R.color.black));

        switch (business_type.toLowerCase()) {
            case "business":
                tvBusinessBT.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
                break;
            case "aggregator":
                tvAggreBT.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
                break;
            case "circle":
                tvCircleBT.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
                break;
        }
        tvBusinessType.setText(business_type);

    }

    public void showBusinessType() {

        playAnim.slideUpRlAnim(llAnimAddcat2);
        rlAddCategory2.setVisibility(View.VISIBLE);

    }

    public void hideBT() {

        playAnim.slideUpRl(llAnimAddcat2);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                rlAddCategory2.setVisibility(View.GONE);
            }
        }, 700);
    }

    ArrayList<SpinModel> alCurrency = new ArrayList<>();
    boolean isFromSignUp = false;
    String sms_status = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_currency_for_business);
        ButterKnife.bind(this);

        initNoPicker();
        initMultiImg();
        insertDiscount();

        String[] PERMISSIONS = {android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_EXTERNAL_STORAGE};
        if (!Inad.hasPermissions(this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, 801);
        } else {
        }

        tvShoppingCartLink.setTextColor(Color.parseColor("#0000EE"));
        tvShoppingCartLink.setPaintFlags(tvShoppingCartLink.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        tvTextLink.setTextColor(Color.parseColor("#0000EE"));
        tvTextLink.setPaintFlags(tvShoppingCartLink.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        tvTextLink.setTextColor(Color.parseColor("#0000EE"));
        tvTextLink.setPaintFlags(tvShoppingCartLink.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        tvServiceLink.setTextColor(Color.parseColor("#0000EE"));
        tvServiceLink.setPaintFlags(tvShoppingCartLink.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        tvDentalLink.setTextColor(Color.parseColor("#0000EE"));
        tvDentalLink.setPaintFlags(tvShoppingCartLink.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        tvDentalLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(EditCurrencyForBusinessActivity.this, SelectTemplateActivity.class);
                startActivity(i);
            }
        });

        rlAddCategory.setVisibility(View.GONE);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            isFromSignUp = extras.getBoolean("isFromSignUp", false);
        }

        sessionManager = new SessionManager(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Profile");

        setVisible(true);

        rlPb.setVisibility(View.VISIBLE);

        //callBuisenssProfile(false);
        getCurrency();

        loadImg.loadImg(EditCurrencyForBusinessActivity.this, "", ivCopmanyProfile);

        /*rgServiceType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (i) {
                    case R.id.rbShoppingCart:
                        setServiceType(field_json_data);
                        break;

                    case R.id.rbService:
                        setServiceType("service");
                        break;

                    case R.id.rbCatalogueOnly:
                        serviceTypeString = getString(R.string.catalogue_only);
                        break;
                }
            }
        });*/

        radioGroupCheckedChange();

        setShareLink();

        if (getString(R.string.app_name_condition).equalsIgnoreCase("Space lattice")) {
            llCustomerTraction.setVisibility(View.GONE);
            llPromo.setVisibility(View.GONE);
            llWalkIn.setVisibility(View.GONE);
            llWorking.setVisibility(View.GONE);
            llShareLink.setVisibility(View.GONE);
        } else {
            llCustomerTraction.setVisibility(View.VISIBLE);
            llPromo.setVisibility(View.VISIBLE);
            llWalkIn.setVisibility(View.VISIBLE);
            llWorking.setVisibility(View.VISIBLE);
            llShareLink.setVisibility(View.VISIBLE);
        }
    }

    int posSel = 0;

    public void setCurrency() {

        View view = getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }

        //spinCurrency.clearFocus();
        spinCurrency.setTitle("Currency");
        adapter = new ArrayAdapter<SpinModel>(this, R.layout.spinner_currency, alCurrency);

        spinCurrency.setAdapter(adapter);
        spinCurrency.setSelection(posSel);


        spinCurrency.regCall(new SearchableSpinner.CallClk() {
            @Override
            public void getClk(int pos, Object item) {

                String json = new Gson().toJson(item, new TypeToken<SpinModel>() {
                }.getType());

                SpinModel obj = new Gson().fromJson(json, SpinModel.class);

                spinCurrency.setSelection(obj.cPos);
                currency_code = obj.code;
                symbol_native = obj.symbol_native;
                setCurrencySymbol();

                setServiceType(field_json_data);
                if (rbJewel.isChecked()) setJewelInfo("");
            }
        });

        ivHideList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llAddressList.setVisibility(View.GONE);
            }
        });
        ivAddList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llAddressList.setVisibility(View.VISIBLE);
            }
        });
    }

    ArrayAdapter<SpinModel> adapter;

    public void setBpsForSurvey() {
        if (adapter != null && currencyModelForBps != null && spinCurrency != null) {
            //int pos = adapter.getPosition(currencyModelForBps);
            //spinCurrency.setSelection(pos);

            String json = new Gson().toJson(currencyModelForBps, new TypeToken<SpinModel>() {
            }.getType());

            SpinModel obj = new Gson().fromJson(json, SpinModel.class);

            spinCurrency.setSelection(obj.cPos);
            currency_code = obj.code;
            symbol_native = obj.symbol_native;
            setCurrencySymbol();

            setServiceType(field_json_data);

            //Inad.alerterInfo("Survey", "Bridge point system applied for survey.",EditCurrencyForBusinessActivity.this);
        }

    }

    SpinModel currencyModelForBps;
    ArrayList<SpinModel> alService = new ArrayList<>();

    public void setServiceType(String fieldJson) {

        View view = getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }


        //spinCurrency.clearFocus();
        serviceType.setTitle("Payment Method");

        alService = new ArrayList<>();
        // alService.add(new SpinModel("0", "Request", 0));
        //alService.add(new SpinModel("1", "Invoice", 1));
        //alService.add(new SpinModel("0", "Service", 0));
        alService.add(new SpinModel("0", "COD only", 0));

        // 1 -  If currency is other than Indian or service type is service show default is COD

        String service_type = fieldJson; // for service
        if (!fieldJson.equalsIgnoreCase("service")) { // shopping
            service_type = JsonObjParse.getValueEmpty(fieldJson, "service_type").toLowerCase();

            if (service_type.equalsIgnoreCase("service")) {
                service_type = ""; // for cod only
            }
        } else {

        }

        if (rbShoppingCart.isChecked() || rbService.isChecked() || rbShoppingCartWithCb.isChecked() || rbWholeSale.isChecked() || rbProperty.isChecked() || rbJewel.isChecked()) {

            if (service_type.equalsIgnoreCase(getString(R.string.upload_pdf))) {
                service_type = ""; // for cod only
            }

            if (service_type.equalsIgnoreCase(getString(R.string.catalogue_only))) {
                service_type = ""; // for cod only
            }

            if (service_type.equalsIgnoreCase(getString(R.string.none))) {
                service_type = ""; // for cod only
            }

            if (service_type.equalsIgnoreCase(getString(R.string.survey))) {
                service_type = ""; // for cod only
            }

            if (service_type.equalsIgnoreCase(getString(R.string.link_to_web))) {
                service_type = ""; // for cod only
            }

            if (service_type.equalsIgnoreCase(getString(R.string.sms_only))) {
                service_type = ""; // for cod only
            }



           /* if (service_type.equalsIgnoreCase(getString(R.string.shopping_cart_cb))) {
                service_type = ""; // for cod only
            }*/

        }

        // 2 - If service is chosen, then show only COD
        if (!service_type.equalsIgnoreCase("Service") && currency_code.equalsIgnoreCase("INR")) {
            alService.add(new SpinModel("1", "On-line payment only", 1));
            alService.add(new SpinModel("2", "On-line payment & COD", 2));
        }

        ArrayAdapter<SpinModel> adapter = new ArrayAdapter<SpinModel>(this, R.layout.spinner_currency, alService);

        serviceType.setAdapter(adapter);


        if (!service_type.isEmpty()) {


            switch (service_type) {
               /* case "request":
                    serviceType.setSelection(0);
                    serviceTypeString = "Request";
                    break;

                case "invoice":
                    serviceType.setSelection(1);
                    serviceTypeString = "Invoice";
                    break;
*/
                case "service":
                    serviceType.setSelection(0);
                    serviceTypeString = "Service"; // If service is chosen, then show only COD
                    // If service is chosen, then show only COD

                    break;

                case "cod only":
                    serviceType.setSelection(0);
                    serviceTypeString = "COD only";
                    break;

                case "on-line payment only":

                    if (currency_code.equalsIgnoreCase("INR")) {
                        serviceType.setSelection(1);
                        serviceTypeString = "On-line payment only";
                    } else {
                        serviceType.setSelection(0);
                        serviceTypeString = "COD only";
                    }

                    break;

                case "on-line payment & cod":

                    if (currency_code.equalsIgnoreCase("INR")) {
                        serviceType.setSelection(2);
                        serviceTypeString = "On-line payment & COD";
                    } else {
                        serviceType.setSelection(0);
                        serviceTypeString = "COD only";
                    }
                    break;
                case "catalogue only":
                    serviceTypeString = getString(R.string.catalogue_only);
                    break;
                case "upload pdf":
                    serviceTypeString = getString(R.string.upload_pdf);
                    break;
                default:
                    serviceType.setSelection(0);
                    serviceTypeString = "COD only";
                    break;
            }
        } else {
            serviceType.setSelection(0);
            serviceTypeString = "COD only";
        }


        serviceType.regCall(new SearchableSpinner.CallClk() {
            @Override
            public void getClk(int pos, Object item) {

                if (alService.size() == 1) { // only cod if service

                } else {
                    String json = new Gson().toJson(item, new TypeToken<SpinModel>() {
                    }.getType());

                    SpinModel obj = new Gson().fromJson(json, SpinModel.class);

                    serviceType.setSelection(obj.cPos);
                    serviceTypeString = obj.name;
                }
            }
        });
    }

    SessionManager sessionManager;
    String username = "";
    Userprofile userprofile = new Userprofile();

    public void callBuisenssProfile(final boolean isUpdate) {


        ApiInterface apiService = ApiClient.getClient(this).create(ApiInterface.class);

        Call call;
        //call = apiService.getBusinessProfile(sessionManager.getsession());
        call = apiService.getagregatorProfile(sessionManager.getcurrentu_nm());

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                rlPb.setVisibility(View.GONE);

                if (response.isSuccessful()) {

                    if (isUpdate) {

                        ShareModel.getI().isCheckStInCatalogue = true;
                        new SaveProfile(EditCurrencyForBusinessActivity.this, new SaveProfile.OnDialogClickListener() {
                            @Override
                            public void onDialogImageRunClick(int positon, String add) {

                                if (positon == 0) {

                                } else if (positon == 1) {
                                    redirecttohome();
                                    //finish();
                                }
                            }
                        }, "Profile Updated Successfully !").show();
                    } else {
                        String a = new Gson().toJson(response.body());
                        Log.e("response", a);
                        try {

                            JSONObject jObjRes = new JSONObject(a);
                            JSONObject jObjdata = jObjRes.getJSONObject("response");
                            //JSONObject jObjResponse = jObjdata.getJSONObject("data");
                            JSONObject jObjResponse = jObjdata.getJSONArray("data").getJSONObject(0);

                            userprofile = new Gson().fromJson(jObjResponse.toString(), Userprofile.class);
   /*

                            JSONArray jadata = jObjdata.getJSONArray("data");
                            JSONObject jObjResponse = jadata.getJSONObject(0);

                            */

                            String code = jObjResponse.getString("code");
                            sms_status = jObjResponse.getString("sms_status");

                            String firstname = jObjResponse.getString("firstname");
                            String contact_no = jObjResponse.getString("contact_no");

                            String location = JsonObjParse.getValueEmpty(jObjResponse.toString(), "location");
                            lati = JsonObjParse.getValueEmpty(jObjResponse.toString(), "latitude");
                            longi = JsonObjParse.getValueEmpty(jObjResponse.toString(), "longitude");
                            initLoc();
                            tv_google_loc.setText(location);

                            if (location != null && !location.isEmpty()) {
                                try {
                                    JSONArray array = new JSONArray(location);
                                    String type, address;
                                    for (int i = 0; i < array.length(); i++) {
                                        JSONObject obj = array.getJSONObject(i);
                                        type = obj.getString("addressType");
                                        address = obj.getString("address");
                                        addressList.add(new AddAddress(type, address));
                                    }
                                    tv_google_loc.setText(addressList.get(0).getAddress());
                                    mAdapter = new AddressAdapter(addressList, EditCurrencyForBusinessActivity.this, true);
                                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(EditCurrencyForBusinessActivity.this);
                                    rvAddressList.setLayoutManager(mLayoutManager);
                                    rvAddressList.setAdapter(mAdapter);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            if (!lati.isEmpty())
                                tvLatLongInfo.setText("Latitude : " + lati + " , " + "Longitude : " + longi);


                            String company_detail_long = JsonObjParse.getValueEmpty(jObjResponse.toString(), "company_detail_long");
                            etFullDesc.setText(company_detail_long);

                            field_json_data = JsonObjParse.getValueEmpty(jObjResponse.toString(), "field_json_data");


                            if (jObjResponse.has("business_type")) {
                                business_type = jObjResponse.getString("business_type");
                            }
                            setAct();


                            if (business_type != null && business_type.isEmpty()) {
                                business_type = "Business";
                                setPassInfoReqForAggreAndCircle(""); // sign up
                            } else {
                                setPassInfoReqForAggreAndCircle(business_type);
                            }

                            tvBusinessType.setText("  " + business_type);


                            //String field_json_data = jObjResponse.getString("field_json_data");

                            name.setText(firstname);
                            mobile.setText(contact_no);
                            mobile_business = contact_no;

                            if (sms_status.equalsIgnoreCase("Yes")) {
                                cbSms.setChecked(true);
                            } else {
                                cbSms.setChecked(false);

                            }

                            if (jObjResponse.getString("company_image").isEmpty())
                                tvAddImg.setText("Add\nProfile Image");
                            else tvAddImg.setText("Update\nProfile Image");
                            tvAddImg.setText("Select\nProfile Image");


                            /*JSONArray jaCurrencyArr = jObjdata.getJSONArray("currencyArr");
                            for (int i = 0; i < jaCurrencyArr.length(); i++) {

                                JSONObject joCurrency = jaCurrencyArr.getJSONObject(i);


                                SpinModel obj = new Gson().fromJson(joCurrency.toString(), SpinModel.class);
                                if (code.equalsIgnoreCase(obj.code)) {
                                    posSel = i;
                                    currency_code = obj.code;
                                    symbol_native = obj.symbol_native;
                                    setCurrencySymbol();
                                }

                                obj.cPos = i;

                                alCurrency.add(obj);


                            }*/


                            setTax(field_json_data);

                            username = jObjResponse.getString("username");
                            etShareId.setText(username);
                            setShareUserId(field_json_data);
                            setCondition(field_json_data);

                            //setCurrency();

                            String web_link_of_shop = JsonObjParse.getValueEmpty(field_json_data, "web_link_of_shop").toLowerCase();
                            etWebLinkOfShop.setText(web_link_of_shop + "");

                            String service_type = JsonObjParse.getValueEmpty(field_json_data, "service_type").toLowerCase();
                            service_type_of = JsonObjParse.getValueEmpty(field_json_data, "service_type_of").toLowerCase();
                            String licence_info = JsonObjParse.getValueEmpty(field_json_data, "licence_info");
                            etEntrLicenceInfo.setText(licence_info);


                            if (service_type_of.equalsIgnoreCase("Service")) {
                                rbService.performClick();
                                //rbService.setChecked(true);
                            } else if (service_type_of.equalsIgnoreCase(getString(R.string.catalogue_only))) {
                                rbCatalogueOnly.performClick();
                                //rbCatalogueOnly.setChecked(true);
                            } else if (service_type_of.equalsIgnoreCase(getString(R.string.upload_pdf))) {
                                rbUploadPdf.performClick();
                                //rbCatalogueOnly.setChecked(true);
                            } else if (service_type_of.equalsIgnoreCase(getString(R.string.none))) {
                                rbNone.performClick();
                                //rbCatalogueOnly.setChecked(true);
                            } else if (service_type_of.equalsIgnoreCase(getString(R.string.survey))) {
                                rbSurvey.performClick();
                                //rbCatalogueOnly.setChecked(true);
                            } else if (service_type_of.equalsIgnoreCase(getString(R.string.jewel))) {
                                rbJewel.performClick();
                                //rbCatalogueOnly.setChecked(true);
                            } else if (service_type_of.equalsIgnoreCase(getString(R.string.link_to_web))) {
                                rbWebLink.performClick();
                                //rbCatalogueOnly.setChecked(true);
                            } else if (service_type_of.equalsIgnoreCase(getString(R.string.shopping_cart_cb))) {
                                rbShoppingCartWithCb.performClick();
                                //rbCatalogueOnly.setChecked(true);
                            } else if (service_type_of.equalsIgnoreCase(getString(R.string.sms_only))) {
                                rbSentSms.performClick();
                                //rbCatalogueOnly.setChecked(true);
                            } else if (service_type_of.equalsIgnoreCase(getString(R.string.wholesale))) {
                                rbWholeSale.performClick();
                                //rbCatalogueOnly.setChecked(true);
                            } else if (service_type_of.equalsIgnoreCase(getString(R.string.property))) {
                                rbProperty.performClick();
                                //rbCatalogueOnly.setChecked(true);
                            } else if (service_type_of.equalsIgnoreCase(getString(R.string.dental))) {
                                rbDental.performClick();
                            } else if (service_type_of.equalsIgnoreCase(getString(R.string.delivery))) {
                                rbDelivery.performClick();
                            } else { // 3 options
                                rbShoppingCart.performClick();
                                //rbShoppingCart.setChecked(true);
                            }

                            String disc_info = JsonObjParse.getValueEmpty(field_json_data, "disc_info");
                            if (disc_info.length() > 2) {
                                try {
                                    disc_info = disc_info.substring(1, disc_info.length() - 1);
                                    etDiscInfo.setText(disc_info);

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            } else {
                                etDiscInfo.setText("");
                            }

                            String picturePath = JsonObjParse.getValueEmpty(field_json_data, "picturePath");
                            webLink = JsonObjParse.getValueEmpty(field_json_data, "webLink");
                            etWebLink.setText(webLink);

                            delivery_charge = JsonObjParse.getValueEmpty(field_json_data, "delivery_charge");
                            etDelivery.setText(delivery_charge);


                            if (!picturePath.isEmpty()) {
                                setPdfNm(picturePath);
                            }

                            min = JsonObjParse.getValueEmpty(field_json_data, "min");
                            if (min.isEmpty()) min = "1";
                            etMinVal.setText(min);

                            max = JsonObjParse.getValueEmpty(field_json_data, "max");
                            if (max.isEmpty()) max = "";
                            etMaxVal.setText(max);

                            String template_type = JsonObjParse.getValueEmpty(field_json_data, "template_type");
                            if (template_type.equalsIgnoreCase("image")) {
                                rbImageTemp.setChecked(true);
                            } else {
                                rbNormalTemp.setChecked(true);
                            }

                            //String company_image = getString(R.string.BASEURL) + getString(R.string.endCompanyImgUrl) + jObjResponse.getString("company_image");
                            //loadImg.loadImg(EditCurrencyForBusinessActivity.this, company_image, ivCopmanyProfile);

                            String company_images_field = JsonObjParse.getValueEmpty(field_json_data, "company_images_field");
                            TypeToken<ArrayList<ModelFile>> token = new TypeToken<ArrayList<ModelFile>>() {
                            };
                            ArrayList<ModelFile> al_temp_selet = new Gson().fromJson(company_images_field, token.getType());
                            if (al_temp_selet == null) al_temp_selet = new ArrayList<>();

                            al_temp_selet.addAll(al_selet);
                            shopImgFileAdapter.assignItem(al_temp_selet);
                            al_selet = new ArrayList<>();
                            al_selet = al_temp_selet;
                            setPager();


                            if (ShareModel.getI().bus_setting_from.equals("master")) { // only setting
                                String t = username;
                                t = t.toUpperCase();
                                getSupportActionBar().setTitle(t);
                            }

                            String disc = JsonObjParse.getValueEmpty(field_json_data, "productTotalDiscount");
                            productTotalDiscount = new Gson().fromJson(disc, ProductTotalDiscount.class);
                            if (productTotalDiscount == null)
                                productTotalDiscount = new ProductTotalDiscount();
                            alRange = productTotalDiscount.alRange;
                            if (productTotalDiscount.product_total_discount_type_of.equalsIgnoreCase("range")) {
                                rbRange.setChecked(true);
                            } else {
                                rbDiscount.setChecked(true);

                            }

                            rangeInDiscountListAdapter.setItem(alRange);

                            clearRange();

                            setWorkingHrInfo("");
                            setJewelInfo("");
                            setTractionService();
                            setWalkInValues();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                } else {

                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                rlPb.setVisibility(View.GONE);

                Log.e("error message", t.toString());
            }
        });
    }

    GetServiceTypeFromBusiUserProfile getServiceTypeFromBusiUserProfile = new GetServiceTypeFromBusiUserProfile();

    public void getCurrency() { // from profile cookie

        getServiceTypeFromBusiUserProfile.getBusinessProfileFromUserId(this, sessionManager.getsession(), new GetServiceTypeFromBusiUserProfile.Apicallback() {
            @Override
            public void onGetResponse(String a, String response, String sms_emp) {


                try {

                    JSONObject jObjRes = new JSONObject(response);
                    JSONObject jObjdata = jObjRes.getJSONObject("response");
                    JSONObject jObjResponse = jObjdata.getJSONObject("data");

                    String code = jObjResponse.getString("code");


                    JSONArray jaCurrencyArr = jObjdata.getJSONArray("currencyArr");
                    for (int i = 0; i < jaCurrencyArr.length(); i++) {

                        JSONObject joCurrency = jaCurrencyArr.getJSONObject(i);


                        SpinModel obj = new Gson().fromJson(joCurrency.toString(), SpinModel.class);
                        if (code.equalsIgnoreCase(obj.code)) {
                            posSel = i;
                            currency_code = obj.code;
                            symbol_native = obj.symbol_native;
                            setCurrencySymbol();

                        }

                        if (obj.name.equalsIgnoreCase(getString(R.string.bps_for_survey))) {
                            currencyModelForBps = obj;
                        }

                        obj.cPos = i;

                        alCurrency.add(obj);

                    }


                    setCurrency();

                    callBuisenssProfile(false);


                } catch (JSONException e) {
                    e.printStackTrace();

                }


            }
        });


    }

    String field_json_data = "";
    String currency_code = "";
    String symbol_native = "";
    String section = "settinginfo";
    String serviceTypeString = "";
    String service_type_of = "";
    String webLink = "";
    String delivery_charge = "";

    // userinfo

    public void callUpdateBuisenssProfile() {

        rlPb.setVisibility(View.VISIBLE);

        ApiInterface apiService =
                ApiClient.getClient(this).create(ApiInterface.class);
        Call call;

        Map<String, RequestBody> stringRequestBodyHashMap = new HashMap<>();


        if (section.equalsIgnoreCase("settinginfo")) {
            RequestBody sms_status;
            if (cbSms.isChecked()) {
                sms_status = RequestBody.create(MediaType.parse("multipart/form-data"), "Yes");
            } else {
                sms_status = RequestBody.create(MediaType.parse("multipart/form-data"), "No");
            }
            stringRequestBodyHashMap.put("sms_status", sms_status);


        } else if (section.equalsIgnoreCase("userinfo")) {


            RequestBody location = RequestBody.create(MediaType.parse("multipart/form-data"), tv_google_loc.getText().toString().trim());
            stringRequestBodyHashMap.put("location", location);

            RequestBody latitude = RequestBody.create(MediaType.parse("multipart/form-data"), lati);
            stringRequestBodyHashMap.put("latitude", latitude);

            RequestBody longitude = RequestBody.create(MediaType.parse("multipart/form-data"), longi);
            stringRequestBodyHashMap.put("longitude", longitude);

            RequestBody profileimage = RequestBody.create(MediaType.parse("multipart/form-data"), userprofile.getProfileimage());
            stringRequestBodyHashMap.put("profileimage", profileimage);

            RequestBody firstname = RequestBody.create(MediaType.parse("multipart/form-data"), userprofile.getFirstname());
            stringRequestBodyHashMap.put("firstname", firstname);

            RequestBody lastname = RequestBody.create(MediaType.parse("multipart/form-data"), userprofile.getLastname());
            stringRequestBodyHashMap.put("lastname", lastname);

            RequestBody contact_no = RequestBody.create(MediaType.parse("multipart/form-data"), userprofile.getContact_no());
            stringRequestBodyHashMap.put("contact_no", contact_no);

            RequestBody email_id = RequestBody.create(MediaType.parse("multipart/form-data"), userprofile.getEmail_id());
            stringRequestBodyHashMap.put("email_id", email_id);

          /*  String imgList = new Gson().toJson(al_selet);
            RequestBody company_image = RequestBody.create(MediaType.parse("multipart/form-data"), imgList);
            stringRequestBodyHashMap.put("profileimage", company_image);*/

        } else if (section.equalsIgnoreCase("bussinessinfo")) {
            RequestBody currencyBody = RequestBody.create(MediaType.parse("multipart/form-data"), currency_code);
            stringRequestBodyHashMap.put("currency_code", currencyBody);



            /*try {
                JSONArray jafield_json_data = new JSONArray();

                JSONObject jofield_json_data_f1 = new JSONObject();
                jofield_json_data_f1.put("Tax-name",   etNm1.getText().toString().trim());
                jofield_json_data_f1.put("Tax-value",  etVal1.getText().toString().trim());

                JSONObject jofield_json_data_f2 = new JSONObject();
                jofield_json_data_f2.put("Tax-name",   etNm2.getText().toString().trim());
                jofield_json_data_f2.put("Tax-value",   etVal2.getText().toString().trim());

                jafield_json_data.put(jofield_json_data_f1);
                jafield_json_data.put(jofield_json_data_f2);


                RequestBody field_json_dataBody = RequestBody.create(MediaType.parse("multipart/form-data"), jafield_json_data.toString());
                stringRequestBodyHashMap.put("field_json_data", field_json_dataBody);

            } catch (JSONException e) {
                e.printStackTrace();
            }*/
            JSONObject joFielddata = new JSONObject();
            try {
                JSONArray jafield_json_data = new JSONArray();


                JSONObject jofield_json_data_f1 = new JSONObject();

                String f2 = etNm2.getText().toString().trim();
                if (!f2.isEmpty()) {
                    String val = etVal2.getText().toString().trim();
                    if (val.isEmpty()) val = "0";
                    jofield_json_data_f1.put(etNm2.getText().toString().toUpperCase().trim(), val);
                }

                String f1 = etNm1.getText().toString().trim();
                if (!f1.isEmpty()) {
                    String val = etVal1.getText().toString().trim();
                    if (val.isEmpty()) val = "0";
                    jofield_json_data_f1.put(etNm1.getText().toString().toUpperCase().trim(), val);
                }

                jafield_json_data.put(jofield_json_data_f1);


                joFielddata.put("Tax", jafield_json_data);
                joFielddata.put("shareId", etShareId.getText().toString().trim());
                joFielddata.put("delivery_partner_info", etDelPartner.getText().toString().trim());
                joFielddata.put("mobile_business", mobile_business);
                joFielddata.put("mobile_delivery", tvDelMobile.getText().toString());
                joFielddata.put("customer_nm_delivery", tvDelCustNm.getText().toString());


                if (business_type.equalsIgnoreCase("aggregator")) {

                    if (cbInfoReqForAggreAndCircle.isChecked()) {
                        pass_info_aggregator = "Yes";
                    } else {
                        pass_info_aggregator = "No";
                    }

                } else if (business_type.equalsIgnoreCase("circle")) {

                    if (cbInfoReqForAggreAndCircle.isChecked()) {
                        pass_info_circle = "Yes";
                    } else {
                        pass_info_circle = "No";
                    }

                }
                // sign up :. chesk visibility
                else if (business_type.equalsIgnoreCase("business") && cbInfoReqForAggreAndCircle.getVisibility() == View.VISIBLE) {

                    if (cbInfoReqForAggreAndCircle.isChecked()) {
                        pass_info_aggregator = "Yes";
                        pass_info_circle = "Yes";
                    } else {
                        pass_info_aggregator = "No";
                        pass_info_circle = "No";
                    }
                }

                joFielddata.put("pass_info_aggregator", pass_info_aggregator);
                joFielddata.put("conditions", etEntrCondition.getText().toString().trim());
                joFielddata.put("pass_info_circle", pass_info_circle);
                joFielddata.put("sms_status", sms_status); // business

                if (cbShowStock.isChecked()) {
                    joFielddata.put("show_stock_position", "Yes");
                } else {
                    joFielddata.put("show_stock_position", "No");
                }

                if (cbRoundOff.isChecked()) {
                    joFielddata.put("is_roundoff_near", "Yes");
                } else {
                    joFielddata.put("is_roundoff_near", "No");
                }

                if (cbUserTraction.isChecked()) {
                    joFielddata.put("is_user_traction", "Yes");
                } else {
                    joFielddata.put("is_user_traction", "No");
                }

                joFielddata.put("tractionId", spn_traction.getSelectedItemPosition());


                if (cbMrp.isChecked()) {
                    joFielddata.put("is_mrp", "Yes");
                } else {
                    joFielddata.put("is_mrp", "No");
                }

                if (cbPromo.isChecked()) {
                    joFielddata.put("is_promo", "Yes");
                } else {
                    joFielddata.put("is_promo", "No");
                }

                joFielddata.put("walk_in_details", getWalkInJson());


                if (cbIsPickUpOnly.isChecked()) {
                    joFielddata.put("pick_up_only", "Yes");
                } else {
                    joFielddata.put("pick_up_only", "No");
                }

                /*jofield_json_data_f1 = new JSONObject();

                String f1 = etNm1.getText().toString().trim();
                if(!f1.isEmpty()){
                    jofield_json_data_f1.put(etDelCharge.getText().toString().trim(),  etDelChargeVal.getText().toString().trim());
                    jofield_json_data_f1.put(etPckgCharge.getText().toString().trim(), etPckgChargeVal.getText().toString().trim());

                    joFielddata.put("charges",jafield_json_data);

                    jafield_json_data.put(joFielddata);

                }
*/


            } catch (JSONException e) {
                e.printStackTrace();
            }


            try {

                /*  Charges  */
                JSONArray jafield_json_data = new JSONArray();


                JSONObject joCharges_json_data_f1 = new JSONObject();

                String c1 = etDelCharge.getText().toString().trim();
                if (!c1.isEmpty()) {
                    String val = etDelChargeVal.getText().toString().trim();
                    if (val.isEmpty()) val = "0";
                    joCharges_json_data_f1.put(etDelCharge.getText().toString().trim(), val);

                }


                String c2 = etPckgCharge.getText().toString().trim();
                if (!c2.isEmpty()) {
                    String val = etPckgChargeVal.getText().toString().trim();
                    if (val.isEmpty()) val = "0";
                    joCharges_json_data_f1.put(etPckgCharge.getText().toString().trim(), val);
                }

                jafield_json_data.put(joCharges_json_data_f1);


                joFielddata.put("charges", jafield_json_data);

                //JSONObject joServiceType = new JSONObject();
                //joServiceType.put("service_type",serviceTypeString);

                joFielddata.put("open_time", etOpenTime.getText().toString().trim());
                joFielddata.put("close_time", etClosedTime.getText().toString().trim());
                joFielddata.put("work_hr_msg", etWorkHrMsg.getText().toString().trim());

                String metal_list = "";
                Metal metal = new Metal();
                metal.alMetal = alMetal;
                metal.serviceId = act_cps.getSelectedItemPosition();

                metal_list = new Gson().toJson(metal);
                joFielddata.put("metal_list", metal_list);

                joFielddata.put("silver_today_price", etSilverTodayPrice.getText().toString().trim());
                SharedPrefUserDetail.setString(EditCurrencyForBusinessActivity.this, SharedPrefUserDetail.silver_today_price_catalogue, etSilverTodayPrice.getText().toString().trim());
                joFielddata.put("gold_today_price", etGoldTodayPrice.getText().toString().trim());
                SharedPrefUserDetail.setString(EditCurrencyForBusinessActivity.this, SharedPrefUserDetail.gold_today_price_catalogue, etGoldTodayPrice.getText().toString().trim());

                joFielddata.put("service_type", serviceTypeString.toLowerCase());
                joFielddata.put("service_type_of", service_type_of.toLowerCase());
                joFielddata.put("web_link_of_shop", etWebLinkOfShop.getText().toString().trim().toLowerCase());

                productTotalDiscount.disc_perc = etDiscPercTotal.getText().toString().trim();
                productTotalDiscount.max_disc_val = etMaxDiscVal.getText().toString().trim();
                productTotalDiscount.min_purchase_val = etMinPurchaseVal.getText().toString().trim();
                productTotalDiscount.alRange = alRange;
                String disc = new Gson().toJson(productTotalDiscount);
                joFielddata.put("productTotalDiscount", disc);

                joFielddata.put("licence_info", etEntrLicenceInfo.getText().toString().trim());
                String company_images_field = new Gson().toJson(al_selet);
                joFielddata.put("company_images_field", company_images_field);

                joFielddata.put("location", tv_google_loc.getText().toString().trim());
                joFielddata.put("lati", lati);
                joFielddata.put("longi", longi);

                joFielddata.put("webLink", webLink);
                joFielddata.put("delivery_charge", delivery_charge);
                joFielddata.put("disc_info", "'" + etDiscInfo.getText().toString().trim() + "'");

                if (rbNormalTemp.isChecked()) {
                    joFielddata.put("template_type", "normal");
                } else {
                    joFielddata.put("template_type", "image");
                }

                if (picturePath.contains("http")) {
                    joFielddata.put("picturePath", picturePath);
                } else {
                    String picturePath = JsonObjParse.getValueEmpty(field_json_data, "picturePath");
                    joFielddata.put("picturePath", picturePath);

                }

                joFielddata.put("min", min);
                joFielddata.put("max", max);

                String dis_perc = etDisPercVal.getText().toString();
                if (dis_perc.trim().isEmpty()) dis_perc = "0";
                joFielddata.put("dis_perc", dis_perc);

                if (cbSmsCust.isChecked()) {
                    joFielddata.put("sms_customer", "Yes");
                } else {
                    joFielddata.put("sms_customer", "No");
                }

                if (cbSmsEmp.isChecked()) {
                    joFielddata.put("sms_employee", "Yes");
                } else {
                    joFielddata.put("sms_employee", "No");
                }

                if (cbSmsDelivery.isChecked()) {
                    joFielddata.put("sms_delivery", "Yes");
                } else {
                    joFielddata.put("sms_delivery", "No");
                }


                joFielddata.put("username", sessionManager.getcurrentu_nm());

            } catch (Exception e) {
                e.printStackTrace();
            }


            RequestBody field_json_data = RequestBody.create(MediaType.parse("multipart/form-data"), joFielddata.toString());
            stringRequestBodyHashMap.put("field_json_data", field_json_data);

            RequestBody company_detail_long = RequestBody.create(MediaType.parse("multipart/form-data"), etFullDesc.getText().toString().trim());
            stringRequestBodyHashMap.put("company_detail_long", company_detail_long);

           /* RequestBody location = RequestBody.create(MediaType.parse("multipart/form-data"), tv_google_loc.getText().toString().trim());
            stringRequestBodyHashMap.put("location", location);*/

            RequestBody business_typeBody = RequestBody.create(MediaType.parse("multipart/form-data"), business_type);
            stringRequestBodyHashMap.put("business_type", business_typeBody);


        }

        RequestBody sectionBody = RequestBody.create(MediaType.parse("multipart/form-data"), section);
        stringRequestBodyHashMap.put("section", sectionBody);


        if (section.equalsIgnoreCase("bussinessinfo") && !path.isEmpty()) {

            RequestBody company_image = RequestBody.create(MediaType.parse("image*//*"), new File(path));
            MultipartBody.Part imgFile = MultipartBody.Part.createFormData("company_image", new File(path).getName(), company_image);


            //stringRequestBodyHashMap.put("company_image", company_image);

            call = apiService.updateBusinessProfileWithImg(sessionManager.getsession(), stringRequestBodyHashMap, imgFile);
        } else {
            call = apiService.updateBusinessProfile(sessionManager.getsession(), stringRequestBodyHashMap);
        }

        //company_image

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {

                if (response.isSuccessful()) {

                    if (section.equalsIgnoreCase("settinginfo")) {
                        section = "userinfo";
                        callUpdateBuisenssProfile();
                    } else if (section.equalsIgnoreCase("userinfo")) {
                        section = "bussinessinfo";
                        callUpdateBuisenssProfile();
                    } else {
                        section = "settinginfo";
                        path = "";
                        callBuisenssProfile(true);
                    }

                } else {
                    rlPb.setVisibility(View.GONE);

                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Toast.makeText(EditCurrencyForBusinessActivity.this, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(EditCurrencyForBusinessActivity.this, "something went wrong", Toast.LENGTH_LONG).show(); // getting gtml 500
                    }


                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                rlPb.setVisibility(View.GONE);

                Log.e("error message", t.toString());
            }
        });
    }

    String user = "";

    public void redirecttohome() { // For Business
        if (isFromSignUp) {
            Intent i = new Intent(EditCurrencyForBusinessActivity.this, SearchActivity.class);  // All
            i.putExtra("session", sessionManager.getsession());
            i.putExtra("isfromsignup", false);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            sessionManager.setisBusinesslogic(true);
            startActivity(i);
        } else {
            Intent i = new Intent(EditCurrencyForBusinessActivity.this, CatalougeTabActivity.class);  // All
            startActivity(i);
        }
    }

    String path = "";
    LoadImg loadImg = new LoadImg();


    ShowGalleryCamera showGalleryCamera = new ShowGalleryCamera(this);

    @BindView(R.id.ivCopmanyProfile)
    ImageView ivCopmanyProfile;


    @OnClick(R.id.tvAddImg)
    public void funIvCopmanyProfile(View v) {
        //showGalleryCamera.showPictureDialog();
        showGalleryCamera.showMultiPictureDialog();

      /*  Intent i = new Intent(this, FileActivity.class);
        i.putExtra("isdoc", false);
        startActivityForResult(i, 8);*/
    }

    @OnClick(R.id.iv_clear)
    public void iv_clear(View v) {
        tv_google_loc.setText("");
        longi = "";
        lati = "";
        tvLatLongInfo.setText("");
    }

    @OnClick(R.id.btnUpdate)
    public void btnUpdate(View v) {

        boolean isUpdate = false;
        if (rbJewel.isChecked() && alMetal.size() == 0) {
            isUpdate = false;
            Toast.makeText(EditCurrencyForBusinessActivity.this, "Please enter material", Toast.LENGTH_LONG).show();
            return;
        } else if (tvAddEditMetal.getText().toString().equalsIgnoreCase("edit")) {
            isUpdate = false;
            Toast.makeText(EditCurrencyForBusinessActivity.this, "Please update commodity first", Toast.LENGTH_LONG).show();
            return;
        } else if (rbUploadPdf.isChecked()) {
            if (tvPdfNm.getText().toString().equalsIgnoreCase(getString(R.string.no_file_selected))) {
                //tvUploadPdf.performClick();
                Toast.makeText(EditCurrencyForBusinessActivity.this, "Please select file", Toast.LENGTH_LONG).show();
                return;
            } else if (picturePath.contains("http")) {
                isUpdate = true;

            } else { // fst upload


                rl_upload.setVisibility(View.VISIBLE);

                isUpdate = false;

                MyRequestCall myRequestCall = new MyRequestCall();

                myRequestCall.uploadAwsS3(sessionManager.getcurrentu_nm(), EditCurrencyForBusinessActivity.this, picturePath, new MyRequestCall.CallRequest() {
                    @Override
                    public void onGetResponse(String response) {

                        if (!response.isEmpty()) {
                            setPdfNm(response);
                            if (btnUpdate.getText().equals("NEXT")) setVisible(false);
                            else callUpdateBuisenssProfile();
                        }
                        rl_upload.setVisibility(View.GONE);

                    }
                });


            }
        } else if (rbShoppingCartWithCb.isChecked()) {
            String minVal = etMinVal.getText().toString().trim();
            String maxVal = etMaxVal.getText().toString().trim();

            if (minVal.length() == 0) {
                isUpdate = false;
                Toast.makeText(EditCurrencyForBusinessActivity.this, "Please enter minimum value", Toast.LENGTH_LONG).show();
            } else {

                int minI = Integer.parseInt(minVal);
                int maxI = -1;
                if (!maxVal.isEmpty()) {
                    maxI = Integer.parseInt(maxVal);
                    if (minI > maxI || minI == 0 || maxI == 0) {
                        isUpdate = false;
                        Toast.makeText(EditCurrencyForBusinessActivity.this, "Please enter valid value", Toast.LENGTH_LONG).show();
                    } else {
                        min = minVal;
                        max = maxVal;
                        isUpdate = true;
                    }

                } else {
                    min = minVal;
                    max = maxVal;
                    isUpdate = true;
                }

            }

        } else if (rbWebLink.isChecked()) {
            webLink = etWebLink.getText().toString().trim();
            if (webLink.isEmpty()) {
                isUpdate = false;
                Toast.makeText(EditCurrencyForBusinessActivity.this, "Please enter url", Toast.LENGTH_LONG).show();
            } else {
                isUpdate = true;

            }
        } else if (rbDelivery.isChecked()) {
            delivery_charge = etDelivery.getText().toString().trim();
            if (delivery_charge.isEmpty()) {
                isUpdate = false;
                Toast.makeText(EditCurrencyForBusinessActivity.this, "Please enter charges per km", Toast.LENGTH_LONG).show();
            } else {
                isUpdate = true;

            }
        } /*else if (rbJewel.isChecked()) {
            String gold_price = etGoldTodayPrice.getText().toString().trim();
            String silver_price = etSilverTodayPrice.getText().toString().trim();
            if (gold_price.isEmpty() || silver_price.isEmpty()) {
                isUpdate = false;
                Toast.makeText(EditCurrencyForBusinessActivity.this, "Please enter all today prices", Toast.LENGTH_LONG).show();
            } else {
                isUpdate = true;

            }
        } */ else {
            isUpdate = true;
        }


        if (rlImg.getVisibility() == View.VISIBLE && al_selet.size() > 0) {
            isUpdate = false;

            rl_upload.setVisibility(View.VISIBLE);
            indexOfImg = 0;
            uploadOfflineImg();

        }

        if (isUpdate) {
            if (btnUpdate.getText().equals("NEXT")) setVisible(false);
            else {
                callUpdateBuisenssProfile();
            }
        }

    }

    int indexOfImg = 0;

    public void uploadOfflineImg() {

        int p = indexOfImg + 1;
        tv_progress.setText("Uploading.. " + p + "/" + al_selet.size());
        if (indexOfImg == al_selet.size()) {
            rl_upload.setVisibility(View.GONE);
            callUpdateBuisenssProfile();
        } else if (al_selet.get(indexOfImg).getImgpath().contains("http")) {


            indexOfImg++;
            uploadOfflineImg();


        } else {

            MyRequestCall myRequestCall = new MyRequestCall();

            myRequestCall.uploadCatAwsS3(sessionManager.getcurrentu_nm(), this, indexOfImg, al_selet.get(indexOfImg).getImgpath(), new MyRequestCall.CallRequest() {
                @Override
                public void onGetResponse(String response) {

                    if (!response.isEmpty()) {

                        al_selet.get(indexOfImg).setImgpath(response);
                        al_selet.get(indexOfImg).setIsoffline(false);
                        indexOfImg++;
                        uploadOfflineImg();
                        //path = response;

                    } else {
                        rl_upload.setVisibility(View.GONE);

                    }

                }
            });

        }


    }

    @BindView(R.id.rlBsBusinessProfile)
    RelativeLayout rlBsBusinessProfile;

    @Override
    public void onBackPressed() {
        if (rl_upload.getVisibility() == View.VISIBLE) {

        } else if (llAddressList.getVisibility() == View.VISIBLE) {
            llAddressList.setVisibility(View.GONE);
        } else if (rlAddCategory.getVisibility() == View.VISIBLE) {
            hideAddCat();
        } else if (rlAddCategory2.getVisibility() == View.VISIBLE) {
            hideBT();
        } else if (rlAddCategory3.getVisibility() == View.VISIBLE) {
            hideEnterImgInfo();
        } else if (rlLink.getVisibility() == View.VISIBLE) {
            rlLink.setVisibility(View.GONE);
        } else if (ShareModel.getI().bus_setting_from.equals("profile")) { // only img
            super.onBackPressed();
        } else if (ShareModel.getI().bus_setting_from.equals("master")) { // only setting
            super.onBackPressed();
        } else if (btnUpdate.getText().equals("UPDATE")) {
            setVisible(true);
        } else super.onBackPressed();

    }

    public void setVisible(boolean isProfileVisible) { // Nifty Dialogue
        BaseEffects animator = Effectstype.Slidetop.getAnimator();
        animator.start(rlImg);
        animator.start(llBusSetInfo);


        if (ShareModel.getI().bus_setting_from.equals("signup")) {
            rlImg.setVisibility(isProfileVisible ? View.VISIBLE : View.GONE);
            llBusSetInfo.setVisibility(isProfileVisible ? View.GONE : View.VISIBLE);
            tvAddImg.setVisibility(isProfileVisible ? View.VISIBLE : View.GONE);
            llAddImg.setVisibility(isProfileVisible ? View.VISIBLE : View.GONE);
            btnUpdate.setText(isProfileVisible ? "NEXT" : "UPDATE");
        } else if (ShareModel.getI().bus_setting_from.equals("profile")) { // only img
            rlImg.setVisibility(View.VISIBLE);
            llBusSetInfo.setVisibility(View.GONE);
            tvAddImg.setVisibility(View.VISIBLE);
            llAddImg.setVisibility(View.VISIBLE);
            btnUpdate.setText("UPDATE");
        } else if (ShareModel.getI().bus_setting_from.equals("master")) { // only setting
            rlImg.setVisibility(View.GONE);
            llBusSetInfo.setVisibility(View.VISIBLE);
            tvAddImg.setVisibility(View.GONE);
            llAddImg.setVisibility(View.GONE);
            btnUpdate.setText("UPDATE");
        }


       /* if (mDuration != -1) {
            animator.setDuration(Math.abs(mDuration));
        }*/

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @BindView(R.id.etNm1)
    EditText etNm1;

    @BindView(R.id.etVal1)
    EditText etVal1;

    @BindView(R.id.etNm2)
    EditText etNm2;

    @BindView(R.id.etVal2)
    EditText etVal2;

    @BindView(R.id.etDelCharge)
    EditText etDelCharge;

    @BindView(R.id.etPckgCharge)
    EditText etPckgCharge;

    @BindView(R.id.etPckgChargeVal)
    EditText etPckgChargeVal;

    @BindView(R.id.etDelChargeVal)
    EditText etDelChargeVal;

    @BindView(R.id.tvDiscount)
    TextView tvDiscount;

    @BindView(R.id.etDisPercVal)
    EditText etDisPercVal;


    @BindView(R.id.tvBusinessType)
    TextView tvBusinessType;

    public void setTax(String field_json_data) throws JSONException {

        if (field_json_data != null) {

            String Tax = JsonObjParse.getValueEmpty(field_json_data, "Tax");
            if (!Tax.isEmpty()) {

                JSONObject joTax = new JSONObject(field_json_data);

                JSONArray arr = joTax.getJSONArray("Tax");

                JSONObject element;

                etVal2.setText("");
                etNm2.setText("");

                for (int i = 0; i < arr.length(); i++) {
                    element = arr.getJSONObject(i); // which for example will be Types,TotalPoints,ExpiringToday in the case of the first array(All_Details)

                    Iterator keys = element.keys();

                    while (keys.hasNext()) {
                        try {
                            String key = (String) keys.next();

                            String taxVal = JsonObjParse.getValueFromJsonObj(element, key);

                            if (etVal2.getText().toString().isEmpty()) {
                                etVal2.setText(taxVal);
                                etNm2.setText(key);
                            } else {
                                etVal1.setText(taxVal);
                                etNm1.setText(key);
                            }

                            Log.e("key", key);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

                /*  Charges  */

                arr = joTax.getJSONArray("charges");


                etDelCharge.setText("");
                etDelChargeVal.setText("");

                for (int i = 0; i < arr.length(); i++) {
                    element = arr.getJSONObject(i); // which for example will be Types,TotalPoints,ExpiringToday in the case of the first array(All_Details)

                    Iterator keys = element.keys();
                    while (keys.hasNext()) {
                        try {
                            String key = (String) keys.next();

                            String taxVal = JsonObjParse.getValueFromJsonObj(element, key);

                            if (etDelCharge.getText().toString().isEmpty()) {
                                etDelChargeVal.setText(taxVal);
                                etDelCharge.setText(key);
                            } else {
                                etPckgChargeVal.setText(taxVal);
                                etPckgCharge.setText(key);
                            }

                            Log.e("key", key);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

            }

            String dis_perc = JsonObjParse.getValueEmpty(field_json_data, "dis_perc");
            etDisPercVal.setText(dis_perc);

            String sms_customer = JsonObjParse.getValueEmpty(field_json_data, "sms_customer");

            if (sms_customer.equalsIgnoreCase("Yes")) {
                cbSmsCust.setChecked(true);
            } else {
                cbSmsCust.setChecked(false);
            }


            String sms_employee = JsonObjParse.getValueEmpty(field_json_data, "sms_employee");

            if (sms_employee.equalsIgnoreCase("Yes")) {
                cbSmsEmp.setChecked(true);
            } else {
                cbSmsEmp.setChecked(false);
            }

            String sms_delivery = JsonObjParse.getValueEmpty(field_json_data, "sms_delivery");

            if (sms_delivery.equalsIgnoreCase("Yes")) {
                cbSmsDelivery.setChecked(true);
            } else {
                cbSmsDelivery.setChecked(false);
            }

            String show_stock_position = JsonObjParse.getValueEmpty(field_json_data, "show_stock_position");

            if (show_stock_position.equalsIgnoreCase("Yes")) {
                cbShowStock.setChecked(true);
            } else {
                cbShowStock.setChecked(false);
            }

            setIsPickUp();

/*
            JSONArray jaFiled = new JSONArray(field_json_data);
            for (int i = 0; i < jaFiled.length(); i++) {
                JSONObject jo = jaFiled.getJSONObject(i);
                switch (i) {
                    case 0:
                        String f1Nm = jo.getString("Tax-name");
                        String f1Val = jo.getString("Tax-value");
                        etNm1.setText(f1Nm);
                        etVal1.setText(f1Val);
                        break;
                    case 1:
                        String f2Nm = jo.getString("Tax-name");
                        String f2Val = jo.getString("Tax-value");
                        etNm2.setText(f2Nm);
                        etVal2.setText(f2Val);
                        break;
                }
            }
*/

        }
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    public void setCurrencySymbol() {

        tvC1.setText("%");
        tvC2.setText("%");
        tvC3.setText(symbol_native);
        tvC5.setText(symbol_native);
        tvC6.setText(symbol_native);
        tvC4.setText(symbol_native);
        tvMinLbl.setText("Min. (" + symbol_native + ")");
        tvMaxLbl.setText("Max. (" + symbol_native + ")");
        tvDiscLbl.setText("Discount (%)");

        etPckgCharge.setSelected(true);
        etDelCharge.setSelected(true);
    }

    @BindView(R.id.tvC4)
    TextView tvC4;
    @BindView(R.id.tvC1)
    TextView tvC1;
    @BindView(R.id.tvC2)
    TextView tvC2;
    @BindView(R.id.tvC3)
    TextView tvC3;
    @BindView(R.id.tvC5)
    TextView tvC5;
    @BindView(R.id.tvC6)
    TextView tvC6;


    String business_type = "Business";


    @OnClick(R.id.ivCurrencyInfo)
    void ivCurrencyInfo(View v) {
        showInfo("Currency", "• You can select the currency at which you operate.\n• Catalogue option can be used to create your own catalogue under this currency.");
    }

    @OnClick(R.id.ivServiceType)
    void ivServiceType(View v) {
        showInfo("Service", "Chosen template will be visible to the consumer for placing their request.");
    }

    @OnClick(R.id.ivPayInfo)
    void ivPayInfo(View v) {
        showInfo("Payment Method", "• Online and COD payment methods are possible for Indian currency and shopping cart.\n• If you choose any other currency other than Indian currency or change the service type to Service, only COD option is enabled.");
    }

    @OnClick(R.id.ivTaxInfo)
    void ivTaxInfo(View v) {
        showInfo("Tax", "• Set  TAX NAME and the default TAX value here.\n• The Tax Value can be changed when the product entry is created in the catalogue.");
    }

    @OnClick(R.id.ivDiscountInfo)
    void ivDiscountInfo(View v) {
        showInfo("Discount", "The Discount Percentage can be changed when the product entry is created in the catalogue.");
    }

    @OnClick(R.id.ivSmsInfo)
    void ivSmsInfo(View v) {
        showInfo("SMS", "• SMS for Business - Business will receive a SMS when a new request " +
                "received\n" +
                "\n" +
                "• SMS for Customer - Customer will receive a notification. " +
                "My Task (open) to My Task ( ACT ) by performing a right swipe\n\n" +
                "• SMS for Employee - Employee will receive a notification. Long press a request in MY Task and assign a ticket to the employee");

    }

    @OnClick(R.id.ivBusinessInfo)
    void ivBusinessInfo(View v) {
        showInfo("Business Type", "• Business\n- You are capable of exposing your products / service and perform on-line sales\n- Share your registerd id to your Customer. They can request product / service using your catalogue." +
                "\n\n• Aggregator\n- Group multiple Business under one roof.\n- Share your registerd id to your Customer. They can access all the businesses and the respective caltalogue." +
                "\n\n• Circle\n- Group subset of businesses from one or more aggregator. Customer can access specific business under this circle.\n- Share your registerd id to your Customer. They can access all the businesses and its caltalogue." +
                "\n\n• Delivery\n- Business who wants to specialise on Delivery for others.\n- Business ID should be added to the below Delivery Section." +
                "");

    }

    @OnClick(R.id.ivDelInfo)
    void ivDelInfo(View v) {
        showInfo("Delivery", "• You can add Delivery Charges / packaging charges here." +
                "\n• When a customer placing an order, this cost will also include as part of the request." +
                "\n• You can add a Delivery partner who has been registered in the same platform." +
                "\n• Delivery Partner will receive a copy of the request placed by the customer." +
                "\n• By Setting up SMS, he will also be notified when you receive a request." +
                "");
    }

    @OnClick(R.id.ivInfoShare)
    void ivInfoShare(View v) {
        showInfo("Share", "• You can share your user id to your consumer using share icon in Menu option.\n• If you want to share your aggregator id instead of yours, then you may please change it here.");
    }


    PlayAnim playAnim = new PlayAnim();

    @BindView(R.id.llAnimAddcat)
    LinearLayout llAnimAddcat;

    @BindView(R.id.tvAddEditTitle)
    TextView tvAddEditTitle;

    @BindView(R.id.tvAdd)
    TextView tvAdd;

    @BindView(R.id.tvCancel)
    TextView tvCancel;

    @BindView(R.id.tvMsg)
    TextView tvMsg;

    @BindView(R.id.rlAddCategory)
    RelativeLayout rlAddCategory;

    @OnClick(R.id.tvCancel)
    public void tvCancel(View v) {
        hideAddCat();
    }

    String validateBusinessId = "";


    @OnClick(R.id.tvAdd)
    public void tvAdd(View v) {

        if (et2.getVisibility() == View.VISIBLE) {
            setAftervalidate();
        } else if (tvCancel.getVisibility() == View.VISIBLE && tvAddEditTitle.getText().toString().contains("Share")) {

            validateBusinessId = et1.getText().toString().trim();

            if (validateBusinessId.isEmpty()) {
                setAftervalidate();
            } else {
                loadImg.loadFitImg(EditCurrencyForBusinessActivity.this, tvAdd);
                validateBusinessApi();
            }


        } else if (tvCancel.getVisibility() == View.VISIBLE && tvAddEditTitle.getText().toString().contains("Delivery")) {

            validateBusinessId = et1.getText().toString().trim();


            if (validateBusinessId.isEmpty()) {
                setAftervalidate();
            } else {
                loadImg.loadFitImg(EditCurrencyForBusinessActivity.this, tvAdd);
                validateBusinessApi();

            }


        } else {


            hideAddCat();


        }


    }


    public void hideAddCat() {

        playAnim.slideUpRl(llAnimAddcat);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                rlAddCategory.setVisibility(View.GONE);
                Utils.hideSoftKeyboard(EditCurrencyForBusinessActivity.this, et1);

            }
        }, 700);


    }

    public void showInfo(String title, String msg) {

        playAnim.slideDownRl(llAnimAddcat);
        rlAddCategory.setVisibility(View.VISIBLE);

        tvAddEditTitle.setText(title);
        tvAdd.setText("Ok");

        tvAdd.setBackground(ContextCompat.getDrawable(this, R.drawable.roundcorner_color));

        tvCancel.setVisibility(View.GONE);
        tvMsg.setText(msg);
        tvMsg.setVisibility(View.VISIBLE);

        tilHint.setVisibility(View.GONE);
        tilMobile.setVisibility(View.GONE);
        et1.setVisibility(View.GONE);
        et2.setVisibility(View.GONE);
        if (msg.isEmpty()) { // editable
            tilHint.setVisibility(View.VISIBLE);
            et1.setVisibility(View.VISIBLE);
            tvCancel.setVisibility(View.VISIBLE);
            et1.requestFocus();
            if (title.contains("Share")) {
                tilHint.setHint("Entre share id");
                et1.setText(etShareId.getText().toString());
            } else {
                tilHint.setHint("Entre delivery partner id");
                et1.setText(etDelPartner.getText().toString());
            }
            et1.setSelection(et1.getText().length());
            Utils.showSoftKeyboard(this);


        }


    }

    @BindView(R.id.tvShoppingCartLink)
    TextView tvShoppingCartLink;


    @OnClick(R.id.tvShoppingCartLink)
    public void tvShoppingCartLink(View v) {
        hideAddCat();
        loadImg("https://s3-ap-southeast-1.amazonaws.com/chitandbridge.com/Digimart/Screenshot_2018-11-23-17-36-09-23.png");
    }

    @BindView(R.id.tvTextLink)
    TextView tvTextLink;

    @OnClick(R.id.tvTextLink)
    public void tvTextLink(View v) {
        loadImg("https://s3-ap-southeast-1.amazonaws.com/chitandbridge.com/Digimart/Screenshot_2018-11-23-17-36-09-23.png");
    }

    @BindView(R.id.tvImgLink)
    TextView tvImgLink;

    @OnClick(R.id.tvImgLink)
    public void tvImgLink(View v) {
        //loadImg("https://s3-ap-southeast-1.amazonaws.com/chitandbridge.com/Digimart/Screenshot_2018-11-23-17-36-09-23.png");
    }

    @BindView(R.id.tvServiceLink)
    TextView tvServiceLink;

    @BindView(R.id.rlLink)
    RelativeLayout rlLink;

    @OnClick(R.id.tvServiceLink)
    public void tvServiceLink(View v) {
        loadImg("https://s3-ap-southeast-1.amazonaws.com/chitandbridge.com/Digimart/Screenshot_2018-11-23-17-40-19-97.png");
    }

    @BindView(R.id.ivImg)
    ImageView ivImg;

    public void loadImg(String url) {
        rlLink.setVisibility(View.VISIBLE);
        loadImg.loadFitImg(EditCurrencyForBusinessActivity.this, url, ivImg);

    }

    @OnClick(R.id.rlLink)
    public void rlLink(View v) {

    }

    @OnClick(R.id.rlAddCategory)
    public void rlAddCategory(View v) {
        hideAddCat();
    }

    // Share Section

    @BindView(R.id.etShareId)
    TextView etShareId;

    @BindView(R.id.tvDelMobile)
    TextView tvDelMobile;

    @BindView(R.id.tvDelCustNm)
    TextView tvDelCustNm;

    @BindView(R.id.etDelPartner)
    TextView etDelPartner;

    @BindView(R.id.tilHint)
    TextInputLayout tilHint;

    @BindView(R.id.et1)
    EditText et1;

    @BindView(R.id.tilMobile)
    TextInputLayout tilMobile;

    @BindView(R.id.et2)
    EditText et2;

    @BindView(R.id.et3)
    EditText et3;

    @OnClick(R.id.llEditShare)
    public void llEditShare(View v) {
        showInfo("Share", "");
    }

    @OnClick(R.id.llDelPatner)
    public void llDelPatner(View v) {
        showInfo("Delivery partner", "");
    }

    public void setShareUserId(String field_json_data) {

        if (field_json_data != null) {
            String shareId = JsonObjParse.getValueEmpty(field_json_data, "shareId");
            if (!shareId.isEmpty()) {
                etShareId.setText(shareId);
            }

            String delivery_partner_info = JsonObjParse.getValueEmpty(field_json_data, "delivery_partner_info");
            String mobile_delivery = JsonObjParse.getValueEmpty(field_json_data, "mobile_delivery");
            String customer_nm_delivery = JsonObjParse.getValueEmpty(field_json_data, "customer_nm_delivery");
            if (!delivery_partner_info.isEmpty()) {
                etDelPartner.setText(delivery_partner_info);
                tvDelMobile.setText(mobile_delivery);
                tvDelCustNm.setText(customer_nm_delivery);
            }
        }
    }

    public void validateBusinessApi() {


        new GetServiceTypeFromBusiUserProfile(this, validateBusinessId, new GetServiceTypeFromBusiUserProfile.Apicallback() {
            @Override
            public void onGetResponse(String a, String response, String sms_emp) {
                if (response.isEmpty()) {

                    tvAdd.setText("Ok");
                    tvAdd.setBackground(ContextCompat.getDrawable(EditCurrencyForBusinessActivity.this, R.drawable.roundcorner_color));


                    Inad.alerter("Alert !", "Please enter valid id..", EditCurrencyForBusinessActivity.this);
                } else if (tvAddEditTitle.getText().toString().contains("Share")) {

                    setAftervalidate();

                } else if (tvAddEditTitle.getText().toString().contains("Delivery")) { // for moboile no
                    try {

                        tvAdd.setText("Ok");
                        tvAdd.setBackground(ContextCompat.getDrawable(EditCurrencyForBusinessActivity.this, R.drawable.roundcorner_color));

                        et2.setVisibility(View.VISIBLE);
                        tilMobile.setVisibility(View.VISIBLE);

                        JSONObject Jsonresponse = new JSONObject(response);
                        JSONObject Jsonresponseinfo = Jsonresponse.getJSONObject("response");
                        JSONArray jaData = Jsonresponseinfo.getJSONArray("data");
                        if (jaData.length() > 0) {
                            JSONObject data = jaData.getJSONObject(0);
                            String contact_no = JsonObjParse.getValueEmpty(data.toString(), "contact_no");
                            String firstname = JsonObjParse.getValueEmpty(data.toString(), "firstname");
                            et2.setText(contact_no);
                            et3.setText(firstname);
                        } else {

                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {

                }
            }
        });
    }

    // cbInfoReqForAggreAndCircle

    @BindView(R.id.cbInfoReqForAggreAndCircle)
    CheckBox cbInfoReqForAggreAndCircle;

    String pass_info_aggregator = "", pass_info_circle = "";

    public void setPassInfoReqForAggreAndCircle(String businessType) {
        // isReceiveCopyOfTheRequest
        if (businessType.equalsIgnoreCase("business")) {
            cbInfoReqForAggreAndCircle.setVisibility(View.GONE);

        } else { // sign up(first time) , aggregator, circle
            cbInfoReqForAggreAndCircle.setVisibility(View.VISIBLE);
        }

        if (field_json_data != null) {
            pass_info_circle = JsonObjParse.getValueEmpty(field_json_data, "pass_info_circle");
            pass_info_aggregator = JsonObjParse.getValueEmpty(field_json_data, "pass_info_aggregator");
        }


        cbInfoReqForAggreAndCircle.setChecked(false);
        if (business_type.equalsIgnoreCase("aggregator") && pass_info_aggregator.equalsIgnoreCase("yes"))
            cbInfoReqForAggreAndCircle.setChecked(true);
        if (business_type.equalsIgnoreCase("circle") && pass_info_circle.equalsIgnoreCase("yes"))
            cbInfoReqForAggreAndCircle.setChecked(true);
        if (businessType.isEmpty()) cbInfoReqForAggreAndCircle.setChecked(true);  // sign up

    }


    // Show Stock
    @BindView(R.id.cbShowStock)
    CheckBox cbShowStock;

    @BindView(R.id.cbIsPickUpOnly)
    CheckBox cbIsPickUpOnly;

    public void setIsPickUp() {

        cbIsPickUpOnly.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                inActiveDelivery(b);
            }
        });


        String pick_up_only = JsonObjParse.getValueEmpty(field_json_data, "pick_up_only");

        if (pick_up_only.equalsIgnoreCase("Yes")) {
            cbIsPickUpOnly.setChecked(true);
        } else {
            cbIsPickUpOnly.setChecked(false);
        }


    }

    @BindView(R.id.llDeliveryBlock)
    LinearLayout llDeliveryBlock;

    @BindView(R.id.llDelPatner)
    LinearLayout llDelPatner;

    @BindView(R.id.ivEdiDel)
    ImageView ivEdiDel;

    @BindView(R.id.tvPid)
    TextView tvPid;

    @BindView(R.id.tvCNo)
    TextView tvCNo;


    public void inActiveDelivery(boolean isActive) {
        isActive = !isActive;
        etDelCharge.setEnabled(isActive);
        etDelChargeVal.setEnabled(isActive);
        etPckgCharge.setEnabled(isActive);
        etPckgChargeVal.setEnabled(isActive);
        llDelPatner.setEnabled(isActive);
        cbSmsDelivery.setEnabled(isActive);
        tvDelMobile.setEnabled(isActive);
        etDelPartner.setEnabled(isActive);
        etDelPartner.setEnabled(isActive);
        ivEdiDel.setEnabled(isActive);
        tvPid.setEnabled(isActive);
        tvCNo.setEnabled(isActive);
    }

    // Condition
    @BindView(R.id.etEntrCondition)
    EditText etEntrCondition;

    @BindView(R.id.cbRoundOff)
    CheckBox cbRoundOff;

    @OnClick(R.id.ivConditionsInfo)
    public void ivConditionsInfo() {
        loadImg("https://s3-ap-southeast-1.amazonaws.com/chitandbridge.com/Digimart/image001.png");
    }

    @BindView(R.id.cbUserTraction)
    CheckBox cbUserTraction;

    @BindView(R.id.cbMrp)
    CheckBox cbMrp;

    @BindView(R.id.cbPromo)
    CheckBox cbPromo;

    public void setCondition(String field_json_data) {

        if (field_json_data != null) {
            String conditions = JsonObjParse.getValueEmpty(field_json_data, "conditions");
            if (!conditions.isEmpty()) {
                etEntrCondition.setText(conditions + "");
            }
            cbRoundOff.setChecked(false);
            String is_roundoff_near = JsonObjParse.getValueEmpty(field_json_data, "is_roundoff_near");
            if (is_roundoff_near.equalsIgnoreCase("yes")) {
                cbRoundOff.setChecked(true);
            }

            cbUserTraction.setChecked(false);
            String is_user_traction = JsonObjParse.getValueEmpty(field_json_data, "is_user_traction");
            if (is_user_traction.equalsIgnoreCase("yes")) {
                cbUserTraction.setChecked(true);
            }

            cbMrp.setChecked(false);
            String is_mrp = JsonObjParse.getValueEmpty(field_json_data, "is_mrp");
            if (is_mrp.equalsIgnoreCase("yes")) {
                cbMrp.setChecked(true);
            }

            cbPromo.setChecked(false);
            String is_promo = JsonObjParse.getValueEmpty(field_json_data, "is_promo");
            if (is_promo.equalsIgnoreCase("yes")) {
                cbPromo.setChecked(true);
            }

        }
    }

    @BindView(R.id.rbUploadPdf)
    RadioButton rbUploadPdf;

    @BindView(R.id.rbNone)
    RadioButton rbNone;

    @BindView(R.id.rbSurvey)
    RadioButton rbSurvey;

    @BindView(R.id.rbJewel)
    RadioButton rbJewel;

    @BindView(R.id.rbDental)
    RadioButton rbDental;

    @BindView(R.id.rbDelivery)
    RadioButton rbDelivery;

    @BindView(R.id.llJewelInfo)
    LinearLayout llJewelInfo;

    @BindView(R.id.tvUploadPdf)
    TextView tvUploadPdf;

    @BindView(R.id.tvPdfNm)
    TextView tvPdfNm;

    @BindView(R.id.tv_progress)
    TextView tv_progress;

    @BindView(R.id.rl_upload)
    RelativeLayout rl_upload;

    // Web link

    @BindView(R.id.rbWebLink)
    RadioButton rbWebLink;

    @BindView(R.id.llWebLink)
    LinearLayout llWebLink;

    @BindView(R.id.etWebLink)
    EditText etWebLink;

    @BindView(R.id.etDelivery)
    EditText etDelivery;

    @BindView(R.id.llDelivery)
    LinearLayout llDelivery;

    @BindView(R.id.etDiscInfo)
    EditText etDiscInfo;


    // Sms only

    @BindView(R.id.rbSentSms)
    RadioButton rbSentSms;

    @BindView(R.id.tvSmsOnly)
    TextView tvSmsOnly;

    @BindView(R.id.rbWholeSale)
    RadioButton rbWholeSale;

    @BindView(R.id.rbProperty)
    RadioButton rbProperty;


    @OnClick(R.id.tvUploadPdf)
    public void tvUploadPdf(View v) {
      /*  Intent intent = new Intent();
        intent.setType("application/pdf");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivityForResult(Intent.createChooser(intent, "Select Pdf"), 40);*/



      /*  Intent pictureActionIntent = null;
        pictureActionIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(pictureActionIntent, 40);*/

       /* Intent i = new Intent(EditCurrencyForBusinessActivity.this, FileNewActivity.class);
        i.putExtra("isdoc", true);
        startActivityForResult(i, 40);*/

        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
        startActivityForResult(intent, 40);


    }

    public void radioGroupCheckedChange() {

        rbService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                tvPdfNm.setVisibility(View.GONE);
                tvUploadPdf.setVisibility(View.GONE);
                llMinMax.setVisibility(View.GONE);

                llWebLink.setVisibility(View.GONE);
                llDelivery.setVisibility(View.GONE);
                rbWebLink.setChecked(false);
                rbNone.setChecked(false);

                rbJewel.setChecked(false);
                rbDental.setChecked(false);
                rbDelivery.setChecked(false);
                llJewelInfo.setVisibility(View.GONE);


                rbSentSms.setChecked(false);
                rbService.setChecked(true);
                rbShoppingCart.setChecked(false);
                rbCatalogueOnly.setChecked(false);
                rbUploadPdf.setChecked(false);
                rbShoppingCartWithCb.setChecked(false);
                rbWholeSale.setChecked(false);
                rbProperty.setChecked(false);
                rbSurvey.setChecked(false);

                service_type_of = "Service";
                setServiceType("service");

            }
        });

        rbCatalogueOnly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                tvPdfNm.setVisibility(View.GONE);
                tvUploadPdf.setVisibility(View.GONE);
                llMinMax.setVisibility(View.GONE);

                llWebLink.setVisibility(View.GONE);
                llDelivery.setVisibility(View.GONE);
                rbWebLink.setChecked(false);
                rbNone.setChecked(false);

                rbJewel.setChecked(false);
                rbDental.setChecked(false);
                llJewelInfo.setVisibility(View.GONE);


                serviceTypeString = getString(R.string.catalogue_only);
                rbCatalogueOnly.setChecked(true);
                rbShoppingCart.setChecked(false);
                rbService.setChecked(false);
                rbUploadPdf.setChecked(false);
                rbSentSms.setChecked(false);
                rbShoppingCartWithCb.setChecked(false);
                rbWholeSale.setChecked(false);
                rbProperty.setChecked(false);
                rbSurvey.setChecked(false);


                service_type_of = getString(R.string.catalogue_only);

                //if(serviceTypeString.equalsIgnoreCase(R.string.upload_pdf))

            }
        });

        rbShoppingCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                tvPdfNm.setVisibility(View.GONE);
                tvUploadPdf.setVisibility(View.GONE);
                llMinMax.setVisibility(View.GONE);

                llWebLink.setVisibility(View.GONE);
                llDelivery.setVisibility(View.GONE);
                rbWebLink.setChecked(false);

                rbJewel.setChecked(false);
                rbDental.setChecked(false);
                rbDelivery.setChecked(false);
                llJewelInfo.setVisibility(View.GONE);


                rbNone.setChecked(false);

                rbShoppingCart.setChecked(true);
                rbService.setChecked(false);
                rbCatalogueOnly.setChecked(false);
                rbUploadPdf.setChecked(false);
                rbSentSms.setChecked(false);
                rbShoppingCartWithCb.setChecked(false);
                rbWholeSale.setChecked(false);
                rbProperty.setChecked(false);
                rbSurvey.setChecked(false);

                setServiceType(field_json_data);

                service_type_of = getString(R.string.shopping_cart);

            }
        });

        rbUploadPdf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                tvPdfNm.setVisibility(View.VISIBLE);
                tvUploadPdf.setVisibility(View.VISIBLE);
                llMinMax.setVisibility(View.GONE);

                llWebLink.setVisibility(View.GONE);
                llDelivery.setVisibility(View.GONE);
                rbWebLink.setChecked(false);

                rbJewel.setChecked(false);
                rbDental.setChecked(false);
                rbDelivery.setChecked(false);
                llJewelInfo.setVisibility(View.GONE);


                serviceTypeString = getString(R.string.upload_pdf);
                rbNone.setChecked(false);

                rbUploadPdf.setChecked(true);
                rbShoppingCart.setChecked(false);
                rbService.setChecked(false);
                rbCatalogueOnly.setChecked(false);
                rbSentSms.setChecked(false);
                rbShoppingCartWithCb.setChecked(false);
                rbWholeSale.setChecked(false);
                rbProperty.setChecked(false);
                rbSurvey.setChecked(false);

                service_type_of = getString(R.string.upload_pdf);


            }
        });

        rbWebLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                tvPdfNm.setVisibility(View.GONE);
                tvUploadPdf.setVisibility(View.GONE);
                llMinMax.setVisibility(View.GONE);
                llWebLink.setVisibility(View.VISIBLE);
                llDelivery.setVisibility(View.GONE);

                //serviceTypeString = getString(R.string.shopping_cart_cb);
                rbNone.setChecked(false);
                rbJewel.setChecked(false);
                rbDental.setChecked(false);
                rbDelivery.setChecked(false);
                llJewelInfo.setVisibility(View.GONE);


                rbUploadPdf.setChecked(false);
                rbShoppingCart.setChecked(false);
                rbService.setChecked(false);
                rbCatalogueOnly.setChecked(false);
                rbShoppingCartWithCb.setChecked(false);
                rbSentSms.setChecked(false);
                rbWebLink.setChecked(true);
                rbWholeSale.setChecked(false);
                rbProperty.setChecked(false);
                rbSurvey.setChecked(false);


                serviceTypeString = getString(R.string.link_to_web);
                service_type_of = getString(R.string.link_to_web);


            }
        });

        rbShoppingCartWithCb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                tvPdfNm.setVisibility(View.GONE);
                tvUploadPdf.setVisibility(View.GONE);
                llMinMax.setVisibility(View.VISIBLE);

                llWebLink.setVisibility(View.GONE);
                llDelivery.setVisibility(View.GONE);
                rbWebLink.setChecked(false);
                //serviceTypeString = getString(R.string.shopping_cart_cb);
                rbNone.setChecked(false);
                rbJewel.setChecked(false);
                rbDental.setChecked(false);
                rbDelivery.setChecked(false);
                llJewelInfo.setVisibility(View.GONE);


                rbUploadPdf.setChecked(false);
                rbShoppingCart.setChecked(false);
                rbService.setChecked(false);
                rbCatalogueOnly.setChecked(false);
                rbSentSms.setChecked(false);
                rbShoppingCartWithCb.setChecked(true);
                rbWholeSale.setChecked(false);
                rbProperty.setChecked(false);
                rbSurvey.setChecked(false);

                setServiceType(field_json_data);

                service_type_of = getString(R.string.shopping_cart_cb);


            }
        });

        rbSentSms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                tvPdfNm.setVisibility(View.GONE);
                tvUploadPdf.setVisibility(View.GONE);
                llMinMax.setVisibility(View.GONE);
                llWebLink.setVisibility(View.GONE);
                llDelivery.setVisibility(View.GONE);

                //serviceTypeString = getString(R.string.shopping_cart_cb);
                rbNone.setChecked(false);
                rbJewel.setChecked(false);
                rbDental.setChecked(false);
                rbDelivery.setChecked(false);
                llJewelInfo.setVisibility(View.GONE);


                rbUploadPdf.setChecked(false);
                rbShoppingCart.setChecked(false);
                rbService.setChecked(false);
                rbCatalogueOnly.setChecked(false);
                rbShoppingCartWithCb.setChecked(false);
                rbWebLink.setChecked(false);
                rbSentSms.setChecked(true);
                rbWholeSale.setChecked(false);
                rbProperty.setChecked(false);
                rbSurvey.setChecked(false);

                serviceTypeString = getString(R.string.sms_only);
                service_type_of = getString(R.string.sms_only);


            }
        });

        rbWholeSale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                tvPdfNm.setVisibility(View.GONE);
                tvUploadPdf.setVisibility(View.GONE);
                llMinMax.setVisibility(View.GONE);
                llWebLink.setVisibility(View.GONE);
                llDelivery.setVisibility(View.GONE);

                //serviceTypeString = getString(R.string.shopping_cart_cb);
                rbNone.setChecked(false);


                rbUploadPdf.setChecked(false);
                rbShoppingCart.setChecked(false);
                rbService.setChecked(false);
                rbCatalogueOnly.setChecked(false);
                rbShoppingCartWithCb.setChecked(false);
                rbWebLink.setChecked(false);
                rbSentSms.setChecked(false);
                rbProperty.setChecked(false);
                rbWholeSale.setChecked(true);
                rbSurvey.setChecked(false);
                rbJewel.setChecked(false);
                rbDental.setChecked(false);
                rbDelivery.setChecked(false);
                llJewelInfo.setVisibility(View.GONE);


                setServiceType(field_json_data);


                service_type_of = getString(R.string.wholesale);


            }
        });


        rbProperty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                tvPdfNm.setVisibility(View.GONE);
                tvUploadPdf.setVisibility(View.GONE);
                llMinMax.setVisibility(View.GONE);
                llWebLink.setVisibility(View.GONE);
                llDelivery.setVisibility(View.GONE);

                //serviceTypeString = getString(R.string.shopping_cart_cb);

                rbNone.setChecked(false);

                rbUploadPdf.setChecked(false);
                rbShoppingCart.setChecked(false);
                rbService.setChecked(false);
                rbCatalogueOnly.setChecked(false);
                rbShoppingCartWithCb.setChecked(false);
                rbWebLink.setChecked(false);
                rbSentSms.setChecked(false);
                rbWholeSale.setChecked(false);
                rbProperty.setChecked(true);
                rbSurvey.setChecked(false);
                rbJewel.setChecked(false);
                rbDental.setChecked(false);
                rbDelivery.setChecked(false);
                llJewelInfo.setVisibility(View.GONE);


                setServiceType(field_json_data);


                service_type_of = getString(R.string.property);


            }
        });

        rbNone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                tvPdfNm.setVisibility(View.GONE);
                tvUploadPdf.setVisibility(View.GONE);
                llMinMax.setVisibility(View.GONE);
                llWebLink.setVisibility(View.GONE);
                llDelivery.setVisibility(View.GONE);

                //serviceTypeString = getString(R.string.shopping_cart_cb);


                rbUploadPdf.setChecked(false);
                rbShoppingCart.setChecked(false);
                rbService.setChecked(false);
                rbCatalogueOnly.setChecked(false);
                rbShoppingCartWithCb.setChecked(false);
                rbSentSms.setChecked(false);
                rbNone.setChecked(true);
                rbWebLink.setChecked(false);
                rbWholeSale.setChecked(false);
                rbProperty.setChecked(false);
                rbSurvey.setChecked(false);

                rbJewel.setChecked(false);
                rbDental.setChecked(false);
                rbDelivery.setChecked(false);
                llJewelInfo.setVisibility(View.GONE);


                serviceTypeString = getString(R.string.none);
                service_type_of = getString(R.string.none);


            }
        });

        rbSurvey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                tvPdfNm.setVisibility(View.GONE);
                tvUploadPdf.setVisibility(View.GONE);
                llMinMax.setVisibility(View.GONE);
                llWebLink.setVisibility(View.GONE);
                llDelivery.setVisibility(View.GONE);

                //serviceTypeString = getString(R.string.shopping_cart_cb);


                rbUploadPdf.setChecked(false);
                rbShoppingCart.setChecked(false);
                rbService.setChecked(false);
                rbCatalogueOnly.setChecked(false);
                rbShoppingCartWithCb.setChecked(false);
                rbSentSms.setChecked(false);
                rbNone.setChecked(false);
                rbWebLink.setChecked(false);
                rbWholeSale.setChecked(false);
                rbProperty.setChecked(false);
                rbSurvey.setChecked(true);
                rbJewel.setChecked(false);
                rbDental.setChecked(false);
                rbDelivery.setChecked(false);
                llJewelInfo.setVisibility(View.GONE);

                serviceTypeString = getString(R.string.survey);
                service_type_of = getString(R.string.survey);

                setBpsForSurvey();

            }
        });


        rbJewel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                tvPdfNm.setVisibility(View.GONE);
                tvUploadPdf.setVisibility(View.GONE);
                llMinMax.setVisibility(View.GONE);
                llWebLink.setVisibility(View.GONE);
                llDelivery.setVisibility(View.GONE);
                llJewelInfo.setVisibility(View.VISIBLE);

                //serviceTypeString = getString(R.string.shopping_cart_cb);


                rbUploadPdf.setChecked(false);
                rbShoppingCart.setChecked(false);
                rbService.setChecked(false);
                rbCatalogueOnly.setChecked(false);
                rbShoppingCartWithCb.setChecked(false);
                rbSentSms.setChecked(false);
                rbNone.setChecked(false);
                rbWebLink.setChecked(false);
                rbWholeSale.setChecked(false);
                rbProperty.setChecked(false);
                rbSurvey.setChecked(false);
                rbJewel.setChecked(true);


                setServiceType(field_json_data);
                service_type_of = getString(R.string.jewel);

            }
        });

        rbDental.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvPdfNm.setVisibility(View.GONE);
                tvUploadPdf.setVisibility(View.GONE);
                llMinMax.setVisibility(View.GONE);

                llWebLink.setVisibility(View.GONE);
                llDelivery.setVisibility(View.GONE);
                rbWebLink.setChecked(false);
                rbNone.setChecked(false);

                rbJewel.setChecked(false);
                rbService.setChecked(false);
                llJewelInfo.setVisibility(View.GONE);


                rbSentSms.setChecked(false);
                rbDental.setChecked(true);
                rbDelivery.setChecked(false);
                rbShoppingCart.setChecked(false);
                rbCatalogueOnly.setChecked(false);
                rbUploadPdf.setChecked(false);
                rbShoppingCartWithCb.setChecked(false);
                rbWholeSale.setChecked(false);
                rbProperty.setChecked(false);
                rbSurvey.setChecked(false);

                service_type_of = getString(R.string.dental);
                serviceTypeString = getString(R.string.dental);
            }
        });
        rbDelivery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvPdfNm.setVisibility(View.GONE);
                tvUploadPdf.setVisibility(View.GONE);
                llMinMax.setVisibility(View.GONE);

                llWebLink.setVisibility(View.GONE);
                llDelivery.setVisibility(View.VISIBLE);
                rbWebLink.setChecked(false);
                rbNone.setChecked(false);

                rbJewel.setChecked(false);
                rbService.setChecked(false);
                llJewelInfo.setVisibility(View.GONE);


                rbSentSms.setChecked(false);
                rbDental.setChecked(false);
                rbDelivery.setChecked(true);
                rbShoppingCart.setChecked(false);
                rbCatalogueOnly.setChecked(false);
                rbUploadPdf.setChecked(false);
                rbShoppingCartWithCb.setChecked(false);
                rbWholeSale.setChecked(false);
                rbProperty.setChecked(false);
                rbSurvey.setChecked(false);

                service_type_of = getString(R.string.delivery);
                serviceTypeString = getString(R.string.delivery);
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to ak aris

        if (requestCode == 40 && resultCode == RESULT_OK) {

            //String picturePath = data.getExtras().getString("al_selet");
            String picturePath = null;
            try {
                File file = FileHelper.getFileFromUri(EditCurrencyForBusinessActivity.this, data.getData());
                picturePath = file.getAbsolutePath();

                if (picturePath.endsWith("pdf") || picturePath.endsWith(".png") || picturePath.endsWith(".jpg") || picturePath.endsWith(".jpeg")) {
                    setPdfNm(picturePath);
                } else {
                    Toast.makeText(EditCurrencyForBusinessActivity.this, "Please select pdf/image", Toast.LENGTH_LONG).show();
                }

            } catch (Exception e) {
                Toast.makeText(EditCurrencyForBusinessActivity.this, "File not found", Toast.LENGTH_LONG).show();

                e.printStackTrace();
            }

        } else if (resultCode == -1 && requestCode == 8) {
          /*  String picturePath = data.getExtras().getString("al_selet");
            ArrayList<ModelFile> al_temp_selet = new Gson().fromJson(picturePath, new com.google.gson.reflect.TypeToken<ArrayList<ModelFile>>() {
            }.getType());
            al_temp_selet.addAll(al_selet);
            selectFileAdapter.assignItem(al_temp_selet);
            al_selet = new ArrayList<>();
            al_selet = al_temp_selet;
            setPager();*/
        } else if (requestCode == 98 && resultCode == -1) {
            ArrayList<ModelFile> al_temp_selet = new ArrayList<>();
            ArrayList<String> pathList = data.getExtras().getStringArrayList(PickImageActivity.KEY_DATA_RESULT);
            if (pathList != null && !pathList.isEmpty()) {
                for (int i = 0; i < pathList.size(); i++) {
                    al_temp_selet.add(new ModelFile(pathList.get(i), true));
                }
            }

            //String picturePath = data.getExtras().getString("al_selet");
            //ArrayList<ModelFile> al_temp_selet = new Gson().fromJson(picturePath, new com.google.gson.reflect.TypeToken<ArrayList<ModelFile>>() {}.getType());
            al_temp_selet.addAll(al_selet);
            shopImgFileAdapter.assignItem(al_temp_selet);
            al_selet = new ArrayList<>();
            al_selet = al_temp_selet;
            setPager();


        } else if (resultCode == RESULT_OK) {
            // Compressing

            showGalleryCamera.onGetResult(requestCode, resultCode, data, new ShowGalleryCamera.CallGetImg() {
                @Override
                public void onGetImg(String str) {
                    // get Compressed
                    path = str;

                    ArrayList<ModelFile> al_temp_selet = new ArrayList<>();
                    al_temp_selet.add(new ModelFile(path, true));
                    //String picturePath = data.getExtras().getString("al_selet");
                    //ArrayList<ModelFile> al_temp_selet = new Gson().fromJson(picturePath, new com.google.gson.reflect.TypeToken<ArrayList<ModelFile>>() {}.getType());
                    al_temp_selet.addAll(al_selet);
                    shopImgFileAdapter.assignItem(al_temp_selet);
                    al_selet = new ArrayList<>();
                    al_selet = al_temp_selet;
                    setPager();


                    loadImg.loadImg(EditCurrencyForBusinessActivity.this, new File(path).getAbsolutePath(), ivCopmanyProfile);
                }
            });
        }


        super.onActivityResult(requestCode, resultCode, data);

    }

    public void setPdfNm(String Path) {


        tvPdfNm.setPaintFlags(tvPdfNm.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        this.picturePath = Path;
        //tvPdfNm.setSelected(true);
        int pos = Path.lastIndexOf("/");
        if (pos > 0) {
            Path = Path.substring(pos + 1);
        }

        tvPdfNm.setTextColor(ContextCompat.getColor(this, R.color.green));
        //File pathExt = new File(picturePath);

        tvPdfNm.setText(Path);
    }

    String picturePath = "", min = "", max = "";


    @OnClick(R.id.tvPdfNm)
    public void tvPdfNm(View v) {

        File file = new File(picturePath);
        if (file.exists()) {

            Intent target = new Intent(Intent.ACTION_VIEW);

            if (picturePath.endsWith(".png") || picturePath.endsWith(".jpg") || picturePath.endsWith(".jpeg")) {
                target.setDataAndType(Uri.fromFile(file), "image/*");

            } else { // pdf

                target.setDataAndType(Uri.fromFile(file), "application/pdf"); //

            }


            target.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            Intent intent = Intent.createChooser(target, "Open File");
            try {
                startActivity(intent);
            } catch (ActivityNotFoundException e) {
                // Instruct the user to install a PDF reader here, or something
            }

        } else if (picturePath.contains("http")) {
            Intent i = new Intent(this, WebViewActivity.class);
            i.putExtra("url", picturePath);
            i.putExtra("title", tvPdfNm.getText().toString());
            startActivity(i);
        } else {

        }
    }

    // Location

    AutoCompleteTextView tv_google_loc;
    String lati = "", longi = "";
    private GoogleApiClient mGoogleApiClient;
    private PlaceArrayAdapter mPlaceArrayAdapter;
    private static final LatLngBounds BOUNDS_MOUNTAIN_VIEW = new LatLngBounds(
            new LatLng(37.398160, -122.180831), new LatLng(37.430610, -121.972090));
    private static final int GOOGLE_API_CLIENT_ID = 0;

    private AdapterView.OnItemClickListener mAutocompleteClickListener
            = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            final PlaceArrayAdapter.PlaceAutocomplete item = mPlaceArrayAdapter.getItem(position);
            final String placeId = String.valueOf(item.placeId);
            //Log.i(LOG_TAG, "Selected: " + item.description);
            PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                    .getPlaceById(mGoogleApiClient, placeId);
            placeResult.setResultCallback(mUpdatePlaceDetailsCallback);
            rlPb.setVisibility(View.VISIBLE);
            getLocationFromAddress(tv_google_loc.getText().toString().trim());
            Utils.hideSoftKeyboard(EditCurrencyForBusinessActivity.this, et1);

            //Log.i(LOG_TAG, "Fetching details for ID: " + item.placeId);
        }
    };

    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {

                return;
            }
            // Selecting the first object buffer.
            final Place place = places.get(0);
            CharSequence attributions = places.getAttributions();


            //tv_google_loc.setText(Html.fromHtml(place.getAddress() + ""));

        }
    };

    public void initLoc() {
        tv_google_loc = (AutoCompleteTextView) findViewById(R.id.tv_google_loc);

        tv_google_loc.setThreshold(1);
        tv_google_loc.setOnItemClickListener(mAutocompleteClickListener);


        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Places.GEO_DATA_API)
                .enableAutoManage(this, GOOGLE_API_CLIENT_ID, new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

                    }
                })
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(@Nullable Bundle bundle) {
                        mPlaceArrayAdapter.setGoogleApiClient(mGoogleApiClient);

                    }

                    @Override
                    public void onConnectionSuspended(int i) {

                    }
                })
                .build();


        mPlaceArrayAdapter = new PlaceArrayAdapter(this, android.R.layout.simple_list_item_1,
                BOUNDS_MOUNTAIN_VIEW, null);
        tv_google_loc.setAdapter(mPlaceArrayAdapter);

    }

    @Override
    public void onPause() {
        super.onPause();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.stopAutoManage(this);
            mGoogleApiClient.disconnect();
        }

    }

    @BindView(R.id.ivCloc)
    ImageView ivCloc;


    @OnClick(R.id.ivCloc)
    public void ivCloc(View v) {
        rlPb.setVisibility(View.VISIBLE);
        getLocation();
    }

    LocationManager locationManager;

    public void endGPS() {

        try {
            locationManager.removeUpdates(locationListener);
            locationManager = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    LocationListener locationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {

            rlPb.setVisibility(View.GONE);

            try {
                Geocoder geocoder = new Geocoder(EditCurrencyForBusinessActivity.this, Locale.getDefault());
                List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
                lati = location.getLatitude() + "";
                longi = location.getLongitude() + "";
                tvLatLongInfo.setText("Latitude : " + lati + " , " + "Longitude : " + longi);
                tv_google_loc.setText("" + addresses.get(0).getAddressLine(0));
                endGPS();

            } catch (Exception e) {
                e.printStackTrace();
            }


        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {

        }

        @Override
        public void onProviderEnabled(String s) {

        }

        @Override
        public void onProviderDisabled(String s) {

        }
    };

    void getLocation() { // start gps
        try {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    public void getLocationFromAddress(final String strAddress) {

        Geocoder coder = new Geocoder(this);
        List<Address> address;


        try {
            address = coder.getFromLocationName(strAddress, 1);
            if (address == null) {
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // Do something after 5s = 5000ms
                        getLocationFromAddress(strAddress);
                    }
                }, 500);
            } else {
                rlPb.setVisibility(View.GONE);
                Address location = address.get(0);
                lati = "" + location.getLatitude();
                longi = "" + location.getLongitude();
                tvLatLongInfo.setText("Latitude : " + lati + " , " + "Longitude : " + longi);
                sv.fullScroll(View.FOCUS_DOWN);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }


    }


    // multi image uploading

    @BindView(R.id.rv_selectfile)
    RecyclerView rv_selectfile;

    @BindView(R.id.pager)
    ViewPager pager;

    @BindView(R.id.indicator)
    CircleIndicator indicator;


    ShopImgFileAdapter shopImgFileAdapter;
    private ProgressDialog dialog;
    ArrayList<ModelFile> al_selet = new ArrayList<>();
    ArrayList<String> al_pager = new ArrayList<>();
    ArrayList<String> al_uploadedimg = new ArrayList<>();
    ImagePagerAdapter imagePagerAdapter;

    public void initMultiImg() {
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rv_selectfile.setLayoutManager(mLayoutManager);


        shopImgFileAdapter = new ShopImgFileAdapter(al_selet, this, new ShopImgFileAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(String item) {
                setPager();
            }

            @Override
            public void onDelete(int pos) {
                al_selet.remove(pos);
                shopImgFileAdapter.assignItem(al_selet);
                setPager();
                //if(al_selet.size()==0)iv_upload.setVisibility(View.GONE);
                //else iv_upload.setVisibility(View.VISIBLE);


            }

            @Override
            public void onClickString(String pos) {


            }

            @Override
            public void onImgClick(List<ModelFile> data, int pos) {


            }

            @Override
            public void onAddEditInfo(ModelFile item, int pos) {
                imgInfoPos = pos;
                if (item.getImgInfo() == null) etShopImgInfo.setText("");
                etShopImgInfo.setText(item.getImgInfo());
                showEnterImgInfo();
            }
        }, false, false, true);
        rv_selectfile.setAdapter(shopImgFileAdapter);

        ItemTouchHelper.Callback callback = new ShopDragTouchHelperIssue(shopImgFileAdapter);
        ItemTouchHelper helper = new ItemTouchHelper(callback);
        helper.attachToRecyclerView(rv_selectfile);

        setPager();

    }

    @BindView(R.id.ivShopGallery)
    ImageView ivShopGallery;

    @OnClick(R.id.ivShopGallery)
    public void ivShopGallery(View v) {


        String imgarray = new Gson().toJson(al_selet);

        Intent intent = new Intent(this, ImagePreviewActivity.class);
        intent.putExtra("imgarray", imgarray);
        intent.putExtra("pos", pager.getCurrentItem());
        intent.putExtra("img_title", userprofile.getFirstname());
        startActivity(intent);

    }


    public void setPager() {
        al_pager = new ArrayList<>();
        for (int i = 0; i < al_selet.size(); i++) {
            al_pager.add(al_selet.get(i).getImgpath());
        }
        imagePagerAdapter = new ImagePagerAdapter(this, al_pager, "");
        pager.setAdapter(imagePagerAdapter);
        indicator.setViewPager(pager);

        ivShopGallery.setVisibility(View.GONE);
        if (al_pager.size() > 0) {
            ivShopGallery.setVisibility(View.VISIBLE);
        }
    }

    // Disocont , Range for Product Total
    @BindView(R.id.rgDisc)
    RadioGroup rgDisc;
    @BindView(R.id.rbDiscount)
    RadioButton rbDiscount;
    @BindView(R.id.rbRange)
    RadioButton rbRange;
    @BindView(R.id.llTotalDiscBlock)
    LinearLayout llTotalDiscBlock;
    @BindView(R.id.llRangeBlock)
    LinearLayout llRangeBlock;
    @BindView(R.id.etDiscPercTotal)
    EditText etDiscPercTotal;
    @BindView(R.id.etMaxDiscVal)
    EditText etMaxDiscVal;
    @BindView(R.id.etMinPurchaseVal)
    EditText etMinPurchaseVal;
    @BindView(R.id.tvMinLbl)
    TextView tvMinLbl;
    @BindView(R.id.tvDiscLbl)
    TextView tvDiscLbl;
    @BindView(R.id.tvMaxLbl)
    TextView tvMaxLbl;

    @BindView(R.id.ivAddRange)
    ImageView ivAddRange;

    @BindView(R.id.ivSaveRange)
    ImageView ivSaveRange;

    @BindView(R.id.etMinLbl)
    EditText etMinLbl;
    @BindView(R.id.etMaxLbl)
    EditText etMaxLbl;
    @BindView(R.id.etDiscLbl)
    EditText etDiscLbl;
    @BindView(R.id.rvRange)
    RecyclerView rvRange;

    @OnClick(R.id.ivAddRange)
    public void ivAddRange(View view) {
        alRange.add(new ProductTotalDiscount(etMinLbl.getText().toString().trim(), etMaxLbl.getText().toString().trim(), etDiscLbl.getText().toString().trim()));
        rvRange.scrollToPosition(alRange.size() - 1);

    }

    @BindView(R.id.ivClrRange)
    ImageView ivClrRange;

    @OnClick(R.id.ivClrRange)
    public void ivClrRange(View view) {
        clearRange();

    }

    @OnClick(R.id.ivSaveRange)
    public void ivSaveRange(View view) {

        String minRange = etMinLbl.getText().toString().trim();
        String maxRange = etMaxLbl.getText().toString().trim();
        String discRange = etDiscLbl.getText().toString().trim();

        if (minRange.isEmpty()) {
            Inad.alerter("Alert", "Enter minimum value", EditCurrencyForBusinessActivity.this);
        } else if (maxRange.isEmpty()) {
            Inad.alerter("Alert", "Enter maximum value", EditCurrencyForBusinessActivity.this);

        } else if (discRange.isEmpty()) {
            Inad.alerter("Alert", "Enter discount value", EditCurrencyForBusinessActivity.this);

        } else {
            int minR = Integer.parseInt(minRange);
            int maxR = Integer.parseInt(maxRange);
            int discR = Integer.parseInt(discRange);

            if (maxR <= minR) {
                Inad.alerter("Alert", "Enter valid values", EditCurrencyForBusinessActivity.this);
            } else if (discR == 0) {
                Inad.alerter("Alert", "Enter valid values", EditCurrencyForBusinessActivity.this);

            } else {
                if (isEdit) {
                    alRange.get(editPos).min_range = minRange;
                    alRange.get(editPos).max_range = maxRange;
                    alRange.get(editPos).disc_range = discRange;
                    rangeInDiscountListAdapter.setItem(alRange);
                } else {
                    alRange.add(new ProductTotalDiscount(minRange, maxRange, discRange));
                    rangeInDiscountListAdapter.setItem(alRange);
                }
                ///ivClrRange.performClick();
                clearRange();
            }
        }


    }

    RangeInDiscountListAdapter rangeInDiscountListAdapter;
    boolean isEdit = false;
    int editPos = 0;

    public void insertDiscount() {

        rgDisc.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {

                switch (i) {
                    case R.id.rbDiscount:
                        productTotalDiscount.product_total_discount_type_of = "discount";
                        llTotalDiscBlock.setVisibility(View.VISIBLE);
                        llRangeBlock.setVisibility(View.GONE);

                        etDiscPercTotal.setText(productTotalDiscount.disc_perc + "");
                        etMinPurchaseVal.setText(productTotalDiscount.min_purchase_val + "");
                        etMaxDiscVal.setText(productTotalDiscount.max_disc_val + "");

                        break;
                    case R.id.rbRange:
                        productTotalDiscount.product_total_discount_type_of = "range";
                        llTotalDiscBlock.setVisibility(View.GONE);
                        llRangeBlock.setVisibility(View.VISIBLE);
                        break;
                }
            }
        });

        rangeInDiscountListAdapter = new RangeInDiscountListAdapter(alRange, this, symbol_native, new RangeInDiscountListAdapter.OnItemClickListener() {
            @Override
            public void onDelete(int item) {
                if (isEdit) {
                    Inad.alerter("Alert", "Please save/clear changes", EditCurrencyForBusinessActivity.this);
                } else {
                    alRange.remove(item);
                    rangeInDiscountListAdapter.setItem(alRange);
                    clearRange();
                }


            }

            @Override
            public void onEdit(int item) {
                isEdit = true;
                editPos = item;
                ProductTotalDiscount productTotalDiscount = alRange.get(item);

                if (alRange.size() == 1) {
                    setMinLbl(true);
                    setMaxLbl(true);
                } else if (item == 0) {
                    setMinLbl(true);
                    setMaxLbl(false);
                } else {
                    setMinLbl(false);
                    setMaxLbl(true);
                }

                etMinLbl.setText(productTotalDiscount.min_range);
                etMaxLbl.setText(productTotalDiscount.max_range);
                etDiscLbl.setText(productTotalDiscount.disc_range);

            }
        });

        rvRange.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        rvRange.setAdapter(rangeInDiscountListAdapter);
        rvRange.setNestedScrollingEnabled(false);

    }

    ProductTotalDiscount productTotalDiscount = new ProductTotalDiscount();
    ArrayList<ProductTotalDiscount> alRange = new ArrayList<>();


    // Shop Web link
    @BindView(R.id.etWebLinkOfShop)
    EditText etWebLinkOfShop;

    @OnClick(R.id.ivShopWeb)
    public void ivShopWeb(View view) {
        String url = etWebLinkOfShop.getText().toString().trim().toLowerCase();
        if (url.isEmpty()) {
            Inad.alerter("Alert", "Please enter link", this);
            etWebLinkOfShop.requestFocus();
        } else {
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            startActivity(i);
        }

    }


    public void setAftervalidate() {


        if (tvAddEditTitle.getText().toString().contains("Share")) {
            etShareId.setText(validateBusinessId);

            try {
                JSONObject joField = new JSONObject(field_json_data);
                joField.put("shareId", validateBusinessId);

                updateOnlyFieldJson(joField);
            } catch (JSONException e) {
                e.printStackTrace();
            }


        } else if (tvAddEditTitle.getText().toString().contains("Delivery")) {
            etDelPartner.setText(validateBusinessId);
            tvDelMobile.setText(et2.getText().toString().trim());
            tvDelCustNm.setText(et3.getText().toString().trim());

            try {
                JSONObject joField = new JSONObject(field_json_data);

                joField.put("delivery_partner_info", etDelPartner.getText().toString().trim());
                joField.put("mobile_delivery", tvDelMobile.getText().toString());
                joField.put("customer_nm_delivery", tvDelCustNm.getText().toString());


                updateOnlyFieldJson(joField);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } else {
            tvAdd.setText("Ok");
            tvAdd.setBackground(ContextCompat.getDrawable(this, R.drawable.roundcorner_color));

            hideAddCat();

        }

    }

    public void updateOnlyFieldJson(JSONObject joFielddata) {

        loadImg.loadFitImg(EditCurrencyForBusinessActivity.this, tvAdd);


        Map<String, RequestBody> stringRequestBodyHashMap = new HashMap<>();

        RequestBody sectionBody = RequestBody.create(MediaType.parse("multipart/form-data"), "bussinessinfo");
        stringRequestBodyHashMap.put("section", sectionBody);

      /*  try {
            //JSONObject joField = new JSONObject(field_json_data);
            //joField.put("shareId", str_bussinsee_user_id);

            //field_json_data = joField.toString();

        } catch (JSONException e) {
            e.printStackTrace();
        }*/

        RequestBody field_json_data = RequestBody.create(MediaType.parse("multipart/form-data"), joFielddata.toString());
        stringRequestBodyHashMap.put("field_json_data", field_json_data);


        RequestBody business_typeBody = RequestBody.create(MediaType.parse("multipart/form-data"), business_type);
        stringRequestBodyHashMap.put("business_type", business_typeBody);

        MyRequestCall myRequestCall = new MyRequestCall();
        myRequestCall.updateOnlyFieldJsonOfBusinessInfo(this, sessionManager.getsession(), stringRequestBodyHashMap, new MyRequestCall.CallRequest() {
            @Override
            public void onGetResponse(String response) {

                tvAdd.setText("Ok");
                tvAdd.setBackground(ContextCompat.getDrawable(EditCurrencyForBusinessActivity.this, R.drawable.roundcorner_color));
                if (!response.isEmpty()) {
                    hideAddCat();
                }

            }
        });
    }

    // Show enter image info

    @BindView(R.id.llAnimAddcat3)
    LinearLayout llAnimAddcat3;

    @BindView(R.id.rlAddCategory3)
    RelativeLayout rlAddCategory3;

    @BindView(R.id.etShopImgInfo)
    EditText etShopImgInfo;

    @OnClick(R.id.rlAddCategory3)
    public void rlAddCategory3(View v) {
        onBackPressed();
    }

    @OnClick(R.id.tvSaveImgInfo)
    public void tvSaveImgInfo(View v) {


        if (etShopImgInfo.getText().toString().trim().isEmpty()) {
            Inad.alerter("Alert", "Image information is required.", EditCurrencyForBusinessActivity.this);
        } else {
            al_selet.get(imgInfoPos).setImgInfo(etShopImgInfo.getText().toString().trim() + "");
            shopImgFileAdapter.assignItem(al_selet);
            onBackPressed();
        }
    }

    public void clearLbl() {
        etMinLbl.setText("");
        etMaxLbl.setText("");
        etDiscLbl.setText("");
    }

    public void clearRange() {
        isEdit = false;
        setMinLbl(true);
        setMaxLbl(true);
        if (alRange.size() > 0) {
            setMinLbl(false);

            String maxRange = alRange.get(alRange.size() - 1).max_range;
            if (!maxRange.isEmpty()) {
                int minRAdd = Integer.parseInt(maxRange);
                minRAdd++;
                etMinLbl.setText(minRAdd + "");
            }


        }
    }

    public void setMinLbl(boolean isEdit) {
        clearLbl();
        etMinLbl.setEnabled(isEdit ? true : false);
        etMinLbl.setTextColor(isEdit ? ContextCompat.getColor(EditCurrencyForBusinessActivity.this, R.color.black) : ContextCompat.getColor(EditCurrencyForBusinessActivity.this, R.color.infoclr));
    }

    public void setMaxLbl(boolean isEdit) {
        clearLbl();
        etMaxLbl.setEnabled(isEdit ? true : false);
        etMaxLbl.setTextColor(isEdit ? ContextCompat.getColor(EditCurrencyForBusinessActivity.this, R.color.black) : ContextCompat.getColor(EditCurrencyForBusinessActivity.this, R.color.infoclr));
    }

    @OnClick(R.id.tvCancelImgInfo)
    public void tvCancelImgInfo(View v) {
        onBackPressed();
    }

    int imgInfoPos = 0;

    public void showEnterImgInfo() {
        etShopImgInfo.setSelection(etShopImgInfo.getText().toString().trim().length());
        playAnim.slideDownRl(llAnimAddcat3);
        rlAddCategory3.setVisibility(View.VISIBLE);
    }

    public void hideEnterImgInfo() {

        playAnim.slideUpRl(llAnimAddcat3);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Utils.hideSoftKeyboard(EditCurrencyForBusinessActivity.this, etShopImgInfo);
                rlAddCategory3.setVisibility(View.GONE);
            }
        }, 700);


    }

    // Working Hrs

    @BindView(R.id.etOpenTime)
    EditText etOpenTime;

    @BindView(R.id.etClosedTime)
    EditText etClosedTime;

    @BindView(R.id.etWorkHrMsg)
    EditText etWorkHrMsg;

    public void setWorkingHrInfo(String workingHrInfo) {
        String open_time = JsonObjParse.getValueEmpty(field_json_data, "open_time");
        etOpenTime.setText(open_time);
        String close_time = JsonObjParse.getValueEmpty(field_json_data, "close_time");
        etClosedTime.setText(close_time);
        String work_hr_msg = JsonObjParse.getValueEmpty(field_json_data, "work_hr_msg");
        etWorkHrMsg.setText(work_hr_msg);

    }

    @BindView(R.id.etSilverTodayPrice)
    EditText etSilverTodayPrice;

    @BindView(R.id.etGoldTodayPrice)
    EditText etGoldTodayPrice;

    @BindView(R.id.rvMetals)
    RecyclerView rvMetals;

    @BindView(R.id.tvAddEditMetal)
    TextView tvAddEditMetal;

    @BindView(R.id.etMetalNm)
    EditText etMetalNm;

    @BindView(R.id.etMetalPrice)
    EditText etMetalPrice;

    @BindView(R.id.etMetalUnit)
    EditText etMetalUnit;

    @OnClick(R.id.tvAddEditMetal)
    public void tvAddEditMetal(View v) {

        if (etMetalNm.getText().toString().trim().isEmpty()) {
            Inad.alerter("Commodity", "Material name must be required.", EditCurrencyForBusinessActivity.this);
        } else if (etMetalPrice.getText().toString().trim().isEmpty()) {
            Inad.alerter("Commodity", "Material price must be required.", EditCurrencyForBusinessActivity.this);
        } else if (etMetalUnit.getText().toString().trim().isEmpty()) {
            Inad.alerter("Commodity", "Material unit must be required.", EditCurrencyForBusinessActivity.this);
        } else if (tvAddEditMetal.getText().toString().equalsIgnoreCase("edit")) {
            tvAddEditMetal.setText("ADD");
            alMetal.set(metalPos, new Metal(etMetalNm.getText().toString().trim(), etMetalPrice.getText().toString().trim(), false, etMetalUnit.getText().toString().trim(), metalPos));
            metalListAdapter.setItem(alMetal);
            clrMetalEt();
        } else {
            alMetal.add(new Metal(etMetalNm.getText().toString().trim(), etMetalPrice.getText().toString().trim(), false, etMetalUnit.getText().toString().trim(), alMetal.size()));
            metalListAdapter.setItem(alMetal);
            clrMetalEt();
        }
    }

    @BindView(R.id.spn_whom)
    Spinner act_cps;

    public void clrMetalEt() {
        etMetalUnit.setText("");
        etMetalNm.setText("");
        etMetalPrice.setText("");
    }

    MetalListAdapter metalListAdapter;
    ArrayList<Metal> alMetal = new ArrayList<>();
    String metal = "";

    int metalPos = 0;

    public void setJewelInfo(String jewelInfo) {

        String silver_today_price = JsonObjParse.getValueEmpty(field_json_data, "silver_today_price");
        etSilverTodayPrice.setText(silver_today_price);

        String gold_today_price = JsonObjParse.getValueEmpty(field_json_data, "gold_today_price");
        etGoldTodayPrice.setText(gold_today_price);

        alMetal = new ArrayList<>();
        metalListAdapter = new MetalListAdapter(alMetal, this, new MetalListAdapter.OnItemClickListener() {
            @Override
            public void onDelete(int item) {

            }

            @Override
            public void onEdit(int item) {


                for (int i = 0; i < alMetal.size(); i++) {
                    alMetal.get(i).isSelect = false;
                }

                alMetal.get(item).isSelect = true;

                etMetalUnit.setText(alMetal.get(item).unit + "");
                etMetalNm.setText(alMetal.get(item).metalNm + "");
                etMetalPrice.setText(alMetal.get(item).metalPrice + "");
                tvAddEditMetal.setText("EDIT");


                metalPos = item;

                //metal = alMetal.get(item).metalNm;

                //et_mrp.setText(alMetal.get(item).metalPrice);

                metalListAdapter.setItem(alMetal);
            }
        }, symbol_native);

        rvMetals.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        //rvMetals.setLayoutManager(new GridLayoutManager(this, 2));
        rvMetals.setAdapter(metalListAdapter);
        rvMetals.setNestedScrollingEnabled(false);

        String metal_list = JsonObjParse.getValueEmpty(field_json_data, "metal_list");

        Metal metal = new Gson().fromJson(metal_list, Metal.class);

        if (metal == null) metal = new Metal();
        serviceId = metal.serviceId;
        alMetal = metal.alMetal;
        if (alMetal == null) alMetal = new ArrayList<>();

        metalListAdapter.setItem(alMetal);

        setCommodityService();


    }

    AutoCompleteTv autoCompleteTv = new AutoCompleteTv();
    ArrayList<AutoCompleteTv> al_fill = new ArrayList<>();

    public void setCommodityService() {
        if (al_fill.size() > 0) return;
        al_fill = new ArrayList<>();
        al_fill.add(new AutoCompleteTv("Shopping cart", 0));
        al_fill.add(new AutoCompleteTv("Shoppping cart - Allow 1", 1)); // Radio
        al_fill.add(new AutoCompleteTv("Shoppping cart - Allow many", 2)); // Checkbox
        al_fill.add(new AutoCompleteTv("Only catalogue", 3));
        autoCompleteTv.setautofill(EditCurrencyForBusinessActivity.this, act_cps, al_fill);

        act_cps.setSelection(serviceId);


    }

    int serviceId = 0;

    @BindView(R.id.spn_traction)
    Spinner spn_traction;

    ArrayList<AutoCompleteTv> alTractions = new ArrayList<>();
    int tractionId = 0;

    public void setTractionService() {
        String tractionIdStr = JsonObjParse.getValueEmpty(field_json_data, "tractionId");
        if (!tractionIdStr.isEmpty()) tractionId = Integer.parseInt(tractionIdStr);
        alTractions = new ArrayList<>();
        alTractions.add(new AutoCompleteTv("Daily", 0));
        alTractions.add(new AutoCompleteTv("Immediate", 1)); // Radio
        autoCompleteTv.setautofill(EditCurrencyForBusinessActivity.this, spn_traction, alTractions);

        spn_traction.setSelection(tractionId);


    }

    // Number Picker

    @BindView(R.id.npDays)
    NumberPicker npDays;

    @BindView(R.id.cbWalkInInMenu)
    CheckBox cbWalkInInMenu;

    @BindView(R.id.cbWalkInCountCondition)
    CheckBox cbWalkInCountCondition;

    @BindView(R.id.cbWalkInDefultPromoValue)
    CheckBox cbWalkInDefultPromoValue;

    @BindView(R.id.cbWalkInDefultRefValue)
    CheckBox cbWalkInDefultRefValue;

    @BindView(R.id.etWalkInDefaultValue)
    EditText etWalkInDefaultValue;

    @BindView(R.id.etWalkInDefaultRefValue)
    EditText etWalkInDefaultRefValue;

    int length = 366;

    String days[] = new String[length];

    public void initNoPicker() {

        for (int i = 0; i < length; i++) {
            int t = i;
            days[i] = "" + t;
        }

        npDays.setMinValue(1); //from array first value
        //Specify the maximum value/number of NumberPicker
        npDays.setMaxValue(length); //to array last value
        npDays.setWrapSelectorWheel(true);
        npDays.setDisplayedValues(days);

        setNumberPickerTextColor(npDays, R.color.black);

    }

    public void setWalkInValues() {

        String walk_in_details = JsonObjParse.getValueEmpty(field_json_data, "walk_in_details");
        String is_walkin_in_menu = JsonObjParse.getValueEmpty(walk_in_details, "is_walkin_in_menu");
        if (is_walkin_in_menu.equalsIgnoreCase("no")) cbWalkInInMenu.setChecked(false);
        else cbWalkInInMenu.setChecked(true);

        String is_walkin_count_condition = JsonObjParse.getValueEmpty(walk_in_details, "is_walkin_count_condition");
        if (is_walkin_count_condition.equalsIgnoreCase("yes"))
            cbWalkInCountCondition.setChecked(true);
        else cbWalkInCountCondition.setChecked(false);

        String is_walkin_default_value = JsonObjParse.getValueEmpty(walk_in_details, "is_walkin_default_value");
        if (is_walkin_default_value.equalsIgnoreCase("yes"))
            cbWalkInDefultPromoValue.setChecked(true);
        else cbWalkInDefultPromoValue.setChecked(false);

        String is_walkin_default_ref_value = JsonObjParse.getValueEmpty(walk_in_details, "is_walkin_default_ref_value");
        if (is_walkin_default_ref_value.equalsIgnoreCase("yes"))
            cbWalkInDefultRefValue.setChecked(true);
        else cbWalkInDefultRefValue.setChecked(false);

        String default_promo_value = JsonObjParse.getValueEmpty(walk_in_details, "default_promo_value");
        etWalkInDefaultValue.setText(default_promo_value + "");

        String default_promo_ref_value = JsonObjParse.getValueEmpty(walk_in_details, "default_promo_ref_value");
        etWalkInDefaultRefValue.setText(default_promo_ref_value + "");

        String timestamp_day = JsonObjParse.getValueEmpty(walk_in_details, "timestamp_day");
        if (!timestamp_day.isEmpty()) {
            int d = Integer.parseInt(timestamp_day);
            npDays.setValue(d);
        }


    }

    JsonObject getWalkInJson() {
        JsonObject joWalkIn = new JsonObject();

        if (cbWalkInInMenu.isChecked()) joWalkIn.addProperty("is_walkin_in_menu", "Yes");
        else joWalkIn.addProperty("is_walkin_in_menu", "No");

        if (cbWalkInCountCondition.isChecked())
            joWalkIn.addProperty("is_walkin_count_condition", "Yes");
        else joWalkIn.addProperty("is_walkin_count_condition", "No");

        joWalkIn.addProperty("timestamp_ms", convertDaysToMiliSec(npDays.getValue()));
        joWalkIn.addProperty("timestamp_day", npDays.getValue() + "");
        joWalkIn.addProperty("default_promo_value", etWalkInDefaultValue.getText().toString().trim());
        joWalkIn.addProperty("default_promo_ref_value", etWalkInDefaultRefValue.getText().toString().trim());

        if (cbWalkInDefultPromoValue.isChecked())
            joWalkIn.addProperty("is_walkin_default_value", "Yes");
        else joWalkIn.addProperty("is_walkin_default_value", "No");

        if (cbWalkInDefultRefValue.isChecked())
            joWalkIn.addProperty("is_walkin_default_ref_value", "Yes");
        else joWalkIn.addProperty("is_walkin_default_ref_value", "No");

        return joWalkIn;
    }

    public void setNumberPickerTextColor(NumberPicker numberPicker, int color) {

        try {
            Field selectorWheelPaintField = numberPicker.getClass()
                    .getDeclaredField("mSelectorWheelPaint");
            selectorWheelPaintField.setAccessible(true);
            ((Paint) selectorWheelPaintField.get(numberPicker)).setColor(color);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();

        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }

        final int count = numberPicker.getChildCount();
        for (int i = 0; i < count; i++) {
            View child = numberPicker.getChildAt(i);
            if (child instanceof EditText)
                ((EditText) child).setTextColor(color);
        }
        numberPicker.invalidate();
    }


    public long convertDaysToMiliSec(int days) {
        Log.e("okhttp days", days + "");
        return 60 * 60 * 24 * 1000 * days;
    }

    @BindView(R.id.rgShareLink)
    RadioGroup rgShareLink;

    @BindView(R.id.rbDefault)
    RadioButton rbDefault;

    @BindView(R.id.rbCustom)
    RadioButton rbCustom;

    @BindView(R.id.rbPlaystore)
    RadioButton rbPlaystore;

    @BindView(R.id.rbNoneLink)
    RadioButton rbNoneLink;

    @BindView(R.id.llCustomLink)
    LinearLayout llCustomLinkl;

    @BindView(R.id.etCustomLink)
    EditText etCustomLink;

    @BindView(R.id.tvPlaystoreLink)
    TextView tvPlaystoreLink;

    public void setShareLink() {
        rgShareLink.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.rbDefault) {
                    llCustomLinkl.setVisibility(View.GONE);
                    tvPlaystoreLink.setVisibility(View.GONE);
                } else if (checkedId == R.id.rbCustom) {
                    llCustomLinkl.setVisibility(View.VISIBLE);
                    tvPlaystoreLink.setVisibility(View.GONE);
                } else if (checkedId == R.id.rbPlaystore) {
                    tvPlaystoreLink.setText("http://play.google.com/store/apps/details?id=" + getString(R.string.consumer_link));
                    llCustomLinkl.setVisibility(View.GONE);
                    tvPlaystoreLink.setVisibility(View.VISIBLE);
                } else {
                    llCustomLinkl.setVisibility(View.GONE);
                    tvPlaystoreLink.setVisibility(View.GONE);
                }
            }
        });
    }

    @Override
    public void onItemClick(int pos) {
        tv_google_loc.setText(addressList.get(pos).getAddress());
        llAddressList.setVisibility(View.GONE);
    }
}