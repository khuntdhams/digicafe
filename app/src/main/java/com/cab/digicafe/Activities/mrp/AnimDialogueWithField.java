package com.cab.digicafe.Activities.mrp;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.cab.digicafe.Activities.RequestApis.MyRequestCall;
import com.cab.digicafe.Activities.RequestApis.MyRequst;
import com.cab.digicafe.Adapter.mrp.PaymentListAdapter;
import com.cab.digicafe.Adapter.mrp.PaymentViewModel;
import com.cab.digicafe.Adapter.mrp.TaskPrefViewModel;
import com.cab.digicafe.Helper.Inad;
import com.cab.digicafe.Helper.SessionManager;
import com.cab.digicafe.Model.ShareModel;
import com.cab.digicafe.MyCustomClass.DatePickerNew;
import com.cab.digicafe.MyCustomClass.JsonObjParse;
import com.cab.digicafe.MyCustomClass.PlayAnim;
import com.cab.digicafe.R;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;


public class AnimDialogueWithField extends AlertDialog implements
        View.OnClickListener {
    private final OnDialogClickListener listener;
    public AppCompatActivity c;
    public Dialog d;


    @BindView(R.id.llAnim)
    LinearLayout llAnim;

    @BindView(R.id.llPaymentComment)
    LinearLayout llPaymentComment;

    @BindView(R.id.llModifyCostView)
    LinearLayout llModifyCostView;

    @BindView(R.id.tvAdd)
    TextView tvAdd;

    @BindView(R.id.tvCancel)
    TextView tvCancel;

    @BindView(R.id.tvStatus)
    TextView tvStatus;

    @BindView(R.id.etActualPrice)
    TextInputEditText etActualPrice;

    @BindView(R.id.etResUnitOrQuantity)
    TextInputEditText etResUnitOrQuantity;
    @BindView(R.id.etResAmount)
    TextInputEditText etResAmount;

    @BindView(R.id.tvModifyDelDt)
    TextView tvModifyDelDt;

    @BindView(R.id.rlDelDt)
    RelativeLayout rlDelDt;

    @BindView(R.id.tvModifyDelTime)
    TextView tvModifyDelTime;

    @BindView(R.id.rlDelTime)
    RelativeLayout rlDelTime;

    @BindView(R.id.rlHide)
    RelativeLayout rlHide;

    @BindView(R.id.llTaskStatus)
    LinearLayout llTaskStatus;

    @BindView(R.id.llMaterial)
    LinearLayout llMaterial;

    @BindView(R.id.llResource)
    LinearLayout llResource;


    String title = "", status = "";
    TaskPrefViewModel taskPrefViewModel;

    public AnimDialogueWithField(Context a, OnDialogClickListener listener, String title, String view) {
        super(a);
        // TODO Auto-generated constructor stub
        this.c = (AppCompatActivity) a;
        this.listener = listener;
        this.title = title;
        this.status = view;
    }

    String symbol_native = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_anim_dialogue_field);
        ButterKnife.bind(this);

        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        getWindow().setAttributes(lp);

        tvAdd.setOnClickListener(this);
        tvCancel.setOnClickListener(this);
        rlHide.setOnClickListener(this);
        setCancelable(true);
        tvStatus.setText(title);
        /*setOnKeyListener(new OnKeyListener() {

            @Override
            public boolean onKey(DialogInterface arg0, int keyCode,
                                 KeyEvent event) {
                // TODO Auto-generated method stub
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    dismissDialogue(0);
                }
                return true;
            }
        });*/
        rlHide.setOnClickListener(this);
        rlDelDt.setOnClickListener(this);
        rlDelTime.setOnClickListener(this);
        rlPayDate.setOnClickListener(this);
        playAnim.slideDownRl(llAnim);

        taskPrefViewModel = ShareModel.getI().taskPrefViewModel;


        SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy hh:mm aaa");
        try {

            String MRP_TAB = taskPrefViewModel.getToFieldJsonData();
            if (!MRP_TAB.equalsIgnoreCase("") && MRP_TAB.length() > 2) { // {}


                //JSONArray jsonArray = new JSONArray(MRP_TAB);
                //JSONObject jsonObject = jsonArray.getJSONObject(0);

                String forWhich = "";

                if (ShareModel.getI().isFromAllTask) {
                    //forWhich = JsonObjParse.getValueEmpty(jsonObject.toString(), "To");
                    forWhich = taskPrefViewModel.getToFieldJsonData();
                } else {
                    //forWhich = JsonObjParse.getValueEmpty(jsonObject.toString(), "From");
                    forWhich = taskPrefViewModel.getFromFieldJsonData();
                }

                link_status = taskPrefViewModel.getStatus();


                String payment_reference = JsonObjParse.getValueEmpty(forWhich, "payment_reference");
                alPayment = new Gson().fromJson(payment_reference, new TypeToken<ArrayList<PaymentViewModel>>() {
                }.getType());
                if (alPayment == null) alPayment = new ArrayList<>();

                symbol_native = JsonObjParse.getValueEmpty(forWhich, "symbol_native") + " ";

                String dateStr = JsonObjParse.getValueEmpty(forWhich, "EDIT_DATE");

                try {
                    if (!dateStr.equals("")) {
                        Date date = format.parse(dateStr);
                        cal_order.setTime(date);
                        if (status.equalsIgnoreCase(c.getString(R.string.pay_details))) {

                        } else {
                            settime();
                        }
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                etActualPrice.setText("");
                String total_chit_item_value = JsonObjParse.getValueEmpty(forWhich, "edit_total_chit_item_value");
                if (!total_chit_item_value.isEmpty()) {
                    Double amt = Double.valueOf(total_chit_item_value);
                    etActualPrice.setText(Inad.getCurrencyDecimal(amt, c));
                }

                if (taskPrefViewModel.getTaskPurpose().equalsIgnoreCase("Resource")) {
                    String unit_value = JsonObjParse.getValueEmpty(forWhich, "unit_value");
                    etResUnitOrQuantity.setText(unit_value);
                    String amount_value = JsonObjParse.getValueEmpty(forWhich, "amount_value");
                    etResAmount.setText(amount_value);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


        if (status.equalsIgnoreCase(c.getString(R.string.del_details))) {


            if (taskPrefViewModel.getTaskPurpose().equalsIgnoreCase("Resource")) {
                llResource.setVisibility(View.VISIBLE);
                llMaterial.setVisibility(View.GONE);
            } else if (taskPrefViewModel.getTaskPurpose().equalsIgnoreCase("Material")) {
                llResource.setVisibility(View.GONE);
                llMaterial.setVisibility(View.VISIBLE);
            }

            showHideLl(llPaymentComment, false);
            showHideLl(llModifyCostView, true);
            showHideLl(llTaskStatus, false);
            tvAdd.setText("Update");
        } else if (status.equalsIgnoreCase(c.getString(R.string.pay_details))) {
            showHideLl(llPaymentComment, true);
            showHideLl(llModifyCostView, false);
            showHideLl(llTaskStatus, false);
            tvAdd.setText("Add");
            setPayList();
        } else if (status.equalsIgnoreCase(c.getString(R.string.status_selection))) {
            showHideLl(llPaymentComment, false);
            showHideLl(llModifyCostView, false);
            showHideLl(llTaskStatus, true);
            tvAdd.setText("Update");
            setRadioForChangeStatus();
        }


    }

    public void showHideLl(LinearLayout linearLayout, boolean isShow) {
        if (isShow) {
            linearLayout.setVisibility(View.VISIBLE);
        } else {
            linearLayout.setVisibility(View.GONE);
        }
    }


    public void dismissDialogue(final int call) {
        playAnim.slideUpRl(llAnim);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                dismiss();

            }
        }, 700);

    }

    PlayAnim playAnim = new PlayAnim();

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvCancel:
                if (status.equalsIgnoreCase(c.getString(R.string.pay_details))) {
                    if (!tvAdd.getText().toString().equalsIgnoreCase("add")) {

                        etPayRef.setText("");
                        etPaymentAmount.setText("");
                        tvPaymentDt.setText("");

                        tvAdd.setText("Add");
                    } else {
                        dismissDialogue(0);
                    }
                } else {
                    dismissDialogue(0);
                }
                break;
            case R.id.rlDelDt:
                openDateView(tvModifyDelDt);
                break;
            case R.id.rlPayDate:
                openDateView(tvPaymentDt);
                break;
            case R.id.rlDelTime:
                TimePicker mTimePicker = new TimePicker();

                mTimePicker.show(c.getFragmentManager(), "Select delivery time");
                break;
            case R.id.tvAdd:
                //dismissDialogue(1);

                if (status.equalsIgnoreCase(c.getString(R.string.del_details))) {
                    try {
                        if (taskPrefViewModel.getTaskPurpose().equalsIgnoreCase("Resource")) {

                            String strResUnitOrQuantity = etResUnitOrQuantity.getText().toString().trim();
                            String strResAmount = etResAmount.getText().toString().trim();
                            if (strResUnitOrQuantity.isEmpty()) {
                                Toast.makeText(c, "Rate is required.", Toast.LENGTH_SHORT).show();
                            } else if (strResAmount.isEmpty()) {
                                Toast.makeText(c, "Quantity field is required.", Toast.LENGTH_SHORT).show();
                            } else {
                                modifyTaskPreferenceForMrpFromAllTask();
                            }

                        } else if (taskPrefViewModel.getTaskPurpose().equalsIgnoreCase("Material")) {

                            String strActualPrice = etActualPrice.getText().toString().trim();
                            if (strActualPrice.isEmpty()) {
                                Toast.makeText(c, "Actual price field is required.", Toast.LENGTH_SHORT).show();
                            } else {
                                modifyTaskPreferenceForMrpFromAllTask();
                            }
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else if (status.equalsIgnoreCase(c.getString(R.string.pay_details))) {


                    String payRefNm = etPayRef.getText().toString().trim();
                    String payAmt = etPaymentAmount.getText().toString().trim();
                    String payDate = tvPaymentDt.getText().toString().trim();

                    if (payRefNm.isEmpty()) {
                        Toast.makeText(c, "Reference name field is required.", Toast.LENGTH_SHORT).show();
                    } else if (payAmt.isEmpty()) {
                        Toast.makeText(c, "Amount field is required.", Toast.LENGTH_SHORT).show();
                    } else if (payDate.isEmpty()) {
                        Toast.makeText(c, "Date field is required.", Toast.LENGTH_SHORT).show();
                    } else {


                        //etPayRef.setText("");
                        //etPaymentAmount.setText("");
                        //tvPaymentDt.setText("");

                        String date_ = df.format(cal_order.getTime());

                        if (tvAdd.getText().toString().trim().equalsIgnoreCase("add")) {
                            alPayment.add(new PaymentViewModel(payRefNm, payAmt, date_));
                        } else if (tvAdd.getText().toString().trim().equalsIgnoreCase("edit")) {
                            alPayment.set(editPos, new PaymentViewModel(payRefNm, payAmt, date_));
                        } else if (tvAdd.getText().toString().trim().equalsIgnoreCase("delete")) {
                            alPayment.remove(editPos);
                        }
                        //paymentListAdapter.setItem(alPayment);
                        try {
                            modifyTaskPreferenceForMrpFromAllTask();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                } else if (status.equalsIgnoreCase(c.getString(R.string.status_selection))) {
                    try {
                        modifyTaskPreferenceForMrpFromAllTask();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }


                break;
            case R.id.rlHide:
                dismissDialogue(1);
                break;
            default:
                break;
        }
    }

    public interface OnDialogClickListener {
        void onDialogImageRunClick(JsonObject jsonObject);
    }

    Calendar cal_order = Calendar.getInstance();


    public void openDateView(final TextView textView) {

        DatePickerNew mDatePicker = new DatePickerNew(c, new DatePickerNew.PickDob() {
            @Override
            public void onSelect(Date date) {

                cal_order.setTime(date);

                SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy");
                String date_ = format.format(date);

                textView.setText(date_);

            }
        }, textView.getText().toString().trim());
        mDatePicker.setStyle(DialogFragment.STYLE_NORMAL, R.style.CustomDialog);
        mDatePicker.show(c.getFragmentManager(), "Select delivery date");
    }


    public void settime() {
        SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy hh:mm aaa");
        String time = df.format(cal_order.getTime());

        SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy hh:mm aaa");
        try {
            if (!time.equals("")) {
                Date date_ = format.parse(time);


                SimpleDateFormat timeformat = new SimpleDateFormat("hh:mm aaa");
                String str_time = timeformat.format(date_);

                SimpleDateFormat dtformat = new SimpleDateFormat("dd MMM yyyy");

                String str_date = dtformat.format(date_);
                tvModifyDelTime.setText(str_time);
                tvModifyDelDt.setText(str_date);
                tvPaymentDt.setText(str_date);

            }
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("ValidFragment")
    public class TimePicker extends DialogFragment implements TimePickerDialog.OnTimeSetListener {
        @Override
        public void onCancel(DialogInterface dialog) {
            super.onCancel(dialog);

        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            int hour = cal_order.get(Calendar.HOUR_OF_DAY);
            int minute = cal_order.get(Calendar.MINUTE);
            return new TimePickerDialog(getActivity(), this, hour, minute, DateFormat.is24HourFormat(getActivity()));
        }

        @Override
        public void onTimeSet(android.widget.TimePicker view, int hourOfDay, int minute) {

            cal_order.set(cal_order.get(Calendar.YEAR), cal_order.get(Calendar.MONTH), cal_order.get(Calendar.DAY_OF_MONTH), hourOfDay, minute);

            settime();


        }
    }

    JsonObject joAddEdit = new JsonObject();

    SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy hh:mm aaa");
    SessionManager sessionManager = new SessionManager();

    public void modifyTaskPreferenceForMrpFromAllTask() throws JSONException {


        joAddEdit = new JsonObject();

        String to_field_json_data = ShareModel.getI().taskPrefViewModel.getToFieldJsonData();
        String from_field_json_data = ShareModel.getI().taskPrefViewModel.getFromFieldJsonData();

        JSONObject joFrom = new JSONObject(from_field_json_data);
        JSONObject joTo = new JSONObject(to_field_json_data);
        if (joFrom.length() > 2 || joTo.length() > 2) {


            if (status.equalsIgnoreCase(c.getString(R.string.status_selection))) {


            } else if (ShareModel.getI().isFromAllTask) { // All task


                if (status.equalsIgnoreCase(c.getString(R.string.pay_details))) {
                    joTo.put("payment_reference", new Gson().toJson(alPayment));
                } else if (status.equalsIgnoreCase(c.getString(R.string.del_details))) {

                    if (taskPrefViewModel.getTaskPurpose().equalsIgnoreCase("Resource")) {
                        float amt = Float.parseFloat(etResAmount.getText().toString().trim());
                        float unit = Float.parseFloat(etResUnitOrQuantity.getText().toString().trim());
                        float total = amt * unit;
                        joTo.put("edit_total_chit_item_value", total);
                    } else if (taskPrefViewModel.getTaskPurpose().equalsIgnoreCase("Material")) {
                        joTo.put("edit_total_chit_item_value", etActualPrice.getText().toString().trim());
                    }

                    joTo.put("unit_value", etResUnitOrQuantity.getText().toString());
                    joTo.put("amount_value", etResAmount.getText().toString());

                    String date_ = df.format(cal_order.getTime());
                    joTo.put("EDIT_DATE", date_);
                    joTo.put("last_modify_by", sessionManager.getcurrentu_nm());
                }


            } else {                                // HistoryActivity

                if (status.equalsIgnoreCase(c.getString(R.string.pay_details))) {
                    joFrom.put("payment_reference", new Gson().toJson(alPayment));
                } else if (status.equalsIgnoreCase(c.getString(R.string.del_details))) {


                    if (taskPrefViewModel.getTaskPurpose().equalsIgnoreCase("Resource")) {
                        float amt = Float.parseFloat(etResAmount.getText().toString().trim());
                        float unit = Float.parseFloat(etResUnitOrQuantity.getText().toString().trim());
                        float total = amt * unit;
                        joFrom.put("edit_total_chit_item_value", total);

                        joFrom.put("unit_value", etResUnitOrQuantity.getText().toString());
                        joFrom.put("amount_value", etResAmount.getText().toString());


                    } else if (taskPrefViewModel.getTaskPurpose().equalsIgnoreCase("Material")) {
                        joFrom.put("edit_total_chit_item_value", etActualPrice.getText().toString().trim());
                    }


                    String date_ = df.format(cal_order.getTime());
                    joFrom.put("EDIT_DATE", date_);
                    joFrom.put("last_modify_by", sessionManager.getcurrentu_nm());
                }

            }


        }


        joAddEdit.addProperty("task_reference_id", ShareModel.getI().taskPrefViewModel.getTaskReferenceId());

        /*if (!ShareModel.getI().isFromAllTask && ShareModel.getI().taskPrefViewModel.getToCaseId().equalsIgnoreCase("0")) {
            Chit chit = ShareModel.getI().chit; // From order and empty
            joAddEdit.addProperty("to_chit_id", chit.getChit_id());
            joAddEdit.addProperty("to_ref_id", chit.getRef_id());
            joAddEdit.addProperty("to_case_id", chit.getCase_id());
        }*/

        if (status.equalsIgnoreCase(c.getString(R.string.del_details))) {
            joAddEdit.addProperty("to_field_json_data", joTo.toString());
            joAddEdit.addProperty("from_field_json_data", joFrom.toString());
        } else if (status.equalsIgnoreCase(c.getString(R.string.pay_details))) {
            joAddEdit.addProperty("to_field_json_data", joTo.toString());
            joAddEdit.addProperty("from_field_json_data", joFrom.toString());
        } else if (status.equalsIgnoreCase(c.getString(R.string.status_selection))) {
            //joAddEdit.addProperty("to_field_json_data", joTo.toString());
            //joAddEdit.addProperty("from_field_json_data", joFrom.toString());
            joAddEdit.addProperty("status", link_status);
        }

        final JsonObject joMrp = new JsonObject();
        JsonArray jsonElements = new JsonArray();
        jsonElements.add(joAddEdit);

        joMrp.add("olddata", jsonElements);

        MyRequestCall myRequestCall = new MyRequestCall();
        MyRequst myRequst = new MyRequst();
        myRequst.cookie = new SessionManager().getsession();

        myRequestCall.editTaskPreferencr(c, myRequst, joMrp, new MyRequestCall.CallRequest() {
            @Override
            public void onGetResponse(String response) {
                if (!response.isEmpty()) {

                    listener.onDialogImageRunClick(joMrp);
                    dismissDialogue(0);

                }
            }
        });


    }

    @BindView(R.id.rvPayment)
    RecyclerView rvPayment;

    @BindView(R.id.etPayRef)
    EditText etPayRef;

    @BindView(R.id.etPaymentAmount)
    EditText etPaymentAmount;

    @BindView(R.id.tvPaymentDt)
    TextView tvPaymentDt;

    @BindView(R.id.rlPayDate)
    RelativeLayout rlPayDate;

    @BindView(R.id.sv)
    ScrollView sv;


    PaymentListAdapter paymentListAdapter;
    ArrayList<PaymentViewModel> alPayment = new ArrayList<>();
    int editPos = 0;

    public void setPayList() {

        paymentListAdapter = new PaymentListAdapter(alPayment, c, new PaymentListAdapter.OnItemClickListener() {
            @Override
            public void onDelete(int item) {
                tvAdd.setText("Delete");
                editPos = item;
                fillField();
            }

            @Override
            public void onEdit(int item) {
                tvAdd.setText("Edit");
                editPos = item;
                fillField();

            }
        }, symbol_native);

        rvPayment.setLayoutManager(new LinearLayoutManager(c, LinearLayoutManager.VERTICAL, false));
        rvPayment.setAdapter(paymentListAdapter);
        rvPayment.setNestedScrollingEnabled(false);


    }

    private void fillField() {
        PaymentViewModel paymentViewModel = alPayment.get(editPos);
        etPaymentAmount.setText(paymentViewModel.payment_amount);
        etPayRef.setText(paymentViewModel.payment_reference);
        try {
            SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy hh:mm aaa");
            Date date = format.parse(paymentViewModel.payment_date);
            cal_order.setTime(date);
            settime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        sv.fullScroll(View.FOCUS_DOWN);
    }

    @BindView(R.id.rbLink)
    RadioButton rbLink;

    @BindView(R.id.rbDLlink)
    RadioButton rbDLlink;

    @BindView(R.id.rbComplete)
    RadioButton rbComplete;

    @BindView(R.id.rbIssue)
    RadioButton rbIssue;

    @BindView(R.id.rbDelete)
    RadioButton rbDelete;

    @BindView(R.id.rgStatus)
    RadioGroup rgStatus;

    String link_status = "";

    public void setRadioForChangeStatus() {

        rgStatus.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.rbLink:
                        link_status = "Link";
                        break;
                    case R.id.rbDelete:
                        link_status = "Deleted";
                        break;
                    case R.id.rbDLlink:
                        link_status = "Delink";
                        break;
                    case R.id.rbComplete:
                        link_status = "Complete";
                        break;
                    case R.id.rbIssue:
                        link_status = "Issue";
                        break;
                }
            }
        });

        switch (link_status.toLowerCase()) {
            case "link":
                rbLink.setChecked(true);
                break;
            case "delink":
                rbDLlink.setChecked(true);
                break;
            case "complete":
                rbComplete.setChecked(true);
                break;
            case "issue":
                rbIssue.setChecked(true);
                break;
            case "deleted":
                rbDelete.setChecked(true);
                break;
            default:
                rbLink.setChecked(true);
                break;
        }


    }


}


