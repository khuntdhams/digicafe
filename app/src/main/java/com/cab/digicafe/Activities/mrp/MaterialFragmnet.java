package com.cab.digicafe.Activities.mrp;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.cab.digicafe.Activities.RequestApis.MyRequestCall;
import com.cab.digicafe.Activities.RequestApis.MyRequst;
import com.cab.digicafe.Adapter.mrp.MrpListAdapter;
import com.cab.digicafe.Adapter.mrp.TaskPrefViewModel;
import com.cab.digicafe.Helper.Inad;
import com.cab.digicafe.Model.Chit;
import com.cab.digicafe.Model.ShareModel;
import com.cab.digicafe.MyCustomClass.JsonObjParse;
import com.cab.digicafe.R;
import com.google.gson.Gson;
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.cab.digicafe.DetailsFragment.item_detail;

public class MaterialFragmnet extends Fragment implements SwipyRefreshLayout.OnRefreshListener {


    public MaterialFragmnet() {

    }

    public Chit chit;

    @BindView(R.id.rvClicks)
    RecyclerView rvClicks;

    @BindView(R.id.tv_empty)
    TextView tv_empty;

    @BindView(R.id.tvTaskDelDt)
    TextView tvTaskDelDt;

    @BindView(R.id.pbTaskRef)
    ProgressBar pbTaskRef;

    @BindView(R.id.tvTotalOrder)
    TextView tvTotalOrder;

    String currency = "not_same";


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @BindView(R.id.llTaskDt)
    LinearLayout llTaskDt;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_material, container, false);
        //currency = SharedPrefUserDetail.getString(getActivity(), SharedPrefUserDetail.symbol_native,"") + " ";

        ButterKnife.bind(this, v);
        context = getActivity();
        llTaskDt.setVisibility(View.VISIBLE);

        alTask = new ArrayList<>();
        tv_empty.setVisibility(View.GONE);
        tvTotalOrder.setVisibility(View.GONE);

        String MRP_TAB = JsonObjParse.getValueEmpty(item_detail.getFooter_note(), "MRP_TAB");

        if (ShareModel.getI().isFromAllTask) { // From Task
            myRequst.chit_id = item_detail.getChit_id() + ""; // for to - in ALl task
            myRequst.to_chit_hash_id = "";
        } else if (!MRP_TAB.equalsIgnoreCase("")) { // From order

            myRequst.chit_id = ""; // for to - in ALl task
            myRequst.to_chit_hash_id = item_detail.getChit_hash_id();

           /* try {
                JSONArray jsonArray = new JSONArray(MRP_TAB);
                JSONObject jsonObject = jsonArray.getJSONObject(0);

                String To = JsonObjParse.getValueEmpty(jsonObject.toString(), "To"); // // for fromn - in My oreder
                String chit_hash_id = JsonObjParse.getValueEmpty(To, "chit_hash_id");

                myRequst.chit_id = "";
                myRequst.to_chit_hash_id = "" + chit_hash_id;


            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }*/

        }


        try {
            JSONObject jo_summary = new JSONObject(item_detail.getFooter_note());

            if (jo_summary.has("DATE")) {
                tvTaskDelDt.setText(jo_summary.getString("DATE"));

                if (jo_summary.getString("DATE").equals("")) {
                    tvTaskDelDt.setVisibility(View.INVISIBLE);
                } else {
                    tvTaskDelDt.setVisibility(View.VISIBLE);
                    tvTaskDelDt.setSelected(true);
                }


            } else {
                tvTaskDelDt.setVisibility(View.INVISIBLE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


        mSwipyRefreshLayout = (SwipyRefreshLayout) v.findViewById(R.id.swipyrefreshlayout);

        refreshlist();

        setMrpList();


        return v;
    }


    MrpListAdapter mrpListAdapter;
    ArrayList<TaskPrefViewModel> alTask = new ArrayList<>();

    MyRequestCall myRequestCall = new MyRequestCall();

    public void setMrpList() {

        /*String json = SharedPrefUserDetail.getString(this, SharedPrefUserDetail.user_traction_list, "");
        Type type = new TypeToken<ArrayList<ModelUserTraction>>() {
        }.getType();

        alUserTraction = new Gson().fromJson(json, type);

        if (alUserTraction == null) {
            alUserTraction = new ArrayList<>();
        }*/

        //alUserTraction = myRequestCall.getTraction(this);

        mrpListAdapter = new MrpListAdapter(alTask, context, new MrpListAdapter.OnItemClickListener() {
            @Override
            public void onDelete(int item) {
            }

            @Override
            public void onEdit(int item) {
                onRefresh(SwipyRefreshLayoutDirection.TOP);
            }
        });

        rvClicks.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        rvClicks.setAdapter(mrpListAdapter);
        rvClicks.setNestedScrollingEnabled(false);
        pbTaskRef.setVisibility(View.VISIBLE);
        fillList();

    }

    Context context;


    SwipyRefreshLayout mSwipyRefreshLayout;
    int pagecount = 1;
    boolean isdataavailable = true;

    public void refreshlist() {
        mSwipyRefreshLayout.setDirection(SwipyRefreshLayoutDirection.BOTH);
        mSwipyRefreshLayout.setOnRefreshListener(this);
    }

    MyRequst myRequst = new MyRequst();

    Double totalOdrPrice = 0.0;

    private void fillList() {

        myRequst.page = pagecount;


        mSwipyRefreshLayout.setRefreshing(true);

        myRequst.task_purpose = "material";
        myRequestCall.getTaskreference(context, new MyRequestCall.CallRequest() {
            @Override
            public void onGetResponse(String response) {
                pbTaskRef.setVisibility(View.GONE);
                mSwipyRefreshLayout.setRefreshing(false);
                if (pagecount == 1) {
                    alTask = new ArrayList<>();
                    totalOdrPrice = 0.0;
                }

                try {
                    JSONObject jObjRes = new JSONObject(response);

                    JSONObject jObjdata = jObjRes.getJSONObject("response");
                    JSONObject jObjResponse = jObjdata.getJSONObject("data");
                    String totalRecord = jObjResponse.getString("totalRecord");

                    //tvTitle.setText("User Traction [" + totalRecord + "]");
                    JSONArray jObjcontacts = jObjResponse.getJSONArray("content");

                    for (int i = 0; i < jObjcontacts.length(); i++) {
                        if (jObjcontacts.get(i) instanceof JSONObject) {
                            JSONObject jsnObj = (JSONObject) jObjcontacts.get(i);

                            TaskPrefViewModel modelUserTraction = new Gson().fromJson(jsnObj.toString(), TaskPrefViewModel.class);
                            String MRP_TAB = modelUserTraction.getToFieldJsonData();
                            if (!MRP_TAB.equalsIgnoreCase("") && MRP_TAB.length() > 2) { // {}

                                String forWhich = modelUserTraction.getFromFieldJsonData();

                                if (ShareModel.getI().isFromAllTask) {
                                    //forWhich = JsonObjParse.getValueEmpty(jsonObject.toString(), "To");
                                    forWhich = modelUserTraction.getToFieldJsonData();
                                } else {
                                    //forWhich = JsonObjParse.getValueEmpty(jsonObject.toString(), "From");
                                    forWhich = modelUserTraction.getFromFieldJsonData();
                                }

                                String symbol_native = JsonObjParse.getValueEmpty(forWhich, "symbol_native") + " ";
                                if (currency.equals("not_same")) {
                                    currency = symbol_native; // fst assign
                                } else {
                                    if (!currency.equals(symbol_native)) { // check
                                        currency = "";
                                    }
                                }

                                String total_chit_item_value = JsonObjParse.getValueEmpty(forWhich, "edit_total_chit_item_value");
                                if (!total_chit_item_value.isEmpty()) {

                                    String link_status = modelUserTraction.getStatus();
                                    if (link_status.equalsIgnoreCase("delink")) {

                                    } else if (link_status.equalsIgnoreCase("deleted")) {

                                    } else {
                                        totalOdrPrice = totalOdrPrice + Double.valueOf(total_chit_item_value);
                                    }

                                }

                            }

                            String link_status = modelUserTraction.getStatus();
                            if (link_status.equalsIgnoreCase("deleted")) {

                            } else {
                                alTask.add(modelUserTraction);
                            }
                            //alTask.add(modelUserTraction);
                        }
                    }

                    int totalVal = Integer.parseInt(totalRecord);
                    int displayEndRecord = jObjResponse.getInt("displayEndRecord");
                    if (totalVal == displayEndRecord) {
                        isdataavailable = false;
                    } else {
                        isdataavailable = true;
                    }

                    mrpListAdapter.setItem(alTask);

                    if (alTask.size() == 0) {
                        tv_empty.setVisibility(View.VISIBLE);
                        tvTotalOrder.setVisibility(View.VISIBLE);
                    } else {
                        tv_empty.setVisibility(View.GONE);
                        tvTotalOrder.setVisibility(View.GONE);
                    }

                    if (totalOdrPrice > 0) {
                        tvTotalOrder.setVisibility(View.VISIBLE);

                        if (currency.isEmpty() || currency.equals("not_same")) {
                            tvTotalOrder.setVisibility(View.GONE);
                        } else {
                            tvTotalOrder.setVisibility(View.VISIBLE);
                            tvTotalOrder.setText(currency + "" + Inad.getCurrencyDecimal(totalOdrPrice, context));
                        }
                    } else {
                        tvTotalOrder.setVisibility(View.GONE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, null, myRequst);

    }

    @Override
    public void onRefresh(SwipyRefreshLayoutDirection direction) {

        if (mSwipyRefreshLayout == null) {
            return;
        }

        if (direction == SwipyRefreshLayoutDirection.BOTTOM) {
            if (isdataavailable) {
                pagecount++;
                fillList();
            } else {
                mSwipyRefreshLayout.setRefreshing(false);
                Toast.makeText(context, "No more data available", Toast.LENGTH_LONG).show();
            }
        }

        if (direction == SwipyRefreshLayoutDirection.TOP) {
            isdataavailable = true;
            pagecount = 1;
            fillList();
        }

    }


  /*  @BindView(R.id.tvMrpJson)
    TextView tvMrpJson;*/


    // Tab


}
