package com.cab.digicafe.Activities.delivery;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cab.digicafe.Adapter.PlaceArrayAdapter;
import com.cab.digicafe.DetePickerFragment;
import com.cab.digicafe.Fragments.SummaryDetailFragmnet;
import com.cab.digicafe.Helper.SessionManager;
import com.cab.digicafe.Helper.SharedPrefUserDetail;
import com.cab.digicafe.MyCustomClass.JsonObjParse;
import com.cab.digicafe.R;
import com.cab.digicafe.TimePicker;
import com.cab.digicafe.Timeline.DateTimeUtils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.kofigyan.stateprogressbar.StateProgressBar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.cab.digicafe.Activities.MySupplierActivity.fromMrp;
import static com.cab.digicafe.OrderActivity.total_with_tax;

public class MrpDeliveryDialog extends AlertDialog {
    private OnDialogClickListener listener;
    public AppCompatActivity c;
    public Dialog d;
    public TextView tv_done, tv_msg;
    String add, date, time, remark, phone;
    LinearLayout llStepOne, llStepSecond, llStepThird, llStepFour;
    TextView tvFrom, tvNextOne, tvTo, tvNextSecond, tv_date_d, tv_time, tvNextThird,
            tvDate, tvFromName, tvFromContact, tvFromAddress, tvToName, tvToContact, tvToAddress, tvReference, etCalDistance, etCharges;
    TextInputEditText etFromName, etFromContactNo, etToName, etToContactNo, etToAddress, etReference;
    AutoCompleteTextView tv_google_loc, tv_google_loc_to;
    ImageView iv_clear, iv_clear_to, iv_date, iv_time, iv_close, ivFromMap;
    CheckBox cbToAddress;
    StateProgressBar usage_stateprogressbar;
    Double fromLatitude = 0.0, fromLongitude = 0.0;
    Double toLatitude = 0.0, toLongitude = 0.0;
    String locDistance = "";
    String ref_id = "";
    String case_id = "";
    double charge;
    String currency = "";
    String delivery_charge = "";

    public MrpDeliveryDialog(AppCompatActivity a, OnDialogClickListener listener, String add, String time, String date, String remark, String phone) {
        super(a);
        // TODO Auto-generated constructor stub
        this.c = a;
        this.listener = listener;
        this.add = add;
        this.date = date;
        this.time = time;
        this.remark = remark;
        this.phone = phone;
        field_json_data = SharedPrefUserDetail.getString(a, SharedPrefUserDetail.field_json_data, "");

    }

    String field_json_data = ""; // field_json_data - act
    SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mrp_dialog_deliverydetail);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
        params.gravity = Gravity.CENTER;
        getWindow().setAttributes(params);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);


        tv_done = (TextView) findViewById(R.id.tv_done);
        tv_msg = (TextView) findViewById(R.id.tv_msg);

        llStepOne = (LinearLayout) findViewById(R.id.llStepOne);
        llStepSecond = (LinearLayout) findViewById(R.id.llStepSecond);
        llStepThird = (LinearLayout) findViewById(R.id.llStepThird);
        llStepFour = (LinearLayout) findViewById(R.id.llStepFour);

        tvFrom = (TextView) findViewById(R.id.tvFrom);
        tvNextOne = (TextView) findViewById(R.id.tvNextOne);
        tvTo = (TextView) findViewById(R.id.tvTo);
        tvNextSecond = (TextView) findViewById(R.id.tvNextSecond);
        tv_date_d = (TextView) findViewById(R.id.tv_date_d);
        tv_time = (TextView) findViewById(R.id.tv_time);
        tvNextThird = (TextView) findViewById(R.id.tvNextThird);

        etFromName = (TextInputEditText) findViewById(R.id.etFromName);
        etFromContactNo = (TextInputEditText) findViewById(R.id.etFromContactNo);
        etToName = (TextInputEditText) findViewById(R.id.etToName);
        etToContactNo = (TextInputEditText) findViewById(R.id.etToContactNo);
        etToAddress = (TextInputEditText) findViewById(R.id.etToAddress);
        etReference = (TextInputEditText) findViewById(R.id.etReference);

        tv_google_loc = (AutoCompleteTextView) findViewById(R.id.tv_google_loc);
        tv_google_loc_to = (AutoCompleteTextView) findViewById(R.id.tv_google_loc_to);
        iv_clear = (ImageView) findViewById(R.id.iv_clear);
        iv_clear_to = (ImageView) findViewById(R.id.iv_clear_to);
        iv_date = (ImageView) findViewById(R.id.iv_date);
        iv_time = (ImageView) findViewById(R.id.iv_time);
        iv_close = (ImageView) findViewById(R.id.iv_close);
        cbToAddress = (CheckBox) findViewById(R.id.cbToAddress);

        usage_stateprogressbar = (StateProgressBar) findViewById(R.id.usage_stateprogressbar);


        tvDate = (TextView) findViewById(R.id.tvDate);
        tvFromName = (TextView) findViewById(R.id.tvFromName);
        tvFromContact = (TextView) findViewById(R.id.tvFromContact);
        tvFromAddress = (TextView) findViewById(R.id.tvFromAddress);
        tvToName = (TextView) findViewById(R.id.tvToName);
        tvToContact = (TextView) findViewById(R.id.tvToContact);
        tvToAddress = (TextView) findViewById(R.id.tvToAddress);
        ivFromMap = (ImageView) findViewById(R.id.ivFromMap);

        tvReference = (TextView) findViewById(R.id.tvReference);
        etCalDistance = (TextView) findViewById(R.id.etCalDistance);
        etCharges = (TextView) findViewById(R.id.etCharges);

        currency = SharedPrefUserDetail.getString(c, SharedPrefUserDetail.chit_symbol_native, "") + " ";

        delivery_charge = SharedPrefUserDetail.getString(c, SharedPrefUserDetail.delivery_charge, "");

        tvFrom.setText("From");
        tvNextOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (TextUtils.isEmpty(etFromName.getText().toString())) {
                    Toast.makeText(c, "Enter Name", Toast.LENGTH_SHORT).show();
                } else if (TextUtils.isEmpty(etFromContactNo.getText().toString())) {
                    Toast.makeText(c, "Enter Contact No.", Toast.LENGTH_SHORT).show();
                } else if (TextUtils.isEmpty(tv_google_loc.getText().toString())) {
                    Toast.makeText(c, "Enter Address", Toast.LENGTH_SHORT).show();
                } else {
                    Geocoder coder = new Geocoder(c);
                    List<Address> address;
                    LatLng p1 = null;

                    try {
                        // May throw an IOException
                        address = coder.getFromLocationName(tv_google_loc.getText().toString(), 1);
                        if (address.size() != 0) {
                            Address location = address.get(0);
                            fromLatitude = location.getLatitude();
                            fromLongitude = location.getLongitude();
                            p1 = new LatLng(location.getLatitude(), location.getLongitude());

                            if (fromLatitude == 0.0 || fromLongitude == 0.0) {
                                Toast.makeText(c, "Please Enter Valid Address", Toast.LENGTH_SHORT).show();
                            } else {
                                llStepOne.setVisibility(View.GONE);
                                llStepSecond.setVisibility(View.VISIBLE);
                                llStepThird.setVisibility(View.GONE);
                                llStepFour.setVisibility(View.GONE);
                                usage_stateprogressbar.setCurrentStateNumber(StateProgressBar.StateNumber.TWO);
                            }
                        } else {
                            Toast.makeText(c, "Please Enter Valid Address", Toast.LENGTH_SHORT).show();
                        }

                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                    //getLocationFromAddress(DeliveryChargesActivity.this, tv_google_loc.getText().toString());
                }
            }
        });

        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        tvNextSecond.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (TextUtils.isEmpty(etToName.getText().toString())) {
                    Toast.makeText(c, "Enter Name", Toast.LENGTH_SHORT).show();
                } else if (TextUtils.isEmpty(etToContactNo.getText().toString())) {
                    Toast.makeText(c, "Enter Contact No.", Toast.LENGTH_SHORT).show();
                } else if (TextUtils.isEmpty(tv_google_loc_to.getText().toString())) {
                    Toast.makeText(c, "Enter Address", Toast.LENGTH_SHORT).show();
                } else {
                    Geocoder coder = new Geocoder(c);
                    List<Address> address;
                    LatLng p1 = null;

                    try {
                        // May throw an IOException
                        address = coder.getFromLocationName(tv_google_loc_to.getText().toString(), 1);
                        if (address.size() != 0) {
                            Address location = address.get(0);
                            toLatitude = location.getLatitude();
                            toLongitude = location.getLongitude();
                            p1 = new LatLng(location.getLatitude(), location.getLongitude());

                            if (toLatitude == 0.0 || toLatitude == 0.0) {
                                Toast.makeText(c, "Please Enter Valid Address", Toast.LENGTH_SHORT).show();
                            } else {
                                llStepOne.setVisibility(View.GONE);
                                llStepSecond.setVisibility(View.GONE);
                                llStepThird.setVisibility(View.VISIBLE);
                                llStepFour.setVisibility(View.GONE);
                                usage_stateprogressbar.setCurrentStateNumber(StateProgressBar.StateNumber.THREE);
                            }
                        } else {
                            Toast.makeText(c, "Please Enter Valid Address", Toast.LENGTH_SHORT).show();
                        }

                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                    //  getLocationToAddress(DeliveryChargesActivity.this, tv_google_loc_to.getText().toString());
                }
            }
        });
        tvNextThird.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(tv_date_d.getText().toString())) {
                    Toast.makeText(c, "Enter Delivery Date", Toast.LENGTH_SHORT).show();
                } else if (TextUtils.isEmpty(tv_time.getText().toString())) {
                    Toast.makeText(c, "Enter Delivery Time", Toast.LENGTH_SHORT).show();
                } else {
                    llStepOne.setVisibility(View.GONE);
                    llStepSecond.setVisibility(View.GONE);
                    llStepThird.setVisibility(View.GONE);
                    llStepFour.setVisibility(View.VISIBLE);
                    usage_stateprogressbar.setCurrentStateNumber(StateProgressBar.StateNumber.FOUR);
                    tv_done.setVisibility(View.VISIBLE);
                    Location locationA = new Location("point A");

                    locationA.setLatitude(fromLatitude);
                    locationA.setLongitude(fromLongitude);

                    Location locationB = new Location("point B");

                    locationB.setLatitude(toLatitude);
                    locationB.setLongitude(toLongitude);

                    float distance = locationA.distanceTo(locationB) / 1000;
                    locDistance = String.format("%.1f", distance);
                    Log.d("distance", locDistance + "");
                    etCalDistance.setText(Float.parseFloat(locDistance) + " km");
                    charge = Double.parseDouble(delivery_charge) * Float.parseFloat(locDistance);
                    etCharges.setText(currency + "" + total_with_tax + "");
                    // setTaxFromProfile();
                    // calc_tax_value();
                    detail();
                }
            }
        });
        findViewById(R.id.iv_clear).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv_google_loc.setText("");
            }
        });
        findViewById(R.id.iv_clear_to).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv_google_loc_to.setText("");
            }
        });
        cbToAddress.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    tvFrom.setText("To");
                    tvTo.setText("From");
                } else {
                    tvFrom.setText("From");
                    tvTo.setText("To");
                }
            }
        });
        tv_google_loc.setThreshold(1);
        tv_google_loc_to.setThreshold(1);
        tv_google_loc.setOnItemClickListener(mAutocompleteClickListener);
        tv_google_loc_to.setOnItemClickListener(mAutocompleteClickListener);
        locationinitialized();

        iv_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DetePickerFragment mDatePicker = new DetePickerFragment(new DetePickerFragment.OnItemClickListener() {
                    @Override
                    public void onItemClick(Boolean isTrue, int year, int month, int dayOfMonth) {
                        if (isTrue) {
                            cal_order.set(year, month, dayOfMonth);
                            settime();
                        } else {
                        }
                    }
                });
                mDatePicker.setStyle(DialogFragment.STYLE_NORMAL, R.style.CustomDialog);
                mDatePicker.show(c.getSupportFragmentManager(), "Select delivery date");
            }
        });
        iv_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TimePicker mTimePicker = new TimePicker(new TimePicker.OnItemTimerClickListener() {
                    @Override
                    public void onItemClick(Boolean isTrue, int hourOfDay, int minute) {
                        if (isTrue) {
                            cal_order.set(cal_order.get(Calendar.YEAR), cal_order.get(Calendar.MONTH), cal_order.get(Calendar.DAY_OF_MONTH), hourOfDay, minute);

                            settime();

                        } else {
                        }
                    }
                });
                mTimePicker.show(c.getSupportFragmentManager(), "Select delivery time");
            }
        });

        if (fromMrp) {
            fillDetails();
        }

        ivFromMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    String uri = "http://maps.google.com/maps?saddr=" + fromLatitude + "," + fromLongitude + "&daddr=" + toLatitude + "," + toLongitude;
                    Log.d("route", uri);
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                    intent.setPackage("com.google.android.apps.maps");
                    c.startActivity(intent);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        tv_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                JSONObject joPart = new JSONObject();
                try {

                    joPart.put("fromName", "" + tvFromName.getText().toString());
                    joPart.put("fromContact", "" + tvFromContact.getText().toString());
                    joPart.put("fromAddress", "" + tvFromAddress.getText().toString());
                    joPart.put("toName", "" + tvToName.getText().toString());
                    joPart.put("toContact", "" + tvFromContact.getText().toString());
                    joPart.put("toAddress", "" + tvToAddress.getText().toString());
                    joPart.put("delivery_distance", "" + etCalDistance.getText().toString());
                    //    joPart.put("delivery_charge", "" + String.valueOf(finalTotal));
                    joPart.put("delivery_date", "" + tv_date_d.getText().toString());
                    joPart.put("delivery_time", "" + tv_time.getText().toString());
                    joPart.put("reference", "" + tvReference.getText().toString());
                    joPart.put("ref_id", "" + ref_id);
                    joPart.put("case_id", "" + case_id);
                    joPart.put("charge", "" + charge);
                    joPart.put("fromLatitude", "" + fromLatitude);
                    joPart.put("fromLongitude", "" + fromLongitude);
                    joPart.put("toLatitude", "" + toLatitude);
                    joPart.put("toLongitude", "" + toLongitude);
                    //  joPart.put("purpose", "" + purpose);
                    listener.onDialogImageRunClick(0, joPart);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                dismiss();
            }
        });
    }

    void fillDetails() {

        try {
            String BusinessInfo = JsonObjParse.getValueEmpty(SummaryDetailFragmnet.item.getFooter_note(), "BusinessInfo");
            String ADDRESS = JsonObjParse.getValueEmpty(SummaryDetailFragmnet.item.getFooter_note(), "ADDRESS");
            ref_id = SummaryDetailFragmnet.item.getRef_id();
            case_id = SummaryDetailFragmnet.item.getCase_id();
            tv_google_loc_to.setText(ADDRESS);
            //  etReference.setText(ref_id + " / " + case_id);

            if (tv_google_loc_to.getText().equals("")) {
                findViewById(R.id.iv_clear_to).setVisibility(View.GONE);
            } else {
                findViewById(R.id.iv_clear_to).setVisibility(View.VISIBLE);
            }


            if (!BusinessInfo.isEmpty()) {
                JSONArray jaBusinessInfo = new JSONArray(BusinessInfo);
                if (jaBusinessInfo.length() > 0) {
                    JSONObject joConsInfo = jaBusinessInfo.getJSONObject(0);
                    etFromName.setText(joConsInfo.getString("customer_nm"));
                    etFromContactNo.setText(joConsInfo.getString("mobile"));
                }
            }


            DecimalFormat twoDForm = new DecimalFormat("#.##");
            Double price = Double.parseDouble(SummaryDetailFragmnet.item.getTotal_chit_item_value());
            //  formatedprice = Double.valueOf(twoDForm.format(price));
            //  etRecAmount.setText(currency + Inad.getCurrencyDecimal(formatedprice, this));
            String DATE = JsonObjParse.getValueEmpty(SummaryDetailFragmnet.item.getFooter_note(), "DATE");

            String date = DateTimeUtils.parseDateTime(DATE, "dd MMM yyyy hh:mm aaa", "dd MMM yyyy");
            String time = DateTimeUtils.parseDateTime(DATE, "dd MMM yyyy hh:mm aaa", "hh:mm aaa");
            tv_date_d.setText(date);
            tv_time.setText(time);

            // jsonObject = new JSONObject(SummaryDetailFragmnet.item.getFooter_note());

            /*String pick_up_only = "";
            if (jsonObject.has("field_json_data")) {
                String field_json_data = JsonObjParse.getValueEmpty(SummaryDetailFragmnet.item.getFooter_note(), "field_json_data");
                pick_up_only = JsonObjParse.getValueEmpty(field_json_data, "pick_up_only");
            }
*/
            String ConsumerInfo = JsonObjParse.getValueEmpty(SummaryDetailFragmnet.item.getFooter_note(), "ConsumerInfo");
            if (!ConsumerInfo.isEmpty()) {
                JSONArray jaConsumerInfo = new JSONArray(ConsumerInfo);
                if (jaConsumerInfo.length() > 0) {
                    JSONObject joConsInfo = jaConsumerInfo.getJSONObject(0);
                    etToName.setText(joConsInfo.getString("customer_nm"));
                    etToContactNo.setText(joConsInfo.getString("mobile"));
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public interface OnDialogClickListener {
        void onDialogImageRunClick(int pos, JSONObject joDelivery);
    }

    @Override
    public void onBackPressed() {

        if (llStepOne.getVisibility() == View.VISIBLE) {
            super.onBackPressed();
        } else if (llStepSecond.getVisibility() == View.VISIBLE) {
            llStepOne.setVisibility(View.VISIBLE);
            llStepSecond.setVisibility(View.GONE);
            llStepThird.setVisibility(View.GONE);
            llStepFour.setVisibility(View.GONE);
            tv_done.setVisibility(View.GONE);
            usage_stateprogressbar.setCurrentStateNumber(StateProgressBar.StateNumber.ONE);
        } else if (llStepThird.getVisibility() == View.VISIBLE) {
            llStepOne.setVisibility(View.GONE);
            llStepSecond.setVisibility(View.VISIBLE);
            llStepThird.setVisibility(View.GONE);
            llStepFour.setVisibility(View.GONE);
            tv_done.setVisibility(View.GONE);
            usage_stateprogressbar.setCurrentStateNumber(StateProgressBar.StateNumber.TWO);
        } else if (llStepFour.getVisibility() == View.VISIBLE) {
            llStepOne.setVisibility(View.GONE);
            llStepSecond.setVisibility(View.GONE);
            llStepThird.setVisibility(View.VISIBLE);
            llStepFour.setVisibility(View.GONE);
            tv_done.setVisibility(View.GONE);
            usage_stateprogressbar.setCurrentStateNumber(StateProgressBar.StateNumber.THREE);
        }
    }

    Calendar cal_order = Calendar.getInstance();

    public void settime() {
        SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy hh:mm aaa");
        String time = df.format(cal_order.getTime());

        SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy hh:mm aaa");
        try {
            if (!time.equals("")) {
                Date date_ = format.parse(time);


                SimpleDateFormat timeformat = new SimpleDateFormat("hh:mm aaa");
                String str_time = timeformat.format(date_);

                SimpleDateFormat dtformat = new SimpleDateFormat("dd MMM yyyy");

                String str_date = dtformat.format(date_);
                tv_date_d.setText(str_date);
                tv_time.setText(str_time);

            }
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void dismiss() {
        super.dismiss();

        mGoogleApiClient.stopAutoManage(c);
        mGoogleApiClient.disconnect();

    }


    public void locationinitialized() {

        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(c)
                    .addApi(Places.GEO_DATA_API)
                    .enableAutoManage(c, GOOGLE_API_CLIENT_ID, new GoogleApiClient.OnConnectionFailedListener() {
                        @Override
                        public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

                        }
                    })
                    .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                        @Override
                        public void onConnected(@Nullable Bundle bundle) {
                            mPlaceArrayAdapter.setGoogleApiClient(mGoogleApiClient);
                        }

                        @Override
                        public void onConnectionSuspended(int i) {
                            mPlaceArrayAdapter.setGoogleApiClient(null);
                        }
                    })
                    .build();
        }

        tv_google_loc.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count > 0) {
                    findViewById(R.id.iv_clear).setVisibility(View.VISIBLE);
                } else {
                    findViewById(R.id.iv_clear).setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        tv_google_loc_to.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count > 0) {
                    findViewById(R.id.iv_clear_to).setVisibility(View.VISIBLE);
                } else {
                    findViewById(R.id.iv_clear_to).setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mPlaceArrayAdapter = new PlaceArrayAdapter(c, android.R.layout.simple_list_item_1,
                BOUNDS_MOUNTAIN_VIEW, null);

        tv_google_loc.setAdapter(mPlaceArrayAdapter);
        tv_google_loc_to.setAdapter(mPlaceArrayAdapter);
    }

    private GoogleApiClient mGoogleApiClient;

    private PlaceArrayAdapter mPlaceArrayAdapter;
    private static final LatLngBounds BOUNDS_MOUNTAIN_VIEW = new LatLngBounds(
            new LatLng(37.398160, -122.180831), new LatLng(37.430610, -121.972090));
    private static final int GOOGLE_API_CLIENT_ID = 0;

    private AdapterView.OnItemClickListener mAutocompleteClickListener
            = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            final PlaceArrayAdapter.PlaceAutocomplete item = mPlaceArrayAdapter.getItem(position);
            final String placeId = String.valueOf(item.placeId);
            //Log.i(LOG_TAG, "Selected: " + item.description);
            PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                    .getPlaceById(mGoogleApiClient, placeId);
            placeResult.setResultCallback(mUpdatePlaceDetailsCallback);
            //Log.i(LOG_TAG, "Fetching details for ID: " + item.placeId);
        }
    };

    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback
            = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {

                return;
            }
            final Place place = places.get(0);
            CharSequence attributions = places.getAttributions();
        }
    };

    public void detail() {
        if (tvFrom.getText().equals("From")) {
            tvFromName.setText(etFromName.getText().toString());
            tvFromContact.setText(etFromContactNo.getText().toString());
            tvFromAddress.setText(tv_google_loc.getText().toString());
            tvToName.setText(etToName.getText().toString());
            tvToContact.setText(etToContactNo.getText().toString());
            tvToAddress.setText(tv_google_loc_to.getText().toString());
        } else {
            tvFromName.setText(etToName.getText().toString());
            tvFromContact.setText(etToContactNo.getText().toString());
            tvFromAddress.setText(tv_google_loc_to.getText().toString());
            tvToName.setText(etFromName.getText().toString());
            tvToContact.setText(etFromContactNo.getText().toString());
            tvToAddress.setText(tv_google_loc.getText().toString());
        }
        tvReference.setText(etReference.getText().toString());
       /* if (ref_id.equalsIgnoreCase("")) {
            tvRefId.setText("From");
        } else {
            tvRefId.setText("From / " + ref_id + " / " + case_id);
        }*/
        tvDate.setText(tv_date_d.getText().toString() + " " + tv_time.getText().toString());
   /*     tvDistance.setText(etCalDistance.getText().toString());
        tvCharge.setText(currency + Inad.getCurrencyDecimal(total, this));
        tvRecAmount.setText(currency + Inad.getCurrencyDecimal(formatedprice, this));


        tvFinalTotal.setText(currency + Inad.getCurrencyDecimal(finalTotal, this));*/
        ivFromMap.setVisibility(View.VISIBLE);
       /* ivToMap.setVisibility(View.VISIBLE);
        if (tvFromAddress.getText().equals("")) {
            ivFromMap.setVisibility(View.GONE);
        } else if (tvToAddress.getText().equals("")) {
            ivToMap.setVisibility(View.GONE);
        }*/
    }
}