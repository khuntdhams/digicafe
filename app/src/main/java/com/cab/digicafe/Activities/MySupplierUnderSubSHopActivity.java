package com.cab.digicafe.Activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cab.digicafe.Activities.RequestApis.MyRequestCall;
import com.cab.digicafe.Adapter.CategoryListAdapter;
import com.cab.digicafe.Adapter.CatererAdapter;
import com.cab.digicafe.Apputil;
import com.cab.digicafe.CatelogActivity;
import com.cab.digicafe.Database.SqlLiteDbHelper;
import com.cab.digicafe.Dialogbox.CustomDialogClass;
import com.cab.digicafe.Dialogbox.SearchSupplier;
import com.cab.digicafe.Dialogbox.UpdateDialogClass;
import com.cab.digicafe.Helper.SessionManager;
import com.cab.digicafe.Helper.SharedPrefUserDetail;
import com.cab.digicafe.Model.Metal;
import com.cab.digicafe.Model.ModelFile;
import com.cab.digicafe.Model.ShareModel;
import com.cab.digicafe.Model.Supplier;
import com.cab.digicafe.Model.SupplierCategory;
import com.cab.digicafe.Model.SupplierNew;
import com.cab.digicafe.MyCustomClass.CheckVersion;
import com.cab.digicafe.MyCustomClass.JsonObjParse;
import com.cab.digicafe.MyCustomClass.PlayAnim;
import com.cab.digicafe.R;
import com.cab.digicafe.Rest.ApiClient;
import com.cab.digicafe.Rest.ApiInterface;
import com.cab.digicafe.Rest.Request.SupplierRequest;
import com.cab.digicafe.serviceTypeClass.GetServiceTypeFromBusiUserProfile;
import com.google.gson.Gson;
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.xiaofeng.flowlayoutmanager.Alignment;
import com.xiaofeng.flowlayoutmanager.FlowLayoutManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.cab.digicafe.Activities.MySupplierActivity.isFavourite;

public class MySupplierUnderSubSHopActivity extends AppCompatActivity implements SwipyRefreshLayout.OnRefreshListener {

    private List<Supplier> catererlist = new ArrayList<>();
    private RecyclerView recyclerView;
    private CatererAdapter mAdapter;
    private ProgressBar loadingview;
    private SessionManager sessionManager;
    SqlLiteDbHelper dbHelper;
    TextView tv_nodata;
    static boolean isdetach = false;
    boolean isfrommyagree = false;
    String to_bridge_id = "", currency_code = "", SupplierTitle = "";

    String industry_id = "";
    String building_id = "";
    Context context;
    boolean isClk = false;
    SearchSupplier searchSupplier;
    String et_supplier = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_caterer_sublist);
        ButterKnife.bind(this);

        rvCategory = (RecyclerView) findViewById(R.id.rvCategory);
        ivDown = (ImageView) findViewById(R.id.ivDown);
        setCategoryRv();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        sessionManager = new SessionManager(this);

        if (getIntent().getExtras() != null) {
            if (getIntent().getExtras().containsKey("SupplierTitle")) {
                SupplierTitle = getIntent().getExtras().getString("SupplierTitle", "");
            }
        }

        getSupportActionBar().setTitle(SupplierTitle.toUpperCase() + "");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        tv_nodata = (TextView) findViewById(R.id.tv_nodata);
        loadingview = (ProgressBar) findViewById(R.id.loadingview);
        mAdapter = new CatererAdapter(catererlist, new CatererAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Supplier item, int pos) {
                if (!isClk) {
                    isClk = true;
                    shopselect(item);

                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            isClk = false;
                        }
                    }, 500);
                }
            }

            @Override
            public void onItemLongClick(Supplier item, int pos) {
                if (sessionManager.getIsConsumer()) {
                    et_supplier = item.getUsername();
                    searchSupplier = new SearchSupplier(MySupplierUnderSubSHopActivity.this, new SearchSupplier.OnDialogClickListener() {
                        @Override
                        public void onDialogImageRunClick(int positon, String add) {

                            if (positon == 1) {
                                SearchSupplier(add);
                            }
                        }

                        @Override
                        public void onImgClick(List<ModelFile> data, int pos) {
                        }

                        @Override
                        public void onAddImg() {
                        }

                    }, et_supplier,true);

                    searchSupplier.show();

                }
            }

            @Override
            public void onDisplyLongDesc(Supplier item) {
                if (!item.getLocation().trim().isEmpty()) {

                    String open_time = JsonObjParse.getValueEmpty(item.getField_json_data(), "open_time");
                    String close_time = JsonObjParse.getValueEmpty(item.getField_json_data(), "close_time");
                    String work_hr_msg = JsonObjParse.getValueEmpty(item.getField_json_data(), "work_hr_msg");
                    String msg = "";
                    if (!open_time.isEmpty()) msg = "Open time : " + open_time + "\n";
                    if (!close_time.isEmpty()) msg = msg + "Close time : " + close_time + "\n";
                    if (!work_hr_msg.isEmpty()) msg = msg + "Note : " + work_hr_msg;
                    showInfo(item.getFirstname(), "Location : " + item.getLocation() + "\n" + msg);

                }
            }

            @Override
            public void onDisplyInfo(Supplier item) {
                String disc_info = JsonObjParse.getValueEmpty(item.getField_json_data(), "disc_info");
                showInfo("" + item.getFirstname(), "Discount : " + disc_info + "");

            }
        }, this, false);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        context = this;

        recyclerView.setAdapter(mAdapter);

        mSwipyRefreshLayout = (SwipyRefreshLayout) findViewById(R.id.swipyrefreshlayout);
        mSwipyRefreshLayout.setOnRefreshListener(this);

        to_bridge_id = SharedPrefUserDetail.getString(context, SharedPrefUserDetail.isfrommyaggregator, "");

        industry_id = sessionManager.getIndustry_id();
        building_id = sessionManager.getBuilding_id();
        usersignin();
        dbHelper = new SqlLiteDbHelper(this);

    }

    public void SearchSupplier(final String searchs) {

        ApiInterface apiService =
                ApiClient.getClient(this).create(ApiInterface.class);

        Call call;
        call = apiService.SearchSupplier(sessionManager.getsession(), searchs);

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {

                try {
                    if (response.isSuccessful()) {


                        if (searchSupplier != null) searchSupplier.dismiss();
                        String a = new Gson().toJson(response.body());


                        if (ShareModel.getI().isAddShopAuto.equals("ys")) {
                            ShareModel.getI().isAddShopAuto = "";
                            JSONObject jObjRes = new JSONObject(a);
                            JSONObject jObjdata = jObjRes.getJSONObject("response");
                            JSONObject jObjResponse = jObjdata.getJSONObject("data");
                            JSONArray ja_con = jObjResponse.getJSONArray("content");
                            SupplierNew supplierNew = new Gson().fromJson(ja_con.get(0).toString(), SupplierNew.class);
                            addAutoSupplierForConsumer(supplierNew, searchs);
                        } else {
                            Intent i = new Intent(MySupplierUnderSubSHopActivity.this, AddSupplierActivity.class);
                            i.putExtra("bridge", a);
                            i.putExtra("username", searchs);
                            i.putExtra("isadd", true);
                            startActivityForResult(i, 10);
                        }

                        //Toast.makeText(MySupplierActivity.this, "DELETED !", Toast.LENGTH_LONG).show();
                        // loadfirstpage();

                    } else {
                        try {

                            JSONObject jObjError = new JSONObject(response.errorBody().string());
                            JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                            String jo_error = jObjErrorresponse.getString("errormsg");
                            searchSupplier.getsupplier(jo_error);
                            // Toast.makeText(MySupplierActivity.this, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();
                        } catch (Exception e) {
                            searchSupplier.getsupplier("ERROR !");
                            Toast.makeText(MySupplierUnderSubSHopActivity.this, "ERROR !", Toast.LENGTH_LONG).show();

                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // Log error here since request failed
            }
        });
    }

    public void addAutoSupplierForConsumer(SupplierNew supplierNew, String unm) {
        try {

            JSONObject jo = new JSONObject();
            JSONArray ja_new = new JSONArray();

            JSONObject jsonObj;
            jsonObj = new JSONObject();
            jsonObj.put("preference", "Yes");
            jsonObj.put("firstname", supplierNew.getFirstname());
            jsonObj.put("lastname", supplierNew.getLastname());
            jsonObj.put("contact_no", supplierNew.getContactNo());
            jsonObj.put("email_id", supplierNew.getEmailId());
            jsonObj.put("location", supplierNew.getLocation());
            jsonObj.put("role", "");

            jsonObj.put("contact_bridge_id", supplierNew.getBridgeId());
            jsonObj.put("account_type", supplierNew.getAccountType());
            jsonObj.put("username", unm);
            jsonObj.put("flag", "add");

            ja_new.put(jsonObj);
            jo.put("newdata", ja_new);
            JSONArray ja_old = new JSONArray();
            jo.put("olddata", ja_old);

            SupplierRequest catRequest = new SupplierRequest();
            catRequest = new Gson().fromJson(jo.toString(), SupplierRequest.class);
            ApiInterface apiService = ApiClient.getClient(this).create(ApiInterface.class);
            Call call = apiService.PutSupplier(sessionManager.getsession(), catRequest);
            call.enqueue(new Callback() {
                @Override
                public void onResponse(Call call, Response response) {

                    if (response.isSuccessful()) {
                        loadfirstpage();
                    } else {
                        loadingview.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onFailure(Call call, Throwable t) {
                    // Log error here since request failed
                    loadingview.setVisibility(View.GONE);
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
            findViewById(R.id.rl_pb_cat).setVisibility(View.GONE);
        }
    }

    int totalcount = 0;
    String str_actionid = "";

    public void loadfirstpage() {
        pagecount = 1;
        isdataavailable = true;
        str_actionid = "";
        totalcount = 0;
        usersignin();
    }
    boolean ismycircle = false;

    public void usersignin() {

        tv_nodata.setVisibility(View.GONE);

        if (!to_bridge_id.equals("")) {
            isfrommyagree = true;
            if (to_bridge_id.equals("noback")) // home
            {
                to_bridge_id = sessionManager.getAggregatorprofile();
            } else {
                to_bridge_id = to_bridge_id; // for aggre shops inside my supplier sub shops
            }
        } else {
            ismycircle = true; // for circle shops inside my supplier sub shops
        }

        ApiInterface apiService = ApiClient.getClient(context).create(ApiInterface.class);

        loadingview.setVisibility(View.VISIBLE);

        Call call = null;
        if (isfrommyagree) {
            call = apiService.getCatererlist(sessionManager.getsession(), to_bridge_id, category, pagecount);  // networkDownListByBridgeID // My Agree
        } else {
            call = apiService.getCatererlist(sessionManager.getsession(), sessionManager.getAggregatorprofile(), industry_id, building_id, pagecount); // My Circle
        }

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                //  String cookie = response.;
                //  Log.d("test", cookie+ " ");
                loadingview.setVisibility(View.GONE);
                mSwipyRefreshLayout.setRefreshing(false);
                if (response.isSuccessful()) {
                    try {

                        Log.e("caterrelist", response.body().toString());
                        String a = new Gson().toJson(response.body());

                        JSONObject jObjRes = new JSONObject(a);
                        JSONObject jObjdata = jObjRes.getJSONObject("response");
                        JSONObject jObjResponse = jObjdata.getJSONObject("data");
                        JSONArray jObjcontacts = jObjResponse.getJSONArray("content");


                        try {

                            String totalRecord = jObjResponse.getString("totalRecord");
                            String displayEndRecord = jObjResponse.getString("displayEndRecord");

                            int total = jObjResponse.getInt("totalRecord");
                            int end = jObjResponse.getInt("displayEndRecord");
                            if (total == end) {
                                isdataavailable = false;
                            } else {
                                isdataavailable = true;
                            }

                        } catch (Exception e) {
                            isdataavailable = false;
                        }


                        if (pagecount == 1) {
                            catererlist.clear();
                        }

                        for (int i = 0; i < jObjcontacts.length(); i++) {
                            if (jObjcontacts.get(i) instanceof JSONObject) {
                                JSONObject jsnObj = (JSONObject) jObjcontacts.get(i);
                                Log.e("aliasjsnObj", jsnObj.toString());

                                Supplier obj = new Gson().fromJson(jsnObj.toString(), Supplier.class);
                                catererlist.add(obj);
                            }
                        }


                        mAdapter.notifyDataSetChanged();
                        tv_nodata.setVisibility(View.GONE);

                        if (catererlist.size() == 1) {
                            //Supplier item = catererlist.get(0);
                            //checkversioncode(item);
                        } else if (catererlist.size() == 0) {
                            tv_nodata.setVisibility(View.VISIBLE);
                        }

                        if (alCategory.size() == 0) {
                            alCategory = new ArrayList<>();
                            if (jObjResponse.has("categoryrArr")) {
                                JSONArray ja_category = jObjResponse.getJSONArray("categoryrArr");
                                if (ja_category.length() > 0)
                                    alCategory.add(new SupplierCategory("All"));
                                for (int i = 0; i < ja_category.length(); i++) {
                                    if (ja_category.get(i) instanceof JSONObject) {
                                        JSONObject jsnObj = (JSONObject) ja_category.get(i);
                                        SupplierCategory obj = new Gson().fromJson(jsnObj.toString(), SupplierCategory.class);
                                        alCategory.add(obj);
                                    }
                                }
                                if (alCategory == null) alCategory = new ArrayList<>();
                                if (alCategory.size() > 0) {

                                    ivDown.setVisibility(View.GONE);
                                    if (alCategory.size() > 5) ivDown.setVisibility(View.VISIBLE);

                                    rvCategory.setVisibility(View.VISIBLE);
                                    alCategory.get(0).setChecked(true);
                                    categoryListAdapter.setitem(alCategory);
                                } else {

                                    rvCategory.setVisibility(View.GONE);
                                }

                            } else {
                                rvCategory.setVisibility(View.GONE);
                            }
                        }

                    } catch (Exception e) {

                        Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                } else {

                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // Log error here since request failed
                mSwipyRefreshLayout.setRefreshing(false);
                loadingview.setVisibility(View.GONE);

                Log.e("error message", t.toString());
            }
        });
    }

    public void onRefresh(SwipyRefreshLayoutDirection direction) {
        //mSwipyRefreshLayout.setRefreshing(false);
        if (direction == SwipyRefreshLayoutDirection.TOP) {
            isdataavailable = true;
            pagecount = 1;
            usersignin();
        } else if (direction == SwipyRefreshLayoutDirection.BOTTOM) {
            if (isdataavailable) {
                pagecount++;
                usersignin();
            } else {
                mSwipyRefreshLayout.setRefreshing(false);
                Toast.makeText(context, "No more data available", Toast.LENGTH_LONG).show();
            }
        }
    }

    public void checkversioncode(final Supplier item) {
        CheckVersion checkVersion = new CheckVersion(context, sessionManager.getAggregator_ID(), new CheckVersion.Apicallback() {
            @Override
            public void onGetResponse(String a, boolean issuccess) {

            }

            @Override
            public void onUpdate(boolean isupdateforce, String ischeck) {

                if (ischeck.equalsIgnoreCase("ys")) {

                    new UpdateDialogClass(MySupplierUnderSubSHopActivity.this, new UpdateDialogClass.OnDialogClickListener() {
                        @Override
                        public void onDialogImageRunClick(int positon) {
                            if (positon == 0) {
                                shopselect(item);
                            } else if (positon == 1) {
                                final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                                try {
                                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                                } catch (android.content.ActivityNotFoundException anfe) {
                                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                                }


                            }
                        }
                    }, "Update is available !", isupdateforce).show();
                } else {
                    shopselect(item);
                }
            }
        });
    }

    public void shopselect(final Supplier item) {

        if (Apputil.isanothercateror(dbHelper, item)) {
            new CustomDialogClass((Activity) context, new CustomDialogClass.OnDialogClickListener() {
                @Override
                public void onDialogImageRunClick(int positon) {

                    if (positon == 0) {

                    } else if (positon == 1) {

                        sessionManager.setcurrentcaterer("");
                        sessionManager.setcurrentcaterername(item.getUsername());
                        sessionManager.setcurrentcaterer(item.getBridgeId());
                        sessionManager.setcurrentcatererstatus(item.getBusiness_status());
                        sessionManager.setRemark("");
                        sessionManager.setDate("");
                        sessionManager.setAddress("");
                        //   sessionManager.setCartproduct("");
                        //   sessionManager.setCartcount(0);
                        dbHelper.deleteallfromcart();
                        nextpage(item);

                    }
                }
            }, "You are moving out of " + sessionManager.getcurrentcatereraliasname() + ".").show();
        } else {
            sessionManager.setcurrentcatererstatus(item.getBusiness_status());
            sessionManager.setcurrentcaterername(item.getUsername());
            sessionManager.setcurrentcaterer(item.getBridgeId());
            nextpage(item);
        }
    }

    String agree = "";

    String purpose = "";

    SwipyRefreshLayout mSwipyRefreshLayout;
    int pagecount = 1;
    boolean isdataavailable = true;

    String category = "";

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    ArrayList<ModelFile> al_selet = new ArrayList<>();

    ImageView ivDown;
    RecyclerView rvCategory;
    ArrayList<SupplierCategory> alCategory = new ArrayList<>();
    CategoryListAdapter categoryListAdapter;

    boolean isLinear = true;

    public void setCategoryRv() {

        if (isLinear) {
            ivDown.setRotation(0);
        } else {
            ivDown.setRotation(180);
        }
        categoryListAdapter = new CategoryListAdapter(alCategory, this, new CategoryListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(SupplierCategory item) {

                //category = item.getPricelistCategoryName();
                category = item.getPricelistCategoryName();
                if (item.getPricelistCategoryName().equalsIgnoreCase("all")) category = "";

                isdataavailable = true;
                pagecount = 1;

                usersignin();
            }
        });

        if (isLinear) {
            rvCategory.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        } else {
            FlowLayoutManager layoutManager = new FlowLayoutManager();
            layoutManager.setAutoMeasureEnabled(true);
            layoutManager.setAlignment(Alignment.LEFT);
            rvCategory.setLayoutManager(layoutManager);
        }

        rvCategory.setItemAnimator(new DefaultItemAnimator());
        rvCategory.setAdapter(categoryListAdapter);
        rvCategory.setNestedScrollingEnabled(false);


        ivDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isLinear = !isLinear;
                setCategoryRv();
            }
        });
    }

    //  Display Long Desc Dialogue

    PlayAnim playAnim = new PlayAnim();

    @BindView(R.id.llAnimAddcat)
    LinearLayout llAnimAddcat;

    @OnClick(R.id.rlAddCategory)
    public void rlAddCategory(View v) {
        onBackPressed();
    }

    @BindView(R.id.rlAddCategory)
    RelativeLayout rlAddCategory;

    @BindView(R.id.tilHint)
    TextInputLayout tilHint;

    @BindView(R.id.tilMobile)
    TextInputLayout tilMobile;

    @BindView(R.id.tvAddEditTitle)
    TextView tvAddEditTitle;

    @BindView(R.id.tvAdd)
    TextView tvAdd;

    @BindView(R.id.tvCancel)
    TextView tvCancel;

    @BindView(R.id.tvMsg)
    TextView tvMsg;

    @OnClick(R.id.tvCancel)
    public void tvCancel(View v) {
        hideAddCat();
    }

    @OnClick(R.id.tvAdd)
    public void tvAdd(View v) {
        hideAddCat();
    }

    public void showInfo(String title, String msg) {
        tilMobile.setVisibility(View.GONE);
        tilHint.setVisibility(View.GONE);
        playAnim.slideDownRl(llAnimAddcat);
        rlAddCategory.setVisibility(View.VISIBLE);

        tvAdd.setText("Yes");
        tvMsg.setVisibility(View.GONE);

        tvAddEditTitle.setText(title);
        tvAdd.setText("Ok");
        tvCancel.setVisibility(View.GONE);
        tvMsg.setText(msg);
        tvMsg.setVisibility(View.VISIBLE);
    }

    public void hideAddCat() {
        playAnim.slideUpRl(llAnimAddcat);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                rlAddCategory.setVisibility(View.GONE);
            }
        }, 700);
    }

    @Override
    public void onBackPressed() {
        if (rlAddCategory.getVisibility() == View.VISIBLE) {
            hideAddCat();
        } else {
            super.onBackPressed();
        }
    }

    private void nextpage(final Supplier item) {

        SharedPrefUserDetail.setString(context, SharedPrefUserDetail.field_json_data, item.getField_json_data());

        new GetServiceTypeFromBusiUserProfile(context, item.getUsername(), new GetServiceTypeFromBusiUserProfile.Apicallback() {
            @Override
            public void onGetResponse(String a, String res, String sms_emp) {
                redirectToUnderShopOrCatalogue(a, res, item);
            }
        });
    }

    public void redirectToUnderShopOrCatalogue(String a, String res, final Supplier item) {

        String service_type_of = "";
        String field_json_data = "";
        try {

            JSONObject Jsonresponse = new JSONObject(res);
            JSONObject Jsonresponseinfo = Jsonresponse.getJSONObject("response");
            JSONArray jaData = Jsonresponseinfo.getJSONArray("data");
            if (jaData.length() > 0) {
                JSONObject data = jaData.getJSONObject(0);
                field_json_data = JsonObjParse.getValueEmpty(data.toString(), "field_json_data");
                service_type_of = JsonObjParse.getValueEmpty(field_json_data, "service_type_of").toLowerCase();

                String is_user_traction = JsonObjParse.getValueEmpty(field_json_data, "is_user_traction");
                if (is_user_traction.equalsIgnoreCase("yes")) {
                    MyRequestCall myRequestCall = new MyRequestCall();
                    SupplierNew supplierNew = new SupplierNew();
                    supplierNew.setBridgeId(item.getBridgeId());
                    //supplierNew.setFirstname(item.getFirstname());
                    supplierNew.setUsername(item.getUsername());

                    myRequestCall.setTraction(MySupplierUnderSubSHopActivity.this, supplierNew, field_json_data);
                }

            } else {
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (item.getAccountType() != null && item.getBusiness_type() != null &&
                item.getAccountType().equalsIgnoreCase("Business") &&
                item.getBusiness_type().equalsIgnoreCase("Aggregator")) {


            sessionManager.setAggregator_ID(getString(R.string.Aggregator)); // From Base Click
            SharedPrefUserDetail.setBoolean(context, SharedPrefUserDetail.isfromsupplier, false);


            String agree = item.getUsername();
            sessionManager.setAggregator_ID(agree);

            SharedPrefUserDetail.setString(context, SharedPrefUserDetail.isfrommyaggregator, item.getBridgeId());


            Intent mainIntent = new Intent(context, MySupplierUnderSubSHopActivity.class);
            mainIntent.putExtra("session", sessionManager.getsession());
            mainIntent.putExtra("SupplierTitle", item.getFirstname());
            mainIntent.putExtra("purpose", purpose);

            startActivity(mainIntent);


        } else if (item.getAccountType() != null && item.getBusiness_type() != null &&
                item.getAccountType().equalsIgnoreCase("Business") &&
                item.getBusiness_type().equalsIgnoreCase("Circle")) {


            sessionManager.setAggregator_ID(getString(R.string.Aggregator)); // From Base Click
            SharedPrefUserDetail.setBoolean(context, SharedPrefUserDetail.isfromsupplier, false);
            SharedPrefUserDetail.setString(context, SharedPrefUserDetail.isfrommyaggregator, "");
            String companyDetailShort = item.getCompany_detail_short().trim();

            try {

                String data = sessionManager.getIndustrySupplier();

                JSONObject jo_data = new JSONObject(data);
                JSONArray ja_buildingArr = jo_data.getJSONArray("buildingArr");

                sessionManager.setBuilding_id("");
                sessionManager.setIndustry_id("");

                //al_building.add(new industrySuppliermodel("Select Circle", ""));
                for (int i = 0; i < ja_buildingArr.length(); i++) {
                    JSONObject jo_res = ja_buildingArr.getJSONObject(i);
                    String building_name = jo_res.getString("building_name").trim();
                    String building_id = jo_res.getString("building_id");

                    if (companyDetailShort.equalsIgnoreCase(building_name)) {
                        sessionManager.setBuilding_id(building_id);
                        break;
                    }
                }

                JSONArray industryArr = jo_data.getJSONArray("industryArr");
                for (int i = 0; i < industryArr.length(); i++) {
                    JSONObject jo_res = industryArr.getJSONObject(i);
                    String industry_name = jo_res.getString("industry_name");
                    String industry_id = jo_res.getString("industry_id");
                    sessionManager.setIndustry_id(industry_id);
                    break;
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            //clickitem(1);
            Intent mainIntent = new Intent(this, MySupplierUnderSubSHopActivity.class);
            mainIntent.putExtra("session", sessionManager.getsession());
            mainIntent.putExtra("isBk", true);
            mainIntent.putExtra("SupplierTitle", item.getFirstname());
            mainIntent.putExtra("purpose", purpose);


            startActivity(mainIntent);

        } else { // business type - business then produst list/service - issue mgmt


            String metal_list = JsonObjParse.getValueEmpty(field_json_data, "metal_list");
            Metal metal = new Gson().fromJson(metal_list, Metal.class);
            if (metal == null) metal = new Metal();
            ShareModel.getI().metal = metal;


            if (service_type_of.equalsIgnoreCase(getString(R.string.none))) {
                showInfo("Message", "Service is not available.");
            } else if (service_type_of.equalsIgnoreCase(getString(R.string.upload_pdf))) {

                String picturePath = JsonObjParse.getValueEmpty(field_json_data, "picturePath");

                if (!picturePath.isEmpty()) {
                    Intent i = new Intent(context, WebViewActivity.class);
                    i.putExtra("url", picturePath);

                    int pos = picturePath.lastIndexOf("/");
                    if (pos > 0) {
                        picturePath = picturePath.substring(pos + 1);
                    }

                    i.putExtra("title", picturePath);
                    startActivity(i);
                }

            } else if (service_type_of.equalsIgnoreCase(getString(R.string.link_to_web))) {

                String webLink = JsonObjParse.getValueEmpty(field_json_data, "webLink").toLowerCase();

                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(webLink));
                startActivity(i);

            } else {

                purpose = a;
                if (a.isEmpty()) purpose = "request";

                purpose = purpose.toLowerCase();

                SharedPrefUserDetail.setString(context, SharedPrefUserDetail.purpose, purpose);

                /* set purpose  */

                sessionManager.setAggregator_ID(getString(R.string.Aggregator)); // From Base Click
                SharedPrefUserDetail.setString(context, SharedPrefUserDetail.isfrommyaggregator, "");

                SharedPrefUserDetail.setString(context, SharedPrefUserDetail.chit_currency_code, item.getCurrency_code());
                SharedPrefUserDetail.setString(context, SharedPrefUserDetail.chit_symbol_native, item.getSymbol_native());
                SharedPrefUserDetail.setString(context, SharedPrefUserDetail.chit_currency_code_id, item.getCurrency_code_id());
                SharedPrefUserDetail.setString(context, SharedPrefUserDetail.chit_symbol, item.getSymbol());

                if (purpose.equalsIgnoreCase("service") || purpose.equalsIgnoreCase(getString(R.string.sms_only))) {
                    sessionManager.setcurrentcatereraliasname(item.getAlias_name());

                    String supl = new Gson().toJson(item);
                    Intent i = new Intent(context, IssueMgmtActivity.class);
                    i.putExtra("purpose", purpose);
                    i.putExtra("supplier", supl);

                    startActivity(i);
                } /*else if (purpose.equalsIgnoreCase(getString(R.string.dental))) {
                    sessionManager.setcurrentcatereraliasname(item.getFirstname());

                    String supl = new Gson().toJson(item);
                    Intent i = new Intent(context, Template_DentalActivity.class);
                    i.putExtra("frombridgeidval", item.getFrom_bridge_id());
                    i.putExtra("purpose", purpose);
                    i.putExtra("supplier", supl);

                    startActivity(i);
                }*/ else {

                    sessionManager.setcurrentcatereraliasname(item.getAlias_name());

                    Intent i = new Intent(context, CatelogActivity.class);
                    i.putExtra("session", sessionManager.getsession());
                    i.putExtra("isFromSupplier", true);
                    i.putExtra("bridgeidval", item.getTo_bridge_id());
                    i.putExtra("frombridgeidval", item.getFrom_bridge_id());
                    i.putExtra("aliasname", item.getAlias_name());
                    i.putExtra("username", item.getUsername());
                    i.putExtra("currency_code", item.getCurrency_code());
                    i.putExtra("company_name", item.getAlias_name());
                    i.putExtra("isBk", true);

                    startActivity(i);
                }
            }
        }
    }
}