package com.cab.digicafe.Activities.RequestApis.MyResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CategoryViewModel implements Serializable {

    @SerializedName("pricelist_category_id")
    @Expose
    public String pricelistCategoryId;
    @SerializedName("bridge_id")
    @Expose
    public String bridgeId;
    @SerializedName("currency_code")
    @Expose
    public String currencyCode;
    @SerializedName("currency_code_id")
    @Expose
    public String currencyCodeId;
    @SerializedName("pricelist_category_name")
    @Expose
    public String pricelistCategoryName;
    @SerializedName("sort_by")
    @Expose
    public String sortBy;

    public CategoryViewModel(String pricelistCategoryName) {
        this.pricelistCategoryName = pricelistCategoryName;
    }

    @SerializedName("pricelist_category_image")
    @Expose
    public String pricelist_category_image;

    public boolean isSelect;

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof CategoryViewModel) {
            return ((CategoryViewModel) obj).pricelistCategoryName.equals(this.pricelistCategoryName);
        } else {
            return super.equals(obj);
        }
    }
}
