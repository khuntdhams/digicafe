package com.cab.digicafe.Activities.master;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.cab.digicafe.Activities.MyAppBaseActivity;
import com.cab.digicafe.Activities.RequestApis.MyRequst;
import com.cab.digicafe.Adapter.ctlg.SurveyOptionsListAdapter;
import com.cab.digicafe.Helper.SessionManager;
import com.cab.digicafe.Model.Catelog;
import com.cab.digicafe.Model.ShareModel;
import com.cab.digicafe.Model.SurveyOptions;
import com.cab.digicafe.MyCustomClass.JsonObjParse;
import com.cab.digicafe.R;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SendSurveyActivity extends MyAppBaseActivity {


    @BindView(R.id.etEnterSurveyQ)
    TextView etEnterSurveyQ;

    @BindView(R.id.tvSubmitSurvey)
    TextView tvSubmitSurvey;

    @BindView(R.id.etConsumerComment)
    EditText etConsumerComment;

    @BindView(R.id.rvSurveyOptions)
    RecyclerView rvSurveyOptions;

    Context context;
    SessionManager sessionManager;

    MyRequst myRequst = new MyRequst();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_survey);

        ButterKnife.bind(this);


        context = this;
        sessionManager = new SessionManager(context);
        myRequst.cookie = sessionManager.getsession();


        myRequst.cookie = sessionManager.getsession();
        myRequst.purpose = ShareModel.getI().purpose;
        myRequst.chit_name = ShareModel.getI().chit_name;


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("Survey" + "");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        checkSurvey();

        tvSubmitSurvey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("selected_options_by_user", optionSelection);
                jsonObject.addProperty("question", question);
                jsonObject.addProperty("comments_by_user", etConsumerComment.getText().toString());

                Log.e("okhttp", jsonObject.toString());
            }
        });

    }


    @Override
    public void onBackPressed() {

        super.onBackPressed();

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
        }


        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        final MenuItem item = menu.findItem(R.id.action_search);
        item.setVisible(false);


        return true;
    }


    ArrayList<SurveyOptions> alOptions = new ArrayList<>();
    SurveyOptionsListAdapter surveyOptionsListAdapter;


    String survey = "";
    int editPos = 0;

    String question = "", optionSelection = "", userComment = "";

    public void checkSurvey() {

        Catelog catelog = ShareModel.getI().catelog;
        //survey
        survey = JsonObjParse.getValueEmpty(catelog.getInternal_json_data(), "survey");


        String optionsList = JsonObjParse.getValueEmpty(survey, "SurveyOptions");

        SurveyOptions surveyOptions = new Gson().fromJson(optionsList, SurveyOptions.class);
        alOptions = surveyOptions.alOptions;
        if (alOptions == null) alOptions = new ArrayList<>();

        etEnterSurveyQ.setText(surveyOptions.que);

        question = surveyOptions.que;


        surveyOptionsListAdapter = new SurveyOptionsListAdapter(alOptions, this, new SurveyOptionsListAdapter.OnItemClickListener() {
            @Override
            public void onDelete(int item) {


            }

            @Override
            public void onEdit(int item, ArrayList<SurveyOptions> moviesList) {

                editPos = item;

                for (int i = 0; i < alOptions.size(); i++) {
                    alOptions.get(i).isEdit = false;
                }

                alOptions.get(editPos).isEdit = true;

                optionSelection = alOptions.get(editPos).option;

                surveyOptionsListAdapter.setItem(alOptions);

                tvSubmitSurvey.setAlpha(1f);
                tvSubmitSurvey.setEnabled(true);


            }
        }, false);

        //rvSurveyOptions.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        rvSurveyOptions.setLayoutManager(new GridLayoutManager(this, 2));
        rvSurveyOptions.setAdapter(surveyOptionsListAdapter);
        rvSurveyOptions.setNestedScrollingEnabled(false);


        tvSubmitSurvey.setAlpha(0.5f);
        tvSubmitSurvey.setEnabled(false);

    }


}
