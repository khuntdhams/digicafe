package com.cab.digicafe.Activities.other.walkin;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ExternalContactViewModel implements Serializable {

    @SerializedName("external_reference_id")
    @Expose
    private String externalReferenceId;
    @SerializedName("bridge_id")
    @Expose
    private String bridgeId;
    @SerializedName("purpose")
    @Expose
    private String purpose;
    @SerializedName("purpose_type")
    @Expose
    private String purposeType;
    @SerializedName("contact_number")
    @Expose
    private String contactNumber;
    @SerializedName("contact_name")
    @Expose
    private String contactName;
    @SerializedName("contact_visit_count")
    @Expose
    private String contactVisitCount;
    @SerializedName("contact_visit_value")
    @Expose
    private String contactVisitValue;
    @SerializedName("source_of_reference")
    @Expose
    private String sourceOfReference;
    @SerializedName("referrer_contact_number")
    @Expose
    private String referrerContactNumber;
    @SerializedName("referrer_contact_name")
    @Expose
    private String referrerContactName;
    @SerializedName("referrel_count")
    @Expose
    private String referrelCount;
    @SerializedName("referrel_value")
    @Expose
    private String referrelValue;
    @SerializedName("priority")
    @Expose
    private String priority;
    @SerializedName("exit_level")
    @Expose
    private String exitLevel;
    @SerializedName("exit_reason")
    @Expose
    private String exitReason;
    @SerializedName("field_json_data")
    @Expose
    private String fieldJsonData;
    @SerializedName("created_date")
    @Expose
    private String createdDate;
    @SerializedName("updated_date")
    @Expose
    private String updatedDate;
    @SerializedName("status")
    @Expose
    private String status;
    private final static long serialVersionUID = 2388879186164365070L;

    public String getExternalReferenceId() {
        return externalReferenceId;
    }

    public void setExternalReferenceId(String externalReferenceId) {
        this.externalReferenceId = externalReferenceId;
    }

    public String getBridgeId() {
        return bridgeId;
    }

    public void setBridgeId(String bridgeId) {
        this.bridgeId = bridgeId;
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public String getPurposeType() {
        return purposeType;
    }

    public void setPurposeType(String purposeType) {
        this.purposeType = purposeType;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getContactVisitCount() {
        if (contactVisitCount.isEmpty()) contactVisitCount = "0";
        return contactVisitCount;
    }

    public void setContactVisitCount(String contactVisitCount) {
        this.contactVisitCount = contactVisitCount;
    }

    public String getContactVisitValue() {
        if (contactVisitValue.isEmpty()) contactVisitValue = "0";
        return contactVisitValue;
    }

    public void setContactVisitValue(String contactVisitValue) {
        this.contactVisitValue = contactVisitValue;
    }

    public String getSourceOfReference() {
        return sourceOfReference;
    }

    public void setSourceOfReference(String sourceOfReference) {
        this.sourceOfReference = sourceOfReference;
    }

    public String getReferrerContactNumber() {
        return referrerContactNumber;
    }

    public void setReferrerContactNumber(String referrerContactNumber) {
        this.referrerContactNumber = referrerContactNumber;
    }

    public String getReferrerContactName() {
        return referrerContactName;
    }

    public void setReferrerContactName(String referrerContactName) {
        this.referrerContactName = referrerContactName;
    }

    public String getReferrelCount() {
        if (referrelCount.isEmpty()) referrelCount = "0";
        return referrelCount;
    }

    public void setReferrelCount(String referrelCount) {
        this.referrelCount = referrelCount;
    }

    public String getReferrelValue() {
        if (referrelValue.isEmpty()) referrelValue = "0";
        return referrelValue;
    }

    public void setReferrelValue(String referrelValue) {
        this.referrelValue = referrelValue;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getExitLevel() {
        return exitLevel;
    }

    public void setExitLevel(String exitLevel) {
        this.exitLevel = exitLevel;
    }

    public String getExitReason() {
        return exitReason;
    }

    public void setExitReason(String exitReason) {
        this.exitReason = exitReason;
    }

    public String getFieldJsonData() {
        if (fieldJsonData == null) fieldJsonData = "";
        return fieldJsonData;
    }

    public void setFieldJsonData(String fieldJsonData) {
        this.fieldJsonData = fieldJsonData;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


}
