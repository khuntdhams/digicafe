package com.cab.digicafe.Activities.master;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cab.digicafe.Activities.RequestApis.MyRequestCall;
import com.cab.digicafe.Adapter.CategoryListAdapter;
import com.cab.digicafe.Adapter.EmployessListAdapter;
import com.cab.digicafe.Adapter.master.MyNwShopAdapter;
import com.cab.digicafe.Helper.LoadImg;
import com.cab.digicafe.Helper.SessionManager;
import com.cab.digicafe.Model.Employee;
import com.cab.digicafe.Model.ShareModel;
import com.cab.digicafe.Model.Supplier;
import com.cab.digicafe.Model.SupplierCategory;
import com.cab.digicafe.Model.Userprofile;
import com.cab.digicafe.MyCustomClass.PlayAnim;
import com.cab.digicafe.MyCustomClass.Utils;
import com.cab.digicafe.R;
import com.cab.digicafe.Rest.ApiClient;
import com.cab.digicafe.Rest.ApiInterface;
import com.cab.digicafe.SignupActivity;
import com.cab.digicafe.serviceTypeClass.GetServiceTypeFromBusiUserProfile;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.xiaofeng.flowlayoutmanager.Alignment;
import com.xiaofeng.flowlayoutmanager.FlowLayoutManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddShopsUnderMasterActivity extends AppCompatActivity implements SwipyRefreshLayout.OnRefreshListener {


    boolean isLoad = false;
    boolean fromAT = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_shop_master);
        ButterKnife.bind(this);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            fromAT = extras.getBoolean("fromAT", false);
        }

        context = this;
        sessionManager = new SessionManager(this);

        rvCategory = (RecyclerView) findViewById(R.id.rvCategory);
        ivDown = (ImageView) findViewById(R.id.ivDown);

        setCategoryRv();

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        tv_nodata = (TextView) findViewById(R.id.tv_nodata);
        loadingview = (ProgressBar) findViewById(R.id.loadingview);
        mAdapter = new MyNwShopAdapter(catererlist, new MyNwShopAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Supplier item, int pos) {

            }

            @Override
            public void onItemEdit(Supplier item, int pos) {

                employee = null;

                supplier_edit = item;
                tvAddEditTitle.setText(item.getUsername() + "");
                showInfo("Edit");

                etShopId.setText(item.getUsername());
                etAliasNm.setText(item.alias_name);
                tvAdd.setText("Update");


                //al_aggre.get(0).setChecked(true);


                if (item.getAssign_name().isEmpty()) {
                    al_aggre.get(0).setChecked(true);
                } else {

                    for (int i = 0; i < al_aggre.size(); i++) {
                        al_aggre.get(i).setChecked(false);
                    }

                    for (int i = 0; i < al_aggre.size(); i++) {
                        String assigni = al_aggre.get(i).getUsername();
                        if (item.getAssign_name().equals(assigni)) {
                            al_aggre.get(i).setChecked(true);
                            break;
                        }
                    }

                }

                employessListAdapter.setitem(al_aggre);
            }

            @Override
            public void onItemLongClick(Supplier item, int pos) {

            }
        }, this, fromAT);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView.setAdapter(mAdapter);

        mSwipyRefreshLayout = (SwipyRefreshLayout) findViewById(R.id.swipyrefreshlayout);
        mSwipyRefreshLayout.setOnRefreshListener(this);

        loadingview.setVisibility(View.VISIBLE);

        tv_nodata.setVisibility(View.GONE);
        new GetServiceTypeFromBusiUserProfile(context, sessionManager.getcurrentu_nm(), new GetServiceTypeFromBusiUserProfile.Apicallback() {
            @Override
            public void onGetResponse(String a, String response, String sms_emp) {

                try {
                    JSONObject Jsonresponse = new JSONObject(response);
                    JSONObject Jsonresponseinfo = Jsonresponse.getJSONObject("response");
                    JSONArray jaData = Jsonresponseinfo.getJSONArray("data");
                    if (jaData.length() > 0) {
                        isLoad = true;
                        if (fromAT) {
                            action_add.setVisible(false);
                        } else {
                            action_add.setVisible(true);
                        }
                        JSONObject data = jaData.getJSONObject(0);

                        userprofile = new Gson().fromJson(data.toString(), Userprofile.class);
                        bridgeId = userprofile.getBridge_id();
                        usersignin();
                    } else {

                        loadingview.setVisibility(View.GONE);

                    }
                } catch (JSONException e) {
                    loadingview.setVisibility(View.GONE);
                    e.printStackTrace();
                }


            }
        });


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getSupportActionBar().setTitle("My Network");
        //counterFab.setVisibility(View.GONE);

        //showInfo();

        rlAddCategory.setVisibility(View.GONE);

        etShopId.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                tvAdd.setText("Add");
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        setEmployee();

    }

    Userprofile userprofile;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.action_add) {
            if (isLoad) {
                showInfo("Add");
            } else {
                Toast.makeText(context, "Please wait while getting data", Toast.LENGTH_LONG).show();
            }
        }

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    MenuItem action_add;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);


        this.menu = menu;
        //final MenuItem action_filter = menu.findItem(R.id.action_filter);
        //if(!isfrommyagree)action_filter.setVisible(true);

        final MenuItem item = menu.findItem(R.id.action_search);
        item.setVisible(false);

        action_add = menu.findItem(R.id.action_add);
        //action_add.setVisible(true);

        return true;
    }

    Menu menu;

    Context context;

    public void findProfile(final String searchs) {


    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    //  Display Long Desc Dialogue

    PlayAnim playAnim = new PlayAnim();

    @BindView(R.id.llAnimAddcat)
    LinearLayout llAnimAddcat;

    @BindView(R.id.etShopId)
    EditText etShopId;

    @BindView(R.id.etAliasNm)
    EditText etAliasNm;

    @BindView(R.id.tvAddEditTitle)
    TextView tvAddEditTitle;

    @BindView(R.id.tvAdd)
    TextView tvAdd;

    @BindView(R.id.tilShopId)
    TextInputLayout tilShopId;

    @BindView(R.id.tvCancel)
    TextView tvCancel;

    @BindView(R.id.tvMsg)
    TextView tvMsg;

    @BindView(R.id.rlAddCategory)
    RelativeLayout rlAddCategory;

    @BindView(R.id.llEditNw)
    LinearLayout llEditNw;

    @OnClick(R.id.tvCancel)
    public void tvCancel(View v) {
        hideAddCat();
    }

    @OnClick(R.id.rlAddCategory)
    public void rlAddCategory(View v) {
        hideAddCat();
    }

    LoadImg loadImg = new LoadImg();

    @OnClick(R.id.tvAdd)
    public void tvAdd(View v) {


        tvMsg.setVisibility(View.GONE);
        String q = etShopId.getText().toString().trim();

        if (tvAdd.getText().toString().equalsIgnoreCase("update")) {

            loadingview.setVisibility(View.VISIBLE);


            addShops(null, "edit");


        } else if (tvAdd.getText().toString().equalsIgnoreCase("register")) {

            ShareModel.getI().alCategory = new Gson().toJson(alCategory);
            ShareModel.getI().userprofile = userprofile;

            Intent i = new Intent(context, SignupActivity.class);
            i.putExtra("from", "addshop");

            i.putExtra("username", etShopId.getText().toString());
            startActivityForResult(i, 55);
        } else if (q.length() < 6 || q.length() > 12) {

            tvMsg.setText("Shop id must be 6 to 12 character required");
            tvMsg.setVisibility(View.VISIBLE);
        } else { // Add

            SessionManager sessionManager = new SessionManager(context);

            new GetServiceTypeFromBusiUserProfile(context, sessionManager.getsession(), etShopId.getText().toString().trim(), new GetServiceTypeFromBusiUserProfile.Apicallback() {
                @Override
                public void onGetResponse(String a, String response, String sms_emp) {


                    try {

                        if (response.isEmpty()) {

                            tvMsg.setVisibility(View.VISIBLE);
                            tvMsg.setText(a);
                            if (a.equalsIgnoreCase("User does not exists")) {
                                tvMsg.setText(a + ", please register.");
                                tvAdd.setText("Register");
                            }

                            //Inad.alerter("Alert !", "Please enter valid id..", context);

                        } else {

                            JSONObject Jsonresponse = new JSONObject(response);
                            JSONObject Jsonresponseinfo = Jsonresponse.getJSONObject("response");
                            JSONObject joData = Jsonresponseinfo.getJSONObject("data");
                            JSONArray jaData = joData.getJSONArray("content");
                            if (jaData.length() > 0) {
                                JSONObject data = jaData.getJSONObject(0);

                                Userprofile userprofile = new Gson().fromJson(data.toString(), Userprofile.class);

                                addShops(userprofile, "add");

                            } else {

                            }

                            /*JSONObject Jsonresponse = new JSONObject(response);
                            JSONObject Jsonresponseinfo = Jsonresponse.getJSONObject("response");
                            JSONArray jaData = Jsonresponseinfo.getJSONArray("data");
                            if (jaData.length() > 0) {
                                JSONObject data = jaData.getJSONObject(0);
                                Userprofile userprofile = new Gson().fromJson(data.toString(), Userprofile.class);

                                addShops(userprofile);

                            }*/
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });


        }

    }

    @Override
    public void onBackPressed() {
        if (rlAddCategory.getVisibility() == View.VISIBLE) {
            hideAddCat();
        } else {
            super.onBackPressed();
        }
    }

    public void hideAddCat() {

        playAnim.slideUpRl(llAnimAddcat);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                rlAddCategory.setVisibility(View.GONE);
                Utils.hideSoftKeyboard(context, etShopId);
            }
        }, 700);
    }

    MyRequestCall myRequestCall = new MyRequestCall();

    public void showInfo(String flag) {
        playAnim.slideDownRl(llAnimAddcat);
        rlAddCategory.setVisibility(View.VISIBLE);
        tvMsg.setVisibility(View.GONE);
        tvAdd.setText(flag);

        if (flag.equalsIgnoreCase("edit")) {
            etShopId.setEnabled(false);
            etShopId.setAlpha(0.5f);
            //tvAddEditTitle.setText("Update network");
            llEditNw.setVisibility(View.VISIBLE);
            tilShopId.setVisibility(View.GONE);

        } else {

            tilShopId.setVisibility(View.VISIBLE);

            etShopId.setEnabled(true);
            etShopId.setAlpha(1f);

            etShopId.setText("");
            etShopId.requestFocus();

            tvAddEditTitle.setText("Add network");
            llEditNw.setVisibility(View.GONE);

        }

        Utils.showSoftKeyboard(this);

    }

    Supplier supplier_edit;

    public void addShops(final Userprofile userprofile, final String flag) {

        try {
            JSONObject sendobj = new JSONObject();


            JSONArray jaNewData = new JSONArray();
            JSONObject a1 = new JSONObject();
            try {
                a1.put("flag", flag);

                if (flag.equalsIgnoreCase("add")) {
                    a1.put("account_type", userprofile.getAccount_type());
                    a1.put("contact_bridge_id", userprofile.getBridge_id());
                    a1.put("contact_no", userprofile.getContact_no());
                    a1.put("email_id", userprofile.getEmail_id());
                    a1.put("firstname", userprofile.getFirstname());
                    a1.put("lastname", userprofile.getLastname());
                    a1.put("location", userprofile.getLocation());
                    a1.put("preference", "Yes");
                    a1.put("role", "");
                    a1.put("username", etShopId.getText().toString().trim());
                    a1.put("is_force_handshake", "1");
                    jaNewData.put(a1);
                    sendobj.put("newdata", jaNewData);
                    JSONArray a = new JSONArray();
                    sendobj.put("olddata", a);
                } else {

                    a1.put("alias_name", etAliasNm.getText().toString().trim());
                    if (employee != null)
                        a1.put("employee_bridge_id", employee.getEmployee_bridge_id());
                    else a1.put("employee_bridge_id", "");
                    a1.put("network_contact_id", supplier_edit.getNetwork_contact_id() + "");
                    a1.put("to_bridge_id", supplier_edit.getTo_bridge_id());
                    a1.put("username", supplier_edit.getUsername());


                    jaNewData.put(a1);
                    sendobj.put("olddata", jaNewData);

                    JSONArray a = new JSONArray();
                    sendobj.put("newdata", a);

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }


            JSONObject object = sendobj;
            JsonParser jsonParser = new JsonParser();
            JsonObject gsonObject = (JsonObject) jsonParser.parse(object.toString());

            SessionManager sessionManager = new SessionManager(context);
            myRequestCall.addNewNwForAggregator(context, sessionManager.getsession(), gsonObject, new MyRequestCall.CallRequest() {
                @Override
                public void onGetResponse(String response) {


                    if (flag.equalsIgnoreCase("edit")) {
                        loadingview.setVisibility(View.GONE);
                        onBackPressed();
                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                finish();
                                startActivity(getIntent());
                            }
                        }, 500);

                    } else {
                        tvAdd.setBackground(ContextCompat.getDrawable(context, R.drawable.roundcorner_color));
                        tvAdd.setText("Add");
                        if (!response.isEmpty()) {
                            Utils.hideSoftKeyboard(context, etShopId);

                            tvMsg.setVisibility(View.VISIBLE);
                            tvMsg.setText(etShopId.getText().toString() + " added.");
                            etShopId.setText("");

                            finish();
                            startActivity(getIntent());

                        } else {

                        }
                    }


                }
            });

        } catch (JsonParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 55) {
            if (resultCode == Activity.RESULT_OK) {
                String username = data.getStringExtra("username");
                showInfo("Add");
                etShopId.setText(username);
                etShopId.setSelection(etShopId.getText().length());
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }

    ImageView ivDown;
    RecyclerView rvCategory;
    ArrayList<SupplierCategory> alCategory = new ArrayList<>();
    CategoryListAdapter categoryListAdapter;

    SwipyRefreshLayout mSwipyRefreshLayout;
    int pagecount = 1;
    boolean isdataavailable = true;

    String category = "";

    boolean isLinear = true;

    public void setCategoryRv() {

        if (isLinear) {
            ivDown.setRotation(0);
        } else {
            ivDown.setRotation(180);
        }

        categoryListAdapter = new CategoryListAdapter(alCategory, this, new CategoryListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(SupplierCategory item) {

                //category = item.getPricelistCategoryName();
                category = item.getPricelistCategoryName();
                if (item.getPricelistCategoryName().equalsIgnoreCase("all")) category = "";

                isdataavailable = true;
                pagecount = 1;

                usersignin();


            }
        });

        if (isLinear) {
            rvCategory.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        } else {
            FlowLayoutManager layoutManager = new FlowLayoutManager();
            layoutManager.setAutoMeasureEnabled(true);
            layoutManager.setAlignment(Alignment.LEFT);
            rvCategory.setLayoutManager(layoutManager);
        }
        //

        rvCategory.setItemAnimator(new DefaultItemAnimator());
        rvCategory.setAdapter(categoryListAdapter);
        rvCategory.setNestedScrollingEnabled(false);


        ivDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isLinear = !isLinear;
                setCategoryRv();
            }
        });

    }


    private ProgressBar loadingview;
    private SessionManager sessionManager;
    private List<Supplier> catererlist = new ArrayList<>();
    private RecyclerView recyclerView;
    private MyNwShopAdapter mAdapter;
    TextView tv_nodata;
    String bridgeId = "";

    public void usersignin() {


        tv_nodata.setVisibility(View.GONE);

        ApiInterface apiService =
                ApiClient.getClient(context).create(ApiInterface.class);


        String to_bridge_id = "";
        loadingview.setVisibility(View.VISIBLE);

        Call call = null;

        call = apiService.getCatererlist(sessionManager.getsession(), bridgeId, category, pagecount);  // networkDownListByBridgeID // My Agree

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {

                loadingview.setVisibility(View.GONE);
                mSwipyRefreshLayout.setRefreshing(false);
                if (response.isSuccessful()) {
                    try {

                        Log.e("caterrelist", response.body().toString());
                        String a = new Gson().toJson(response.body());

                        JSONObject jObjRes = new JSONObject(a);
                        JSONObject jObjdata = jObjRes.getJSONObject("response");
                        JSONObject jObjResponse = jObjdata.getJSONObject("data");
                        JSONArray jObjcontacts = jObjResponse.getJSONArray("content");


                        try {

                            String totalRecord = jObjResponse.getString("totalRecord");
                            String displayEndRecord = jObjResponse.getString("displayEndRecord");

                            int total = jObjResponse.getInt("totalRecord");
                            int end = jObjResponse.getInt("displayEndRecord");
                            if (total == end) {
                                isdataavailable = false;
                            } else {
                                isdataavailable = true;
                            }

                        } catch (Exception e) {
                            isdataavailable = false;
                        }


                        if (pagecount == 1) {
                            catererlist.clear();
                        }

                        for (int i = 0; i < jObjcontacts.length(); i++) {
                            if (jObjcontacts.get(i) instanceof JSONObject) {
                                JSONObject jsnObj = (JSONObject) jObjcontacts.get(i);
                                Log.e("aliasjsnObj", jsnObj.toString());

                                Supplier obj = new Gson().fromJson(jsnObj.toString(), Supplier.class);
                                catererlist.add(obj);
                            }
                        }


                        mAdapter.notifyDataSetChanged();
                        tv_nodata.setVisibility(View.GONE);

                        if (catererlist.size() == 1) {

                        } else if (catererlist.size() == 0) {
                            tv_nodata.setVisibility(View.VISIBLE);
                        }


                        if (alCategory.size() == 0) {
                            alCategory = new ArrayList<>();
                            if (jObjResponse.has("categoryrArr")) {
                                JSONArray ja_category = jObjResponse.getJSONArray("categoryrArr");
                                if (ja_category.length() > 0)
                                    alCategory.add(new SupplierCategory("All"));
                                for (int i = 0; i < ja_category.length(); i++) {
                                    if (ja_category.get(i) instanceof JSONObject) {
                                        JSONObject jsnObj = (JSONObject) ja_category.get(i);
                                        SupplierCategory obj = new Gson().fromJson(jsnObj.toString(), SupplierCategory.class);
                                        alCategory.add(obj);
                                    }
                                }
                                if (alCategory == null) alCategory = new ArrayList<>();
                                if (alCategory.size() > 0) {

                                    ivDown.setVisibility(View.GONE);
                                    if (alCategory.size() > 5) ivDown.setVisibility(View.VISIBLE);

                                    rvCategory.setVisibility(View.VISIBLE);
                                    alCategory.get(0).setChecked(true);
                                    categoryListAdapter.setitem(alCategory);
                                } else {

                                    rvCategory.setVisibility(View.GONE);
                                }

                            } else {
                                rvCategory.setVisibility(View.GONE);
                            }
                        }


                    } catch (Exception e) {

                        Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                } else {

                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // Log error here since request failed
                mSwipyRefreshLayout.setRefreshing(false);
                loadingview.setVisibility(View.GONE);

                Log.e("error message", t.toString());
            }
        });
    }


    @Override
    public void onRefresh(SwipyRefreshLayoutDirection direction) {

        if (direction == SwipyRefreshLayoutDirection.TOP) {
            isdataavailable = true;
            pagecount = 1;
            usersignin();
        } else if (direction == SwipyRefreshLayoutDirection.BOTTOM) {
            if (isdataavailable) {
                pagecount++;
                usersignin();
            } else {
                mSwipyRefreshLayout.setRefreshing(false);
                Toast.makeText(context, "No more data available", Toast.LENGTH_LONG).show();
            }

        }

    }

    private EmployessListAdapter employessListAdapter;
    ArrayList<Employee> al_aggre = new ArrayList<>();
    Employee employee;

    @BindView(R.id.rv_employeee)
    RecyclerView rv_employeee;


    public void setEmployee() {

        employessListAdapter = new EmployessListAdapter(al_aggre, this, new EmployessListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Employee ite) {
                employee = ite;
            }
        });

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        rv_employeee.setLayoutManager(mLayoutManager);
        rv_employeee.setAdapter(employessListAdapter);

        getEmployee();

    }

    public void getEmployee() {

        ApiInterface apiService = ApiClient.getClient(this).create(ApiInterface.class);

        Call call;
        call = apiService.getEmployee(sessionManager.getsession());

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {

                try {

                    if (response.isSuccessful()) {

                        String a = new Gson().toJson(response.body());
                        JSONObject jObjRes = new JSONObject(a);
                        JSONObject jObjdata = jObjRes.getJSONObject("response");
                        JSONObject jObjResponse = jObjdata.getJSONObject("data");
                        String totalRecord = jObjResponse.getString("totalRecord");
                        JSONArray jObjcontacts = jObjResponse.getJSONArray("content");

                        String displayEndRecord = jObjResponse.getString("displayEndRecord");
                        if (totalRecord.equals(displayEndRecord)) {
                            // isdataavailable = false;
                        } else {
                            // isdataavailable = true;
                        }


                        al_aggre.add(new Employee("Root/Owner"));

                        for (int i = 0; i < jObjcontacts.length(); i++) {
                            if (jObjcontacts.get(i) instanceof JSONObject) {
                                JSONObject jsnObj = (JSONObject) jObjcontacts.get(i);
                                Employee obj = new Gson().fromJson(jsnObj.toString(), Employee.class);
                                al_aggre.add(obj);
                            }
                        }

                        rv_employeee.setVisibility(View.VISIBLE);
                        if (al_aggre.size() == 1) {
                            rv_employeee.setVisibility(View.GONE);
                        }

                        employessListAdapter.setitem(al_aggre);

                    } else {
                        try {
                            JSONObject jObjError = new JSONObject(response.errorBody().string());
                            JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                            Toast.makeText(AddShopsUnderMasterActivity.this, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } catch (Exception e) {

                }


            }

            @Override
            public void onFailure(Call call, Throwable t) {


            }
        });


    }


}
