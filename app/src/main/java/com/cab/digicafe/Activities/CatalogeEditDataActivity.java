package com.cab.digicafe.Activities;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.graphics.Point;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.cab.digicafe.Activities.healthTemplate.MyDoctorData;
import com.cab.digicafe.Adapter.SelectFileAdapter;
import com.cab.digicafe.Adapter.TemplateSelectAdapter;
import com.cab.digicafe.Adapter.ctlg.MetalListAdapter;
import com.cab.digicafe.Adapter.ctlg.SurveyOptionsListAdapter;
import com.cab.digicafe.Database.SettingDB;
import com.cab.digicafe.Database.SettingModel;
import com.cab.digicafe.Dialogbox.CustomDialogClass;
import com.cab.digicafe.Dialogbox.DeleteDialogClass;
import com.cab.digicafe.Fragments.CatalougeContent;
import com.cab.digicafe.Helper.Inad;
import com.cab.digicafe.Helper.LoadImg;
import com.cab.digicafe.Helper.SessionManager;
import com.cab.digicafe.Helper.SharedPrefUserDetail;
import com.cab.digicafe.Model.Metal;
import com.cab.digicafe.Model.ModelFile;
import com.cab.digicafe.Model.ShareModel;
import com.cab.digicafe.Model.SpinModel;
import com.cab.digicafe.Model.SurveyOptions;
import com.cab.digicafe.Model.Userprofile;
import com.cab.digicafe.MyCustomClass.ApplicationUtil;
import com.cab.digicafe.MyCustomClass.AutoCompleteTv;
import com.cab.digicafe.MyCustomClass.Compressor;
import com.cab.digicafe.MyCustomClass.JsonObjParse;
import com.cab.digicafe.MyCustomClass.PlayAnim;
import com.cab.digicafe.MyCustomClass.SearchableSpinner;
import com.cab.digicafe.R;
import com.cab.digicafe.Rest.ApiClient;
import com.cab.digicafe.Rest.ApiInterface;
import com.cab.digicafe.Rest.Request.CatRequest;
import com.cab.digicafe.photopicker.activity.PickImageActivity;
import com.google.api.client.extensions.android.json.AndroidJsonFactory;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.services.vision.v1.Vision;
import com.google.api.services.vision.v1.VisionRequestInitializer;
import com.google.api.services.vision.v1.model.AnnotateImageRequest;
import com.google.api.services.vision.v1.model.BatchAnnotateImagesRequest;
import com.google.api.services.vision.v1.model.BatchAnnotateImagesResponse;
import com.google.api.services.vision.v1.model.EntityAnnotation;
import com.google.api.services.vision.v1.model.Feature;
import com.google.api.services.vision.v1.model.WebDetection;
import com.google.api.services.vision.v1.model.WebEntity;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.media.MediaRecorder.VideoSource.CAMERA;

public class CatalogeEditDataActivity extends MyAppBaseActivity implements TextWatcher, TemplateSelectAdapter.OnItemClickListener {

    @BindView(R.id.iv_back)
    ImageView iv_back;

    @BindView(R.id.ll_edit)
    LinearLayout ll_edit;

    @BindView(R.id.ll_save)
    LinearLayout ll_save;

    @BindView(R.id.iv_edit)
    ImageView iv_edit;

    @BindView(R.id.iv_delete)
    ImageView iv_delete;

    @BindView(R.id.iv_save)
    ImageView iv_save;

    @BindView(R.id.tv_title)
    EditText tv_title;

    @BindView(R.id.et_mrp)
    EditText et_mrp;

    @BindView(R.id.et_Amrp)
    EditText et_Amrp;

    @BindView(R.id.et_dis_per)
    EditText et_dis_per;

    @BindView(R.id.et_dis_val)
    EditText et_dis_val;

    @BindView(R.id.tvCatStatus)
    TextView tvCatStatus;

    @BindView(R.id.et_cgst)
    EditText et_cgst;

    @BindView(R.id.et_sgst)
    EditText et_sgst;

    @BindView(R.id.tvTaxField1)
    TextView tvTaxField1;

    @BindView(R.id.txtWeight)
    TextView txtWeight;

    @BindView(R.id.tvTaxField2)
    TextView tvTaxField2;

    @BindView(R.id.llCgst)
    LinearLayout llCgst;

    @BindView(R.id.llSgst)
    LinearLayout llSgst;

    @BindView(R.id.et_offer)
    EditText et_offer;

    @BindView(R.id.et_xref)
    EditText et_xref;

    @BindView(R.id.tv_total)
    TextView tv_total;

   /* @BindView(R.id.iv_upload)
    ImageView iv_upload;*/

    @BindView(R.id.iv_add)
    ImageView iv_add;

    @BindView(R.id.rl_upload)
    RelativeLayout rl_upload;

    @BindView(R.id.tv_progress)
    TextView tv_progress;

    @BindView(R.id.tv_calc_dis)
    TextView tv_calc_dis;

    @BindView(R.id.tv_calc_a_dis)
    TextView tv_calc_a_dis;

    @BindView(R.id.tv_calc_ctax)
    TextView tv_calc_ctax;

    @BindView(R.id.tv_calc_stax)
    TextView tv_calc_stax;

    @BindView(R.id.tvTotalDiscPerc)
    TextView tvTotalDiscPerc;

    @BindView(R.id.tvTotalWoTax)
    TextView tvTotalWoTax;

    @BindView(R.id.tv_p_cur)
    TextView tv_p_cur;

    @BindView(R.id.tv_a_p_cur)
    TextView tv_a_p_cur;

    @BindView(R.id.tv_dp_cur)
    TextView tv_dp_cur;

    @BindView(R.id.etCommnetForStatus)
    EditText etCommnetForStatus;

    @BindView(R.id.rv_selectfile)
    RecyclerView rv_selectfile;
    SelectFileAdapter selectFileAdapter;

    CatalougeContent item;
    String str_model;
    boolean isadd = false, isCopy = false;
    CatalougeContent obj;
    boolean issave = false;
    String out_of_stock = "";
    SettingModel sm;
    String currency = "";
    String currency_code_id = "";

    @BindView(R.id.llSelectTemplete)
    CardView llSelectTemplete;

    @BindView(R.id.rvSelectTemplate)
    RecyclerView rvSelectTemplate;

    MyDoctorData myDoctorData;
    TemplateSelectAdapter mAdapter;
    int templatePos = 0;

    @BindView(R.id.spn_template)
    Spinner spn_template;

    AutoCompleteTv autoCompleteTv = new AutoCompleteTv();
    ArrayList<AutoCompleteTv> al_fill = new ArrayList<>();
    int templateId = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cataloge_edit_data);
        ButterKnife.bind(this);
        rlAddCategory.setVisibility(View.GONE);
        hideAddCat();
        sessionManager = new SessionManager(this);
        myDoctorData = new MyDoctorData();
        Bundle extras = getIntent().getExtras();
        str_model = extras.getString("CatalougeContent");
        isadd = extras.getBoolean("isadd");
        if (extras.containsKey("isCopy")) isCopy = extras.getBoolean("isCopy");
        obj = new Gson().fromJson(str_model, CatalougeContent.class);

        if (isadd) {

            currency = extras.getString("symbol_native");
            currency_code_id = extras.getString("currency_code_id");

            ll_edit.setVisibility(View.GONE);
            ll_save.setVisibility(View.VISIBLE);
            tvCatStatus.setText("Add Item");

        } else {

            tvCatStatus.setText("Edit Item");
            currency = obj.getSymbol_native();
            currency_code_id = obj.getCurrencyCodeId();


            if (isCopy) {
                tvCatStatus.setText("Copy Item");
                ll_edit.setVisibility(View.GONE);
                ll_save.setVisibility(View.VISIBLE);
            } else {
                ll_edit.setVisibility(View.VISIBLE);
                ll_save.setVisibility(View.GONE);
                edit();
            }
            if (!ShareModel.getI().dbUnderMaster.isEmpty()) {
                tvCatStatus.setText("Add Item");
                spn_template.setClickable(false);
                spn_template.setEnabled(false);
            }

        }

        spn_template.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                calc_tax_value();
                if (myDoctorData.getList().get(position).getTemplateName().equalsIgnoreCase("RPD") || myDoctorData.getList().get(position).getTemplateName().equalsIgnoreCase("Precision Attachment")) {
                    llAdditionalPrice.setVisibility(View.VISIBLE);

                } else {
                    llAdditionalPrice.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        tv_p_cur.setText(currency);
        tv_dp_cur.setText(currency);
        tv_a_p_cur.setText(currency);

        sm = new SettingModel();
        final SettingDB db = new SettingDB(this);
        List<SettingModel> list_setting = db.GetItems();
        sm = list_setting.get(0);

        inits3();
        initvision();

        tv_title.setText(obj.getName());

        out_of_stock = obj.getOutOfStock();

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        String userprofile = sessionManager.getUserprofile();
        userProfileData = new Gson().fromJson(userprofile, Userprofile.class);

        try {
            String tax = obj.getTaxJsonData();
            if (tax != null) {
                //JSONObject jo_tax = new JSONObject(tax);
                //sgst = Double.valueOf(jo_tax.getString("SGST"));
                //cgst = Double.valueOf(jo_tax.getString("CGST"));

                if (isadd) {
                    setTaxFromProfile();

                } else {
                    setTax(obj.getTaxJsonData());
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        setAdditional(obj.getAvailableStock());

        et_offer.setText(obj.getOffer());
        et_xref.setText(obj.getCrossReference());
        et_Amrp.setText("0.00");

        iv_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll_edit.setVisibility(View.GONE);
                ll_save.setVisibility(View.VISIBLE);
                save();

            }
        });

        iv_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isdetecting) {
                    showPictureDialog();
                    //showGalleryCamera.showMultiPictureDialog();
                } else {
                    Toast.makeText(CatalogeEditDataActivity.this, "Please wait while detecting images...", Toast.LENGTH_LONG).show();
                }
            }
        });

        if (ShareModel.getI().alCategory != null && !ShareModel.getI().alCategory.equalsIgnoreCase("[]")) {
            iv_save.setImageResource(R.drawable.next);
        } else {
            iv_save.setImageResource(R.mipmap.save);
        }
        if (!ShareModel.getI().dbUnderMaster.isEmpty())
            iv_edit.performClick(); // edit, del not for a database under master

        iv_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String price = et_mrp.getText().toString().trim();
                String title = tv_title.getText().toString().trim();
                String dis_mod = et_dis_per.getText().toString().trim();
                String dis_val = et_dis_val.getText().toString().trim();

                String minPrice = etMinPriceRangeForQ.getText().toString().trim();
                String maxPrice = etMaxPriceRangeForQ.getText().toString().trim();

                int minPval = -2;
                int maxPval = -1;
                if (minPrice.length() > 0) {
                    minPval = Integer.parseInt(etMinPriceRangeForQ.getText().toString().trim());
                }
                if (maxPrice.length() > 0) {
                    maxPval = Integer.parseInt(etMaxPriceRangeForQ.getText().toString().trim());
                }

                if (llJewelInfo.getVisibility() == View.VISIBLE && price.isEmpty()) {
                    Toast.makeText(CatalogeEditDataActivity.this, "Enter material from setting or enter valid values!", Toast.LENGTH_LONG).show();
                } else if (llServeyInfo.getVisibility() == View.VISIBLE) {
                    title = etEnterSurveyQ.getText().toString().trim();
                    tv_title.setText(title);
                    if (title.isEmpty()) {
                        Toast.makeText(CatalogeEditDataActivity.this, "Enter survey question!", Toast.LENGTH_LONG).show();
                    } else if (tvSaveOptions.getText().toString().equalsIgnoreCase("edit")) {
                        Toast.makeText(CatalogeEditDataActivity.this, "Please update record", Toast.LENGTH_LONG).show();
                    } else {

                        if (isCopy && obj.getName().trim().equals(title)) {
                            Toast.makeText(CatalogeEditDataActivity.this, "Survey question is already exist !", Toast.LENGTH_LONG).show();
                        } else if (al_selet.size() > 0) {
                            str_img = new String[al_selet.size()];
                            new ImageCompress().execute();
                        } else {
                            str_img = new String[0];
                            putdata();
                        }

                    }
                } else if (isNegative) {
                    Toast.makeText(CatalogeEditDataActivity.this, "Enter valid values!", Toast.LENGTH_LONG).show();
                } else if (title.isEmpty()) {
                    Toast.makeText(CatalogeEditDataActivity.this, "Title Must Be Required!", Toast.LENGTH_LONG).show();
                }/* else if (price.isEmpty()) {
                    Toast.makeText(CatalogeEditDataActivity.this, "Price Must Be Required!", Toast.LENGTH_LONG).show();

                } else if (dis_mod.isEmpty()) {
                    Toast.makeText(CatalogeEditDataActivity.this, "Discount Percentage Must Be Required!", Toast.LENGTH_LONG).show();

                } else if (dis_val.isEmpty()) {
                    Toast.makeText(CatalogeEditDataActivity.this, "Discount Value Must Be Required!", Toast.LENGTH_LONG).show();
                }*/ else {

                    if (price.isEmpty()) price = "0";
                    if (dis_mod.isEmpty()) dis_mod = "0";
                    if (dis_val.isEmpty()) dis_val = "0";

                    Double dis_price = 0.0;
                    Double dis_perc = 0.0;
                    Double mrp = 0.0;
                    mrp = Double.valueOf(price);

                    dis_price = Double.parseDouble(dis_val);
                    dis_perc = Double.parseDouble(dis_mod);

                    String weight = etJewelWeight.getText().toString().trim();
                    if (llJewelInfo.getVisibility() == View.VISIBLE && !weight.isEmpty()) {
                        float wt = Float.parseFloat(weight);
                        //mrp = (mrp * wt) / 10; // 10 gram
                        mrp = (mrp * wt) / 1; // 1 unit
                    }

                    if (dis_price.doubleValue() > mrp.doubleValue()) {
                        Toast.makeText(CatalogeEditDataActivity.this, "Discount Value Can Not Larger Than Mrp Value!", Toast.LENGTH_LONG).show();
                    } else if (dis_perc > 100) {
                        Toast.makeText(CatalogeEditDataActivity.this, "Discount Percentage Must Be Valid Required!", Toast.LENGTH_LONG).show();
                    } else if (llWholesale.getVisibility() == View.VISIBLE && minPrice.isEmpty()) {
                        Toast.makeText(CatalogeEditDataActivity.this, "Min. Price Must Be Required!", Toast.LENGTH_LONG).show();
                    }
                   /* else if (llWholesale.getVisibility()==View.VISIBLE&&maxPrice.isEmpty()) {
                        Toast.makeText(CatalogeEditDataActivity.this, "Max. Price Must Be Required!", Toast.LENGTH_LONG).show();
                    }*/
                    else if (llPropertyFinancialBlock.getVisibility() == View.VISIBLE && et_mrp.getText().toString().isEmpty()) {
                        Toast.makeText(CatalogeEditDataActivity.this, "Please enter price!", Toast.LENGTH_LONG).show();
                    } else if (llPropertyFinancialBlock.getVisibility() == View.VISIBLE && et_xref.getText().toString().isEmpty()) {
                        Toast.makeText(CatalogeEditDataActivity.this, "Please enter xRef!", Toast.LENGTH_LONG).show();
                    } else if (llWholesale.getVisibility() == View.VISIBLE && maxPval != -1 && minPval == maxPval) {
                        Toast.makeText(CatalogeEditDataActivity.this, "Price should not be same!", Toast.LENGTH_LONG).show();
                    } else if (llWholesale.getVisibility() == View.VISIBLE && maxPval != -1 && minPval > maxPval) {
                        Toast.makeText(CatalogeEditDataActivity.this, "Please enter valid minimum price!", Toast.LENGTH_LONG).show();
                    } else {
                        if (isCopy && obj.getName().trim().equals(title)) {
                            Toast.makeText(CatalogeEditDataActivity.this, "Name is already exist !", Toast.LENGTH_LONG).show();
                        } else if (al_selet.size() > 0) {
                            str_img = new String[al_selet.size()];
                            new ImageCompress().execute();
                        } else {
                            str_img = new String[0];
                            putdata();
                        }

                    }

                }
            }
        });

        String[] PERMISSIONS = {android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_EXTERNAL_STORAGE};
        if (!Inad.hasPermissions(CatalogeEditDataActivity.this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(CatalogeEditDataActivity.this, PERMISSIONS, 801);
        } else {

        }

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rv_selectfile.setLayoutManager(mLayoutManager);


        selectFileAdapter = new SelectFileAdapter(al_selet, this, new SelectFileAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(String item) {
            }

            @Override
            public void onDelete(int pos) {
                al_selet.remove(pos);
                selectFileAdapter.setItem(al_selet);
                //if(al_selet.size()==0)iv_upload.setVisibility(View.GONE);
                //else iv_upload.setVisibility(View.VISIBLE);

            }

            @Override
            public void onClickString(String pos) {
                tv_title.setText(pos);
            }

            @Override
            public void onImgClick(List<ModelFile> data, int pos) {

                if (isdetecting) {
                    Toast.makeText(CatalogeEditDataActivity.this, "Please wait while detecting images...", Toast.LENGTH_SHORT).show();
                } else {
                    ArrayList<ModelFile> al_img = new ArrayList<>();
                    al_img.addAll(data);
                    String imgarray = new Gson().toJson(al_img);
                    Intent intent = new Intent(CatalogeEditDataActivity.this, ImagePreviewActivity.class);
                    intent.putExtra("imgarray", imgarray);
                    intent.putExtra("pos", pos);
                    intent.putExtra("isdrag", false);
                    startActivityForResult(intent, 11);
                }

            }
        }, false, true, false);
        rv_selectfile.setAdapter(selectFileAdapter);

        //if(al_selet.size()==0)iv_upload.setVisibility(View.GONE);
        //else iv_upload.setVisibility(View.VISIBLE);


       /* iv_upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                str_img = new String[al_selet.size()];
                uploadimg();

            }
        });*/

        rl_upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        // Set Whole Tax Before listener

        llPropertyFinancialBlock.setVisibility(View.GONE);
        et_dis_per.addTextChangedListener(this);
        et_dis_val.addTextChangedListener(this);
        et_mrp.addTextChangedListener(this);
        et_Amrp.addTextChangedListener(this);
        et_cgst.addTextChangedListener(this);
        et_sgst.addTextChangedListener(this);
        etSqFt.addTextChangedListener(this);

        et_additional_per.addTextChangedListener(this);
        et_additional_val.addTextChangedListener(this);

        etInfoVal1.addTextChangedListener(this);
        etInfoVal2.addTextChangedListener(this);
        etInfoVal3.addTextChangedListener(this);
        etInfoVal4.addTextChangedListener(this);
        etInfoVal5.addTextChangedListener(this);

        //et_cgst.addTextChangedListener(new ValueTextWatcher(et_cgst, et_sgst, this));
        //et_sgst.addTextChangedListener(new ValueTextWatcher(et_sgst, et_cgst, this));

        et_cgst.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                ApplicationUtil.setFocusedId(v.getId());
            }
        });

        et_sgst.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                ApplicationUtil.setFocusedId(v.getId());
            }
        });

       /* et_sgst.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                issgst = true;
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(issgst)
                {
                    et_cgst.setText(s+"");
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

                et_cgst.addTextChangedListener(this);
                et_cgst.requestFocus();
            }
        });
*/

        /******************** Tax from Discount *************************/

        if (centralgst == 0 && isadd) tv_calc_ctax.setText("");
        else tv_calc_ctax.setText("+ " + currency + Inad.getCurrencyDecimal(centralgst, this));
        if (stategst == 0 && isadd) tv_calc_stax.setText("");
        else tv_calc_stax.setText("+ " + currency + Inad.getCurrencyDecimal(stategst, this));

        try {
            c_img = 0;
            JSONArray ja_img = new JSONArray(obj.getImageJsonData().toString());
            str_img = new String[ja_img.length()];
            for (int i = 0; i < ja_img.length(); i++) {
                str_img[i] = ja_img.get(i).toString();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (str_img.length > 0) {
            downloadimg();
            Detection(0);
        }

        iv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DeleteDialogClass(CatalogeEditDataActivity.this, new DeleteDialogClass.OnDialogClickListener() {
                    @Override
                    public void onDialogImageRunClick(int pos, boolean isdel) {
                        if (pos == 0) {

                        } else if (pos == 1) {
                            //str_actionid = str_actionid.replaceAll(",$", "");
                            if (isdel) {
                                DeleteData(obj.getPricelistId());
                            } else {

                                Gson gson = new GsonBuilder().create();
                                String json = gson.toJson(obj);// obj is your object


                                try {

                                    JSONObject jo = new JSONObject();
                                    JSONArray ja_new = new JSONArray();
                                    jo.put("newdata", ja_new);
                                    JSONArray ja_old = new JSONArray();


                                    JSONObject jsonObj = new JSONObject(json);
                                    String stock = jsonObj.getString("out_of_stock");
                                    if (stock.equalsIgnoreCase("yes")) {
                                        jsonObj.put("out_of_stock", "No");
                                    } else {
                                        jsonObj.put("out_of_stock", "Yes");
                                    }

                                    //jsonObj.put("field_json_data",new JSONObject());
                                    jsonObj.put("flag", "edit");
                                    ja_old.put(jsonObj);
                                    jo.put("olddata", ja_old);

                                    CatRequest catalougeContent = new CatRequest();

                                    catalougeContent = new Gson().fromJson(jo.toString(), CatRequest.class);

                                    putData(catalougeContent);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    findViewById(R.id.rl_pb_cat).setVisibility(View.GONE);
                                }

                            }

                        }
                    }

                }, out_of_stock).show();


              /*  new CustomDialogClass(CatalogeEditDataActivity.this, new CustomDialogClass.OnDialogClickListener() {
                    @Override
                    public void onDialogImageRunClick(int positon) {
                        if (positon == 0) {

                        } else if (positon == 1) {
                            //str_actionid = str_actionid.replaceAll(",$", "");
                            DeleteData(obj.getPricelistId());
                        }
                    }
                }, "DELETE ?").show();*/

            }
        });
        tv_cur1.setText(currency);
        tv_cur2.setText(currency);
        tv_cur3.setText(currency);
        tv_cur4.setText(currency);
        tv_cur5.setText(currency);
        tv_cur6.setText(currency);
        tv_cur7.setText(currency);
        tv_cur_11.setText(currency);
        tv_cur_12.setText(currency);
        txtDedPriceCs.setText("Actual Price (" + currency + ")");
        txtDedOrgPriceCs.setText("Original Price (" + currency + ")");
        txtAddOrgPriceCs.setText("Original Price (" + currency + ")");
        txtAddPriceCs.setText("Actual Price (" + currency + ")");

        if (isadd) {
            checkWholeSale(userProfileData.field_json_data);
        } else {
            setWholeSale();
        }

        checkSurvey();

        checkjewel();

        service_type_of = JsonObjParse.getValueEmpty(userProfileData.field_json_data, "service_type_of");
        if (service_type_of.equalsIgnoreCase(getString(R.string.dental)) && isadd) {
            llSelectTemplete.setVisibility(View.VISIBLE);
        } else {
            llSelectTemplete.setVisibility(View.GONE);
        }

        mAdapter = new TemplateSelectAdapter(myDoctorData.getTemplateList(), this);
        rvSelectTemplate.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        rvSelectTemplate.setAdapter(mAdapter);

        setCommodityService();
        checkTemplate();
    }

    @Override
    public void onItemClick(int tempPos) {
        templatePos = tempPos;
        Log.d("templatePos", templatePos + "");
    }

    public void edit() {
        et_mrp.setEnabled(false);
        et_Amrp.setEnabled(false);
        spn_template.setEnabled(false);
        et_dis_per.setEnabled(false);
        et_dis_val.setEnabled(false);
        et_cgst.setEnabled(false);
        et_sgst.setEnabled(false);
        et_offer.setEnabled(false);
        et_xref.setEnabled(false);
        iv_add.setEnabled(false);
        tv_title.setEnabled(false);
        etMinQIFWholesale.setEnabled(false);
        etMinPriceRangeForQ.setEnabled(false);
        etMaxPriceRangeForQ.setEnabled(false);
        etMinDeldays.setEnabled(false);
        etMaxDeldays.setEnabled(false);
        etStockPos.setEnabled(false);

        etSqFt.setEnabled(false);
        etInfo1.setEnabled(false);
        etInfoVal1.setEnabled(false);
        etInfo2.setEnabled(false);
        etInfoVal2.setEnabled(false);
        etInfo3.setEnabled(false);
        etInfoVal3.setEnabled(false);
        etInfo4.setEnabled(false);
        etInfoVal4.setEnabled(false);
        etInfo5.setEnabled(false);
        etInfoVal5.setEnabled(false);
        etUds.setEnabled(false);
        etCarpetArea.setEnabled(false);
        etBr.setEnabled(false);
        etBath.setEnabled(false);
        etKitchen.setEnabled(false);
        etCommnetForStatus.setEnabled(false);
        etEnterSurveyQ.setEnabled(false);
        etOptionsforSurvey.setEnabled(false);

        issave = true;
    }

    public void save() {
        et_mrp.setEnabled(true);
        et_Amrp.setEnabled(true);
        if (!ShareModel.getI().dbUnderMaster.isEmpty()) {
            spn_template.setEnabled(false);
        } else {
            spn_template.setEnabled(true);
        }
        et_dis_per.setEnabled(true);
        et_dis_val.setEnabled(true);
        et_cgst.setEnabled(true);
        et_sgst.setEnabled(true);
        et_offer.setEnabled(true);
        if (property.isEmpty()) et_xref.setEnabled(true);
        iv_add.setEnabled(true);
        tv_title.setEnabled(true);
        etMinQIFWholesale.setEnabled(true);
        etMinPriceRangeForQ.setEnabled(true);
        etMaxPriceRangeForQ.setEnabled(true);
        etMinDeldays.setEnabled(true);
        etMaxDeldays.setEnabled(true);
        etStockPos.setEnabled(true);

        etSqFt.setEnabled(true);
        etInfo1.setEnabled(true);
        etInfoVal1.setEnabled(true);
        etInfo2.setEnabled(true);
        etInfoVal2.setEnabled(true);
        etInfo3.setEnabled(true);
        etInfoVal3.setEnabled(true);
        etInfo4.setEnabled(true);
        etInfoVal4.setEnabled(true);
        etInfo5.setEnabled(true);
        etInfoVal5.setEnabled(true);
        etUds.setEnabled(true);
        etCarpetArea.setEnabled(true);
        etBr.setEnabled(true);
        etBath.setEnabled(true);
        etKitchen.setEnabled(true);
        etCommnetForStatus.setEnabled(true);

        etEnterSurveyQ.setEnabled(true);
        etOptionsforSurvey.setEnabled(true);


        issave = false;

        if (llJewelInfo.getVisibility() == View.VISIBLE) {
            et_mrp.setEnabled(false);
            et_mrp.setBackground(null);
            //et_mrp.setText(gold_today_price);
        }

        //checkjewel();

    }

    //ShowGalleryCamera showGalleryCamera = new ShowGalleryCamera(this);
    String path = "";
    LoadImg loadImg = new LoadImg();

    private void showPictureDialog() {
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(CatalogeEditDataActivity.this);
        String[] pictureDialogItems = {"Photo Gallery", "Camera"};
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:

                              /*  Intent i = new Intent(CatalogeEditDataActivity.this, FileActivity.class);
                                i.putExtra("isdoc", false);
                                startActivityForResult(i, 8);*/

                                Intent mIntent = new Intent(CatalogeEditDataActivity.this, PickImageActivity.class);
                                mIntent.putExtra(PickImageActivity.KEY_LIMIT_MAX_IMAGE, 30);
                                mIntent.putExtra(PickImageActivity.KEY_LIMIT_MIN_IMAGE, 1);
                                startActivityForResult(mIntent, 98);

                                break;
                            case 1:
                                cameraIntent();
                                break;
                        }
                    }
                });
        pictureDialog.show();
    }

    private void cameraIntent() {

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        this.screen_width = size.x;
        this.screen_height = size.y;

        File root = new File(Environment.getExternalStorageDirectory()
                + File.separator + getString(R.string.app_name) + File.separator + "Capture" + File.separator);
        root.mkdirs();
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        f3f = new File(root, "IMG" + timeStamp + ".jpg");

        try {
            imageUri = FileProvider.getUriForFile(
                    CatalogeEditDataActivity.this, getApplicationContext()
                            .getPackageName() + ".provider", this.f3f);
        } catch (Exception e) {
            Toast.makeText(this, "Please check SD card! Image shot is impossible!", Toast.LENGTH_SHORT).show();
        }

        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        intent.putExtra("output", imageUri);
        try {
            startActivityForResult(intent, CAMERA);
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
        }

    }

    ExifInterface exif;
    File f3f;
    int screen_height;
    int screen_width;
    private Uri imageUri;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to ak aris
        if (resultCode == -1 && requestCode == CAMERA) {

            Bitmap bitmap;
            int orientation;
            String albumnm = "";
            try {
                bitmap = BitmapFactory.decodeFile(this.f3f.getPath());
                this.exif = new ExifInterface(this.f3f.getPath());
                if (bitmap != null) {
                    // f3f.getAbsolutePath(

                    String picturePath = f3f.getPath();
                    ArrayList<ModelFile> al_temp_selet = new ArrayList<>();
                    al_temp_selet.add(new ModelFile(picturePath, true));
                    al_temp_selet.addAll(al_selet);
                    selectFileAdapter.setItem(al_temp_selet);
                    al_selet = new ArrayList<>();
                    al_selet.addAll(al_temp_selet);
                    Detection(0);

                } else {
                    Toast.makeText(getApplicationContext(), "Picture not taken !", Toast.LENGTH_SHORT).show();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }


        } else if (resultCode == -1 && requestCode == 8) {
            String picturePath = data.getExtras().getString("al_selet");
            ArrayList<ModelFile> al_temp_selet = new Gson().fromJson(picturePath, new TypeToken<ArrayList<ModelFile>>() {
            }.getType());
            al_temp_selet.addAll(al_selet);
            selectFileAdapter.setItem(al_temp_selet);
            al_selet = new ArrayList<>();
            al_selet.addAll(al_temp_selet);
            Detection(0);
            //if(al_selet.size()==0)iv_upload.setVisibility(View.GONE);
            //else iv_upload.setVisibility(View.VISIBLE);

        } else if (requestCode == 98 && resultCode == -1) {
            ArrayList<ModelFile> al_temp_selet = new ArrayList<>();
            ArrayList<String> pathList = data.getExtras().getStringArrayList(PickImageActivity.KEY_DATA_RESULT);
            if (pathList != null && !pathList.isEmpty()) {
                for (int i = 0; i < pathList.size(); i++) {
                    al_temp_selet.add(new ModelFile(pathList.get(i), true));
                }
            }

            al_temp_selet.addAll(al_selet);
            selectFileAdapter.setItem(al_temp_selet);
            al_selet = new ArrayList<>();
            //al_selet = al_temp_selet;
            al_selet.addAll(al_temp_selet);
            Detection(0);


        } else if (requestCode == 102 && resultCode == RESULT_OK) {
            // Compressing

        } else if (resultCode == -1 && requestCode == 11) {
            /*String picturePath = data.getExtras().getString("al_selet");
            al_selet = new Gson().fromJson(picturePath, new TypeToken<ArrayList<ModelFile>>() {
            }.getType());
            selectFileAdapter.setItem(al_selet);*/


        } else if (resultCode == -1 && requestCode == 25) {
            /*String picturePath = data.getExtras().getString("al_selet");
            al_selet = new Gson().fromJson(picturePath, new TypeToken<ArrayList<ModelFile>>() {
            }.getType());
            selectFileAdapter.setItem(al_selet);*/


            SaveEdit(isadd);

        }


        super.onActivityResult(requestCode, resultCode, data);

    }

    ArrayList<ModelFile> al_selet = new ArrayList<>();

    AmazonS3 s3;
    TransferUtility transferUtility;

    public void inits3() {
        s3 = new AmazonS3Client(new BasicAWSCredentials(getString(R.string.accesskey), getString(R.string.secretkey)));
        transferUtility = new TransferUtility(s3, getApplicationContext());
    }

    String namegsxsax = "";

    public void uploadimg() {
        rl_upload.setVisibility(View.VISIBLE);
        int cc = c_img + 1;
        int total = al_selet.size();
        tv_progress.setText("Uploading " + cc + "/" + total);


        File file = new File(al_selet.get(c_img).getImgpath());
        namegsxsax = "IMG_" + System.currentTimeMillis() + ".jpg";

        if (al_selet.get(c_img).isIsoffline()) {
            TransferObserver transferObserver = transferUtility.upload("cab-videofiles", namegsxsax, file);
            transferObserverListener(transferObserver);
        } else {  // if already upload

            String imgnm = al_selet.get(c_img).getImgpath();
            imgnm = imgnm.substring(imgnm.lastIndexOf("/") + 1);

            str_img[c_img] = getString(R.string.baseaws) + imgnm;

            c_img++;
            if (c_img < al_selet.size()) {
                uploadimg();
            } else {
                rl_upload.setVisibility(View.GONE);
                c_img = 0;
                putdata();
            }
        }


    }

    public void transferObserverListener(final TransferObserver transferObserver) {

        transferObserver.setTransferListener(new TransferListener() {
            @Override
            public void onStateChanged(int id, TransferState state) {
                if (state.name().equals("COMPLETED")) {
                    //str_img.put(getString(R.string.baseaws)+namegsxsax);
                    str_img[c_img] = getString(R.string.baseaws) + namegsxsax;

                    c_img++;
                    if (c_img < al_selet.size()) {
                        uploadimg();
                    } else {
                        rl_upload.setVisibility(View.GONE);
                        c_img = 0;
                        putdata();

                      /*  Gson gson = new GsonBuilder().create();
                        String json = gson.toJson(obj);// obj is your object


                        try {

                            JSONObject jo = new JSONObject();
                            JSONArray ja_new = new JSONArray();
                            jo.put("newdata", ja_new);
                            JSONArray ja_old = new JSONArray();

                            JsonArray ja_img = new JsonArray();
                            for (int i=0;i<str_img.length;i++)
                            {
                                ja_img.add(str_img[i]);
                            }

                            JSONObject jsonObj = new JSONObject(json);
                            jsonObj.put("image_json_data", ja_img);


                            //jsonObj.put("field_json_data",new JSONObject());

                            ja_old.put(jsonObj);
                            jo.put("olddata", ja_old);


                            CatRequest catalougeContent = new CatRequest();

                            catalougeContent = new Gson().fromJson(jo.toString(), CatRequest.class);

                            putData(catalougeContent);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            findViewById(R.id.rl_pb_cat).setVisibility(View.GONE);
                        }*/
                    }
                } else if (state.name().equals("FAILED")) {
                    rl_upload.setVisibility(View.GONE);
                    Toast.makeText(CatalogeEditDataActivity.this, "Failed Uploading !", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                try {
                    int percentage = (int) (bytesCurrent / bytesTotal * 100);


                    //mProgressDialog.setProgress(percentage);


                    //Log.e("percentage ", " : " + percentage);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(int id, Exception ex) {

                Log.e("error", "error");
            }

        });
    }

    int c_img = 0;
    String[] str_img = new String[]{};

    SessionManager sessionManager;

    public void putData(CatRequest jo_cat) {

        findViewById(R.id.rl_pb_cat).setVisibility(View.VISIBLE);

        ApiInterface apiService =
                ApiClient.getClient(CatalogeEditDataActivity.this).create(ApiInterface.class);

        Call call = apiService.PutCatalogue(sessionManager.getsession(), jo_cat);

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                findViewById(R.id.rl_pb_cat).setVisibility(View.GONE);

                if (response.isSuccessful()) {
                    try {
                        //ll_edit.setVisibility(View.VISIBLE);
                        //ll_save.setVisibility(View.GONE);
                        //edit();

                        SaveEdit(isadd);


                    } catch (Exception e) {


                    }

                } else {


                    try {

                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Log.e("response_message", jObjError.toString());

                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        String errormessage = jObjErrorresponse.getString("errormsg");
                        Toast.makeText(CatalogeEditDataActivity.this, errormessage, Toast.LENGTH_LONG).show();


                    } catch (Exception e) {
                        Toast.makeText(CatalogeEditDataActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }

            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // Log error here since request failed
                findViewById(R.id.rl_pb_cat).setVisibility(View.GONE);

            }
        });
    }

    public void putdata() {
        findViewById(R.id.rl_pb_cat).setVisibility(View.VISIBLE);

        Gson gson = new GsonBuilder().create();
        String json = gson.toJson(obj);// obj is your object

        try {

            String price = et_mrp.getText().toString().trim();
            String Aprice = et_Amrp.getText().toString().trim();
            String dis_mod = et_dis_per.getText().toString().trim();
            String dis_val = et_dis_val.getText().toString().trim();
            String cgst = et_cgst.getText().toString().trim();
            String sgst = et_sgst.getText().toString().trim();
            String offer = et_offer.getText().toString().trim();
            String xref = et_xref.getText().toString().trim();
            String name = tv_title.getText().toString().trim();

            if (price.isEmpty()) price = "0";
            if (dis_mod.isEmpty()) dis_mod = "0";
            if (dis_val.isEmpty()) dis_val = "0";
            if (cgst.isEmpty()) cgst = "0";
            if (sgst.isEmpty()) sgst = "0";


            JSONObject jo = new JSONObject();
            JSONArray ja_new = new JSONArray();

            JSONArray ja_old = new JSONArray();


            JSONObject jsonObj = new JSONObject(json);


            jsonObj.put("name", name);
            jsonObj.put("price", Double.parseDouble(price));
            jsonObj.put("discount_percentage", Double.parseDouble(dis_mod));
            jsonObj.put("discounted_price", Double.parseDouble(dis_val));
            jsonObj.put("offer", offer);

            if (llServeyInfo.getVisibility() == View.VISIBLE) {

                String qPos = "" + etEnterSurveyQPosition.getText().toString().trim();
                if (qPos.isEmpty()) {
                    int t = ShareModel.getI().total_catalog;
                    t++;
                    jsonObj.put("cross_reference", t + "");
                } else {
                    jsonObj.put("cross_reference", qPos + "");
                }
            } else {
                jsonObj.put("cross_reference", xref);
            }

            if (isadd) {
                //jsonObj.put("currency_code", obj.getCurrencyCode());
                jsonObj.put("currency_code_id", currency_code_id);
                //jsonObj.put("symbol", obj.getSymbol());
                jsonObj.put("symbol_native", currency);
                jsonObj.put("field_json_data", ""); // Or JsonObject
            }


            JSONObject joFielddata = new JSONObject();
            JSONObject jofield_json_data_f1 = new JSONObject();
            try {
                JSONArray jafield_json_data = new JSONArray();


                String f2 = tvTaxField1.getText().toString().trim();
                if (!f2.isEmpty()) {
                    String val = et_cgst.getText().toString().trim();
                    if (val.isEmpty()) val = "0";
                    jofield_json_data_f1.put(tvTaxField1.getText().toString().trim(), val);

                }

                String f1 = tvTaxField2.getText().toString().trim();
                if (!f1.isEmpty()) {
                    String val = et_sgst.getText().toString().trim();
                    if (val.isEmpty()) val = "0";
                    jofield_json_data_f1.put(tvTaxField2.getText().toString().trim(), val);

                }

                jafield_json_data.put(jofield_json_data_f1);


                joFielddata.put("Tax", jafield_json_data);


            } catch (JSONException e) {
                e.printStackTrace();
            }
            //JsonObject jo_field = new JsonObject();
            //jo_field.addProperty("CGST", Double.parseDouble(cgst));
            //jo_field.addProperty("SGST", Double.parseDouble(sgst));
            //jsonObj.put("field_json_data", jo_field);
            //jsonObj.put("tax_json_data", jo_field);

            //jsonObj.put("field_json_data", jofield_json_data_f1.toString()); // Or JsonObject
            jsonObj.put("tax_json_data", jofield_json_data_f1.toString());
            jsonObj.put("available_stock", etStockPos.getText().toString().trim());


            //jsonObj.put("field_json_data", joFielddata.toString());
            //jsonObj.put("tax_json_data", joFielddata.toString());

            JsonArray ja_img = new JsonArray();
            for (int i = 0; i < str_img.length; i++) {
                ja_img.add(str_img[i]);
            }

            jsonObj.put("image_json_data", ja_img);

            if (llWholesale.getVisibility() == View.VISIBLE) {
                JsonObject joWholesale = new JsonObject();
                if (etMinQIFWholesale.getText().toString().trim().isEmpty()) {
                    joWholesale.addProperty("minQ", "1");

                } else {
                    joWholesale.addProperty("minQ", etMinQIFWholesale.getText().toString().trim());
                }
                joWholesale.addProperty("minPrice", etMinPriceRangeForQ.getText().toString().trim());
                joWholesale.addProperty("maxPrice", etMaxPriceRangeForQ.getText().toString().trim());
                joWholesale.addProperty("minDay", etMinDeldays.getText().toString().trim());
                joWholesale.addProperty("maxDay", etMaxDeldays.getText().toString().trim());

                JsonObject jsonObject = new JsonObject();
                jsonObject.add("wholesale", joWholesale);

                jsonObj.put("internal_json_data", jsonObject);
            } else if (llServeyInfo.getVisibility() == View.VISIBLE) {
                JsonObject joWholesale = new JsonObject();
                joWholesale.addProperty("survey", true);
                SurveyOptions surveyOptions = new SurveyOptions();
                surveyOptions.alOptions = alOptions;
                surveyOptions.que = etEnterSurveyQ.getText().toString().trim();
                String optionList = new Gson().toJson(surveyOptions);
                joWholesale.addProperty("SurveyOptions", optionList);
                String qPos = "" + etEnterSurveyQPosition.getText().toString().trim();
                if (qPos.isEmpty()) {
                    int t = ShareModel.getI().total_catalog;
                    t++;
                    joWholesale.addProperty("SurveyQPos", t + "");
                } else {
                    joWholesale.addProperty("SurveyQPos", qPos + "");
                }

                if (cbCommentBox.isChecked()) joWholesale.addProperty("CommentBox", "1");
                else joWholesale.addProperty("CommentBox", "");

                JsonObject jsonObject = new JsonObject();
                jsonObject.add("survey", joWholesale);

                jsonObj.put("internal_json_data", jsonObject);
            } else if (llJewelInfo.getVisibility() == View.VISIBLE) {
                JsonObject joWholesale = new JsonObject();
                joWholesale.addProperty("jewel", true);
                joWholesale.addProperty("metal", metal);
                joWholesale.addProperty("metalPosId", metalPosId);
                joWholesale.addProperty("weight", etJewelWeight.getText().toString().trim());
                joWholesale.addProperty("ded_val", et_ded_val.getText().toString().trim());
                joWholesale.addProperty("ded_perc", et_ded_per.getText().toString().trim());
                joWholesale.addProperty("additional_perc", et_additional_per.getText().toString().trim());
                joWholesale.addProperty("additional_val", et_additional_val.getText().toString().trim());

                // Deductions

                JsonArray jaExtraInfo = new JsonArray();

                JsonObject joAdditionalInfo = new JsonObject();
                JsonObject joAdditionalValues = new JsonObject();
                joAdditionalValues.addProperty("act_perc", etActDedPerc1.getText().toString().trim());
                joAdditionalValues.addProperty("org_perc", etOrgDedPerc1.getText().toString().trim());
                joAdditionalValues.addProperty("act_price", etActDedPrice1.getText().toString().trim());
                joAdditionalValues.addProperty("org_price", etOrgDedPrice1.getText().toString().trim());
                joAdditionalInfo.add(etDedInfo1.getText().toString().trim(), joAdditionalValues);
                jaExtraInfo.add(joAdditionalInfo);

                joAdditionalInfo = new JsonObject();
                joAdditionalValues = new JsonObject();
                joAdditionalValues.addProperty("act_perc", etActDedPerc2.getText().toString().trim());
                joAdditionalValues.addProperty("org_perc", etOrgDedPerc2.getText().toString().trim());
                joAdditionalValues.addProperty("act_price", etActDedPrice2.getText().toString().trim());
                joAdditionalValues.addProperty("org_price", etOrgDedPrice2.getText().toString().trim());
                joAdditionalInfo.add(etDedInfo2.getText().toString().trim(), joAdditionalValues);
                jaExtraInfo.add(joAdditionalInfo);

                joAdditionalInfo = new JsonObject();
                joAdditionalValues = new JsonObject();
                joAdditionalValues.addProperty("act_perc", etActDedPerc3.getText().toString().trim());
                joAdditionalValues.addProperty("org_perc", etOrgDedPerc3.getText().toString().trim());
                joAdditionalValues.addProperty("act_price", etActDedPrice3.getText().toString().trim());
                joAdditionalValues.addProperty("org_price", etOrgDedPrice3.getText().toString().trim());
                joAdditionalInfo.add(etDedInfo3.getText().toString().trim(), joAdditionalValues);
                jaExtraInfo.add(joAdditionalInfo);

                joAdditionalInfo = new JsonObject();
                joAdditionalValues = new JsonObject();
                joAdditionalValues.addProperty("act_perc", etActDedPerc4.getText().toString().trim());
                joAdditionalValues.addProperty("org_perc", etOrgDedPerc4.getText().toString().trim());
                joAdditionalValues.addProperty("act_price", etActDedPrice4.getText().toString().trim());
                joAdditionalValues.addProperty("org_price", etOrgDedPrice4.getText().toString().trim());
                joAdditionalInfo.add(etDedInfo4.getText().toString().trim(), joAdditionalValues);
                jaExtraInfo.add(joAdditionalInfo);

                joAdditionalInfo = new JsonObject();
                joAdditionalValues = new JsonObject();
                joAdditionalValues.addProperty("act_perc", etActDedPerc5.getText().toString().trim());
                joAdditionalValues.addProperty("org_perc", etOrgDedPerc5.getText().toString().trim());
                joAdditionalValues.addProperty("act_price", etActDedPrice5.getText().toString().trim());
                joAdditionalValues.addProperty("org_price", etOrgDedPrice5.getText().toString().trim());
                joAdditionalInfo.add(etDedInfo5.getText().toString().trim(), joAdditionalValues);
                jaExtraInfo.add(joAdditionalInfo);

                joWholesale.add("DeductionJewelPrice", jaExtraInfo);


                // Additional

                jaExtraInfo = new JsonArray();

                joAdditionalInfo = new JsonObject();
                joAdditionalValues = new JsonObject();
                joAdditionalValues.addProperty("act_perc", etActAddPerc1.getText().toString().trim());
                joAdditionalValues.addProperty("org_perc", etOrgAddPerc1.getText().toString().trim());
                joAdditionalValues.addProperty("act_price", etActAddPrice1.getText().toString().trim());
                joAdditionalValues.addProperty("org_price", etOrgAddPrice1.getText().toString().trim());
                joAdditionalInfo.add(etAddInfo1.getText().toString().trim(), joAdditionalValues);
                jaExtraInfo.add(joAdditionalInfo);

                joAdditionalInfo = new JsonObject();
                joAdditionalValues = new JsonObject();
                joAdditionalValues.addProperty("act_perc", etActAddPerc2.getText().toString().trim());
                joAdditionalValues.addProperty("org_perc", etOrgAddPerc2.getText().toString().trim());
                joAdditionalValues.addProperty("act_price", etActAddPrice2.getText().toString().trim());
                joAdditionalValues.addProperty("org_price", etOrgAddPrice2.getText().toString().trim());
                joAdditionalInfo.add(etAddInfo2.getText().toString().trim(), joAdditionalValues);
                jaExtraInfo.add(joAdditionalInfo);

                joAdditionalInfo = new JsonObject();
                joAdditionalValues = new JsonObject();
                joAdditionalValues.addProperty("act_perc", etActAddPerc3.getText().toString().trim());
                joAdditionalValues.addProperty("org_perc", etOrgAddPerc3.getText().toString().trim());
                joAdditionalValues.addProperty("act_price", etActAddPrice3.getText().toString().trim());
                joAdditionalValues.addProperty("org_price", etOrgAddPrice3.getText().toString().trim());
                joAdditionalInfo.add(etAddInfo3.getText().toString().trim(), joAdditionalValues);
                jaExtraInfo.add(joAdditionalInfo);

                joAdditionalInfo = new JsonObject();
                joAdditionalValues = new JsonObject();
                joAdditionalValues.addProperty("act_perc", etActAddPerc4.getText().toString().trim());
                joAdditionalValues.addProperty("org_perc", etOrgAddPerc4.getText().toString().trim());
                joAdditionalValues.addProperty("act_price", etActAddPrice4.getText().toString().trim());
                joAdditionalValues.addProperty("org_price", etOrgAddPrice4.getText().toString().trim());
                joAdditionalInfo.add(etAddInfo4.getText().toString().trim(), joAdditionalValues);
                jaExtraInfo.add(joAdditionalInfo);

                joAdditionalInfo = new JsonObject();
                joAdditionalValues = new JsonObject();
                joAdditionalValues.addProperty("act_perc", etActAddPerc5.getText().toString().trim());
                joAdditionalValues.addProperty("org_perc", etOrgAddPerc5.getText().toString().trim());
                joAdditionalValues.addProperty("act_price", etActAddPrice5.getText().toString().trim());
                joAdditionalValues.addProperty("org_price", etOrgAddPrice5.getText().toString().trim());
                joAdditionalInfo.add(etAddInfo5.getText().toString().trim(), joAdditionalValues);
                jaExtraInfo.add(joAdditionalInfo);

                joWholesale.add("AdditionalJewelPrice", jaExtraInfo);


                JsonObject jsonObject = new JsonObject();
                jsonObject.add("jewel", joWholesale);

                jsonObj.put("internal_json_data", jsonObject);
            } else if (llPropertyFinancialBlock.getVisibility() == View.VISIBLE) {

                JsonObject joProperty = new JsonObject();

                joProperty.addProperty("proprty_sqft", etSqFt.getText().toString().trim());
                joProperty.addProperty("uds_sqft", etUds.getText().toString().trim()); // Undivided Share
                joProperty.addProperty("carpet_arae_sqft", etCarpetArea.getText().toString().trim()); // Undivided Share
                joProperty.addProperty("bed", etBr.getText().toString().trim()); // Undivided Share
                joProperty.addProperty("bath", etBath.getText().toString().trim()); // Undivided Share
                joProperty.addProperty("kitchen", etKitchen.getText().toString().trim()); // Undivided Share
                joProperty.addProperty("facing", facing); // Undivided Share
                joProperty.addProperty("status_property", status_property);
                joProperty.addProperty("status_commnet", etCommnetForStatus.getText().toString().trim());


                /*JsonObject joAdditionalInfo = new JsonObject();
                joAdditionalInfo.addProperty(etInfo1.getText().toString().trim(), etInfoVal1.getText().toString().trim());

                joAdditionalInfo.addProperty(etInfo2.getText().toString().trim(), etInfoVal2.getText().toString().trim());
                joAdditionalInfo.addProperty(etInfo3.getText().toString().trim(), etInfoVal3.getText().toString().trim());
                joAdditionalInfo.addProperty(etInfo4.getText().toString().trim(), etInfoVal4.getText().toString().trim());
                joAdditionalInfo.addProperty(etInfo5.getText().toString().trim(), etInfoVal5.getText().toString().trim());
                joProperty.add("ExtraInfoPrice", joAdditionalInfo);
*/

                JsonArray jaExtraInfo = new JsonArray();

                JsonObject joAdditionalInfo = new JsonObject();
                joAdditionalInfo.addProperty(etInfo1.getText().toString().trim(), etInfoVal1.getText().toString().trim());
                jaExtraInfo.add(joAdditionalInfo);

                joAdditionalInfo = new JsonObject();
                joAdditionalInfo.addProperty(etInfo2.getText().toString().trim(), etInfoVal2.getText().toString().trim());
                jaExtraInfo.add(joAdditionalInfo);

                joAdditionalInfo = new JsonObject();
                joAdditionalInfo.addProperty(etInfo3.getText().toString().trim(), etInfoVal3.getText().toString().trim());
                jaExtraInfo.add(joAdditionalInfo);

                joAdditionalInfo = new JsonObject();
                joAdditionalInfo.addProperty(etInfo4.getText().toString().trim(), etInfoVal4.getText().toString().trim());
                jaExtraInfo.add(joAdditionalInfo);

                joAdditionalInfo = new JsonObject();
                joAdditionalInfo.addProperty(etInfo5.getText().toString().trim(), etInfoVal5.getText().toString().trim());
                jaExtraInfo.add(joAdditionalInfo);
                joProperty.add("ExtraInfoPrice", jaExtraInfo);


                JsonObject jsonObject = new JsonObject();
                jsonObject.add("property", joProperty);

                jsonObj.put("internal_json_data", jsonObject);

            } else if (llSelectTemplete.getVisibility() == View.VISIBLE) {

                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("doctor_template_type", myDoctorData.getList().get(spn_template.getSelectedItemPosition()).getTemplateName());
                Log.d("spinnerPos", spn_template.getSelectedItemPosition() + "");
                jsonObject.addProperty("additionalPrice_rpd", Double.parseDouble(Aprice));

                jsonObj.put("internal_json_data", jsonObject);


            }


            obj = new Gson().fromJson(jsonObj.toString(), CatalougeContent.class);

            //jsonObj.put("field_json_data",new JSONObject());

            if (!ShareModel.getI().dbUnderMaster.isEmpty()) {
                jsonObj.put("flag", "add");
                ja_new.put(jsonObj);
                jo.put("newdata", ja_new);
                jo.put("olddata", ja_old);
            } else if (isadd || isCopy) {
                jsonObj.put("flag", "add");
                ja_new.put(jsonObj);
                jo.put("newdata", ja_new);
                jo.put("olddata", ja_old);
            } else {
                jsonObj.put("flag", "edit");
                ja_old.put(jsonObj);
                jo.put("olddata", ja_old);
                jo.put("newdata", ja_new);
            }

            CatRequest catalougeContent = new CatRequest();
            catalougeContent = new Gson().fromJson(jo.toString(), CatRequest.class);

            if (ShareModel.getI().alCategory != null && !ShareModel.getI().alCategory.equalsIgnoreCase("[]")) {
                findViewById(R.id.rl_pb_cat).setVisibility(View.GONE);
                ShareModel.getI().catRequest = catalougeContent;
                startActivityForResult(new Intent(CatalogeEditDataActivity.this, EditCatlogueCategoryActivity.class), 25);
            } else {
                putData(catalougeContent);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            findViewById(R.id.rl_pb_cat).setVisibility(View.GONE);
        }


    }

    public String getdiscountmrp(Double mrpprice, Double discount_percentage, Double discounted_price) {
        Double discountprice = mrpprice;
        tvTotalDiscPerc.setText("");

        if (discount_percentage > 0) {
            if (mrpprice > 0) {
                Double totalDisc = (mrpprice * (discount_percentage / 100));
                tvTotalDiscPerc.setText("- " + currency + Inad.getCurrencyDecimal(totalDisc, this));
                discountprice = mrpprice - totalDisc;
            }
        } else if (discounted_price > 0) {
            Double totalDisc = mrpprice - discounted_price;
            tvTotalDiscPerc.setText("- " + currency + Inad.getCurrencyDecimal(totalDisc, this));

            discountprice = discounted_price;
        }

        return String.valueOf(discountprice);
    }

    public String getValues(Double mrpprice, Double discount_percentage, Double discounted_price) {
        Double discountprice = 0.0;

        if (discount_percentage > 0) {
            if (mrpprice > 0) {
                Double totalDisc = (mrpprice * (discount_percentage / 100));
                discountprice = totalDisc;
            }
        } else if (discounted_price > 0) {
            discountprice = discounted_price;
        }

        return String.valueOf(discountprice);
    }

    public Double calcValuesOfAddDed(Double mrp, String act_perc, String org_perc, String act_price, String org_price) {

        Double dedPriceForJewel = 0.0;

        if (!act_perc.isEmpty()) {
            Double ded1Val = Double.valueOf(act_perc);
            dedPriceForJewel = dedPriceForJewel + Double.valueOf(getValues(mrp, ded1Val));
        } else if (!org_perc.isEmpty()) {
            Double ded1Val = Double.valueOf(org_perc);
            dedPriceForJewel = dedPriceForJewel + Double.valueOf(getValues(mrp, ded1Val));
        } else if (!act_price.isEmpty()) {
            Double ded1Val = Double.valueOf(act_price);
            dedPriceForJewel = dedPriceForJewel + ded1Val;
        } else if (!org_price.isEmpty()) {
            Double ded1Val = Double.valueOf(org_price);
            dedPriceForJewel = dedPriceForJewel + ded1Val;
        }

        return dedPriceForJewel;
    }

    public String getValues(Double mrpprice, Double discount_percentage) {
        Double discountprice = 0.0;

        if (discount_percentage > 0) {
            if (mrpprice > 0) {
                Double totalDisc = (mrpprice * (discount_percentage / 100));
                discountprice = totalDisc;
            }
        }

        return String.valueOf(discountprice);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

        calc_tax_value();

    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    boolean isNegative = false;

    public void calc_tax_value() {
        try {
            String price = et_mrp.getText().toString().trim();
            String Additionalprice = et_Amrp.getText().toString().trim();

            Double mrp = 0.0;
            Double addMrp = 0.0;
            if (!price.isEmpty()) mrp = Double.valueOf(price);

            if (!Additionalprice.isEmpty()) addMrp = Double.valueOf(Additionalprice);

            Double extraInfoPrice = 0.0, dedPriceForJewel = 0.0, additionalPriceForJewel = 0.0;
            Double ded_mrp = 0.0, additional_mrp = 0.0;
            if (llPropertyFinancialBlock.getVisibility() == View.VISIBLE) {

                String sqFt = etSqFt.getText().toString().trim();
                if (!sqFt.isEmpty()) {
                    int sqFtVal = Integer.parseInt(sqFt);
                    mrp = mrp * sqFtVal;
                }

                String info1 = etInfoVal1.getText().toString().trim();
                if (!info1.isEmpty()) {
                    int info1Val = Integer.parseInt(info1);
                    extraInfoPrice = extraInfoPrice + info1Val;
                }

                String info2 = etInfoVal2.getText().toString().trim();
                if (!info2.isEmpty()) {
                    int info2Val = Integer.parseInt(info2);
                    extraInfoPrice = extraInfoPrice + info2Val;
                }

                String info3 = etInfoVal3.getText().toString().trim();
                if (!info3.isEmpty()) {
                    int info3Val = Integer.parseInt(info3);
                    extraInfoPrice = extraInfoPrice + info3Val;
                }

                String info4 = etInfoVal4.getText().toString().trim();
                if (!info4.isEmpty()) {
                    int info4Val = Integer.parseInt(info4);
                    extraInfoPrice = extraInfoPrice + info4Val;
                }

                String info5 = etInfoVal5.getText().toString().trim();
                if (!info5.isEmpty()) {
                    int info5Val = Integer.parseInt(info5);
                    extraInfoPrice = extraInfoPrice + info5Val;
                }
            } else if (llJewelInfo.getVisibility() == View.VISIBLE) {

                String weight = etJewelWeight.getText().toString().trim();
                if (!weight.isEmpty()) {
                    float wt = Float.parseFloat(weight);
                    //mrp = (mrp * wt) / 10; // 10 gram
                    mrp = (mrp * wt) / 1; // 1 unit
                }

                // Calc Deduction

                // if actual is empty then calc original
                String ded1 = etActDedPrice1.getText().toString().trim();
                String oDed1 = etOrgDedPrice1.getText().toString().trim();
                String dedPerc1 = etActDedPerc1.getText().toString().trim();
                String oDedPerc1 = etOrgDedPerc1.getText().toString().trim();

                dedPriceForJewel = dedPriceForJewel + calcValuesOfAddDed(mrp, dedPerc1, oDedPerc1, ded1, oDed1);


                String ded2 = etActDedPrice2.getText().toString().trim();
                String oDed2 = etOrgDedPrice2.getText().toString().trim();
                String dedPerc2 = etActDedPerc2.getText().toString().trim();
                String oDedPerc2 = etOrgDedPerc2.getText().toString().trim();

                dedPriceForJewel = dedPriceForJewel + calcValuesOfAddDed(mrp, dedPerc2, oDedPerc2, ded2, oDed2);


                String ded3 = etActDedPrice3.getText().toString().trim();
                String oDed3 = etOrgDedPrice3.getText().toString().trim();
                String dedPerc3 = etActDedPerc3.getText().toString().trim();
                String oDedPerc3 = etOrgDedPerc3.getText().toString().trim();

                dedPriceForJewel = dedPriceForJewel + calcValuesOfAddDed(mrp, dedPerc3, oDedPerc3, ded3, oDed3);

                String ded4 = etActDedPrice4.getText().toString().trim();
                String oDed4 = etOrgDedPrice4.getText().toString().trim();
                String dedPerc4 = etActDedPerc4.getText().toString().trim();
                String oDedPerc4 = etOrgDedPerc4.getText().toString().trim();

                dedPriceForJewel = dedPriceForJewel + calcValuesOfAddDed(mrp, dedPerc4, oDedPerc4, ded4, oDed4);


                String ded5 = etActDedPrice5.getText().toString().trim();
                String oDed5 = etOrgDedPrice5.getText().toString().trim();
                String dedPerc5 = etActDedPerc5.getText().toString().trim();
                String oDedPerc5 = etOrgDedPerc5.getText().toString().trim();

                dedPriceForJewel = dedPriceForJewel + calcValuesOfAddDed(mrp, dedPerc5, oDedPerc5, ded5, oDed5);


                /////////////////////////////////////////////////////////


                // Calc Additional

                // if actual is empty then calc original
                String add1 = etActAddPrice1.getText().toString().trim();
                String oAdd1 = etOrgAddPrice1.getText().toString().trim();
                String addPerc1 = etActAddPerc1.getText().toString().trim();
                String oAddPerc1 = etOrgAddPerc1.getText().toString().trim();

                additionalPriceForJewel = additionalPriceForJewel + calcValuesOfAddDed(mrp, addPerc1, oAddPerc1, add1, oAdd1);


                String add2 = etActAddPrice2.getText().toString().trim();
                String oAdd2 = etOrgAddPrice2.getText().toString().trim();
                String addPerc2 = etActAddPerc2.getText().toString().trim();
                String oAddPerc2 = etOrgAddPerc2.getText().toString().trim();

                additionalPriceForJewel = additionalPriceForJewel + calcValuesOfAddDed(mrp, addPerc2, oAddPerc2, add2, oAdd2);


                String add3 = etActAddPrice3.getText().toString().trim();
                String oAdd3 = etOrgAddPrice3.getText().toString().trim();
                String addPerc3 = etActAddPerc3.getText().toString().trim();
                String oAddPerc3 = etOrgAddPerc3.getText().toString().trim();

                additionalPriceForJewel = additionalPriceForJewel + calcValuesOfAddDed(mrp, addPerc3, oAddPerc3, add3, oAdd3);


                String add4 = etActAddPrice4.getText().toString().trim();
                String oAdd4 = etOrgAddPrice4.getText().toString().trim();
                String addPerc4 = etActAddPerc4.getText().toString().trim();
                String oAddPerc4 = etOrgAddPerc4.getText().toString().trim();

                additionalPriceForJewel = additionalPriceForJewel + calcValuesOfAddDed(mrp, addPerc4, oAddPerc4, add4, oAdd4);


                String add5 = etActAddPrice5.getText().toString().trim();
                String oAdd5 = etOrgAddPrice5.getText().toString().trim();
                String addPerc5 = etActAddPerc5.getText().toString().trim();
                String oAddPerc5 = etOrgAddPerc5.getText().toString().trim();

                additionalPriceForJewel = additionalPriceForJewel + calcValuesOfAddDed(mrp, addPerc5, oAddPerc5, add5, oAdd5);

                Double ded_price = 0.0;
                Double ded_perc = 0.0;

                String ded_val = et_ded_val.getText().toString();
                if (!ded_val.isEmpty())
                    ded_price = Double.parseDouble(et_ded_val.getText().toString());

                String ded_per = et_ded_per.getText().toString();
                if (!ded_per.isEmpty())
                    ded_perc = Double.parseDouble(et_ded_per.getText().toString());

                ded_price = dedPriceForJewel;

                ded_mrp = Double.valueOf(getValues(mrp, ded_perc, ded_price));

                tvTotalDed.setText("- " + currency + Inad.getCurrencyDecimal(ded_mrp, this));

                Double addi_price = 0.0;
                Double addi_perc = 0.0;

                String addi_val = et_additional_val.getText().toString();
                if (!addi_val.isEmpty())
                    addi_price = Double.parseDouble(et_additional_val.getText().toString());

                String addi_per = et_additional_per.getText().toString();
                if (!addi_per.isEmpty())
                    addi_perc = Double.parseDouble(et_additional_per.getText().toString());

                addi_price = additionalPriceForJewel;

                additional_mrp = Double.valueOf(getValues(mrp, addi_perc, addi_price));

                tvTotalAdditional.setText("+ " + currency + Inad.getCurrencyDecimal(additional_mrp, this));
            }

            mrp = mrp + extraInfoPrice;

            Double dis_price = 0.0;
            Double dis_perc = 0.0;

            String dis_val = et_dis_val.getText().toString();
            if (!dis_val.isEmpty()) {
                dis_price = Double.parseDouble(et_dis_val.getText().toString());
            }

            String dis_per = et_dis_per.getText().toString();
            if (!dis_per.isEmpty()) {
                dis_perc = Double.parseDouble(et_dis_per.getText().toString());
            }

            Double dis_mrp = Double.valueOf(getdiscountmrp(mrp, dis_perc, dis_price).toString());


            if (dis_mrp.doubleValue() != mrp.doubleValue()) {
                tv_calc_dis.setVisibility(View.VISIBLE);
                //tv_calc_dis.setText(currency + Inad.getCurrencyDecimal(dis_mrp, this));
            } else {

                //tv_calc_dis.setVisibility(View.GONE);
            }

           /* if (myDoctorData.getList().get(spn_template.getSelectedItemPosition()).getTemplateName().equalsIgnoreCase("RPD")) {
                dis_mrp = dis_mrp - ded_mrp + additional_mrp + addMrp;
            } else {
                dis_mrp = dis_mrp - ded_mrp + additional_mrp;
            }*/
            dis_mrp = dis_mrp - ded_mrp + additional_mrp;
            isNegative = true;
            if (dis_mrp >= 0) isNegative = false; // at a time of enter ded. price more than price
            else dis_mrp = 0.0;

            tvTotalWoTax.setText("Sub Total : " + currency + Inad.getCurrencyDecimal(dis_mrp, this));
            removeStrikeLineIfNoDiscValuew(tv_calc_dis);
            if (dis_perc == 0 && dis_price > 0) {
                strikeLineIfDiscValuew(tv_calc_dis);
                tvTotalDiscPerc.setText("+ " + currency + Inad.getCurrencyDecimal(dis_price, this));
            }

            tv_calc_dis.setText(currency + Inad.getCurrencyDecimal(mrp, this));
            tv_calc_a_dis.setText(currency + Inad.getCurrencyDecimal(addMrp, this));

            /******************** Tax from Discount *************************/

            Double sgst = 0.0;
            Double cgst = 0.0;
            String s_cgst = et_cgst.getText().toString();
            if (!s_cgst.isEmpty()) cgst = Double.parseDouble(et_cgst.getText().toString());

            String s_sgst = et_sgst.getText().toString();
            if (!s_sgst.isEmpty()) sgst = Double.parseDouble(et_sgst.getText().toString());

            Double stategst = (dis_mrp / 100.0f) * sgst;
            Double centralgst = (dis_mrp / 100.0f) * cgst;

            if (centralgst == 0 && isadd) tv_calc_ctax.setText("");
            else
                tv_calc_ctax.setText("+ " + currency + Inad.getCurrencyDecimal(centralgst, this));
            if (stategst == 0 && isadd) tv_calc_stax.setText("");
            else
                tv_calc_stax.setText("+ " + currency + Inad.getCurrencyDecimal(stategst, this));

            if (dis_mrp > 0) {
                Double total = dis_mrp + stategst + centralgst;
                if (total == 0 && isadd) tv_total.setText("");
                else
                    tv_total.setText("Total : " + currency + Inad.getCurrencyDecimal(total, this) + "" + "");
            } else {
                Double total = mrp + stategst + centralgst;
                if (total == 0 && isadd) tv_total.setText("");
                else
                    tv_total.setText("Total : " + currency + Inad.getCurrencyDecimal(total, this) + "" + "");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void downloadimg() {

        rl_upload.setVisibility(View.VISIBLE);

        int cc = c_img + 1;
        int total = str_img.length;
        tv_progress.setText("Downloading... " + cc + "/" + total);

        File root = new File(Environment.getExternalStorageDirectory()
                + File.separator + getString(R.string.app_name) + File.separator);
        root.mkdirs();
        //String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        //final File localFile = File.createTempFile("images", getFileExtension(fileUri));


        String imgnm = str_img[c_img];
        //imgnm = imgnm.substring(imgnm.lastIndexOf("/") + 1);
        File file = new File(root, imgnm);
        String path = file.getAbsolutePath();

        /*if (file.exists()) {
            c_img++;
            al_selet.add(new ModelFile(path, false));
            if (c_img < str_img.length) {
                downloadimg();
            } else {
                rl_upload.setVisibility(View.GONE);
                c_img = 0;

                selectFileAdapter.setItem(al_selet);

            }
        } else {
            TransferObserver downloadObserver = transferUtility.download("cab-videofiles", imgnm, file);
            doenloadObserverListener(downloadObserver, path);
        }*/

        c_img++;
        al_selet.add(new ModelFile(imgnm, false));
        if (c_img < str_img.length) {
            downloadimg();
        } else {
            rl_upload.setVisibility(View.GONE);
            c_img = 0;
            selectFileAdapter.setItem(al_selet);

        }
    }

    public void doenloadObserverListener(final TransferObserver transferObserver, final String path) {

        transferObserver.setTransferListener(new TransferListener() {
            @Override
            public void onStateChanged(int id, TransferState state) {
                if (state.name().equals("COMPLETED")) {
                    c_img++;
                    al_selet.add(new ModelFile(path, false));
                    if (c_img < str_img.length) {
                        downloadimg();
                    } else {
                        rl_upload.setVisibility(View.GONE);
                        c_img = 0;

                        selectFileAdapter.setItem(al_selet);

                    }
                } else if (state.name().equals("FAILED")) {
                    c_img++;
                    if (c_img < str_img.length) {
                        downloadimg();
                    } else {
                        rl_upload.setVisibility(View.GONE);
                        c_img = 0;
                        selectFileAdapter.setItem(al_selet);
                    }
                }
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                try {
                    int percentage = (int) (bytesCurrent / bytesTotal * 100);


                    //mProgressDialog.setProgress(percentage);


                    //Log.e("percentage ", " : " + percentage);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(int id, Exception ex) {
                Log.e("error", "error");

            }

        });
    }

    public void DeleteData(String deleteIds) {

        findViewById(R.id.rl_pb_cat).setVisibility(View.VISIBLE);

        ApiInterface apiService =
                ApiClient.getClient(CatalogeEditDataActivity.this).create(ApiInterface.class);

        Call call = apiService.DeleteCatalogue(sessionManager.getsession(), deleteIds);

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                findViewById(R.id.rl_pb_cat).setVisibility(View.GONE);

                if (response.isSuccessful()) {
                    try {
                        Intent intent = new Intent();
                        setResult(Activity.RESULT_OK, intent);
                        finish();
                    } catch (Exception e) {


                    }

                } else {

                    try {
                    } catch (Exception e) {
                        Toast.makeText(CatalogeEditDataActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }

            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // Log error here since request failed
                findViewById(R.id.rl_pb_cat).setVisibility(View.GONE);

            }
        });
    }

    @Override
    public void onBackPressed() {

        if (findViewById(R.id.rl_pb_cat).getVisibility() == View.VISIBLE || findViewById(R.id.rl_upload).getVisibility() == View.VISIBLE) {

        } else if (rlAddCategory.getVisibility() == View.VISIBLE) {

        } else {
           /* if (ll_save.getVisibility() == View.VISIBLE) {

                ll_save.setVisibility(View.GONE);
                ll_edit.setVisibility(View.VISIBLE);
                edit();

            } else*/
            if (!issave && !isadd) {
                new CustomDialogClass(CatalogeEditDataActivity.this, new CustomDialogClass.OnDialogClickListener() {
                    @Override
                    public void onDialogImageRunClick(int positon) {
                        if (positon == 0) {
                            finish();
                        } else if (positon == 1) {
                            //str_actionid = str_actionid.replaceAll(",$", "");

                        }
                    }
                }, "SAVE ?").show();

            } else {
                super.onBackPressed();
            }
        }
    }

    ArrayList<ModelFile> CompressItemSelect;

    public class ImageCompress extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            CompressItemSelect = new ArrayList();
            findViewById(R.id.rl_pb_cat).setVisibility(View.VISIBLE);

        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                for (int i = 0; i < al_selet.size(); i++) {
                    if (al_selet.get(i).isIsoffline()) {
                        File ImageFile = new File(al_selet.get(i).getImgpath());
                        File compressedImageFile = null;
                        String strFile = "";
                        try {
                            compressedImageFile = new Compressor(CatalogeEditDataActivity.this).compressToFile(ImageFile);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        strFile = String.valueOf(compressedImageFile);

                        CompressItemSelect.add(new ModelFile(strFile, al_selet.get(i).isIsoffline()));
                    } else {
                        CompressItemSelect.add(new ModelFile(al_selet.get(i).getImgpath(), al_selet.get(i).isIsoffline()));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            findViewById(R.id.rl_pb_cat).setVisibility(View.GONE);
            al_selet = CompressItemSelect;
            uploadimg();
        }
    }

    boolean issgst = false;

    Vision vision;

    public void initvision() {
        Vision.Builder visionBuilder = new Vision.Builder(
                new NetHttpTransport(),
                new AndroidJsonFactory(),
                null);

        visionBuilder.setVisionRequestInitializer(
                new VisionRequestInitializer("AIzaSyBljKEMCnXTENIltyt8_EzpIHRgKoNLbcI"));

        vision = visionBuilder.build();

    }

    private void Detection(final int pos) {
        AsyncTask.execute(new Runnable() {

            @Override
            public void run() {
                try {

                    if (!sm.getIsvisiondetect().equals("1")) return;

                    isdetecting = true;
                    //InputStream inputStream = new FileInputStream("");;
                    if (al_selet.get(pos).isIsdetect()) {

                        runOnUiThread(new Runnable() {
                            public void run() {

                                int c_p = pos;
                                c_p++;
                                if (c_p < al_selet.size()) {
                                    Detection(c_p);
                                } else {
                                    isdetecting = false;
                                    Toast.makeText(CatalogeEditDataActivity.this, "Image Detected !", Toast.LENGTH_LONG).show();
                                }

                            }
                        });
                        return;

                    }
                    InputStream inputStream = null;
                    if (al_selet.get(pos).isIsoffline()) {
                        inputStream = new FileInputStream(al_selet.get(pos).getImgpath());
                    } else {
                        inputStream = new URL(al_selet.get(pos).getImgpath()).openStream();
                    }

                    byte[] photoData = org.apache.commons.io.IOUtils.toByteArray(inputStream);

                    com.google.api.services.vision.v1.model.Image inputImage = new com.google.api.services.vision.v1.model.Image();
                    inputImage.encodeContent(photoData);

                   /* Feature landFeature = new Feature();
                    landFeature.setType("LANDMARK_DETECTION");*/


                    Feature Webdetection = new Feature();
                    Webdetection.setType("WEB_DETECTION");
                    //Webdetection.setMaxResults(2);

                    Feature desiredFeature = new Feature();
                    desiredFeature.setType("TEXT_DETECTION");

                    Feature landdetect = new Feature();
                    landdetect.setType("LANDMARK_DETECTION");

                    Feature facedetect = new Feature();
                    facedetect.setType("FACE_DETECTION");


                    AnnotateImageRequest request = new AnnotateImageRequest();
                    request.setImage(inputImage);
                    request.setFeatures(Arrays.asList(Webdetection, desiredFeature, landdetect));

                    BatchAnnotateImagesRequest batchRequest = new BatchAnnotateImagesRequest();
                    batchRequest.setRequests(Arrays.asList(request));

                    BatchAnnotateImagesResponse batchResponse =
                            vision.images().annotate(batchRequest).execute();

                    ArrayList<ModelFile> al_detect = new ArrayList<>();

                    WebDetection web = batchResponse.getResponses().get(0).getWebDetection();
                    //TextAnnotation tax = batchResponse.getResponses().get(0).getFullTextAnnotation();
                    List<EntityAnnotation> tax = batchResponse.getResponses().get(0).getTextAnnotations();
                    List<EntityAnnotation> landmark = batchResponse.getResponses().get(0).getLandmarkAnnotations();

                    al_detect.add(new ModelFile(true, "Web Detect", pos));
                    if (web != null) {
                        List<WebEntity> webEntities = web.getWebEntities();
                        for (int a = 0; a < webEntities.size(); a++) {
                            String det_str = webEntities.get(a).getDescription();
                            al_detect.add(new ModelFile(false, det_str, pos));
                        }
                    }

                   /* if (tax != null) {
                        al_detect.add(new ModelFile(true, "Full Text Detect"));
                        al_detect.add(new ModelFile(false, tax.toString()));
                    }*/
                    al_detect.add(new ModelFile(true, "Text Detect", pos));
                    if (tax != null) {

                        for (int a = 0; a < tax.size(); a++) {
                            String det_str = tax.get(a).getDescription();
                            al_detect.add(new ModelFile(false, det_str, pos));
                        }
                    }

                    al_detect.add(new ModelFile(true, "LandMark Detect", pos));
                    if (landmark != null) {
                        for (int a = 0; a < landmark.size(); a++) {
                            String det_str = landmark.get(a).getDescription();
                            al_detect.add(new ModelFile(false, det_str, pos));
                        }
                    }


                    al_selet.get(pos).setAl_detect(al_detect);
                    //al_selet.get(pos).setDescription(description);
                    al_selet.get(pos).setIsdetect(true);


                    runOnUiThread(new Runnable() {
                        public void run() {
                            selectFileAdapter.setItem(al_selet);
                            int c_p = pos;
                            c_p++;
                            if (c_p < al_selet.size()) {
                                Detection(c_p);
                            } else {
                                isdetecting = false;
                                Toast.makeText(CatalogeEditDataActivity.this, "Image Detected !", Toast.LENGTH_LONG).show();
                            }
                        }
                    });

                } catch (Exception e) {
                    Log.d("ERROR", e.getMessage());
                }
            }
        });
    }

    boolean isdetecting = false;

    public void SaveEdit(boolean isAdd) {

        if (!ShareModel.getI().dbUnderMaster.isEmpty()) {

            showIsAddAgainDialogue();


        } else if (!isAdd) {
            Gson gson = new Gson();
            String json = gson.toJson(obj);

            Intent intent = new Intent();
            setResult(Activity.RESULT_OK, intent);
            finish();

            //startActivity(getIntent().putExtra("CatalougeContent", json));
        } else {
            Intent intent = new Intent();
            intent.putExtra("isAddNew", true);
            setResult(Activity.RESULT_OK, intent);
            finish();
        }

    }

    Userprofile userProfileData;

    public void setTaxFromProfile() {

        try {
            setTax(userProfileData.field_json_data);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setTax(String field_json_data) throws JSONException {

        Double dis_price = 0.0;
        Double dis_perc = 0.0;

        if (!obj.getDiscountedPrice().equals(""))
            dis_price = Double.valueOf(obj.getDiscountedPrice());
        if (!obj.getDiscountPercentage().equals(""))
            dis_perc = Double.valueOf(obj.getDiscountPercentage());

        if (dis_perc == 0 && isadd) et_dis_per.setText("");
        else et_dis_per.setText(Inad.getCurrencyDecimal(dis_perc, this) + "");
        if (dis_price == 0 && isadd) et_dis_val.setText("");
        else et_dis_val.setText(Inad.getCurrencyDecimal(dis_price, this) + "");

        Double sgst = 0.0;
        Double cgst = 0.0;

        if (field_json_data != null) {

            String Tax = JsonObjParse.getValueEmpty(field_json_data, "Tax");
            String dis_perc_from_field = JsonObjParse.getValueEmpty(field_json_data, "dis_perc");
            if (!dis_perc_from_field.isEmpty()) // if update then empty
            {
                et_dis_per.setText(dis_perc_from_field);
            }

            if (!Tax.isEmpty()) { // Add New

                JSONObject joTax = new JSONObject(field_json_data);

                JSONArray arr = joTax.getJSONArray("Tax");

                JSONObject element;

                et_cgst.setText("");
                tvTaxField1.setText("");
                et_sgst.setText("");
                tvTaxField2.setText("");

                llCgst.setVisibility(View.GONE);
                llSgst.setVisibility(View.GONE);

                for (int i = 0; i < arr.length(); i++) {
                    element = arr.getJSONObject(i); // which for example will be Types,TotalPoints,ExpiringToday in the case of the first array(All_Details)

                    Iterator keys = element.keys();

                    while (keys.hasNext()) {
                        try {
                            String key = (String) keys.next();

                            String taxVal = JsonObjParse.getValueFromJsonObj(element, key);

                            if (et_cgst.getText().toString().isEmpty()) {
                                //sgst = Double.valueOf(jo_tax.getString("SGST"));
                                //cgst = Double.valueOf(jo_tax.getString("CGST"));
                                cgst = Double.valueOf(taxVal);
                                et_cgst.setText(taxVal);
                                tvTaxField1.setText(key);
                                llCgst.setVisibility(View.VISIBLE);

                            } else {
                                sgst = Double.valueOf(taxVal);
                                et_sgst.setText(taxVal);
                                tvTaxField2.setText(key);
                                llSgst.setVisibility(View.VISIBLE);
                            }

                            Log.e("key", key);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            } else { // Update
                et_cgst.setText("");
                tvTaxField1.setText("");
                et_sgst.setText("");
                tvTaxField2.setText("");

                llCgst.setVisibility(View.GONE);
                llSgst.setVisibility(View.GONE);

                if (!field_json_data.equalsIgnoreCase("[]")) {

                    JSONObject joTax = new JSONObject(field_json_data); // :. field and tax is same
                    Iterator keys = joTax.keys();

                    while (keys.hasNext()) {
                        try {
                            String key = (String) keys.next();

                            String taxVal = JsonObjParse.getValueFromJsonObj(joTax, key);

                            if (et_cgst.getText().toString().isEmpty()) {
                                //sgst = Double.valueOf(jo_tax.getString("SGST"));
                                //cgst = Double.valueOf(jo_tax.getString("CGST"));
                                cgst = Double.valueOf(taxVal);
                                et_cgst.setText(taxVal);
                                tvTaxField1.setText(key);
                                llCgst.setVisibility(View.VISIBLE);

                            } else {
                                sgst = Double.valueOf(taxVal);
                                et_sgst.setText(taxVal);
                                tvTaxField2.setText(key);
                                llSgst.setVisibility(View.VISIBLE);
                            }

                            Log.e("key", key);

                        } catch (Exception e) {

                            e.printStackTrace();

                        }
                    }
                }
            }
        }

        if (cgst == 0 && isadd) et_cgst.setText("");
        else et_cgst.setText(Inad.getCurrencyDecimal(cgst, this) + "");
        if (sgst == 0 && isadd) et_sgst.setText("");
        else et_sgst.setText(Inad.getCurrencyDecimal(sgst, this) + "");


        Double mrp = 0.0;
        if (!obj.getPrice().isEmpty()) mrp = Double.valueOf(obj.getPrice());


        Double dis_mrp = Double.valueOf(getdiscountmrp(mrp, dis_perc, dis_price).toString());
        if (dis_mrp.doubleValue() != mrp.doubleValue()) {
            // tv_calc_dis.setText(getString(R.string.currency) + String.format("%.2f", dis_mrp));
            tv_calc_dis.setText(currency + Inad.getCurrencyDecimal(dis_mrp, this));
        } else {
            //tv_calc_dis.setVisibility(View.GONE);
        }
        // tv_calc_dis.setText(getString(R.string.currency) + String.format("%.2f", dis_mrp));
        tv_calc_dis.setText(currency + Inad.getCurrencyDecimal(dis_mrp, this));

        stategst = (dis_mrp / 100.0f) * sgst;
        centralgst = (dis_mrp / 100.0f) * cgst;

        if (mrp == 0 && isadd) et_mrp.setText("");
        else et_mrp.setText(Inad.getCurrencyDecimal(mrp, this) + "");

        Double total = dis_mrp + stategst + centralgst;
        if (total == 0 && isadd) tv_total.setText("");
        else
            tv_total.setText("Total : " + currency + Inad.getCurrencyDecimal(total, this) + "" + "");

    }

    Double stategst = 0.0;
    Double centralgst = 0.0;

    PlayAnim playAnim = new PlayAnim();

    @BindView(R.id.llAnimAddcat)
    LinearLayout llAnimAddcat;

    @BindView(R.id.tvAddEditTitle)
    TextView tvAddEditTitle;

    @BindView(R.id.tvAdd)
    TextView tvAdd;

    @BindView(R.id.tvCancel)
    TextView tvCancel;

    @BindView(R.id.tvMsg)
    TextView tvMsg;

    @BindView(R.id.rlAddCategory)
    RelativeLayout rlAddCategory;

    @OnClick(R.id.tvCancel)
    public void tvCancel(View v) {

        ShareModel.getI().dbUnderMaster = "";

        sessionManager.setAggregator_ID(getString(R.string.Aggregator));
        SharedPrefUserDetail.setBoolean(CatalogeEditDataActivity.this, SharedPrefUserDetail.isfromsupplier, false);
        SharedPrefUserDetail.setString(CatalogeEditDataActivity.this, SharedPrefUserDetail.isfrommyaggregator, "");


        ShareModel.getI().isAddNew = true;

        Intent i = new Intent(CatalogeEditDataActivity.this, CatalougeTabActivity.class);
        i.putExtra("session", sessionManager.getsession());
        startActivity(i);

    }

    @OnClick(R.id.tvAdd)
    public void tvAdd(View v) {
        hideAddCat();
        Intent intent = new Intent();
        intent.putExtra("isAddNew", true);
        setResult(Activity.RESULT_OK, intent);
        finish();
    }

    public void hideAddCat() {

        playAnim.slideUpRl(llAnimAddcat);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                rlAddCategory.setVisibility(View.GONE);
            }
        }, 700);
    }

    public void showIsAddAgainDialogue() {

        playAnim.slideDownRl(llAnimAddcat);
        rlAddCategory.setVisibility(View.VISIBLE);

        tvAdd.setText("Yes");
        tvMsg.setVisibility(View.GONE);

        tvAddEditTitle.setText("Product Added");
        tvAdd.setText("Yes");
        tvCancel.setText("No");
        tvMsg.setText("Add another product?");
        tvMsg.setVisibility(View.VISIBLE);
    }

    @BindView(R.id.etStockPos)
    EditText etStockPos;

    @BindView(R.id.llStock)
    LinearLayout llStock;

    public void setAdditional(String availableStock) { // from user profile
        if (isadd) {
            etStockPos.setText("");
        } else {
            etStockPos.setText("");
            if (availableStock != null) {
                etStockPos.setText(availableStock + "");
            }
        }
        llStock.setVisibility(View.VISIBLE);

        /*if (userProfileData.field_json_data != null) {

            String show_stock_position = JsonObjParse.getValueEmpty(userProfileData.field_json_data, "show_stock_position");
            if(show_stock_position.equalsIgnoreCase("yes")){
                llStock.setVisibility(View.VISIBLE);
            }
            else {
                llStock.setVisibility(View.GONE);
                etStockPos.setText("");
            }
        }*/
    }

    // Wholesale
    @BindView(R.id.llWholesale)
    LinearLayout llWholesale;

    @BindView(R.id.llDiscBlck)
    LinearLayout llDiscBlck;

    @BindView(R.id.llPrice)
    LinearLayout llPrice;

    @BindView(R.id.llAdditionalPrice)
    LinearLayout llAdditionalPrice;

    @BindView(R.id.llPriceRange)
    LinearLayout llPriceRange;

    @BindView(R.id.llDeliveryRange)
    LinearLayout llDeliveryRange;

    @BindView(R.id.etMinQIFWholesale)
    EditText etMinQIFWholesale;

    @BindView(R.id.tv_cur1)
    TextView tv_cur1;

    @BindView(R.id.tv_cur2)
    TextView tv_cur2;

    @BindView(R.id.etMinPriceRangeForQ)
    EditText etMinPriceRangeForQ;

    @BindView(R.id.etMaxPriceRangeForQ)
    EditText etMaxPriceRangeForQ;

    @BindView(R.id.etMinDeldays)
    EditText etMinDeldays;

    @BindView(R.id.etMaxDeldays)
    EditText etMaxDeldays;

    @BindView(R.id.llPropertyFinancialBlock)
    LinearLayout llPropertyFinancialBlock;

    @BindView(R.id.llPropertyInfoBlock)
    LinearLayout llPropertyInfoBlock;

    @BindView(R.id.llNotProperty)
    LinearLayout llNotProperty;

    String service_type_of;

    public void checkWholeSale(String field_json_data) {
        // internal_json_data
        service_type_of = JsonObjParse.getValueEmpty(field_json_data, "service_type_of");
        llDiscBlck.setVisibility(View.VISIBLE);
        llPrice.setVisibility(View.VISIBLE);
        llNotProperty.setVisibility(View.VISIBLE);
        llWholesale.setVisibility(View.GONE);
        llPriceRange.setVisibility(View.GONE);
        llDeliveryRange.setVisibility(View.GONE);
        llPropertyFinancialBlock.setVisibility(View.GONE);
        llPropertyInfoBlock.setVisibility(View.GONE);

        if (service_type_of.equalsIgnoreCase(getString(R.string.wholesale))) {
            llWholesale.setVisibility(View.VISIBLE);
            llPriceRange.setVisibility(View.VISIBLE);
            llDeliveryRange.setVisibility(View.VISIBLE);
            llDiscBlck.setVisibility(View.GONE);
            llPrice.setVisibility(View.GONE);
        } else if (service_type_of.equalsIgnoreCase(getString(R.string.property))) {


            //llDiscBlck.setVisibility(View.GONE);
            llPropertyFinancialBlock.setVisibility(View.VISIBLE);
            llPropertyInfoBlock.setVisibility(View.VISIBLE);
            //llNotProperty.setVisibility(View.GONE);

            llStock.setVisibility(View.GONE);
            setFacing("");

        }

        /*new GetServiceTypeFromBusiUserProfile(this, sessionManager.getcurrentu_nm(), new GetServiceTypeFromBusiUserProfile.Apicallback() {
            @Override
            public void onGetResponse(String a, String response, String sms_emp) {

                String field_json_data = "";
                try {

                    JSONObject Jsonresponse = new JSONObject(response);
                    JSONObject Jsonresponseinfo = Jsonresponse.getJSONObject("response");
                    JSONArray jaData = Jsonresponseinfo.getJSONArray("data");
                    if (jaData.length() > 0) {
                        JSONObject data = jaData.getJSONObject(0);
                        field_json_data = JsonObjParse.getValueEmpty(data.toString(), "field_json_data");

                        String service_type_of = JsonObjParse.getValueEmpty(field_json_data, "service_type_of");
                        if (service_type_of.equalsIgnoreCase(getString(R.string.wholesale))) {

                        }

                    } else {

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
*/
    }

    @BindView(R.id.etSqFt)
    EditText etSqFt;
    @BindView(R.id.etBr)
    EditText etBr;
    @BindView(R.id.etUds)
    EditText etUds;
    @BindView(R.id.etCarpetArea)
    EditText etCarpetArea;
    @BindView(R.id.etBath)
    EditText etBath;
    @BindView(R.id.etKitchen)
    EditText etKitchen;

    @BindView(R.id.etInfo1)
    EditText etInfo1;
    @BindView(R.id.tv_cur3)
    TextView tv_cur3;
    @BindView(R.id.etInfoVal1)
    EditText etInfoVal1;
    @BindView(R.id.etInfo2)
    EditText etInfo2;
    @BindView(R.id.tv_cur4)
    TextView tv_cur4;
    @BindView(R.id.etInfoVal2)
    EditText etInfoVal2;
    @BindView(R.id.etInfo3)
    EditText etInfo3;
    @BindView(R.id.tv_cur5)
    TextView tv_cur5;
    @BindView(R.id.etInfoVal3)
    EditText etInfoVal3;
    @BindView(R.id.etInfo4)
    EditText etInfo4;
    @BindView(R.id.tv_cur6)
    TextView tv_cur6;
    @BindView(R.id.etInfoVal4)
    EditText etInfoVal4;
    @BindView(R.id.etInfo5)
    EditText etInfo5;
    @BindView(R.id.tv_cur7)
    TextView tv_cur7;
    @BindView(R.id.etInfoVal5)
    EditText etInfoVal5;
    String property = "";

    public void setWholeSale() {

        // internal_json_data
        String wholesale = JsonObjParse.getValueEmpty(obj.getInternalJsonData(), "wholesale");
        property = JsonObjParse.getValueEmpty(obj.getInternalJsonData(), "property");

        llDiscBlck.setVisibility(View.VISIBLE);
        llPrice.setVisibility(View.VISIBLE);
        llWholesale.setVisibility(View.GONE);
        llPriceRange.setVisibility(View.GONE);
        llDeliveryRange.setVisibility(View.GONE);
        llPropertyFinancialBlock.setVisibility(View.GONE);
        llPropertyInfoBlock.setVisibility(View.GONE);
        llNotProperty.setVisibility(View.VISIBLE);

        if (!wholesale.isEmpty()) {
            llWholesale.setVisibility(View.VISIBLE);
            llPriceRange.setVisibility(View.VISIBLE);
            llDeliveryRange.setVisibility(View.VISIBLE);
            llDiscBlck.setVisibility(View.GONE);
            llPrice.setVisibility(View.GONE);

            String minQ = JsonObjParse.getValueEmpty(wholesale, "minQ");
            etMinQIFWholesale.setText(minQ);

            String minPrice = JsonObjParse.getValueEmpty(wholesale, "minPrice");
            etMinPriceRangeForQ.setText(minPrice);

            String maxPrice = JsonObjParse.getValueEmpty(wholesale, "maxPrice");
            etMaxPriceRangeForQ.setText(maxPrice);

            String minDay = JsonObjParse.getValueEmpty(wholesale, "minDay");
            etMinDeldays.setText(minDay);

            String maxDay = JsonObjParse.getValueEmpty(wholesale, "maxDay");
            etMaxDeldays.setText(maxDay);
        } else if (!property.isEmpty()) {
            //llDiscBlck.setVisibility(View.GONE);
            llPropertyFinancialBlock.setVisibility(View.VISIBLE);
            llPropertyInfoBlock.setVisibility(View.VISIBLE);
            //llNotProperty.setVisibility(View.GONE);
            llStock.setVisibility(View.GONE);
            if (!isCopy) et_xref.setAlpha(0.5f);


            String proprty_sqft = JsonObjParse.getValueEmpty(property, "proprty_sqft");
            etSqFt.setText(proprty_sqft);

            String uds_sqft = JsonObjParse.getValueEmpty(property, "uds_sqft");
            etUds.setText(uds_sqft);

            String carpet_arae_sqft = JsonObjParse.getValueEmpty(property, "carpet_arae_sqft");
            etCarpetArea.setText(carpet_arae_sqft);

            String bed = JsonObjParse.getValueEmpty(property, "bed");
            etBr.setText(bed);

            String bath = JsonObjParse.getValueEmpty(property, "bath");
            etBath.setText(bath);

            String kitchen = JsonObjParse.getValueEmpty(property, "kitchen");
            etKitchen.setText(kitchen);

            try {
                String ExtraInfoPrice = JsonObjParse.getValueEmpty(property, "ExtraInfoPrice");
                setExtraPropertyInfo(ExtraInfoPrice);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            facing = JsonObjParse.getValueEmpty(property, "facing");
            status_property = JsonObjParse.getValueEmpty(property, "status_property");
            String status_commnet = JsonObjParse.getValueEmpty(property, "status_commnet");
            etCommnetForStatus.setText(status_commnet + "");

            setFacing(facing);
            calc_tax_value();
        }
    }

    ArrayList<SpinModel> alFacing = new ArrayList<>();

    String facing = "", status_property = "";

    @BindView(R.id.spinFacing)
    SearchableSpinner spinFacing;

    @BindView(R.id.rgPropertyStatus)
    RadioGroup rgPropertyStatus;

    @BindView(R.id.rbRedStatus)
    RadioButton rbRedStatus;

    @BindView(R.id.rbGrnStatus)
    RadioButton rbGrnStatus;

    @BindView(R.id.rbAmberStatus)
    RadioButton rbAmberStatus;

    public void setFacing(String faci) {
        this.facing = faci;
        spinFacing.setTitle("Select Facing");

        alFacing.add(new SpinModel("0", "North", 0));
        alFacing.add(new SpinModel("1", "East", 1));
        alFacing.add(new SpinModel("2", "West", 2));
        alFacing.add(new SpinModel("3", "South", 3));
        alFacing.add(new SpinModel("4", "North - East", 4));
        alFacing.add(new SpinModel("5", "North - West", 5));
        alFacing.add(new SpinModel("6", "South - East", 6));
        alFacing.add(new SpinModel("7", "South - West", 7));

        ArrayAdapter<SpinModel> adapter = new ArrayAdapter<SpinModel>(this, R.layout.spinner_currency, alFacing);
        spinFacing.setAdapter(adapter);

        switch (facing) {
            case "North":
                spinFacing.setSelection(0);
                break;
            case "East":
                spinFacing.setSelection(1);
                break;
            case "West":
                spinFacing.setSelection(2);
                break;
            case "South":
                spinFacing.setSelection(3);
                break;
            case "North - East":
                spinFacing.setSelection(4);
                break;
            case "North - West":
                spinFacing.setSelection(5);
                break;
            case "South - West":
                spinFacing.setSelection(7);
                break;
            case "South - East":
                spinFacing.setSelection(6);
                break;
            default:
                facing = "North";
                break;
        }

        spinFacing.regCall(new SearchableSpinner.CallClk() {
            @Override
            public void getClk(int pos, Object item) {

                String json = new Gson().toJson(item, new com.google.common.reflect.TypeToken<SpinModel>() {
                }.getType());

                SpinModel obj = new Gson().fromJson(json, SpinModel.class);

                spinFacing.setSelection(obj.cPos);
                facing = obj.name;

            }
        });


        rgPropertyStatus.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (i) {
                    case (R.id.rbRedStatus):
                        status_property = "red";
                        break;
                    case (R.id.rbGrnStatus):
                        status_property = "green";
                        break;
                    case (R.id.rbAmberStatus):
                        status_property = "amber";
                        break;
                    default:
                        status_property = "red";
                        break;
                }
            }
        });

        switch (status_property) {
            case ("red"):
                rbRedStatus.setChecked(true);
                break;
            case ("green"):
                rbGrnStatus.setChecked(true);
                break;
            case ("amber"):
                rbAmberStatus.setChecked(true);
                break;
            default:
                status_property = "red";
                rbRedStatus.setChecked(true);
                break;
        }


    }

   /* public void setExtraPropertyInfo(String extraInfo) throws JSONException {
        if (extraInfo != null && !extraInfo.isEmpty()) {
            JSONObject joExtra = new JSONObject(extraInfo);
            Iterator keys = joExtra.keys();
            int i = 0;
            while (keys.hasNext()) {
                try {
                    String key = (String) keys.next();
                    String info = JsonObjParse.getValueFromJsonObj(joExtra, key);
                    switch (i) {
                        case 0:
                            etInfo1.setText(key);
                            etInfoVal1.setText(info);
                            break;
                        case 1:
                            etInfo2.setText(key);
                            etInfoVal2.setText(info);
                            break;
                        case 2:
                            etInfo3.setText(key);
                            etInfoVal3.setText(info);
                            break;
                        case 3:
                            etInfo4.setText(key);
                            etInfoVal4.setText(info);
                            break;
                        case 4:
                            etInfo5.setText(key);
                            etInfoVal5.setText(info);
                            break;
                    }
                    i++;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }*/

    public void setExtraPropertyInfo(String extraInfo) throws JSONException {
        if (extraInfo != null && !extraInfo.isEmpty()) {

            JSONArray jaExtraInfo = new JSONArray(extraInfo);

            for (int i = 0; i < jaExtraInfo.length(); i++) {
                JSONObject jsonObject = jaExtraInfo.getJSONObject(i);

                Iterator keys = jsonObject.keys();
                while (keys.hasNext()) {
                    try {
                        String key = (String) keys.next();
                        String info = JsonObjParse.getValueFromJsonObj(jsonObject, key);
                        switch (i) {
                            case 0:
                                etInfo1.setText(key);
                                etInfoVal1.setText(info);
                                break;
                            case 1:
                                etInfo2.setText(key);
                                etInfoVal2.setText(info);
                                break;
                            case 2:
                                etInfo3.setText(key);
                                etInfoVal3.setText(info);
                                break;
                            case 3:
                                etInfo4.setText(key);
                                etInfoVal4.setText(info);
                                break;
                            case 4:
                                etInfo5.setText(key);
                                etInfoVal5.setText(info);
                                break;
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }
        }
    }

    // survey

    @BindView(R.id.llServeyInfo)
    LinearLayout llServeyInfo;

    @BindView(R.id.llNotSurveyJewel)
    LinearLayout llNotSurveyJewel;

    @BindView(R.id.cbCommentBox)
    CheckBox cbCommentBox;

    @BindView(R.id.rvSurveyOptions)
    RecyclerView rvSurveyOptions;

    @BindView(R.id.etOptionsforSurvey)
    EditText etOptionsforSurvey;

    @BindView(R.id.etValuesforSurvey)
    EditText etValuesforSurvey;

    @BindView(R.id.etEnterSurveyQ)
    EditText etEnterSurveyQ;

    @BindView(R.id.etEnterSurveyQPosition)
    EditText etEnterSurveyQPosition;

    @BindView(R.id.etEmoji)
    EditText etEmoji;

    @BindView(R.id.tvClrOptions)
    TextView tvClrOptions;

    @BindView(R.id.tvSaveOptions)
    TextView tvSaveOptions;

    ArrayList<SurveyOptions> alOptions = new ArrayList<>();
    SurveyOptionsListAdapter surveyOptionsListAdapter;

    String survey = "";
    int editPos = 0;

    public void checkSurvey() {
        llNotSurveyJewel.setVisibility(View.VISIBLE);
        llServeyInfo.setVisibility(View.GONE);


        //survey
        service_type_of = JsonObjParse.getValueEmpty(userProfileData.field_json_data, "service_type_of");
        survey = JsonObjParse.getValueEmpty(obj.getInternalJsonData(), "survey");


        if (service_type_of.equalsIgnoreCase(getString(R.string.survey)) && isadd) {
            llNotSurveyJewel.setVisibility(View.GONE);
            llServeyInfo.setVisibility(View.VISIBLE);

            int t = ShareModel.getI().total_catalog;
            t++;
            etEnterSurveyQPosition.setText("" + t);

        } else if (!survey.isEmpty()) {
            llNotSurveyJewel.setVisibility(View.GONE);
            llServeyInfo.setVisibility(View.VISIBLE);

            String optionsList = JsonObjParse.getValueEmpty(survey, "SurveyOptions");

            SurveyOptions surveyOptions = new Gson().fromJson(optionsList, SurveyOptions.class);
            alOptions = surveyOptions.alOptions;
            if (alOptions == null) alOptions = new ArrayList<>();


            String surveyQPos = JsonObjParse.getValueEmpty(survey, "SurveyQPos");
            String commentBox = JsonObjParse.getValueEmpty(survey, "CommentBox");
            if (commentBox.equalsIgnoreCase("1")) cbCommentBox.setChecked(true);
            etEnterSurveyQ.setText(surveyOptions.que);

            etEnterSurveyQPosition.setText(obj.getCrossReference() + "");

        }

        etOptionsforSurvey.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (etOptionsforSurvey.getText().toString().trim().length() > 0) {
                    tvSaveOptions.setEnabled(true);
                    tvSaveOptions.setAlpha(1f);

                } else {
                    tvSaveOptions.setEnabled(false);
                    tvSaveOptions.setAlpha(0.5f);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        etOptionsforSurvey.setText("");

        tvClrOptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etOptionsforSurvey.setText("");
                etEmoji.setText("");
            }
        });

        tvSaveOptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (etValuesforSurvey.getText().toString().trim().isEmpty()) {
                    Inad.alerter("Message", "Please enter values", CatalogeEditDataActivity.this);
                    etValuesforSurvey.requestFocus();

                } else if (!etValuesforSurvey.getText().toString().trim().matches("[0-9.]*")) {
                    Inad.alerter("Message", "Please enter number", CatalogeEditDataActivity.this);
                    etValuesforSurvey.requestFocus();
                } else if (tvSaveOptions.getText().toString().equalsIgnoreCase("edit")) {

                    boolean isValuePresent = false;

                    int val = Integer.parseInt(etValuesforSurvey.getText().toString().trim());
                    String op = etOptionsforSurvey.getText().toString().trim();
                    for (int i = 0; i < alOptions.size(); i++) {
                        int v = alOptions.get(i).value;
                        String op_ = alOptions.get(i).option;

                        if ((v == val && i != editPos) || (op.equalsIgnoreCase(op_) && i != editPos)) {
                            isValuePresent = true;
                            break;
                        }
                    }

                    if (isValuePresent) {
                        Inad.alerter("Messsage", "Enter different option/value..", CatalogeEditDataActivity.this);
                    } else {

                        alOptions.get(editPos).isEdit = false;
                        alOptions.set(editPos, new SurveyOptions(etOptionsforSurvey.getText().toString().trim(), etEmoji.getText().toString().trim(), val));
                        surveyOptionsListAdapter.setItem(alOptions);
                        tvSaveOptions.setText("ADD");

                        etOptionsforSurvey.setText("");
                        etValuesforSurvey.setText("");
                    }

                } else {

                    if (alOptions.size() < 6) {
                        boolean isValuePresent = false;
                        String op = etOptionsforSurvey.getText().toString().trim();
                        int val = Integer.parseInt(etValuesforSurvey.getText().toString().trim());
                        for (int i = 0; i < alOptions.size(); i++) {
                            int v = alOptions.get(i).value;
                            String op_ = alOptions.get(i).option;
                            if (v == val || op.equalsIgnoreCase(op_)) {
                                isValuePresent = true;
                                break;
                            }
                        }

                        if (isValuePresent) {
                            Inad.alerter("Messsage", "Enter different option/value..", CatalogeEditDataActivity.this);
                        } else {

                            alOptions.add(new SurveyOptions(etOptionsforSurvey.getText().toString().trim(), etEmoji.getText().toString().trim(), val));
                            surveyOptionsListAdapter.setItem(alOptions);

                            etOptionsforSurvey.setText("");
                            etValuesforSurvey.setText("");

                        }

                    } else {
                        Inad.alerter("Survey", "Maximum 6 options", CatalogeEditDataActivity.this);
                    }

                }
            }
        });

        surveyOptionsListAdapter = new SurveyOptionsListAdapter(alOptions, this, new SurveyOptionsListAdapter.OnItemClickListener() {
            @Override
            public void onDelete(int item) {

            }

            @Override
            public void onEdit(int item, ArrayList<SurveyOptions> moviesList) {

                tvSaveOptions.setText("EDIT");
                editPos = item;

                for (int i = 0; i < alOptions.size(); i++) {
                    alOptions.get(i).isEdit = false;
                }

                alOptions.get(editPos).isEdit = true;

                etOptionsforSurvey.setText(alOptions.get(editPos).option);
                etValuesforSurvey.setText(alOptions.get(editPos).value + "");
                etEmoji.setText(alOptions.get(editPos).emoji);

                surveyOptionsListAdapter.setItem(alOptions);
            }
        }, true);

        //rvSurveyOptions.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        rvSurveyOptions.setLayoutManager(new GridLayoutManager(this, 2));
        rvSurveyOptions.setAdapter(surveyOptionsListAdapter);
        rvSurveyOptions.setNestedScrollingEnabled(false);

    }

    String jewel = "", metal = "gold", weight = "";
    int metalPosId = 0;
    @BindView(R.id.rvMetals)
    RecyclerView rvMetals;

    @BindView(R.id.llJewelInfo)
    LinearLayout llJewelInfo;

    @BindView(R.id.etJewelWeight)
    EditText etJewelWeight;

    @BindView(R.id.et_ded_val)
    EditText et_ded_val;

    @BindView(R.id.tv_cur_11)
    TextView tv_cur_11;

    @BindView(R.id.et_ded_per)
    EditText et_ded_per;

    @BindView(R.id.et_additional_val)
    EditText et_additional_val;

    @BindView(R.id.tv_cur_12)
    TextView tv_cur_12;

    @BindView(R.id.txtDedPriceCs)
    TextView txtDedPriceCs;

    @BindView(R.id.txtDedOrgPriceCs)
    TextView txtDedOrgPriceCs;

    @BindView(R.id.txtAddOrgPriceCs)
    TextView txtAddOrgPriceCs;

    @BindView(R.id.txtAddPriceCs)
    TextView txtAddPriceCs;

    @BindView(R.id.tvTotalDed)
    TextView tvTotalDed;

    @BindView(R.id.tvTotalAdditional)
    TextView tvTotalAdditional;

    @BindView(R.id.et_additional_per)
    EditText et_additional_per;

    MetalListAdapter metalListAdapter;
    ArrayList<Metal> alMetal = new ArrayList<>();

    public boolean setDefaultMetal(String q) {
        String silver_today_price = JsonObjParse.getValueEmpty(userProfileData.field_json_data, "silver_today_price");
        String gold_today_price = JsonObjParse.getValueEmpty(userProfileData.field_json_data, "gold_today_price");

        if (isadd && q.equalsIgnoreCase("gold")) {
            et_mrp.setText(gold_today_price);
            return true;
        } else if (metal.equalsIgnoreCase(q)) {
            if (metal.equalsIgnoreCase("gold")) {
                et_mrp.setText(gold_today_price);
            }
            if (metal.equalsIgnoreCase("silver")) {
                et_mrp.setText(silver_today_price);
            }

            return true;
        }
        return false;
    }

    public void checkjewel() {
        alMetal = new ArrayList<>();
        llJewelInfo.setVisibility(View.GONE);

        service_type_of = JsonObjParse.getValueEmpty(userProfileData.field_json_data, "service_type_of");
        jewel = JsonObjParse.getValueEmpty(obj.getInternalJsonData(), "jewel");

        metal = JsonObjParse.getValueEmpty(jewel, "metal");
        if (metal.isEmpty()) metal = "gold";

        weight = JsonObjParse.getValueEmpty(jewel, "weight");
        etJewelWeight.setText(weight);

        String ded_perc = JsonObjParse.getValueEmpty(jewel, "ded_perc");
        et_ded_per.setText(ded_perc);

        String ded_val = JsonObjParse.getValueEmpty(jewel, "ded_val");
        et_ded_val.setText(ded_val);

        String additional_perc = JsonObjParse.getValueEmpty(jewel, "additional_perc");
        et_additional_per.setText(additional_perc);

        String additional_val = JsonObjParse.getValueEmpty(jewel, "additional_val");
        et_additional_val.setText(additional_val);

        String silver_today_price = JsonObjParse.getValueEmpty(userProfileData.field_json_data, "silver_today_price");
        String gold_today_price = JsonObjParse.getValueEmpty(userProfileData.field_json_data, "gold_today_price");

        if (isadd && service_type_of.equalsIgnoreCase(getString(R.string.jewel))) {
            et_mrp.setEnabled(false);
            et_mrp.setBackground(null);
            //et_mrp.setText(gold_today_price);
            llJewelInfo.setVisibility(View.VISIBLE);


        } else if (!jewel.isEmpty())// edit , copy
        {
            et_mrp.setEnabled(false);
            et_mrp.setBackground(null);
            llJewelInfo.setVisibility(View.VISIBLE);
        }

        if (llJewelInfo.getVisibility() == View.VISIBLE) {

            etJewelWeight.addTextChangedListener(this);
            et_ded_per.addTextChangedListener(this);
            et_ded_val.addTextChangedListener(this);
            et_additional_per.addTextChangedListener(this);
            et_additional_val.addTextChangedListener(this);

            alMetal.addAll(ShareModel.getI().metal.alMetal);

            //alMetal.add(new Metal("GOLD", gold_today_price, setDefaultMetal("gold")));
            //alMetal.add(new Metal("SILVER", silver_today_price, setDefaultMetal("silver")));


            try {
                String DeductionJewelPrice = JsonObjParse.getValueEmpty(jewel, "DeductionJewelPrice");
                setDeductionJewelInfo(DeductionJewelPrice);

                String AdditionalJewelPrice = JsonObjParse.getValueEmpty(jewel, "AdditionalJewelPrice");
                setAdditionalJewelInfo(AdditionalJewelPrice);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            calc_tax_value();
        }


        metalListAdapter = new MetalListAdapter(alMetal, this, new MetalListAdapter.OnItemClickListener() {
            @Override
            public void onDelete(int item) {

            }

            @Override
            public void onEdit(int item) {


                for (int i = 0; i < alMetal.size(); i++) {
                    alMetal.get(i).isSelect = false;
                }

                alMetal.get(item).isSelect = true;

                metal = alMetal.get(item).metalNm;
                metalPosId = item;

                et_mrp.setText(alMetal.get(item).metalPrice);

                metalListAdapter.setItem(alMetal);
            }
        }, currency);

        rvMetals.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        //rvMetals.setLayoutManager(new GridLayoutManager(this, 2));
        rvMetals.setAdapter(metalListAdapter);
        rvMetals.setNestedScrollingEnabled(false);

        String metalPos = JsonObjParse.getValueEmpty(jewel, "metalPosId");
        metalPosId = 0;
        if (!metalPos.isEmpty()) metalPosId = Integer.parseInt(metalPos);

        for (int i = 0; i < alMetal.size(); i++) {
            alMetal.get(i).isSelect = false;
        }

        if (alMetal.size() > metalPosId) {
            et_mrp.setText(alMetal.get(metalPosId).metalPrice);
            alMetal.get(metalPosId).isSelect = true;
            txtWeight.setText("Weight (" + alMetal.get(metalPosId).unit + ")");
        }
        metalListAdapter.setItem(alMetal);

        rvMetals.scrollToPosition(metalPosId);
    }

    /// Deductions for jewellery

    @BindView(R.id.etDedInfo1)
    EditText etDedInfo1;

    @BindView(R.id.etOrgDedPerc1)
    EditText etOrgDedPerc1;

    @BindView(R.id.etActDedPerc1)
    EditText etActDedPerc1;

    @BindView(R.id.etOrgDedPrice1)
    EditText etOrgDedPrice1;

    @BindView(R.id.etActDedPrice1)
    EditText etActDedPrice1;

    @BindView(R.id.etDedInfo2)
    EditText etDedInfo2;

    @BindView(R.id.etOrgDedPerc2)
    EditText etOrgDedPerc2;

    @BindView(R.id.etActDedPerc2)
    EditText etActDedPerc2;

    @BindView(R.id.etOrgDedPrice2)
    EditText etOrgDedPrice2;

    @BindView(R.id.etActDedPrice2)
    EditText etActDedPrice2;

    @BindView(R.id.etDedInfo3)
    EditText etDedInfo3;

    @BindView(R.id.etOrgDedPerc3)
    EditText etOrgDedPerc3;

    @BindView(R.id.etActDedPerc3)
    EditText etActDedPerc3;

    @BindView(R.id.etOrgDedPrice3)
    EditText etOrgDedPrice3;

    @BindView(R.id.etActDedPrice3)
    EditText etActDedPrice3;

    @BindView(R.id.etDedInfo4)
    EditText etDedInfo4;

    @BindView(R.id.etOrgDedPerc4)
    EditText etOrgDedPerc4;

    @BindView(R.id.etActDedPerc4)
    EditText etActDedPerc4;

    @BindView(R.id.etOrgDedPrice4)
    EditText etOrgDedPrice4;

    @BindView(R.id.etActDedPrice4)
    EditText etActDedPrice4;

    @BindView(R.id.etDedInfo5)
    EditText etDedInfo5;

    @BindView(R.id.etOrgDedPerc5)
    EditText etOrgDedPerc5;

    @BindView(R.id.etActDedPerc5)
    EditText etActDedPerc5;

    @BindView(R.id.etOrgDedPrice5)
    EditText etOrgDedPrice5;

    @BindView(R.id.etActDedPrice5)
    EditText etActDedPrice5;

    @BindView(R.id.llDedInfoPrice)
    LinearLayout llDedInfoPrice;

    @BindView(R.id.llDedShowHideClk)
    LinearLayout llDedShowHideClk;

    @BindView(R.id.ivDownLlDedInfo)
    ImageView ivDownLlDedInfo;

    @OnClick(R.id.llDedShowHideClk)
    public void llDedShowHideClk(View v) {
        if (llDedInfoPrice.getVisibility() == View.VISIBLE) {
            ivDownLlDedInfo.setRotation(270);
            llDedInfoPrice.setVisibility(View.GONE);
        } else {
            ivDownLlDedInfo.setRotation(0);
            llDedInfoPrice.setVisibility(View.VISIBLE);
        }
    }

    public void setDeductionJewelInfo(String deductionInfo) throws JSONException {
        if (deductionInfo != null && !deductionInfo.isEmpty()) {

            JSONArray jaExtraInfo = new JSONArray(deductionInfo);

            for (int i = 0; i < jaExtraInfo.length(); i++) {
                JSONObject jsonObject = jaExtraInfo.getJSONObject(i);

                Iterator keys = jsonObject.keys();
                while (keys.hasNext()) {
                    try {
                        String key = (String) keys.next();
                        String info = JsonObjParse.getValueFromJsonObj(jsonObject, key);
                        switch (i) {
                            case 0:
                                etDedInfo1.setText(key);
                                etOrgDedPerc1.setText(JsonObjParse.getValueEmpty(info, "org_perc") + "");
                                etActDedPerc1.setText(JsonObjParse.getValueEmpty(info, "act_perc") + "");
                                etOrgDedPrice1.setText(JsonObjParse.getValueEmpty(info, "org_price") + "");
                                etActDedPrice1.setText(JsonObjParse.getValueEmpty(info, "act_price") + "");
                                break;
                            case 1:
                                etDedInfo2.setText(key);
                                etOrgDedPerc2.setText(JsonObjParse.getValueEmpty(info, "org_perc") + "");
                                etActDedPerc2.setText(JsonObjParse.getValueEmpty(info, "act_perc") + "");
                                etOrgDedPrice2.setText(JsonObjParse.getValueEmpty(info, "org_price") + "");
                                etActDedPrice2.setText(JsonObjParse.getValueEmpty(info, "act_price") + "");
                                break;
                            case 2:
                                etDedInfo3.setText(key);
                                etOrgDedPerc3.setText(JsonObjParse.getValueEmpty(info, "org_perc") + "");
                                etActDedPerc3.setText(JsonObjParse.getValueEmpty(info, "act_perc") + "");
                                etOrgDedPrice3.setText(JsonObjParse.getValueEmpty(info, "org_price") + "");
                                etActDedPrice3.setText(JsonObjParse.getValueEmpty(info, "act_price") + "");
                                break;
                            case 3:
                                etDedInfo4.setText(key);
                                etOrgDedPerc4.setText(JsonObjParse.getValueEmpty(info, "org_perc") + "");
                                etActDedPerc4.setText(JsonObjParse.getValueEmpty(info, "act_perc") + "");
                                etOrgDedPrice4.setText(JsonObjParse.getValueEmpty(info, "org_price") + "");
                                etActDedPrice4.setText(JsonObjParse.getValueEmpty(info, "act_price") + "");
                                break;
                            case 4:
                                etDedInfo5.setText(key);
                                etOrgDedPerc5.setText(JsonObjParse.getValueEmpty(info, "org_perc") + "");
                                etActDedPerc5.setText(JsonObjParse.getValueEmpty(info, "act_perc") + "");
                                etOrgDedPrice5.setText(JsonObjParse.getValueEmpty(info, "org_price") + "");
                                etActDedPrice5.setText(JsonObjParse.getValueEmpty(info, "act_price") + "");
                                break;
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }
        }

        etActDedPrice1.addTextChangedListener(this);
        etActDedPrice2.addTextChangedListener(this);
        etActDedPrice3.addTextChangedListener(this);
        etActDedPrice4.addTextChangedListener(this);
        etActDedPrice5.addTextChangedListener(this);
        etOrgDedPrice1.addTextChangedListener(this);
        etOrgDedPrice2.addTextChangedListener(this);
        etOrgDedPrice3.addTextChangedListener(this);
        etOrgDedPrice4.addTextChangedListener(this);
        etOrgDedPrice5.addTextChangedListener(this);
        etOrgDedPerc1.addTextChangedListener(this);
        etOrgDedPerc2.addTextChangedListener(this);
        etOrgDedPerc3.addTextChangedListener(this);
        etOrgDedPerc4.addTextChangedListener(this);
        etOrgDedPerc5.addTextChangedListener(this);
        etActDedPerc1.addTextChangedListener(this);
        etActDedPerc2.addTextChangedListener(this);
        etActDedPerc3.addTextChangedListener(this);
        etActDedPerc4.addTextChangedListener(this);
        etActDedPerc5.addTextChangedListener(this);
    }

    @BindView(R.id.etAddInfo1)
    EditText etAddInfo1;

    @BindView(R.id.etOrgAddPerc1)
    EditText etOrgAddPerc1;

    @BindView(R.id.etActAddPerc1)
    EditText etActAddPerc1;

    @BindView(R.id.etOrgAddPrice1)
    EditText etOrgAddPrice1;

    @BindView(R.id.etActAddPrice1)
    EditText etActAddPrice1;

    @BindView(R.id.etAddInfo2)
    EditText etAddInfo2;

    @BindView(R.id.etOrgAddPerc2)
    EditText etOrgAddPerc2;

    @BindView(R.id.etActAddPerc2)
    EditText etActAddPerc2;

    @BindView(R.id.etOrgAddPrice2)
    EditText etOrgAddPrice2;

    @BindView(R.id.etActAddPrice2)
    EditText etActAddPrice2;

    @BindView(R.id.etAddInfo3)
    EditText etAddInfo3;

    @BindView(R.id.etOrgAddPerc3)
    EditText etOrgAddPerc3;

    @BindView(R.id.etActAddPerc3)
    EditText etActAddPerc3;

    @BindView(R.id.etOrgAddPrice3)
    EditText etOrgAddPrice3;

    @BindView(R.id.etActAddPrice3)
    EditText etActAddPrice3;

    @BindView(R.id.etAddInfo4)
    EditText etAddInfo4;

    @BindView(R.id.etOrgAddPerc4)
    EditText etOrgAddPerc4;

    @BindView(R.id.etActAddPerc4)
    EditText etActAddPerc4;

    @BindView(R.id.etOrgAddPrice4)
    EditText etOrgAddPrice4;

    @BindView(R.id.etActAddPrice4)
    EditText etActAddPrice4;

    @BindView(R.id.etAddInfo5)
    EditText etAddInfo5;

    @BindView(R.id.etOrgAddPerc5)
    EditText etOrgAddPerc5;

    @BindView(R.id.etActAddPerc5)
    EditText etActAddPerc5;

    @BindView(R.id.etOrgAddPrice5)
    EditText etOrgAddPrice5;

    @BindView(R.id.etActAddPrice5)
    EditText etActAddPrice5;

    @BindView(R.id.llAddInfoPrice)
    LinearLayout llAddInfoPrice;

    @BindView(R.id.llAddShowHideClk)
    LinearLayout llAddShowHideClk;

    @BindView(R.id.ivDownLlAddInfo)
    ImageView ivDownLlAddInfo;

    @OnClick(R.id.llAddShowHideClk)
    public void llAddShowHideClk(View v) {
        if (llAddInfoPrice.getVisibility() == View.VISIBLE) {
            ivDownLlAddInfo.setRotation(270);
            llAddInfoPrice.setVisibility(View.GONE);
        } else {
            ivDownLlAddInfo.setRotation(0);
            llAddInfoPrice.setVisibility(View.VISIBLE);
        }
    }

    public void setAdditionalJewelInfo(String deductionInfo) throws JSONException {
        if (deductionInfo != null && !deductionInfo.isEmpty()) {

            JSONArray jaExtraInfo = new JSONArray(deductionInfo);

            for (int i = 0; i < jaExtraInfo.length(); i++) {
                JSONObject jsonObject = jaExtraInfo.getJSONObject(i);

                Iterator keys = jsonObject.keys();
                while (keys.hasNext()) {
                    try {
                        String key = (String) keys.next();
                        String info = JsonObjParse.getValueFromJsonObj(jsonObject, key);

                        switch (i) {
                            case 0:
                                etAddInfo1.setText(key);
                                etOrgAddPerc1.setText(JsonObjParse.getValueEmpty(info, "org_perc") + "");
                                etActAddPerc1.setText(JsonObjParse.getValueEmpty(info, "act_perc") + "");
                                etOrgAddPrice1.setText(JsonObjParse.getValueEmpty(info, "org_price") + "");
                                etActAddPrice1.setText(JsonObjParse.getValueEmpty(info, "act_price") + "");
                                break;
                            case 1:
                                etAddInfo2.setText(key);
                                etOrgAddPerc2.setText(JsonObjParse.getValueEmpty(info, "org_perc") + "");
                                etActAddPerc2.setText(JsonObjParse.getValueEmpty(info, "act_perc") + "");
                                etOrgAddPrice2.setText(JsonObjParse.getValueEmpty(info, "org_price") + "");
                                etActAddPrice2.setText(JsonObjParse.getValueEmpty(info, "act_price") + "");
                                break;
                            case 2:
                                etAddInfo3.setText(key);
                                etOrgAddPerc3.setText(JsonObjParse.getValueEmpty(info, "org_perc") + "");
                                etActAddPerc3.setText(JsonObjParse.getValueEmpty(info, "act_perc") + "");
                                etOrgAddPrice3.setText(JsonObjParse.getValueEmpty(info, "org_price") + "");
                                etActAddPrice3.setText(JsonObjParse.getValueEmpty(info, "act_price") + "");
                                break;
                            case 3:
                                etAddInfo4.setText(key);
                                etOrgAddPerc4.setText(JsonObjParse.getValueEmpty(info, "org_perc") + "");
                                etActAddPerc4.setText(JsonObjParse.getValueEmpty(info, "act_perc") + "");
                                etOrgAddPrice4.setText(JsonObjParse.getValueEmpty(info, "org_price") + "");
                                etActAddPrice4.setText(JsonObjParse.getValueEmpty(info, "act_price") + "");
                                break;
                            case 4:
                                etAddInfo5.setText(key);
                                etOrgAddPerc5.setText(JsonObjParse.getValueEmpty(info, "org_perc") + "");
                                etActAddPerc5.setText(JsonObjParse.getValueEmpty(info, "act_perc") + "");
                                etOrgAddPrice5.setText(JsonObjParse.getValueEmpty(info, "org_price") + "");
                                etActAddPrice5.setText(JsonObjParse.getValueEmpty(info, "act_price") + "");
                                break;
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }
        }

        etActAddPrice1.addTextChangedListener(this);
        etActAddPrice2.addTextChangedListener(this);
        etActAddPrice3.addTextChangedListener(this);
        etActAddPrice4.addTextChangedListener(this);
        etActAddPrice5.addTextChangedListener(this);
        etOrgAddPrice1.addTextChangedListener(this);
        etOrgAddPrice2.addTextChangedListener(this);
        etOrgAddPrice3.addTextChangedListener(this);
        etOrgAddPrice4.addTextChangedListener(this);
        etOrgAddPrice5.addTextChangedListener(this);
        etOrgAddPerc1.addTextChangedListener(this);
        etOrgAddPerc2.addTextChangedListener(this);
        etOrgAddPerc3.addTextChangedListener(this);
        etOrgAddPerc4.addTextChangedListener(this);
        etOrgAddPerc5.addTextChangedListener(this);
        etActAddPerc1.addTextChangedListener(this);
        etActAddPerc2.addTextChangedListener(this);
        etActAddPerc3.addTextChangedListener(this);
        etActAddPerc4.addTextChangedListener(this);
        etActAddPerc5.addTextChangedListener(this);
    }

    public void strikeLineIfDiscValuew(TextView textView) {
        textView.setPaintFlags(textView.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
    }

    public void removeStrikeLineIfNoDiscValuew(TextView textView) {
        textView.setPaintFlags(textView.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
    }

    public void setCommodityService() {
        if (al_fill.size() > 0) return;
        al_fill = new ArrayList<>();

        for (int i = 0; i < myDoctorData.getList().size(); i++) {
            al_fill.add(new AutoCompleteTv(myDoctorData.getList().get(i).getTemplateName(), i));
        }
    /*    al_fill.add(new AutoCompleteTv("Shopping cart", 0));
        al_fill.add(new AutoCompleteTv("Shoppping cart - Allow 1", 1)); // Radio
        al_fill.add(new AutoCompleteTv("Shoppping cart - Allow many", 2)); // Checkbox
        al_fill.add(new AutoCompleteTv("Only catalogue", 3));*/
        autoCompleteTv.setautofill(CatalogeEditDataActivity.this, spn_template, al_fill);

        spn_template.setSelection(templateId);

    }

    String doctor_template_type = "";

    private void checkTemplate() {

        service_type_of = JsonObjParse.getValueEmpty(userProfileData.field_json_data, "service_type_of");
        doctor_template_type = JsonObjParse.getValueEmpty(obj.getInternalJsonData(), "doctor_template_type");
        Double Amrp = 0.0;
        if (!JsonObjParse.getValueEmpty(obj.getInternalJsonData(), "additionalPrice_rpd").isEmpty())
            Amrp = Double.valueOf(JsonObjParse.getValueEmpty(obj.getInternalJsonData(), "additionalPrice_rpd"));

        Log.d("doctor_template_type", doctor_template_type);
        if (!doctor_template_type.isEmpty()) {
            llSelectTemplete.setVisibility(View.VISIBLE);
            for (int i = 0; i < myDoctorData.getList().size(); i++) {
                if (myDoctorData.getList().get(i).getTemplateName().equalsIgnoreCase(doctor_template_type)) {
                    myDoctorData.getList().get(i).setCheck(true);
                    spn_template.setSelection(i);
                } else {
                    myDoctorData.getList().get(i).setCheck(false);
                }
            }
            mAdapter.notifyDataSetChanged();

            et_Amrp.setText(Inad.getCurrencyDecimal(Amrp, this) + "");
        }

    }
}