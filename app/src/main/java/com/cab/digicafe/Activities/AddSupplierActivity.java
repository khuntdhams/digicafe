package com.cab.digicafe.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cab.digicafe.Adapter.AddressAdapter;
import com.cab.digicafe.Helper.SessionManager;
import com.cab.digicafe.Model.AddAddress;
import com.cab.digicafe.Model.SupplierNew;
import com.cab.digicafe.R;
import com.cab.digicafe.Rest.ApiClient;
import com.cab.digicafe.Rest.ApiInterface;
import com.cab.digicafe.Rest.Request.SupplierRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddSupplierActivity extends MyAppBaseActivity implements AddressAdapter.OnItemClickListener {

    String bridge = "";

    boolean isadd = false;

    @BindView(R.id.et_fname)
    EditText et_fname;

    @BindView(R.id.et_lname)
    EditText et_lname;

    @BindView(R.id.et_phone)
    EditText et_phone;

    @BindView(R.id.et_email)
    EditText et_email;

    @BindView(R.id.et_loc)
    EditText et_loc;

    @BindView(R.id.et_role)
    EditText et_role;

    @BindView(R.id.tv_title)
    TextView tv_title;

    @BindView(R.id.btn_done)
    Button btn_done;

    @BindView(R.id.iv_back)
    ImageView iv_back;

    @BindView(R.id.tvUNm)
    TextView tvUNm;

    SessionManager sessionManager;
    SupplierNew supplierNew;
    String username = "";


    ArrayList<AddAddress> addressList = new ArrayList<>();
    AddressAdapter mAdapter;
    @BindView(R.id.llAddressList)
    LinearLayout llAddressList;
    @BindView(R.id.ivHideList)
    ImageView ivHideList;
    @BindView(R.id.rvAddressList)
    RecyclerView rvAddressList;
    @BindView(R.id.ivAddList)
    ImageView ivAddList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_add_supplier);

        ButterKnife.bind(this);

        sessionManager = new SessionManager();
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {

            } else {
                bridge = extras.getString("bridge", "");
                username = extras.getString("username", "");
                isadd = extras.getBoolean("isadd", false);
                tvUNm.setText(username);
            }
        }


        try {
            if (isadd) {
                tv_title.setText("Add Supplier");
                JSONObject jObjRes = new JSONObject(bridge);
                JSONObject jObjdata = jObjRes.getJSONObject("response");
                JSONObject jObjResponse = jObjdata.getJSONObject("data");
                JSONArray ja_con = jObjResponse.getJSONArray("content");
                supplierNew = new Gson().fromJson(ja_con.get(0).toString(), SupplierNew.class);
            } else {
                tv_title.setText("Edit Supplier");
                supplierNew = new Gson().fromJson(bridge, SupplierNew.class);
                tvUNm.setText(supplierNew.getUsername() + "");

            }

            String firstname = supplierNew.getFirstname();
            et_fname.setText(firstname);

            String lastname = supplierNew.getLastname();
            et_lname.setText(lastname);

            String contact_no = supplierNew.getContactNo();
            et_phone.setText(contact_no);

            String email_id = supplierNew.getEmailId();
            et_email.setText(email_id);

            String location = supplierNew.getLocation();
            et_loc.setText(location);

            if (location != null && !location.isEmpty()) {
                try {
                    JSONArray array = new JSONArray(location);
                    String type, address;
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject obj = array.getJSONObject(i);
                        type = obj.getString("addressType");
                        address = obj.getString("address");
                        addressList.add(new AddAddress(type, address));
                    }
                    et_loc.setText(addressList.get(0).getAddress());
                    mAdapter = new AddressAdapter(addressList, this, true);
                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
                    rvAddressList.setLayoutManager(mLayoutManager);
                    rvAddressList.setAdapter(mAdapter);
                } catch (Exception e) {
                    e.printStackTrace();
                }


            } else {
            }


        } catch (Exception e) {

        }

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        btn_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                try {

                    JSONObject jo = new JSONObject();
                    JSONArray ja_new = new JSONArray();


                    JSONObject jsonObj;

                    if (isadd) {
                        jsonObj = new JSONObject();
                    } else {
                        Gson gson = new GsonBuilder().create();
                        String json = gson.toJson(supplierNew);// obj is your object

                        jsonObj = new JSONObject(json);
                    }


                    jsonObj.put("preference", "Yes");
                    jsonObj.put("firstname", et_fname.getText().toString().trim());
                    jsonObj.put("lastname", et_lname.getText().toString().trim());
                    jsonObj.put("contact_no", et_phone.getText().toString().trim());
                    jsonObj.put("email_id", et_email.getText().toString().trim());
                    jsonObj.put("location", et_loc.getText().toString().trim());
                    jsonObj.put("role", et_role.getText().toString().trim());

                    if (isadd) {
                        jsonObj.put("contact_bridge_id", supplierNew.getBridgeId());
                        jsonObj.put("account_type", supplierNew.getAccountType());
                        jsonObj.put("username", username);
                        jsonObj.put("flag", "add");

                        ja_new.put(jsonObj);
                        jo.put("newdata", ja_new);
                        JSONArray ja_old = new JSONArray();
                        jo.put("olddata", ja_old);
                    } else {

                        jo.put("newdata", ja_new);
                        JSONArray ja_old = new JSONArray();

                        jsonObj.put("flag", "edit");
                        ja_old.put(jsonObj);
                        jo.put("olddata", ja_old);
                    }

                    SupplierRequest catRequest = new SupplierRequest();

                    catRequest = new Gson().fromJson(jo.toString(), SupplierRequest.class);

                    //String json = new Gson().toJson(catRequest);// obj is your object
                    //String json1 = new Gson().toJson(catRequest);// obj is your object

                    putData(catRequest);
                } catch (JSONException e) {
                    e.printStackTrace();
                    findViewById(R.id.rl_pb_cat).setVisibility(View.GONE);
                }

            }
        });

        ivHideList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llAddressList.setVisibility(View.GONE);
            }
        });
        ivAddList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llAddressList.setVisibility(View.VISIBLE);
            }
        });

    }


    public void putData(SupplierRequest jo_cat) {

        findViewById(R.id.rl_pb_cat).setVisibility(View.VISIBLE);

        ApiInterface apiService =
                ApiClient.getClient(this).create(ApiInterface.class);

        Call call = apiService.PutSupplier(sessionManager.getsession(), jo_cat);

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                findViewById(R.id.rl_pb_cat).setVisibility(View.GONE);

                if (response.isSuccessful()) {
                    try {
                        //ll_edit.setVisibility(View.VISIBLE);
                        //ll_save.setVisibility(View.GONE);
                        //edit();

                        Intent resultIntent = new Intent();
                        setResult(RESULT_OK, resultIntent);
                        finish();


                    } catch (Exception e) {


                    }

                } else {

                    try {
                    } catch (Exception e) {
                        Toast.makeText(AddSupplierActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }

            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // Log error here since request failed
                findViewById(R.id.rl_pb_cat).setVisibility(View.GONE);

            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    public void onItemClick(int pos) {
        et_loc.setText(addressList.get(pos).getAddress());
        llAddressList.setVisibility(View.GONE);
    }

    @Override
    public void onBackPressed() {
        if (llAddressList.getVisibility() == View.VISIBLE) {
            llAddressList.setVisibility(View.GONE);
        } else {
            super.onBackPressed();
        }
    }
}
