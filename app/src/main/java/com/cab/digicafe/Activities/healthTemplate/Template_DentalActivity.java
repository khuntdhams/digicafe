package com.cab.digicafe.Activities.healthTemplate;

import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.TextInputEditText;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cab.digicafe.Activities.ImagePreviewActivity;
import com.cab.digicafe.Activities.RequestApis.MyRequestCall;
import com.cab.digicafe.Adapter.SelectFileAdapter;
import com.cab.digicafe.Adapter.SelectFileBiteAdapter;
import com.cab.digicafe.Adapter.SelectFileModelAdapter;
import com.cab.digicafe.Adapter.SelectFileTempAdapter;
import com.cab.digicafe.Adapter.SelectFileTrayAdapter;
import com.cab.digicafe.Adapter.TeethAdapter;
import com.cab.digicafe.Adapter.TeethImageAdapter;
import com.cab.digicafe.Apputil;
import com.cab.digicafe.CatelogActivity;
import com.cab.digicafe.Database.SettingModel;
import com.cab.digicafe.Database.SqlLiteDbHelper;
import com.cab.digicafe.Helper.Inad;
import com.cab.digicafe.Helper.SessionManager;
import com.cab.digicafe.Helper.SharedPrefUserDetail;
import com.cab.digicafe.Model.Catelog;
import com.cab.digicafe.Model.Dental;
import com.cab.digicafe.Model.ModelFile;
import com.cab.digicafe.Model.Teeth;
import com.cab.digicafe.MyCustomClass.JsonObjParse;
import com.cab.digicafe.R;
import com.cab.digicafe.photopicker.activity.PickImageActivity;
import com.google.api.client.extensions.android.json.AndroidJsonFactory;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.services.vision.v1.Vision;
import com.google.api.services.vision.v1.VisionRequestInitializer;
import com.google.api.services.vision.v1.model.AnnotateImageRequest;
import com.google.api.services.vision.v1.model.BatchAnnotateImagesRequest;
import com.google.api.services.vision.v1.model.BatchAnnotateImagesResponse;
import com.google.api.services.vision.v1.model.EntityAnnotation;
import com.google.api.services.vision.v1.model.Feature;
import com.google.api.services.vision.v1.model.WebDetection;
import com.google.api.services.vision.v1.model.WebEntity;
import com.google.gson.Gson;
import com.kofigyan.stateprogressbar.StateProgressBar;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.media.MediaRecorder.VideoSource.CAMERA;

public class Template_DentalActivity extends HealthTemplateBaseActivity implements View.OnClickListener, TeethAdapter.OnItemClickListener {

    @BindView(R.id.iv_back)
    ImageView iv_back;

    @BindView(R.id.rlStepOne)
    LinearLayout rlStepOne;

    @BindView(R.id.rlStepTwo)
    LinearLayout rlStepTwo;

    @BindView(R.id.rlStepThree)
    LinearLayout rlStepThree;

    @BindView(R.id.rlStepFour)
    LinearLayout rlStepFour;

    @BindView(R.id.rlStepFive)
    RelativeLayout rlStepFive;

    @BindView(R.id.nextStepOne)
    TextView nextStepOne;

    @BindView(R.id.nextStepTwo)
    TextView nextStepTwo;

    @BindView(R.id.nextStepThree)
    TextView nextStepThree;

    @BindView(R.id.nextStepFour)
    TextView nextStepFour;

    @BindView(R.id.nextStepFive)
    TextView nextStepFive;

    @BindView(R.id.usage_stateprogressbar)
    StateProgressBar usage_stateprogressbar;

    @BindView(R.id.rgSex)
    RadioGroup rgSex;

    @BindView(R.id.rbMale)
    RadioButton rbMale;

    @BindView(R.id.rbFemale)
    RadioButton rbFemale;

    @BindView(R.id.et_name)
    TextInputEditText et_name;

    @BindView(R.id.et_age)
    TextInputEditText et_age;

    @BindView(R.id.rvTheethList)
    RecyclerView rvTheethList;

    @BindView(R.id.selectTeeth)
    TextView selectTeeth;

    ArrayList<Teeth> teethList = new ArrayList<>();
    TeethAdapter mAdapter;

    ArrayList<Dental> teethDetail = new ArrayList<>();

    @BindView(R.id.iv_add)
    ImageView iv_add;

    boolean isdetecting = false;
    SettingModel sm;
    SelectFileAdapter selectFileAdapter;
    SelectFileTrayAdapter selectFileTrayAdapter;
    SelectFileModelAdapter selectFileModelAdapter;
    SelectFileBiteAdapter selectFileBiteAdapter;
    SelectFileTempAdapter selectFileTempAdapter;

    @BindView(R.id.rv_selectfile)
    RecyclerView rv_selectfile;

    @BindView(R.id.et_instruction)
    TextInputEditText et_instruction;

    StringBuilder teeth = new StringBuilder();

    @BindView(R.id.et_shade)
    TextInputEditText et_shade;

    @BindView(R.id.cb_trays)
    CheckBox cb_trays;
    @BindView(R.id.iv_add_tray)
    ImageView iv_add_tray;
    @BindView(R.id.rv_selectTray)
    RecyclerView rv_selectTray;

    @BindView(R.id.cb_model)
    CheckBox cb_model;
    @BindView(R.id.iv_add_model)
    ImageView iv_add_model;
    @BindView(R.id.rv_selectModel)
    RecyclerView rv_selectModel;

    @BindView(R.id.cb_bite)
    CheckBox cb_bite;
    @BindView(R.id.iv_add_bite)
    ImageView iv_add_bite;
    @BindView(R.id.rv_selectBite)
    RecyclerView rv_selectBite;

    @BindView(R.id.cb_temp)
    CheckBox cb_temp;
    @BindView(R.id.iv_add_temp)
    ImageView iv_add_temp;
    @BindView(R.id.rv_selectTemp)
    RecyclerView rv_selectTemp;

    ExifInterface exif;
    File f3f;
    int screen_height;
    int screen_width;
    private Uri imageUri;
    ArrayList<ModelFile> al_selet = new ArrayList<>();
    ArrayList<ModelFile> al_selet_tray = new ArrayList<>();
    ArrayList<ModelFile> al_selet_model = new ArrayList<>();
    ArrayList<ModelFile> al_selet_bite = new ArrayList<>();
    ArrayList<ModelFile> al_selet_temp = new ArrayList<>();
    Vision vision;
    int code = 0;

    @BindView(R.id.cbCoping)
    CheckBox cbCoping;
    @BindView(R.id.cbScaling)
    CheckBox cbScaling;
    @BindView(R.id.cbCeramic)
    CheckBox cbCeramic;
    @BindView(R.id.cbDelivery)
    CheckBox cbDelivery;

    @BindView(R.id.tvName)
    TextView tvName;
    @BindView(R.id.tvAge)
    TextView tvAge;
    @BindView(R.id.tvSex)
    TextView tvSex;
    @BindView(R.id.tvSelectedTeeth)
    TextView tvSelectedTeeth;
    @BindView(R.id.tvInstruction)
    TextView tvInstruction;
    @BindView(R.id.tvPreference)
    TextView tvPreference;
    @BindView(R.id.rvTraysImage)
    RecyclerView rvTraysImage;
    TeethImageAdapter adapter;
    @BindView(R.id.llTray)
    LinearLayout llTray;
    @BindView(R.id.llModel)
    LinearLayout llModel;
    @BindView(R.id.rvModelsImage)
    RecyclerView rvModelsImage;
    @BindView(R.id.llBite)
    LinearLayout llBite;
    @BindView(R.id.rvBiteImage)
    RecyclerView rvBiteImage;
    @BindView(R.id.llTemp)
    LinearLayout llTemp;
    @BindView(R.id.rvTempImage)
    RecyclerView rvTempImage;
    @BindView(R.id.llShade)
    LinearLayout llShade;
    @BindView(R.id.rvShadeImage)
    RecyclerView rvShadeImage;
    @BindView(R.id.tvShade)
    TextView tvShade;

    String sex = "Male";
    StringBuilder preference;

    @BindView(R.id.rl_upload)
    RelativeLayout rl_upload;
    @BindView(R.id.tv_progress)
    TextView tv_progress;
    String frombridgeidval;
    SessionManager sessionManager;
    boolean isOnlyShow = false;

    Catelog catelog;

    @BindView(R.id.serialno)
    TextView serialno;
    @BindView(R.id.productqty)
    TextView productqty;
    @BindView(R.id.productprice)
    TextView productprice;

    String currency = "";
    int teethCount = 0;
    @BindView(R.id.llDetail)
    LinearLayout llDetail;

    @BindView(R.id.tvProdName)
    TextView tvProdName;
    @BindView(R.id.llProduct)
    LinearLayout llProduct;
    boolean isFromDatabse = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dental);
        ButterKnife.bind(this);

        isOnlyShow = getIntent().getBooleanExtra("isOnlyShow", false);
        isFromDatabse = getIntent().getBooleanExtra("Database", false);
        if (isOnlyShow) {
            nextStepFive.setVisibility(View.GONE);
        } else {

            if (isFromDatabse) {
                nextStepFive.setVisibility(View.GONE);
            }
            if (getIntent().getExtras().containsKey("Catelog")) {
                String c = getIntent().getExtras().getString("Catelog", "");
                catelog = new Gson().fromJson(c, Catelog.class);

                if (catelog.getCartcount() > 0) {
                    nextStepFive.setText("UPDATE");

                    SqlLiteDbHelper dbHelper = new SqlLiteDbHelper(Template_DentalActivity.this);
                    String internal_json = Apputil.getCatelog(dbHelper, catelog).getInternal_json_data();
                    String d = JsonObjParse.getValueEmpty(internal_json, "dental");

                    dental = new Gson().fromJson(d, Dental.class);
                    if (dental == null) dental = new Dental();

                    et_name.setText(dental.getName());
                    et_age.setText(dental.getAge());
                    sex = dental.getSex();
                    teeth = dental.getTeeth();
                    teethList = dental.getTeethList();
                    et_instruction.setText(dental.getInstructioon());
                    et_shade.setText(dental.getShade());

                    al_selet = dental.getAl_selet();
                    al_selet_bite = dental.getAl_selet_bite();
                    al_selet_model = dental.getAl_selet_model();
                    al_selet_temp = dental.getAl_selet_temp();
                    al_selet_tray = dental.getAl_selet_tray();

                    sex = dental.getSex();

                    cbCoping.setChecked(dental.isCoingTrac);
                    cbScaling.setChecked(dental.ispreScale);
                    cbCeramic.setChecked(dental.isCeramic);
                    cbDelivery.setChecked(dental.isDirect);
                    cb_trays.setChecked(dental.isTray);
                    cb_model.setChecked(dental.isModel);
                    cb_bite.setChecked(dental.isBite);
                    cb_temp.setChecked(dental.isTemp);

                    Detail();

                    onItemClick();

                } else {
                }
            }
        }

        sessionManager = new SessionManager(this);
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                frombridgeidval = null;
            } else {
                frombridgeidval = extras.getString("frombridgeidval");
            }
        } else {
            frombridgeidval = (String) savedInstanceState.getSerializable("frombridgeidval");
        }

        rbMale.setSelected(true);

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (rl_upload.getVisibility() == View.VISIBLE) {

                } else {
                    if (rlStepOne.getVisibility() == View.VISIBLE) {
                        onBackPressed();
                    } else if (rlStepTwo.getVisibility() == View.VISIBLE) {
                        rlStepOne.setVisibility(View.VISIBLE);
                        rlStepTwo.setVisibility(View.GONE);
                        rlStepThree.setVisibility(View.GONE);
                        rlStepFour.setVisibility(View.GONE);
                        rlStepFive.setVisibility(View.GONE);
                        usage_stateprogressbar.setCurrentStateNumber(StateProgressBar.StateNumber.ONE);
                    } else if (rlStepThree.getVisibility() == View.VISIBLE) {
                        rlStepOne.setVisibility(View.GONE);
                        rlStepTwo.setVisibility(View.VISIBLE);
                        rlStepThree.setVisibility(View.GONE);
                        rlStepFour.setVisibility(View.GONE);
                        rlStepFive.setVisibility(View.GONE);
                        usage_stateprogressbar.setCurrentStateNumber(StateProgressBar.StateNumber.TWO);
                    } else if (rlStepFour.getVisibility() == View.VISIBLE) {
                        rlStepOne.setVisibility(View.GONE);
                        rlStepTwo.setVisibility(View.GONE);
                        rlStepThree.setVisibility(View.VISIBLE);
                        rlStepFour.setVisibility(View.GONE);
                        rlStepFive.setVisibility(View.GONE);
                        usage_stateprogressbar.setCurrentStateNumber(StateProgressBar.StateNumber.THREE);
                    } else if (rlStepFive.getVisibility() == View.VISIBLE) {
                        rlStepOne.setVisibility(View.GONE);
                        rlStepTwo.setVisibility(View.GONE);
                        rlStepThree.setVisibility(View.GONE);
                        rlStepFour.setVisibility(View.VISIBLE);
                        rlStepFive.setVisibility(View.GONE);
                        usage_stateprogressbar.setCurrentStateNumber(StateProgressBar.StateNumber.FOUR);
                    } else {
                        onBackPressed();
                    }
                }

            }
        });

        nextStepOne.setOnClickListener(this);
        nextStepTwo.setOnClickListener(this);
        nextStepThree.setOnClickListener(this);
        nextStepFour.setOnClickListener(this);
        nextStepFive.setOnClickListener(this);

        mAdapter = new TeethAdapter(teethList, this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        rvTheethList.setLayoutManager(new GridLayoutManager(this, 8, GridLayoutManager.HORIZONTAL, false));
        rvTheethList.setAdapter(mAdapter);

        if (teethList.isEmpty()) prepareMovieData();

        rgSex.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (R.id.rbMale == checkedId) {
                    sex = "Male";
                } else {
                    sex = "Female";
                }
            }
        });

        cbCoping.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {

                }
            }
        });

        iv_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isdetecting) {
                    showPictureDialog();
                    code = 0;
                    //showGalleryCamera.showMultiPictureDialog();

                } else {
                    Toast.makeText(Template_DentalActivity.this, "Please wait while detecting images...", Toast.LENGTH_LONG).show();
                }

            }
        });

        iv_add_tray.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isdetecting) {
                    showPictureDialog();
                    code = 1;
                    //showGalleryCamera.showMultiPictureDialog();

                } else {
                    Toast.makeText(Template_DentalActivity.this, "Please wait while detecting images...", Toast.LENGTH_LONG).show();
                }
            }
        });
        iv_add_model.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isdetecting) {
                    showPictureDialog();
                    code = 2;
                    //showGalleryCamera.showMultiPictureDialog();

                } else {
                    Toast.makeText(Template_DentalActivity.this, "Please wait while detecting images...", Toast.LENGTH_LONG).show();
                }
            }
        });

        iv_add_bite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isdetecting) {
                    showPictureDialog();
                    code = 3;
                    //showGalleryCamera.showMultiPictureDialog();

                } else {
                    Toast.makeText(Template_DentalActivity.this, "Please wait while detecting images...", Toast.LENGTH_LONG).show();
                }
            }
        });

        iv_add_temp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isdetecting) {
                    showPictureDialog();
                    code = 4;
                    //showGalleryCamera.showMultiPictureDialog();

                } else {
                    Toast.makeText(Template_DentalActivity.this, "Please wait while detecting images...", Toast.LENGTH_LONG).show();
                }
            }
        });

        LinearLayoutManager mLayoutManager1 = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rv_selectfile.setLayoutManager(mLayoutManager1);
        selectFileAdapter = new SelectFileAdapter(al_selet, this, new SelectFileAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(String item) {
            }

            @Override
            public void onDelete(int pos) {
                al_selet.remove(pos);
                selectFileAdapter.setItem(al_selet);
            }

            @Override
            public void onClickString(String pos) {
            }

            @Override
            public void onImgClick(List<ModelFile> data, int pos) {

                if (isdetecting) {
                    Toast.makeText(Template_DentalActivity.this, "Please wait while detecting images...", Toast.LENGTH_SHORT).show();
                } else {
                    ArrayList<ModelFile> al_img = new ArrayList<>();
                    al_img.addAll(data);
                    String imgarray = new Gson().toJson(al_img);
                    Intent intent = new Intent(Template_DentalActivity.this, ImagePreviewActivity.class);
                    intent.putExtra("imgarray", imgarray);
                    intent.putExtra("pos", pos);
                    intent.putExtra("isdrag", false);
                    startActivityForResult(intent, 11);
                }

            }
        }, false, true, false);
        rv_selectfile.setAdapter(selectFileAdapter);

        LinearLayoutManager mLayoutManager2 = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rv_selectTray.setLayoutManager(mLayoutManager2);
        selectFileTrayAdapter = new SelectFileTrayAdapter(al_selet_tray, this, new SelectFileTrayAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(String item) {
            }

            @Override
            public void onDelete(int pos) {
                al_selet_tray.remove(pos);
                selectFileTrayAdapter.setItem(al_selet_tray);
            }

            @Override
            public void onClickString(String pos) {
            }

            @Override
            public void onImgClick(List<ModelFile> data, int pos) {

                if (isdetecting) {
                    Toast.makeText(Template_DentalActivity.this, "Please wait while detecting images...", Toast.LENGTH_SHORT).show();
                } else {
                    ArrayList<ModelFile> al_img = new ArrayList<>();
                    al_img.addAll(data);
                    String imgarray = new Gson().toJson(al_img);
                    Intent intent = new Intent(Template_DentalActivity.this, ImagePreviewActivity.class);
                    intent.putExtra("imgarray", imgarray);
                    intent.putExtra("pos", pos);
                    intent.putExtra("isdrag", false);
                    startActivityForResult(intent, 11);
                }

            }
        }, false, true, false);
        rv_selectTray.setAdapter(selectFileTrayAdapter);

        LinearLayoutManager mLayoutManager3 = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rv_selectModel.setLayoutManager(mLayoutManager3);
        selectFileModelAdapter = new SelectFileModelAdapter(al_selet_model, this, new SelectFileModelAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(String item) {
            }

            @Override
            public void onDelete(int pos) {
                al_selet_model.remove(pos);
                selectFileModelAdapter.setItem(al_selet_model);
            }

            @Override
            public void onClickString(String pos) {
            }

            @Override
            public void onImgClick(List<ModelFile> data, int pos) {

                if (isdetecting) {
                    Toast.makeText(Template_DentalActivity.this, "Please wait while detecting images...", Toast.LENGTH_SHORT).show();
                } else {
                    ArrayList<ModelFile> al_img = new ArrayList<>();
                    al_img.addAll(data);
                    String imgarray = new Gson().toJson(al_img);
                    Intent intent = new Intent(Template_DentalActivity.this, ImagePreviewActivity.class);
                    intent.putExtra("imgarray", imgarray);
                    intent.putExtra("pos", pos);
                    intent.putExtra("isdrag", false);
                    startActivityForResult(intent, 11);
                }

            }
        }, false, true, false);
        rv_selectModel.setAdapter(selectFileModelAdapter);

        LinearLayoutManager mLayoutManager4 = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rv_selectBite.setLayoutManager(mLayoutManager4);
        selectFileBiteAdapter = new SelectFileBiteAdapter(al_selet_bite, this, new SelectFileBiteAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(String item) {
            }

            @Override
            public void onDelete(int pos) {
                al_selet_bite.remove(pos);
                selectFileBiteAdapter.setItem(al_selet_bite);
            }

            @Override
            public void onClickString(String pos) {
            }

            @Override
            public void onImgClick(List<ModelFile> data, int pos) {

                if (isdetecting) {
                    Toast.makeText(Template_DentalActivity.this, "Please wait while detecting images...", Toast.LENGTH_SHORT).show();
                } else {
                    ArrayList<ModelFile> al_img = new ArrayList<>();
                    al_img.addAll(data);
                    String imgarray = new Gson().toJson(al_img);
                    Intent intent = new Intent(Template_DentalActivity.this, ImagePreviewActivity.class);
                    intent.putExtra("imgarray", imgarray);
                    intent.putExtra("pos", pos);
                    intent.putExtra("isdrag", false);
                    startActivityForResult(intent, 11);
                }

            }
        }, false, true, false);
        rv_selectBite.setAdapter(selectFileBiteAdapter);

        LinearLayoutManager mLayoutManager5 = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rv_selectTemp.setLayoutManager(mLayoutManager5);
        selectFileTempAdapter = new SelectFileTempAdapter(al_selet_temp, this, new SelectFileTempAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(String item) {
            }

            @Override
            public void onDelete(int pos) {
                al_selet_temp.remove(pos);
                selectFileTempAdapter.setItem(al_selet_temp);
            }

            @Override
            public void onClickString(String pos) {
            }

            @Override
            public void onImgClick(List<ModelFile> data, int pos) {

                if (isdetecting) {
                    Toast.makeText(Template_DentalActivity.this, "Please wait while detecting images...", Toast.LENGTH_SHORT).show();
                } else {
                    ArrayList<ModelFile> al_img = new ArrayList<>();
                    al_img.addAll(data);
                    String imgarray = new Gson().toJson(al_img);
                    Intent intent = new Intent(Template_DentalActivity.this, ImagePreviewActivity.class);
                    intent.putExtra("imgarray", imgarray);
                    intent.putExtra("pos", pos);
                    intent.putExtra("isdrag", false);
                    startActivityForResult(intent, 11);
                }

            }
        }, false, true, false);
        rv_selectTemp.setAdapter(selectFileTempAdapter);

        getPrice();
    }

    Double sgst = 0.0;
    Double cgst = 0.0;

    public void getPrice() {
        if (isOnlyShow) {
            nextStepFive.setVisibility(View.GONE);
            llDetail.setVisibility(View.GONE);
            llProduct.setVisibility(View.GONE);

        } else {
            if (getIntent().getExtras().containsKey("Catelog")) {
                try {
                    int qty = teethCount;
                    llDetail.setVisibility(View.VISIBLE);
                    llProduct.setVisibility(View.VISIBLE);
                    try {
                        tvProdName.setText(catelog.getDefault_name());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (currency.equalsIgnoreCase("null")) {
                        currency = catelog.getSymbol_native() + " ";
                    } else {
                        currency = SharedPrefUserDetail.getString(this, SharedPrefUserDetail.chit_symbol_native, "") + " ";
                    }

                    DecimalFormat twoDForm = new DecimalFormat("#.##");
                    Double price = Double.parseDouble(catelog.getMrp());

                    Double dis_price = 0.0;
                    Double dis_perc = 0.0;

                    if (!catelog.getDiscounted_price().equals(""))
                        dis_price = Double.valueOf(catelog.getDiscounted_price());
                    if (!catelog.getDiscount_percentage().equals(""))
                        dis_perc = Double.valueOf(catelog.getDiscount_percentage());


                    Double dis_mrp = Double.valueOf(getdiscountmrp(price, dis_perc, dis_price).toString());
                    sgst = 0.0;
                    cgst = 0.0;
                    try {
                        String tax = catelog.getTax_json_data();
                        if (tax != null) {
                            setTax(tax);
                        }

                        Log.e("stategst", String.valueOf(sgst));
                        Log.e("stategst", String.valueOf(cgst));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    Double stategst = (dis_mrp / 100.0f) * sgst;
                    Double centralgst = (dis_mrp / 100.0f) * cgst;
                    Double total = dis_mrp + stategst + centralgst;
                    Double Pricetotal = total * qty;

                    serialno.setText("# " + qty);
                    productqty.setText(currency + Inad.getCurrencyDecimal(total, this));
                    productprice.setText(currency + Inad.getCurrencyDecimal(Pricetotal, this));

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void setTax(String field_json_data) throws JSONException {

        if (field_json_data != null) {

            boolean isF1 = true;

            JSONObject joTax = new JSONObject(field_json_data);

            Iterator keys = joTax.keys();

            while (keys.hasNext()) {
                try {
                    String key = (String) keys.next();

                    String taxVal = JsonObjParse.getValueFromJsonObj(joTax, key);

                    if (isF1) {
                        isF1 = false;
                        sgst = Double.valueOf(taxVal);


                    } else {
                        cgst = Double.valueOf(taxVal);

                    }

                    Log.e("key", key);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public String getdiscountmrp(Double mrpprice, Double discount_percentage, Double discounted_price) {
        Double discountprice = mrpprice;


        if (discount_percentage > 0) {
            if (mrpprice > 0) {
                Double totalDisc = (mrpprice * (discount_percentage / 100));
                discountprice = mrpprice - totalDisc;
            }
        } else if (discounted_price > 0) {
            Double totalDisc = mrpprice - discounted_price;

            discountprice = discounted_price;
        }

        return String.valueOf(discountprice);
    }


    private void showPictureDialog() {
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(Template_DentalActivity.this);
        String[] pictureDialogItems = {"Photo Gallery", "Camera"};
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:

                              /*  Intent i = new Intent(CatalogeEditDataActivity.this, FileActivity.class);
                                i.putExtra("isdoc", false);
                                startActivityForResult(i, 8);*/

                                Intent mIntent = new Intent(Template_DentalActivity.this, PickImageActivity.class);
                                mIntent.putExtra(PickImageActivity.KEY_LIMIT_MAX_IMAGE, 30);
                                mIntent.putExtra(PickImageActivity.KEY_LIMIT_MIN_IMAGE, 1);
                                if (code == 0) {
                                    startActivityForResult(mIntent, 98);
                                } else if (code == 1) {
                                    startActivityForResult(mIntent, 99);
                                } else if (code == 2) {
                                    startActivityForResult(mIntent, 97);
                                } else if (code == 3) {
                                    startActivityForResult(mIntent, 96);
                                } else if (code == 4) {
                                    startActivityForResult(mIntent, 95);
                                }
                                break;
                            case 1:
                                cameraIntent();
                                break;
                        }
                    }
                });
        pictureDialog.show();
    }

    private void cameraIntent() {

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        this.screen_width = size.x;
        this.screen_height = size.y;

        File root = new File(Environment.getExternalStorageDirectory()
                + File.separator + getString(R.string.app_name) + File.separator + "Capture" + File.separator);
        root.mkdirs();
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        f3f = new File(root, "IMG" + timeStamp + ".jpg");

        try {
            imageUri = FileProvider.getUriForFile(
                    Template_DentalActivity.this, getApplicationContext()
                            .getPackageName() + ".provider", this.f3f);
        } catch (Exception e) {
            Toast.makeText(this, "Please check SD card! Image shot is impossible!", Toast.LENGTH_SHORT).show();
        }

        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        intent.putExtra("output", imageUri);
        try {
            if (code == 0) {
                startActivityForResult(intent, CAMERA);
            } else if (code == 1) {
                startActivityForResult(intent, 101);
            } else if (code == 2) {
                startActivityForResult(intent, 102);
            } else if (code == 3) {
                startActivityForResult(intent, 103);
            } else if (code == 4) {
                startActivityForResult(intent, 104);
            }
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to ak aris
        if (resultCode == -1 && requestCode == CAMERA) {
            Bitmap bitmap;
            try {
                bitmap = BitmapFactory.decodeFile(this.f3f.getPath());
                this.exif = new ExifInterface(this.f3f.getPath());
                if (bitmap != null) {
                    // f3f.getAbsolutePath(

                    String picturePath = f3f.getPath();
                    ArrayList<ModelFile> al_temp_selet = new ArrayList<>();
                    al_temp_selet.add(new ModelFile(picturePath, true));
                    al_temp_selet.addAll(al_selet);
                    selectFileAdapter.setItem(al_temp_selet);
                    al_selet = new ArrayList<>();
                    al_selet.addAll(al_temp_selet);
                    Detection(0);

                } else {
                    Toast.makeText(getApplicationContext(), "Picture not taken !", Toast.LENGTH_SHORT).show();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        } else if (requestCode == 98 && resultCode == -1) {
            ArrayList<ModelFile> al_temp_selet = new ArrayList<>();
            ArrayList<String> pathList = data.getExtras().getStringArrayList(PickImageActivity.KEY_DATA_RESULT);
            if (pathList != null && !pathList.isEmpty()) {
                for (int i = 0; i < pathList.size(); i++) {
                    al_temp_selet.add(new ModelFile(pathList.get(i), true));
                }
            }

            al_temp_selet.addAll(al_selet);
            selectFileAdapter.setItem(al_temp_selet);
            al_selet = new ArrayList<>();
            //al_selet = al_temp_selet;
            al_selet.addAll(al_temp_selet);
            Detection(0);

        } else if (resultCode == -1 && requestCode == 101) {
            Bitmap bitmap;
            try {
                bitmap = BitmapFactory.decodeFile(this.f3f.getPath());
                this.exif = new ExifInterface(this.f3f.getPath());
                if (bitmap != null) {
                    // f3f.getAbsolutePath(

                    String picturePath = f3f.getPath();
                    ArrayList<ModelFile> al_temp_selet = new ArrayList<>();
                    al_temp_selet.add(new ModelFile(picturePath, true));
                    al_temp_selet.addAll(al_selet_tray);
                    selectFileTrayAdapter.setItem(al_temp_selet);
                    al_selet_tray = new ArrayList<>();
                    al_selet_tray.addAll(al_temp_selet);
                    Detection1(0);

                } else {
                    Toast.makeText(getApplicationContext(), "Picture not taken !", Toast.LENGTH_SHORT).show();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (requestCode == 99 && resultCode == -1) {
            ArrayList<ModelFile> al_temp_selet = new ArrayList<>();
            ArrayList<String> pathList = data.getExtras().getStringArrayList(PickImageActivity.KEY_DATA_RESULT);
            if (pathList != null && !pathList.isEmpty()) {
                for (int i = 0; i < pathList.size(); i++) {
                    al_temp_selet.add(new ModelFile(pathList.get(i), true));
                }
            }

            al_temp_selet.addAll(al_selet_tray);
            selectFileTrayAdapter.setItem(al_temp_selet);
            al_selet_tray = new ArrayList<>();
            al_selet_tray.addAll(al_temp_selet);
            Detection1(0);

        } else if (resultCode == -1 && requestCode == 102) {
            Bitmap bitmap;
            try {
                bitmap = BitmapFactory.decodeFile(this.f3f.getPath());
                this.exif = new ExifInterface(this.f3f.getPath());
                if (bitmap != null) {
                    // f3f.getAbsolutePath(

                    String picturePath = f3f.getPath();
                    ArrayList<ModelFile> al_temp_selet = new ArrayList<>();
                    al_temp_selet.add(new ModelFile(picturePath, true));
                    al_temp_selet.addAll(al_selet_model);
                    selectFileModelAdapter.setItem(al_temp_selet);
                    al_selet_model = new ArrayList<>();
                    al_selet_model.addAll(al_temp_selet);
                    Detection2(0);

                } else {
                    Toast.makeText(getApplicationContext(), "Picture not taken !", Toast.LENGTH_SHORT).show();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (requestCode == 97 && resultCode == -1) {
            ArrayList<ModelFile> al_temp_selet = new ArrayList<>();
            ArrayList<String> pathList = data.getExtras().getStringArrayList(PickImageActivity.KEY_DATA_RESULT);
            if (pathList != null && !pathList.isEmpty()) {
                for (int i = 0; i < pathList.size(); i++) {
                    al_temp_selet.add(new ModelFile(pathList.get(i), true));
                }
            }

            al_temp_selet.addAll(al_selet_model);
            selectFileModelAdapter.setItem(al_temp_selet);
            al_selet_model = new ArrayList<>();
            al_selet_model.addAll(al_temp_selet);
            Detection2(0);

        } else if (resultCode == -1 && requestCode == 103) {
            Bitmap bitmap;
            try {
                bitmap = BitmapFactory.decodeFile(this.f3f.getPath());
                this.exif = new ExifInterface(this.f3f.getPath());
                if (bitmap != null) {
                    // f3f.getAbsolutePath(

                    String picturePath = f3f.getPath();
                    ArrayList<ModelFile> al_temp_selet = new ArrayList<>();
                    al_temp_selet.add(new ModelFile(picturePath, true));
                    al_temp_selet.addAll(al_selet_bite);
                    selectFileBiteAdapter.setItem(al_temp_selet);
                    al_selet_bite = new ArrayList<>();
                    al_selet_bite.addAll(al_temp_selet);
                    Detection3(0);

                } else {
                    Toast.makeText(getApplicationContext(), "Picture not taken !", Toast.LENGTH_SHORT).show();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (requestCode == 96 && resultCode == -1) {
            ArrayList<ModelFile> al_temp_selet = new ArrayList<>();
            ArrayList<String> pathList = data.getExtras().getStringArrayList(PickImageActivity.KEY_DATA_RESULT);
            if (pathList != null && !pathList.isEmpty()) {
                for (int i = 0; i < pathList.size(); i++) {
                    al_temp_selet.add(new ModelFile(pathList.get(i), true));
                }
            }

            al_temp_selet.addAll(al_selet_bite);
            selectFileBiteAdapter.setItem(al_temp_selet);
            al_selet_bite = new ArrayList<>();
            al_selet_bite.addAll(al_temp_selet);
            Detection3(0);

        } else if (resultCode == -1 && requestCode == 104) {
            Bitmap bitmap;
            try {
                bitmap = BitmapFactory.decodeFile(this.f3f.getPath());
                this.exif = new ExifInterface(this.f3f.getPath());
                if (bitmap != null) {
                    // f3f.getAbsolutePath(

                    String picturePath = f3f.getPath();
                    ArrayList<ModelFile> al_temp_selet = new ArrayList<>();
                    al_temp_selet.add(new ModelFile(picturePath, true));
                    al_temp_selet.addAll(al_selet_temp);
                    selectFileTempAdapter.setItem(al_temp_selet);
                    al_selet_temp = new ArrayList<>();
                    al_selet_temp.addAll(al_temp_selet);
                    Detection4(0);

                } else {
                    Toast.makeText(getApplicationContext(), "Picture not taken !", Toast.LENGTH_SHORT).show();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (requestCode == 95 && resultCode == -1) {
            ArrayList<ModelFile> al_temp_selet = new ArrayList<>();
            ArrayList<String> pathList = data.getExtras().getStringArrayList(PickImageActivity.KEY_DATA_RESULT);
            if (pathList != null && !pathList.isEmpty()) {
                for (int i = 0; i < pathList.size(); i++) {
                    al_temp_selet.add(new ModelFile(pathList.get(i), true));
                }
            }

            al_temp_selet.addAll(al_selet_temp);
            selectFileTempAdapter.setItem(al_temp_selet);
            al_selet_temp = new ArrayList<>();
            al_selet_temp.addAll(al_temp_selet);
            Detection4(0);

        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void initvision() {
        Vision.Builder visionBuilder = new Vision.Builder(
                new NetHttpTransport(),
                new AndroidJsonFactory(),
                null);

        visionBuilder.setVisionRequestInitializer(
                new VisionRequestInitializer("AIzaSyBljKEMCnXTENIltyt8_EzpIHRgKoNLbcI"));

        vision = visionBuilder.build();


    }

    private void Detection(final int pos) {
        AsyncTask.execute(new Runnable() {

            @Override
            public void run() {
                try {

                    if (!sm.getIsvisiondetect().equals("1")) return;

                    isdetecting = true;
                    //InputStream inputStream = new FileInputStream("");;
                    if (al_selet.get(pos).isIsdetect()) {

                        runOnUiThread(new Runnable() {
                            public void run() {

                                int c_p = pos;
                                c_p++;
                                if (c_p < al_selet.size()) {
                                    Detection(c_p);
                                } else {
                                    isdetecting = false;
                                    Toast.makeText(Template_DentalActivity.this, "Image Detected !", Toast.LENGTH_LONG).show();
                                }

                            }
                        });
                        return;

                    }
                    InputStream inputStream = null;
                    if (al_selet.get(pos).isIsoffline()) {
                        inputStream = new FileInputStream(al_selet.get(pos).getImgpath());
                    } else {
                        inputStream = new URL(al_selet.get(pos).getImgpath()).openStream();
                    }

                    byte[] photoData = org.apache.commons.io.IOUtils.toByteArray(inputStream);

                    com.google.api.services.vision.v1.model.Image inputImage = new com.google.api.services.vision.v1.model.Image();
                    inputImage.encodeContent(photoData);

                   /* Feature landFeature = new Feature();
                    landFeature.setType("LANDMARK_DETECTION");*/


                    Feature Webdetection = new Feature();
                    Webdetection.setType("WEB_DETECTION");
                    //Webdetection.setMaxResults(2);

                    Feature desiredFeature = new Feature();
                    desiredFeature.setType("TEXT_DETECTION");

                    Feature landdetect = new Feature();
                    landdetect.setType("LANDMARK_DETECTION");

                    Feature facedetect = new Feature();
                    facedetect.setType("FACE_DETECTION");


                    AnnotateImageRequest request = new AnnotateImageRequest();
                    request.setImage(inputImage);
                    request.setFeatures(Arrays.asList(Webdetection, desiredFeature, landdetect));

                    BatchAnnotateImagesRequest batchRequest = new BatchAnnotateImagesRequest();
                    batchRequest.setRequests(Arrays.asList(request));

                    BatchAnnotateImagesResponse batchResponse =
                            vision.images().annotate(batchRequest).execute();

                    ArrayList<ModelFile> al_detect = new ArrayList<>();

                    WebDetection web = batchResponse.getResponses().get(0).getWebDetection();
                    //TextAnnotation tax = batchResponse.getResponses().get(0).getFullTextAnnotation();
                    List<EntityAnnotation> tax = batchResponse.getResponses().get(0).getTextAnnotations();
                    List<EntityAnnotation> landmark = batchResponse.getResponses().get(0).getLandmarkAnnotations();

                    al_detect.add(new ModelFile(true, "Web Detect", pos));
                    if (web != null) {
                        List<WebEntity> webEntities = web.getWebEntities();
                        for (int a = 0; a < webEntities.size(); a++) {
                            String det_str = webEntities.get(a).getDescription();
                            al_detect.add(new ModelFile(false, det_str, pos));
                        }
                    }

                   /* if (tax != null) {
                        al_detect.add(new ModelFile(true, "Full Text Detect"));
                        al_detect.add(new ModelFile(false, tax.toString()));
                    }*/
                    al_detect.add(new ModelFile(true, "Text Detect", pos));
                    if (tax != null) {

                        for (int a = 0; a < tax.size(); a++) {
                            String det_str = tax.get(a).getDescription();
                            al_detect.add(new ModelFile(false, det_str, pos));
                        }
                    }

                    al_detect.add(new ModelFile(true, "LandMark Detect", pos));
                    if (landmark != null) {
                        for (int a = 0; a < landmark.size(); a++) {
                            String det_str = landmark.get(a).getDescription();
                            al_detect.add(new ModelFile(false, det_str, pos));
                        }
                    }


                    al_selet.get(pos).setAl_detect(al_detect);
                    //al_selet.get(pos).setDescription(description);
                    al_selet.get(pos).setIsdetect(true);


                    runOnUiThread(new Runnable() {
                        public void run() {
                            selectFileAdapter.setItem(al_selet);
                            int c_p = pos;
                            c_p++;
                            if (c_p < al_selet.size()) {
                                Detection(c_p);
                            } else {
                                isdetecting = false;
                                Toast.makeText(Template_DentalActivity.this, "Image Detected !", Toast.LENGTH_LONG).show();
                            }
                        }
                    });

                } catch (Exception e) {
                    Log.d("ERROR", e.getMessage());
                }
            }
        });
    }

    private void Detection1(final int pos) {
        AsyncTask.execute(new Runnable() {

            @Override
            public void run() {
                try {

                    if (!sm.getIsvisiondetect().equals("1")) return;

                    isdetecting = true;
                    //InputStream inputStream = new FileInputStream("");;
                    if (al_selet_tray.get(pos).isIsdetect()) {

                        runOnUiThread(new Runnable() {
                            public void run() {

                                int c_p = pos;
                                c_p++;
                                if (c_p < al_selet_tray.size()) {
                                    Detection1(c_p);
                                } else {
                                    isdetecting = false;
                                    Toast.makeText(Template_DentalActivity.this, "Image Detected !", Toast.LENGTH_LONG).show();
                                }

                            }
                        });
                        return;

                    }
                    InputStream inputStream = null;
                    if (al_selet_tray.get(pos).isIsoffline()) {
                        inputStream = new FileInputStream(al_selet_tray.get(pos).getImgpath());
                    } else {
                        inputStream = new URL(al_selet_tray.get(pos).getImgpath()).openStream();
                    }

                    byte[] photoData = org.apache.commons.io.IOUtils.toByteArray(inputStream);

                    com.google.api.services.vision.v1.model.Image inputImage = new com.google.api.services.vision.v1.model.Image();
                    inputImage.encodeContent(photoData);

                   /* Feature landFeature = new Feature();
                    landFeature.setType("LANDMARK_DETECTION");*/


                    Feature Webdetection = new Feature();
                    Webdetection.setType("WEB_DETECTION");
                    //Webdetection.setMaxResults(2);

                    Feature desiredFeature = new Feature();
                    desiredFeature.setType("TEXT_DETECTION");

                    Feature landdetect = new Feature();
                    landdetect.setType("LANDMARK_DETECTION");

                    Feature facedetect = new Feature();
                    facedetect.setType("FACE_DETECTION");


                    AnnotateImageRequest request = new AnnotateImageRequest();
                    request.setImage(inputImage);
                    request.setFeatures(Arrays.asList(Webdetection, desiredFeature, landdetect));

                    BatchAnnotateImagesRequest batchRequest = new BatchAnnotateImagesRequest();
                    batchRequest.setRequests(Arrays.asList(request));

                    BatchAnnotateImagesResponse batchResponse =
                            vision.images().annotate(batchRequest).execute();

                    ArrayList<ModelFile> al_detect = new ArrayList<>();

                    WebDetection web = batchResponse.getResponses().get(0).getWebDetection();
                    //TextAnnotation tax = batchResponse.getResponses().get(0).getFullTextAnnotation();
                    List<EntityAnnotation> tax = batchResponse.getResponses().get(0).getTextAnnotations();
                    List<EntityAnnotation> landmark = batchResponse.getResponses().get(0).getLandmarkAnnotations();

                    al_detect.add(new ModelFile(true, "Web Detect", pos));
                    if (web != null) {
                        List<WebEntity> webEntities = web.getWebEntities();
                        for (int a = 0; a < webEntities.size(); a++) {
                            String det_str = webEntities.get(a).getDescription();
                            al_detect.add(new ModelFile(false, det_str, pos));
                        }
                    }

                   /* if (tax != null) {
                        al_detect.add(new ModelFile(true, "Full Text Detect"));
                        al_detect.add(new ModelFile(false, tax.toString()));
                    }*/
                    al_detect.add(new ModelFile(true, "Text Detect", pos));
                    if (tax != null) {

                        for (int a = 0; a < tax.size(); a++) {
                            String det_str = tax.get(a).getDescription();
                            al_detect.add(new ModelFile(false, det_str, pos));
                        }
                    }

                    al_detect.add(new ModelFile(true, "LandMark Detect", pos));
                    if (landmark != null) {
                        for (int a = 0; a < landmark.size(); a++) {
                            String det_str = landmark.get(a).getDescription();
                            al_detect.add(new ModelFile(false, det_str, pos));
                        }
                    }


                    al_selet_tray.get(pos).setAl_detect(al_detect);
                    //al_selet.get(pos).setDescription(description);
                    al_selet_tray.get(pos).setIsdetect(true);


                    runOnUiThread(new Runnable() {
                        public void run() {
                            selectFileTrayAdapter.setItem(al_selet_tray);
                            int c_p = pos;
                            c_p++;
                            if (c_p < al_selet_tray.size()) {
                                Detection1(c_p);
                            } else {
                                isdetecting = false;
                                Toast.makeText(Template_DentalActivity.this, "Image Detected !", Toast.LENGTH_LONG).show();
                            }
                        }
                    });

                } catch (Exception e) {
                    Log.d("ERROR", e.getMessage());
                }
            }
        });
    }

    private void Detection2(final int pos) {
        AsyncTask.execute(new Runnable() {

            @Override
            public void run() {
                try {

                    if (!sm.getIsvisiondetect().equals("1")) return;

                    isdetecting = true;
                    //InputStream inputStream = new FileInputStream("");;
                    if (al_selet_model.get(pos).isIsdetect()) {

                        runOnUiThread(new Runnable() {
                            public void run() {

                                int c_p = pos;
                                c_p++;
                                if (c_p < al_selet_model.size()) {
                                    Detection2(c_p);
                                } else {
                                    isdetecting = false;
                                    Toast.makeText(Template_DentalActivity.this, "Image Detected !", Toast.LENGTH_LONG).show();
                                }

                            }
                        });
                        return;

                    }
                    InputStream inputStream = null;
                    if (al_selet_model.get(pos).isIsoffline()) {
                        inputStream = new FileInputStream(al_selet_model.get(pos).getImgpath());
                    } else {
                        inputStream = new URL(al_selet_model.get(pos).getImgpath()).openStream();
                    }

                    byte[] photoData = org.apache.commons.io.IOUtils.toByteArray(inputStream);

                    com.google.api.services.vision.v1.model.Image inputImage = new com.google.api.services.vision.v1.model.Image();
                    inputImage.encodeContent(photoData);

                   /* Feature landFeature = new Feature();
                    landFeature.setType("LANDMARK_DETECTION");*/


                    Feature Webdetection = new Feature();
                    Webdetection.setType("WEB_DETECTION");
                    //Webdetection.setMaxResults(2);

                    Feature desiredFeature = new Feature();
                    desiredFeature.setType("TEXT_DETECTION");

                    Feature landdetect = new Feature();
                    landdetect.setType("LANDMARK_DETECTION");

                    Feature facedetect = new Feature();
                    facedetect.setType("FACE_DETECTION");


                    AnnotateImageRequest request = new AnnotateImageRequest();
                    request.setImage(inputImage);
                    request.setFeatures(Arrays.asList(Webdetection, desiredFeature, landdetect));

                    BatchAnnotateImagesRequest batchRequest = new BatchAnnotateImagesRequest();
                    batchRequest.setRequests(Arrays.asList(request));

                    BatchAnnotateImagesResponse batchResponse =
                            vision.images().annotate(batchRequest).execute();

                    ArrayList<ModelFile> al_detect = new ArrayList<>();

                    WebDetection web = batchResponse.getResponses().get(0).getWebDetection();
                    //TextAnnotation tax = batchResponse.getResponses().get(0).getFullTextAnnotation();
                    List<EntityAnnotation> tax = batchResponse.getResponses().get(0).getTextAnnotations();
                    List<EntityAnnotation> landmark = batchResponse.getResponses().get(0).getLandmarkAnnotations();

                    al_detect.add(new ModelFile(true, "Web Detect", pos));
                    if (web != null) {
                        List<WebEntity> webEntities = web.getWebEntities();
                        for (int a = 0; a < webEntities.size(); a++) {
                            String det_str = webEntities.get(a).getDescription();
                            al_detect.add(new ModelFile(false, det_str, pos));
                        }
                    }

                   /* if (tax != null) {
                        al_detect.add(new ModelFile(true, "Full Text Detect"));
                        al_detect.add(new ModelFile(false, tax.toString()));
                    }*/
                    al_detect.add(new ModelFile(true, "Text Detect", pos));
                    if (tax != null) {

                        for (int a = 0; a < tax.size(); a++) {
                            String det_str = tax.get(a).getDescription();
                            al_detect.add(new ModelFile(false, det_str, pos));
                        }
                    }

                    al_detect.add(new ModelFile(true, "LandMark Detect", pos));
                    if (landmark != null) {
                        for (int a = 0; a < landmark.size(); a++) {
                            String det_str = landmark.get(a).getDescription();
                            al_detect.add(new ModelFile(false, det_str, pos));
                        }
                    }


                    al_selet_model.get(pos).setAl_detect(al_detect);
                    //al_selet.get(pos).setDescription(description);
                    al_selet_model.get(pos).setIsdetect(true);


                    runOnUiThread(new Runnable() {
                        public void run() {
                            selectFileModelAdapter.setItem(al_selet_model);
                            int c_p = pos;
                            c_p++;
                            if (c_p < al_selet_model.size()) {
                                Detection2(c_p);
                            } else {
                                isdetecting = false;
                                Toast.makeText(Template_DentalActivity.this, "Image Detected !", Toast.LENGTH_LONG).show();
                            }
                        }
                    });

                } catch (Exception e) {
                    Log.d("ERROR", e.getMessage());
                }
            }
        });
    }

    private void Detection3(final int pos) {
        AsyncTask.execute(new Runnable() {

            @Override
            public void run() {
                try {

                    if (!sm.getIsvisiondetect().equals("1")) return;

                    isdetecting = true;
                    //InputStream inputStream = new FileInputStream("");;
                    if (al_selet_bite.get(pos).isIsdetect()) {

                        runOnUiThread(new Runnable() {
                            public void run() {

                                int c_p = pos;
                                c_p++;
                                if (c_p < al_selet_bite.size()) {
                                    Detection2(c_p);
                                } else {
                                    isdetecting = false;
                                    Toast.makeText(Template_DentalActivity.this, "Image Detected !", Toast.LENGTH_LONG).show();
                                }

                            }
                        });
                        return;

                    }
                    InputStream inputStream = null;
                    if (al_selet_bite.get(pos).isIsoffline()) {
                        inputStream = new FileInputStream(al_selet_bite.get(pos).getImgpath());
                    } else {
                        inputStream = new URL(al_selet_bite.get(pos).getImgpath()).openStream();
                    }

                    byte[] photoData = org.apache.commons.io.IOUtils.toByteArray(inputStream);

                    com.google.api.services.vision.v1.model.Image inputImage = new com.google.api.services.vision.v1.model.Image();
                    inputImage.encodeContent(photoData);

                   /* Feature landFeature = new Feature();
                    landFeature.setType("LANDMARK_DETECTION");*/


                    Feature Webdetection = new Feature();
                    Webdetection.setType("WEB_DETECTION");
                    //Webdetection.setMaxResults(2);

                    Feature desiredFeature = new Feature();
                    desiredFeature.setType("TEXT_DETECTION");

                    Feature landdetect = new Feature();
                    landdetect.setType("LANDMARK_DETECTION");

                    Feature facedetect = new Feature();
                    facedetect.setType("FACE_DETECTION");


                    AnnotateImageRequest request = new AnnotateImageRequest();
                    request.setImage(inputImage);
                    request.setFeatures(Arrays.asList(Webdetection, desiredFeature, landdetect));

                    BatchAnnotateImagesRequest batchRequest = new BatchAnnotateImagesRequest();
                    batchRequest.setRequests(Arrays.asList(request));

                    BatchAnnotateImagesResponse batchResponse =
                            vision.images().annotate(batchRequest).execute();

                    ArrayList<ModelFile> al_detect = new ArrayList<>();

                    WebDetection web = batchResponse.getResponses().get(0).getWebDetection();
                    //TextAnnotation tax = batchResponse.getResponses().get(0).getFullTextAnnotation();
                    List<EntityAnnotation> tax = batchResponse.getResponses().get(0).getTextAnnotations();
                    List<EntityAnnotation> landmark = batchResponse.getResponses().get(0).getLandmarkAnnotations();

                    al_detect.add(new ModelFile(true, "Web Detect", pos));
                    if (web != null) {
                        List<WebEntity> webEntities = web.getWebEntities();
                        for (int a = 0; a < webEntities.size(); a++) {
                            String det_str = webEntities.get(a).getDescription();
                            al_detect.add(new ModelFile(false, det_str, pos));
                        }
                    }

                   /* if (tax != null) {
                        al_detect.add(new ModelFile(true, "Full Text Detect"));
                        al_detect.add(new ModelFile(false, tax.toString()));
                    }*/
                    al_detect.add(new ModelFile(true, "Text Detect", pos));
                    if (tax != null) {

                        for (int a = 0; a < tax.size(); a++) {
                            String det_str = tax.get(a).getDescription();
                            al_detect.add(new ModelFile(false, det_str, pos));
                        }
                    }

                    al_detect.add(new ModelFile(true, "LandMark Detect", pos));
                    if (landmark != null) {
                        for (int a = 0; a < landmark.size(); a++) {
                            String det_str = landmark.get(a).getDescription();
                            al_detect.add(new ModelFile(false, det_str, pos));
                        }
                    }


                    al_selet_bite.get(pos).setAl_detect(al_detect);
                    //al_selet.get(pos).setDescription(description);
                    al_selet_bite.get(pos).setIsdetect(true);


                    runOnUiThread(new Runnable() {
                        public void run() {
                            selectFileBiteAdapter.setItem(al_selet_bite);
                            int c_p = pos;
                            c_p++;
                            if (c_p < al_selet_bite.size()) {
                                Detection2(c_p);
                            } else {
                                isdetecting = false;
                                Toast.makeText(Template_DentalActivity.this, "Image Detected !", Toast.LENGTH_LONG).show();
                            }
                        }
                    });

                } catch (Exception e) {
                    Log.d("ERROR", e.getMessage());
                }
            }
        });
    }

    private void Detection4(final int pos) {
        AsyncTask.execute(new Runnable() {

            @Override
            public void run() {
                try {

                    if (!sm.getIsvisiondetect().equals("1")) return;

                    isdetecting = true;
                    //InputStream inputStream = new FileInputStream("");;
                    if (al_selet_temp.get(pos).isIsdetect()) {

                        runOnUiThread(new Runnable() {
                            public void run() {

                                int c_p = pos;
                                c_p++;
                                if (c_p < al_selet_temp.size()) {
                                    Detection2(c_p);
                                } else {
                                    isdetecting = false;
                                    Toast.makeText(Template_DentalActivity.this, "Image Detected !", Toast.LENGTH_LONG).show();
                                }

                            }
                        });
                        return;

                    }
                    InputStream inputStream = null;
                    if (al_selet_temp.get(pos).isIsoffline()) {
                        inputStream = new FileInputStream(al_selet_temp.get(pos).getImgpath());
                    } else {
                        inputStream = new URL(al_selet_temp.get(pos).getImgpath()).openStream();
                    }

                    byte[] photoData = org.apache.commons.io.IOUtils.toByteArray(inputStream);

                    com.google.api.services.vision.v1.model.Image inputImage = new com.google.api.services.vision.v1.model.Image();
                    inputImage.encodeContent(photoData);

                   /* Feature landFeature = new Feature();
                    landFeature.setType("LANDMARK_DETECTION");*/


                    Feature Webdetection = new Feature();
                    Webdetection.setType("WEB_DETECTION");
                    //Webdetection.setMaxResults(2);

                    Feature desiredFeature = new Feature();
                    desiredFeature.setType("TEXT_DETECTION");

                    Feature landdetect = new Feature();
                    landdetect.setType("LANDMARK_DETECTION");

                    Feature facedetect = new Feature();
                    facedetect.setType("FACE_DETECTION");


                    AnnotateImageRequest request = new AnnotateImageRequest();
                    request.setImage(inputImage);
                    request.setFeatures(Arrays.asList(Webdetection, desiredFeature, landdetect));

                    BatchAnnotateImagesRequest batchRequest = new BatchAnnotateImagesRequest();
                    batchRequest.setRequests(Arrays.asList(request));

                    BatchAnnotateImagesResponse batchResponse =
                            vision.images().annotate(batchRequest).execute();

                    ArrayList<ModelFile> al_detect = new ArrayList<>();

                    WebDetection web = batchResponse.getResponses().get(0).getWebDetection();
                    //TextAnnotation tax = batchResponse.getResponses().get(0).getFullTextAnnotation();
                    List<EntityAnnotation> tax = batchResponse.getResponses().get(0).getTextAnnotations();
                    List<EntityAnnotation> landmark = batchResponse.getResponses().get(0).getLandmarkAnnotations();

                    al_detect.add(new ModelFile(true, "Web Detect", pos));
                    if (web != null) {
                        List<WebEntity> webEntities = web.getWebEntities();
                        for (int a = 0; a < webEntities.size(); a++) {
                            String det_str = webEntities.get(a).getDescription();
                            al_detect.add(new ModelFile(false, det_str, pos));
                        }
                    }

                   /* if (tax != null) {
                        al_detect.add(new ModelFile(true, "Full Text Detect"));
                        al_detect.add(new ModelFile(false, tax.toString()));
                    }*/
                    al_detect.add(new ModelFile(true, "Text Detect", pos));
                    if (tax != null) {

                        for (int a = 0; a < tax.size(); a++) {
                            String det_str = tax.get(a).getDescription();
                            al_detect.add(new ModelFile(false, det_str, pos));
                        }
                    }

                    al_detect.add(new ModelFile(true, "LandMark Detect", pos));
                    if (landmark != null) {
                        for (int a = 0; a < landmark.size(); a++) {
                            String det_str = landmark.get(a).getDescription();
                            al_detect.add(new ModelFile(false, det_str, pos));
                        }
                    }


                    al_selet_temp.get(pos).setAl_detect(al_detect);
                    //al_selet.get(pos).setDescription(description);
                    al_selet_temp.get(pos).setIsdetect(true);


                    runOnUiThread(new Runnable() {
                        public void run() {
                            selectFileBiteAdapter.setItem(al_selet_temp);
                            int c_p = pos;
                            c_p++;
                            if (c_p < al_selet_temp.size()) {
                                Detection2(c_p);
                            } else {
                                isdetecting = false;
                                Toast.makeText(Template_DentalActivity.this, "Image Detected !", Toast.LENGTH_LONG).show();
                            }
                        }
                    });

                } catch (Exception e) {
                    Log.d("ERROR", e.getMessage());
                }
            }
        });
    }

    private void prepareMovieData() {

        Teeth movie = new Teeth("11", "21", false, false);
        teethList.add(movie);
        movie = new Teeth("12", "22", false, false);
        teethList.add(movie);
        movie = new Teeth("13", "23", false, false);
        teethList.add(movie);
        movie = new Teeth("14", "24", false, false);
        teethList.add(movie);
        movie = new Teeth("15", "25", false, false);
        teethList.add(movie);
        movie = new Teeth("16", "26", false, false);
        teethList.add(movie);
        movie = new Teeth("17", "27", false, false);
        teethList.add(movie);
        movie = new Teeth("18", "28", false, false);
        teethList.add(movie);
        movie = new Teeth("31", "41", false, false);
        teethList.add(movie);
        movie = new Teeth("32", "42", false, false);
        teethList.add(movie);
        movie = new Teeth("33", "43", false, false);
        teethList.add(movie);
        movie = new Teeth("34", "44", false, false);
        teethList.add(movie);
        movie = new Teeth("35", "45", false, false);
        teethList.add(movie);
        movie = new Teeth("36", "46", false, false);
        teethList.add(movie);
        movie = new Teeth("37", "47", false, false);
        teethList.add(movie);
        movie = new Teeth("38", "48", false, false);
        teethList.add(movie);

       /* Teeth movie = new Teeth("18", "48", false, false);
        teethList.add(movie);
        movie = new Teeth("17", "47", false, false);
        teethList.add(movie);
        movie = new Teeth("16", "46", false, false);
        teethList.add(movie);
        movie = new Teeth("15", "45", false, false);
        teethList.add(movie);
        movie = new Teeth("14", "44", false, false);
        teethList.add(movie);
        movie = new Teeth("13", "43", false, false);
        teethList.add(movie);
        movie = new Teeth("12", "42", false, false);
        teethList.add(movie);
        movie = new Teeth("11", "41", false, false);
        teethList.add(movie);
        movie = new Teeth("21", "31", false, false);
        teethList.add(movie);
        movie = new Teeth("22", "32", false, false);
        teethList.add(movie);
        movie = new Teeth("23", "33", false, false);
        teethList.add(movie);
        movie = new Teeth("24", "34", false, false);
        teethList.add(movie);
        movie = new Teeth("25", "35", false, false);
        teethList.add(movie);
        movie = new Teeth("26", "36", false, false);
        teethList.add(movie);
        movie = new Teeth("27", "37", false, false);
        teethList.add(movie);
        movie = new Teeth("28", "38", false, false);
        teethList.add(movie);*/

       /* Teeth movie = new Teeth("1", "32", false, false);
        teethList.add(movie);
        movie = new Teeth("2", "31", false, false);
        teethList.add(movie);
        movie = new Teeth("3", "30", false, false);
        teethList.add(movie);
        movie = new Teeth("4", "29", false, false);
        teethList.add(movie);
        movie = new Teeth("5", "28", false, false);
        teethList.add(movie);
        movie = new Teeth("6", "27", false, false);
        teethList.add(movie);
        movie = new Teeth("7", "26", false, false);
        teethList.add(movie);
        movie = new Teeth("8", "25", false, false);
        teethList.add(movie);
        movie = new Teeth("9", "24", false, false);
        teethList.add(movie);
        movie = new Teeth("10", "23", false, false);
        teethList.add(movie);
        movie = new Teeth("11", "22", false, false);
        teethList.add(movie);
        movie = new Teeth("12", "21", false, false);
        teethList.add(movie);
        movie = new Teeth("13", "20", false, false);
        teethList.add(movie);
        movie = new Teeth("14", "19", false, false);
        teethList.add(movie);
        movie = new Teeth("15", "18", false, false);
        teethList.add(movie);
        movie = new Teeth("16", "17", false, false);
        teethList.add(movie);*/


        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.nextStepOne:

                if (TextUtils.isEmpty(et_name.getText().toString())) {
                    Toast.makeText(this, "Enter Patient Name", Toast.LENGTH_SHORT).show();
                } else if (TextUtils.isEmpty(et_age.getText().toString())) {
                    Toast.makeText(this, "Enter Patient Age", Toast.LENGTH_SHORT).show();
                } else {
                    rlStepOne.setVisibility(View.GONE);
                    rlStepTwo.setVisibility(View.VISIBLE);
                    rlStepThree.setVisibility(View.GONE);
                    rlStepFour.setVisibility(View.GONE);
                    rlStepFive.setVisibility(View.GONE);
                    usage_stateprogressbar.setCurrentStateNumber(StateProgressBar.StateNumber.TWO);
                }
                break;

            case R.id.nextStepTwo:

                if (teeth.length() == 0) {
                    Toast.makeText(this, "Select Teeth", Toast.LENGTH_SHORT).show();
                } else {
                    rlStepOne.setVisibility(View.GONE);
                    rlStepTwo.setVisibility(View.GONE);
                    rlStepThree.setVisibility(View.VISIBLE);
                    rlStepFour.setVisibility(View.GONE);
                    rlStepFive.setVisibility(View.GONE);
                    usage_stateprogressbar.setCurrentStateNumber(StateProgressBar.StateNumber.THREE);
                }
                break;

            case R.id.nextStepThree:
                if (TextUtils.isEmpty(et_shade.getText().toString())) {
                    Toast.makeText(this, "Enter Shade", Toast.LENGTH_SHORT).show();
                } else {
                    rlStepOne.setVisibility(View.GONE);
                    rlStepTwo.setVisibility(View.GONE);
                    rlStepThree.setVisibility(View.GONE);
                    rlStepFour.setVisibility(View.VISIBLE);
                    rlStepFive.setVisibility(View.GONE);
                    usage_stateprogressbar.setCurrentStateNumber(StateProgressBar.StateNumber.FOUR);
                }
                break;

            case R.id.nextStepFour:
                rlStepOne.setVisibility(View.GONE);
                rlStepTwo.setVisibility(View.GONE);
                rlStepThree.setVisibility(View.GONE);
                rlStepFour.setVisibility(View.GONE);
                rlStepFive.setVisibility(View.VISIBLE);
                usage_stateprogressbar.setCurrentStateNumber(StateProgressBar.StateNumber.FIVE);
                Detail();
                getPrice();
                break;

            case R.id.nextStepFive:
                imageUpload(0, 0);
                // Log.d("teethDetail", new Gson().toJson(teethDetail));
                break;
        }
    }

    @Override
    public void onBackPressed() {

        if (rl_upload.getVisibility() == View.VISIBLE) {

        } else {
            if (rlStepOne.getVisibility() == View.VISIBLE) {
                super.onBackPressed();
            } else if (rlStepTwo.getVisibility() == View.VISIBLE) {
                rlStepOne.setVisibility(View.VISIBLE);
                rlStepTwo.setVisibility(View.GONE);
                rlStepThree.setVisibility(View.GONE);
                rlStepFour.setVisibility(View.GONE);
                rlStepFive.setVisibility(View.GONE);
                usage_stateprogressbar.setCurrentStateNumber(StateProgressBar.StateNumber.ONE);
            } else if (rlStepThree.getVisibility() == View.VISIBLE) {
                rlStepOne.setVisibility(View.GONE);
                rlStepTwo.setVisibility(View.VISIBLE);
                rlStepThree.setVisibility(View.GONE);
                rlStepFour.setVisibility(View.GONE);
                rlStepFive.setVisibility(View.GONE);
                usage_stateprogressbar.setCurrentStateNumber(StateProgressBar.StateNumber.TWO);
            } else if (rlStepFour.getVisibility() == View.VISIBLE) {
                rlStepOne.setVisibility(View.GONE);
                rlStepTwo.setVisibility(View.GONE);
                rlStepThree.setVisibility(View.VISIBLE);
                rlStepFour.setVisibility(View.GONE);
                rlStepFive.setVisibility(View.GONE);
                usage_stateprogressbar.setCurrentStateNumber(StateProgressBar.StateNumber.THREE);
            } else if (rlStepFive.getVisibility() == View.VISIBLE) {
                rlStepOne.setVisibility(View.GONE);
                rlStepTwo.setVisibility(View.GONE);
                rlStepThree.setVisibility(View.GONE);
                rlStepFour.setVisibility(View.VISIBLE);
                rlStepFive.setVisibility(View.GONE);
                usage_stateprogressbar.setCurrentStateNumber(StateProgressBar.StateNumber.FOUR);
            } else {
                onBackPressed();
            }
        }
    }

    @Override
    public void onItemClick() {
        selectTeeth.setText("");
        teeth = new StringBuilder();
        teethCount = 0;
        for (int i = 0; i < teethList.size(); i++) {
            if (teethList.get(i).isCheckTop() && teethList.get(i).isCheckBottom()) {
                if (teeth.length() == 0) {
                    teeth.append(teethList.get(i).getNumberTop() + "," + teethList.get(i).getNumberBottom());
                    teethCount = teethCount + 2;
                } else {
                    teeth.append("," + teethList.get(i).getNumberTop() + "," + teethList.get(i).getNumberBottom());
                    teethCount = teethCount + 2;
                }

            } else if (teethList.get(i).isCheckTop()) {
                if (teeth.length() == 0) {
                    teeth.append(teethList.get(i).getNumberTop());
                    teethCount++;
                } else {
                    teeth.append("," + teethList.get(i).getNumberTop());
                    teethCount++;
                }

            } else if (teethList.get(i).isCheckBottom()) {
                if (teeth.length() == 0) {
                    teeth.append(teethList.get(i).getNumberBottom());
                    teethCount++;
                } else {
                    teeth.append("," + teethList.get(i).getNumberBottom());
                    teethCount++;
                }
            }
        }
        selectTeeth.setText(teeth);
    }

    Dental dental = new Dental();

    public void Detail() {

        if (sex.equals("Male")) {
            rbMale.setChecked(true);
        } else {
            rbFemale.setChecked(true);
        }

        tvName.setText(et_name.getText().toString());
        dental.setName(tvName.getText().toString());

        tvAge.setText(et_age.getText().toString());
        dental.setAge(tvAge.getText().toString());

        tvSex.setText(sex);
        dental.setSex(sex);

        tvSelectedTeeth.setText(teeth);
        dental.setTeethList(teethList);
        dental.setTeeth(teeth);

        tvInstruction.setText(et_instruction.getText().toString());
        dental.setInstructioon(tvInstruction.getText().toString());

        preference = new StringBuilder();
        dental.isCoingTrac = false;
        if (cbCoping.isChecked()) {
            dental.isCoingTrac = true;
            if (preference.length() == 0) {
                preference.append("COPING TRYIN");
            } else {
                preference.append("," + "COPING TRYIN");
            }
        }
        dental.ispreScale = false;
        if (cbScaling.isChecked()) {
            dental.ispreScale = true;
            if (preference.length() == 0) {
                preference.append("PREGLAZED TRYIN");
            } else {
                preference.append("," + "PREGLAZED TRYIN");
            }
        }
        dental.isCeramic = false;
        if (cbCeramic.isChecked()) {
            dental.isCeramic = true;
            if (preference.length() == 0) {
                preference.append("CERAMIC TRYIN");
            } else {
                preference.append("," + "CERAMIC TRYIN");
            }
        }
        dental.isDirect = false;
        if (cbDelivery.isChecked()) {
            dental.isDirect = true;
            if (preference.length() == 0) {
                preference.append("DIRECT DELIVERY");
            } else {
                preference.append("," + "DIRECT DELIVERY");
            }
        }
        tvPreference.setText(preference);
        dental.setPreference(preference.toString());

        tvShade.setText(et_shade.getText().toString());
        dental.setShade(et_shade.getText().toString());

        adapter = new TeethImageAdapter(al_selet, this);
        rvShadeImage.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        rvShadeImage.setAdapter(adapter);
        dental.setAl_selet(al_selet);

        if (cb_trays.isChecked() || al_selet_tray.size() > 0) {
            dental.isTray = true;
            llTray.setVisibility(View.VISIBLE);
            adapter = new TeethImageAdapter(al_selet_tray, this);
            rvTraysImage.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
            rvTraysImage.setAdapter(adapter);
        } else {
            dental.isTray = false;
            llTray.setVisibility(View.GONE);
        }
        dental.setAl_selet_tray(al_selet_tray);


        if (cb_model.isChecked() || al_selet_model.size() > 0) {
            dental.isModel = true;
            llModel.setVisibility(View.VISIBLE);
            adapter = new TeethImageAdapter(al_selet_model, this);
            rvModelsImage.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
            rvModelsImage.setAdapter(adapter);
        } else {
            dental.isModel = false;
            llModel.setVisibility(View.GONE);
        }
        dental.setAl_selet_model(al_selet_model);

        if (cb_bite.isChecked() || al_selet_bite.size() > 0) {
            dental.isBite = true;
            llBite.setVisibility(View.VISIBLE);
            adapter = new TeethImageAdapter(al_selet_bite, this);
            rvBiteImage.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
            rvBiteImage.setAdapter(adapter);
        } else {
            dental.isBite = false;
            llBite.setVisibility(View.GONE);
        }
        dental.setAl_selet_bite(al_selet_bite);

        if (cb_temp.isChecked() || al_selet_temp.size() > 0) {
            dental.isTemp = true;
            llTemp.setVisibility(View.VISIBLE);
            adapter = new TeethImageAdapter(al_selet_temp, this);
            rvTempImage.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
            rvTempImage.setAdapter(adapter);
        } else {
            dental.isTemp = false;
            llTemp.setVisibility(View.GONE);
        }
        dental.setAl_selet_temp(al_selet_temp);

        teethDetail.add(dental);
    }

    ArrayList<ModelFile> imageList = new ArrayList<>();
    String msg;

    public void imageUpload(final int arrayPos, final int uploadIndexPos) {

        rl_upload.setVisibility(View.VISIBLE);
        imageList = new ArrayList<>();
        msg = "";
        switch (arrayPos) {
            case 0:
                msg = "Uploading Shade ";
                imageList = al_selet;
                break;
            case 1:
                msg = "Uploading Tray ";
                imageList = al_selet_tray;
                break;
            case 2:
                msg = "Uploading Model ";
                imageList = al_selet_model;
                break;
            case 3:
                msg = "Uploading Bite";
                imageList = al_selet_bite;
                break;
            case 4:
                msg = "Uploading Temp";
                imageList = al_selet_temp;
                break;
        }

        if (uploadIndexPos < imageList.size()) {

            int k = uploadIndexPos + 1;
            msg = msg + " " + k + "/" + imageList.size();
            tv_progress.setText(msg);
            MyRequestCall myRequestCall = new MyRequestCall();

            if (!imageList.get(uploadIndexPos).isIsoffline()) {

                imageUpload(arrayPos, k);
                return;
            } else {

                myRequestCall.uploadAwsS3(sessionManager.getcurrentu_nm(), Template_DentalActivity.this, imageList.get(uploadIndexPos).getImgpath(), new MyRequestCall.CallRequest() {
                    @Override
                    public void onGetResponse(String response) {

                        int t = uploadIndexPos;

                        if (!response.isEmpty()) {

                            imageList.get(uploadIndexPos).setImgpath(response);
                            imageList.get(uploadIndexPos).setIsoffline(false);

                            switch (arrayPos) {
                                case 0:
                                    al_selet = imageList;
                                    break;
                                case 1:
                                    al_selet_tray = imageList;
                                    break;
                                case 2:
                                    al_selet_model = imageList;
                                    break;
                                case 3:
                                    al_selet_bite = imageList;
                                    break;
                                case 4:
                                    al_selet_temp = imageList;
                                    break;
                            }

                            t++;
                            imageUpload(arrayPos, t);
                            //  callUpdateBuisenssProfile();
                        }
                    }
                });
            }

        } else {
            int t = arrayPos;
            t++;

            if (t < 5) {
                imageUpload(t, 0);
            } else {

                dental.setAl_selet(al_selet);
                dental.setAl_selet_bite(al_selet_bite);
                dental.setAl_selet_model(al_selet_model);
                dental.setAl_selet_temp(al_selet_temp);
                dental.setAl_selet_tray(al_selet_tray);

                rl_upload.setVisibility(View.GONE);

                insertCart();
                //Toast.makeText(this, "All Image Are Has Been Uploaded", Toast.LENGTH_SHORT).show();

               /* callCreateChit(this, new CallRequest() {
                    @Override
                    public void onGetResponse(String response) {

                        sessionManager.setcurrentcaterer("");
                        sessionManager.setcurrentcaterername("");
                        sessionManager.setCartproduct("");
                        sessionManager.setCartcount(0);
                        sessionManager.setRemark("");
                        sessionManager.setDate("");
                        sessionManager.setDobForSurvey("");
                        sessionManager.setMobile("");
                        sessionManager.setAddress("");
                        SqlLiteDbHelper dbHelper = new SqlLiteDbHelper(Template_DentalActivity.this);
                        dbHelper.deleteallfromcart();
                        sessionManager.setAggregator_ID(getString(R.string.Aggregator));
                        SharedPrefUserDetail.setString(Template_DentalActivity.this, SharedPrefUserDetail.isfrommyaggregator, "");

                        finish();
                        Intent i = new Intent(Template_DentalActivity.this, HistoryActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        i.putExtra("session", sessionManager.getsession());
                        startActivity(i);


                    }
                }, frombridgeidval, dental);*/
            }
        }
    }

    SqlLiteDbHelper dbHelper;

    public void insertCart() {
        dbHelper = new SqlLiteDbHelper(this);


        String internal_json = catelog.getInternal_json_data();

        try {
            JSONObject joInternalJson = new JSONObject(internal_json);
            joInternalJson.put("dental", new Gson().toJson(dental));
            catelog.setInternal_json_data(joInternalJson.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (catelog.getCartcount() == 0) {
            catelog.setCartcount(1);
            Toast.makeText(Template_DentalActivity.this, "Report added", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(Template_DentalActivity.this, "Report updated", Toast.LENGTH_SHORT).show();

        }

        String[] teethList = dental.getTeeth().toString().split(",");
        productcount = teethList.length;
        catelog.setCartcount(productcount);

        Apputil.inserttocart(dbHelper, catelog, CatelogActivity.aliasname);
        nextStepFive.setText("UPDATE");


        finish();
    }

}