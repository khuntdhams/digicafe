package com.cab.digicafe.Activities.master;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cab.digicafe.Activities.MyAppBaseActivity;
import com.cab.digicafe.Activities.RequestApis.MyRequestCall;
import com.cab.digicafe.Activities.RequestApis.MyRequst;
import com.cab.digicafe.Activities.RequestApis.MyResponse.CategoryViewModel;
import com.cab.digicafe.Adapter.master.CategoryFlowListAdapter;
import com.cab.digicafe.Adapter.master.CategoryUnderMasterListAdapter;
import com.cab.digicafe.Helper.DragTouchTopBottomHelperForAllFlowCategory;
import com.cab.digicafe.Helper.DragTouchTopBottomHelperForCategory;
import com.cab.digicafe.Helper.LoadImg;
import com.cab.digicafe.Helper.SessionManager;
import com.cab.digicafe.MyCustomClass.PlayAnim;
import com.cab.digicafe.MyCustomClass.ShowGalleryCamera;
import com.cab.digicafe.MyCustomClass.Utils;
import com.cab.digicafe.R;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.xiaofeng.flowlayoutmanager.Alignment;
import com.xiaofeng.flowlayoutmanager.FlowLayoutManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CategoryUnderMsterActivity extends MyAppBaseActivity implements SwipyRefreshLayout.OnRefreshListener {


    @BindView(R.id.ivDelCat)
    ImageView ivDelCat;

    @BindView(R.id.rvCat)
    RecyclerView rvCat;

    @BindView(R.id.tv_empty)
    TextView tv_empty;

    @BindView(R.id.tvTitle)
    TextView tvTitle;

    @BindView(R.id.rl_pb)
    RelativeLayout rl_pb;

    @BindView(R.id.rlAddCategory)
    RelativeLayout rlAddCategory;


    @BindView(R.id.llAnimAddcat)
    LinearLayout llAnimAddcat;

    @BindView(R.id.etCatNm)
    EditText etCatNm;

    @BindView(R.id.tvCatNm)
    TextView tvCatNm;

    @BindView(R.id.etSortNm)
    EditText etSortNm;

    @BindView(R.id.tvAddEditTitle)
    TextView tvAddEditTitle;

    @BindView(R.id.tvAdd)
    TextView tvAdd;

    @BindView(R.id.tvMsg)
    TextView tvMsg;

    MyRequestCall myRequestCall = new MyRequestCall();
    MyRequst myRequst = new MyRequst();
    Context context;
    SessionManager sessionManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_under_mster);
        ButterKnife.bind(this);

        ivDelCat.setVisibility(View.GONE);
        tv_empty.setVisibility(View.GONE);
        etSortNm.setVisibility(View.GONE);
        context = this;
        sessionManager = new SessionManager(context);
        myRequst.cookie = sessionManager.getsession();
        myRequst.page = pagecount;

        setCategory();

        refreshlist();

        callCat();

        rlAddCategory.setVisibility(View.GONE);


        etCatNm.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String cat = etCatNm.getText().toString().trim();
                tvCatNm.setText(cat);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


    }

    PlayAnim playAnim = new PlayAnim();

    @OnClick(R.id.iv_back)
    public void iv_back(View v) {
        onBackPressed();
    }


    public void hideAddCat() {

        /*if(isReplace){
            alCategory = new ArrayList<>();
            alCategory.addAll(tmpAlCategoryCancelReplace);

            categoryUnderMasterListAdapter.setitem(alCategory);
            categoryFlowListAdapter.setitem(alCategory);
        }
*/
        Utils.hideSoftKeyboard(context, etCatNm);


        playAnim.slideUpRl(llAnimAddcat);
        isEdit = false;
        isReplace = false;
        isDelete = false;
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                rlAddCategory.setVisibility(View.GONE);

            }
        }, 700);


    }

    String sortBy = ""; // if empty category

    boolean isEdit = false;
    boolean isReplace = false;
    boolean isDelete = false;
    CategoryViewModel categoryViewModel;


    public void showAddCat() {

        playAnim.slideDownRl(llAnimAddcat);
        rlAddCategory.setVisibility(View.VISIBLE);
        etCatNm.setCursorVisible(true);

        etSortNm.setText(sortBy);
        etCatNm.setText("");
        tvAddEditTitle.setText("Add Category");
        tvAdd.setText("Add");

        etCatNm.setVisibility(View.VISIBLE);
        //etSortNm.setVisibility(View.VISIBLE);
        tvMsg.setVisibility(View.GONE);

        loadImg.loadCategoryImg(context, "", ivCatImg);

        path = "";
        if (categoryViewModel != null && categoryViewModel.pricelist_category_image != null) {
            path = categoryViewModel.pricelist_category_image;
        }


        rlAddImg.setVisibility(View.GONE);

        if (isEdit) {
            rlAddImg.setVisibility(View.VISIBLE);

            Utils.showSoftKeyboard(context);
            etCatNm.requestFocus();
            etCatNm.setText(categoryViewModel.pricelistCategoryName);
            etCatNm.setSelection(etCatNm.getText().toString().length());
            etSortNm.setText(categoryViewModel.sortBy);
            tvAddEditTitle.setText("Edit Category");
            tvAdd.setText("Update");
            loadImg.loadCategoryImg(context, path, ivCatImg);

        } else if (isReplace) {

            rlAddImg.setVisibility(View.GONE);

            etCatNm.setVisibility(View.GONE);
            tvAddEditTitle.setText("Replace Category");
            tvAdd.setText("Replace");
            tvMsg.setText("Are you sure you want to replace category?");
            tvMsg.setText(Html.fromHtml(replaceStr));
            tvMsg.setVisibility(View.VISIBLE);
        } else if (isDelete) {
            etCatNm.setVisibility(View.GONE);
            tvAddEditTitle.setText("Delete Category");
            tvAdd.setText("Delete");
            tvMsg.setText("Are you sure you want to delete?");
            tvMsg.setVisibility(View.VISIBLE);
        } else {

            rlAddImg.setVisibility(View.VISIBLE);

            etCatNm.requestFocus();
            Utils.showSoftKeyboard(context);
        }

    }

    CategoryUnderMasterListAdapter categoryUnderMasterListAdapter;
    private ArrayList<CategoryViewModel> alCategory = new ArrayList<>();

    @BindView(R.id.sv)
    NestedScrollView sv;

    boolean isScroll = true;

    String replaceStr = "";
    int fromPos = 0, toPos = 0;

    public void setCategory() {

        categoryUnderMasterListAdapter = new CategoryUnderMasterListAdapter(alCategory, context, new CategoryUnderMasterListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(CategoryViewModel item) {
                isEdit = true;
                categoryViewModel = item;
                showAddCat();
            }

            @Override
            public void onCheckChange(int pos, boolean isChecked) {

                //alCategory.get(pos).isSelect = isChecked;

                for (int i = 0; i < alCategory.size(); i++) {
                    ivDelCat.setVisibility(View.GONE);
                    if (alCategory.get(i).isSelect) {
                        ivDelCat.setVisibility(View.VISIBLE);
                        break;
                    }
                }
            }

            @Override
            public void onDrag(int firstPosition, int secondPosition) {
                fromPos = firstPosition;
                toPos = secondPosition;
                isReplace = true;
                replaceStr = "Are you sure you want to replace <font color='#DA482F'><b>" + alCategory.get(firstPosition).pricelistCategoryName + "</b></font> to <font color='#DA482F'><b>" + alCategory.get(secondPosition).pricelistCategoryName + "</b></font> category?";
                showAddCat();
            }

        });

        final LinearLayoutManager mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rvCat.setLayoutManager(mLayoutManager);
        //rvCat.setItemAnimator(new DefaultItemAnimator());
        rvCat.setAdapter(categoryUnderMasterListAdapter);
        rvCat.setNestedScrollingEnabled(false);


        ItemTouchHelper.Callback callback = new DragTouchTopBottomHelperForCategory(categoryUnderMasterListAdapter, context, new DragTouchTopBottomHelperForCategory.TouchCall() {
            @Override
            public void callDrag(boolean isdrag) {
                if (isdrag) {
                    mSwipyRefreshLayout.setEnabled(false);
                } else {

                    mSwipyRefreshLayout.setEnabled(true);
                }
            }

            @Override
            public void onScroll(final int p) {

            }

            @Override
            public void onSwap(final int from, final int to) {

                categoryUnderMasterListAdapter.swap(from, to);

                /*new Handler().post(new Runnable() {
                    @Override
                    public void run() {

                    }
                });*/


            }
        });
        ItemTouchHelper helper = new ItemTouchHelper(callback);
        helper.attachToRecyclerView(rvCat);


    }

    boolean isDrag = false;
    @BindView(R.id.ivDown)
    ImageView ivDown;

    @BindView(R.id.rvCategory)
    RecyclerView rvCategory;
    CategoryFlowListAdapter categoryFlowListAdapter;

    boolean isLinear = true;

    public void setAllCategoryRv() {

        if (isLinear) {
            ivDown.setRotation(0);
        } else {
            ivDown.setRotation(180);
        }

        categoryFlowListAdapter = new CategoryFlowListAdapter(alCategory, context, new CategoryFlowListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(CategoryViewModel item) {


            }

            @Override
            public void onDrag() {
                isReplace = true;
                showAddCat();
            }
        });

        if (isLinear) {
            rvCategory.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        } else {
            FlowLayoutManager layoutManager = new FlowLayoutManager();
            layoutManager.setAutoMeasureEnabled(true);
            layoutManager.setAlignment(Alignment.LEFT);
            rvCategory.setLayoutManager(layoutManager);
        }
        //

        rvCategory.setItemAnimator(new DefaultItemAnimator());
        rvCategory.setAdapter(categoryFlowListAdapter);
        rvCategory.setNestedScrollingEnabled(false);


        ivDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isLinear = !isLinear;
                setAllCategoryRv();
            }
        });


        ItemTouchHelper.Callback callback = new DragTouchTopBottomHelperForAllFlowCategory(categoryFlowListAdapter, context, new DragTouchTopBottomHelperForAllFlowCategory.TouchCall() {
            @Override
            public void callDrag(boolean isdrag) {
                if (isdrag) {
                    mSwipyRefreshLayout.setEnabled(false);
                } else {
                    mSwipyRefreshLayout.setEnabled(true);
                }
            }
        });
        //ItemTouchHelper helper = new ItemTouchHelper(callback);
        //helper.attachToRecyclerView(rvCategory);


    }


    SwipyRefreshLayout mSwipyRefreshLayout;
    int pagecount = 1;
    boolean isdataavailable = true;

    public void refreshlist() {
        mSwipyRefreshLayout = (SwipyRefreshLayout) findViewById(R.id.swipyrefreshlayout);
        mSwipyRefreshLayout.setDirection(SwipyRefreshLayoutDirection.BOTH);
        mSwipyRefreshLayout.setOnRefreshListener(this);
    }

    @Override
    public void onRefresh(SwipyRefreshLayoutDirection direction) {

        if (direction == SwipyRefreshLayoutDirection.BOTTOM) {
            if (isdataavailable) {
                pagecount++;
                myRequst.page = pagecount;
                callCat();
            } else {
                mSwipyRefreshLayout.setRefreshing(false);
                Toast.makeText(this, "No more data available", Toast.LENGTH_LONG).show();
            }
        }

        if (direction == SwipyRefreshLayoutDirection.TOP) {

            isdataavailable = true;
            pagecount = 1;
            myRequst.page = pagecount;
            alCategory = new ArrayList<>();

            callCat();


        }
    }


    public void callCat() {
        myRequestCall.getCategoryUnderMaster(context, myRequst, new MyRequestCall.CallRequest() {
            @Override
            public void onGetResponse(String response) {
                rl_pb.setVisibility(View.GONE);
                mSwipyRefreshLayout.setRefreshing(false);
                try {
                    JSONObject jObjRes = new JSONObject(response);

                    JSONObject jObjdata = jObjRes.getJSONObject("response");
                    JSONObject jObjResponse = jObjdata.getJSONObject("data");
                    String totalRecord = jObjResponse.getString("totalRecord");
                    tvTitle.setText("Category [" + totalRecord + "]");

                    JSONArray jObjcontacts = jObjResponse.getJSONArray("content");

                    String displayEndRecord = jObjResponse.getString("displayEndRecord");
                    if (totalRecord.equals(displayEndRecord)) {
                        isdataavailable = false;
                    } else {
                        isdataavailable = true;
                    }

                    ArrayList<CategoryViewModel> tmpAlCategory = new ArrayList<>();

                    for (int i = 0; i < jObjcontacts.length(); i++) {
                        if (jObjcontacts.get(i) instanceof JSONObject) {
                            JSONObject jsnObj = (JSONObject) jObjcontacts.get(i);
                            CategoryViewModel obj = new Gson().fromJson(jsnObj.toString(), CategoryViewModel.class);
                            if (myRequst.page == 1 && i == 0) {
                                int sort = 0;
                                try {
                                    sort = Integer.parseInt(obj.sortBy);
                                    sort++;
                                    sortBy = sort + "";
                                } catch (NumberFormatException e) {
                                    e.printStackTrace();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            tmpAlCategory.add(obj);
                        }
                    }

                    Collections.reverse(tmpAlCategory);
                    alCategory.addAll(tmpAlCategory);

                    categoryUnderMasterListAdapter.setitem(alCategory);

                    ivDown.setVisibility(View.GONE);
                    if (alCategory.size() > 5) ivDown.setVisibility(View.VISIBLE);

                    setAllCategoryRv();


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                tv_empty.setVisibility(View.GONE);
                if (alCategory.size() == 0) {
                    tv_empty.setVisibility(View.VISIBLE);
                    sortBy = "1";
                }

            }
        });

    }


    @OnClick(R.id.ivAddCat)
    public void ivAddCat(View v) {
        showAddCat();
    }

    @OnClick(R.id.tvCancel)
    public void tvCancel(View v) {

        hideAddCat();
    }

    @OnClick(R.id.ivCancel)
    public void ivCancel(View v) {

    }

    @BindView(R.id.rl_upload)
    RelativeLayout rl_upload;

    @OnClick(R.id.tvAdd)
    public void tvAdd(View v) {

        if (isReplace) {
            addCategory(fromPos);
            //hideAddCat();
        } else if (isDelete) {
            deleteCategory();
        } else {

            if (path.isEmpty() || path.contains("http")) {
                addCategory(-1);
            } else {

                rl_upload.setVisibility(View.VISIBLE);

                MyRequestCall myRequestCall = new MyRequestCall();

                myRequestCall.uploadCatAwsS3(sessionManager.getcurrentu_nm(), context, 0, path, new MyRequestCall.CallRequest() {
                    @Override
                    public void onGetResponse(String response) {

                        if (!response.isEmpty()) {

                            path = response;
                            addCategory(-1);

                        }
                        rl_upload.setVisibility(View.GONE);
                    }
                });

            }


        }
    }

    @OnClick(R.id.ivDelCat)
    public void ivDelCat(View v) {
        isDelete = true;
        showAddCat();
        //deleteCategory();
    }

    @Override
    public void onBackPressed() {
        if (rl_pb.getVisibility() == View.VISIBLE) {

        } else if (rlAddCategory.getVisibility() == View.VISIBLE) {
            hideAddCat();
        } else {
            super.onBackPressed();
        }
    }


    public void addCategory(final int pos) { // pos fro replace only

        try {
            JSONObject sendobj = new JSONObject();

            if (isEdit) {

                Gson gson = new GsonBuilder().create();
                String json = gson.toJson(categoryViewModel);// obj is your object

                try {

                    JSONArray ja_new = new JSONArray();
                    sendobj.put("newdata", ja_new);

                    JSONArray ja_old = new JSONArray();

                    JSONObject jsonObj = new JSONObject(json);
                    jsonObj.put("pricelist_category_name", etCatNm.getText().toString().trim());
                    jsonObj.put("pricelist_category_image", path);

                    //jsonObj.put("field_json_data",new JSONObject());
                    jsonObj.put("flag", "edit");
                    ja_old.put(jsonObj);
                    sendobj.put("olddata", ja_old);


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            } else if (isReplace) {

                categoryViewModel = alCategory.get(pos);

                Gson gson = new GsonBuilder().create();
                String json = gson.toJson(categoryViewModel);// obj is your object


                try {

                    JSONArray ja_new = new JSONArray();
                    sendobj.put("newdata", ja_new);

                    JSONArray ja_old = new JSONArray();

                    JSONObject jsonObj = new JSONObject(json);
                    if (pos == fromPos) {
                        jsonObj.put("sort_by", alCategory.get(toPos).sortBy);
                    } else {
                        jsonObj.put("sort_by", alCategory.get(fromPos).sortBy);
                    }

                    //jsonObj.put("field_json_data",new JSONObject());
                    jsonObj.put("flag", "edit");
                    ja_old.put(jsonObj);
                    sendobj.put("olddata", ja_old);


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            } else {

                JSONArray jaNewData = new JSONArray();
                JSONObject a1 = new JSONObject();
                try {
                    a1.put("flag", "add");

                    a1.put("pricelist_category_name", etCatNm.getText().toString().trim());
                    a1.put("pricelist_category_image", path);
                    a1.put("sort_by", etSortNm.getText().toString().trim());
                    jaNewData.put(a1);
                    sendobj.put("newdata", jaNewData);

                    JSONArray a = new JSONArray();
                    sendobj.put("olddata", a);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }


            rl_pb.setVisibility(View.VISIBLE);

            JSONObject object = sendobj;
            JsonParser jsonParser = new JsonParser();
            JsonObject gsonObject = (JsonObject) jsonParser.parse(object.toString());

            myRequestCall.addCategoryUnderMaster(context, myRequst, gsonObject, new MyRequestCall.CallRequest() {
                @Override
                public void onGetResponse(String response) {

                    if (!response.isEmpty()) {
                        if (isReplace && pos == fromPos) {
                            addCategory(toPos);
                        } else {
                            Utils.hideSoftKeyboard(context, etCatNm);
                            finish();
                            startActivity(getIntent());
                        }

                    } else {
                        rl_pb.setVisibility(View.GONE);
                    }

                }
            });

        } catch (JsonParseException e) {
            e.printStackTrace();
        }
    }

    public void deleteCategory() {
        rl_pb.setVisibility(View.VISIBLE);

        String dltId = "";
        for (int i = 0; i < alCategory.size(); i++) {
            if (alCategory.get(i).isSelect)
                dltId = dltId + "" + alCategory.get(i).pricelistCategoryId + ",";
        }

        myRequst.deleteIds = dltId;
        myRequestCall.deleteCategoryUnderMaster(context, myRequst, new MyRequestCall.CallRequest() {
            @Override
            public void onGetResponse(String response) {
                if (!response.isEmpty()) {
                    Utils.hideSoftKeyboard(context, etCatNm);
                    finish();
                    startActivity(getIntent());
                } else {
                    rl_pb.setVisibility(View.GONE);
                }

            }
        });

    }


    @BindView(R.id.ivCatImg)
    ImageView ivCatImg;

    @BindView(R.id.rlAddImg)
    RelativeLayout rlAddImg;

    ShowGalleryCamera showGalleryCamera = new ShowGalleryCamera(this);

    @OnClick(R.id.ivCatImg)
    public void ivCatImg(View v) {
        showGalleryCamera.showPictureDialog();
    }

    String path = "";
    LoadImg loadImg = new LoadImg();


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to ak aris

        if (resultCode == RESULT_OK) {
            // Compressing

            showGalleryCamera.onGetResult(requestCode, resultCode, data, new ShowGalleryCamera.CallGetImg() {
                @Override
                public void onGetImg(String str) {
                    // get Compressed
                    path = str;
                    loadImg.loadCategoryImg(context, new File(path).getAbsolutePath(), ivCatImg);
                }
            });
        }


        super.onActivityResult(requestCode, resultCode, data);

    }

    public void loadCatImg() {

    }
}
