package com.cab.digicafe.Activities;

import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.Html;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.andremion.counterfab.CounterFab;
import com.cab.digicafe.Adapter.CatalogTabAdapter;
import com.cab.digicafe.Adapter.CategoryListAdapter;
import com.cab.digicafe.BaseActivity;
import com.cab.digicafe.Dialogbox.CopyCatalogueItem;
import com.cab.digicafe.Dialogbox.CustomDialogClass;
import com.cab.digicafe.Dialogbox.DialogLoading;
import com.cab.digicafe.Dialogbox.FilterDialog;
import com.cab.digicafe.Dialogbox.SaveProfile;
import com.cab.digicafe.Fragments.CatalougeContent;
import com.cab.digicafe.Helper.Inad;
import com.cab.digicafe.Helper.RecyclerCatTabTouchHelper;
import com.cab.digicafe.Helper.SessionManager;
import com.cab.digicafe.Model.Metal;
import com.cab.digicafe.Model.ModelFile;
import com.cab.digicafe.Model.ShareModel;
import com.cab.digicafe.Model.SupplierCategory;
import com.cab.digicafe.MyCustomClass.JsonObjParse;
import com.cab.digicafe.MyCustomClass.RefreshSession;
import com.cab.digicafe.R;
import com.cab.digicafe.Rest.ApiClient;
import com.cab.digicafe.Rest.ApiInterface;
import com.cab.digicafe.Rest.Request.CatRequest;
import com.cab.digicafe.photopicker.activity.PickImageActivity;
import com.cab.digicafe.serviceTypeClass.GetServiceTypeFromBusiUserProfile;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.xiaofeng.flowlayoutmanager.Alignment;
import com.xiaofeng.flowlayoutmanager.FlowLayoutManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import info.hoang8f.android.segmented.SegmentedGroup;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.media.MediaRecorder.VideoSource.CAMERA;

public class CatalougeTabActivity extends BaseActivity implements SearchView.OnQueryTextListener, CatalogTabAdapter.OnItemClickListener, RecyclerCatTabTouchHelper.RecyclerItemTouchHelperListener1, SwipyRefreshLayout.OnRefreshListener {
    private RecyclerView recyclerView;
    private CatalogTabAdapter mAdapter;
    CounterFab counterFab;
    private List<CatalougeContent> orderlist = new ArrayList<>();
    SessionManager sessionManager;
    String sessionString;
    MenuItem item;
    Menu menu;
    String status_code = "4";
    int totalcount = 0;
    String str_actionid = "";
    SegmentedGroup ststussegmented;
    String service_type_of = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.content_catalouge_tab, frameLayout);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Catalogue");
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        rvCategory = (RecyclerView) findViewById(R.id.rvCategory);
        tv_empty = (TextView) findViewById(R.id.tv_empty);
        ivDown = (ImageView) findViewById(R.id.ivDown);
        setCategoryRv();
        sessionManager = new SessionManager(this);

        // listen refresh event
        counterFab = (CounterFab) findViewById(R.id.counter_fab);
        counterFab.setVisibility(View.GONE);
        sessionManager = new SessionManager(this);

        ststussegmented = (SegmentedGroup) findViewById(R.id.segmented2);

        mAdapter = new CatalogTabAdapter(orderlist, this, this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);


        ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new RecyclerCatTabTouchHelper(0, 0, this);
        new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(recyclerView);

        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                // bridgeidval = null;
            } else {
                sessionString = extras.getString("session");
                //  bridgeidval= extras.getString("bridgeidval");

            }
        } else {
            sessionString = (String) savedInstanceState.getSerializable("session");
            // bridgeidval= (String) savedInstanceState.getSerializable("bridgeidval");
        }

        refreshlist();

        findViewById(R.id.rl_pb_cat).setVisibility(View.VISIBLE);
        //usersignin(pagecount);
        findViewById(R.id.rl_pb_cat).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        findViewById(R.id.iv_add).setVisibility(View.VISIBLE);
        findViewById(R.id.iv_add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                callGetBuisenssProfile();

            }
        });

        findViewById(R.id.rl_pb_cat).setVisibility(View.VISIBLE);
        pagecount = 1;
        if (ShareModel.getI().isAddNew) {

        } else {

            if (ShareModel.getI().isCheckStInCatalogue || getST().equalsIgnoreCase(getString(R.string.jewel))) {
                // For only business - catalogue
                new GetServiceTypeFromBusiUserProfile(this, sessionManager.getcurrentu_nm(), new GetServiceTypeFromBusiUserProfile.Apicallback() {
                    @Override
                    public void onGetResponse(String a, String response, String sms_emp) {

                        try {

                            ShareModel.getI().isCheckStInCatalogue = false;

                            JSONObject Jsonresponse = new JSONObject(response);
                            JSONObject Jsonresponseinfo = Jsonresponse.getJSONObject("response");
                            JSONArray jaData = Jsonresponseinfo.getJSONArray("data");
                            if (jaData.length() > 0) {
                                JSONObject data = jaData.getJSONObject(0);
                                String field_json_data = JsonObjParse.getValueEmpty(data.toString(), "field_json_data");
                                String metal_list = JsonObjParse.getValueEmpty(field_json_data, "metal_list");
                                //SharedPrefUserDetail.setString(CatalougeTabActivity.this, SharedPrefUserDetail.metal_list, metal_list);
                                Metal metal = new Gson().fromJson(metal_list, Metal.class);
                                if (metal == null) metal = new Metal();
                                ShareModel.getI().metal = metal;

                            } else {


                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        usersignin(pagecount);
                    }
                });

            } else {
                usersignin(pagecount);
            }

        }
        //DialogLoading.dialogLoading(this);


        String[] PERMISSIONS = {android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_EXTERNAL_STORAGE};
        if (!Inad.hasPermissions(this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, 801);
        } else {
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {

            Inad.exitalert(this);


           /* AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.AlertDialogCustom));
            builder
                    .setMessage(getString(R.string.quit))
                    .setTitle("")
                    .setCancelable(false)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            finish();
                            Intent intent = new Intent(Intent.ACTION_MAIN);
                            intent.addCategory(Intent.CATEGORY_HOME);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        }
                    })
                    .setNegativeButton("No", null)

                    .show();*/
        }
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {
        if (viewHolder instanceof CatalogTabAdapter.MyViewHolder) {
            final CatalougeContent deletedItem = orderlist.get(viewHolder.getAdapterPosition());
            //mAdapter.removeItem(viewHolder.getAdapterPosition());

            if (direction == ItemTouchHelper.LEFT) {


                new SaveProfile(CatalougeTabActivity.this, new SaveProfile.OnDialogClickListener() {
                    @Override
                    public void onDialogImageRunClick(int positon, String add) {

                        if (positon == 0) {

                            mAdapter.setitem(orderlist, filter);


                        } else if (positon == 1) {

                            findViewById(R.id.rl_pb_cat).setVisibility(View.VISIBLE);

                            // out of stock
                            Gson gson = new GsonBuilder().create();
                            String json = gson.toJson(deletedItem);// obj is your object

                            try {

                                JSONObject jo = new JSONObject();
                                JSONArray ja_new = new JSONArray();
                                jo.put("newdata", ja_new);
                                JSONArray ja_old = new JSONArray();


                                JSONObject jsonObj = new JSONObject(json);
                                String stock = jsonObj.getString("out_of_stock");
                                if (stock.equalsIgnoreCase("yes")) {
                                    jsonObj.put("out_of_stock", "No");
                                } else {
                                    jsonObj.put("out_of_stock", "Yes");
                                }

                                //jsonObj.put("field_json_data",new JSONObject());

                                ja_old.put(jsonObj);
                                jo.put("olddata", ja_old);


                                CatRequest catalougeContent = new CatRequest();

                                catalougeContent = new Gson().fromJson(jo.toString(), CatRequest.class);

                                swipe(catalougeContent);
                            } catch (JSONException e) {
                                e.printStackTrace();
                                findViewById(R.id.rl_pb_cat).setVisibility(View.GONE);
                            }
                        }
                    }
                }, deletedItem.getOutOfStock()).show();
            }
        }
    }


    @Override
    public void onRefresh(SwipyRefreshLayoutDirection direction) {

        mSwipyRefreshLayout.setRefreshing(false);

        if (direction == SwipyRefreshLayoutDirection.TOP) {


            orderlist = new ArrayList<>();


            if (issearching) {
                issearchdata = true;
                searchpagectr = 1;
                searchMytask(sessionString, q_string, new MyCallback() {
                    @Override
                    public void callbackCall(List<CatalougeContent> filteredModelList) {
                        mAdapter.setitem(filteredModelList, filter);
                    }
                });
            } else {
                isdataavailable = true;
                pagecount = 1;
                usersignin(pagecount);

            }


        } else if (direction == SwipyRefreshLayoutDirection.BOTTOM) {
            Log.e("scroll direction", "bottom");

            if (issearching && issearchdata) {
                searchpagectr++;
                searchMytask(sessionString, q_string, new MyCallback() {
                    @Override
                    public void callbackCall(List<CatalougeContent> filteredModelList) {
                        mAdapter.setitem(filteredModelList, filter);
                    }
                });
            } else if (issearching) {
                Toast.makeText(CatalougeTabActivity.this, "No more data available", Toast.LENGTH_LONG).show();
            } else if (!issearching && isdataavailable) {
                pagecount++;
                usersignin(pagecount);

            } else {
                Toast.makeText(CatalougeTabActivity.this, "No more data available", Toast.LENGTH_LONG).show();
            }

        }


    }


    SwipyRefreshLayout mSwipyRefreshLayout;
    int pagecount = 1;
    boolean isdataavailable = true;
    int pagesize = 50;

    public void refreshlist() {
        mSwipyRefreshLayout = (SwipyRefreshLayout) findViewById(R.id.swipyrefreshlayout);
        mSwipyRefreshLayout.setOnRefreshListener(this);
    }

    private List<CatalougeContent> filter(List<CatalougeContent> models, String query) {
        query = query.toLowerCase();
        final List<CatalougeContent> filteredModelList = new ArrayList<>();
        for (CatalougeContent model : models) {
            final String text = model.getName().toLowerCase();
            if (text.contains(query)) {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;
    }


    String category = "";
    String order_by = "asc";
    String order = "name";

    public void usersignin(final int pagecount) {
        ApiInterface apiService = ApiClient.getClient(CatalougeTabActivity.this).create(ApiInterface.class);
        Call call = apiService.getCatalouge(sessionManager.getsession(), pagecount, out_of_stock, category, order_by, order);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                findViewById(R.id.rl_pb_cat).setVisibility(View.GONE);
                DialogLoading.dialogLoading_Dismiss();
                mSwipyRefreshLayout.setRefreshing(false);
                if (response.isSuccessful()) {
                    try {
                        String a = new Gson().toJson(response.body());
                        Log.e("catalogresponse", a);
                        JSONObject jObjRes = new JSONObject(a);
                        JSONObject jObjdata = jObjRes.getJSONObject("response");
                        JSONObject jObjResponse = jObjdata.getJSONObject("data");
                        String totalRecord = jObjResponse.getString("totalRecord");
                        ShareModel.getI().total_catalog = Integer.parseInt(totalRecord);
                        String displayEndRecord = jObjResponse.getString("displayEndRecord");
                        JSONArray jObjcontacts = jObjResponse.getJSONArray("content");
                        /*if (jObjcontacts.length() < pagesize) {
                            isdataavailable = false;
                        } else {
                            isdataavailable = true;
                        }*/
                        if (totalRecord.equals(displayEndRecord)) {
                            isdataavailable = false;
                        } else {
                            isdataavailable = true;
                        }
                        if (pagecount == 1) {
                            orderlist = new ArrayList<CatalougeContent>();
                        }

                        boolean isSurvey = false;

                        if (getST().equalsIgnoreCase(getString(R.string.survey))) {
                            isSurvey = true;
                        }

                        for (int i = 0; i < jObjcontacts.length(); i++) {
                            if (jObjcontacts.get(i) instanceof JSONObject) {
                                JSONObject jsnObj = (JSONObject) jObjcontacts.get(i);
                                CatalougeContent obj = new Gson().fromJson(jsnObj.toString(), CatalougeContent.class);
                                if (isSurvey) {
                                    try {
                                        //String survey = JsonObjParse.getValueEmpty(obj.getInternalJsonData(), "survey");
                                        //String surveyQPos = JsonObjParse.getValueEmpty(survey, "SurveyQPos");
                                        if (obj.getCrossReference().isEmpty()) obj.sortOrder = 0;
                                        else
                                            obj.sortOrder = Integer.parseInt(obj.getCrossReference());
                                    } catch (NumberFormatException e) {
                                        e.printStackTrace();
                                    }

                                }
                                orderlist.add(obj);
                            }
                        }

                        if (isSurvey && orderlist.size() > 0) {

                            Collections.sort(orderlist, new Comparator<CatalougeContent>() {
                                @Override
                                public int compare(CatalougeContent p1, CatalougeContent p2) {
                                    return p1.getSortOrder() - p2.getSortOrder(); // Ascending
                                }
                            });

                        }

                        String title = "";

                        switch (filter) {
                            case "all":
                                //title = "Catalogue [" + totalRecord + "]";
                                title = "Catlg [" + totalRecord + "]";
                                break;
                            case "avail":
                                //title = "Available [" + totalRecord + "]";
                                title = "Avlbl [" + totalRecord + "]";
                                break;
                            case "notavail":
                                //title = "Unavailable [" + totalRecord + "]";
                                title = "UnAvlbl [" + totalRecord + "]";
                                break;
                            default:
                                //title = "Catalogue [" + totalRecord + "]";
                                title = "Catlg [" + totalRecord + "]";
                                break;
                        }

                        if (orderlist.size() == 0) {
                            tv_empty.setVisibility(View.VISIBLE);
                        } else tv_empty.setVisibility(View.GONE);

                        //getSupportActionBar().setTitle(title);
                        getSupportActionBar().setTitle(Html.fromHtml("<small>" + title + "</small>"));

                        mAdapter.setitem(orderlist, filter);

                        if (alCategory.size() == 0) {
                            alCategory = new ArrayList<>();
                            if (jObjResponse.has("category")) {
                                JSONArray ja_category = jObjResponse.getJSONArray("category");
                                if (ja_category.length() > 0)
                                    alCategory.add(new SupplierCategory("All"));
                                for (int i = 0; i < ja_category.length(); i++) {
                                    if (ja_category.get(i) instanceof JSONObject) {
                                        JSONObject jsnObj = (JSONObject) ja_category.get(i);
                                        SupplierCategory obj = new Gson().fromJson(jsnObj.toString(), SupplierCategory.class);
                                        alCategory.add(obj);
                                    }
                                }
                                if (alCategory == null) alCategory = new ArrayList<>();
                                if (alCategory.size() > 0) {
                                    isCat = true;
                                    ivDown.setVisibility(View.GONE);
                                    if (alCategory.size() > 5) ivDown.setVisibility(View.VISIBLE);

                                    rvCategory.setVisibility(View.VISIBLE);
                                    alCategory.get(0).setChecked(true);
                                    categoryListAdapter.setitem(alCategory);
                                } else {
                                    isCat = false;
                                    rvCategory.setVisibility(View.GONE);
                                }


                                ShareModel.getI().alCategory = new Gson().toJson(alCategory, new TypeToken<ArrayList<SupplierCategory>>() {
                                }.getType());


                            } else {
                                rvCategory.setVisibility(View.GONE);
                            }
                        }

                        //mAdapter.notifyDataSetChanged();
                    } catch (Exception e) {


                        Toast.makeText(CatalougeTabActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }

                } else {


                    switch (response.code()) {
                        case 401:
                            new RefreshSession(CatalougeTabActivity.this, sessionManager);  // session expired
                            break;
                        case 404:
                            Toast.makeText(CatalougeTabActivity.this, "not found", Toast.LENGTH_SHORT).show();
                            break;
                        case 500:
                            Toast.makeText(CatalougeTabActivity.this, "server broken", Toast.LENGTH_SHORT).show();
                            break;
                        default:
                            Toast.makeText(CatalougeTabActivity.this, "unknown error", Toast.LENGTH_SHORT).show();
                            break;
                    }


                    try {
                    } catch (Exception e) {
                        Toast.makeText(CatalougeTabActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }

                //  List<Movie> movies = response.body().getResults();
                //  recyclerView.setAdapter(new MoviesAdapter(movies, R.layout.list_item_movie, getApplicationContext()));
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // Log error here since request failed
                findViewById(R.id.rl_pb_cat).setVisibility(View.GONE);
                mSwipyRefreshLayout.setRefreshing(false);

            }
        });
    }

    public void swipe(CatRequest jo_cat) {
        ApiInterface apiService =
                ApiClient.getClient(CatalougeTabActivity.this).create(ApiInterface.class);

        Call call = apiService.SwipeCatalogue(sessionManager.getsession(), jo_cat);

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {

                mSwipyRefreshLayout.setRefreshing(false);
                if (response.isSuccessful()) {
                    try {


                        if (issearching) {

                            searchpagectr = 1;
                            searchMytask(sessionString, q_string, new MyCallback() {
                                @Override
                                public void callbackCall(List<CatalougeContent> filteredModelList) {
                                    mAdapter.setitem(filteredModelList, filter);
                                }
                            });
                        } else {

                            pagecount = 1;
                            usersignin(pagecount);

                        }


                        //mAdapter.notifyDataSetChanged();
                    } catch (Exception e) {

                        findViewById(R.id.rl_pb_cat).setVisibility(View.GONE);
                        Toast.makeText(CatalougeTabActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }

                } else {

                    findViewById(R.id.rl_pb_cat).setVisibility(View.GONE);
                    try {
                    } catch (Exception e) {
                        Toast.makeText(CatalougeTabActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }

            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // Log error here since request failed
                mSwipyRefreshLayout.setRefreshing(false);
                findViewById(R.id.rl_pb_cat).setVisibility(View.GONE);

            }
        });
    }

    public boolean onQueryTextChange(String newText) {

        issearching = false;

        if (newText.equalsIgnoreCase("")) {

            searchpagectr = 1;
            //List<CatalougeContent> filteredModelList = filter(orderlist, newText);
            //mAdapter.setitem(filteredModelList, filter);

            pagecount = 1;
            usersignin(pagecount);


            return true;
        }

        if (newText.length() == 4 || newText.length() == 0) {

        } else {
            return true;
        }


        searchMytask(sessionString, newText, new MyCallback() {
            @Override
            public void callbackCall(List<CatalougeContent> filteredModelList) {
                issearching = true;
                mAdapter.setitem(filteredModelList, filter);
            }
        });


        return true;
    }

    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    boolean isOdrAlternate = true; // order name - desc, asc, Created date - desc, asc

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        if (id == R.id.action_share) {

            String bridge_id = sessionManager.getE_bridgeid();
            String username = sessionManager.getcurrentu_nm();
            String shareUrl = getString(R.string.BASEURL) + "request-form/" + username + "/" + bridge_id + "/INR";

            Intent share = new Intent(android.content.Intent.ACTION_SEND);
            share.setType("text/plain");
            share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);

            // Add data to the intent, the receiving app will decide
            // what to do with it.
            share.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name));
            share.putExtra(Intent.EXTRA_TEXT, shareUrl);
            startActivity(Intent.createChooser(share, "Share"));


        }

        if (id == R.id.action_sort) {

            isOdrAlternate = !isOdrAlternate;
            if (isOdrAlternate) {
                if (order.equals("created_date")) order = "name";
                else order = "created_date";
            }

            if (order_by.equals("asc")) {

                order_by = "desc";
                action_sort.setIcon(R.drawable.ic_sort_asc);
            } else {

                order_by = "asc";
                action_sort.setIcon(R.drawable.ic_sort_desc);
            }
            loadFst();

        }

        if (id == R.id.action_delete) {
            new CustomDialogClass(CatalougeTabActivity.this, new CustomDialogClass.OnDialogClickListener() {
                @Override
                public void onDialogImageRunClick(int positon) {
                    if (positon == 0) {

                    } else if (positon == 1) {
                        //str_actionid = str_actionid.replaceAll(",$", "");
                        Log.e("str_actionid", str_actionid);

                    }
                }
            }, "Are you sure you want to add " + totalcount + " chit in archived?").show();
        }

        if (id == R.id.action_filter) {
            new FilterDialog(CatalougeTabActivity.this, new FilterDialog.OnDialogClickListener() {

                @Override
                public void onDialogImageRunClick(int pos, String add) {


                    if (!filter.equals(add) && pos == 1) {
                        findViewById(R.id.rl_pb_cat).setVisibility(View.VISIBLE);
                        filter = add;

                        pagecount = 1;

                        switch (add) {
                            case "all":
                                out_of_stock = "";
                                break;
                            case "avail":
                                out_of_stock = "No";
                                break;
                            case "notavail":
                                out_of_stock = "Yes";
                                break;
                            default:
                                out_of_stock = "";
                                break;
                        }

                        usersignin(pagecount);


                        //mAdapter.setitem(orderlist,filter);
                    }


                }
            }, filter).show();
        }

        return super.onOptionsItemSelected(item);
    }

    MenuItem action_sort;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);


        final MenuItem item = menu.findItem(R.id.action_search);

        MenuItem item1 = menu.findItem(R.id.action_filter);
        item1.setVisible(true);

        action_sort = menu.findItem(R.id.action_sort);
        action_sort.setVisible(true);

        if (ShareModel.getI().isAddNew) {
            isAddNewProcuct();
            usersignin(pagecount);
        } else {

        }


        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        //searchView.setInputType(InputType.TYPE_CLASS_NUMBER);
        searchView.setOnQueryTextListener(this);

        MenuItemCompat.setOnActionExpandListener(item,
                new MenuItemCompat.OnActionExpandListener() {
                    @Override
                    public boolean onMenuItemActionCollapse(MenuItem item) {
// Do something when collapsed
                        mAdapter.setFilter(orderlist);
                        return true; // Return true to collapse action view
                    }

                    @Override
                    public boolean onMenuItemActionExpand(MenuItem item) {
// Do something when expanded
                        return true; // Return true to expand action view
                    }
                });

        MenuItem action_share = menu.findItem(R.id.action_share);
        action_share.setVisible(true);


        return true;
    }

    String filter = "all";

    public List<CatalougeContent> searchMytask(String usersession, String q, final MyCallback listener) {

        q_string = q;
        final List<CatalougeContent> filteredModelList = new ArrayList<>();

        ApiInterface apiService =
                ApiClient.getClient(CatalougeTabActivity.this).create(ApiInterface.class);
        Call call = apiService.SearchCatalogue(usersession, searchpagectr, q, out_of_stock, category, order_by, order);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                findViewById(R.id.rl_pb_cat).setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    try {
                        String a = new Gson().toJson(response.body());
                        Log.e("catalogresponse", a);
                        JSONObject jObjRes = new JSONObject(a);
                        JSONObject jObjdata = jObjRes.getJSONObject("response");
                        JSONObject jObjResponse = jObjdata.getJSONObject("data");
                        String totalRecord = jObjResponse.getString("totalRecord");
                        JSONArray jObjcontacts = jObjResponse.getJSONArray("content");

                        String displayEndRecord = jObjResponse.getString("displayEndRecord");

                        if (totalRecord.equals(displayEndRecord)) {
                            issearchdata = false;
                        } else {
                            issearchdata = true;
                        }


                        for (int i = 0; i < jObjcontacts.length(); i++) {
                            if (jObjcontacts.get(i) instanceof JSONObject) {
                                JSONObject jsnObj = (JSONObject) jObjcontacts.get(i);

                                CatalougeContent obj = new Gson().fromJson(jsnObj.toString(), CatalougeContent.class);
                                filteredModelList.add(obj);
                            }
                        }

                        orderlist = new ArrayList<>();
                        orderlist.addAll(filteredModelList);


                        listener.callbackCall(filteredModelList);


                    } catch (Exception e) {


                        Toast.makeText(CatalougeTabActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }

                } else {


                    try {
                    } catch (Exception e) {
                        Toast.makeText(CatalougeTabActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }

            }

            @Override
            public void onFailure(Call call, Throwable t) {
                findViewById(R.id.rl_pb_cat).setVisibility(View.GONE);
                // Log error here since request failed  mSwipyRefreshLayout.setRefreshing(false);
                Log.e("error message", t.toString());
            }
        });
        return filteredModelList;

    }

    interface MyCallback {
        void callbackCall(List<CatalougeContent> filteredModelList);
    }

    private MyCallback listener;


    String out_of_stock = "";
    int searchpagectr = 1;
    boolean issearching = false;
    boolean issearchdata = true;
    String q_string = "";


    @Override
    protected void onResume() {
        super.onResume();


    }


    /********************************  save as  **************************************************/
    CopyCatalogueItem copyCatalogueItem;

    @Override
    public void onItemLongClick(CatalougeContent item) {

        /*
         copyCatalogueItem = new CopyCatalogueItem(CatalougeTabActivity.this, new CopyCatalogueItem.OnDialogClickListener() {
            @Override
            public void onDialogImageRunClick(int positon, String add) {

                if (positon == 0) {

                } else if (positon == 1) {

                }

            }

            @Override
            public void onAddImg() {
                showPictureDialog();
            }

             @Override
             public void onSave() {

                 findViewById(R.id.rl_pb_cat).setVisibility(View.VISIBLE);
                 pagecount = 1;
                 usersignin(pagecount);

             }

             @Override
             public void onImgClick(List<ModelFile> data, int pos) {
                 ArrayList<ModelFile> al_img = new ArrayList<>();
                 al_img.addAll(data);
                 String imgarray = new Gson().toJson(al_img);



                 Intent intent = new Intent(CatalougeTabActivity.this, ImagePreviewActivity.class);
                 intent.putExtra("imgarray", imgarray);
                 intent.putExtra("pos", pos);
                 intent.putExtra("isdrag", false);
                 startActivityForResult(intent, 11);
             }

         }, item);
         copyCatalogueItem.show();

*/

        ShareModel.getI().dbUnderMaster = ""; // unhide edit delete

        Intent i = new Intent(CatalougeTabActivity.this, CatalogeEditDataActivity.class);
        Gson gson = new Gson();
        String json = gson.toJson(item);
        i.putExtra("CatalougeContent", json);
        i.putExtra("isadd", false);
        i.putExtra("isCopy", true);

        startActivityForResult(i, 21);

    }

    @Override
    public void onItemClick(CatalougeContent item) {
        ShareModel.getI().dbUnderMaster = ""; // unhide edit delete

        Intent i = new Intent(CatalougeTabActivity.this, CatalogeEditDataActivity.class);
        Gson gson = new Gson();
        String json = gson.toJson(item);
        i.putExtra("CatalougeContent", json);
        i.putExtra("isadd", false);

        startActivityForResult(i, 21);
    }

    @Override
    public void onImgClick(CatalougeContent item) {

        String imageJsonData = item.getImageJsonData();
        ArrayList<ModelFile> al_img = new ArrayList<>();

        try {
            JSONArray ja_img = new JSONArray(imageJsonData.toString());
            for (int i = 0; i < ja_img.length(); i++) {
                al_img.add(new ModelFile(ja_img.get(i).toString(), false));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        //GalleryActivity.images = al_img;
        //Intent i = new Intent(CatalougeTabActivity.this, GalleryActivity.class);
        //startActivity(i);

        String imgarray = new Gson().toJson(al_img);

        Gson gson = new Gson();
        String json = gson.toJson(item);

        Intent intent = new Intent(CatalougeTabActivity.this, ImagePreviewActivity.class);
        intent.putExtra("imgarray", imgarray);
        intent.putExtra("pos", 0);
        intent.putExtra("isdrag", true);
        intent.putExtra("CatalougeContent", json);

        startActivityForResult(intent, 21);
    }

    @Override
    public void onadditem(CatalougeContent qty) {

    }

    @Override
    public void ondecreasecount(CatalougeContent qty) {

    }

    private void showPictureDialog() {
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(this);
        String[] pictureDialogItems = {"Photo Gallery", "Camera"};
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                // openImagePickerIntent();

                              /*  Intent i = new Intent(CatalougeTabActivity.this, FileActivity.class);
                                i.putExtra("isdoc", false);
                                startActivityForResult(i, 8);*/

                                Intent mIntent = new Intent(CatalougeTabActivity.this, PickImageActivity.class);
                                mIntent.putExtra(PickImageActivity.KEY_LIMIT_MAX_IMAGE, 30);
                                mIntent.putExtra(PickImageActivity.KEY_LIMIT_MIN_IMAGE, 1);
                                startActivityForResult(mIntent, 98);


                               /* new FilePicker(CatalogeEditDataActivity.this, new FilePicker.OnDialogClickListener() {
                                    @Override
                                    public void onDialogImageRunClick(int pos, boolean isdoc) {

                                        Intent i = new Intent(CatalogeEditDataActivity.this, FileActivity.class);
                                        i.putExtra("isdoc", isdoc);
                                        startActivityForResult(i, 8);


                                    }
                                }).show();*/


                                break;
                            case 1:
                                cameraIntent();
                                break;
                        }
                    }
                });
        pictureDialog.show();
    }

    private void cameraIntent() {

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        this.screen_width = size.x;
        this.screen_height = size.y;

        File root = new File(Environment.getExternalStorageDirectory()
                + File.separator + getString(R.string.app_name) + File.separator + "Capture" + File.separator);
        root.mkdirs();
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        f3f = new File(root, "IMG" + timeStamp + ".jpg");

        try {
            imageUri = FileProvider.getUriForFile(
                    CatalougeTabActivity.this, getApplicationContext()
                            .getPackageName() + ".provider", this.f3f);
        } catch (Exception e) {
            Toast.makeText(this, "Please check SD card! Image shot is impossible!", Toast.LENGTH_SHORT).show();
        }

        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        intent.putExtra("output", imageUri);
        try {
            startActivityForResult(intent, CAMERA);
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
        }

    }

    ExifInterface exif;
    File f3f;
    int screen_height;
    int screen_width;
    private Uri imageUri;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to ak aris
        if (resultCode == -1 && requestCode == CAMERA) {


            Bitmap bitmap;
            int orientation;
            String albumnm = "";


            try {
                bitmap = BitmapFactory.decodeFile(this.f3f.getPath());
                this.exif = new ExifInterface(this.f3f.getPath());
                if (bitmap != null) {


                    String picturePath = f3f.getPath();
                    al_selet = new ArrayList<>();
                    al_selet.add(new ModelFile(picturePath, true));
                    copyCatalogueItem.onAddedImg(al_selet);


                } else {
                    Toast.makeText(getApplicationContext(), "Picture not taken !", Toast.LENGTH_SHORT).show();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }


        } else if (resultCode == -1 && requestCode == 8) {
            String picturePath = data.getExtras().getString("al_selet");
            al_selet = new Gson().fromJson(picturePath, new TypeToken<ArrayList<ModelFile>>() {
            }.getType());
            copyCatalogueItem.onAddedImg(al_selet);
        } else if (requestCode == 98 && resultCode == -1) {

            ArrayList<ModelFile> al_temp_selet = new ArrayList<>();
            ArrayList<String> pathList = data.getExtras().getStringArrayList(PickImageActivity.KEY_DATA_RESULT);
            if (pathList != null && !pathList.isEmpty()) {
                for (int i = 0; i < pathList.size(); i++) {
                    al_temp_selet.add(new ModelFile(pathList.get(i), true));
                }
            }

            al_temp_selet.addAll(al_selet);

            al_selet = new ArrayList<>();
            al_selet.addAll(al_temp_selet);
            copyCatalogueItem.onAddedImg(al_selet);


        } else if (requestCode == 21) {
           /* String picturePath = data.getExtras().getString("al_selet");
            ArrayList<ModelFile> al_temp_selet = new Gson().fromJson(picturePath, new TypeToken<ArrayList<ModelFile>>() {
            }.getType());
            al_temp_selet.addAll(al_selet);
            selectFileAdapter.setItem(al_temp_selet);
            al_selet = new ArrayList<>();
            al_selet.addAll(al_temp_selet);*/

            if (resultCode == RESULT_OK) {
                findViewById(R.id.rl_pb_cat).setVisibility(View.VISIBLE);
                pagecount = 1;
                usersignin(pagecount);
            }

        } else if (resultCode == RESULT_OK && requestCode == 22) {
           /* String picturePath = data.getExtras().getString("al_selet");
            ArrayList<ModelFile> al_temp_selet = new Gson().fromJson(picturePath, new TypeToken<ArrayList<ModelFile>>() {
            }.getType());
            al_temp_selet.addAll(al_selet);
            selectFileAdapter.setItem(al_temp_selet);
            al_selet = new ArrayList<>();
            al_selet.addAll(al_temp_selet);*/


            boolean isAddNew = data.getExtras().getBoolean("isAddNew");
            if (isAddNew) {
                isAddNewProcuct();
            }

            findViewById(R.id.rl_pb_cat).setVisibility(View.VISIBLE);
            pagecount = 1;
            usersignin(pagecount);


        } else if (resultCode == -1 && requestCode == 11) {
         /*   String picturePath = data.getExtras().getString("al_selet");
            al_selet = new Gson().fromJson(picturePath, new TypeToken<ArrayList<ModelFile>>() {
            }.getType());
            copyCatalogueItem.onDragImg(al_selet);*/
        }


        super.onActivityResult(requestCode, resultCode, data);

    }

    ArrayList<ModelFile> al_selet = new ArrayList<>();
    TextView tv_empty;
    ImageView ivDown;
    RecyclerView rvCategory;
    ArrayList<SupplierCategory> alCategory = new ArrayList<>();
    CategoryListAdapter categoryListAdapter;

    boolean isLinear = true;

    public void setCategoryRv() {

        if (isLinear) {
            ivDown.setRotation(0);
        } else {
            ivDown.setRotation(180);
        }

        categoryListAdapter = new CategoryListAdapter(alCategory, CatalougeTabActivity.this, new CategoryListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(SupplierCategory item) {

                category = item.getPricelistCategoryName();
                if (category.equalsIgnoreCase("all")) category = "";

                loadFst();


            }
        });

        if (isLinear) {
            rvCategory.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        } else {
            FlowLayoutManager layoutManager = new FlowLayoutManager();
            layoutManager.setAutoMeasureEnabled(true);
            layoutManager.setAlignment(Alignment.LEFT);
            rvCategory.setLayoutManager(layoutManager);
        }
        //

        rvCategory.setItemAnimator(new DefaultItemAnimator());
        rvCategory.setAdapter(categoryListAdapter);
        rvCategory.setNestedScrollingEnabled(false);


        ivDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isLinear = !isLinear;
                setCategoryRv();
            }
        });

    }

    public void loadFst() {
        if (issearching) {
            issearchdata = true;
            searchpagectr = 1;
            searchMytask(sessionString, q_string, new MyCallback() {
                @Override
                public void callbackCall(List<CatalougeContent> filteredModelList) {
                    mAdapter.setitem(filteredModelList, filter);
                }
            });
        } else {
            isdataavailable = true;
            pagecount = 1;
            usersignin(pagecount);

        }

        DialogLoading.dialogLoading(CatalougeTabActivity.this);
    }

    public static boolean isCat = false;

    String symbol_native = "", currency_code_id = "";

    public void callGetBuisenssProfile() {

        findViewById(R.id.rl_pb_cat).setVisibility(View.VISIBLE);

        ApiInterface apiService =
                ApiClient.getClient(this).create(ApiInterface.class);
        Call call;

        call = apiService.getBusinessProfile(sessionManager.getsession());

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                findViewById(R.id.rl_pb_cat).setVisibility(View.GONE);

                if (response.isSuccessful()) {

                    String a = new Gson().toJson(response.body());
                    Log.e("response", a);
                    try {
                        JSONObject jObjRes = new JSONObject(a);
                        JSONObject jObjdata = jObjRes.getJSONObject("response");
                        JSONObject jObjResponse = jObjdata.getJSONObject("data");
                        symbol_native = jObjResponse.getString("symbol_native");
                        currency_code_id = jObjResponse.getString("currency_code_id");

                        String field_json_data = JsonObjParse.getValueEmpty(jObjResponse.toString(), "field_json_data");
                        String service_type_of = JsonObjParse.getValueEmpty(field_json_data, "service_type_of");


                        ShareModel.getI().dbUnderMaster = "";

                        if (service_type_of.equalsIgnoreCase(getString(R.string.dental))) {
                           /* Intent i = new Intent(CatalougeTabActivity.this, SelectTemplateActivity.class);
                            startActivity(i);*/
                            CatalougeContent item = new CatalougeContent();
                            Intent i = new Intent(CatalougeTabActivity.this, CatalogeEditDataActivity.class);
                            Gson gson = new Gson();
                            String json = gson.toJson(item);
                            i.putExtra("CatalougeContent", json);
                            i.putExtra("isadd", true);
                            i.putExtra("currency_code_id", currency_code_id);
                            i.putExtra("symbol_native", symbol_native);
                            startActivityForResult(i, 22);
                        } else {
                            CatalougeContent item = new CatalougeContent();
                            Intent i = new Intent(CatalougeTabActivity.this, CatalogeEditDataActivity.class);
                            Gson gson = new Gson();
                            String json = gson.toJson(item);
                            i.putExtra("CatalougeContent", json);
                            i.putExtra("isadd", true);
                            i.putExtra("currency_code_id", currency_code_id);
                            i.putExtra("symbol_native", symbol_native);
                            startActivityForResult(i, 22);
                        }
                        //obj = new Gson().fromJson(jObjResponse.toString(), CatalougeContent.class);


                    } catch (JSONException e) {

                    }

                } else {

                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                findViewById(R.id.rl_pb_cat).setVisibility(View.GONE);

            }
        });
    }

    public void isAddNewProcuct() {
        isOdrAlternate = !isOdrAlternate;
        order = "created_date";
        order_by = "desc";
        action_sort.setIcon(R.drawable.ic_sort_asc);
        ShareModel.getI().isAddNew = false;
    }
}