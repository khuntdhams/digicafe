package com.cab.digicafe.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cab.digicafe.Helper.SessionManager;
import com.cab.digicafe.R;
import com.cab.digicafe.Rest.ApiClient;
import com.cab.digicafe.Rest.ApiInterface;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LogoActivity extends MyAppBaseActivity {

    @BindView(R.id.iv_complany)
    ImageView iv_complany;

    @BindView(R.id.tv_comp_name)
    TextView tv_comp_name;

    @BindView(R.id.tv_shortdesc)
    TextView tv_shortdesc;

    @BindView(R.id.tv_location)
    TextView tv_location;

    @BindView(R.id.tv_mobile)
    TextView tv_mobile;

    @BindView(R.id.rl_pb)
    RelativeLayout rl_pb;
    boolean isstatic = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logo);
        sessionManager = new SessionManager(this);
        ButterKnife.bind(this);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        Bundle extras = getIntent().getExtras();
        if (extras == null) {

        } else {
            if (extras.containsKey("isstatic")) {
                isstatic = extras.getBoolean("isstatic");
            }

        }


        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (isstatic) {
            findViewById(R.id.ll_policy).setVisibility(View.GONE);
            iv_complany.setImageResource(R.mipmap.bridgelogo);

            tv_comp_name.setText("JJIFFEE TECHNOLOGIES (PVT) LTD");
            tv_location.setText("");
            tv_location.setVisibility(View.GONE);
            tv_mobile.setText("+91 9840348857");
            tv_shortdesc.setText(Html.fromHtml("An e-platform with built in <b>Collaborative Business Process</b> and <b>Work Flow</b> management capability."));

        } else {
            findViewById(R.id.ll_policy).setVisibility(View.VISIBLE);
            getuserprofile();
        }

        findViewById(R.id.ll_pp).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LogoActivity.this, WebViewActivity.class);
                i.putExtra("url", "https://s3-ap-southeast-1.amazonaws.com/cab-policy/privacy_statement.pdf");
                i.putExtra("title", "Privacy Policy");
                startActivity(i);

            }
        });

        findViewById(R.id.ll_rp).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LogoActivity.this, WebViewActivity.class);
                i.putExtra("url", "https://s3-ap-southeast-1.amazonaws.com/cab-policy/refund_policy.pdf");
                i.putExtra("title", "Refund Policy");
                startActivity(i);
            }
        });

        findViewById(R.id.ll_tc).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LogoActivity.this, WebViewActivity.class);
                i.putExtra("url", "https://s3-ap-southeast-1.amazonaws.com/cab-policy/terms_of_service.pdf");
                i.putExtra("title", "Terms of Service");
                startActivity(i);
            }
        });

    }

    public void getuserprofile() {
        ApiInterface apiService =
                ApiClient.getClient(LogoActivity.this).create(ApiInterface.class);
        Call call;

        call = apiService.getagregatorProfile(sessionManager.getAggregator_ID());

        rl_pb.setVisibility(View.VISIBLE);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                rl_pb.setVisibility(View.GONE);

                if (response.isSuccessful()) {
                    String a = new Gson().toJson(response.body());
                    Log.e("response", a);
                    try {
                        JSONObject jObjRes = new JSONObject(a);
                        JSONObject jObjdata = jObjRes.getJSONObject("response");
                        JSONArray aggregatorobj = jObjdata.getJSONArray("data");

                        JSONObject aggregator = aggregatorobj.getJSONObject(0);

                        String company_image = aggregator.getString("company_image");
                        String company_name = aggregator.getString("company_name");
                        String company_detail_short = aggregator.getString("company_detail_short");
                        String location = aggregator.getString("location");
                        String contact_no = aggregator.getString("contact_no");

                        String endurl = "assets/uploadimages/";

                        String url = getString(R.string.BASEURL) + endurl + company_image;

                        if (company_image.equalsIgnoreCase("")) {
                            Picasso.with(LogoActivity.this)
                                    .load(R.mipmap.launcher)
                                    .into(iv_complany);
                        } else {
                            Picasso.with(LogoActivity.this)
                                    .load(url)
                                    .into(iv_complany);
                        }

                        tv_comp_name.setText(company_name);
                        tv_location.setText(location);
                        tv_mobile.setText(contact_no);
                        tv_shortdesc.setText(company_detail_short);

                        if (company_detail_short.isEmpty()) {
                            tv_shortdesc.setVisibility(View.GONE);
                        }
                    } catch (JSONException e) {

                    }

                } else {
                    Log.e("test", " trdds1");

                    try {
                       /* JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Toast.makeText(MainActivity.this, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();*/
                    } catch (Exception e) {
                        // Toast.makeText(LoginActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }

                //  List<Movie> movies = response.body().getResults();
                //  recyclerView.setAdapter(new MoviesAdapter(movies, R.layout.list_item_movie, getApplicationContext()));
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // Log error here since request failed
                Log.e("error message", t.toString());
                rl_pb.setVisibility(View.GONE);
            }
        });
    }


    SessionManager sessionManager;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
       /* DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
           // drawer.closeDrawer(GravityCompat.START);
        }  else {
           // Inad.exitalert(this);
        }*/

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
