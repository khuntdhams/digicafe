package com.cab.digicafe.Activities;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.cab.digicafe.Activities.RequestApis.MyRequestCall;
import com.cab.digicafe.Activities.RequestApis.MyRequst;
import com.cab.digicafe.Adapter.AddressAdapter;
import com.cab.digicafe.Adapter.PlaceArrayAdapter;
import com.cab.digicafe.Adapter.SelectFileAdapter;
import com.cab.digicafe.Adapter.taxGroup.TaxGroupModel;
import com.cab.digicafe.CatelogActivity;
import com.cab.digicafe.Database.SettingDB;
import com.cab.digicafe.Database.SettingModel;
import com.cab.digicafe.Database.SqlLiteDbHelper;
import com.cab.digicafe.DetePickerFragment;
import com.cab.digicafe.Helper.DragTouchHelperIssue;
import com.cab.digicafe.Helper.Inad;
import com.cab.digicafe.Helper.SessionManager;
import com.cab.digicafe.Helper.SharedPrefUserDetail;
import com.cab.digicafe.HistoryActivity;
import com.cab.digicafe.InvoicesummaryActivity;
import com.cab.digicafe.Model.AddAddress;
import com.cab.digicafe.Model.Catelog;
import com.cab.digicafe.Model.ModelFile;
import com.cab.digicafe.Model.ProductTotalDiscount;
import com.cab.digicafe.Model.SupplierNew;
import com.cab.digicafe.Model.Userprofile;
import com.cab.digicafe.MyCustomClass.Compressor;
import com.cab.digicafe.MyCustomClass.JsonObjParse;
import com.cab.digicafe.OrderActivity;
import com.cab.digicafe.R;
import com.cab.digicafe.Rest.ApiClient;
import com.cab.digicafe.Rest.ApiInterface;
import com.cab.digicafe.TimePicker;
import com.cab.digicafe.Timeline.DateTimeUtils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Headers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.media.MediaRecorder.VideoSource.CAMERA;

public class IssueMgmtActivity extends MyAppBaseActivity implements View.OnClickListener, AddressAdapter.OnItemClickListener, DetePickerFragment.OnItemClickListener, TimePicker.OnItemTimerClickListener {

    @BindView(R.id.iv_back)
    ImageView iv_back;

    @BindView(R.id.iv_date)
    ImageView iv_date;

    @BindView(R.id.tv_date)
    TextView tv_date;

    @BindView(R.id.iv_time)
    ImageView iv_time;

    @BindView(R.id.tv_time)
    TextView tv_time;

    @BindView(R.id.et_address)
    EditText et_address;

    @BindView(R.id.et_phone)
    EditText et_phone;

    @BindView(R.id.et_detail)
    EditText et_detail;

    @BindView(R.id.tv_done)
    TextView tv_done;

    @BindView(R.id.rv_selectfile)
    RecyclerView rv_selectfile;
    SelectFileAdapter selectFileAdapter;

    @BindView(R.id.iv_add)
    ImageView iv_add;

    @BindView(R.id.iv_cart)
    ImageView iv_cart;

    @BindView(R.id.iv_camera)
    ImageView iv_camera;

    @BindView(R.id.tv_badge)
    TextView tv_badge;

    @BindView(R.id.flCart)
    FrameLayout flCart;

    @BindView(R.id.llBtn)
    LinearLayout llBtn;

    @BindView(R.id.tvTitle)
    TextView tvTitle;

    @BindView(R.id.rlSmsOnly)
    RelativeLayout rlSmsOnly;

    @BindView(R.id.svService)
    ScrollView svService;

    SupplierNew item;

    String field_json_data_info = "", business_type_info = ""; // field_json_data_info - info, business_type - info

    @BindView(R.id.tvAdd)
    TextView tvAdd;

    @BindView(R.id.etSmsInfo)
    EditText etSmsInfo;

    ArrayList<AddAddress> addressList = new ArrayList<>();
    AddressAdapter mAdapter;
    @BindView(R.id.llAddressList)
    LinearLayout llAddressList;
    @BindView(R.id.ivHideList)
    ImageView ivHideList;
    @BindView(R.id.rvAddressList)
    RecyclerView rvAddressList;
    @BindView(R.id.ivAddList)
    ImageView ivAddList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_issue_mgmt);
        ButterKnife.bind(this);

        field_json_data_info = SharedPrefUserDetail.getString(this, SharedPrefUserDetail.field_json_data_info, "");
        business_type_info = SharedPrefUserDetail.getString(this, SharedPrefUserDetail.business_type, "");


        sessionManager = new SessionManager(this);
        inits3();
        field_json_data = SharedPrefUserDetail.getString(this, SharedPrefUserDetail.field_json_data, "");
        business_type = SharedPrefUserDetail.getString(this, SharedPrefUserDetail.business_type, "");

        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {

            } else {
                //frombridgeidval = extras.getString("frombridgeidval");
                /*purpose = extras.getString("purpose");
                from_bridge_id = extras.getString("from_bridge_id");
                to_bridge_id = extras.getString("to_bridge_id");
                alias_name = extras.getString("alias_name");
                username = extras.getString("username");*/

                //String supplier = extras.getString("supplier");
                //item = new Gson().fromJson(supplier, Supplier.class);
                //frombridgeidval = from_bridge_id;

                purpose = extras.getString("purpose");
                String supplier = extras.getString("supplier");
                item = new Gson().fromJson(supplier, SupplierNew.class);
                frombridgeidval = item.getBridgeId();

                llBtn.setVisibility(View.VISIBLE);
                svService.setVisibility(View.VISIBLE);
                rlSmsOnly.setVisibility(View.GONE);
                tvTitle.setText("Service");


                if (purpose.equalsIgnoreCase(getString(R.string.sms_only))) {
                    llBtn.setVisibility(View.GONE);
                    svService.setVisibility(View.GONE);
                    rlSmsOnly.setVisibility(View.VISIBLE);
                    tvTitle.setText("Sms only");
                    et_detail.setText("sms-only");
                    PAYMENT_MODE = "NIL";
                }

            }

        } else {

        }

        dbHelper = new SqlLiteDbHelper(this);

        tvAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tv_done.performClick();
            }
        });

        iv_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String value = tv_google_loc.getText().toString();
                String remark = et_detail.getText().toString().trim();
                String phone = et_phone.getText().toString();

                headernotes = remark;
                final String date_ = df.format(cal_order.getTime());


                sessionManager.setDate(date_);
                sessionManager.setAddress(value);
                sessionManager.setRemark(headernotes);
                sessionManager.setT_Phone(phone);


                String img = new Gson().toJson(al_selet);


                Intent intent = new Intent(IssueMgmtActivity.this, CatelogActivity.class);
                intent.putExtra("session", sessionManager.getsession());

                if (item.getContact_bridge_id() == null) // circle - aggre (from catererfragment)
                {
                    item.setContact_bridge_id(item.getBridgeId());
                }

                intent.putExtra("bridgeidval", item.getContact_bridge_id());
                intent.putExtra("frombridgeidval", item.getBridgeId());

                intent.putExtra("aliasname", item.getUsername());
                intent.putExtra("username", item.getUsername());
                intent.putExtra("isrequset", true);
                intent.putExtra("purpose", purpose);
                intent.putExtra("img", img);
                intent.putExtra("company_name", item.getCompanyName());
                startActivity(intent);


            }
        });

        iv_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showPictureDialog();

                //cameraIntent();
            }
        });


        findViewById(R.id.iv_clear).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv_google_loc.setText("");
            }
        });

        tv_google_loc = (AutoCompleteTextView) findViewById(R.id.tv_google_loc);
        tv_google_loc.setThreshold(1);
        tv_google_loc.setOnItemClickListener(mAutocompleteClickListener);
        locationinitialized();
        //String expected_delivery_time =  DateTimeUtils.parseDateTime(time, "dd MMM yyyy hh:mm aaa", "yyyy/MM/dd kk:mm");


        //et_address.setText(add);
        //tv_google_loc.setText(add);
        //et_detail.setText(remark);
        //et_phone.setText(phone);


        tv_done.setOnClickListener(this);
        iv_date.setOnClickListener(this);
        iv_time.setOnClickListener(this);
        //findViewById(R.id.iv_address).setOnClickListener(this);


        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        settime();

        iv_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(IssueMgmtActivity.this, FileActivity.class);
                i.putExtra("isdoc", false);
                startActivityForResult(i, 8);
                //showPictureDialog();


            }
        });

        String[] PERMISSIONS = {android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_EXTERNAL_STORAGE};
        if (!Inad.hasPermissions(this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, 801);
        } else {

        }

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rv_selectfile.setLayoutManager(mLayoutManager);


        selectFileAdapter = new SelectFileAdapter(al_selet, this, new SelectFileAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(String item) {

            }

            @Override
            public void onDelete(int pos) {
                al_selet.remove(pos);
                selectFileAdapter.assignItem(al_selet);
                //if(al_selet.size()==0)iv_upload.setVisibility(View.GONE);
                //else iv_upload.setVisibility(View.VISIBLE);


            }

            @Override
            public void onClickString(String pos) {


            }

            @Override
            public void onImgClick(List<ModelFile> data, int pos) {


            }
        }, false, false, true);
        rv_selectfile.setAdapter(selectFileAdapter);

        ItemTouchHelper.Callback callback = new DragTouchHelperIssue(selectFileAdapter);
        ItemTouchHelper helper = new ItemTouchHelper(callback);
        helper.attachToRecyclerView(rv_selectfile);

        checkIsAnyRecord();

        ivHideList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llAddressList.setVisibility(View.GONE);
            }
        });
        ivAddList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llAddressList.setVisibility(View.VISIBLE);
            }
        });

    }

    public void locationinitialized() {


        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(Places.GEO_DATA_API)
                    .enableAutoManage(this, GOOGLE_API_CLIENT_ID, new GoogleApiClient.OnConnectionFailedListener() {
                        @Override
                        public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

                        }
                    })
                    .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                        @Override
                        public void onConnected(@Nullable Bundle bundle) {
                            mPlaceArrayAdapter.setGoogleApiClient(mGoogleApiClient);
                        }

                        @Override
                        public void onConnectionSuspended(int i) {
                            mPlaceArrayAdapter.setGoogleApiClient(null);
                        }
                    })
                    .build();
        }

        tv_google_loc.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count > 0) {
                    findViewById(R.id.iv_clear).setVisibility(View.VISIBLE);
                } else {
                    findViewById(R.id.iv_clear).setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mPlaceArrayAdapter = new PlaceArrayAdapter(this, android.R.layout.simple_list_item_1,
                BOUNDS_MOUNTAIN_VIEW, null);

        tv_google_loc.setAdapter(mPlaceArrayAdapter);
    }

    private GoogleApiClient mGoogleApiClient;
    AutoCompleteTextView tv_google_loc;
    private PlaceArrayAdapter mPlaceArrayAdapter;
    private static final LatLngBounds BOUNDS_MOUNTAIN_VIEW = new LatLngBounds(
            new LatLng(37.398160, -122.180831), new LatLng(37.430610, -121.972090));
    private static final int GOOGLE_API_CLIENT_ID = 0;


    private AdapterView.OnItemClickListener mAutocompleteClickListener
            = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            final PlaceArrayAdapter.PlaceAutocomplete item = mPlaceArrayAdapter.getItem(position);
            final String placeId = String.valueOf(item.placeId);
            //Log.i(LOG_TAG, "Selected: " + item.description);
            PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                    .getPlaceById(mGoogleApiClient, placeId);
            placeResult.setResultCallback(mUpdatePlaceDetailsCallback);
            //Log.i(LOG_TAG, "Fetching details for ID: " + item.placeId);
        }
    };


    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback
            = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {

                return;
            }
            // Selecting the first object buffer.
            final Place place = places.get(0);
            CharSequence attributions = places.getAttributions();


            //tv_google_loc.setText(Html.fromHtml(place.getAddress() + ""));

        }
    };
    SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy hh:mm aaa");

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_done:
                String remark = et_detail.getText().toString().trim();
                headernotes = remark;

                if (headernotes.isEmpty()) {
                    Toast.makeText(IssueMgmtActivity.this, "Detail Field Is Required !", Toast.LENGTH_LONG).show();
                } else {

                    dbHelper = new SqlLiteDbHelper(this);

                    productlist = dbHelper.getAllcaretproduct();
                    productcount = dbHelper.getcartcount();

                    if (productlist.size() == 0) {
                        productlist = new ArrayList<>();
                        Catelog catelog = new Catelog();
                        productlist.add(catelog);
                        productcount = 0;
                    }

                    String value = tv_google_loc.getText().toString();

                    String phone = et_phone.getText().toString();


                    final String date_ = df.format(cal_order.getTime());

                    billamount();
                    sessionManager.setDate(date_);
                    sessionManager.setAddress(value);
                    sessionManager.setRemark(headernotes);
                    sessionManager.setT_Phone(phone);


                    if (purpose.equalsIgnoreCase(getString(R.string.sms_only))) {
                        sessionManager.setRemark(etSmsInfo.getText().toString().trim());
                        //   purpose = purpose  + ", " +sessionManager.getcurrentu_nm() +", " +sessionManager.getMobile()+" "+sessionManager.getAddress();
                    }

                    if (al_selet.size() > 0) {
                        str_img = new String[al_selet.size()];
                        new ImageCompress().execute();
                        //uploadimg();
                    } else {
                        usersignin(sessionManager.getsession(), date_, value);
                    }
                }
                break;
            case R.id.iv_date:
                DetePickerFragment mDatePicker = new DetePickerFragment(IssueMgmtActivity.this);
                mDatePicker.setStyle(DialogFragment.STYLE_NORMAL, R.style.CustomDialog);
                mDatePicker.show(getSupportFragmentManager(), "Select delivery date");

                break;
            case R.id.iv_time:
                TimePicker mTimePicker = new TimePicker(IssueMgmtActivity.this);
                mTimePicker.show(getSupportFragmentManager(), "Select delivery time");
                break;
            case R.id.iv_address:

                break;
            default:
                break;
        }
    }

    Calendar cal_order = Calendar.getInstance();

   /* @SuppressLint("ValidFragment")
    public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {
        @Override
        public void onCancel(DialogInterface dialog) {
            super.onCancel(dialog);

        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar c = Calendar.getInstance();


            *//*String dtStart = sessionManager.getDate();
            SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy hh:mm aaa");
            try {
                if (!dtStart.equals("")) {
                    Date date = format.parse(dtStart);
                    c.setTime(date);
                    System.out.println(date);
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }*//*

            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {


            SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy hh:mm aaa");


            cal_order.set(year, month, day);
            settime();
            //showdeliverydialog();

            //String date = df.format(calendar.getTime());
            //displayCurrentTime.setText("Selected date: " + String.valueOf(year) + " - " + String.valueOf(month) + " - " + String.valueOf(day));
        }
    }

    DatePickerFragment mDatePicker;*/

    String PAYMENT_MODE = "COD";

/*    @SuppressLint("ValidFragment")
    public class TimePicker extends DialogFragment implements TimePickerDialog.OnTimeSetListener {
        @Override
        public void onCancel(DialogInterface dialog) {
            super.onCancel(dialog);

        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar c = Calendar.getInstance();

          *//*  String dtStart = sessionManager.getDate();
            SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy hh:mm aaa");
            try {
                if (!dtStart.equals("")) {
                    Date date = format.parse(dtStart);
                    c.setTime(date);
                    System.out.println(date);
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }*//*


            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);
            return new TimePickerDialog(getActivity(), this, hour, minute, DateFormat.is24HourFormat(getActivity()));
        }

        @Override
        public void onTimeSet(android.widget.TimePicker view, int hourOfDay, int minute) {

            cal_order.set(cal_order.get(Calendar.YEAR), cal_order.get(Calendar.MONTH), cal_order.get(Calendar.DAY_OF_MONTH), hourOfDay, minute);

            settime();


            //showdeliverydialog();


           *//* SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy hh:mm aaa");
            final String date_ = df.format(cal_order.getTime());

            if(getString(R.string.isaddress).equals("1")) {

                new AddAddress(OrderActivity.this, new AddAddress.OnDialogClickListener() {
                    @Override
                    public void onDialogImageRunClick(int positon, String add) {
                        if (positon == 0) {

                        } else if (positon == 1) {
                            //str_actionid = str_actionid.replaceAll(",$", "");
                            dialog.setMessage("Creating your order ...");
                            dialog.show();
                            billamount();
                            sessionManager.setDate(date_);
                            sessionManager.setAddress(add);
                            sessionManager.setRemark(headernotes);
                            usersignin(sessionString, cartdetails, userdetails, date_, add);
                        }
                    }
                }, sessionManager.getAddress()).show();

            }
            else {
                dialog.setMessage("Creating your order ...");
                dialog.show();
                billamount();
                sessionManager.setDate(date_);
                sessionManager.setAddress("");
                sessionManager.setRemark(headernotes);
                usersignin(sessionString, cartdetails, userdetails, date_, "");
            }*//*


        }
    }*/

    @Override
    public void onItemClick(Boolean isTrue, int year, int month, int dayOfMonth) {
        if (isTrue) {
            cal_order.set(year, month, dayOfMonth);
            settime();
        } else {
        }
    }

    @Override
    public void onItemClick(Boolean isTrue, int hourOfDay, int minute) {
        if (isTrue) {
            cal_order.set(cal_order.get(Calendar.YEAR), cal_order.get(Calendar.MONTH), cal_order.get(Calendar.DAY_OF_MONTH), hourOfDay, minute);

            settime();

        } else {

        }
    }


    public void settime() {
        SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy hh:mm aaa");
        String time = df.format(cal_order.getTime());

        SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy hh:mm aaa");
        try {
            if (!time.equals("")) {
                Date date_ = format.parse(time);


                SimpleDateFormat timeformat = new SimpleDateFormat("hh:mm aaa");
                String str_time = timeformat.format(date_);

                SimpleDateFormat dtformat = new SimpleDateFormat("dd MMM yyyy");

                String str_date = dtformat.format(date_);
                tv_date.setText(str_date);
                tv_time.setText(str_time);

            }
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public ArrayList<Catelog> productlist = new ArrayList<>();
    SqlLiteDbHelper dbHelper;
    SessionManager sessionManager;
    JSONObject userdetails;

    public void usersignin(final String usersession, final String dt, final String address) {

        try {
            dialog = new ProgressDialog(IssueMgmtActivity.this);
            dialog.setMessage("Submitting...");
            dialog.show();
        } catch (Exception e) {

        }


        try {
            userdetails = new JSONObject(sessionManager.getUserprofile());
            loggedin = new Gson().fromJson(userdetails.toString(), Userprofile.class);

        } catch (JSONException e) {
            e.printStackTrace();
        }


        final JsonObject sendobj = new JsonObject();
        JsonArray a = new JsonArray();
        JsonArray cartproduct = new JsonArray();

        for (int i = 0; i < productlist.size(); i++) {
            Catelog product = productlist.get(i);
            //  JSONObject productobj= null;
            //  try {
            //       productobj = cartdetails.getJSONObject(i);
            JsonObject productsync = new JsonObject();
              /*  productsync.put("entry_id",productobj.getString("entry_id"));
                productsync.put("bridge_id",productobj.getString("bridge_id"));
                productsync.put("flag","add");
                productsync.put("quantity",productobj.getString("cartcount"));
                productsync.put("particulars",productobj.getString("name"));
                productsync.put("price",productobj.getString("mrp"));


*/

             /*    productobj.put("bridge_id",productobj.getString("bridge_id"));
                productobj.put("bridge_id",productobj.getString("bridge_id"));*/
            // cartproduct.put(productsync);

           /* productsync.addProperty("entry_id",product.getEntry_id());
            productsync.addProperty("bridge_id",product.getBridge_id());
            productsync.addProperty("flag","add");
            productsync.addProperty("quantity",product.getCartcount());
            productsync.addProperty("particulars",product.getDefault_name());
            productsync.addProperty("price",product.getMrp());*/

            final SettingDB db = new SettingDB(this);
            SessionManager sessionManager = new SessionManager(this);

            List<SettingModel> list_setting = db.GetItems();
            SettingModel sm = new SettingModel();
            sm = list_setting.get(0);


            String particular = product.getDefault_name();
            String cross_reference = product.getCross_reference();
            if (cross_reference != null && !cross_reference.equals("")) {
                particular = particular + "; [" + cross_reference + "]";
            }
            String offer = product.getOffer();
            if (offer != null && !offer.equals("") && sm.getIsconcateoffer().equals("1")) {
                particular = particular + "; " + product.getOffer();
            }
            productsync.addProperty("entry_id", product.getEntry_id());
            productsync.addProperty("bridge_id", product.getBridge_id());
            productsync.addProperty("flag", "add");
            productsync.addProperty("quantity", product.getCartcount());
            productsync.addProperty("particulars", particular);
            productsync.addProperty("price", String.valueOf(cal_price(i)));
            productsync.addProperty("offer", product.getOffer());

            cartproduct.add(productsync);

          /*  } catch (JSONException e) {
                e.printStackTrace();
            }*/

        }


        try {
            JsonObject a1 = new JsonObject();
         /*  a1.put("email","athi123@gmail.com");
           a1.put("contact_number","22222222222");
           a1.put("location","");
           a1.put("chit_name","testform");*/
            a1.addProperty("email", loggedin.getEmail_id());
            a1.addProperty("contact_number", loggedin.getContact_no());
            a1.addProperty("location", loggedin.getLocation());
            a1.addProperty("chit_name", loggedin.getFirstname());
            a1.addProperty("header_note", headernotes);


            //a1.addProperty("footer_note", jo_footer.toString());
            sendobj.add("newdata", cartproduct);
            sendobj.add("olddata", a);
            sendobj.add("chitHeader", a1);


        } catch (JsonParseException e) {
            e.printStackTrace();
        }

        Log.e("sendobj", sendobj.toString());
        String currency_code = SharedPrefUserDetail.getString(IssueMgmtActivity.this, SharedPrefUserDetail.chit_currency_code, "");
        String currency_code_id = SharedPrefUserDetail.getString(IssueMgmtActivity.this, SharedPrefUserDetail.chit_currency_code_id, "");

        ApiInterface apiService =
                ApiClient.getClient(IssueMgmtActivity.this).create(ApiInterface.class);
        Call call = apiService.createchit(usersession, sendobj, sessionManager.getAggregatorprofile(), frombridgeidval, "d97be841a6d6cbb66bc3ae165b31b85f", currency_code, currency_code_id, "2");
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                int statusCode = response.code();
                Log.e("test", " trdds");

                Headers headerList = response.headers();
                Log.d("test", statusCode + " ");

                //  String cookie = response.;
                //  Log.d("test", cookie+ " ");


                if (response.isSuccessful()) {
                    try {
                        String a = new Gson().toJson(response.body());
                        Log.e("response", a);
                        JSONObject responseobj = new JSONObject(a);
                        JSONObject responseval = responseobj.getJSONObject("response");

                        JSONObject data = responseval.getJSONObject("data");
                        hashidval = data.getString("chitHashId");
                        JSONObject newobj = new JSONObject();
                        try {
                            JsonObject jObj = new JsonObject();
                            jObj.addProperty("email", loggedin.getEmail_id());
                            jObj.addProperty("contact_number", sessionManager.getT_Phone());
                            jObj.addProperty("location", sessionManager.getAddress());
                            jObj.addProperty("act", sessionManager.getcurrentcaterername());
                            jObj.addProperty("chit_name", "");
                            jObj.addProperty("chitHashId", hashidval);
                            jObj.addProperty("latitude", "");
                            jObj.addProperty("longtitude", "");
                            jObj.addProperty("expected_delivery_time", "");
                            jObj.addProperty("for_non_bridge_name", "");
                            jObj.addProperty("subject", sessionManager.getcurrentcatereraliasname());


                            String currency_code = SharedPrefUserDetail.getString(IssueMgmtActivity.this, SharedPrefUserDetail.chit_currency_code, "");
                            String symbol_native = SharedPrefUserDetail.getString(IssueMgmtActivity.this, SharedPrefUserDetail.chit_symbol_native, "");
                            String currency_code_id = SharedPrefUserDetail.getString(IssueMgmtActivity.this, SharedPrefUserDetail.chit_currency_code_id, "");
                            String symbol = SharedPrefUserDetail.getString(IssueMgmtActivity.this, SharedPrefUserDetail.chit_symbol, "");

                            jObj.addProperty("currency_code", currency_code);
                            jObj.addProperty("symbol_native", symbol_native);
                            jObj.addProperty("currency_code_id", currency_code_id);
                            jObj.addProperty("symbol", symbol);


                            boolean iss = SharedPrefUserDetail.getBoolean(IssueMgmtActivity.this, SharedPrefUserDetail.isfromsupplier, false);
                            if (iss) {

                                jObj.addProperty("info", "");

                                if (field_json_data != null) {

                                 /*   String delivery_partner_info = JsonObjParse.getValueEmpty(field_json_data, "delivery_partner_info");
                                    if(!delivery_partner_info.isEmpty()){
                                        StringBuilder stringBuilder = new StringBuilder();
                                        stringBuilder.append(delivery_partner_info+" , ");
                                        jObj.addProperty("info", stringBuilder.toString());
                                    }*/
                                }


                            } else {
                                // if its aggre / circle
                                // blrmc1(delivery flag of this) under blrmcc(info flag of this)
                                String info = SharedPrefUserDetail.getString(IssueMgmtActivity.this, SharedPrefUserDetail.infoMainShop, "");
                                //String act = sessionManager.getAggregator_ID();
                                String act = sessionManager.getcurrentcaterername();
                                //String infoPass = info + " , " + act + " , ";
                                StringBuilder stringBuilder = new StringBuilder(); // old is correct
                                //if(!info.isEmpty())stringBuilder.append(info+" , ");
                                //if(!act.isEmpty())stringBuilder.append(act+" , ");
                                if (field_json_data_info != null) {
                                    String pass_info_circle = JsonObjParse.getValueEmpty(field_json_data_info, "pass_info_circle");
                                    String pass_info_aggregator = JsonObjParse.getValueEmpty(field_json_data_info, "pass_info_aggregator");
                                    if (!pass_info_circle.equalsIgnoreCase("no") && business_type_info.equalsIgnoreCase("circle")) {
                                        if (!info.isEmpty()) stringBuilder.append(info + " , ");
                                    }
                                    if (!pass_info_aggregator.equalsIgnoreCase("no") && business_type_info.equalsIgnoreCase("aggregator")) {
                                        if (!info.isEmpty()) stringBuilder.append(info + " , ");
                                    }
                                }

                                if (!act.isEmpty()) stringBuilder.append(act + " , ");

                                if (field_json_data != null) {

                                   /* String delivery_partner_info = JsonObjParse.getValueEmpty(field_json_data, "delivery_partner_info");
                                    if(!delivery_partner_info.isEmpty()){
                                        stringBuilder.append(delivery_partner_info+" , ");
                                    }*/
                                }

                                Log.d("okhttp info", stringBuilder.toString());
                                jObj.addProperty("info", stringBuilder.toString());
                                //jObj.addProperty("info", sessionManager.getAggregator_ID());

                            }

                            jObj.addProperty("header_note", headernotes);
                            jObj.addProperty("purpose", purpose);
                            jObj.addProperty("chit_item_count", productcount);
                            jObj.addProperty("total_chit_item_value", String.valueOf(grandtotal));
                            JsonObject jo_footer = new JsonObject();

                            final SettingDB db = new SettingDB(IssueMgmtActivity.this);
                            List<SettingModel> list_setting = db.GetItems();
                            SettingModel sm = new SettingModel();
                            sm = list_setting.get(0);

                            if (purpose.equalsIgnoreCase(getString(R.string.sms_only))) {
                                jo_footer.addProperty("PAYMENT_MODE", PAYMENT_MODE);

                            } else {
                                jo_footer.addProperty("PAYMENT_MODE", PAYMENT_MODE);

                            }

                            jo_footer.addProperty("NAME", loggedin.getFirstname());

                            jo_footer.addProperty("DATE", dt);  // dt
                            jo_footer.addProperty("ADDRESS", address);  // address
                            jo_footer.addProperty("field_json_data", field_json_data);
                            jObj.addProperty("footer_note", jo_footer.toString());
                            //jObj.addProperty("total_chit_item_value", pricevalue);
                            // sendobj.put("chitHeader",newobj);
                            Log.e("chitheadervalue", jObj.toString());

                            confirmorder();

                            //showorderconfirmation(jObj, chithashid, address, dt);
                            // updatechit(usersession,jObj,chithashid);
                        } catch (JsonParseException e) {
                            e.printStackTrace();
                        }
                    } catch (Exception e) {
                        Log.e("errortest", e.getMessage());

                        Toast.makeText(IssueMgmtActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                    //  Intent i = new Intent(MainActivity.this, MainActivity.class);
                    //  startActivity(i);
                } else {
                    Log.e("test", " trdds1");

                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Toast.makeText(IssueMgmtActivity.this, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(IssueMgmtActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }

                //  List<Movie> movies = response.body().getResults();
                //  recyclerView.setAdapter(new MoviesAdapter(movies, R.layout.list_item_movie, getApplicationContext()));
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // Log error here since request failed
                Log.e("error message", t.toString());
            }
        });
    }


    public void showorderconfirmation(JsonObject hashobj, String hashstring, String add, String dt) {
        Intent i = new Intent(IssueMgmtActivity.this, InvoicesummaryActivity.class);
        i.putExtra("hashheadervalue", hashobj.toString());
        i.putExtra("hashid", hashstring);
        i.putExtra("add", add);
        i.putExtra("dt", dt);
        i.putExtra("purpose", purpose);

        String img = new Gson().toJson(al_selet);
        i.putExtra("img", img);
        startActivity(i);
    }

    public Double cal_price(int i) { // for each prod

        Double totalbill_ = 0.0;


        int qty = productlist.get(i).getCartcount();
        Double bill = 0.0;
        try {
            bill = Double.valueOf(productlist.get(i).getdiscountmrp());
        } catch (Exception e) {
            bill = 0.0;
        }

        Double sgst = 0.0;
        Double cgst = 0.0;
        try {
            String tax = productlist.get(i).getTax_json_data();
            if (tax != null) {
                JSONObject obj = new JSONObject(tax);
                sgst = Double.valueOf(obj.getString("SGST"));
                cgst = Double.valueOf(obj.getString("CGST"));
            }

            Log.e("stategst", String.valueOf(sgst));
            Log.e("stategst", String.valueOf(cgst));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Double totalamt = bill;
        Double stategst = (totalamt / 100.0f) * sgst;
        Double centralgst = (totalamt / 100.0f) * cgst;


        totalbill_ = bill + centralgst + stategst;

        /*centralgstamount.setText(""+ String.format("%.2f", totalcentralgst));
        stategstamount.setText(""+ String.format("%.2f", totalstategst));
        billamount.setText(""+ String.format("%.2f", totalbill));
        grandtotalamount.setText(""+String.format("%.2f", grandtotal));
        String.format("%.2f", grandtotal);*/
        Log.e("final", String.valueOf(totalbill_));

        return totalbill_;
    }

    Userprofile loggedin;
    String headernotes;
    String frombridgeidval = "", purpose = "";
    int productcount;

    Double totalbill = 0.0;
    Double totalReqDisc = 0.0;
    Double ReqDisc = 0.0;
    Double totalProductDisc = 0.0;
    Double totalstategst = 0.0;
    Double totalcentralgst = 0.0;
    Double grandtotal = 0.0;
    Double totalVat = 0.0;
    Double totalWithRoundOff = 0.0;


    public void billamount() {

        alTax = new ArrayList<>();

        ArrayList<Catelog> productlist = new ArrayList<>();
        productlist = dbHelper.getAllcaretproduct();

        totalVat = 0.0;
        totalbill = 0.0;
        totalstategst = 0.0;
        totalcentralgst = 0.0;
        for (int i = 0; i < productlist.size(); i++) {
            int qty = productlist.get(i).getCartcount();
            Double bill = Double.valueOf(productlist.get(i).getdiscountmrp()) * qty;

            Double mrp = Double.valueOf(productlist.get(i).getMrp()) * qty;  // mrp
            mrp = mrp - bill;
            totalProductDisc = totalProductDisc + mrp;


            totalbill = totalbill + bill;
            Log.e("totalbil", productlist.get(i).getdiscountmrp());
            Log.e("qty", String.valueOf(qty));

            Double vat = 0.0;
            Double sgst = 0.0;
            Double cgst = 0.0;
            try {
               /* String tax = productlist.get(i).getTax_json_data();
                if (tax != null) {
                    JSONObject obj = new JSONObject(tax);
                    if (obj.has("VAT")) {
                        vat = Double.valueOf(obj.getString("VAT"));
                    } else {
                        sgst = Double.valueOf(obj.getString("SGST"));
                        cgst = Double.valueOf(obj.getString("CGST"));
                    }
                }

                Log.e("stategst", String.valueOf(sgst));
                Log.e("stategst", String.valueOf(cgst));*/


                String tax = productlist.get(i).getTax_json_data();
                if (tax != null) {
                   /* JSONObject obj = new JSONObject(tax);
                    if (obj.has("VAT")) {
                        vat = Double.valueOf(obj.getString("VAT"));
                    } else {
                        sgst = Double.valueOf(obj.getString("SGST"));
                        cgst = Double.valueOf(obj.getString("CGST"));
                    }*/

                    boolean isF1 = true;

                    JSONObject joTax = new JSONObject(tax);


                    Iterator keys = joTax.keys();

                    while (keys.hasNext()) {

                        try {
                            String key = (String) keys.next();

                            String taxVal = JsonObjParse.getValueFromJsonObj(joTax, key);


                            if (isF1) {

                                isF1 = false;
                                sgst = Double.valueOf(taxVal);

                                Double tax1 = (bill / 100.0f) * sgst;
                                boolean isExist = false;
                                for (int k = 0; k < alTax.size(); k++) {
                                    String tmpTaxVal = alTax.get(k).taxValPercentage;
                                    String tmpKey = alTax.get(k).taxKey;
                                    if (tmpKey.equals(key) && taxVal.equals(tmpTaxVal)) {
                                        isExist = true;
                                        alTax.get(k).taxVal = alTax.get(k).taxVal + tax1;
                                        break;
                                    }
                                }
                                if (!isExist && tax1 > 0) {
                                    alTax.add(new TaxGroupModel(tax1, key, taxVal));
                                }


                            } else {
                                cgst = Double.valueOf(taxVal);

                                Double tax2 = (bill / 100.0f) * cgst;
                                boolean isExist = false;
                                for (int k = 0; k < alTax.size(); k++) {
                                    String tmpTaxVal = alTax.get(k).taxValPercentage;
                                    String tmpKey = alTax.get(k).taxKey;
                                    if (tmpKey.equals(key) && taxVal.equals(tmpTaxVal)) {
                                        isExist = true;
                                        alTax.get(k).taxVal = alTax.get(k).taxVal + tax2;
                                        break;
                                    }
                                }
                                if (!isExist && tax2 > 0) {
                                    alTax.add(new TaxGroupModel(tax2, key, taxVal));
                                }

                            }

                            Log.e("key", key);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                }


                Log.e("stategst", String.valueOf(sgst));
                Log.e("stategst", String.valueOf(cgst));
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Double totalamt = bill;
            Double stategst = (totalamt / 100.0f) * sgst;
            Log.e("stategst", String.valueOf(stategst));
            totalstategst = totalstategst + stategst;

            Double centralgst = (totalamt / 100.0f) * cgst;
            Log.e("stategst", String.valueOf(centralgst));
            totalcentralgst = totalcentralgst + centralgst;

            Double valueAt = (totalamt / 100.0f) * vat;
            Log.e("valueAt", String.valueOf(valueAt));
            totalVat = totalVat + valueAt;
        }


        checkDiscount();

        grandtotal = totalbill + totalcentralgst + totalstategst + totalVat - totalReqDisc;

        setCharges();

        /*centralgstamount.setText(""+ String.format("%.2f", totalcentralgst));
        stategstamount.setText(""+ String.format("%.2f", totalstategst));
        billamount.setText(""+ String.format("%.2f", totalbill));
        grandtotalamount.setText(""+String.format("%.2f", grandtotal));
        String.format("%.2f", grandtotal);*/
        Log.e("totalstategst", String.valueOf(totalstategst));
        Log.e("final", String.valueOf(totalbill));
    }

    ArrayList<TaxGroupModel> alTax = new ArrayList<>();


    private void showPictureDialog() {
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(this);
        String[] pictureDialogItems = {"Photo Gallery", "Camera"};
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:

                                Intent i = new Intent(IssueMgmtActivity.this, FileActivity.class);
                                i.putExtra("isdoc", false);
                                startActivityForResult(i, 8);

                                break;
                            case 1:
                                cameraIntent();
                                break;
                            case 2:
                                //Intent intent = new Intent(IssueMgmtActivity.this, CatalougeTabActivity.class);
                                //intent.putExtra("isfromissue", true);
                                //startActivityForResult(intent, 11);

                                //dbHelper = new SqlLiteDbHelper(IssueMgmtActivity.this);
                                //productlist = dbHelper.getAllcaretproduct();
                                //productcount = dbHelper.getcartcount();
                                //sessionManager = new SessionManager(IssueMgmtActivity.this);
                                //billamount();

                                String value = tv_google_loc.getText().toString();
                                String remark = et_detail.getText().toString();
                                String phone = et_phone.getText().toString();

                                headernotes = remark;
                                final String date_ = df.format(cal_order.getTime());


                                sessionManager.setDate(date_);
                                sessionManager.setAddress(value);
                                sessionManager.setRemark(headernotes);
                                sessionManager.setT_Phone(phone);


                                String img = new Gson().toJson(al_selet);

                                if (item.getContact_bridge_id() == null) // circle - aggre (from catererfragment)
                                {
                                    item.setContact_bridge_id(item.getBridgeId());
                                }

                                Intent intent = new Intent(IssueMgmtActivity.this, CatelogActivity.class);
                                intent.putExtra("session", sessionManager.getsession());
                                intent.putExtra("bridgeidval", item.getContact_bridge_id());
                                intent.putExtra("frombridgeidval", item.getBridgeId());
                                intent.putExtra("aliasname", item.getUsername());
                                intent.putExtra("username", item.getUsername());
                                intent.putExtra("currency_code", item.getCurrencyCode());
                                intent.putExtra("company_name", item.getCompanyName());
                                intent.putExtra("isrequset", true);
                                intent.putExtra("purpose", purpose);
                                intent.putExtra("img", img);
                                startActivity(intent);

                                break;
                        }
                    }
                });
        pictureDialog.show();
    }


    private void cameraIntent() {

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        this.screen_width = size.x;
        this.screen_height = size.y;

        File root = new File(Environment.getExternalStorageDirectory()
                + File.separator + getString(R.string.app_name) + File.separator + "Capture" + File.separator);
        root.mkdirs();
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        f3f = new File(root, "IMG" + timeStamp + ".jpg");

        try {
            imageUri = FileProvider.getUriForFile(
                    IssueMgmtActivity.this, getApplicationContext()
                            .getPackageName() + ".provider", this.f3f);
        } catch (Exception e) {
            Toast.makeText(this, "Please check SD card! Image shot is impossible!", Toast.LENGTH_SHORT).show();
        }

        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        intent.putExtra("output", imageUri);
        try {
            startActivityForResult(intent, CAMERA);
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
        }

    }


    ExifInterface exif;
    File f3f;
    int screen_height;
    int screen_width;
    private Uri imageUri;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to ak aris
        if (resultCode == -1 && requestCode == CAMERA) {


            Bitmap bitmap;
            int orientation;
            String albumnm = "";


            try {
                bitmap = BitmapFactory.decodeFile(this.f3f.getPath());
                this.exif = new ExifInterface(this.f3f.getPath());
                if (bitmap != null) {
                    // f3f.getAbsolutePath(

                    String picturePath = f3f.getPath();
                    ArrayList<ModelFile> al_temp_selet = new ArrayList<>();
                    al_temp_selet.add(new ModelFile(picturePath, true));
                    al_temp_selet.addAll(al_selet);
                    selectFileAdapter.assignItem(al_temp_selet);
                    al_selet = new ArrayList<>();
                    al_selet = al_temp_selet;
                    //Detection(0);

                } else {
                    Toast.makeText(getApplicationContext(), "Picture not taken !", Toast.LENGTH_SHORT).show();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }


        } else if (resultCode == -1 && requestCode == 8) {
            String picturePath = data.getExtras().getString("al_selet");
            ArrayList<ModelFile> al_temp_selet = new Gson().fromJson(picturePath, new TypeToken<ArrayList<ModelFile>>() {
            }.getType());
            al_temp_selet.addAll(al_selet);
            selectFileAdapter.assignItem(al_temp_selet);
            al_selet = new ArrayList<>();
            al_selet = al_temp_selet;
            //Detection(0);
            //if(al_selet.size()==0)iv_upload.setVisibility(View.GONE);
            //else iv_upload.setVisibility(View.VISIBLE);

        } else if (resultCode == -1 && requestCode == 11) {
            CatalougeContent = data.getStringExtra("CatalougeContent");
            Toast.makeText(IssueMgmtActivity.this, "Catalogue Added !", Toast.LENGTH_LONG).show();


        }


        super.onActivityResult(requestCode, resultCode, data);

    }


    String CatalougeContent = "";


    public void confirmorder() {

        Userprofile loggedin = null;

        try {
            JSONObject userdetails = new JSONObject(sessionManager.getUserprofile());
            loggedin = new Gson().fromJson(userdetails.toString(), Userprofile.class);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            JsonObject jObj = new JsonObject();
            jObj.addProperty("email", loggedin.getEmail_id());
            jObj.addProperty("contact_number", sessionManager.getT_Phone());
            jObj.addProperty("location", sessionManager.getAddress());
            jObj.addProperty("act", sessionManager.getcurrentcaterername());
            jObj.addProperty("chit_name", "");
            jObj.addProperty("chitHashId", hashidval);
            jObj.addProperty("latitude", "");
            jObj.addProperty("longtitude", "");

            String expected_delivery_time = DateTimeUtils.parseDateTime(sessionManager.getDate(), "dd MMM yyyy hh:mm aaa", "yyyy/MM/dd kk:mm");

            jObj.addProperty("expected_delivery_time", expected_delivery_time);
            jObj.addProperty("for_non_bridge_name", "");
            jObj.addProperty("subject", sessionManager.getcurrentcatereraliasname());


            String currency_code = SharedPrefUserDetail.getString(IssueMgmtActivity.this, SharedPrefUserDetail.chit_currency_code, "");
            String symbol_native = SharedPrefUserDetail.getString(IssueMgmtActivity.this, SharedPrefUserDetail.chit_symbol_native, "");
            String currency_code_id = SharedPrefUserDetail.getString(IssueMgmtActivity.this, SharedPrefUserDetail.chit_currency_code_id, "");
            String symbol = SharedPrefUserDetail.getString(IssueMgmtActivity.this, SharedPrefUserDetail.chit_symbol, "");

            jObj.addProperty("currency_code", currency_code);
            jObj.addProperty("symbol_native", symbol_native);
            jObj.addProperty("currency_code_id", currency_code_id);
            jObj.addProperty("symbol", symbol);


            boolean iss = SharedPrefUserDetail.getBoolean(IssueMgmtActivity.this, SharedPrefUserDetail.isfromsupplier, false);
            if (iss) {

                jObj.addProperty("info", "");

                if (field_json_data != null) {

                                 /*   String delivery_partner_info = JsonObjParse.getValueEmpty(field_json_data, "delivery_partner_info");
                                    if(!delivery_partner_info.isEmpty()){
                                        StringBuilder stringBuilder = new StringBuilder();
                                        stringBuilder.append(delivery_partner_info+" , ");
                                        jObj.addProperty("info", stringBuilder.toString());
                                    }*/
                }
                Log.d("okhttp info iss", "empty");


            } else {

                // if its aggre / circle
                // blrmc1(delivery flag of this) under blrmcc(info flag of this)
                String info = SharedPrefUserDetail.getString(IssueMgmtActivity.this, SharedPrefUserDetail.infoMainShop, "");
                //String act = sessionManager.getAggregator_ID();
                String act = sessionManager.getcurrentcaterername();
                //String infoPass = info + " , " + act + " , ";
                StringBuilder stringBuilder = new StringBuilder(); // old is correct
                //if(!info.isEmpty())stringBuilder.append(info+" , ");
                //if(!act.isEmpty())stringBuilder.append(act+" , ");
                if (field_json_data_info != null) {
                    String pass_info_circle = JsonObjParse.getValueEmpty(field_json_data_info, "pass_info_circle");
                    String pass_info_aggregator = JsonObjParse.getValueEmpty(field_json_data_info, "pass_info_aggregator");
                    if (!pass_info_circle.equalsIgnoreCase("no") && business_type_info.equalsIgnoreCase("circle")) {
                        if (!info.isEmpty()) stringBuilder.append(info + " , ");
                    }
                    if (!pass_info_aggregator.equalsIgnoreCase("no") && business_type_info.equalsIgnoreCase("aggregator")) {
                        if (!info.isEmpty()) stringBuilder.append(info + " , ");
                    }
                }

                if (!act.isEmpty()) stringBuilder.append(act + " , ");

                if (field_json_data != null) {

                    String delivery_partner_info = JsonObjParse.getValueEmpty(field_json_data, "delivery_partner_info");
                    if (!delivery_partner_info.isEmpty()) {
                        stringBuilder.append(delivery_partner_info + " , ");
                    }
                }

                Log.d("okhttp info", stringBuilder.toString());
                jObj.addProperty("info", stringBuilder.toString());
                //jObj.addProperty("info", sessionManager.getAggregator_ID());


            }


            jObj.addProperty("header_note", sessionManager.getRemark());
            jObj.addProperty("purpose", purpose);
            jObj.addProperty("chit_item_count", dbHelper.getcartcount());
            jObj.addProperty("total_chit_item_value", String.valueOf(grandtotal));

            JsonObject jo_footer = new JsonObject();
            jo_footer.addProperty("SGST", String.valueOf(totalstategst));
            jo_footer.addProperty("CGST", String.valueOf(totalcentralgst));
            jo_footer.addProperty("VAT", String.valueOf(totalVat));
            jo_footer.addProperty("TOTALBILL", String.valueOf(totalbill));
            jo_footer.addProperty("GRANDTOTAL", String.valueOf(grandtotal));
            jo_footer.addProperty("TOTLREQDISC", String.valueOf(totalReqDisc));
            jo_footer.addProperty("REQ_DISC", String.valueOf(ReqDisc));
            jo_footer.addProperty("TOTLPRODISC", String.valueOf(totalProductDisc));
            jo_footer.addProperty("NAME", loggedin.getFirstname());

            if (totalWithRoundOff > 0) {
                jObj.addProperty("total_chit_item_value", String.valueOf(totalWithRoundOff));
                jo_footer.addProperty("totalWithRoundOff", String.valueOf(totalWithRoundOff));
            }

            String taxArray = new Gson().toJson(alTax);
            jo_footer.addProperty("taxArray", taxArray);


            if (al_uploadedimg.size() > 0) {
                String imgjson = new Gson().toJson(al_uploadedimg);
                jo_footer.addProperty("img", imgjson);
            } else {

                jo_footer.addProperty("img", "");

            }


            jo_footer.addProperty("PAYMENT_MODE", PAYMENT_MODE);


            // pass info

            // Consumer Array

            JsonArray jaConsumerContact = new JsonArray();
            JsonObject joConsumer = new JsonObject();
            String sms_customer = JsonObjParse.getValueEmpty(field_json_data, "sms_customer");
            joConsumer.addProperty("sms", sms_customer);
            joConsumer.addProperty("customer_nm", loggedin.getFirstname());
            joConsumer.addProperty("mobile", sessionManager.getT_Phone());
            jaConsumerContact.add(joConsumer);
            jo_footer.add("ConsumerInfo", jaConsumerContact);

            // Business Array

            JsonArray jaBusinessContact = new JsonArray();
            JsonObject joBusiness = new JsonObject();
            String sms_business = JsonObjParse.getValueEmpty(field_json_data, "sms_status");
            joBusiness.addProperty("sms", sms_business);
            String mobile_business = JsonObjParse.getValueEmpty(field_json_data, "mobile_business");
            joBusiness.addProperty("mobile", mobile_business);
            joBusiness.addProperty("customer_nm", sessionManager.getcurrentcatereraliasname());
            jaBusinessContact.add(joBusiness);

            jo_footer.add("BusinessInfo", jaBusinessContact);

            // Delivery Array

            JsonArray jaDeliveryContact = new JsonArray();
            JsonObject joDelivery = new JsonObject();

            String mobile_delivery = JsonObjParse.getValueEmpty(field_json_data, "mobile_delivery");
            joDelivery.addProperty("mobile", mobile_delivery);
            String companynm = JsonObjParse.getValueEmpty(field_json_data, "customer_nm_delivery");
            joDelivery.addProperty("customer_nm", companynm + "");
            //String pick_up_only = JsonObjParse.getValueEmpty(field_json_data, "pick_up_only");
            if (pick_up_only.equalsIgnoreCase("Yes")) { // from business profile
                joDelivery.addProperty("sms", "no");
                joDelivery.addProperty("pick_up_only", "Yes");
            } else {
                String sms_delivery = JsonObjParse.getValueEmpty(field_json_data, "sms_delivery");
                joDelivery.addProperty("sms", sms_delivery);
                joDelivery.addProperty("pick_up_only", "No");
            }
            jaDeliveryContact.add(joDelivery);
            jo_footer.add("DeliveryInfo", jaDeliveryContact);

            //

            jo_footer.addProperty("DATE", sessionManager.getDate());
            jo_footer.addProperty("ADDRESS", sessionManager.getAddress());
            jo_footer.addProperty("payuResponse", "");
            jo_footer.addProperty("chithashid", hashidval);
            jo_footer.addProperty("field_json_data", field_json_data);

            jObj.addProperty("footer_note", jo_footer.toString());
            updatechit(sessionManager.getsession(), jObj, hashidval);
            //updatechit(sessionManager.getsession(),jObj,hashidval);
        } catch (JsonParseException e) {
            e.printStackTrace();
        }
    }

    private ProgressDialog dialog;
    ArrayList<ModelFile> al_selet = new ArrayList<>();
    ArrayList<String> al_uploadedimg = new ArrayList<>();
    String hashidval = "";

    public void updatechit(String usersession, JsonObject chitdetails, String hashid) {


        // Gson gson = new Gson();
        // gson.toJson(chitdetails.toString());                   // prints 1
        ApiInterface apiService =
                ApiClient.getClient(IssueMgmtActivity.this).create(ApiInterface.class);
        Log.e("beforeapical", chitdetails.toString());
        // String json = chitdetails.toString();
        String json = new Gson().toJson(chitdetails);
        String currency_code = SharedPrefUserDetail.getString(IssueMgmtActivity.this, SharedPrefUserDetail.chit_currency_code, "");
        String currency_code_id = SharedPrefUserDetail.getString(IssueMgmtActivity.this, SharedPrefUserDetail.chit_currency_code_id, "");

        Call call = apiService.updatechit(usersession, chitdetails, sessionManager.getAggregatorprofile(), hashid, "d97be841a6d6cbb66bc3ae165b31b85f", currency_code, currency_code_id, "2");
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {

                if (dialog != null) {
                    dialog.dismiss();
                }

                int statusCode = response.code();
                Log.e("test", " trddsdadass" + response.code());

                Headers headerList = response.headers();
                Log.d("test", statusCode + " ");

                //  String cookie = response.;
                //  Log.d("test", cookie+ " ");


                if (response.isSuccessful()) {
                    orderplacedsuccess();
                    try {
                        String a = new Gson().toJson(response.body());
                        Log.e("response", a);


                    } catch (Exception e) {
                        Log.e("errortest", e.getMessage());

                        Toast.makeText(IssueMgmtActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                    //  Intent i = new Intent(MainActivity.this, MainActivity.class);
                    //  startActivity(i);
                } else {


                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Log.e("test", " trdds1" + jObjErrorresponse.toString());
                        String msg = jObjErrorresponse.getString("errormsg");
                        if (msg.equalsIgnoreCase("To is empty")) {
                            msg = "You cannot order to yourselves.";
                        }
                        Toast.makeText(IssueMgmtActivity.this, msg, Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(IssueMgmtActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }

                //  List<Movie> movies = response.body().getResults();
                //  recyclerView.setAdapter(new MoviesAdapter(movies, R.layout.list_item_movie, getApplicationContext()));
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // Log error here since request failed
                if (dialog != null) {
                    dialog.dismiss();
                }
                Log.e("error message", t.toString());
            }
        });
    }


    public void orderplacedsuccess() {
        sessionManager.setcurrentcaterer("");
        sessionManager.setcurrentcaterername("");
        sessionManager.setCartproduct("");
        sessionManager.setCartcount(0);
        sessionManager.setRemark("");
        sessionManager.setDate("");
        sessionManager.setAddress("");
       /* Intent i= new Intent(InvoicesummaryActivity.this,PaymentActivity.class);
        startActivity(i);*/
        dbHelper.deleteallfromcart();


        finish();

        sessionManager.setAggregator_ID(getString(R.string.Aggregator));
        SharedPrefUserDetail.setBoolean(IssueMgmtActivity.this, SharedPrefUserDetail.isfromsupplier, false);
        SharedPrefUserDetail.setString(IssueMgmtActivity.this, SharedPrefUserDetail.isfrommyaggregator, "");


        Intent i = new Intent(IssueMgmtActivity.this, HistoryActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        i.putExtra("session", sessionManager.getsession());
        i.putExtra("purpose", purpose);
        startActivity(i);

    }


    AmazonS3 s3;
    TransferUtility transferUtility;

    public void inits3() {
        s3 = new AmazonS3Client(new BasicAWSCredentials(getString(R.string.accesskey), getString(R.string.secretkey)));
        transferUtility = new TransferUtility(s3, getApplicationContext());

    }

    String namegsxsax = "";

    public void uploadimg() {
        rl_upload.setVisibility(View.VISIBLE);
        int cc = c_img + 1;
        int total = CompressItemSelect.size();
        tv_progress.setText("Uploading " + cc + "/" + total);


        File file = new File(CompressItemSelect.get(c_img));
        namegsxsax = "ISSUEIMG_" + System.currentTimeMillis() + ".jpg";


        TransferObserver transferObserver = transferUtility.upload("cab-videofiles", namegsxsax, file);
        transferObserverListener(transferObserver);


    }

    int c_img = 0;
    String[] str_img = new String[]{};

    public void transferObserverListener(final TransferObserver transferObserver) {

        transferObserver.setTransferListener(new TransferListener() {
            @Override
            public void onStateChanged(int id, TransferState state) {
                if (state.name().equals("COMPLETED")) {


                    str_img[c_img] = getString(R.string.baseaws) + namegsxsax;
                    al_uploadedimg.add(str_img[c_img]);

                    c_img++;
                    if (c_img < al_selet.size()) {
                        uploadimg();
                    } else {
                        rl_upload.setVisibility(View.GONE);
                        c_img = 0;
                        usersignin(sessionManager.getsession(), sessionManager.getDate(), sessionManager.getAddress());

                        //confirmorder("");


                        //putdata();

                    }
                } else if (state.name().equals("FAILED")) {
                    rl_upload.setVisibility(View.GONE);
                    Toast.makeText(IssueMgmtActivity.this, "Failed Uploading !", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                try {
                    int percentage = (int) (bytesCurrent / bytesTotal * 100);


                    //mProgressDialog.setProgress(percentage);


                    //Log.e("percentage ", " : " + percentage);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(int id, Exception ex) {

                Log.e("error", "error");
            }

        });
    }

    @Override
    public void onBackPressed() {

        if (findViewById(R.id.rl_pb_cat).getVisibility() == View.VISIBLE || findViewById(R.id.rl_upload).getVisibility() == View.VISIBLE) {

        } else if (llAddressList.getVisibility() == View.VISIBLE) {
            llAddressList.setVisibility(View.GONE);
        } else {
            super.onBackPressed();
        }
    }


    ArrayList<String> CompressItemSelect;

    public class ImageCompress extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            CompressItemSelect = new ArrayList();
            findViewById(R.id.rl_pb_cat).setVisibility(View.VISIBLE);

        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                for (int i = 0; i < al_selet.size(); i++) {

                    File ImageFile = new File(al_selet.get(i).getImgpath());
                    File compressedImageFile = null;
                    String strFile = "";
                    try {
                        compressedImageFile = new Compressor(IssueMgmtActivity.this).compressToFile(ImageFile);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    strFile = String.valueOf(compressedImageFile);

                    CompressItemSelect.add(strFile);

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            findViewById(R.id.rl_pb_cat).setVisibility(View.GONE);

            uploadimg();
        }
    }

    @BindView(R.id.rl_upload)
    RelativeLayout rl_upload;

    @BindView(R.id.tv_progress)
    TextView tv_progress;


    @Override
    protected void onResume() {
        super.onResume();

        productcount = dbHelper.getcartcount();
        if (productcount == 0) {
            tv_badge.setVisibility(View.GONE);
        } else {
            tv_badge.setVisibility(View.VISIBLE);
            tv_badge.setText(productcount + "");
        }
        setadd_phone();
    }


    public void setadd_phone() {

        try {
            Userprofile loggedin = null;

            try {
                JSONObject userdetails = new JSONObject(sessionManager.getUserprofile());
                loggedin = new Gson().fromJson(userdetails.toString(), Userprofile.class);

            } catch (JSONException e) {
                e.printStackTrace();
            }

            et_phone.setText(loggedin.getContact_no() + "");
            tv_google_loc.setText(loggedin.getLocation() + "");

            if (loggedin.getLocation() != null && !loggedin.getLocation().isEmpty()) {
                try {
                    JSONArray array = new JSONArray(loggedin.getLocation());
                    String type, address;
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject obj = array.getJSONObject(i);
                        type = obj.getString("addressType");
                        address = obj.getString("address");
                        addressList.add(new AddAddress(type, address));
                    }
                    tv_google_loc.setText(addressList.get(0).getAddress());
                    mAdapter = new AddressAdapter(addressList, this, true);
                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
                    rvAddressList.setLayoutManager(mLayoutManager);
                    rvAddressList.setAdapter(mAdapter);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


            etSmsInfo.setText(loggedin.getFirstname() + ", " + loggedin.getContact_no() + ", " + loggedin.getLocation());


        } catch (Exception e) {

            e.printStackTrace();
        }
    }

    /// Charges


    Double charge1 = 0.0, charge2 = 0.0;
    String field_json_data = "", business_type = "";

    String tvChargeNm1 = "";

    public void setCharges() {

        try {

            if (field_json_data != null && !field_json_data.isEmpty()) {

                JSONObject joField = new JSONObject(field_json_data);
                JSONArray arr = joField.getJSONArray("charges");

                JSONObject element;

                tvChargeNm1 = "";

                for (int i = 0; i < arr.length(); i++) {
                    element = arr.getJSONObject(i); // which for example will be Types,TotalPoints,ExpiringToday in the case of the first array(All_Details)

                    Iterator keys = element.keys();
                    while (keys.hasNext()) {
                        try {
                            String key = (String) keys.next();

                            String taxVal = JsonObjParse.getValueFromJsonObj(element, key);

                            if (tvChargeNm1.isEmpty()) {
                                if (taxVal.isEmpty()) taxVal = "0";
                                charge1 = Double.valueOf(taxVal);
                                tvChargeNm1 = key;

                            } else {
                                if (taxVal.isEmpty()) taxVal = "0";
                                charge2 = Double.valueOf(taxVal);

                            }

                            Log.e("key", key);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

                //grandtotal = grandtotal + charge1 + charge2;

                setIsPickUp();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    String pick_up_only;

    public void setIsPickUp() {

        pick_up_only = JsonObjParse.getValueEmpty(field_json_data, "pick_up_only");

        if (pick_up_only.equalsIgnoreCase("Yes")) { // from business profile

            grandtotal = grandtotal;
           /* findViewById(R.id.rl_address).setVisibility(View.GONE);
            llCharge1.setVisibility(View.GONE);
            llCharge2.setVisibility(View.GONE);

            txt_add.setText("Pickup Only : ");

            grandtotalamount.setText(currency + Inad.getCurrencyDecimal(grandtotal, InvoicesummaryActivity.this));

            cbIsPickUpOnly.setChecked(true);
            cbIsPickUpOnly.setVisibility(View.GONE);
            llCharge1.setVisibility(View.GONE);
            llCharge2.setVisibility(View.GONE);*/
        } else {

            grandtotal = grandtotal + charge1 + charge2;

        }

        totalWithRoundOff = 0.0;

        String is_roundoff_near = JsonObjParse.getValueEmpty(field_json_data, "is_roundoff_near");

        if (is_roundoff_near.equalsIgnoreCase("yes")) {

            totalWithRoundOff = (double) Math.round(grandtotal);

            if (totalWithRoundOff.doubleValue() == grandtotal.doubleValue()) {
                totalWithRoundOff = 0.0;
            }
        }

    }


    MyRequestCall myRequestCall = new MyRequestCall();
    MyRequst myRequst = new MyRequst();

    public void checkIsAnyRecord() {

        findViewById(R.id.rl_pb_cat).setVisibility(View.VISIBLE);

        myRequst.cookie = sessionManager.getsession();
        myRequst.aggre = sessionManager.getAggregatorprofile();
        myRequst.frombridgeidval = item.getBridgeId();

        if (item.getContact_bridge_id() == null) // circle - aggre (from catererfragment)
        {
            item.setContact_bridge_id(item.getBridgeId());
        }

        myRequst.bridgeidval = item.getContact_bridge_id();
        myRequst.currency_code = item.getCurrencyCode();
        myRequst.page = 1;


        myRequestCall.getCatalogueList(this, myRequst, new MyRequestCall.CallRequest() {
            @Override
            public void onGetResponse(String response) {

                try {
                    JSONObject jObjRes = new JSONObject(response);
                    JSONObject jObjdata = jObjRes.getJSONObject("response");
                    JSONObject jObjResponse = jObjdata.getJSONObject("data");
                    String totalRecord = jObjResponse.getString("totalRecord");

                    flCart.setVisibility(View.VISIBLE);
                    if (totalRecord.equals("0")) flCart.setVisibility(View.GONE);


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                findViewById(R.id.rl_pb_cat).setVisibility(View.GONE);


            }
        });


    }


    public void checkDiscount() {
        String str_productTotalDiscount = JsonObjParse.getValueEmpty(field_json_data, "productTotalDiscount");
        ProductTotalDiscount productTotalDiscount = new Gson().fromJson(str_productTotalDiscount, ProductTotalDiscount.class);

        if (productTotalDiscount == null)
            productTotalDiscount = new ProductTotalDiscount();


        if (productTotalDiscount.product_total_discount_type_of.equalsIgnoreCase("range")) {

            ArrayList<ProductTotalDiscount> alRange = new ArrayList<>();
            alRange = productTotalDiscount.alRange;
            totalReqDisc = 0.0;
            for (int i = 0; i < alRange.size(); i++) {

                productTotalDiscount = alRange.get(i);
                String min_range = productTotalDiscount.min_range;
                String max_range = productTotalDiscount.max_range;
                String disc_range = productTotalDiscount.disc_range;

                Double minR = Double.parseDouble(min_range);
                Double maxR = Double.parseDouble(max_range);
                Double disR = Double.parseDouble(disc_range);


                if (totalbill.doubleValue() >= minR.doubleValue() && totalbill.doubleValue() <= maxR.doubleValue()) {

                    totalReqDisc = (totalbill / 100.0f) * disR;
                    setReqDiscLbl(disR);
                    break;
                }

            }


        } else { // defauult check

            String disc_perc = productTotalDiscount.disc_perc;
            String max_disc_val = productTotalDiscount.max_disc_val;
            String min_purchase_val = productTotalDiscount.min_purchase_val;

            boolean isDiscountPossible = true;
            if (disc_perc.isEmpty()) {
                isDiscountPossible = false;
            } else {
                Double discVal = Double.parseDouble(disc_perc);
                Double minPurcVal = 0.0, maxDiscVal = 0.0;
                if (!min_purchase_val.isEmpty()) {
                    minPurcVal = Double.parseDouble(min_purchase_val);
                }
                if (!max_disc_val.isEmpty()) {
                    maxDiscVal = Double.parseDouble(max_disc_val);
                }

                if (minPurcVal >= 0) {

                    if (totalbill.doubleValue() < minPurcVal.doubleValue()) {
                        isDiscountPossible = false;
                    } else {

                        if (isDiscountPossible) {

                            totalReqDisc = (totalbill / 100.0f) * discVal;
                            if (maxDiscVal.doubleValue() > 0 && totalReqDisc.doubleValue() > maxDiscVal.doubleValue()) {
                                setReqDiscLbl(-1);
                                totalReqDisc = maxDiscVal;
                            } else {
                                setReqDiscLbl(discVal);
                                totalReqDisc = totalReqDisc;
                            }
                        }
                    }
                }
            }
        }

        Double totalSavings = totalProductDisc + totalReqDisc;
    }

    public void setReqDiscLbl(double disc) {
        ReqDisc = disc;
    }

    @Override
    public void onItemClick(int pos) {
        tv_google_loc.setText(addressList.get(pos).getAddress());
        llAddressList.setVisibility(View.GONE);
    }
}