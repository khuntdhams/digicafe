package com.cab.digicafe.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.cab.digicafe.Helper.Inad;
import com.cab.digicafe.Helper.SessionManager;
import com.cab.digicafe.LoginActivity;
import com.cab.digicafe.R;
import com.cab.digicafe.Rest.ApiClient;
import com.cab.digicafe.Rest.ApiInterface;
import com.cab.digicafe.SignupActivity;
import com.google.gson.Gson;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotPwdActivity extends MyAppBaseActivity {

    EditText username;
    EditText et_email;
    TextInputLayout usernamewrapper;
    TextInputLayout til_email;
    String str_username, str_email;
    SessionManager sessionManager;

    TextView tv_login, tv_signup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_pwd);

        username = (EditText) findViewById(R.id.username);
        et_email = (EditText) findViewById(R.id.et_email);
        usernamewrapper = (TextInputLayout) findViewById(R.id.usernameWrapper);
        til_email = (TextInputLayout) findViewById(R.id.til_email);
        tv_login = (TextView) findViewById(R.id.tv_login);
        tv_signup = (TextView) findViewById(R.id.tv_signup);
        sessionManager = new SessionManager(this);

        findViewById(R.id.iv_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        tv_login.setPaintFlags(tv_login.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        tv_signup.setPaintFlags(tv_signup.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);


        findViewById(R.id.tv_login).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ForgotPwdActivity.this, LoginActivity.class);
                startActivity(i);
            }
        });

        findViewById(R.id.tv_signup).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ForgotPwdActivity.this, SignupActivity.class);
                startActivity(i);
            }
        });

        findViewById(R.id.tv_send).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                str_username = username.getText().toString().trim();
                str_email = et_email.getText().toString().trim();


                String emailregex =
                        "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";


                if (str_username == null || str_username.length() == 0) {
                    usernamewrapper.setError("Please enter username!");
                } else if (str_email == null || str_email.length() == 0) {
                    til_email.setError("Please enter email!");
                } else if (str_email.matches(emailregex) == false) {
                    til_email.setError("Email address must be valid required!");
                } else if (str_username.contains("@") && str_username.contains(".br")) {
                    Inad.alerter("Message !", "Please contact your employer to reset password", ForgotPwdActivity.this);
                } else {
                    usersignin();
                }
            }
        });
    }

    private ProgressDialog dialog;
    String u_name = "";

    public void usersignin() {
        dialog = new ProgressDialog(ForgotPwdActivity.this);
        dialog.setMessage("Loading...");
        dialog.show();

        ApiInterface apiService =
                ApiClient.getClient(ForgotPwdActivity.this).create(ApiInterface.class);
        u_name = "";
        if (str_username.contains("@") && str_username.contains(".br")) {  // employee - athi@blrmc2.br
            u_name = str_username;
        } else if (str_username.matches("[0-9.]*"))   // consumer - all digits
        {
            u_name = str_username + "@" + sessionManager.getAggregator_ID() + ".cr";
        } else {  // business
            u_name = str_username;
        }

        //final String u_name = str_username + "@" + sessionManager.getAggregator_ID() + ".cr";


        Call call = apiService.forgotpwd(u_name, str_email);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                dialog.dismiss();
                if (response.isSuccessful()) {
                    try {
                        String a = new Gson().toJson(response.body());
                        Toast.makeText(ForgotPwdActivity.this, "Password sent successfully!", Toast.LENGTH_LONG).show();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    Intent i = new Intent(ForgotPwdActivity.this, LoginActivity.class);
                    i.putExtra("username", str_username);
                    startActivity(i);
                } else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Toast.makeText(ForgotPwdActivity.this, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(ForgotPwdActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }

            }

            @Override
            public void onFailure(Call call, Throwable t) {
                dialog.dismiss();
            }
        });
    }


}
