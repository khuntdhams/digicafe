package com.cab.digicafe.Activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cab.digicafe.Activities.RequestApis.MyRequestCall;
import com.cab.digicafe.Activities.RequestApis.MyRequst;
import com.cab.digicafe.Activities.RequestApis.MyResponse.CategoryViewModel;
import com.cab.digicafe.Adapter.CategoryAddEditListAdapter;
import com.cab.digicafe.Helper.SessionManager;
import com.cab.digicafe.Model.ShareModel;
import com.cab.digicafe.Model.SupplierCategory;
import com.cab.digicafe.MyCustomClass.Utils;
import com.cab.digicafe.R;
import com.cab.digicafe.Rest.ApiClient;
import com.cab.digicafe.Rest.ApiInterface;
import com.cab.digicafe.Rest.Request.CatRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.xiaofeng.flowlayoutmanager.Alignment;
import com.xiaofeng.flowlayoutmanager.FlowLayoutManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditCatlogueCategoryActivity extends AppCompatActivity {

    CatRequest catalougeContent = new CatRequest();

    MyRequestCall myRequestCall = new MyRequestCall();
    MyRequst myRequst = new MyRequst();
    boolean isdataavailable = true;
    private ArrayList<CategoryViewModel> alCategory_new = new ArrayList<>();
    Context context;
    SessionManager sessionManager;
    String sortBy = "";
    int sort = 0;
    int cat = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_catlogue_category);
        ButterKnife.bind(this);

        context = this;
        sessionManager = new SessionManager(context);
        myRequst.cookie = sessionManager.getsession();

        catalougeContent = ShareModel.getI().catRequest;

        setCategoryRv();


        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        iv_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // out of stock
                callCat();
            }
        });
    }

    @BindView(R.id.rvCat)
    RecyclerView rvCat;

    @BindView(R.id.iv_back)
    ImageView iv_back;

    @BindView(R.id.iv_save)
    ImageView iv_save;

    @BindView(R.id.tvSelectCat)
    TextView tvSelectCat;


    ArrayList<SupplierCategory> alCategory = new ArrayList<>();
    CategoryAddEditListAdapter categoryAddEditListAdapter;

    boolean isLinear = false;
    String category;
    StringBuilder stringBuilder;

    public void setCategoryRv() {

        alCategory = new Gson().fromJson(ShareModel.getI().alCategory, new TypeToken<ArrayList<SupplierCategory>>() {
        }.getType());

        if (catalougeContent.getOlddata().size() > 0) {
            category = catalougeContent.getOlddata().get(0).getCategory();
        } else if (catalougeContent.getNewdata().size() > 0) {

            Gson gson = new GsonBuilder().create();
            String json = gson.toJson(catalougeContent);// obj is your object


            JSONObject joMain = null;
            try {
                joMain = new JSONObject(json);

                String newdata = joMain.getString("newdata");

                JSONArray jaNewData = new JSONArray(newdata);
                JSONObject jo = jaNewData.getJSONObject(0);
                //jo.put("category",stringBuilder+"");

                category = jo.getString("category");


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        String[] separated = category.split("; ");

        for (int i = 0; i < alCategory.size(); i++) {

            for (int j = 0; j < separated.length; j++) {

                if (separated[j].equalsIgnoreCase(alCategory.get(i).getPricelistCategoryName())) {
                    alCategory.get(i).setChecked(true);
                    break;
                } else {
                    alCategory.get(i).setChecked(false);
                }

            }
        }

        if (alCategory.size() > 0 && alCategory.get(0).getPricelistCategoryName().equalsIgnoreCase("All"))
            alCategory.remove(0);

        categoryAddEditListAdapter = new CategoryAddEditListAdapter(alCategory, this, new CategoryAddEditListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(SupplierCategory item) {


                stringBuilder = new StringBuilder();
                for (int i = 0; i < alCategory.size(); i++) {
                    if (alCategory.get(i).isChecked()) {
                        if (stringBuilder.toString().isEmpty()) {
                            stringBuilder.append("" + alCategory.get(i).getPricelistCategoryName());
                        } else {
                            stringBuilder.append("; " + alCategory.get(i).getPricelistCategoryName());
                        }
                    }

                }


                tvSelectCat.setText("Select Category : " + stringBuilder + "");


            }
        });

        if (isLinear) {
            rvCat.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        } else {
            FlowLayoutManager layoutManager = new FlowLayoutManager();
            layoutManager.setAutoMeasureEnabled(true);
            layoutManager.setAlignment(Alignment.LEFT);
            rvCat.setLayoutManager(layoutManager);
        }
        //

        rvCat.setItemAnimator(new DefaultItemAnimator());
        rvCat.setAdapter(categoryAddEditListAdapter);
        rvCat.setNestedScrollingEnabled(false);

    }

    public void putData(CatRequest jo_cat) {

        findViewById(R.id.rl_pb_cat).setVisibility(View.VISIBLE);

        ApiInterface apiService =
                ApiClient.getClient(EditCatlogueCategoryActivity.this).create(ApiInterface.class);
        sessionManager = new SessionManager(this);

        Call call = apiService.PutCatalogue(sessionManager.getsession(), jo_cat);

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                findViewById(R.id.rl_pb_cat).setVisibility(View.GONE);

                if (response.isSuccessful()) {
                    try {
                        //ll_edit.setVisibility(View.VISIBLE);
                        //ll_save.setVisibility(View.GONE);
                        //edit();

                        if (catalougeContent.getNewdata().size() > 0) { // Add New

                        } else if (catalougeContent.getOlddata().size() > 0) { // Edit Old

                        }

                        Intent intent = new Intent();
                        intent.putExtra("isAddNew", true);
                        setResult(Activity.RESULT_OK, intent);
                        finish();

                    } catch (Exception e) {


                    }

                } else {

                    try {

                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Log.e("response_message", jObjError.toString());

                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        String errormessage = jObjErrorresponse.getString("errormsg");
                        Toast.makeText(EditCatlogueCategoryActivity.this, errormessage, Toast.LENGTH_LONG).show();

                    } catch (Exception e) {
                        Toast.makeText(EditCatlogueCategoryActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }

            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // Log error here since request failed
                findViewById(R.id.rl_pb_cat).setVisibility(View.GONE);

            }
        });
    }

    public void callCat() {
        findViewById(R.id.rl_pb_cat).setVisibility(View.VISIBLE);
        myRequestCall.getCategoryUnderMaster(context, myRequst, new MyRequestCall.CallRequest() {
            @Override
            public void onGetResponse(String response) {
                findViewById(R.id.rl_pb_cat).setVisibility(View.GONE);
                try {
                    JSONObject jObjRes = new JSONObject(response);

                    JSONObject jObjdata = jObjRes.getJSONObject("response");
                    JSONObject jObjResponse = jObjdata.getJSONObject("data");
                    String totalRecord = jObjResponse.getString("totalRecord");

                    JSONArray jObjcontacts = jObjResponse.getJSONArray("content");

                    String displayEndRecord = jObjResponse.getString("displayEndRecord");
                    if (totalRecord.equals(displayEndRecord)) {
                        isdataavailable = false;
                    } else {
                        isdataavailable = true;
                    }

                    ArrayList<CategoryViewModel> tmpAlCategory = new ArrayList<>();

                    for (int i = 0; i < jObjcontacts.length(); i++) {
                        if (jObjcontacts.get(i) instanceof JSONObject) {
                            JSONObject jsnObj = (JSONObject) jObjcontacts.get(i);
                            CategoryViewModel obj = new Gson().fromJson(jsnObj.toString(), CategoryViewModel.class);
                            try {
                                sort = Integer.parseInt(obj.sortBy);
                                sort++;
                                sortBy = sort + "";
                            } catch (NumberFormatException e) {
                                e.printStackTrace();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            tmpAlCategory.add(obj);
                        }
                    }
                    alCategory_new.addAll(tmpAlCategory);
                    if(tmpAlCategory.size()==0){
                        sortBy = 1 + "";
                    }
                    addCat();

                } catch (JSONException e) {
                    e.printStackTrace();
                    addCat();
                } catch (Exception e) {
                    e.printStackTrace();
                    addCat();
                }
            }
        });

    }


    public void addCat() {
        findViewById(R.id.rl_pb_cat).setVisibility(View.VISIBLE);

        if (cat < alCategory.size()) {
            if (alCategory.get(cat).isChecked()) {
                if (alCategory_new.contains(new CategoryViewModel(alCategory.get(cat).getPricelistCategoryName()))) {
                    cat++;
                    addCat();
                } else {
                    JSONObject sendobj = new JSONObject();
                    JSONArray jaNewData1 = new JSONArray();
                    JSONObject a1 = new JSONObject();
                    try {
                        a1.put("flag", "add");
                        a1.put("pricelist_category_name", alCategory.get(cat).getPricelistCategoryName());
                        a1.put("pricelist_category_image", alCategory.get(cat).pricelist_category_image);
                        a1.put("sort_by", sortBy);
                        jaNewData1.put(a1);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    JSONArray a = new JSONArray();
                    try {
                        sendobj.put("newdata", jaNewData1);
                        sendobj.put("olddata", a);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    JSONObject object = sendobj;
                    JsonParser jsonParser = new JsonParser();
                    JsonObject gsonObject = (JsonObject) jsonParser.parse(object.toString());

                    myRequestCall.addCategoryUnderMaster(context, myRequst, gsonObject, new MyRequestCall.CallRequest() {
                        @Override
                        public void onGetResponse(String response) {
                            findViewById(R.id.rl_pb_cat).setVisibility(View.GONE);

                            if (sortBy.equalsIgnoreCase("")) {

                                sortBy = 1 + "";
                            } else {
                                sort = Integer.parseInt(sortBy) + 1;
                                sortBy = sort + "";
                            }

                            if (!response.isEmpty()) {
                                cat++;
                                addCat();
                                Log.d("response", response + "");
                            } else {
                                Log.d("response", response + "");
                            }
                        }
                    });
                }
            } else {
                cat++;
                addCat();
            }

        } else {
            saveData();
        }
    }

    public void saveData() {
        try {
            if (catalougeContent.getNewdata().size() > 0) { // Add New

                Gson gson = new GsonBuilder().create();
                String json = gson.toJson(catalougeContent);// obj is your object

                JSONObject joMain = new JSONObject(json);
                String newdata = joMain.getString("newdata");

                JSONArray jaNewData = new JSONArray(newdata);
                JSONObject jo = jaNewData.getJSONObject(0);
                jo.put("category", stringBuilder + "");

                joMain.put("newdata", jaNewData);

                if (stringBuilder != null)
                    catalougeContent = new Gson().fromJson(joMain.toString(), CatRequest.class);


                // if(stringBuilder!=null)catalougeContent.getNewdata().get(0).setCategory(stringBuilder+"");
            } else if (catalougeContent.getOlddata().size() > 0) { // Edit Old
                if (stringBuilder != null)
                    catalougeContent.getOlddata().get(0).setCategory(stringBuilder + "");
            }

            String jsn = new Gson().toJson(catalougeContent, CatRequest.class);

            Log.e("ja", jsn);
            putData(catalougeContent);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
