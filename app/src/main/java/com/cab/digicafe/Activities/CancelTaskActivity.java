package com.cab.digicafe.Activities;


import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.andremion.counterfab.CounterFab;
import com.cab.digicafe.Adapter.MyTaskAdapter;
import com.cab.digicafe.BaseActivity;
import com.cab.digicafe.ChitlogActivity;
import com.cab.digicafe.DetailsFragment;
import com.cab.digicafe.Dialogbox.CustomDialogClass;
import com.cab.digicafe.Fragments.SummaryDetailFragmnet;
import com.cab.digicafe.Helper.Inad;
import com.cab.digicafe.Helper.RecyclerItemTouchHelper3;
import com.cab.digicafe.Helper.SessionManager;
import com.cab.digicafe.Model.Chit;
import com.cab.digicafe.MyCustomClass.RefreshSession;
import com.cab.digicafe.R;
import com.cab.digicafe.Rest.ApiClient;
import com.cab.digicafe.Rest.ApiInterface;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import info.hoang8f.android.segmented.SegmentedGroup;
import okhttp3.Headers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CancelTaskActivity extends BaseActivity implements SearchView.OnQueryTextListener, MyTaskAdapter.OnItemClickListener, SwipyRefreshLayout.OnRefreshListener, RecyclerItemTouchHelper3.RecyclerItemTouchHelperListener {

    private RecyclerView recyclerView;
    private MyTaskAdapter mAdapter;
    CounterFab counterFab;
    private List<Chit> orderlist = new ArrayList<>();
    SessionManager sessionManager;
    String sessionString;
    MenuItem item;
    Menu menu;
    int totalcount = 0;
    String str_actionid = "";
    RadioButton openstatus, Close_, Archive_;
    SegmentedGroup s_group;
    String tab = "closedtask";

    List<Chit> temp_opentask = new ArrayList<>();
    List<Chit> temp_closetask = new ArrayList<>();

    boolean isbusiness = false;

    @Override
    public void onTouchSwipe(boolean isSwipeEnable) {
        mSwipyRefreshLayout.setEnabled(isSwipeEnable);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.content_mytask, frameLayout);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        refreshlist();

        sessionManager = new SessionManager(this);

        getSupportActionBar().setTitle("Cancelled Task");

        isbusiness = sessionManager.getisBusinesslogic();

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        s_group = (SegmentedGroup) findViewById(R.id.segmented2);

        s_group.setVisibility(View.GONE);
        sessionManager = new SessionManager(this);
        // listen refresh event
        counterFab = (CounterFab) findViewById(R.id.counter_fab);
        counterFab.setVisibility(View.GONE);

        openstatus = (RadioButton) findViewById(R.id.openstatus);
        Close_ = (RadioButton) findViewById(R.id.closestatus);

        Archive_ = (RadioButton) findViewById(R.id.inprogressstatus);

        Archive_.setVisibility(View.GONE);


        openstatus.setText("Open");
        Close_.setText("Close");


        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                // bridgeidval = null;
            } else {
                sessionString = extras.getString("session");
                //  bridgeidval= extras.getString("bridgeidval");

            }
        } else {
            sessionString = (String) savedInstanceState.getSerializable("session");
            // bridgeidval= (String) savedInstanceState.getSerializable("bridgeidval");
        }


        s_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {


                findViewById(R.id.rl_pb_ctask).setVisibility(View.VISIBLE);

                switch (checkedId) {
                    case R.id.openstatus:
                        tab = "new_all";
                        break;
                    case R.id.closestatus:
                        tab = "closedtask";
                        break;
                    default:
                        break;
                }


                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(CancelTaskActivity.this);
                recyclerView.setLayoutManager(mLayoutManager);
                //recyclerView.setItemAnimator(new DefaultItemAnimator());

                mAdapter = new MyTaskAdapter(orderlist, CancelTaskActivity.this, CancelTaskActivity.this, tab, isbusiness);
                recyclerView.setAdapter(mAdapter);
                new ItemTouchHelper(new RecyclerItemTouchHelper3(0, 0, CancelTaskActivity.this)).attachToRecyclerView(recyclerView);

                recyclerView.setNestedScrollingEnabled(false);
                isdataavailable = true;
                pagecount = 1;

                usersignin(sessionString, 1, false);

               /* if (totalcount > 0) {
                    try {
                        for (int i = 0; i < orderlist.size(); i++) {
                            if (orderlist.get(i).isIslongpress()) {
                                orderlist.get(i).setIslongpress(false);
                            }
                        }

                        totalcount = 0;
                        str_actionid = "";

                        MenuItem item1 = menu.findItem(R.id.action_delete);
                        item1.setVisible(false);
                        mAdapter.notifyDataSetChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }


                setcte();*/


            }
        });

        Close_.setChecked(true);


    }

    @Override
    public void onItemClick(Chit item) {
        Intent i = new Intent(CancelTaskActivity.this, ChitlogActivity.class);
        i.putExtra("chit_hash_id", item.getChit_hash_id());
        i.putExtra("chit_id", item.getChit_id() + "");
        i.putExtra("transtactionid", item.getChit_name() + "");
        i.putExtra("productqty", item.getTotal_chit_item_value() + "");
        i.putExtra("productprice", item.getChit_item_count() + "");
        i.putExtra("purpose", item.getPurpose());
        i.putExtra("Refid", item.getRef_id());
        i.putExtra("Caseid", item.getCase_id());
        i.putExtra("iscomment", true);
        DetailsFragment.chititemcount = "" + item.getChit_item_count();
        DetailsFragment.chititemprice = "" + item.getTotal_chit_item_value();
        SummaryDetailFragmnet.item = item;
        DetailsFragment.item_detail = item;
        startActivity(i);
    }

    @Override
    public void onItemLongClick(Chit item, int pos) {

        if (orderlist.get(pos).isIslongpress()) {
            orderlist.get(pos).setIslongpress(false);
        } else {
            orderlist.get(pos).setIslongpress(true);
        }

        boolean isanyitem = false;
        str_actionid = "";
        totalcount = 0;
        for (int i = 0; i < orderlist.size(); i++) {
            if (orderlist.get(i).isIslongpress()) {
                totalcount++;
                isanyitem = true;
                str_actionid = str_actionid + String.valueOf(orderlist.get(i).getChit_id()) + ",";
            }
        }

        mAdapter.notifyDataSetChanged();


        Log.e("totalcount", String.valueOf(totalcount));
        MenuItem item1 = menu.findItem(R.id.action_delete);
        if (totalcount > 0) {
            item1.setVisible(true);
        } else {
            item1.setVisible(false);
        }

    }

    @Override
    public void onRefresh(SwipyRefreshLayoutDirection direction) {
        //mSwipyRefreshLayout.setRefreshing(false);

        Log.e("scroll direction", "Refresh triggered at "
                + (direction == SwipyRefreshLayoutDirection.TOP ? "top" : "bottom"));
        if (direction == SwipyRefreshLayoutDirection.TOP) {
            isdataavailable = true;
            pagecount = 1;
            usersignin(sessionString, pagecount, false);
        } else if (direction == SwipyRefreshLayoutDirection.BOTTOM) {
            if (isdataavailable) {
                pagecount++;
                usersignin(sessionString, pagecount, false);
            } else {
                mSwipyRefreshLayout.setRefreshing(false);
                Toast.makeText(this, "No more data available", Toast.LENGTH_LONG).show();
            }

        }
    }

    SwipyRefreshLayout mSwipyRefreshLayout;
    int pagecount = 1;
    boolean isdataavailable = true;
    int pagesize = 50;

    public void refreshlist() {
        mSwipyRefreshLayout = (SwipyRefreshLayout) findViewById(R.id.swipyrefreshlayout);
        mSwipyRefreshLayout.setDirection(SwipyRefreshLayoutDirection.BOTH);
        mSwipyRefreshLayout.setOnRefreshListener(this);
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {


        if (viewHolder instanceof MyTaskAdapter.MyViewHolder) {

            findViewById(R.id.rl_pb_ctask).setVisibility(View.VISIBLE);

            final Chit deletedItem = orderlist.get(viewHolder.getAdapterPosition());
            final int deletedIndex = viewHolder.getAdapterPosition();
            mAdapter.removeItem(viewHolder.getAdapterPosition());

            String actionid = String.valueOf(deletedItem.getChit_id());
            actionid = actionid + ",";

            String actionCode = "";

            String msg = "";


            if (tab.equalsIgnoreCase("closedtask")) {

                if (direction == ItemTouchHelper.RIGHT) {

                    /*final String f_actionid = actionid;
                    final String f_actionCode = actionCode;
                    String employee_bridge_id = "";
                    assignarchived(sessionString, actionid, employee_bridge_id, msg, new MyCallback() {
                        @Override
                        public void callbackCall(List<Chit> filteredModelList) {

                        }

                        @Override
                        public void callback_remove() {

                            pagecount = 1;
                            isdataavailable = true;
                            usersignin(sessionString, pagecount, true);

                        }
                    });*/

                    removearchived(sessionString, actionid, true, "5");


                }

            }


        }
    }

    public void removearchived(final String usersession, String chitIds, final boolean isswipe, String actionCode) {

        ApiInterface apiService =
                ApiClient.getClient(CancelTaskActivity.this).create(ApiInterface.class);
        JsonObject a1 = new JsonObject();
        try {

            a1.addProperty("chitIds", chitIds);
            a1.addProperty("actionCode", actionCode);

        } catch (JsonParseException e) {
            e.printStackTrace();
        }
        // Toast.makeText(AllTaskActivity.this, actionCode, Toast.LENGTH_LONG).show();

        //Call call = apiService.removechit(usersession,chitIds+",","10");
        Call call = apiService.removechit(usersession, a1);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                int statusCode = response.code();
                Log.e("test", sessionString + "sessionstring");

                Headers headerList = response.headers();
                Log.d("testhistory", statusCode + " ");

                if (response.isSuccessful()) {
                    try {
                        Log.e("response.body()", response.body().toString());
                        String a = new Gson().toJson(response.body());

                        JSONObject jo_res = new JSONObject(a);
                        String response_ = jo_res.getString("response");
                        JSONObject jo_txt = new JSONObject(response_);
                        String actionTxt = jo_txt.getString("actionTxt");


                        Toast.makeText(CancelTaskActivity.this, actionTxt, Toast.LENGTH_LONG).show();


                    } catch (Exception e) {
                        Log.e("errortest", e.getMessage());

                    }

                } else {
                    Log.e("test", " trdds1");

                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Toast.makeText(CancelTaskActivity.this, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(CancelTaskActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }

                totalcount = 0;
                str_actionid = "";
                MenuItem item1 = menu.findItem(R.id.action_delete);
                item1.setVisible(false);
                pagecount = 1;
                isdataavailable = true;
                usersignin(usersession, pagecount, true);

            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // Log error here since request failed
                findViewById(R.id.rl_pb_ctask).setVisibility(View.GONE);
                Log.e("error message", t.toString());
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (totalcount > 0) {
            try {
                for (int i = 0; i < orderlist.size(); i++) {
                    if (orderlist.get(i).isIslongpress()) {
                        orderlist.get(i).setIslongpress(false);
                    }
                }

                totalcount = 0;
                str_actionid = "";

                MenuItem item1 = menu.findItem(R.id.action_delete);
                item1.setVisible(false);
                mAdapter.notifyDataSetChanged();
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
           /* AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.AlertDialogCustom));
            builder
                    .setMessage(getString(R.string.quit))
                    .setTitle("")
                    .setCancelable(false)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            finish();
                            Intent intent = new Intent(Intent.ACTION_MAIN);
                            intent.addCategory(Intent.CATEGORY_HOME);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        }
                    })
                    .setNegativeButton("No", null)

                    .show();*/

            Inad.exitalert(this);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);


        final MenuItem item = menu.findItem(R.id.action_search);

        /*MenuItem item1 = menu.findItem(R.id.action_filter);
        item1.setVisible(false);*/

        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setInputType(InputType.TYPE_CLASS_NUMBER);
        searchView.setOnQueryTextListener(this);

        MenuItemCompat.setOnActionExpandListener(item,
                new MenuItemCompat.OnActionExpandListener() {
                    @Override
                    public boolean onMenuItemActionCollapse(MenuItem item) {
// Do something when collapsed
                        mAdapter.setFilter(orderlist);
                        return true; // Return true to collapse action view
                    }

                    @Override
                    public boolean onMenuItemActionExpand(MenuItem item) {
// Do something when expanded
                        return true; // Return true to expand action view
                    }
                });


        return true;
    }

    String msg_alert = "";
    String actionCode_multi = "";

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.


        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        if (id == R.id.action_delete) {


            new CustomDialogClass(CancelTaskActivity.this, new CustomDialogClass.OnDialogClickListener() {
                @Override
                public void onDialogImageRunClick(int positon) {
                    if (positon == 0) {

                    } else if (positon == 1) {
                        //str_actionid = str_actionid.replaceAll(",$", "");
                        Log.e("str_actionid", str_actionid);


                        findViewById(R.id.rl_pb_ctask).setVisibility(View.VISIBLE);
                        assignarchived(sessionString, str_actionid, "", "", new MyCallback() {
                            @Override
                            public void callbackCall(List<Chit> filteredModelList) {

                            }

                            @Override
                            public void callback_remove() {

                                totalcount = 0;
                                str_actionid = "";
                                MenuItem item1 = menu.findItem(R.id.action_delete);
                                item1.setVisible(false);
                                pagecount = 1;
                                isdataavailable = true;
                                usersignin(sessionString, pagecount, true);


                            }
                        });


                    }
                }
            }, "ASSIGN ?").show();
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    private List<Chit> filter(List<Chit> models, String query) {
        query = query.toLowerCase();
        final List<Chit> filteredModelList = new ArrayList<>();
        for (Chit model : models) {
            final String text = model.getSubject().toLowerCase();
            if (text.contains(query)) {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;
    }

    List<Chit> filteredModelList = new ArrayList<>();

    @Override
    public boolean onQueryTextChange(String newText) {
       /* final List<Chit> filteredModelList = filter(orderlist, newText);

        mAdapter.setFilter(filteredModelList);*/


        if (newText.equalsIgnoreCase("")) {
            // final List<Chit> filteredModelList = filter(orderlist, newText);
            // mAdapter.setFilter(filteredModelList);
            // setcte(null,tab,String.valueOf(filteredModelList.size()));
            // return true;
        }
        String filter = "";
        if (tab.equalsIgnoreCase("new_all")) {
            filter = "3,4,8,99,";
        } else if (tab.equalsIgnoreCase("closedtask")) {
            filter = "7,";
        }

        if (newText.length() == 4 || newText.length() == 0) {

        } else {
            return true;
        }

        filteredModelList = searchMytask(sessionString, newText, filter, new MyCallback() {
            @Override
            public void callbackCall(List<Chit> filteredModelList) {
                mAdapter.setFilter(filteredModelList);


            }

            @Override
            public void callback_remove() {

            }
        });
        return true;
    }

    public void assignarchived(final String usersession, String chitIds, String employee_bridge_id, final String msg, final MyCallback listener) {

        ApiInterface apiService =
                ApiClient.getClient(CancelTaskActivity.this).create(ApiInterface.class);
        JsonObject a1 = new JsonObject();
        try {

            a1.addProperty("chitIds", chitIds);
            a1.addProperty("employee_bridge_id", employee_bridge_id);


        } catch (JsonParseException e) {
            e.printStackTrace();
        }
        //   Toast.makeText(MyTaskActivity.this, employee_bridge_id, Toast.LENGTH_LONG).show();

        //Call call = apiService.removechit(usersession,chitIds+",","10");


        Call call = apiService.assigntask(usersession, a1);


        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                int statusCode = response.code();
                Log.e("test", sessionString + "sessionstring");

                Headers headerList = response.headers();
                Log.d("testhistory", statusCode + " ");

                if (response.isSuccessful()) {
                    try {
                        Log.e("response.body()", response.body().toString());
                        String a = new Gson().toJson(response.body());

                        if (!msg.equalsIgnoreCase("")) {
                            Toast.makeText(CancelTaskActivity.this, msg, Toast.LENGTH_LONG).show();
                        }

                        listener.callback_remove();


                    } catch (Exception e) {
                        Log.e("errortest", e.getMessage());

                    }

                } else {
                    Log.e("test", " trdds1");

                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Toast.makeText(CancelTaskActivity.this, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(CancelTaskActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }


            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // Log error here since request failed
                Log.e("error message", t.toString());
                findViewById(R.id.rl_pb_ctask).setVisibility(View.GONE);
            }
        });
    }

    public List<Chit> searchMytask(String usersession, String q, String filter, final MyCallback listener) {

        final List<Chit> filteredModelList = new ArrayList<>();

        ApiInterface apiService =
                ApiClient.getClient(CancelTaskActivity.this).create(ApiInterface.class);
        Call call = null;
        if (tab.equalsIgnoreCase("new_all")) {
            call = apiService.searchAlltask(usersession, q, filter, 1);
        } else if (tab.equalsIgnoreCase("closedtask")) {
            call = apiService.searchMytask(usersession, q, filter);
        }


        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {


                if (response.isSuccessful()) {
                    try {
                        String a = new Gson().toJson(response.body());

                        JSONObject jObjRes = new JSONObject(a);
                        JSONObject jObjdata = jObjRes.getJSONObject("response");
                        JSONObject jObjResponse = jObjdata.getJSONObject("data");
                        String totalRecord = jObjResponse.getString("totalRecord");
                        JSONArray jObjcontacts = jObjResponse.getJSONArray("content");


                        for (int i = 0; i < jObjcontacts.length(); i++) {
                            if (jObjcontacts.get(i) instanceof JSONObject) {
                                JSONObject jsnObj = (JSONObject) jObjcontacts.get(i);
                                Chit obj = new Gson().fromJson(jsnObj.toString(), Chit.class);

                                filteredModelList.add(obj);
                            }
                        }

                        listener.callbackCall(filteredModelList);

                        setcte(filteredModelList, tab, totalRecord);

                    } catch (Exception e) {

                        Toast.makeText(CancelTaskActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                } else {


                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Toast.makeText(CancelTaskActivity.this, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(CancelTaskActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }


            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // Log error here since request failed  mSwipyRefreshLayout.setRefreshing(false);
                Log.e("error message", t.toString());
            }
        });
        return filteredModelList;

    }

    interface MyCallback {
        void callbackCall(List<Chit> filteredModelList);

        void callback_remove();
    }

    private MyCallback listener;

    public void setcte(List<Chit> orderlist, String tab, String tt) {

        int newtask = 0, closetask = 0;


        try {
            /*for (int i = 0; i < orderlist.size(); i++) {
                String t_status = orderlist.get(i).getTransaction_status();


               *//* if (t_status.equalsIgnoreCase("ACTIVE") || t_status.equalsIgnoreCase("ACCEPTED") || t_status.equalsIgnoreCase("HOLD") || t_status.equalsIgnoreCase("INPROGRESS")) {
                    newtask++;
                }*//*

                if (t_status.equalsIgnoreCase("Withdrawn")) {
                    closetask++;
                }
            }*/

            if (tt.equalsIgnoreCase("0")) {
                recyclerView.setVisibility(View.GONE);
                findViewById(R.id.tv_nodata).setVisibility(View.VISIBLE);
            } else {
                recyclerView.setVisibility(View.VISIBLE);
                findViewById(R.id.tv_nodata).setVisibility(View.GONE);
            }


            getSupportActionBar().setTitle("Cancelled Task [ " + tt + " ]");

        /*    if (tab.equalsIgnoreCase("new")) {

                rb_newtask.setText("Open (" + tt + ")");
            } else if (tab.equalsIgnoreCase("inprogress")) {
                rb_inprogtask.setText("Progress (" + tt + ")");
            } else if (tab.equalsIgnoreCase("close")) {
                rb_close.setText("Close (" + tt + ")");
            }*/


        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public void usersignin(String usersession, final int pagecount, final boolean isremoved) {


        ApiInterface apiService =
                ApiClient.getClient(CancelTaskActivity.this).create(ApiInterface.class);

        Call call = null;

        if (tab.equals("new_all")) {

            filter_ = "3,4,8,99,";

            call = apiService.getMyTaskunassign(usersession, pagecount, filter_, "");
        } else {
            filter_ = "7,";
            call = apiService.getMyTask(usersession, pagecount, filter_, "");
        }

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                findViewById(R.id.rl_pb_ctask).setVisibility(View.GONE);
                mSwipyRefreshLayout.setRefreshing(false);
                int statusCode = response.code();

                if (response.isSuccessful()) {
                    try {
                        String a = new Gson().toJson(response.body());

                        JSONObject jObjRes = new JSONObject(a);
                        JSONObject jObjdata = jObjRes.getJSONObject("response");
                        JSONObject jObjResponse = jObjdata.getJSONObject("data");
                        String totalRecord = jObjResponse.getString("totalRecord");
                        JSONArray jObjcontacts = jObjResponse.getJSONArray("content");


                        String displayEndRecord = jObjResponse.getString("displayEndRecord");
                        if (totalRecord.equals(displayEndRecord)) {
                            isdataavailable = false;
                        } else {
                            isdataavailable = true;
                        }

                        List<Chit> temp_orderlist = new ArrayList<>();
                        for (int i = 0; i < jObjcontacts.length(); i++) {
                            if (jObjcontacts.get(i) instanceof JSONObject) {
                                JSONObject jsnObj = (JSONObject) jObjcontacts.get(i);
                                Chit obj = new Gson().fromJson(jsnObj.toString(), Chit.class);
                                temp_orderlist.add(obj);
                            }
                        }

                        if (pagecount == 1) {
                            orderlist.clear();
                        }
                        orderlist.addAll(temp_orderlist);
                        mAdapter.notifyDataSetChanged();

                        if (orderlist.size() == 0) {
                            recyclerView.setVisibility(View.GONE);
                            findViewById(R.id.tv_nodata).setVisibility(View.VISIBLE);
                        } else {
                            recyclerView.setVisibility(View.VISIBLE);
                            findViewById(R.id.tv_nodata).setVisibility(View.GONE);
                        }

                        setcte(orderlist, tab, totalRecord);


                    } catch (Exception e) {

                        Toast.makeText(CancelTaskActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                } else {

                    switch (response.code()) {
                        case 401:
                            new RefreshSession(CancelTaskActivity.this, sessionManager);  // session expired
                            break;
                        case 404:
                            Toast.makeText(CancelTaskActivity.this, "not found", Toast.LENGTH_SHORT).show();
                            break;
                        case 500:
                            Toast.makeText(CancelTaskActivity.this, "server broken", Toast.LENGTH_SHORT).show();
                            break;
                        default:
                            Toast.makeText(CancelTaskActivity.this, "unknown error", Toast.LENGTH_SHORT).show();
                            break;
                    }


                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Toast.makeText(CancelTaskActivity.this, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(CancelTaskActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // Log error here since request failed
                findViewById(R.id.rl_pb_ctask).setVisibility(View.GONE);
                mSwipyRefreshLayout.setRefreshing(false);
            }
        });
    }

    public void Mytaskctr(String usersession, final int pagecount, final boolean isremoved) {


        ApiInterface apiService =
                ApiClient.getClient(CancelTaskActivity.this).create(ApiInterface.class);

        Call call = null;

        filter_ = "5,6,7,9,";
        call = apiService.getMyTask(usersession, pagecount, filter_, "");


        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {

                if (response.isSuccessful()) {
                    try {
                        String a = new Gson().toJson(response.body());

                        JSONObject jObjRes = new JSONObject(a);
                        JSONObject jObjdata = jObjRes.getJSONObject("response");
                        JSONObject jObjResponse = jObjdata.getJSONObject("data");

                        JSONArray jObjcontacts = jObjResponse.getJSONArray("content");

                        List<Chit> temp_orderlist = new ArrayList<>();
                        for (int i = 0; i < jObjcontacts.length(); i++) {
                            if (jObjcontacts.get(i) instanceof JSONObject) {
                                JSONObject jsnObj = (JSONObject) jObjcontacts.get(i);
                                Chit obj = new Gson().fromJson(jsnObj.toString(), Chit.class);
                                temp_orderlist.add(obj);
                            }
                        }

                        //setcte(temp_orderlist, "");
                    } catch (Exception e) {

                        Toast.makeText(CancelTaskActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                } else {

                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Toast.makeText(CancelTaskActivity.this, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(CancelTaskActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // Log error here since request failed

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        timer = new CountDownTimer(15 * 60 * 1000, 1000) {

            public void onTick(long millisUntilFinished) {
                //Some code

                long sec = millisUntilFinished / 1000;
                Log.e("sec", String.valueOf(sec));
            }

            public void onFinish() {

                isdataavailable = true;
                pagecount = 1;
                usersignin(sessionString, 1, false);


            }
        };

        timer.start();


        findViewById(R.id.rl_pb_ctask).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });


    }

    @Override
    protected void onPause() {
        super.onPause();


        timer.cancel();

    }

    CountDownTimer timer;
    String filter_ = "";
}
