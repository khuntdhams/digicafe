package com.cab.digicafe.Activities.mrp;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cab.digicafe.ChitlogActivity;
import com.cab.digicafe.R;
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MrpFragmnet extends Fragment {


    public MrpFragmnet() {

    }

    @BindView(R.id.tlMrp)
    TabLayout tlMrp;

    @BindView(R.id.vpMrp)
    ViewPager vpMrp;

    public static int vPos = 0;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_mrp, container, false);
        //currency = SharedPrefUserDetail.getString(getActivity(), SharedPrefUserDetail.symbol_native,"") + " ";

        ButterKnife.bind(this, v);
        context = getActivity();

        tlMrp.setupWithViewPager(vpMrp);//setting tab over viewpager

        setupViewPager(vpMrp);

        tlMrp.setSelectedTabIndicatorColor(Color.parseColor("#FF0000"));
        //tlMrp.setSelectedTabIndicatorHeight((int) (5 * getResources().getDisplayMetrics().density));
        tlMrp.setTabTextColors(ContextCompat.getColor(context, R.color.gray), ContextCompat.getColor(context, R.color.colorPrimary));

        return v;
    }


    Context context;


    public void onRefresh(SwipyRefreshLayoutDirection direction) {
        if (vpMrp != null && materialFragmnet != null) {
            vpMrp.setCurrentItem(1);
            materialFragmnet.onRefresh(direction);
        }

    }
    public void onRefreshDelivery(SwipyRefreshLayoutDirection direction) {
        if (vpMrp != null && deliveryFragmnet != null) {
            vpMrp.setCurrentItem(4);
            deliveryFragmnet.onRefresh(direction);
        }

    }

    public void onRefreshResource(SwipyRefreshLayoutDirection direction) {
        if (vpMrp != null && materialFragmnet != null) {
            vpMrp.setCurrentItem(2);
            resourceFragmnet.onRefresh(direction);
        }

    }

    public void onRefreshAll(SwipyRefreshLayoutDirection direction, int sortBy, int orderBy) {
        if (vpMrp != null && allFragmnet != null) {
            vpMrp.setCurrentItem(0);
            allFragmnet.onRefreshBySort(direction, sortBy, orderBy);
        }

    }

    public void onRefreshPlann(SwipyRefreshLayoutDirection direction) {
        if (vpMrp != null && materialFragmnet != null) {
            vpMrp.setCurrentItem(3);
            planningFragmnet.onRefresh(direction);
        }

    }


    MrpViewPagerAdapter adapter;

    MaterialFragmnet materialFragmnet = new MaterialFragmnet();
    DeliveryFragmnet deliveryFragmnet = new DeliveryFragmnet();
    ResourceFragmnet resourceFragmnet = new ResourceFragmnet();
    PlanningFragmnet planningFragmnet = new PlanningFragmnet();
    AllFragmnet allFragmnet = new AllFragmnet();


    private void setupViewPager(ViewPager viewPager) {

        adapter = new MrpViewPagerAdapter(getChildFragmentManager());

        adapter.addFrag(allFragmnet, "All");
        adapter.addFrag(materialFragmnet, "Material");
        adapter.addFrag(resourceFragmnet, "Resource");
        adapter.addFrag(planningFragmnet, "Plan");
        adapter.addFrag(deliveryFragmnet, "Delivery");

        viewPager.setAdapter(adapter);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                ((ChitlogActivity) getActivity()).setLinkIcons(i);
                vPos = i;
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
        ((ChitlogActivity) getActivity()).setLinkIcons(0);

    }

    class MrpViewPagerAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();//fragment arraylist
        private final List<String> mFragmentTitleList = new ArrayList<>();//title arraylist

        public MrpViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

        //adding fragments and title method
        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }



  /*  @BindView(R.id.tvMrpJson)
    TextView tvMrpJson;*/


    // Tab


}
