package com.cab.digicafe.Activities;

import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.andremion.counterfab.CounterFab;
import com.cab.digicafe.Adapter.CircleListAdapter;
import com.cab.digicafe.BaseActivity;
import com.cab.digicafe.Helper.Inad;
import com.cab.digicafe.Helper.SessionManager;
import com.cab.digicafe.Helper.SharedPrefUserDetail;
import com.cab.digicafe.Model.ServiceProvider;
import com.cab.digicafe.MyCustomClass.ModelDevice;
import com.cab.digicafe.MyCustomClass.SearchableSpinner;
import com.cab.digicafe.R;
import com.cab.digicafe.Rest.ApiClient;
import com.cab.digicafe.Rest.ApiInterface;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChageCircle extends BaseActivity {


    @BindView(R.id.done)
    Button done;

    @BindView(R.id.rl_pb)
    RelativeLayout rl_pb;


    SessionManager sessionManager;

    @BindView(R.id.rv_circle)
    RecyclerView rv_circle;

    @BindView(R.id.counter_fab)
    CounterFab counterFab;
    CircleListAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.changecircle, frameLayout);


        ButterKnife.bind(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Select Circle");
        counterFab.setVisibility(View.GONE);


        sessionManager = new SessionManager();

        spinner_building = (SearchableSpinner) findViewById(R.id.spinner_building);
        rl_pb.setVisibility(View.VISIBLE);


        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (building_id.isEmpty()) {
                    Toast.makeText(ChageCircle.this, "Select Circle !", Toast.LENGTH_LONG).show();
                } else {
                    setuserbuildid_and_indid(true);

                }
            }
        });

        //  setuserbuildid_and_indid(true);

        mAdapter = new CircleListAdapter(al_building, ChageCircle.this, new CircleListAdapter.OnItemClickListener() {


            @Override
            public void onItemClick(industrySuppliermodel item) {

                building_id = item.getId();
            }


        });

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        rv_circle.setLayoutManager(mLayoutManager);
        rv_circle.setItemAnimator(new DefaultItemAnimator());
        rv_circle.setAdapter(mAdapter);

        GetAddress("", false);


    }


    public void GetAddress(String search, final boolean isfilter) {


        ApiInterface apiService =
                ApiClient.getClient(this).create(ApiInterface.class);


        Call call;
        call = apiService.getaddress(sessionManager.getsession(), search);

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                rl_pb.setVisibility(View.GONE);

                int statusCode = response.code();
                //  String cookie = response.;
                //  Log.d("test", cookie+ " ");

                if (response.isSuccessful()) {


                    try {
                        String a = new Gson().toJson(response.body());
                        JSONObject Jsonresponse = new JSONObject(a);
                        JSONObject jo_response = Jsonresponse.getJSONObject("response");

                        String data = jo_response.getString("data");
                        sessionManager.setIndustrySupplier(data);

                        JSONObject jo_data = new JSONObject(data);
                        JSONArray ja_buildingArr = jo_data.getJSONArray("buildingArr");


                        al_building = new ArrayList();
                        //al_building.add(new industrySuppliermodel("Select Circle", ""));
                        for (int i = 0; i < ja_buildingArr.length(); i++) {
                            JSONObject jo_res = ja_buildingArr.getJSONObject(i);
                            String building_name = jo_res.getString("building_name");
                            String building_id = jo_res.getString("building_id");
                            al_building.add(new industrySuppliermodel(building_name, building_id));
                        }


                        setbuild();

                        if (isfilter) {
                            for (int i = 0; i < al_building.size(); i++) {
                                String t_id = al_building.get(i).getId();
                                if (t_id.trim().equals(building_id.trim())) {
                                    //spinner_building.setSelection(i);
                                    al_building.get(i).setIschecked(true);
                                    break;
                                }
                            }
                        } else {
                            setuserbuildid_and_indid(false);
                        }


                        mAdapter.setitem(al_building);


                    } catch (Exception e) {
                        Log.e("test", " trdds" + e.getLocalizedMessage());

                    }
                } else {

                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Toast.makeText(ChageCircle.this, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        // Toast.makeText(CatelogActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }

                //  List<Movie> movies = response.body().getResults();
                //  recyclerView.setAdapter(new MoviesAdapter(movies, R.layout.list_item_movie, getApplicationContext()));
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // Log error here since request failed
                rl_pb.setVisibility(View.GONE);

            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();

    }

    SearchableSpinner spinner_building;
    ArrayList<industrySuppliermodel> al_building = new ArrayList<>();


    public static class industrySuppliermodel {
        private String name;
        private String id;

        boolean ischecked = false;

        public boolean isIschecked() {
            return ischecked;
        }

        public void setIschecked(boolean ischecked) {
            this.ischecked = ischecked;
        }

        public industrySuppliermodel() {
        }

        public industrySuppliermodel(String name, String id) {
            this.name = name;
            this.id = id;
        }


        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        /**
         * Pay attention here, you have to override the toString method as the
         * ArrayAdapter will reads the toString of the given object for the name
         *
         * @return contact_name
         */
        @Override
        public String toString() {
            return name;
        }
    }

    ArrayAdapter<industrySuppliermodel> build_dataAdapter;

    public void setbuild() {
        /*if(al_building==null)al_building = new ArrayList<>();

        build_dataAdapter = new ArrayAdapter<industrySuppliermodel>(this, R.layout.spinner_item, al_building);

        // ArrayAdapter adapter = ArrayAdapter.createFromResource(this, R.array.planets, R.layout.spinner_item);
        build_dataAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        spinner_building.setAdapter(build_dataAdapter);
        spinner_building.setTitle("Select Circle");

        spinner_building.setOnSearchTextChangedListener(new SearchableListDialog.OnSearchTextChanged() {
            @Override
            public void onSearchTextChanged(String strText) {


                //GetAddress(strText,true);

            }
        });*/


    }

    ArrayAdapter<industrySuppliermodel> ind_dataAdapter;


    String building_id = "";


    ArrayList<ModelDevice> al_user = new ArrayList<>();
    boolean isuseravailable = false;
    int userpos = 0;
    String user = "";

    public void setuserbuildid_and_indid(boolean isset) {

        user = sessionManager.getcurrentu_nm();
        final Gson gson = new Gson();

        String json = SharedPrefUserDetail.getString(this, SharedPrefUserDetail.loginuser, "");


        Type type = new TypeToken<ArrayList<ModelDevice>>() {
        }.getType();

        al_user = gson.fromJson(json, type);

        if (al_user == null) {
            al_user = new ArrayList<>();
        }

        if (isset)  // fst time
        {
            isuseravailable = false;
            for (int i = 0; i < al_user.size(); i++) {
                String alluser = al_user.get(i).getUsernm();
                if (user.equals(alluser)) {
                    isuseravailable = true;
                    userpos = i;
                    al_user.get(i).setBuild_id(building_id);

                    break;
                }
            }
            String str_array = gson.toJson(al_user);
            SharedPrefUserDetail.setString(this, SharedPrefUserDetail.loginuser, str_array);
            sessionManager.setTLoginuser(str_array);


            sessionManager.setBuilding_id(building_id);

            Inad.alerter("", "Circle Updated!", this);

            onBackPressed();


        } else {  // isget  // next time
            isuseravailable = false;
            for (int i = 0; i < al_user.size(); i++) {
                String alluser = al_user.get(i).getUsernm();
                if (user.equals(alluser)) {
                    isuseravailable = true;
                    userpos = i;

                    break;
                }
            }
            building_id = al_user.get(userpos).getBuild_id();
            //industry_id = al_user.get(userpos).getInd_id();


            for (int i = 0; i < al_building.size(); i++) {
                String t_id = al_building.get(i).getId();
                if (t_id.trim().equals(building_id.trim())) {
                    //spinner_building.setSelection(i);
                    al_building.get(i).setIschecked(true);
                    break;
                }
            }


        }


     /*   sessionManager.setBuilding_id(building_id);
        sessionManager.setIndustry_id(industry_id);*/

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);


        final MenuItem item = menu.findItem(R.id.action_search);

        MenuItem item1 = menu.findItem(R.id.action_filter);
        item1.setVisible(false);

        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        //searchView.setInputType(InputType.TYPE_CLASS_NUMBER);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                GetAddress(newText, true);
                return false;
            }
        });

        MenuItemCompat.setOnActionExpandListener(item,
                new MenuItemCompat.OnActionExpandListener() {
                    @Override
                    public boolean onMenuItemActionCollapse(MenuItem item) {
// Do something when collapsed
                        // mAdapter.setFilter(al_aggre);
                        return true; // Return true to collapse action view
                    }

                    @Override
                    public boolean onMenuItemActionExpand(MenuItem item) {
// Do something when expanded
                        return true; // Return true to expand action view
                    }
                });


        return true;
    }

    private List<ServiceProvider> filter(List<ServiceProvider> models, String query) {
        query = query.toLowerCase();
        final List<ServiceProvider> filteredModelList = new ArrayList<>();
        for (ServiceProvider model : models) {
            final String text = model.getCompany_name().toLowerCase();
            if (text.contains(query)) {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;
    }


}
