package com.cab.digicafe.Activities.mrp;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.CircularProgressDrawable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.cab.digicafe.Activities.RequestApis.MyRequestCall;
import com.cab.digicafe.Activities.RequestApis.MyRequst;
import com.cab.digicafe.Adapter.mrp.TaskPrefViewModel;
import com.cab.digicafe.Fragments.SummaryDetailFragmnet;
import com.cab.digicafe.Helper.Constants;
import com.cab.digicafe.Helper.SessionManager;
import com.cab.digicafe.Model.Chit;
import com.cab.digicafe.Model.Employee;
import com.cab.digicafe.Model.ShareModel;
import com.cab.digicafe.MyCustomClass.AutoCompleteTv;
import com.cab.digicafe.MyCustomClass.DatePickerNew;
import com.cab.digicafe.MyCustomClass.JsonObjParse;
import com.cab.digicafe.MyCustomClass.PlayAnim;
import com.cab.digicafe.R;
import com.cab.digicafe.Rest.ApiClient;
import com.cab.digicafe.Rest.ApiInterface;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.cab.digicafe.Activities.mrp.MrpFragmnet.vPos;


public class AnimDialogueWithFieldForExternalLink extends AlertDialog implements
        View.OnClickListener, TextWatcher {
    private final OnDialogClickListener listener;
    public AppCompatActivity c;
    public Dialog d;


    @BindView(R.id.llAnim)
    LinearLayout llAnim;

    @BindView(R.id.tvAdd)
    TextView tvAdd;

    @BindView(R.id.tvCancel)
    TextView tvCancel;

    @BindView(R.id.tvStatus)
    TextView tvStatus;

    @BindView(R.id.tvModifyDelDt)
    TextView tvModifyDelDt;

    @BindView(R.id.rlDelDt)
    RelativeLayout rlDelDt;

    @BindView(R.id.tvModifyDelTime)
    TextView tvModifyDelTime;

    @BindView(R.id.rlDelTime)
    RelativeLayout rlDelTime;

    @BindView(R.id.llResources)
    LinearLayout llResources;

    @BindView(R.id.rlHide)
    RelativeLayout rlHide;

    @BindView(R.id.etShopNm)
    EditText etShopNm;

    @BindView(R.id.etLocation)
    EditText etLocation;

    @BindView(R.id.etReceiptNo)
    EditText etReceiptNo;

    @BindView(R.id.etAmount)
    EditText etAmount;

    @BindView(R.id.llExternalLinkField)
    LinearLayout llExternalLinkField;


    String title = "", status = "";
    TaskPrefViewModel taskPrefViewModel;

    public AnimDialogueWithFieldForExternalLink(Context a, OnDialogClickListener listener, String title, String view) {
        super(a);
        // TODO Auto-generated constructor stub
        this.c = (AppCompatActivity) a;
        this.listener = listener;
        this.title = title;
        this.status = view;
    }

    String symbol_native = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_anim_dialogue_field_external_link);
        ButterKnife.bind(this);

        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        getWindow().setAttributes(lp);

        tvAdd.setOnClickListener(this);
        tvCancel.setOnClickListener(this);
        rlHide.setOnClickListener(this);
        rlDelTime.setOnClickListener(this);
        setCancelable(true);
        tvStatus.setText(title);
        /*setOnKeyListener(new OnKeyListener() {

            @Override
            public boolean onKey(DialogInterface arg0, int keyCode,
                                 KeyEvent event) {
                // TODO Auto-generated method stub
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    dismissDialogue(0);
                }
                return true;
            }
        });*/
        rlHide.setOnClickListener(this);
        rlDelDt.setOnClickListener(this);

        playAnim.slideDownRl(llAnim);

        taskPrefViewModel = ShareModel.getI().taskPrefViewModel;

        if (status.equalsIgnoreCase(c.getString(R.string.link_other))) {
            llResources.setVisibility(View.GONE);
            llExternalLinkField.setVisibility(View.VISIBLE);
        } else if (status.equalsIgnoreCase(c.getString(R.string.resources))) {
            showLoader(tvAdd);
            getEmployee();
            llResources.setVisibility(View.VISIBLE);
            llExternalLinkField.setVisibility(View.GONE);
        }

        etResAmount.addTextChangedListener(this);
        etResUnitOrQuantity.addTextChangedListener(this);
    }

    public void showHideLl(LinearLayout linearLayout, boolean isShow) {
        if (isShow) {
            linearLayout.setVisibility(View.VISIBLE);
        } else {
            linearLayout.setVisibility(View.GONE);
        }
    }


    public void dismissDialogue(final int call) {
        playAnim.slideUpRl(llAnim);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                listener.onDialogImageRunClick(call);
                dismiss();

            }
        }, 700);

    }

    PlayAnim playAnim = new PlayAnim();
    SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy hh:mm aaa");

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvCancel:

                dismissDialogue(0);

                break;
            case R.id.rlDelDt:
                openDateView(tvModifyDelDt);

                break;

            case R.id.rlDelTime:
                TimePicker mTimePicker = new TimePicker();
                mTimePicker.show(c.getFragmentManager(), "Select delivery time");
                break;

            case R.id.tvAdd:
                //dismissDialogue(1);

                if (status.equalsIgnoreCase(c.getString(R.string.link_other))) {
                    String shopName = etShopNm.getText().toString().trim();
                    String recNo = etReceiptNo.getText().toString().trim();
                    String delDate = tvModifyDelDt.getText().toString().trim();

                    if (shopName.isEmpty()) {
                        Toast.makeText(c, "Shop name field is required.", Toast.LENGTH_SHORT).show();
                    } else if (recNo.isEmpty()) {
                        Toast.makeText(c, "Receipt number field is required.", Toast.LENGTH_SHORT).show();
                    } else if (delDate.isEmpty()) {
                        Toast.makeText(c, "Date field is required.", Toast.LENGTH_SHORT).show();
                    } else {

                        addTaskPreferenceForMrpFromAllTask();

                    }
                } else if (status.equalsIgnoreCase(c.getString(R.string.resources))) {

                    String amount = etResAmount.getText().toString().trim();
                    String unit = etResUnitOrQuantity.getText().toString().trim();
                    String delDate = tvModifyDelDt.getText().toString().trim();

                    int empolyeePos = spn_employee.getSelectedItemPosition();
                    if (empolyeePos == 0) {
                        Toast.makeText(c, "Select an employee.", Toast.LENGTH_SHORT).show();
                    } else if (unit.isEmpty()) {
                        Toast.makeText(c, "Rate field is required.", Toast.LENGTH_SHORT).show();
                    } else if (amount.isEmpty()) {
                        Toast.makeText(c, "Quantity field is required.", Toast.LENGTH_SHORT).show();
                    } else if (delDate.isEmpty()) {
                        Toast.makeText(c, "Date field is required.", Toast.LENGTH_SHORT).show();
                    } else {
                        addTaskPreferenceForMrpFromAllTask();
                    }
                }

                break;
            case R.id.rlHide:
                dismissDialogue(1);
                break;
            default:
                break;
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

        String unit = etResUnitOrQuantity.getText().toString().trim();
        String amount = etResAmount.getText().toString().trim();

        if (amount.isEmpty() || unit.isEmpty()) {
            etTotalResAmount.setText("");
        } else {
            float amt_val = Float.parseFloat(amount);
            float unit_val = Float.parseFloat(unit);
            float total = amt_val * unit_val;
            etTotalResAmount.setText(total + "");
        }

    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    public interface OnDialogClickListener {
        void onDialogImageRunClick(int jsonObject);
    }

    Calendar cal_order = Calendar.getInstance();


    public void openDateView(final TextView textView) {

        DatePickerNew mDatePicker = new DatePickerNew(c, new DatePickerNew.PickDob() {
            @Override
            public void onSelect(Date date) {

                cal_order.setTime(date);

                SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy");
                String date_ = format.format(date);

                textView.setText(date_);

            }
        }, textView.getText().toString().trim());
        mDatePicker.setStyle(DialogFragment.STYLE_NORMAL, R.style.CustomDialog);
        mDatePicker.show(c.getFragmentManager(), "Select delivery date");
    }


    public void settime() {
        SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy hh:mm aaa");
        String time = df.format(cal_order.getTime());

        SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy hh:mm aaa");
        try {
            if (!time.equals("")) {
                Date date_ = format.parse(time);


                SimpleDateFormat timeformat = new SimpleDateFormat("hh:mm aaa");
                String str_time = timeformat.format(date_);

                SimpleDateFormat dtformat = new SimpleDateFormat("dd MMM yyyy");

                String str_date = dtformat.format(date_);
                tvModifyDelDt.setText(str_date);
                tvModifyDelTime.setText(str_time);


            }
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("ValidFragment")
    public class TimePicker extends DialogFragment implements TimePickerDialog.OnTimeSetListener {
        @Override
        public void onCancel(DialogInterface dialog) {
            super.onCancel(dialog);

        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            int hour = cal_order.get(Calendar.HOUR_OF_DAY);
            int minute = cal_order.get(Calendar.MINUTE);
            return new TimePickerDialog(getActivity(), this, hour, minute, DateFormat.is24HourFormat(getActivity()));
        }

        @Override
        public void onTimeSet(android.widget.TimePicker view, int hourOfDay, int minute) {

            cal_order.set(cal_order.get(Calendar.YEAR), cal_order.get(Calendar.MONTH), cal_order.get(Calendar.DAY_OF_MONTH), hourOfDay, minute);


            settime();


        }
    }


    @BindView(R.id.sv)
    ScrollView sv;


    MyRequestCall myRequestCall = new MyRequestCall();
    MyRequst myRequst = new MyRequst();
    JsonObject joAddEdit = new JsonObject();
    JSONObject joFrom = new JSONObject(); // From should be his customer info
    //JSONObject joTo = new JSONObject(); // supplier ; In To, supplier , name address etc

    public void addTaskPreferenceForMrpFromAllTask() {

        showLoader(tvAdd);


        // from all task only pass flage

        Chit chit = SummaryDetailFragmnet.item;


        try {

            String DATE = df.format(cal_order.getTime());
            joFrom.put("DATE", DATE);
            joFrom.put("EDIT_DATE", DATE);
            joFrom.put("symbol_native", chit.getSymbol_native());

            if (status.equalsIgnoreCase(c.getString(R.string.link_other))) {
                joFrom.put("total_chit_item_value", etAmount.getText().toString().trim());
                joFrom.put("edit_total_chit_item_value", etAmount.getText().toString().trim());
                joFrom.put("location", etLocation.getText().toString().trim());
                joFrom.put("subject", etShopNm.getText().toString().trim());
                joFrom.put("receipt_number", etReceiptNo.getText().toString().trim()); // only for in task list so in To only
                joFrom.put("sort", Constants.SORT_MAT); // Material
            } else if (status.equalsIgnoreCase(c.getString(R.string.resources))) {


                int empolyeePos = spn_employee.getSelectedItemPosition();
                if (empolyeePos < alTractions.size()) {
                    String empNm = alTractions.get(empolyeePos).name;
                    joFrom.put("subject", empNm);
                }

                float amt = Float.parseFloat(etResAmount.getText().toString().trim());
                float unit = Float.parseFloat(etResUnitOrQuantity.getText().toString().trim());
                float total = amt * unit;
                joFrom.put("total_chit_item_value", total);
                joFrom.put("edit_total_chit_item_value", total);
                joFrom.put("sort", Constants.SORT_RES); // Res
                joFrom.put("unit_value", etResUnitOrQuantity.getText().toString());
                joFrom.put("amount_value", etResAmount.getText().toString());
                joFrom.put("unit_type", type);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        joAddEdit.addProperty("flag", "add");

        joAddEdit.addProperty("from_chit_hash_id", chit.getChit_hash_id());
        joAddEdit.addProperty("from_chit_id", chit.getChit_id());
        joAddEdit.addProperty("from_ref_id", chit.getRef_id());
        joAddEdit.addProperty("from_case_id", chit.getCase_id());
        joAddEdit.addProperty("from_purpose", chit.getPurpose());
        joAddEdit.addProperty("from_field_json_data", "{}");

        joAddEdit.addProperty("to_bridge_id", "0");
        joAddEdit.addProperty("to_chit_hash_id", "0");
        joAddEdit.addProperty("to_chit_id", "0");
        joAddEdit.addProperty("to_ref_id", "0");
        joAddEdit.addProperty("to_case_id", "0");

        joAddEdit.addProperty("to_field_json_data", joFrom.toString());

        if (status.equalsIgnoreCase(c.getString(R.string.link_other))) {
            if (vPos == 1) {
                joAddEdit.addProperty("task_purpose", "Material");
            } else if (vPos == 4) {
                joAddEdit.addProperty("task_purpose", "Delivery");
            }
            joAddEdit.addProperty("link_flag", "External");
            joAddEdit.addProperty("to_purpose", "external");
        } else if (status.equalsIgnoreCase(c.getString(R.string.resources))) {
            joAddEdit.addProperty("task_purpose", "Resource");
            joAddEdit.addProperty("link_flag", "Internal");
            joAddEdit.addProperty("to_purpose", "Resource");
        }

        JsonObject joMrpObj = new JsonObject();
        JsonArray jsonElements = new JsonArray();
        jsonElements.add(joAddEdit);

        joMrpObj.add("newdata", jsonElements);

        myRequst.cookie = new SessionManager().getsession();

        myRequestCall.addTaskPreferencr(c, myRequst, joMrpObj, new MyRequestCall.CallRequest() {
            @Override
            public void onGetResponse(String response) {

                if (!response.isEmpty()) {

                } else {

                }

                tvAdd.setBackground(null);
                tvAdd.setBackgroundColor(ContextCompat.getColor(c, R.color.colorPrimary));
                tvAdd.setBackground(ContextCompat.getDrawable(c, R.drawable.roundcorner_color));
                tvAdd.setText("Add");

                dismissDialogue(2);

            }
        });


    }

    public void showLoader(TextView textView) {
        textView.setText("");
        CircularProgressDrawable circularProgressDrawable = new CircularProgressDrawable(c);
        circularProgressDrawable.setStrokeWidth(5f);
        circularProgressDrawable.setCenterRadius(30f);
        circularProgressDrawable.start();
        textView.setBackground(circularProgressDrawable);
    }

    @BindView(R.id.etResUnitOrQuantity)
    EditText etResUnitOrQuantity;

    @BindView(R.id.etResAmount)
    EditText etResAmount;

    @BindView(R.id.etTotalResAmount)
    EditText etTotalResAmount;

    @BindView(R.id.tvUnitDetails)
    TextView tvUnitDetails;

    //

    public void getEmployee() {

        ApiInterface apiService = ApiClient.getClient(c).create(ApiInterface.class);

        myRequst.cookie = new SessionManager().getsession();

        Call call;
        call = apiService.getEmployee(myRequst.cookie);

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {

                tvAdd.setBackground(null);
                tvAdd.setBackgroundColor(ContextCompat.getColor(c, R.color.colorPrimary));
                tvAdd.setBackground(ContextCompat.getDrawable(c, R.drawable.roundcorner_color));
                tvAdd.setText("Add");

                try {
                    if (response.isSuccessful()) {

                        String a = new Gson().toJson(response.body());
                        JSONObject jObjRes = new JSONObject(a);
                        JSONObject jObjdata = jObjRes.getJSONObject("response");
                        JSONObject jObjResponse = jObjdata.getJSONObject("data");
                        String totalRecord = jObjResponse.getString("totalRecord");
                        JSONArray jObjcontacts = jObjResponse.getJSONArray("content");

                        String displayEndRecord = jObjResponse.getString("displayEndRecord");
                        if (totalRecord.equals(displayEndRecord)) {
                            // isdataavailable = false;
                        } else {
                            // isdataavailable = true;
                        }

                        alTractions = new ArrayList<>();
                        alTractions.add(new AutoCompleteTv("Select an employee", 0, "", ""));

                        for (int i = 0; i < jObjcontacts.length(); i++) {
                            if (jObjcontacts.get(i) instanceof JSONObject) {
                                JSONObject jsnObj = (JSONObject) jObjcontacts.get(i);
                                Employee obj = new Gson().fromJson(jsnObj.toString(), Employee.class);
                                int p = i + 1;
                                alTractions.add(new AutoCompleteTv(obj.getEmployee_name(), p, obj.getField_json_data(), ""));
                            }
                        }
                        setEmployeeSpinner();
                    } else {
                        try {
                            JSONObject jObjError = new JSONObject(response.errorBody().string());
                            JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                            Toast.makeText(c, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call call, Throwable t) {

            }
        });


    }

    int serviceId = 0;

    @BindView(R.id.spn_employee)
    Spinner spn_employee;
    AutoCompleteTv autoCompleteTv = new AutoCompleteTv();
    ArrayList<AutoCompleteTv> alTractions = new ArrayList<>();
    int tractionId = 0;
    String type = "";

    public void setEmployeeSpinner() {

        autoCompleteTv.setautofill(c, spn_employee, alTractions);
        spn_employee.setSelection(tractionId);

        spn_employee.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                tvUnitDetails.setText("");
                etResUnitOrQuantity.setText("");
                etResAmount.setText("");
                type = "";
                if (position > 0) {
                    String unit = JsonObjParse.getValueEmpty(alTractions.get(position).getField_json(), "unit");
                    //String amount = JsonObjParse.getValueEmpty(alTractions.get(position).getField_json(), "amount");
                    type = JsonObjParse.getValueEmpty(alTractions.get(position).getField_json(), "type");

                    tvUnitDetails.setText("Rate/Unit : " + unit + ",   Unit type : " + type);

                    etResUnitOrQuantity.setText(unit);
                    //etResAmount.setText(amount);

                } else {
                    // spin_area = "";

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


}


