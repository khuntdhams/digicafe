package com.cab.digicafe.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.cab.digicafe.Adapter.AggreListAdapter;
import com.cab.digicafe.Helper.SessionManager;
import com.cab.digicafe.Helper.SharedPrefUserDetail;
import com.cab.digicafe.MainActivity;
import com.cab.digicafe.Model.ModelCustom;
import com.cab.digicafe.Model.ServiceProvider;
import com.cab.digicafe.ProfileActivity;
import com.cab.digicafe.R;
import com.cab.digicafe.Rest.ApiClient;
import com.cab.digicafe.Rest.ApiInterface;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SPActivity extends MyAppBaseActivity implements SearchView.OnQueryTextListener {

    @BindView(R.id.rv_aggre)
    RecyclerView rv_aggre;

    @BindView(R.id.next)
    Button next;


    SessionManager sessionManager;
    AggreListAdapter mAdapter;
    String aggre = "";
    String fname, email, phone;
    boolean isfrombase = false;
    boolean isback = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sp);

        ButterKnife.bind(this);

        sessionManager = new SessionManager(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("Service Provider");

       /* if (getIntent().getExtras().containsKey("isfrombase")) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            isfrombase = true;
            next.setText("DONE");

            if (getIntent().getExtras().containsKey("isback")){
                isback = true;
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            }
        } else {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        }*/


        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {

            } else {

                isback = extras.getBoolean("isback", true);

                if (isback) {
                    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                } else {
                    getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                }

            }
        } else {

        }


        //getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        mAdapter = new AggreListAdapter(al_aggre, SPActivity.this, new AggreListAdapter.OnItemClickListener() {


            @Override
            public void onItemClick(ServiceProvider item) {
                aggre = item.getUsername();

                SharedPrefUserDetail.setString(SPActivity.this, SharedPrefUserDetail.isfrommyaggregator, item.getTo_bridge_id());

                Intent mainIntent = new Intent(SPActivity.this, MainActivity.class);
                mainIntent.putExtra("session", sessionManager.getsession());
               /* mainIntent.putExtra("isfrommyagree",true);
                mainIntent.putExtra("to_bridge_id",item.getTo_bridge_id());
                mainIntent.putExtra("currency_code",item.getCurrency_code());*/
                startActivity(mainIntent);


            }
        });


        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        rv_aggre.setLayoutManager(mLayoutManager);
        rv_aggre.setItemAnimator(new DefaultItemAnimator());
        rv_aggre.setAdapter(mAdapter);


        findViewById(R.id.rl_pb).setVisibility(View.VISIBLE);
        next.setEnabled(false);
        usersignin(false);

        if (isback) {
            next.setText("Done");
        } else {
            next.setText("Next");
        }


        next.setVisibility(View.GONE);

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (!aggre.equalsIgnoreCase("")) {
                      /*  Intent i = new Intent(SPActivity.this, ProfileAddressActivity.class);
                        i.putExtra("f_name", fname);
                        i.putExtra("email", email);
                        i.putExtra("phone", phone);
                        i.putExtra("aggre", aggre);
                        startActivity(i);*/

                    findViewById(R.id.rl_pb).setVisibility(View.VISIBLE);
                    usersignin(true);


                } else {
                    Toast.makeText(SPActivity.this, "Select Service Provider!", Toast.LENGTH_LONG).show();
                }

            }
        });


    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    public void chooseaggregator() {


        ApiInterface apiService =
                ApiClient.getClient(this).create(ApiInterface.class);


        Call call;


        call = apiService.GetAggreId(sessionManager.getsession(), digicafebridgeid);


        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                findViewById(R.id.rl_pb).setVisibility(View.GONE);
                if (response.isSuccessful()) {

                    try {
                        String a = new Gson().toJson(response.body());

                        JSONObject Jsonresponse = new JSONObject(a);
                        JSONObject Jsonresponseinfo = Jsonresponse.getJSONObject("response");

                        String data = Jsonresponseinfo.getString("data");
                        JSONObject jo_data = new JSONObject(data);

                        JSONArray ja_content = jo_data.getJSONArray("content");

                        for (int i = 0; i < ja_content.length(); i++) {

                            JSONObject jsnObj = (JSONObject) ja_content.get(i);
                            //String unm = jsnObj.getString("username");
                            ServiceProvider sp = new Gson().fromJson(jsnObj.toString(), ServiceProvider.class);
                            String aggreid = sessionManager.getAggregator_ID();

                            if (aggreid.equalsIgnoreCase(sp.getUsername())) {
                                aggre = aggreid;
                                //     sp.setIschecked(true);
                            } else {
                                //     sp.setIschecked(false);
                            }


                            al_aggre.add(sp);

                        }

                        next.setEnabled(true);

                        if (!isfrombase && al_aggre.size() == 1) {
                            al_aggre.get(0).setIschecked(true);
                            mAdapter.notifyDataSetChanged();

                            aggre = al_aggre.get(0).getUsername();

                           /* Intent i = new Intent(SPActivity.this, ProfileAddressActivity.class);
                            i.putExtra("f_name", fname);
                            i.putExtra("email", email);
                            i.putExtra("phone", phone);
                            i.putExtra("aggre", aggre);
                            startActivity(i);
*/


                        } else {
                            mAdapter.notifyDataSetChanged();
                        }


                    } catch (Exception e) {


                    }
                } else {


                    try {

                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Toast.makeText(SPActivity.this, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {

                    }
                }


            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // Log error here since request failed
                findViewById(R.id.rl_pb).setVisibility(View.GONE);
            }
        });


    }

    ArrayList<ServiceProvider> al_aggre = new ArrayList<>();

    @Override
    public void onBackPressed() {
        if (isback) {
            super.onBackPressed();
        } else {

        }
    }


    public void usersignin(final boolean isupdate) {
        findViewById(R.id.rl_pb).setVisibility(View.VISIBLE);

        ApiInterface apiService =
                ApiClient.getClient(SPActivity.this).create(ApiInterface.class);


        Call call;
        String sp = getString(R.string.Aggregator);
        if (isupdate) {
            sp = aggre;
        }


        call = apiService.getagregatorProfile(sp);

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                int statusCode = response.code();
                findViewById(R.id.rl_pb).setVisibility(View.GONE);
                if (response.isSuccessful()) {

                    try {
                        String a = new Gson().toJson(response.body());
                        JSONObject Jsonresponse = new JSONObject(a);
                        JSONObject Jsonresponseinfo = Jsonresponse.getJSONObject("response");


                        Log.e("test", " trdds" + Jsonresponse.toString());
                        JSONArray aggregatorobj = Jsonresponseinfo.getJSONArray("data");
                        JSONObject aggregator = aggregatorobj.getJSONObject(0);
                        String aggregatorbridgeid = aggregator.getString("bridge_id");
                        String isactive = aggregator.getString("business_status");

                        String company_name = aggregator.getString("company_name");
                        sessionManager.setKEY_CURRENTCOMPANYNAME(company_name);

                        digicafebridgeid = aggregatorbridgeid;
                        Log.e("aggregatorbridgeid", aggregatorbridgeid);

                        if (isupdate) {
                            findViewById(R.id.rl_pb).setVisibility(View.GONE);
                            if (aggregator.has("company_detail_long")) {
                                String servicearea = aggregator.getString("company_detail_long");
                                //sessionManager.setservicearea(servicearea);
                            }

                            String company_image = aggregator.getString("company_image");
                            //sessionManager.setCompImg(company_image);

                            //sessionManager.setAggregatorprofile(aggregatorbridgeid);
                            //sessionManager.setAggregator_ID(aggre);

                            ArrayList<ModelCustom> al_user = new ArrayList<>();

                            final Gson gson = new Gson();


                            boolean isuseravailable = false;

                            String json = SharedPrefUserDetail.getString(SPActivity.this, SharedPrefUserDetail.userwithagreeid, "");


                            Type type = new TypeToken<ArrayList<ModelCustom>>() {
                            }.getType();

                            al_user = gson.fromJson(json, type);

                            if (al_user == null) {
                                al_user = new ArrayList<>();
                            }

                            String user = sessionManager.getcurrentu_nm();

                            isuseravailable = false;
                            for (int i = 0; i < al_user.size(); i++) {
                                String alluser = al_user.get(i).getUsernm();
                                if (user.equals(alluser)) {
                                    isuseravailable = true;
                                    //   al_user.get(i).setAggreid(aggre);
                                    break;
                                }

                            }

                            if (!isuseravailable) {
                                // al_user.add(new ModelCustom(user, aggre));
                            }

                            String str_array = gson.toJson(al_user);
                            SharedPrefUserDetail.setString(SPActivity.this, SharedPrefUserDetail.userwithagreeid, str_array);

                            if (isback) {
                                redirecttohome(user);
                            } else {
                                Intent i = new Intent(SPActivity.this, ProfileActivity.class);
                                i.putExtra("session", sessionManager.getsession());
                                i.putExtra("isback", true);
                                startActivity(i);
                            }


                        } else {
                            chooseaggregator();
                        }


                    } catch (Exception e) {


                    }
                } else {


                }

                //  List<Movie> movies = response.body().getResults();
                //  recyclerView.setAdapter(new MoviesAdapter(movies, R.layout.list_item_movie, getApplicationContext()));
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                findViewById(R.id.rl_pb).setVisibility(View.GONE);
            }
        });
    }


    String digicafebridgeid = "";

    public boolean onQueryTextChange(String newText) {

        List<ServiceProvider> filteredModelList = filter(al_aggre, newText);
        mAdapter.setitem(filteredModelList);


        return true;
    }

    public boolean onQueryTextSubmit(String query) {
        return false;
    }


    private List<ServiceProvider> filter(List<ServiceProvider> models, String query) {
        query = query.toLowerCase();
        final List<ServiceProvider> filteredModelList = new ArrayList<>();
        for (ServiceProvider model : models) {
            final String text = model.getCompany_name().toLowerCase();
            if (text.contains(query)) {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);


        final MenuItem item = menu.findItem(R.id.action_search);

        MenuItem item1 = menu.findItem(R.id.action_filter);
        item1.setVisible(false);

        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        //searchView.setInputType(InputType.TYPE_CLASS_NUMBER);
        searchView.setOnQueryTextListener(this);

        MenuItemCompat.setOnActionExpandListener(item,
                new MenuItemCompat.OnActionExpandListener() {
                    @Override
                    public boolean onMenuItemActionCollapse(MenuItem item) {
// Do something when collapsed
                        // mAdapter.setFilter(al_aggre);
                        return true; // Return true to collapse action view
                    }

                    @Override
                    public boolean onMenuItemActionExpand(MenuItem item) {
// Do something when expanded
                        return true; // Return true to expand action view
                    }
                });


        return true;
    }


    public void redirecttohome(String str_username) {

        findViewById(R.id.rl_pb).setVisibility(View.GONE);

        if (str_username.contains("@") && str_username.contains(".br")) // Employee
        {
            sessionManager.setisBusinesslogic(false);
            Intent i = new Intent(SPActivity.this, SearchActivity.class);  // All
            i.putExtra("session", sessionManager.getsession());
            // i.putExtra("isfromsignup", isfromsignup);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);

        } else if (str_username.matches("[0-9.]*")) // consumer
        {
            sessionManager.setisBusinesslogic(false);
            Intent i = new Intent(SPActivity.this, MainActivity.class);
            i.putExtra("session", sessionManager.getsession());
            // i.putExtra("isfromsignup", isfromsignup);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);


        } else {  // // Business


            Intent i = new Intent(SPActivity.this, SearchActivity.class);  // All
            i.putExtra("session", sessionManager.getsession());
            // i.putExtra("isfromsignup", isfromsignup);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            sessionManager.setisBusinesslogic(true);
            startActivity(i);
        }
    }


}
