package com.cab.digicafe.Activities.mrp;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.CircularProgressDrawable;
import android.support.v7.app.AlertDialog;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cab.digicafe.Activities.RequestApis.MyRequestCall;
import com.cab.digicafe.Activities.RequestApis.MyRequst;
import com.cab.digicafe.Fragments.SummaryDetailFragmnet;
import com.cab.digicafe.Helper.Constants;
import com.cab.digicafe.Helper.SessionManager;
import com.cab.digicafe.Model.Chit;
import com.cab.digicafe.MyCustomClass.JsonObjParse;
import com.cab.digicafe.MyCustomClass.PlayAnim;
import com.cab.digicafe.R;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.cab.digicafe.Activities.mrp.MrpFragmnet.vPos;
import static com.cab.digicafe.DetailsFragment.item_detail;


public class LinkToOdrCnfmDialogue extends AlertDialog implements
        View.OnClickListener {
    private final OnDialogClickListener listener;
    public Context c;
    public Dialog d;


    @BindView(R.id.llAnim)
    LinearLayout llAnim;

    @BindView(R.id.tvAdd)
    TextView tvAdd;

    @BindView(R.id.tvCancel)
    TextView tvCancel;


    @BindView(R.id.tvStatus)
    TextView tvStatus;

    @BindView(R.id.tvMsg)
    TextView tvMsg;

    @BindView(R.id.rlHide)
    RelativeLayout rlHide;
    Chit chitFromOdr;

    String title = "", msg = "";

    public LinkToOdrCnfmDialogue(Context a, OnDialogClickListener listener, String title, String msg, Chit chit) {
        super(a);
        // TODO Auto-generated constructor stub
        this.c = a;
        this.chitFromOdr = chit;
        this.listener = listener;
        this.title = title;
        this.msg = msg;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_anim_link_odr_dialogue);
        ButterKnife.bind(this);

        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        getWindow().setAttributes(lp);

        tvAdd.setOnClickListener(this);
        tvCancel.setOnClickListener(this);
        rlHide.setOnClickListener(this);
        setCancelable(true);

        tvStatus.setText(title);
        tvMsg.setText(msg);

        setOnKeyListener(new OnKeyListener() {

            @Override
            public boolean onKey(DialogInterface arg0, int keyCode,
                                 KeyEvent event) {
                // TODO Auto-generated method stub
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    dismissDialogue(0);
                }
                return true;
            }
        });

        rlHide.setOnClickListener(this);
        playAnim.slideDownRl(llAnim);

    }


    public void dismissDialogue(final int call) {
        playAnim.slideUpRl(llAnim);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                listener.onDialogImageRunClick(call, "");
                dismiss();

            }
        }, 700);

    }

    PlayAnim playAnim = new PlayAnim();

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvCancel:
                dismissDialogue(0);
                break;
            case R.id.tvAdd:
                fillList();


                //dismissDialogue(2);
                break;
            case R.id.rlHide:
                dismissDialogue(1);
                break;
            default:
                break;
        }
    }

    public interface OnDialogClickListener {
        void onDialogImageRunClick(int pos, String add);
    }

    MyRequestCall myRequestCall = new MyRequestCall();
    MyRequst myRequst = new MyRequst();
    JsonObject joAddEdit = new JsonObject();
    //JSONArray jaMrp = new JSONArray();

    public void addTaskPreferenceForMrpFromAllTask() {

        showLoader(tvAdd);


        // from all task only pass flage

        Chit chit = SummaryDetailFragmnet.item;
        //jaMrp = new JSONArray();
        //JSONObject joMrp = new JSONObject();
        JSONObject joFrom = new JSONObject(); // From should be his customer info
        JSONObject joTo = new JSONObject(); // supplier ; In To, supplier , name address etc

        try {

            /*joFrom.put("from_bridge_id", chit.getFrom_bridge_id());
            String ConsumerInfo = JsonObjParse.getValueEmpty(chit.getFooter_note(), "ConsumerInfo");
            if (!ConsumerInfo.isEmpty()) {
                JSONArray jaConsumerInfo = new JSONArray(ConsumerInfo);
                if (jaConsumerInfo.length() > 0) {
                    JSONObject joConsInfo = jaConsumerInfo.getJSONObject(0);

                    joFrom.put("ConsumerInfo", joConsInfo.toString());
                }
            }
            joFrom.put("ref_id", chit.getRef_id());
            joFrom.put("case_id", chit.getCase_id());
            joFrom.put("purpose", chit.getPurpose());
            joFrom.put("chit_hash_id", chit.getChit_hash_id());
            joFrom.put("chit_item_count", chit.getChit_item_count());
            joFrom.put("transaction_status", chit.getTransaction_status());
            joFrom.put("currency_code", chit.getCurrency_code());
            joFrom.put("currency_code_id", chit.getCurrency_code_id());
            joFrom.put("from_chit_id", chit.getChit_id());*/

            try {
                JSONObject jo_summary = new JSONObject(chit.getFooter_note());
                joFrom.put("DATE", jo_summary.getString("DATE"));
                joFrom.put("EDIT_DATE", jo_summary.getString("DATE"));
            } catch (Exception e) {
                e.printStackTrace();
            }
            joFrom.put("subject", chit.getSubject());
            joFrom.put("symbol_native", chit.getSymbol_native());
            joFrom.put("total_chit_item_value", chit.getTotal_chit_item_value());
            joFrom.put("edit_total_chit_item_value", chit.getTotal_chit_item_value());
            joFrom.put("location", chit.getLocation());

            //joMrp.put("From", joFrom);

            /****************************************************************************/

            /*joTo.put("to_bridge_id", chitFromOdr.getTo_bridge_id());
            joTo.put("chit_item_count", chitFromOdr.getChit_item_count());
            joTo.put("chit_hash_id", chitFromOdr.getChit_hash_id());
            joTo.put("currency_code", chitFromOdr.getCurrency_code());
            joTo.put("currency_code_id", chitFromOdr.getCurrency_code_id());
            joTo.put("to_chit_id", chitFromOdr.getChit_id());

            String BusinessInfo = JsonObjParse.getValueEmpty(chitFromOdr.getFooter_note(), "BusinessInfo");
            joTo.put("BusinessInfo", BusinessInfo);*/

            String DATE = JsonObjParse.getValueEmpty(chitFromOdr.getFooter_note(), "DATE");
            joTo.put("DATE", DATE);
            joTo.put("EDIT_DATE", DATE);
            joTo.put("subject", chitFromOdr.getSubject());
            joTo.put("symbol_native", chitFromOdr.getSymbol_native());
            joTo.put("total_chit_item_value", chitFromOdr.getTotal_chit_item_value() + "");
            joTo.put("edit_total_chit_item_value", chitFromOdr.getTotal_chit_item_value());
            joTo.put("location", chitFromOdr.getLocation());
            joTo.put("sort", Constants.SORT_MAT);


            //joMrp.put("To", joTo);

            /****************************************************************************/

            //joMrp.put("chit_id", chit.getChit_id());

            //jaMrp.put(joMrp);

                   /* String MRP_TAB = JsonObjParse.getValueEmpty(chit.getFooter_note(), "MRP_TAB");
                    if(!MRP_TAB.equalsIgnoreCase("")){
                        jaMrp.put(MRP_TAB);
                    }*/


        } catch (JSONException e) {
            e.printStackTrace();
        }


        joAddEdit.addProperty("flag", "add");

        joAddEdit.addProperty("from_chit_hash_id", chit.getChit_hash_id());
        joAddEdit.addProperty("from_chit_id", chit.getChit_id());
        joAddEdit.addProperty("from_ref_id", chit.getRef_id());
        joAddEdit.addProperty("from_case_id", chit.getCase_id());
        joAddEdit.addProperty("from_purpose", chit.getPurpose());
        joAddEdit.addProperty("from_field_json_data", joFrom.toString());

        joAddEdit.addProperty("to_bridge_id", chitFromOdr.getTo_bridge_id());
        joAddEdit.addProperty("to_chit_hash_id", chitFromOdr.getChit_hash_id());
        joAddEdit.addProperty("to_chit_id", chitFromOdr.getChit_id());
        joAddEdit.addProperty("to_ref_id", chitFromOdr.getRef_id());
        joAddEdit.addProperty("to_case_id", chitFromOdr.getCase_id());
        joAddEdit.addProperty("to_purpose", chitFromOdr.getPurpose());

        joAddEdit.addProperty("to_field_json_data", joTo.toString());
        if (vPos == 1) {
            joAddEdit.addProperty("task_purpose", "Material");
        } else if (vPos == 4) {
            joAddEdit.addProperty("task_purpose", "Delivery");
        }else{
            joAddEdit.addProperty("task_purpose", "Material");
        }

        JsonObject joMrpObj = new JsonObject();
        JsonArray jsonElements = new JsonArray();
        jsonElements.add(joAddEdit);

        joMrpObj.add("newdata", jsonElements);

        myRequst.cookie = new SessionManager().getsession();

        myRequestCall.addTaskPreferencr(c, myRequst, joMrpObj, new MyRequestCall.CallRequest() {
            @Override
            public void onGetResponse(String response) {

                if (!response.isEmpty()) {

                } else {

                }

                tvAdd.setBackground(null);
                tvAdd.setBackgroundColor(ContextCompat.getColor(c, R.color.colorPrimary));
                tvAdd.setText("Yes");

                dismissDialogue(2);

            }
        });

    }

    public void showLoader(TextView textView) {
        textView.setText("");
        CircularProgressDrawable circularProgressDrawable = new CircularProgressDrawable(c);
        circularProgressDrawable.setStrokeWidth(5f);
        circularProgressDrawable.setCenterRadius(30f);
        circularProgressDrawable.start();
        textView.setBackground(circularProgressDrawable);
    }


    Double totalOdrPrice = 0.0;

    private void fillList() {

        myRequst.page = 1;

        myRequst.chit_id = item_detail.getChit_id() + ""; // for to - in ALl task
        myRequst.to_chit_hash_id = chitFromOdr.getChit_hash_id();

        myRequst.task_purpose = "";

        myRequestCall.getTaskreference(c, new MyRequestCall.CallRequest() {
            @Override
            public void onGetResponse(String response) {


                try {
                    JSONObject jObjRes = new JSONObject(response);

                    JSONObject jObjdata = jObjRes.getJSONObject("response");
                    JSONObject jObjResponse = jObjdata.getJSONObject("data");
                    String totalRecord = jObjResponse.getString("totalRecord");

                    int totalVal = Integer.parseInt(totalRecord);
                    if (totalVal > 0) {

                        tvAdd.setBackground(null);
                        tvAdd.setBackgroundColor(ContextCompat.getColor(c, R.color.colorPrimary));
                        tvAdd.setText("Yes");

                        Toast.makeText(c, "Order is added already.", Toast.LENGTH_SHORT).show();

                    } else {
                        addTaskPreferenceForMrpFromAllTask();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, null, myRequst);

    }
}