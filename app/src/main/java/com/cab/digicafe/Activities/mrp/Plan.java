package com.cab.digicafe.Activities.mrp;

public class Plan {
    String task_title = "", task_amount = "", task_status = "";
    String task_due_date = "";


    public Plan(String task_title, String task_amount, String task_status, String task_due_date) {
        this.task_title = task_title;
        this.task_amount = task_amount;
        this.task_status = task_status;
        this.task_due_date = task_due_date;
    }

    public Plan() {

    }

    public String getTask_title() {
        return task_title;
    }

    public void setTask_title(String task_title) {
        this.task_title = task_title;
    }

    public String getTask_amount() {
        return task_amount;
    }

    public void setTask_amount(String task_amount) {
        this.task_amount = task_amount;
    }

    public String getTask_status() {
        return task_status;
    }

    public void setTask_status(String task_status) {
        this.task_status = task_status;
    }

    public String getTask_due_date() {
        return task_due_date;
    }

    public void setTask_due_date(String task_due_date) {
        this.task_due_date = task_due_date;
    }
}
