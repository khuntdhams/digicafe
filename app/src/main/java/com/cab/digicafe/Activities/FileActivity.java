package com.cab.digicafe.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.MergeCursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.cab.digicafe.Adapter.SelectFileAdapter;
import com.cab.digicafe.Model.ModelFile;
import com.cab.digicafe.R;
import com.google.gson.Gson;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class FileActivity extends MyAppBaseActivity {

    private File sdcard;
    private ArrayList<ModelFile> filelist = new ArrayList<ModelFile>();
    ArrayList<Bitmap> al_bmp = new ArrayList<Bitmap>();

    RecyclerView rv_gallery2;
    RecyclerView rv_selectfile;
    static final int REQUEST_PERMISSION_KEY = 131;
    boolean isdoc = false;
    SelectFileAdapter selectFileAdapter;
    ArrayList<ModelFile> al_selet = new ArrayList<>();
    TextView tv_save;
    LinearLayout ll_selected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_file);

        rv_gallery2 = (RecyclerView) findViewById(R.id.rv_gallery2);
        rv_selectfile = (RecyclerView) findViewById(R.id.rv_selectfile);
        tv_save = (TextView) findViewById(R.id.tv_save);
        ll_selected = (LinearLayout) findViewById(R.id.ll_selected);


        sdcard = new File(Environment.getExternalStorageDirectory().getAbsolutePath());
        Bundle myIntent = getIntent().getExtras();
        if (myIntent != null) {
            if (myIntent.containsKey("isdoc")) {

                isdoc = myIntent.getBoolean("isdoc", false);
                //destination_city = myIntent.getString("destination_city");
            }
        }


        findViewById(R.id.rl_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        /*String[] PERMISSIONS = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};
        if (!hasPermissions(this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, REQUEST_PERMISSION_KEY);
        }  else {

        }*/

        if (isdoc) {
            LoadVideo loadVideo = new LoadVideo();
            loadVideo.execute();
        } else {
            LoadAlbum loadAlbumTask = new LoadAlbum();
            loadAlbumTask.execute();
        }

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rv_selectfile.setLayoutManager(mLayoutManager);
        //recyclerView.setItemAnimator(new DefaultItemAnimator());

        selectFileAdapter = new SelectFileAdapter(al_selet, this, new SelectFileAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(String item) {

            }

            @Override
            public void onDelete(int pos) {
                al_selet.remove(pos);
                selectFileAdapter.setItem(al_selet);
                if (al_selet.size() == 0) ll_selected.setVisibility(View.GONE);

            }

            @Override
            public void onClickString(String pos) {

            }

            @Override
            public void onImgClick(List<ModelFile> data, int pos) {

            }
        }, isdoc, false, false);
        rv_selectfile.setAdapter(selectFileAdapter);

        tv_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                String paths = new Gson().toJson(al_selet);
                i.putExtra("al_selet", paths);
                setResult(RESULT_OK, i);
                onBackPressed();

            }
        });

        ll_selected.setVisibility(View.GONE);

    }

    class LoadAllFolder extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        protected String doInBackground(String... args) {


            if (sdcard.isDirectory()) {
                File[] files = sdcard.listFiles();

                try {
                    for (File f : files) {
                        if (!f.isDirectory()) {
                            if (f.getName().endsWith(".doc") || f.getName().endsWith(".pdf") || f.getName().endsWith(".docx") || f.getName().endsWith(".jpg") || f.getName().endsWith(".png") || f.getName().endsWith(".jpeg")) {
                                // Log.d(" FILES",f.getName());
                                //filelist.add(f.getAbsolutePath());

                            }
                        } else {
                        }
                    }
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    //e.printStackTrace();
                }
            }


            return null;
        }

        @Override
        protected void onPostExecute(String xml) {


        }
    }

    public class AlbumAdapter extends RecyclerView.Adapter<AlbumAdapter.MyviewHolder> {

        private Context activity;
        private ArrayList<ModelFile> data;
        int lastPosition = -1;

        public AlbumAdapter(Context a, ArrayList<ModelFile> d) {
            activity = a;
            data = d;
        }

        public class MyviewHolder extends RecyclerView.ViewHolder {

            ImageView galleryImage, iv_play;
            TextView tv_filenm;

            public MyviewHolder(View view) {
                super(view);

                galleryImage = (ImageView) view.findViewById(R.id.galleryImage);
                iv_play = (ImageView) view.findViewById(R.id.iv_play);
                tv_filenm = (TextView) view.findViewById(R.id.tv_filenm);
                //tv_ = (TextView)view.findViewById(R.id.tv_);

            }
        }

        @Override
        public MyviewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_album_row, parent, false);
            return new MyviewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final MyviewHolder holder, final int position) {

            ModelFile modelFile = data.get(position);
            holder.galleryImage.setId(position);

            holder.tv_filenm.setSelected(true);

            try {
                final BitmapFactory.Options options = new BitmapFactory.Options();
                if (isdoc) {
                    Bitmap bmThumbnail;
                    bmThumbnail = ThumbnailUtils.createVideoThumbnail(modelFile.getImgpath(), MediaStore.Video.Thumbnails.MINI_KIND);

                    holder.galleryImage.setImageDrawable(null);
                    holder.galleryImage.setImageBitmap(bmThumbnail);
                    holder.iv_play.setVisibility(View.VISIBLE);
                    // Glide.with(activity).load(data.get(position)).asBitmap().diskCacheStrategy(DiskCacheStrategy.ALL).placeholder(R.drawable.defaultplace).into(holder.galleryImage);


                } else if (!isdoc) {
                    holder.iv_play.setVisibility(View.GONE);
                    Glide.with(activity).load(new File(modelFile.getImgpath())).diskCacheStrategy(DiskCacheStrategy.ALL).placeholder(R.drawable.defaultplace).into(holder.galleryImage);

                }


                holder.tv_filenm.setText(new File(modelFile.getImgpath()).getName());

            } catch (Exception e) {
            }

            holder.galleryImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    /*Intent i = new Intent();
                    i.putExtra("path",data.get(position));
                    setResult(RESULT_OK,i);
                    finish();*/
                    al_selet.add(new ModelFile(data.get(position).getImgpath(), true));
                    Collections.reverse(al_selet);
                    selectFileAdapter.setItem(al_selet);
                    ll_selected.setVisibility(View.VISIBLE);
                    //rv_selectfile.scrollToPosition(data.size()-1);
                }
            });


        }

        @Override
        public int getItemCount() {
            return data.size();
        }
    }

    class LoadAlbum extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        protected String doInBackground(String... args) {
            String xml = "";

            String path = null;
            String videopath = null;
            String videoname = null;
            String album = null;
            String timestamp = null;
            String countPhoto = null;
            Uri uriExternal = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
            Uri uriInternal = MediaStore.Images.Media.INTERNAL_CONTENT_URI;
            Uri uriInternalFile = MediaStore.Files.getContentUri("internal");
            Uri uriExternalFile = MediaStore.Files.getContentUri("external");

            String[] projection = {MediaStore.MediaColumns.DATA,
                    MediaStore.Images.Media.BUCKET_DISPLAY_NAME, MediaStore.MediaColumns.DATE_MODIFIED};

            String selection = MediaStore.Files.FileColumns.MEDIA_TYPE + "="
                    + MediaStore.Files.FileColumns.MEDIA_TYPE_NONE;

            Cursor cursorExternal = getContentResolver().query(uriExternal, projection, "_data IS NOT NULL) GROUP BY (bucket_display_name",
                    null, null);
            Cursor cursorInternal = getContentResolver().query(uriInternal, projection, "_data IS NOT NULL) GROUP BY (bucket_display_name",
                    null, null);

            Cursor cursorInternalFile = getContentResolver().query(uriInternalFile, null, selection,
                    null, null);

            Cursor cursorExtFile = getContentResolver().query(uriExternalFile, null, selection,
                    null, null);

            Cursor cursor = new MergeCursor(new Cursor[]{cursorExternal, cursorInternal, cursorInternalFile, cursorExtFile});

            while (cursor.moveToNext()) {

                path = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA));

                if (isdoc) {
                    if (path.endsWith(".pdf") || path.endsWith(".doc") || path.endsWith(".docx")) {
                        // filelist.add(path);
                    }
                } else if (!isdoc) {
                    if (path.endsWith(".png") || path.endsWith(".jpg") || path.endsWith(".jpeg")) {
                        filelist.add(new ModelFile(path, true));
                    }
                }


            }
            cursor.close();

            return xml;
        }

        @Override
        protected void onPostExecute(String xml) {

            GridLayoutManager gridLayoutManager = new GridLayoutManager(FileActivity.this, 3);
            AlbumAdapter adapter2 = new AlbumAdapter(FileActivity.this, filelist);
            rv_gallery2.setAdapter(adapter2);
            rv_gallery2.setLayoutManager(gridLayoutManager);


        }
    }

    class LoadVideo extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        protected String doInBackground(String... args) {
            String xml = "";

            String path = null;
            Uri uriExternal = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
            Uri uriInternal = MediaStore.Video.Media.INTERNAL_CONTENT_URI;
            Uri uriInternalFile = MediaStore.Files.getContentUri("internal");
            Uri uriExternalFile = MediaStore.Files.getContentUri("external");

            String[] projection = {MediaStore.Video.VideoColumns.DATA};

            Cursor cursorExternal = getContentResolver().query(uriExternal, projection, null,
                    null, null);
            Cursor cursorInternal = getContentResolver().query(uriInternal, projection, null,
                    null, null);


            Cursor cursor = new MergeCursor(new Cursor[]{cursorExternal, cursorInternal});

            while (cursor.moveToNext()) {

                path = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA));
                filelist.add(new ModelFile(path, true));
                Log.e("path", path);

            }
            cursor.close();

            return xml;
        }

        @Override
        protected void onPostExecute(String xml) {

            GridLayoutManager gridLayoutManager = new GridLayoutManager(FileActivity.this, 3);
            AlbumAdapter adapter2 = new AlbumAdapter(FileActivity.this, filelist);
            rv_gallery2.setAdapter(adapter2);
            rv_gallery2.setLayoutManager(gridLayoutManager);


        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_PERMISSION_KEY: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    LoadAlbum loadAlbumTask = new LoadAlbum();
                    loadAlbumTask.execute();
                } else {
                    Toast.makeText(FileActivity.this, "You must accept permissions.", Toast.LENGTH_LONG).show();
                }
            }
        }

    }


}
