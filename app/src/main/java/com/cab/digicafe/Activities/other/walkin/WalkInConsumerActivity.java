package com.cab.digicafe.Activities.other.walkin;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.cab.digicafe.Activities.MyAppBaseActivity;
import com.cab.digicafe.Activities.RequestApis.MyRequst;
import com.cab.digicafe.Helper.CurrencyCommaSeperator;
import com.cab.digicafe.Helper.Inad;
import com.cab.digicafe.Helper.SessionManager;
import com.cab.digicafe.MyCustomClass.AutoCompleteTv;
import com.cab.digicafe.MyCustomClass.JsonObjParse;
import com.cab.digicafe.MyCustomClass.PlayAnim;
import com.cab.digicafe.MyCustomClass.Utils;
import com.cab.digicafe.R;
import com.cab.digicafe.Timeline.DateTimeUtils;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

public class WalkInConsumerActivity extends MyAppBaseActivity implements WalkInContarct.View {

    @BindView(R.id.tvTitle)
    TextView tvTitle;


    @BindView(R.id.tvCancel)
    TextView tvCancel;

    @OnClick(R.id.tvCancel)
    public void tvCancel(View v) {
        currentstatus = "getConsumer";
        setSubmitBtnTxt();
    }

    public void clearEt(EditText editText) {
        editText.setText("");
    }

    @OnClick(R.id.iv_back)
    public void iv_back(View v) {
        onBackPressed();
    }

    @BindView(R.id.tvSubmiWalkIn)
    TextView tvSubmiWalkIn;

    String currentstatus = "getConsumer";

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }

    public void setSubmitBtnTxt() {

        //tvCancel.setEnabled(true);
        //tvCancel.setAlpha(1f);
        setViewToggle(tvCancel, true);

        llEnterNewConsumer.setVisibility(View.GONE);
        llFoundConsumerDetails.setVisibility(View.GONE);
        llShowingEnterPromo.setVisibility(View.GONE);


        llConsumerPhone.setVisibility(View.VISIBLE);

        if (currentstatus.equals("getConsumer")) {

            tvTitle.setText("Find walkin details");


          /*  tvCancel.setEnabled(false);
            tvCancel.setAlpha(0.7f);*/

            isReferrer = false;
            isReferrerManually = false;
            setViewToggle(tvCancel, false);
            setViewToggle(etConsumerPhone, true);
            setViewToggle(etReferrePhone, true);
            setViewToggle(ivSearchReferrer, true);

            clearEt(etEnterPromoPt);
            clearEt(etConsumerNm);
            clearEt(etEnterPromoPtForNew);
            clearEt(etReferrePhone);
            clearEt(etEnterPromoPtForReferrer);
            clearEt(etReferrerNm);

            //cbFound.setVisibility(View.VISIBLE);
            //cbFound.setText("Found consumer - testing purpose");
            //etConsumerPhone.setEnabled(true);
            //etConsumerPhone.setAlpha(1f);

            tvSubmiWalkIn.setText("Search");


        } else if (currentstatus.equals("updateConsumer")) { // found

            tvTitle.setText("Edit walkin details");


            //cbFound.setVisibility(View.GONE);
            //etConsumerPhone.setEnabled(false);
            //etConsumerPhone.setAlpha(0.5f);

            setViewToggle(etConsumerPhone, false);


            playAnim(llFoundConsumerDetails);
            llFoundConsumerDetails.setVisibility(View.VISIBLE);
            tvSubmiWalkIn.setText("Update");


            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {

                    if (sessionManager.getIsEmployess()) {
                        getBusinessProfileForEmployee(new MyCallForFieldJson() {
                            @Override
                            public void call(String field_json) {

                                String walk_in_details = JsonObjParse.getValueEmpty(field_json, "walk_in_details");
                                String default_promo_value = JsonObjParse.getValueEmpty(walk_in_details, "default_promo_value");
                                etEnterPromoPt.setText(default_promo_value);

                            }
                        });
                    } else {
                        String walk_in_details = getValueFromFieldJson("walk_in_details");
                        String default_promo_value = JsonObjParse.getValueEmpty(walk_in_details, "default_promo_value");
                        etEnterPromoPt.setText(default_promo_value);
                    }

                }
            }, 500);


        } else if (currentstatus.equals("enterConsumer")) { // not found

            //cbFound.setVisibility(View.GONE);
            //etConsumerPhone.setEnabled(false);
            //etConsumerPhone.setAlpha(0.5f);
            setViewToggle(etConsumerPhone, false);

            llShowingEnterPromo.setVisibility(View.GONE);
            tvTitle.setText("Insert walkin details");

            //cbFound.setVisibility(View.VISIBLE);
            //cbFound.setText("Found referrer - testing purpose");


            focusEt(etConsumerNm);

            llEnterNewConsumer.setVisibility(View.VISIBLE);
            setRefChannel();
            playAnim(llEnterNewConsumer);
            tvSubmiWalkIn.setText("Save");


            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {

                    if (sessionManager.getIsEmployess()) {

                        getBusinessProfileForEmployee(new MyCallForFieldJson() {
                            @Override
                            public void call(String field_json) {
                                String walk_in_details = JsonObjParse.getValueEmpty(field_json, "walk_in_details");
                                String default_promo_value = JsonObjParse.getValueEmpty(walk_in_details, "default_promo_value");
                                etEnterPromoPtForNew.setText(default_promo_value);
                            }
                        });

                    } else {
                        String walk_in_details = getValueFromFieldJson("walk_in_details");
                        String default_promo_value = JsonObjParse.getValueEmpty(walk_in_details, "default_promo_value");
                        etEnterPromoPtForNew.setText(default_promo_value);
                    }

                }
            }, 500);


        }

    }

    String getConsNm = "", getLastVisit = "";
    int getTotalVisit = 0, getPromoPt = 0, getRefVisit = 0, getRefPromoPt = 0;
    int enterPromo = 0, enterRefPromo = 0, enterVisit = 0, enterRefVisit = 0;

    int grandVisit = 0, grandPoint = 0;

    public void calcPromoAndVisit(String entrPromo) {

        enterPromo = getPromoPt;
        enterRefPromo = getRefPromoPt;
        if (!entrPromo.isEmpty()) {
            enterPromo = enterPromo + Integer.parseInt(entrPromo);
            enterRefPromo = enterRefPromo + Integer.parseInt(entrPromo);
        } else {

        }

        enterVisit = getTotalVisit;
        enterRefVisit = getRefVisit;
        if (isReferrer && cbIncVisitForReferrer.isChecked()) {
            enterVisit++;
            enterRefVisit++;
        } else if (!isReferrer && cbIncVisit.isChecked()) {
            enterVisit++;
            enterRefVisit++;

        } else {

        }

        tvUpdatingVisitAndPromo.setText("Total visit is : " + enterVisit + ", and\n total promotional point is : " + enterPromo);
        tvUpdatingVisitAndReferrerPromo.setText("Total visit of referrer is : " + enterVisit + ", and\n total promotional point of referrer is : " + enterPromo);

    }

    @OnClick(R.id.ivSearchReferrer)
    public void ivSearchReferrer(View v) {
        String referrerPhone = etReferrePhone.getText().toString().trim();
        if (referrerPhone.isEmpty()) {
            Inad.alerterInfo("Alert", "Please enter referrer contact number", (Activity) context);
        } else if (referrerPhone.length() < 10) {
            Inad.alerterInfo("Alert", "Please enter valid contact number", (Activity) context);
        } else if (referrerPhone.equalsIgnoreCase(etConsumerPhone.getText().toString().trim())) {
            Inad.alerterInfo("Walk-in", "You can not referrer to self..", (Activity) context);
        } else {
            myRequst.contact_number = referrerPhone;
            isSearchReferrer = true;
            getCotactsByNo(jsonObject, myRequst);


        }

    }

    @OnClick(R.id.tvSubmiWalkIn)
    public void tvSubmiWalkIn(View v) {

        String conumerPhone = etConsumerPhone.getText().toString().trim();
        String conumerNm = etConsumerNm.getText().toString().trim();

        if (currentstatus.equals("getConsumer")) { // for referrer
            if (conumerPhone.isEmpty()) {
                Inad.alerterInfo("Alert", "Please enter consumer contact number", (Activity) context);
            } else if (conumerPhone.length() < 10) {
                Inad.alerterInfo("Alert", "Please enter valid contact number", (Activity) context);
            } else {
                myRequst.contact_number = conumerPhone;
                getCotactsByNo(jsonObject, myRequst);
            }
        } else if (currentstatus.equals("updateConsumer")) {

            String conumerPromo = etEnterPromoPt.getText().toString().trim();
            String conumerNmUpdate = tvGetConsNm.getText().toString().trim();

            if (conumerNmUpdate.isEmpty()) {
                tvGetConsNm.requestFocus();
                Inad.alerterInfo("Alert", "Please enter consumer name", (Activity) context);
            } else if (conumerPromo.isEmpty()) {
                Inad.alerterInfo("Alert", "Please enter consumer promotional point", (Activity) context);
            } else {

                JsonObject joNewData = new JsonObject();
                joNewData.addProperty("external_reference_id", externalContactViewModel.getExternalReferenceId());
                joNewData.addProperty("contact_visit_count", enterVisit); // for new
                joNewData.addProperty("contact_visit_value", enterPromo);
                joNewData.addProperty("purpose_type", externalContactViewModel.getPurposeType());
                joNewData.addProperty("contact_number", externalContactViewModel.getContactNumber());
                joNewData.addProperty("contact_name", conumerNmUpdate);

                if (cbIncVisit.isChecked()) {
                    String fieldJson = externalContactViewModel.getFieldJsonData();
                    try {
                        JSONObject joFIeld = new JSONObject(fieldJson);
                        joFIeld.put("last_visit", DateTimeUtils.formatCurrentDateToStringWithTime());
                        joNewData.addProperty("field_json_data", joFIeld.toString()); // last + current
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                JsonArray jsonElements = new JsonArray();
                jsonElements.add(joNewData);

                jsonObject = new JsonObject();
                jsonObject.add("olddata", jsonElements);
                updateConatcts(jsonObject, myRequst);

                //Inad.alerterGrnInfo("Alert", "Update consumer successfully", (Activity) context);
                //tvReqJson.setText("Update Consumer : " + jsonObject.toString() + "");
            }

        } else if (currentstatus.equals("enterConsumer")) {

            String conumerPromo = etEnterPromoPtForNew.getText().toString().trim();
            String referrerPromo = etEnterPromoPtForReferrer.getText().toString().trim();
            String referrerPhone = etReferrePhone.getText().toString().trim();
            String referrerNm = etReferrerNm.getText().toString().trim();

            if (conumerNm.isEmpty()) {
                Inad.alerterInfo("Alert", "Please enter consumer name", (Activity) context);
            } else if (conumerPromo.isEmpty()) {
                Inad.alerterInfo("Alert", "Please enter consumer promo", (Activity) context);
            } else {

                try {
                    if ((isReferrerManually || isReferrer) && (referrerPhone.isEmpty() || referrerNm.isEmpty() || referrerPromo.isEmpty())) {
                        Inad.alerterInfo("Alert", "Please enter referrer details", (Activity) context);
                        return;
                    }

                    JsonObject joNewData = new JsonObject();
                    joNewData.addProperty("flag", "add");
                    joNewData.addProperty("purpose_type", "walkin");
                    joNewData.addProperty("contact_number", conumerPhone);
                    joNewData.addProperty("contact_name", conumerNm);
                    joNewData.addProperty("contact_visit_count", "1"); // for new
                    joNewData.addProperty("contact_visit_value", conumerPromo);

                    JSONObject joField = new JSONObject();
                    joField.put("last_visit", DateTimeUtils.formatCurrentDateToStringWithTime());
                    joField.put("referrer_channel_id", spn_ref_channel.getSelectedItemPosition());

                    if (isReferrer || isReferrerManually) {
                        joNewData.addProperty("source_of_reference", "");
                        joNewData.addProperty("referrer_contact_number", referrerPhone);
                        joNewData.addProperty("referrer_contact_name", referrerNm);
                        //joNewData.addProperty("referrel_count", "1"); // last + current
                        joNewData.addProperty("referrel_count", "0"); // last + current
                        //joNewData.addProperty("referrel_value", referrerPromo); // last + current
                        joNewData.addProperty("referrel_value", "0"); // last + current
                        //joField.put("last_ref_visit", DateTimeUtils.formatCurrentDateToStringWithTime());
                        joField.put("last_ref_visit", "");
                    } else {
                        joNewData.addProperty("referrel_count", "0"); // last + current

                    }
                    joField.put("user_id", sessionManager.getcurrentu_nm());
                    joNewData.addProperty("field_json_data", joField.toString()); // last + current

                    JsonArray jsonElements = new JsonArray();
                    jsonElements.add(joNewData);

                    jsonObject = new JsonObject();
                    jsonObject.add("newdata", jsonElements);
                    insertConatcts(jsonObject, myRequst);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }
    }

    Context context;
    WalkInContarct.Presenter presenter;
    JsonObject jsonObject = new JsonObject();
    MyRequst myRequst = new MyRequst();
    ExternalContactViewModel externalContactViewModel = new ExternalContactViewModel();
    boolean isReferrer = false;
    boolean isReferrerManually = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_walk_in_consumer);
        context = this;
        sessionManager = new SessionManager(context);
        ButterKnife.bind(this);
        presenter = new WalkInPresenter(this, this);

        setSubmitBtnTxt();

        etReferrePhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                String refPhone = s.toString().trim();
                if (refPhone.length() > 9) {

                    if (refPhone.equalsIgnoreCase(etConsumerPhone.getText().toString().trim())) {
                        Inad.alerterInfo("Walk-in", "You can not referrer to self..", (Activity) context);
                    } else {
                        isReferrerManually = true;
                        ivSearchReferrer.performClick();
                        llShowingEnterPromo.setVisibility(View.VISIBLE);

                        autoAddRef();
                    }
                } else {
                    isReferrerManually = false;
                    llShowingEnterPromo.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        etConsumerPhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                String consPhone = s.toString().trim();
                if (consPhone.length() > 9) {

                    View view = getCurrentFocus();
                    if (view != null) {
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    }

                    tvSubmiWalkIn.performClick();
                } else {

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        focusEt(etConsumerPhone);

    }

    @BindView(R.id.etConsumerNm)
    EditText etConsumerNm;

    @BindView(R.id.etConsumerPhone)
    EditText etConsumerPhone;

    @BindView(R.id.etConsumerRefNm)
    EditText etConsumerRefNm;

    @BindView(R.id.etConsumerRefPhone)
    EditText etConsumerRefPhone;

    @BindView(R.id.tvReqJson)
    TextView tvReqJson;

    @BindView(R.id.llConsumerPhone)
    LinearLayout llConsumerPhone;

    @BindView(R.id.llFoundConsumerDetails)
    LinearLayout llFoundConsumerDetails;

    @BindView(R.id.llEnterNewConsumer)
    LinearLayout llEnterNewConsumer;

    @BindView(R.id.tvGetConsNm)
    EditText tvGetConsNm;

    @BindView(R.id.tvGetLastVisit)
    TextView tvGetLastVisit;

    @BindView(R.id.tvGetTotalVisit)
    TextView tvGetTotalVisit;

    @BindView(R.id.tvGetPromoPt)
    TextView tvGetPromoPt;

    @BindView(R.id.etEnterPromoPt)
    EditText etEnterPromoPt;

    @BindView(R.id.etEnterPromoPtForNew)
    EditText etEnterPromoPtForNew;

    @BindView(R.id.etEnterPromoPtForReferrer)
    EditText etEnterPromoPtForReferrer;

    @BindView(R.id.cbIncVisit)
    CheckBox cbIncVisit;

    @BindView(R.id.cbIncVisitForReferrer)
    CheckBox cbIncVisitForReferrer;

    @BindView(R.id.pb)
    ProgressBar pb;

    @BindView(R.id.tvUpdatingVisitAndPromo)
    TextView tvUpdatingVisitAndPromo;

    @BindView(R.id.tvUpdatingVisitAndReferrerPromo)
    TextView tvUpdatingVisitAndReferrerPromo;

    @BindView(R.id.ivSearchReferrer)
    ImageView ivSearchReferrer;

    @BindView(R.id.etReferrePhone)
    EditText etReferrePhone;

    @BindView(R.id.etReferrerNm)
    EditText etReferrerNm;

    @BindView(R.id.llShowingEnterPromo)
    LinearLayout llShowingEnterPromo;


    SessionManager sessionManager;


    @Override
    public void showMessage(String message) {

    }

    @Override
    public void showError(String error) {

    }

    boolean isSearchReferrer = false;

    @Override
    public void showProgress() {
        if (isSearchReferrer) {

            ivSearchReferrer.setEnabled(false);
            showLoader(ivSearchReferrer);
        } else {
            tvSubmiWalkIn.setEnabled(false);
            showLoader(tvSubmiWalkIn);
        }

    }

    @Override
    public void hideProgress() {

        pb.setVisibility(View.GONE);
        if (isReferrer) {

        } else {
            tvSubmiWalkIn.setEnabled(true);
            tvSubmiWalkIn.setBackground(null);
            tvSubmiWalkIn.setBackgroundColor(ContextCompat.getColor(context, R.color.ok));

            ivSearchReferrer.setImageResource(R.mipmap.ic_search);
            ivSearchReferrer.setBackground(ContextCompat.getDrawable(context, R.drawable.oval_red));
        }


    }

    public void getCotactsByNo(JsonObject jsonObject, MyRequst myRequst) {
        presenter.findExternalContactsByContactNo(jsonObject, myRequst);
    }


    @Override
    public void onGetContact(Response response) {

        isSearchReferrer = false;

        try {

            String data = new Gson().toJson(response.body());
            JSONObject jObjRes = new JSONObject(data);

            JSONObject jObjdata = jObjRes.getJSONObject("response");
            JSONObject jObjResponse = jObjdata.getJSONObject("data");
            String totalRecord = jObjResponse.getString("totalRecord");

            //tvTitle.setText("WalkIn [" + totalRecord + "]");
            JSONArray jObjcontacts = jObjResponse.getJSONArray("content");

            if (currentstatus.equalsIgnoreCase("enterConsumer")) { // for referrer

                if (jObjcontacts.length() > 0) {

                    cbIncVisitForReferrer.setVisibility(View.VISIBLE);


                    JSONObject joAt0 = jObjcontacts.getJSONObject(0);
                    externalContactViewModel = new Gson().fromJson(joAt0.toString(), ExternalContactViewModel.class);
                    //Inad.alerterGrnInfo("Walk-in", "Referrer details found", (Activity) context);

                    isReferrer = true;
                    isReferrerManually = false;
                    setViewToggle(etReferrePhone, false);
                    setViewToggle(ivSearchReferrer, false);

                    if (spn_ref_channel.getSelectedItemPosition() == 8) {
                        etReferrerNm.setText("");
                        focusEt(etReferrerNm);
                    } else {
                        etReferrerNm.setText(externalContactViewModel.getContactName());
                    }

                    getTotalVisit = Integer.parseInt(externalContactViewModel.getContactVisitCount());
                    getPromoPt = Integer.parseInt(externalContactViewModel.getContactVisitValue());
                    getRefVisit = Integer.parseInt(externalContactViewModel.getReferrelCount());
                    getRefPromoPt = Integer.parseInt(externalContactViewModel.getReferrelValue());

                    llShowingEnterPromo.setVisibility(View.VISIBLE);


                    autoAddRef();


                } else {
                    setViewToggle(ivSearchReferrer, true);
                    isReferrer = false;
                    isReferrerManually = true;
                    cbIncVisitForReferrer.setVisibility(View.GONE);
                    //etReferrePhone.setText("");
                    //Inad.alerterInfo("Walk-in", "Referrer not found", (Activity) context);

                    tvReqJson.setText("");

                    try {
                        if (etReferrerNm.getText().toString().length() == 0) {
                            etReferrerNm.requestFocus();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

                etEnterPromoPtForReferrer.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        String entrPromo = String.valueOf(s);
                        calcPromoAndVisit(entrPromo);

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                    }
                });


                cbIncVisitForReferrer.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        calcPromoAndVisit(etEnterPromoPtForReferrer.getText().toString().trim());
                    }
                });

                calcPromoAndVisit("");


            } else if (currentstatus.equalsIgnoreCase("getConsumer") && jObjcontacts.length() > 0) {
                // for consumer
                JSONObject joAt0 = jObjcontacts.getJSONObject(0);
                externalContactViewModel = new Gson().fromJson(joAt0.toString(), ExternalContactViewModel.class);
                //Inad.alerterGrnInfo("Walk-in", "Consumer details found", (Activity) context);

                currentstatus = "updateConsumer"; // if found
                setSubmitBtnTxt();

                getConsNm = externalContactViewModel.getContactName();
                //getLastVisit = externalContactViewModel.getUpdatedDate();

                String walk_in_details = externalContactViewModel.getFieldJsonData();
                getLastVisit = JsonObjParse.getValueEmpty(walk_in_details, "last_visit");


                //getLastVisit = DateTimeUtils.parseDateTime(getLastVisit, "yyyy-MM-dd HH:mm:ss", "dd MMM, yyyy hh:mm a");
                getTotalVisit = Integer.parseInt(externalContactViewModel.getContactVisitCount());
                getRefVisit = Integer.parseInt(externalContactViewModel.getReferrelCount());
                getPromoPt = Integer.parseInt(externalContactViewModel.getContactVisitValue());
                getRefPromoPt = Integer.parseInt(externalContactViewModel.getReferrelValue());

                tvGetConsNm.setText(" " + getConsNm);
                tvGetLastVisit.setText(" " + getLastVisit);

                //grandVisit = getRefVisit + getTotalVisit;
                grandPoint = getRefPromoPt + getPromoPt;

                setVisit(0);
                setRefVisit(0);

                setPoints(0);

                //setRefPoints(0);

                checkVisitCtrShow();

                //tvGetPromoPt.setText("" + getPromoPt);


                llFoundConsumerDetails.setVisibility(View.VISIBLE);
                etEnterPromoPt.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        String entrPromo = String.valueOf(s);
                        calcPromoAndVisit(entrPromo);
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                    }
                });


                cbIncVisit.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        calcPromoAndVisit(etEnterPromoPt.getText().toString().trim());
                    }
                });

                calcPromoAndVisit("");


                if (sessionManager.getIsEmployess()) {
                    getBusinessProfileForEmployee(new MyCallForFieldJson() {
                        @Override
                        public void call(String field_json) {

                            String walk_in_details_setting = JsonObjParse.getValueEmpty(field_json, "walk_in_details");
                            if (checkIncrementVisitCondition(walk_in_details_setting)) {
                                cbIncVisit.setEnabled(true);
                                cbIncVisit.setChecked(true);
                            } else {
                                cbIncVisit.setEnabled(false);
                                cbIncVisit.setChecked(false);
                            }

                        }
                    });
                } else {
                    String walk_in_details_setting = getValueFromFieldJson("walk_in_details");
                    if (checkIncrementVisitCondition(walk_in_details_setting)) {
                        cbIncVisit.setEnabled(true);
                        cbIncVisit.setChecked(true);
                    } else {
                        cbIncVisit.setEnabled(false);
                        cbIncVisit.setChecked(false);
                    }
                }


            } else {
                //Inad.alerter("Walk-in", "Consumer not found, Please enter new walkin..", (Activity) context);
                currentstatus = "enterConsumer"; // if found
                setSubmitBtnTxt();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public void setVisit(int val) {
        if (val < getTotalVisit) {

            if (getTotalVisit > 1000) {
                val = val + 100;
            } else if (getTotalVisit > 100) {
                val = val + 10;
            } else if (getTotalVisit > 25) {
                val = val + 5;
            } else {
                val++;
            }

            tvGetTotalVisit.setText(CurrencyCommaSeperator.convertStringToComma(val) + "");
            final Handler handler = new Handler();
            final int finalVal = val;
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    setVisit(finalVal);
                }
            }, 100);
        } else {

            tvGetTotalVisit.setText(CurrencyCommaSeperator.convertStringToComma(getTotalVisit) + "");

        }
    }

    public void setRefVisit(int val) {
        if (val < getRefVisit) {

            if (getRefVisit > 1000) {
                val = val + 100;
            } else if (getRefVisit > 100) {
                val = val + 10;
            } else if (getRefVisit > 25) {
                val = val + 5;
            } else {
                val++;
            }

            tvGetTotalRefVisit.setText(CurrencyCommaSeperator.convertStringToComma(val) + "");
            final Handler handler = new Handler();
            final int finalVal = val;
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    setRefVisit(finalVal);
                }
            }, 100);
        } else {

            tvGetTotalRefVisit.setText(CurrencyCommaSeperator.convertStringToComma(getRefVisit) + "");

        }
    }

    public void setPoints(int val) {
        if (val < grandPoint) {

            if (grandPoint > 5000) {
                val = val + 500;
            } else if (grandPoint > 1000) {
                val = val + 100;
            } else if (grandPoint > 100) {
                val = val + 25;
            } else if (grandPoint > 25) {
                val = val + 5;
            } else {
                val++;
            }

            tvGetPromoPt.setText(CurrencyCommaSeperator.convertStringToComma(val) + "");

            final Handler handler = new Handler();
            final int finalVal = val;
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    setPoints(finalVal);
                }
            }, 100);
        } else {
            tvGetPromoPt.setText(CurrencyCommaSeperator.convertStringToComma(grandPoint) + "");
        }
    }

    public void setRefPoints(int val) {
        if (val < getRefPromoPt) {

            if (getRefPromoPt > 5000) {
                val = val + 500;
            } else if (getRefPromoPt > 1000) {
                val = val + 100;
            } else if (getRefPromoPt > 100) {
                val = val + 25;
            } else if (getRefPromoPt > 25) {
                val = val + 5;
            } else {
                val++;
            }

            tvGetTotalRefPt.setText(CurrencyCommaSeperator.convertStringToComma(val) + "");

            final Handler handler = new Handler();
            final int finalVal = val;
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    setRefPoints(finalVal);
                }
            }, 100);
        } else {
            tvGetTotalRefPt.setText(CurrencyCommaSeperator.convertStringToComma(getRefPromoPt) + "");

        }
    }

    public void insertConatcts(JsonObject jsonObject, MyRequst myRequst) {
        presenter.insertExternalContactsByContactNo(jsonObject, myRequst);
    }

    public void updateReferrer() { // if found  and update only ref visit date

        JsonObject joNewData = new JsonObject();
        joNewData.addProperty("external_reference_id", externalContactViewModel.getExternalReferenceId());
        //joNewData.addProperty("contact_visit_count", enterVisit); // for new
        //joNewData.addProperty("contact_visit_value", enterPromo);
        joNewData.addProperty("referrel_count", enterRefVisit); // for new
        joNewData.addProperty("referrel_value", enterRefPromo);
        joNewData.addProperty("purpose_type", externalContactViewModel.getPurposeType());
        joNewData.addProperty("contact_number", externalContactViewModel.getContactNumber());
        joNewData.addProperty("contact_name", externalContactViewModel.getContactName());

        String fieldJson = externalContactViewModel.getFieldJsonData();
        try {
            JSONObject joFIeld = new JSONObject(fieldJson);
            joFIeld.put("last_ref_visit", DateTimeUtils.formatCurrentDateToStringWithTime());
            joNewData.addProperty("field_json_data", joFIeld.toString()); // last + current
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonArray jsonElements = new JsonArray();
        jsonElements.add(joNewData);

        jsonObject = new JsonObject();
        jsonObject.add("olddata", jsonElements);
        updateConatcts(jsonObject, myRequst);


    }

    public void insertReferrerManually() { // if not found , and enter only ref visit date

        JsonObject joNewData = new JsonObject();
        joNewData.addProperty("flag", "add");
        joNewData.addProperty("purpose_type", "walkin");
        joNewData.addProperty("contact_number", etReferrePhone.getText().toString().trim());
        joNewData.addProperty("contact_name", etReferrerNm.getText().toString().trim());
        joNewData.addProperty("contact_visit_count", "0"); // for new
        joNewData.addProperty("contact_visit_value", "0");

        joNewData.addProperty("source_of_reference", "");
        joNewData.addProperty("referrer_contact_number", "");
        joNewData.addProperty("referrer_contact_name", "");
        joNewData.addProperty("referrel_count", "1"); // last + current

        try {
            JSONObject joFIeld = new JSONObject();
            joFIeld.put("last_ref_visit", DateTimeUtils.formatCurrentDateToStringWithTime());
            joFIeld.put("user_id", sessionManager.getcurrentu_nm());
            joNewData.addProperty("field_json_data", joFIeld.toString()); // last + current
        } catch (JSONException e) {
            e.printStackTrace();
        }


        //joNewData.addProperty("referrel_value", getPromoPt); // last + current
        joNewData.addProperty("referrel_value", etEnterPromoPtForReferrer.getText().toString().trim()); // last + current


        JsonArray jsonElements = new JsonArray();
        jsonElements.add(joNewData);

        jsonObject = new JsonObject();
        jsonObject.add("newdata", jsonElements);
        insertConatcts(jsonObject, myRequst);


    }

    public void updateConatcts(JsonObject jsonObject, MyRequst myRequst) {
        presenter.insertExternalContactsByContactNo(jsonObject, myRequst);
    }

    @Override
    public void onGetInsertedContact(Response response) {

        try {
            String data = new Gson().toJson(response.body());
            JSONObject jObjRes = new JSONObject(data);

            JSONObject jObjdata = jObjRes.getJSONObject("response");
            String success = jObjdata.getString("success");

            if (success.equalsIgnoreCase("ok")) {
                if (currentstatus.equalsIgnoreCase("enterConsumer")) {
                    if (isReferrer) {
                        isReferrerManually = false;
                        isReferrer = false;
                        updateReferrer();
                    } else if (isReferrerManually) {
                        isReferrerManually = false;
                        insertReferrerManually();

                    } else {
                        Inad.alerterGrnInfo("Saved", "Save consumer successfully", (Activity) context);
                        tvCancel.performClick();
                    }

                } else if (currentstatus.equalsIgnoreCase("updateConsumer")) {
                    Inad.alerterGrnInfo("Updated", "Edit consumer successfully", (Activity) context);
                    tvCancel.performClick();
                }


            }

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public void setViewToggle(View v, boolean isEnable) {
        v.setEnabled(isEnable);
        if (isEnable) v.setAlpha(1f);
        else v.setAlpha(0.6f);
    }

    PlayAnim playAnim = new PlayAnim();

    public void playAnim(LinearLayout linearLayout) { // slide up
        playAnim.slideDownRl(linearLayout);
    }

    @OnClick(R.id.ivEditNm)
    public void ivEditNm(View v) {
        tvGetConsNm.requestFocus();
        tvGetConsNm.setSelection(tvGetConsNm.getText().toString().length());
    }

    @BindView(R.id.llVisitCtr)
    LinearLayout llVisitCtr;

    @BindView(R.id.llVisitPtCtr)
    LinearLayout llVisitPtCtr;

    @BindView(R.id.llRefVisitCtr)
    LinearLayout llRefVisitCtr;

    @BindView(R.id.llRefVisitPtCtr)
    LinearLayout llRefVisitPtCtr;

    @BindView(R.id.tvGetTotalRefVisit)
    TextView tvGetTotalRefVisit;

    @BindView(R.id.tvGetTotalRefPt)
    TextView tvGetTotalRefPt;

    public void checkVisitCtrShow() {

        showTv(llVisitCtr);
        showTv(llVisitPtCtr);
        showTv(llRefVisitCtr);


        hideTv(llRefVisitPtCtr);

        //if (grandVisit == 0) hideTv(llVisitCtr);
        if (getTotalVisit == 0) hideTv(llVisitCtr);
        if (grandPoint == 0) hideTv(llVisitPtCtr);
        //if(getRefPromoPt==0)hideTv(llRefVisitPtCtr);
        if (getRefVisit == 0) hideTv(llRefVisitCtr);
    }

    public void hideTv(LinearLayout textView) {
        textView.setVisibility(View.GONE);
    }

    public void showTv(LinearLayout textView) {
        textView.setVisibility(View.VISIBLE);
    }

    @BindView(R.id.spn_ref_channel)
    Spinner spn_ref_channel;

    @BindView(R.id.llIfRefChannelOnly)
    LinearLayout llIfRefChannelOnly;

    @BindView(R.id.llRefEnterBox)
    LinearLayout llRefEnterBox;

    @BindView(R.id.tilEnterPromoPtReferrer)
    TextInputLayout tilEnterPromoPtReferrer;

    @BindView(R.id.tilRefNm)
    TextInputLayout tilRefNm;

    ArrayList<AutoCompleteTv> alRefCahnnel = new ArrayList<>();
    AutoCompleteTv autoCompleteTv = new AutoCompleteTv();
    int channelId = 0;

    public void setRefChannel() {

        channelId = 0;
        isReferrer = false; // if avail
        isReferrerManually = false; // for if not available

        /*if (externalContactViewModel != null) {
            String referrer_channel_id = JsonObjParse.getValueEmpty(externalContactViewModel.getFieldJsonData(), "referrer_channel_id");
            if (!referrer_channel_id.isEmpty()) channelId = Integer.parseInt(referrer_channel_id);
        }*/
        alRefCahnnel = new ArrayList<>();
        alRefCahnnel.add(new AutoCompleteTv("None", "None", 0, ""));
        alRefCahnnel.add(new AutoCompleteTv("Referrer", "Referrer", 1, "")); //
        alRefCahnnel.add(new AutoCompleteTv("Youtube", "Youtube", 2, getString(R.string.utube_no))); //
        alRefCahnnel.add(new AutoCompleteTv("Whatsapp", "Whatsapp", 3, getString(R.string.wtsap_no))); //
        alRefCahnnel.add(new AutoCompleteTv("Twitter", "Twitter", 4, getString(R.string.twtr_no))); //
        alRefCahnnel.add(new AutoCompleteTv("Facebook", "Facebook", 5, getString(R.string.fb_no))); //
        alRefCahnnel.add(new AutoCompleteTv("Events", "Events", 6, getString(R.string.events))); //
        alRefCahnnel.add(new AutoCompleteTv("Campaign", "Campaign", 7, getString(R.string.compn))); //
        alRefCahnnel.add(new AutoCompleteTv("Others", "Others", 8, getString(R.string.others))); //

        autoCompleteTv.setautofill(context, spn_ref_channel, alRefCahnnel);
        spn_ref_channel.setSelection(channelId);

        spn_ref_channel.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                isReferrer = false;
                isReferrerManually = false;
                setViewToggle(ivSearchReferrer, true);
                setViewToggle(etReferrePhone, true);


                autoCompleteTv = alRefCahnnel.get(position);

                etReferrePhone.setText("");
                etReferrerNm.setText("");

                if (position > 1) {
                    etReferrePhone.setText(autoCompleteTv.ref_no);
                    if (position != 8)
                        etReferrerNm.setText(autoCompleteTv.refNm); // for other its empty
                    pb.setVisibility(View.VISIBLE);
                }

                tilEnterPromoPtReferrer.setVisibility(View.GONE);
                llRefEnterBox.setVisibility(View.VISIBLE);
                tilRefNm.setVisibility(View.VISIBLE);
                tilRefNm.setHint("Enter referrer name");

                switch (position) {

                    case 1:
                        focusEt(etReferrePhone);
                        tilRefNm.setVisibility(View.VISIBLE);
                        llIfRefChannelOnly.setVisibility(View.VISIBLE);
                        break;

                    case 8:
                        llRefEnterBox.setVisibility(View.GONE);
                        tilRefNm.setVisibility(View.VISIBLE);
                        tilRefNm.setHint("Enter reason");
                        llIfRefChannelOnly.setVisibility(View.VISIBLE);
                        tilEnterPromoPtReferrer.setVisibility(View.GONE);
                        break;

                    default:
                        llIfRefChannelOnly.setVisibility(View.GONE);
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }


    public boolean checkIncrementVisitCondition(String walk_in_details) {

        //String walk_in_details = getValueFromFieldJson("walk_in_details");

        String is_walkin_count_condition = JsonObjParse.getValueEmpty(walk_in_details, "is_walkin_count_condition");
        if (is_walkin_count_condition.equalsIgnoreCase("yes")) {
            String timestamp_day = JsonObjParse.getValueEmpty(walk_in_details, "timestamp_day");
            if (!timestamp_day.isEmpty()) {
                int d = Integer.parseInt(timestamp_day);
                d = d - 1;
                if (d > 0) {
                    long dayInMs = convertDaysToMiliSec(d);
                    if (externalContactViewModel != null) {
                        String last_visit = JsonObjParse.getValueEmpty(externalContactViewModel.getFieldJsonData(), "last_visit");
                        if (!last_visit.isEmpty()) {
                            try {
                                Date lastVisitDate = DateTimeUtils.formatStringToDate(last_visit);
                                Date cDate = new Date();
                                long diffInMs = cDate.getTime() - lastVisitDate.getTime();

                                long days = (diffInMs / (60 * 60 * 24 * 1000));
                                if (days >= d) {
                                    return true;
                                } else {
                                    return false;
                                }
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        } else {
                            return true;
                        }
                    }

                } else {
                    return true;
                }
            }

        } else {
            return true;
        }


        return false;
    }

    public long convertDaysToMiliSec(int days) {
        return 60 * 60 * 24 * 1000 * days;
    }

    public void focusEt(EditText editText) {
        editText.requestFocus();

        Utils.showSoftKeyboard(context);

    }

    @OnClick(R.id.ivInfoEnterUser)
    public void ivInfoEnterUser(View v) {
        Inad.showAnimDialogue(this, "Information", "Facebook : 9999999999" + "\n"
                + "Whatsapp : 8888888888" + "\n" + "Youtube : 7777777777" + "\n" + "Twitter : 6666666666" +
                "\n" + "Events : 5555555555" + "\n" + "Campaign : 4444444444" +
                "\n" + "Advertisement : 3333333333" + "\n" + "Others : 2222222222");

    }


    public void autoAddRef() {

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {


                if (sessionManager.getIsEmployess()) {
                    getBusinessProfileForEmployee(new MyCallForFieldJson() {
                        @Override
                        public void call(String field_json) {
                            String walk_in_details = JsonObjParse.getValueEmpty(field_json, "walk_in_details");

                            String default_promo_ref_value = JsonObjParse.getValueEmpty(walk_in_details, "default_promo_ref_value");
                            if (default_promo_ref_value.isEmpty()) default_promo_ref_value = "0";
                            etEnterPromoPtForReferrer.setText(default_promo_ref_value);
                        }
                    });

                } else {
                    String walk_in_details = getValueFromFieldJson("walk_in_details");

                    String default_promo_ref_value = JsonObjParse.getValueEmpty(walk_in_details, "default_promo_ref_value");
                    if (default_promo_ref_value.isEmpty()) default_promo_ref_value = "0";
                    etEnterPromoPtForReferrer.setText(default_promo_ref_value);
                }

            }
        }, 700);


    }
}
