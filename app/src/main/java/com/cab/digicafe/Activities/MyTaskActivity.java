package com.cab.digicafe.Activities;


import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.andremion.counterfab.CounterFab;
import com.cab.digicafe.Adapter.EmployessListAdapter;
import com.cab.digicafe.Adapter.MyTaskAdapter;
import com.cab.digicafe.BaseActivity;
import com.cab.digicafe.ChitlogActivity;
import com.cab.digicafe.DetailsFragment;
import com.cab.digicafe.Dialogbox.CustomDialogClass;
import com.cab.digicafe.Dialogbox.EmployeeFilterDialog;
import com.cab.digicafe.Dialogbox.EmployeeListDialog;
import com.cab.digicafe.Dialogbox.FilterPurposeDialog;
import com.cab.digicafe.Fragments.SummaryDetailFragmnet;
import com.cab.digicafe.Helper.Constants;
import com.cab.digicafe.Helper.Inad;
import com.cab.digicafe.Helper.RecyclerItemTouchHelper3;
import com.cab.digicafe.Helper.SessionManager;
import com.cab.digicafe.Model.Chit;
import com.cab.digicafe.Model.Employee;
import com.cab.digicafe.Model.ShareModel;
import com.cab.digicafe.Model.Userprofile;
import com.cab.digicafe.MyCustomClass.CanelOrder_Refund;
import com.cab.digicafe.MyCustomClass.JsonObjParse;
import com.cab.digicafe.MyCustomClass.RefreshSession;
import com.cab.digicafe.R;
import com.cab.digicafe.Rest.ApiClient;
import com.cab.digicafe.Rest.ApiInterface;
import com.cab.digicafe.serviceTypeClass.GetServiceTypeFromBusiUserProfile;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import info.hoang8f.android.segmented.SegmentedGroup;
import okhttp3.Headers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyTaskActivity extends BaseActivity implements SearchView.OnQueryTextListener, MyTaskAdapter.OnItemClickListener, SwipyRefreshLayout.OnRefreshListener, RecyclerItemTouchHelper3.RecyclerItemTouchHelperListener {

    private RecyclerView recyclerView;
    private MyTaskAdapter mAdapter;
    CounterFab counterFab;
    private List<Chit> orderlist = new ArrayList<>();
    SessionManager sessionManager;
    String sessionString;
    MenuItem item;
    Menu menu;
    Employee eItem;
    int totalcount = 0;
    String str_actionid = "";
    RadioButton rb_newtask, rb_inprogtask, rb_close;
    SegmentedGroup s_group;
    String tab = "";
    boolean isbusiness = false;
    LinearLayout llEmployeeFilter;
    RecyclerView rvEmployeeName;
    ProgressBar pb;
    ArrayList<Employee> al_aggre = new ArrayList<>();
    private EmployessListAdapter eAdapter;
    Employee assignItem;

    @Override
    public void onTouchSwipe(boolean isSwipeEnable) {
        mSwipyRefreshLayout.setEnabled(isSwipeEnable);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.content_mytask, frameLayout);
        Constants.chit_id_to = 2;

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        sessionManager = new SessionManager(this);
        if (sessionManager.getcurrentu_nm().contains("@")) // Employee
        {

            getSupportActionBar().setTitle("My Task");
        } else {

            //getSupportActionBar().setTitle("Receipts");
        }
        getSupportActionBar().setTitle("My Task");
        isbusiness = sessionManager.getisBusinesslogic();
        refreshlist();

        llEmployeeFilter = (LinearLayout) findViewById(R.id.llEmployeeFilter);
        rvEmployeeName = (RecyclerView) findViewById(R.id.rvEmployeeName);
        pb = (ProgressBar) findViewById(R.id.pb);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        s_group = (SegmentedGroup) findViewById(R.id.segmented2);
        sessionManager = new SessionManager(this);
        // listen refresh event
        counterFab = (CounterFab) findViewById(R.id.counter_fab);
        counterFab.setVisibility(View.GONE);

        rb_newtask = (RadioButton) findViewById(R.id.openstatus);
        rb_close = (RadioButton) findViewById(R.id.closestatus);

        rb_inprogtask = (RadioButton) findViewById(R.id.inprogressstatus);

        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                // bridgeidval = null;
            } else {
                sessionString = extras.getString("session");
                //  bridgeidval= extras.getString("bridgeidval");
            }
        } else {
            sessionString = (String) savedInstanceState.getSerializable("session");
            // bridgeidval= (String) savedInstanceState.getSerializable("bridgeidval");
        }

        s_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                findViewById(R.id.rl_pb_ctask).setVisibility(View.VISIBLE);

                switch (checkedId) {

                    case R.id.openstatus:
                        tab = "new";
                        break;
                    case R.id.inprogressstatus:
                        tab = "inprogress";

                        break;
                    case R.id.closestatus:
                        tab = "close";
                        break;
                    default:

                        break;
                }

                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(MyTaskActivity.this);
                recyclerView.setLayoutManager(mLayoutManager);
                //recyclerView.setItemAnimator(new DefaultItemAnimator());

                mAdapter = new MyTaskAdapter(orderlist, MyTaskActivity.this, MyTaskActivity.this, tab, isbusiness);
                recyclerView.setAdapter(mAdapter);
                new ItemTouchHelper(new RecyclerItemTouchHelper3(0, 0, MyTaskActivity.this)).attachToRecyclerView(recyclerView);


                recyclerView.setNestedScrollingEnabled(false);
                isdataavailable = true;
                pagecount = 1;

                usersignin(sessionString, 1, false, false);

              /*  if (totalcount > 0) {
                    try {
                        for (int i = 0; i < orderlist.size(); i++) {
                            if (orderlist.get(i).isIslongpress()) {
                                orderlist.get(i).setIslongpress(false);
                            }
                        }

                        totalcount = 0;
                        str_actionid = "";

                        MenuItem item1 = menu.findItem(R.id.action_delete);
                        item1.setVisible(false);
                        mAdapter.notifyDataSetChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
*/
            }
        });

        rb_newtask.setChecked(true);

        rb_newtask.setText("Open");

        rb_inprogtask.setText("Act");

        rb_close.setText("Close");

        eAdapter = new EmployessListAdapter(al_aggre, MyTaskActivity.this, new EmployessListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Employee ite) {

                eItem = ite;
                //dismiss();
                //listener.onDialogImageRunClick(0, item);
            }
        });

        RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(this);
        rvEmployeeName.setLayoutManager(mLayoutManager1);
        rvEmployeeName.setItemAnimator(new DefaultItemAnimator());
        rvEmployeeName.setAdapter(eAdapter);
        rvEmployeeName.setNestedScrollingEnabled(false);

        getEmployee();
    }

    String smsFlagForOpen = "";

    @Override
    public void onItemClick(Chit item) {

        Intent i = new Intent(MyTaskActivity.this, ChitlogActivity.class);
        i.putExtra("chit_hash_id", item.getChit_hash_id());
        i.putExtra("chit_id", item.getChit_id() + "");
        i.putExtra("transtactionid", item.getChit_name() + "");
        i.putExtra("productqty", item.getTotal_chit_item_value() + "");
        i.putExtra("productprice", item.getChit_item_count() + "");
        i.putExtra("purpose", item.getPurpose());
        i.putExtra("Refid", item.getRef_id());
        i.putExtra("Caseid", item.getCase_id());
        i.putExtra("isprint", true);
        i.putExtra("iscomment", true);

        if (tab.equalsIgnoreCase("new")) {  // open task

            i.putExtra("isdiaries", true);  // if open & business then diary for comment

        }

        ShareModel.getI().isFromAllTask = true;
        i.putExtra("isMrp", true);

        DetailsFragment.chititemcount = "" + item.getChit_item_count();
        DetailsFragment.chititemprice = "" + item.getTotal_chit_item_value();
        SummaryDetailFragmnet.item = item;
        DetailsFragment.item_detail = item;
        startActivity(i);
    }

    @Override
    public void onItemLongClick(Chit item, int pos) {

        if (orderlist.get(pos).isIslongpress()) {
            orderlist.get(pos).setIslongpress(false);
        } else {
            orderlist.get(pos).setIslongpress(true);
        }

        boolean isanyitem = false;
        str_actionid = "";
        totalcount = 0;
        for (int i = 0; i < orderlist.size(); i++) {
            if (orderlist.get(i).isIslongpress()) {
                totalcount++;
                isanyitem = true;
                str_actionid = str_actionid + String.valueOf(orderlist.get(i).getChit_id()) + ",";
            }
        }

        mAdapter.notifyDataSetChanged();


        Log.e("totalcount", String.valueOf(totalcount));
        MenuItem item1 = menu.findItem(R.id.action_delete);
        if (totalcount > 0) {
            item1.setVisible(true);
            showsearch(false);
        } else {
            item1.setVisible(false);
            showsearch(true);
        }


        /****************  Multiple Refund  *********************/

        try {
            JSONObject jo_summary = new JSONObject(item.getFooter_note());

            if (jo_summary.getString("PAYMENT_MODE").equals("RAZORPAY")) {
                boolean isadded = false;
                for (int i = 0; i < al_refund.size(); i++) {
                    String payuResponse = al_refund.get(i);
                    if (jo_summary.getString("payuResponse").equals(payuResponse)) {
                        isadded = true;
                        if (isadded) {
                            al_refund.remove(i);
                            break;
                        }
                    }
                }
                if (!isadded) {
                    al_refund.add(jo_summary.getString("payuResponse"));
                }


            } else {

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onRefresh(SwipyRefreshLayoutDirection direction) {
        //mSwipyRefreshLayout.setRefreshing(false);

        Log.e("scroll direction", "Refresh triggered at "
                + (direction == SwipyRefreshLayoutDirection.TOP ? "top" : "bottom"));
        if (direction == SwipyRefreshLayoutDirection.TOP) {
            isdataavailable = true;
            pagecount = 1;
            usersignin(sessionString, pagecount, false, false);
        } else if (direction == SwipyRefreshLayoutDirection.BOTTOM) {
            if (isdataavailable) {
                pagecount++;
                usersignin(sessionString, pagecount, false, false);
            } else {
                mSwipyRefreshLayout.setRefreshing(false);
                Toast.makeText(this, "No more data available", Toast.LENGTH_LONG).show();
            }

        }
    }

    SwipyRefreshLayout mSwipyRefreshLayout;
    int pagecount = 1;
    boolean isdataavailable = true;
    int pagesize = 50;

    public void refreshlist() {
        mSwipyRefreshLayout = (SwipyRefreshLayout) findViewById(R.id.swipyrefreshlayout);
        mSwipyRefreshLayout.setOnRefreshListener(this);


    }

    String filter_;

    public void usersignin(String usersession, final int pagecount, final boolean isremoved, final boolean isAssign) {


        isSmsForWhat = "";
        smsFlagForOpen = "";

        ApiInterface apiService =
                ApiClient.getClient(MyTaskActivity.this).create(ApiInterface.class);


        if (tab.equalsIgnoreCase("new")) {

            filter_ = "3,8,99,";
        } else if (tab.equalsIgnoreCase("inprogress")) {
            filter_ = "4,";

        } else if (tab.equalsIgnoreCase("close")) {
            filter_ = "5,";   // filter_ = "5,6,7,9";
        }

        Call call = apiService.getMyTask(usersession, pagecount, filter_, purpose);

        if (isbusiness) {
            call = apiService.getMyTaskunassign(usersession, pagecount, filter_, purpose);
        }

        if (isAssign) {
            call = apiService.getFilterByAssign(usersession, pagecount, filter_, purpose, assignItem.getEmployee_bridge_id());
        }


        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                findViewById(R.id.rl_pb_ctask).setVisibility(View.GONE);
                mSwipyRefreshLayout.setRefreshing(false);


                if (response.isSuccessful()) {
                    try {
                        String a = new Gson().toJson(response.body());

                        JSONObject jObjRes = new JSONObject(a);
                        JSONObject jObjdata = jObjRes.getJSONObject("response");


                        JSONObject jObjResponse = jObjdata.getJSONObject("data");
                        String totalRecord = jObjResponse.getString("totalRecord");
                        JSONArray jObjcontacts = jObjResponse.getJSONArray("content");

                        String displayEndRecord = jObjResponse.getString("displayEndRecord");
                        if (totalRecord.equals(displayEndRecord)) {
                            isdataavailable = false;
                        } else {
                            isdataavailable = true;
                        }
                        /*if (jObjcontacts.length() < pagesize) {
                            isdataavailable = false;
                        } else {
                            isdataavailable = true;
                        }*/


                        Log.e("responsehistory", jObjcontacts.toString());

                        List<Chit> temp_orderlist = new ArrayList<>();
                        for (int i = 0; i < jObjcontacts.length(); i++) {
                            if (jObjcontacts.get(i) instanceof JSONObject) {
                                JSONObject jsnObj = (JSONObject) jObjcontacts.get(i);
                                Chit obj = new Gson().fromJson(jsnObj.toString(), Chit.class);
                                Log.e("alias", obj.getChit_name());
                                temp_orderlist.add(obj);
                            }
                        }

                        if (pagecount == 1) {
                            orderlist.clear();
                        }
                        orderlist.addAll(temp_orderlist);
                        mAdapter.notifyDataSetChanged();


                        if (orderlist.size() == 0) {
                            recyclerView.setVisibility(View.GONE);
                            findViewById(R.id.tv_nodata).setVisibility(View.VISIBLE);
                        } else {
                            recyclerView.setVisibility(View.VISIBLE);
                            findViewById(R.id.tv_nodata).setVisibility(View.GONE);
                        }


                        setctr(orderlist, totalRecord);

                        if (al_refund != null && al_refund.size() > 0) {
                            al_refund.clear();
                        }

                    } catch (Exception e) {
                        Log.e("errortest", e.getMessage());

                        Toast.makeText(MyTaskActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                } else {
                    Log.e("test", " trdds1");

                    switch (response.code()) {
                        case 401:
                            new RefreshSession(MyTaskActivity.this, sessionManager);  // session expired
                            break;
                        case 404:
                            Toast.makeText(MyTaskActivity.this, "not found", Toast.LENGTH_SHORT).show();
                            break;
                        case 500:
                            Toast.makeText(MyTaskActivity.this, "server broken", Toast.LENGTH_SHORT).show();
                            break;
                        default:
                            Toast.makeText(MyTaskActivity.this, "unknown error", Toast.LENGTH_SHORT).show();
                            break;
                    }


                    try {


                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Toast.makeText(MyTaskActivity.this, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(MyTaskActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }

                totalcount = 0;
                str_actionid = "";
                MenuItem item1 = null;
                try {
                    item1 = menu.findItem(R.id.action_delete);
                    item1.setVisible(false);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                showsearch(true);
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // Log error here since request failed
                mSwipyRefreshLayout.setRefreshing(false);
                findViewById(R.id.rl_pb_ctask).setVisibility(View.GONE);
            }
        });
    }

    String isSmsForWhat = "";

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {


        if (viewHolder instanceof MyTaskAdapter.MyViewHolder) {


            final Chit deletedItem = orderlist.get(viewHolder.getAdapterPosition());
            final int deletedIndex = viewHolder.getAdapterPosition();
            mAdapter.removeItem(viewHolder.getAdapterPosition());

            String actionid = String.valueOf(deletedItem.getChit_id());
            actionid = actionid + ",";

            String actionCode = "";

            String msg = "";

            isSmsForWhat = "";

            if (tab.equalsIgnoreCase("new")) {

                if (direction == ItemTouchHelper.LEFT) {

                    // new ass
                    actionCode = "";
                    msg = "ALL TASK";

                    if (sessionManager.getisBusinesslogic()) {// no need to sent sms own

                    } else {
                        isSmsForWhat = "sms_employee"; // for employee login
                    }


                } else if (direction == ItemTouchHelper.RIGHT) {

                    actionCode = "4";
                    // for both bus & emp
                    isSmsForWhat = "sms_customer";

                }

            } else if (tab.equalsIgnoreCase("inprogress")) {
                if (direction == ItemTouchHelper.LEFT) {

                    // new ass
                    actionCode = "2";
                } else if (direction == ItemTouchHelper.RIGHT) {

                    actionCode = "5";
                    isSmsForWhat = "sms_customer"; // for cancel task

                }

            } else if (tab.equalsIgnoreCase("close")) {

                if (direction == ItemTouchHelper.LEFT) {

                    // new ass
                    actionCode = "4";
                } else if (direction == ItemTouchHelper.RIGHT) {
                    // msg = "Archiving";
                    // actionCode = "";

                }
            }

            smsFlagForOpen = "";
            if (!isSmsForWhat.isEmpty()) {

                if (sessionManager.getisBusinesslogic()) { // only right swipe no need left

                    String userprofile = sessionManager.getUserprofile();
                    try {
                        Userprofile userProfileData = new Gson().fromJson(userprofile, Userprofile.class);
                        smsFlagForOpen = JsonObjParse.getValueEmpty(userProfileData.field_json_data, isSmsForWhat);
                        // left - employee, right - customer
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                    loadData(actionCode, actionid, msg);


                } else { // for employee login - right and left -> right - snet sms to consumer, left sent sms to employee

                    Log.e("getcurrentu_nm", sessionManager.getcurrentu_nm());

                    // to get current user profile for employee
                    String empUseName = sessionManager.getcurrentu_nm();
                    int indexOfannot = empUseName.indexOf("@") + 1;
                    empUseName = empUseName.substring(indexOfannot, empUseName.indexOf("."));

                    final String finalActionCode = actionCode;
                    final String finalActionid = actionid;
                    final String finalMsg = msg;

                    new GetServiceTypeFromBusiUserProfile(MyTaskActivity.this, empUseName, new GetServiceTypeFromBusiUserProfile.Apicallback() {
                        @Override
                        public void onGetResponse(String a, String response, String sms_emp) {

                            try {
                                JSONObject Jsonresponse = new JSONObject(response);
                                JSONObject Jsonresponseinfo = Jsonresponse.getJSONObject("response");
                                JSONArray jaData = Jsonresponseinfo.getJSONArray("data");
                                if (jaData.length() > 0) {
                                    JSONObject data = jaData.getJSONObject(0);
                                    String field_json_data = JsonObjParse.getValueNull(data.toString(), "field_json_data");
                                    smsFlagForOpen = JsonObjParse.getValueNull(field_json_data, isSmsForWhat);

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            loadData(finalActionCode, finalActionid, finalMsg);

                        }
                    });


                }

            } else {
                loadData(actionCode, actionid, msg);
            }

        }
    }

    public void loadData(String actionCode, String actionid, String msg) {
        findViewById(R.id.rl_pb_ctask).setVisibility(View.VISIBLE);

        if (actionCode.equalsIgnoreCase("")) {

            assignarchived(sessionString, actionid, "", msg);
        } else {
            removearchived(sessionString, actionid, true, actionCode);
        }

    }


    public void removearchived(final String usersession, String chitIds, final boolean isswipe, String actionCode) {

        ApiInterface apiService =
                ApiClient.getClient(MyTaskActivity.this).create(ApiInterface.class);
        JsonObject a1 = new JsonObject();
        try {

            a1.addProperty("chitIds", chitIds);
            a1.addProperty("actionCode", actionCode);

            if (!isSmsForWhat.isEmpty() && smsFlagForOpen.equalsIgnoreCase("yes")) {
                isSmsForWhat = "";
                a1.addProperty("sms", "true");
            }


        } catch (JsonParseException e) {
            e.printStackTrace();
        }
        // Toast.makeText(MyTaskActivity.this, actionCode, Toast.LENGTH_LONG).show();

        //Call call = apiService.removechit(usersession,chitIds+",","10");
        Call call = apiService.removechit(usersession, a1);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                int statusCode = response.code();
                Log.e("test", sessionString + "sessionstring");

                Headers headerList = response.headers();
                Log.d("testhistory", statusCode + " ");

                if (response.isSuccessful()) {
                    try {

                        if (isremoveclick) {
                            isremoveclick = false;
                            refundafterwithdrawn();
                        }
                        Log.e("response.body()", response.body().toString());
                        String a = new Gson().toJson(response.body());

                        JSONObject jo_res = new JSONObject(a);
                        String response_ = jo_res.getString("response");
                        JSONObject jo_txt = new JSONObject(response_);
                        String actionTxt = jo_txt.getString("actionTxt");


                        Toast.makeText(MyTaskActivity.this, actionTxt, Toast.LENGTH_LONG).show();


                    } catch (Exception e) {
                        Log.e("errortest", e.getMessage());

                    }

                } else {
                    Log.e("test", " trdds1");

                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Toast.makeText(MyTaskActivity.this, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(MyTaskActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }

                totalcount = 0;
                str_actionid = "";
                MenuItem item1 = menu.findItem(R.id.action_delete);
                item1.setVisible(false);
                showsearch(true);
                pagecount = 1;
                isdataavailable = true;
                usersignin(usersession, pagecount, true, false);

            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // Log error here since request failed
                Log.e("error message", t.toString());
                findViewById(R.id.rl_pb_ctask).setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (totalcount > 0) {
            try {
                for (int i = 0; i < orderlist.size(); i++) {
                    if (orderlist.get(i).isIslongpress()) {
                        orderlist.get(i).setIslongpress(false);
                    }
                }

                totalcount = 0;
                str_actionid = "";

                MenuItem item1 = menu.findItem(R.id.action_delete);
                item1.setVisible(false);
                showsearch(true);
                mAdapter.notifyDataSetChanged();


                if (al_refund != null && al_refund.size() > 0) {
                    al_refund.clear();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
           /* AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.AlertDialogCustom));
            builder
                    .setMessage(getString(R.string.quit))
                    .setTitle("")
                    .setCancelable(false)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            finish();
                            Intent intent = new Intent(Intent.ACTION_MAIN);
                            intent.addCategory(Intent.CATEGORY_HOME);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        }
                    })
                    .setNegativeButton("No", null)

                    .show();*/

            Inad.exitalert(this);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);


        final MenuItem item = menu.findItem(R.id.action_search);

        MenuItem item1 = menu.findItem(R.id.action_filter);
        item1.setVisible(true);

        MenuItem item2 = menu.findItem(R.id.action_sort);
        item2.setVisible(true);

        /*MenuItem item1 = menu.findItem(R.id.action_filter);
        item1.setVisible(false);*/

        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setInputType(InputType.TYPE_CLASS_NUMBER);
        searchView.setOnQueryTextListener(this);

        MenuItemCompat.setOnActionExpandListener(item,
                new MenuItemCompat.OnActionExpandListener() {
                    @Override
                    public boolean onMenuItemActionCollapse(MenuItem item) {
// Do something when collapsed
                        mAdapter.setFilter(orderlist);
                        return true; // Return true to collapse action view
                    }

                    @Override
                    public boolean onMenuItemActionExpand(MenuItem item) {
// Do something when expanded
                        return true; // Return true to expand action view
                    }
                });

        return true;
    }

    String msg_alert = "";
    String actionCode_multi = "";

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.


        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        if (id == R.id.action_sort) {
            new EmployeeFilterDialog(MyTaskActivity.this, new EmployeeFilterDialog.OnDialogClickListener() {
                @Override
                public void onDialogImageRunClick(int pos, final Employee add) {

                    if (pos == 1 && add != null) {
                        assignItem = add;
                        usersignin(sessionString, 1, false, true);
                        //Toast.makeText(MyTaskActivity.this, add + "", Toast.LENGTH_SHORT).show();
                      /*  if (add.getUsername().equalsIgnoreCase(sessionManager.getcurrentu_nm())) // its for employee login only
                        {
                            Inad.alerter("Alert !", "Can't assign yourself.", MyTaskActivity.this);
                        } else {

                            String empUseName = add.getUsername();
                            int indexOfannot = empUseName.indexOf("@") + 1;
                            empUseName = empUseName.substring(indexOfannot, empUseName.indexOf("."));

                            new GetServiceTypeFromBusiUserProfile(MyTaskActivity.this, empUseName, new GetServiceTypeFromBusiUserProfile.Apicallback() {
                                @Override
                                public void onGetResponse(String serviecType, String response, String sms_emp) {

                                    isSmsForWhat = "sms_employee";

                                    findViewById(R.id.rl_pb_ctask).setVisibility(View.VISIBLE);

                                    if (sessionManager.getisBusinesslogic()) { // for business user(employer)

                                        String userprofile = sessionManager.getUserprofile();
                                        try {
                                            Userprofile userProfileData = new Gson().fromJson(userprofile, Userprofile.class);
                                            smsFlagForOpen = JsonObjParse.getValueEmpty(userProfileData.field_json_data, isSmsForWhat);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    } else { // for employee
                                        smsFlagForOpen = sms_emp;
                                    }

                                    assignarchived(sessionString, str_actionid, add.getEmployee_bridge_id(), "ASSIGN");

                                }
                            });
                        }*/
                    }
                }
            }).show();
        }

        if (id == R.id.action_employee) {
            new EmployeeListDialog(MyTaskActivity.this, new EmployeeListDialog.OnDialogClickListener() {
                @Override
                public void onDialogImageRunClick(int pos, final Employee add) {

                    if (pos == 1 && add != null) {

                        // for business - sms from getUserprofile
                        // for employee - sms frrom businessprofile - - from user name substring

                        // this works for both login bcz of in business login can not match with employee (@.br)login
                        if (add.getUsername().equalsIgnoreCase(sessionManager.getcurrentu_nm())) // its for employee login only
                        {
                            Inad.alerter("Alert !", "Can't assign yourself.", MyTaskActivity.this);
                        } else {
                            String empUseName = add.getUsername();
                            int indexOfannot = empUseName.indexOf("@") + 1;
                            empUseName = empUseName.substring(indexOfannot, empUseName.indexOf("."));

                            new GetServiceTypeFromBusiUserProfile(MyTaskActivity.this, empUseName, new GetServiceTypeFromBusiUserProfile.Apicallback() {
                                @Override
                                public void onGetResponse(String serviecType, String response, String sms_emp) {

                                    isSmsForWhat = "sms_employee";

                                    findViewById(R.id.rl_pb_ctask).setVisibility(View.VISIBLE);

                                    if (sessionManager.getisBusinesslogic()) { // for business user(employer)

                                        String userprofile = sessionManager.getUserprofile();
                                        try {
                                            Userprofile userProfileData = new Gson().fromJson(userprofile, Userprofile.class);
                                            smsFlagForOpen = JsonObjParse.getValueEmpty(userProfileData.field_json_data, isSmsForWhat);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    } else { // for employee
                                        smsFlagForOpen = sms_emp;
                                    }

                                    assignarchived(sessionString, str_actionid, add.getEmployee_bridge_id(), "ASSIGN");

                                }
                            });
                        }
                    }
                }
            }).show();
        }

        if (id == R.id.action_filter) {
            new FilterPurposeDialog(MyTaskActivity.this, new FilterPurposeDialog.OnDialogClickListener() {

                @Override
                public void onDialogImageRunClick(int pos, String add) {

                    if (!purpose.equals(add) && pos == 1) {
                        findViewById(R.id.rl_pb_ctask).setVisibility(View.VISIBLE);
                        purpose = add;
                        isdataavailable = true;
                        pagecount = 1;

                       /* switch (add) {
                            case "all":
                                out_of_stock = "";
                                break;
                            case "avail":
                                out_of_stock = "No";
                                break;
                            case "notavail":
                                out_of_stock = "Yes";
                                break;
                            default:
                                out_of_stock = "";
                                break;
                        }*/

                        //usersignin(pagecount);

                        usersignin(sessionString, 1, false, false);
                        //mAdapter.setitem(orderlist,filter);
                    }


                }
            }, purpose, true).show();
        }


        if (id == R.id.action_delete) {

            findViewById(R.id.rl_pb_ctask).setVisibility(View.VISIBLE);
            if (tab.equalsIgnoreCase("new")) {

                msg_alert = "In Progress";
                actionCode_multi = "4";


            } else if (tab.equalsIgnoreCase("inprogress")) {
                msg_alert = "Closing";
                actionCode_multi = "5";

            } else if (tab.equalsIgnoreCase("close")) {
                msg_alert = "In Progress";
                actionCode_multi = "4";
            }

            actionCode_multi = "7";


            new CustomDialogClass(MyTaskActivity.this, new CustomDialogClass.OnDialogClickListener() {
                @Override
                public void onDialogImageRunClick(int positon) {
                    if (positon == 0) {
                        findViewById(R.id.rl_pb_ctask).setVisibility(View.GONE);
                    } else if (positon == 1) {
                        //str_actionid = str_actionid.replaceAll(",$", "");
                        Log.e("str_actionid", str_actionid);

                        isremoveclick = true;

                        isSmsForWhat = "cancellation";
                        smsFlagForOpen = "yes";


                        removearchived(sessionString, str_actionid, false, actionCode_multi);
                    }
                }
            }, "CANCEL ?").show();
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    private List<Chit> filter(List<Chit> models, String query) {
        query = query.toLowerCase();
        final List<Chit> filteredModelList = new ArrayList<>();
        for (Chit model : models) {
            final String text = model.getSubject().toLowerCase();
            if (text.contains(query)) {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;
    }

    List<Chit> filteredModelList = new ArrayList<>();

    @Override
    public boolean onQueryTextChange(final String newText) {

       /* final List<Chit> filteredModelList = filter(orderlist, newText);
        mAdapter.setFilter(filteredModelList);*/

        if (newText.equalsIgnoreCase("")) {
            // final List<Chit> filteredModelList = filter(orderlist, newText);
            // mAdapter.setFilter(filteredModelList);
            // setctr(String.valueOf(filteredModelList.size()));
            // return true;
        }
        String filter = "";
        if (tab.equalsIgnoreCase("new")) {

            filter = "3,8,99,";
        } else if (tab.equalsIgnoreCase("inprogress")) {
            filter = "4,";
        } else if (tab.equalsIgnoreCase("close")) {
            filter = "5,";
        }

        if (newText.length() == 4 || newText.length() == 0) {

        } else {
            return true;
        }


        filteredModelList = searchMytask(sessionString, newText, filter, new MyCallback() {
            @Override
            public void callbackCall(List<Chit> filteredModelList) {
                mAdapter.setFilter(filteredModelList);
            }
        });

       /* new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);

            }
        }.execute();*/


        return true;
    }


    public void assignarchived(final String usersession, String chitIds, String employee_bridge_id, final String msg) {

        ApiInterface apiService =
                ApiClient.getClient(MyTaskActivity.this).create(ApiInterface.class);
        JsonObject a1 = new JsonObject();
        try {

            a1.addProperty("chitIds", chitIds);
            a1.addProperty("employee_bridge_id", employee_bridge_id);

            if (!isSmsForWhat.isEmpty() && smsFlagForOpen.equalsIgnoreCase("yes")) {
                a1.addProperty("sms", "true");
            }

        } catch (JsonParseException e) {
            e.printStackTrace();
        }
        //   Toast.makeText(MyTaskActivity.this, employee_bridge_id, Toast.LENGTH_LONG).show();

        //Call call = apiService.removechit(usersession,chitIds+",","10");
        Call call = apiService.assigntask(usersession, a1);


        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                int statusCode = response.code();
                Log.e("test", sessionString + "sessionstring");

                Headers headerList = response.headers();
                Log.d("testhistory", statusCode + " ");

                if (response.isSuccessful()) {
                    try {
                        Log.e("response.body()", response.body().toString());
                        String a = new Gson().toJson(response.body());

                        Toast.makeText(MyTaskActivity.this, msg, Toast.LENGTH_LONG).show();

                    } catch (Exception e) {
                        Log.e("errortest", e.getMessage());

                    }

                } else {
                    Log.e("test", " trdds1");

                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Toast.makeText(MyTaskActivity.this, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(MyTaskActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }

                totalcount = 0;
                str_actionid = "";
                MenuItem item1 = menu.findItem(R.id.action_delete);
                item1.setVisible(false);
                showsearch(true);
                pagecount = 1;
                isdataavailable = true;
                usersignin(usersession, pagecount, true, false);

            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // Log error here since request failed
                findViewById(R.id.rl_pb_ctask).setVisibility(View.GONE);
                Log.e("error message", t.toString());
            }
        });
    }

    public List<Chit> searchMytask(String usersession, String q, String filter, final MyCallback listener) {

        final List<Chit> filteredModelList = new ArrayList<>();

        ApiInterface apiService =
                ApiClient.getClient(MyTaskActivity.this).create(ApiInterface.class);
        Call call = apiService.searchMytask(usersession, q, filter);

        if (isbusiness) {
            call = apiService.searchAlltask(usersession, q, filter, 1);
        }

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {


                if (response.isSuccessful()) {
                    try {
                        String a = new Gson().toJson(response.body());

                        JSONObject jObjRes = new JSONObject(a);
                        JSONObject jObjdata = jObjRes.getJSONObject("response");
                        JSONObject jObjResponse = jObjdata.getJSONObject("data");
                        String totalRecord = jObjResponse.getString("totalRecord");
                        JSONArray jObjcontacts = jObjResponse.getJSONArray("content");


                        for (int i = 0; i < jObjcontacts.length(); i++) {
                            if (jObjcontacts.get(i) instanceof JSONObject) {
                                JSONObject jsnObj = (JSONObject) jObjcontacts.get(i);
                                Chit obj = new Gson().fromJson(jsnObj.toString(), Chit.class);

                                filteredModelList.add(obj);
                            }
                        }

                        listener.callbackCall(filteredModelList);
                        setctr(filteredModelList, totalRecord);

                    } catch (Exception e) {

                        Toast.makeText(MyTaskActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                } else {

                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Toast.makeText(MyTaskActivity.this, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(MyTaskActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // Log error here since request failed  mSwipyRefreshLayout.setRefreshing(false);
                Log.e("error message", t.toString());
            }
        });
        return filteredModelList;

    }

    interface MyCallback {
        void callbackCall(List<Chit> filteredModelList);
    }

    private MyCallback listener;

    public void setctr(List<Chit> orderlist, String tt) {
        int newtask = 0, closetask = 0, inprogtask = 0;
        try {
            /*for (int i = 0; i < orderlist.size(); i++) {
                String t_status = orderlist.get(i).getTransaction_status();

                if (t_status.equalsIgnoreCase("ACTIVE") || t_status.equalsIgnoreCase("ACCEPTED") || t_status.equalsIgnoreCase("HOLD")) {
                    newtask++;

                }

                if (t_status.equalsIgnoreCase("INPROGRESS")) {
                    inprogtask++;
                }


                if (t_status.equalsIgnoreCase("FINISHED") ) {
                    closetask++;

                }

            }
*/
            if (tab.equalsIgnoreCase("new")) {
                rb_newtask.setText("Open [ " + tt + " ]");
                rb_inprogtask.setText("Act");
                rb_close.setText("Close");
            } else if (tab.equalsIgnoreCase("inprogress")) {
                rb_inprogtask.setText("Act [ " + tt + " ]");
                rb_close.setText("Close");
                rb_newtask.setText("Open");
            } else if (tab.equalsIgnoreCase("close")) {
                rb_close.setText("Close [ " + tt + " ]");
                rb_newtask.setText("Open");
                rb_inprogtask.setText("Act");

                if (tt.equalsIgnoreCase("0")) {
                    recyclerView.setVisibility(View.GONE);
                    findViewById(R.id.tv_nodata).setVisibility(View.VISIBLE);
                } else {
                    recyclerView.setVisibility(View.VISIBLE);
                    findViewById(R.id.tv_nodata).setVisibility(View.GONE);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();

        timer = new CountDownTimer(15 * 60 * 1000, 1000) {

            public void onTick(long millisUntilFinished) {
                //Some code
                long sec = millisUntilFinished / 1000;
                Log.e("sec", String.valueOf(sec));
            }

            public void onFinish() {

                isdataavailable = true;
                pagecount = 1;
                usersignin(sessionString, 1, false, false);
            }
        };

        timer.start();

        findViewById(R.id.rl_pb_ctask).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();

        timer.cancel();
    }

    CountDownTimer timer;

    ArrayList<String> al_refund = new ArrayList<>();

    public void refundafterwithdrawn() {
        if (al_refund != null && al_refund.size() > 0) {
            new CanelOrder_Refund(MyTaskActivity.this, al_refund.get(0), new CanelOrder_Refund.RefundCallback() {
                @Override
                public void showconfirmcancel() {

                }

                @Override
                public void showdescription() {

                }

                @Override
                public void refunded() {
                    al_refund.remove(0);
                    refundafterwithdrawn();
                }
            });
        }
    }

    boolean isremoveclick = false;
    String purpose = "";

    public void showsearch(boolean isshow) {
        MenuItem action_search = menu.findItem(R.id.action_search);
        action_search.setVisible(isshow);
        MenuItem action_filter = menu.findItem(R.id.action_filter);
        //action_filter.setVisible(isshow);

        isshow = !isshow;
        MenuItem action_employee = menu.findItem(R.id.action_employee);
        action_employee.setVisible(isshow);
    }

    public void getEmployee() {
        sessionManager = new SessionManager(this);
        pb.setVisibility(View.VISIBLE);
        ApiInterface apiService = ApiClient.getClient(this).create(ApiInterface.class);

        Call call;
        call = apiService.getEmployee(sessionManager.getsession());

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {

                try {
                    pb.setVisibility(View.GONE);
                    if (response.isSuccessful()) {

                        String a = new Gson().toJson(response.body());
                        JSONObject jObjRes = new JSONObject(a);
                        JSONObject jObjdata = jObjRes.getJSONObject("response");
                        JSONObject jObjResponse = jObjdata.getJSONObject("data");
                        String totalRecord = jObjResponse.getString("totalRecord");
                        JSONArray jObjcontacts = jObjResponse.getJSONArray("content");

                        String displayEndRecord = jObjResponse.getString("displayEndRecord");
                        if (totalRecord.equals(displayEndRecord)) {
                            // isdataavailable = false;
                        } else {
                            // isdataavailable = true;
                        }


                        for (int i = 0; i < jObjcontacts.length(); i++) {
                            if (jObjcontacts.get(i) instanceof JSONObject) {
                                JSONObject jsnObj = (JSONObject) jObjcontacts.get(i);
                                Employee obj = new Gson().fromJson(jsnObj.toString(), Employee.class);
                                al_aggre.add(obj);
                            }
                        }

                        eAdapter.setitem(al_aggre);

                       /* if (al_aggre.size() == 0) {
                            tv_empty.setVisibility(View.VISIBLE);
                        } else {
                            tv_empty.setVisibility(View.GONE);
                        }*/

                    } else {
                        try {
                            JSONObject jObjError = new JSONObject(response.errorBody().string());
                            JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } catch (Exception e) {

                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // Log error here since request failed
                try {
                    pb.setVisibility(View.GONE);
                } catch (Exception e) {

                }
            }
        });
    }
}
