package com.cab.digicafe.Activities.mrp;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatRadioButton;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cab.digicafe.Model.ShareModel;
import com.cab.digicafe.MyCustomClass.AutoCompleteTv;
import com.cab.digicafe.MyCustomClass.PlayAnim;
import com.cab.digicafe.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class SortBySelectDialogue extends AlertDialog implements
        View.OnClickListener {
    private final OnDialogClickListener listener;
    public Context c;
    public Dialog d;


    @BindView(R.id.llAnim)
    LinearLayout llAnim;

    @BindView(R.id.tvAdd)
    TextView tvAdd;

    @BindView(R.id.tvCancel)
    TextView tvCancel;


    @BindView(R.id.tvStatus)
    TextView tvStatus;

    @BindView(R.id.tvMsg)
    TextView tvMsg;

    @BindView(R.id.rlHide)
    RelativeLayout rlHide;

    @BindView(R.id.arb_psd)
    AppCompatRadioButton arb_psd;

    @BindView(R.id.arb_ped)
    AppCompatRadioButton arb_ped;

    @BindView(R.id.arb_asd)
    AppCompatRadioButton arb_asd;

    @BindView(R.id.arb_aed)
    AppCompatRadioButton arb_aed;

    @BindView(R.id.rgSortByDate)
    RadioGroup rgSortByDate;

    @BindView(R.id.rgOdr)
    RadioGroup rgOdr;

    @BindView(R.id.arb_asc_odr)
    AppCompatRadioButton arb_asc_odr;

    @BindView(R.id.arb_dsc_odr)
    AppCompatRadioButton arb_dsc_odr;

    @BindView(R.id.cbMaterial)
    CheckBox cbMaterial;
    @BindView(R.id.cbResource)
    CheckBox cbResource;
    @BindView(R.id.cbPlanning)
    CheckBox cbPlanning;
    @BindView(R.id.cbLink)
    CheckBox cbLink;
    @BindView(R.id.cbDLink)
    CheckBox cbDLink;
    @BindView(R.id.cbIssue)
    CheckBox cbIssue;
    @BindView(R.id.cbComplete)
    CheckBox cbComplete;

    String title = "", msg = "";

    int sortBy = 0, orderBy = 0;

    public SortBySelectDialogue(Context a, OnDialogClickListener listener, String title, int sortBy, int orderBy) {
        super(a);
        // TODO Auto-generated constructor stub
        this.c = a;
        this.listener = listener;
        this.title = title;
        this.msg = msg;
        this.sortBy = sortBy;
        this.orderBy = orderBy;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_sort_by_dialogue);
        ButterKnife.bind(this);

        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        getWindow().setAttributes(lp);

        tvAdd.setOnClickListener(this);
        tvCancel.setOnClickListener(this);
        rlHide.setOnClickListener(this);
        setCancelable(true);

        tvStatus.setText(title);
        tvMsg.setText(msg);

        setOnKeyListener(new OnKeyListener() {

            @Override
            public boolean onKey(DialogInterface arg0, int keyCode,
                                 KeyEvent event) {
                // TODO Auto-generated method stub
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    dismissDialogue(0);
                }
                return true;
            }
        });

        rlHide.setOnClickListener(this);
        playAnim.slideDownRl(llAnim);

        rgSortByDate.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.arb_psd:
                        checkSortCtr(0);
                        break;
                    case R.id.arb_ped:
                        checkSortCtr(1);
                        break;
                    case R.id.arb_asd:
                        checkSortCtr(2);
                        break;
                    case R.id.arb_aed:
                        checkSortCtr(3);
                        break;
                }
            }
        });

        rgOdr.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.arb_dsc_odr:
                        orderBy = 0;
                        break;
                    case R.id.arb_asc_odr:
                        orderBy = 1;
                        break;

                }

                checkSortCtr(srtPos);
            }
        });

        setSortBySpinner();


        orderBy = odrPos;


    }


    public void dismissDialogue(final int call) {
        playAnim.slideUpRl(llAnim);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                ShareModel.getI().isSortByMaterial = false;
                ShareModel.getI().isSortByPlanning = false;
                ShareModel.getI().isSortByResource = false;
                if (cbMaterial.isChecked()) ShareModel.getI().isSortByMaterial = true;
                if (cbResource.isChecked()) ShareModel.getI().isSortByResource = true;
                if (cbPlanning.isChecked()) ShareModel.getI().isSortByPlanning = true;


                ShareModel.getI().isLink = false;
                ShareModel.getI().isDlink = false;
                ShareModel.getI().isIssue = false;
                ShareModel.getI().isComplete = false;
                if (cbLink.isChecked()) ShareModel.getI().isLink = true;
                if (cbDLink.isChecked()) ShareModel.getI().isDlink = true;
                if (cbComplete.isChecked()) ShareModel.getI().isComplete = true;
                if (cbIssue.isChecked()) ShareModel.getI().isIssue = true;

                listener.onDialogImageRunClick(call, srtPos, orderBy);
                dismiss();
                playAnim.slideDownRl(llAnim);

            }
        }, 700);

    }

    PlayAnim playAnim = new PlayAnim();

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvCancel:
                dismissDialogue(0);
                break;
            case R.id.tvAdd:
                dismissDialogue(1);
                break;
            case R.id.rlHide:
                dismissDialogue(0);
                break;
            default:
                break;
        }
    }

    public interface OnDialogClickListener {
        void onDialogImageRunClick(int pos, int sortBy, int orderBy);
    }


    //@BindView(R.id.spn_sortby)
    //Spinner spn_sortby;
    AutoCompleteTv autoCompleteTv = new AutoCompleteTv();
    ArrayList<AutoCompleteTv> alTractions = new ArrayList<>();
    int tractionId = 0;
    String type = "";

    public void setSortBySpinner() {

        alTractions = new ArrayList<>();
        alTractions.add(new AutoCompleteTv("Plan start date", 0));
        alTractions.add(new AutoCompleteTv("Plan end date", 1));
        alTractions.add(new AutoCompleteTv("Actual start date", 2));
        alTractions.add(new AutoCompleteTv("Actual end date", 3));

       /* autoCompleteTv.setautofill(c, spn_sortby, alTractions);
        if(sortBy==-1) spn_sortby.setSelection(0);
        else spn_sortby.setSelection(sortBy);

        spn_sortby.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                checkSortCtr(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });*/

        switch (sortBy) {
            case 0:
                arb_psd.setChecked(true);
                break;
            case 1:
                arb_ped.setChecked(true);
                break;
            case 2:
                arb_asd.setChecked(true);
                break;
            case 3:
                arb_aed.setChecked(true);
                break;
            default:
                arb_psd.setChecked(true);
                break;
        }

        switch (orderBy) {
            case 0:
                arb_dsc_odr.setChecked(true);
                break;
            case 1:
                arb_asc_odr.setChecked(true);
                break;
        }

        if (ShareModel.getI().isSortByMaterial) cbMaterial.setChecked(true);
        if (ShareModel.getI().isSortByResource) cbResource.setChecked(true);
        if (ShareModel.getI().isSortByPlanning) cbPlanning.setChecked(true);

        if (ShareModel.getI().isLink) cbLink.setChecked(true);
        if (ShareModel.getI().isDlink) cbDLink.setChecked(true);
        if (ShareModel.getI().isComplete) cbComplete.setChecked(true);
        if (ShareModel.getI().isIssue) cbIssue.setChecked(true);

    }

    public void checkSortCtr(int pos) {
        srtPos = pos;
        if (sortBy == pos) {
            tvAdd.setEnabled(true);
            tvAdd.setAlpha(1f);
        } else {
            tvAdd.setEnabled(true);
            tvAdd.setAlpha(1f);
        }


        if (srtPos == ShareModel.getI().sortBy && orderBy == ShareModel.getI().orderBy) {
            // tvAdd.setEnabled(false);
            // tvAdd.setAlpha(0.5f);

            tvAdd.setEnabled(true);
            tvAdd.setAlpha(1f);
        } else {
            tvAdd.setEnabled(true);
            tvAdd.setAlpha(1f);
        }


    }

    int srtPos = 0;
    int odrPos = 0;
}
