package com.cab.digicafe.Activities.master;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cab.digicafe.Activities.MyAppBaseActivity;
import com.cab.digicafe.Activities.RequestApis.MyRequestCall;
import com.cab.digicafe.Activities.RequestApis.MyRequst;
import com.cab.digicafe.Adapter.master.EnqListAdapter;
import com.cab.digicafe.ChitlogActivity;
import com.cab.digicafe.DetailsFragment;
import com.cab.digicafe.Fragments.SummaryDetailFragmnet;
import com.cab.digicafe.Helper.SessionManager;
import com.cab.digicafe.Model.Chit;
import com.cab.digicafe.Model.ShareModel;
import com.cab.digicafe.MyCustomClass.PlayAnim;
import com.cab.digicafe.R;
import com.google.gson.Gson;
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EnquiryOfPropertyActivity extends MyAppBaseActivity implements SwipyRefreshLayout.OnRefreshListener, SearchView.OnQueryTextListener {

    PlayAnim playAnim = new PlayAnim();


    @BindView(R.id.rvEnq)
    RecyclerView rvEnq;

    @BindView(R.id.tv_empty)
    TextView tv_empty;

    @BindView(R.id.rl_pb)
    RelativeLayout rl_pb;

    Context context;
    SessionManager sessionManager;
    MyRequestCall myRequestCall = new MyRequestCall();
    MyRequst myRequst = new MyRequst();
    SearchView searchView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_property_enquiry);

        ButterKnife.bind(this);


        tv_empty.setVisibility(View.GONE);

        context = this;
        sessionManager = new SessionManager(context);
        myRequst.cookie = sessionManager.getsession();
        myRequst.page = pagecount;

        setEmployee();
        refreshlist();

        myRequst.cookie = sessionManager.getsession();
        myRequst.purpose = ShareModel.getI().purpose;
        myRequst.chit_name = ShareModel.getI().chit_name;


        callEmployee();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("Enquiry" + "");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


    }


    SwipyRefreshLayout mSwipyRefreshLayout;
    int pagecount = 1;
    boolean isdataavailable = true;

    public void refreshlist() {
        mSwipyRefreshLayout = (SwipyRefreshLayout) findViewById(R.id.swipyrefreshlayout);
        mSwipyRefreshLayout.setDirection(SwipyRefreshLayoutDirection.BOTH);
        mSwipyRefreshLayout.setOnRefreshListener(this);
    }

    @Override
    public void onRefresh(SwipyRefreshLayoutDirection direction) {

        if (direction == SwipyRefreshLayoutDirection.BOTTOM) {
            if (isdataavailable) {
                pagecount++;
                myRequst.page = pagecount;
                callEmployee();
                //callCat();
            } else {
                mSwipyRefreshLayout.setRefreshing(false);
                Toast.makeText(this, "No more data available", Toast.LENGTH_LONG).show();
            }

        }

        if (direction == SwipyRefreshLayoutDirection.TOP) {
            pagecount = 1;
            myRequst.page = pagecount;
            alEnq = new ArrayList<>();
            callEmployee();

        }
    }


    EnqListAdapter employeeUnderMasterActivity;
    private ArrayList<Chit> alEnq = new ArrayList<>();

    public void setEmployee() {
        employeeUnderMasterActivity = new EnqListAdapter(alEnq, context, new EnqListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Chit item) {

                Intent i = new Intent(EnquiryOfPropertyActivity.this, ChitlogActivity.class);
                i.putExtra("chit_hash_id", item.getChit_hash_id());
                i.putExtra("chit_id", item.getChit_id() + "");
                i.putExtra("transtactionid", item.getChit_name() + "");
                i.putExtra("productqty", item.getTotal_chit_item_value() + "");
                i.putExtra("productprice", item.getChit_item_count() + "");
                i.putExtra("purpose", item.getPurpose());
                i.putExtra("Refid", item.getRef_id());
                i.putExtra("Caseid", item.getCase_id());
                i.putExtra("isprint", true);
                //i.putExtra("isMrpForAllTask", true);

                /*if (tab.equalsIgnoreCase("new_all")) {  // open task
                    if (sessionManager.getisBusinesslogic()) {
                        i.putExtra("iscomment", true);  // business
                        i.putExtra("isdiaries", true);  // if open & business then diary for comment
                    } else {
                        i.putExtra("iscomment", false);  // dont show for employee
                    }

                } else {
                    i.putExtra("iscomment", true);
                }
*/
                i.putExtra("iscomment", true);
                i.putExtra("isEnquiry", true);

                DetailsFragment.chititemcount = "" + item.getChit_item_count();
                DetailsFragment.chititemprice = "" + item.getTotal_chit_item_value();
                SummaryDetailFragmnet.item = item;
                DetailsFragment.item_detail = item;


                startActivity(i);


            }

            @Override
            public void onTextSearch(String search) {
                searchView.setIconified(false);
                searchView.setQuery(search, true);


                myRequst.contact_number = search;
                myRequst.chit_name = "";
            }

            @Override
            public void onCheckChange(int pos, boolean isChecked) {
                //alCategory.get(pos).isSelect = isChecked;

            }

            @Override
            public void onDrag() {
            }
        });

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rvEnq.setLayoutManager(mLayoutManager);
        rvEnq.setItemAnimator(new DefaultItemAnimator());
        rvEnq.setAdapter(employeeUnderMasterActivity);
        rvEnq.setNestedScrollingEnabled(false);

        //ItemTouchHelper.Callback callback = new DragTouchTopBottomHelperForCategory(categoryUnderMasterListAdapter);
        //ItemTouchHelper helper = new ItemTouchHelper(callback);
        //helper.attachToRecyclerView(rvCat);

    }

    public void callEmployee() {

        myRequst.page = pagecount;

        /*if(myRequst.chit_name.isEmpty())
        {
            tv_empty.setVisibility(View.VISIBLE);
            rl_pb.setVisibility(View.GONE);
            return;
        }*/

        myRequestCall.getPropertyEnquiry(context, myRequst, new MyRequestCall.CallRequest() {
            @Override
            public void onGetResponse(String response) {
                rl_pb.setVisibility(View.GONE);
                mSwipyRefreshLayout.setRefreshing(false);
                try {
                    JSONObject jObjRes = new JSONObject(response);

                    JSONObject jObjdata = jObjRes.getJSONObject("response");
                    JSONObject jObjResponse = jObjdata.getJSONObject("data");
                    String totalRecord = jObjResponse.getString("totalRecord");
                    getSupportActionBar().setTitle("Enquiry [" + totalRecord + "]");

                    JSONArray jObjcontacts = jObjResponse.getJSONArray("content");

                    String displayEndRecord = jObjResponse.getString("displayEndRecord");

                    if (totalRecord.equals(displayEndRecord)) {
                        isdataavailable = false;
                    } else {
                        isdataavailable = true;
                    }

                    ArrayList<Chit> tmpAlCategory = new ArrayList<>();

                    //JSONArray jaEmp = jObjResponse.getJSONArray("employee");

                    for (int i = 0; i < jObjcontacts.length(); i++) {
                        if (jObjcontacts.get(i) instanceof JSONObject) {
                            JSONObject jsnObj = (JSONObject) jObjcontacts.get(i);
                            Chit obj = new Gson().fromJson(jsnObj.toString(), Chit.class);

                         /*   for (int j = 0; j < jaEmp.length(); j++) {
                                if (jaEmp.get(j) instanceof JSONObject) {
                                    JSONObject joEmp = (JSONObject) jObjcontacts.get(i);
                                    String employee_name = joEmp.getString("employee_name");
                                    if(employee_name.equalsIgnoreCase(obj.getAssign_to_bridge_id()));
                                }
                            }
*/

                            tmpAlCategory.add(obj);
                        }
                    }


                    alEnq.addAll(tmpAlCategory);

                    employeeUnderMasterActivity.setitem(alEnq);


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                tv_empty.setVisibility(View.GONE);
                if (alEnq.size() == 0) {
                    tv_empty.setVisibility(View.VISIBLE);
                }

            }
        });

    }

    @Override
    public void onBackPressed() {
        if (rl_pb.getVisibility() == View.VISIBLE) {
        } else {
            super.onBackPressed();
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
        }


        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean onQueryTextSubmit(String s) {
        myRequst.contact_number = s;

        myRequst.chit_name = "";
        pagecount = 1;
        myRequst.page = pagecount;
        alEnq = new ArrayList<>();

        callEmployee();
        return true;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        if (s.length() == 0) {
            myRequst.contact_number = "";
            pagecount = 1;
            myRequst.page = pagecount;
            alEnq = new ArrayList<>();
            myRequst.chit_name = ShareModel.getI().chit_name;

            callEmployee();

        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);

        final MenuItem item = menu.findItem(R.id.action_search);
        searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setOnQueryTextListener(this);


        return true;
    }


}
