package com.cab.digicafe.Activities;

import android.app.Activity;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cab.digicafe.Adapter.ctlg.MetalListAdapter;
import com.cab.digicafe.Helper.Inad;
import com.cab.digicafe.Helper.SessionManager;
import com.cab.digicafe.Helper.SharedPrefUserDetail;
import com.cab.digicafe.Model.Catelog;
import com.cab.digicafe.Model.Metal;
import com.cab.digicafe.Model.ShareModel;
import com.cab.digicafe.Model.Userprofile;
import com.cab.digicafe.MyCustomClass.ApplicationUtil;
import com.cab.digicafe.MyCustomClass.JsonObjParse;
import com.cab.digicafe.R;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class ProductDetiailsDialogue extends AlertDialog implements
        View.OnClickListener, TextWatcher {


   /* @Override
    public void onAddedImg(ArrayList<ModelFile> al_data) {
        ArrayList<ModelFile> al_temp_selet = new ArrayList<>();
        al_temp_selet.addAll(al_selet);
        selectFileAdapter.setItem(al_temp_selet);
        al_selet = new ArrayList<>();
        al_selet.addAll(al_temp_selet);

    }*/

    public interface AddWholesale {
        public void addItems(Catelog catelog);

        public void deleteItems(Catelog catelog);
    }


    public Activity c;

    @BindView(R.id.tv_title)
    EditText tv_title;

    @BindView(R.id.et_mrp)
    EditText et_mrp;

    @BindView(R.id.et_dis_per)
    EditText et_dis_per;

    @BindView(R.id.et_dis_val)
    EditText et_dis_val;

    @BindView(R.id.et_cgst)  // Tax Field 1
            EditText et_cgst;

    @BindView(R.id.et_sgst)  // Tax Field 2
            EditText et_sgst;

    @BindView(R.id.tvTaxField1)
    TextView tvTaxField1;

    @BindView(R.id.tvTaxField2)
    TextView tvTaxField2;

    @BindView(R.id.llCgst)
    LinearLayout llCgst;

    @BindView(R.id.llSgst)
    LinearLayout llSgst;

    @BindView(R.id.et_offer)
    EditText et_offer;


    @BindView(R.id.tv_total)
    TextView tv_total;


    @BindView(R.id.rl_upload)
    RelativeLayout rl_upload;

    @BindView(R.id.tv_progress)
    TextView tv_progress;

    @BindView(R.id.tv_calc_dis)
    TextView tv_calc_dis;

    @BindView(R.id.tv_calc_ctax)
    TextView tv_calc_ctax;

    @BindView(R.id.tv_calc_stax)
    TextView tv_calc_stax;

    @BindView(R.id.iv_cancel)
    ImageView iv_cancel;


    Catelog obj;
    String str_model;

    String currency = "";
    @BindView(R.id.tv_p_cur)
    TextView tv_p_cur;

    @BindView(R.id.tv_dp_cur)
    TextView tv_dp_cur;

    @BindView(R.id.llOfr)
    LinearLayout llOfr;

    @BindView(R.id.llDisPrice)
    LinearLayout llDisPrice;

    @BindView(R.id.llDisPerc)
    LinearLayout llDisPerc;

    @BindView(R.id.llFieldJson)
    LinearLayout llFieldJson;

    @BindView(R.id.et_commnet)
    EditText et_commnet;


    boolean isAdd = false;

    AddWholesale addWholesale;

    @BindView(R.id.llAdditionalPrice)
    LinearLayout llAdditionalPrice;

    String doctor_template_type = "";
    @BindView(R.id.tv_ap_cur)
    TextView tv_ap_cur;
    @BindView(R.id.et_Amrp)
    EditText et_Amrp;
    @BindView(R.id.tv_calc_Adis)
    TextView tv_calc_Adis;

    public ProductDetiailsDialogue(Activity a, Catelog item, boolean isAdd, AddWholesale addWholesale) {
        super(a);
        // TODO Auto-generated constructor stub
        this.c = a;
        this.obj = item;
        this.addWholesale = addWholesale;
        this.isAdd = isAdd;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_copycatalogue_product);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));


        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
        params.gravity = Gravity.CENTER;
        getWindow().setAttributes(params);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);


        ButterKnife.bind(this);


        //currency = SharedPrefUserDetail.getString(c, SharedPrefUserDetail.symbol_native, "") + " ";
        currency = obj.getSymbol_native();

        if (currency == null) {
            currency = SharedPrefUserDetail.getString(c, SharedPrefUserDetail.chit_symbol_native, "") + " ";
        } else {
            currency = obj.getSymbol_native() + " ";
        }

        tv_p_cur.setText(currency);
        tv_ap_cur.setText(currency);
        tv_dp_cur.setText(currency);


        tv_cur3.setText(currency);
        tv_cur4.setText(currency);
        tv_cur5.setText(currency);
        tv_cur6.setText(currency);
        tv_cur7.setText(currency);
        txtDedPriceCs.setText("Actual Price (" + currency + ")");
        txtDedOrgPriceCs.setText("Original Price (" + currency + ")");
        txtAddOrgPriceCs.setText("Original Price (" + currency + ")");
        txtAddPriceCs.setText("Actual Price (" + currency + ")");


        tv_title.setText(obj.getName());
        tv_title.setText(obj.getDefault_name());

        sessionManager = new SessionManager(c);
        String userprofile = sessionManager.getUserprofile();
        try {
            userProfileData = new Gson().fromJson(userprofile, Userprofile.class);
        } catch (Exception e) {
            e.printStackTrace();
        }


        try {
            String tax = obj.getTax_json_data();
            if (tax != null) {
                //JSONObject jo_tax = new JSONObject(tax);
                //sgst = Double.valueOf(jo_tax.getString("SGST"));
                //cgst = Double.valueOf(jo_tax.getString("CGST"));
                setTax(obj.getTax_json_data());
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }


        try {
            String comment = obj.getField_json_data();
            llFieldJson.setVisibility(View.GONE);
            if (!comment.isEmpty() && !comment.equals("{}"))
                llFieldJson.setVisibility(View.VISIBLE);
            et_commnet.setText(comment);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            setAdditional(obj.getAvailable_stock());
        } catch (Exception e) {
            e.printStackTrace();
        }


        et_offer.setText(obj.getOffer());
        llOfr.setVisibility(View.VISIBLE);
        if (obj.getOffer().isEmpty()) {
            llOfr.setVisibility(View.GONE);
        }

        //setCancelable(false);


        //if(al_selet.size()==0)iv_upload.setVisibility(View.GONE);
        //else iv_upload.setVisibility(View.VISIBLE);


       /* iv_upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                str_img = new String[al_selet.size()];
                uploadimg();

            }
        });*/

        rl_upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        findViewById(R.id.rl_pb_cat).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        llPropertyFinancialBlock.setVisibility(View.GONE);

        et_dis_per.addTextChangedListener(this);
        et_dis_val.addTextChangedListener(this);
        et_mrp.addTextChangedListener(this);
        et_cgst.addTextChangedListener(this);
        et_sgst.addTextChangedListener(this);

        etSqFt.addTextChangedListener(this);
        etInfoVal1.addTextChangedListener(this);
        etInfoVal2.addTextChangedListener(this);
        etInfoVal3.addTextChangedListener(this);
        etInfoVal4.addTextChangedListener(this);
        etInfoVal5.addTextChangedListener(this);

        //et_cgst.addTextChangedListener(new ValueTextWatcher(et_cgst, et_sgst, c));
        //et_sgst.addTextChangedListener(new ValueTextWatcher(et_sgst, et_cgst, c));

        et_cgst.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                ApplicationUtil.setFocusedId(v.getId());
            }
        });

        et_sgst.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                ApplicationUtil.setFocusedId(v.getId());
            }
        });


        /******************** Tax from Discount *************************/


        //tv_calc_ctax.setText(currency + String.format("%.2f", centralgst));
        tv_calc_ctax.setText(currency + Inad.getCurrencyDecimal(centralgst, c));
        //tv_calc_stax.setText(currency + String.format("%.2f", stategst));
        tv_calc_stax.setText(currency + Inad.getCurrencyDecimal(stategst, c));


        //c.startActivityForResult(intent, 0);


        iv_cancel.setOnClickListener(this);

        checkWholeSale();

        checkjewel();

        calc_tax_value();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_cancel:

                dismiss();
                break;
            case R.id.txt_ok:

                dismiss();
                break;
            case R.id.iv_cancel:
                dismiss();
                break;
            default:
                break;
        }

    }


    public String getdiscountmrp(Double mrpprice, Double discount_percentage, Double discounted_price) {
        Double discountprice = mrpprice;

        tvTotalDiscPerc.setText("");

        if (discount_percentage > 0) {
            if (mrpprice > 0) {
                Double totalDisc = (mrpprice * (discount_percentage / 100));
                tvTotalDiscPerc.setText("- " + currency + Inad.getCurrencyDecimal(totalDisc, c));
                discountprice = mrpprice - totalDisc;
            }
        } else if (discounted_price > 0) {
            Double totalDisc = mrpprice - discounted_price;
            tvTotalDiscPerc.setText("- " + currency + Inad.getCurrencyDecimal(totalDisc, c));


            discountprice = discounted_price;
        }

        return String.valueOf(discountprice);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        calc_tax_value();

    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    public void calc_tax_value() {
        try {
            String price = et_mrp.getText().toString().trim();
            String Additionalprice = et_Amrp.getText().toString().trim();
            Double dedPriceForJewel = 0.0, additionalPriceForJewel = 0.0;
            Double ded_mrp = 0.0, additional_mrp = 0.0;

            Double mrp = 0.0;
            if (!price.isEmpty()) {
                mrp = Double.valueOf(price);
            } else {

            }

            Double Amrp = 0.0;
            if (!Additionalprice.isEmpty()) Amrp = Double.valueOf(Additionalprice);

            if (llQty.getVisibility() == View.VISIBLE) {
                String q = et_qty.getText().toString().trim();
                if (!q.isEmpty()) {
                    qty = Integer.parseInt(q);
                    mrp = mrp * qty;
                    tvAddTocart.setText(addUpdate + q + " item to cart");
                    tvAddTocart.setEnabled(true);
                    tvAddTocart.setAlpha(1f);
                } else {
                    tvAddTocart.setText(addUpdate + "item to cart");
                    tvAddTocart.setEnabled(false);
                    tvAddTocart.setAlpha(0.5f);
                }
                if (price.isEmpty()) {
                    tvAddTocart.setText(addUpdate + "item to cart");
                    tvAddTocart.setEnabled(false);
                    tvAddTocart.setAlpha(0.5f);
                }

            } else if (llPropertyFinancialBlock.getVisibility() == View.VISIBLE) {

                String sqFt = etSqFt.getText().toString().trim();
                if (!sqFt.isEmpty()) {
                    int sqFtVal = Integer.parseInt(sqFt);
                    mrp = mrp * sqFtVal;
                }

                String info1 = etInfoVal1.getText().toString().trim();
                if (!info1.isEmpty()) {
                    int info1Val = Integer.parseInt(info1);
                    mrp = mrp + info1Val;
                }

                String info2 = etInfoVal2.getText().toString().trim();
                if (!info2.isEmpty()) {
                    int info2Val = Integer.parseInt(info2);
                    mrp = mrp + info2Val;
                }

                String info3 = etInfoVal3.getText().toString().trim();
                if (!info3.isEmpty()) {
                    int info3Val = Integer.parseInt(info3);
                    mrp = mrp + info3Val;
                }

                String info4 = etInfoVal4.getText().toString().trim();
                if (!info4.isEmpty()) {
                    int info4Val = Integer.parseInt(info4);
                    mrp = mrp + info4Val;
                }

                String info5 = etInfoVal5.getText().toString().trim();
                if (!info5.isEmpty()) {
                    int info5Val = Integer.parseInt(info5);
                    mrp = mrp + info5Val;
                }
            } else if (llJewelInfo.getVisibility() == View.VISIBLE) {

                String weight = etJewelWeight.getText().toString().trim();
                if (!weight.isEmpty()) {
                    float wt = Float.parseFloat(weight);
                    //mrp = (mrp * wt) / 10; // 10 gram
                    mrp = (mrp * wt) / 1; // 1 unit
                }

                // Calc Deduction

                // if actual is empty then calc original
                String ded1 = etActDedPrice1.getText().toString().trim();
                String oDed1 = etOrgDedPrice1.getText().toString().trim();
                String dedPerc1 = etActDedPerc1.getText().toString().trim();
                String oDedPerc1 = etOrgDedPerc1.getText().toString().trim();

                dedPriceForJewel = dedPriceForJewel + calcValuesOfAddDed(mrp, dedPerc1, oDedPerc1, ded1, oDed1);


                String ded2 = etActDedPrice2.getText().toString().trim();
                String oDed2 = etOrgDedPrice2.getText().toString().trim();
                String dedPerc2 = etActDedPerc2.getText().toString().trim();
                String oDedPerc2 = etOrgDedPerc2.getText().toString().trim();

                dedPriceForJewel = dedPriceForJewel + calcValuesOfAddDed(mrp, dedPerc2, oDedPerc2, ded2, oDed2);


                String ded3 = etActDedPrice3.getText().toString().trim();
                String oDed3 = etOrgDedPrice3.getText().toString().trim();
                String dedPerc3 = etActDedPerc3.getText().toString().trim();
                String oDedPerc3 = etOrgDedPerc3.getText().toString().trim();

                dedPriceForJewel = dedPriceForJewel + calcValuesOfAddDed(mrp, dedPerc3, oDedPerc3, ded3, oDed3);

                String ded4 = etActDedPrice4.getText().toString().trim();
                String oDed4 = etOrgDedPrice4.getText().toString().trim();
                String dedPerc4 = etActDedPerc4.getText().toString().trim();
                String oDedPerc4 = etOrgDedPerc4.getText().toString().trim();

                dedPriceForJewel = dedPriceForJewel + calcValuesOfAddDed(mrp, dedPerc4, oDedPerc4, ded4, oDed4);


                String ded5 = etActDedPrice5.getText().toString().trim();
                String oDed5 = etOrgDedPrice5.getText().toString().trim();
                String dedPerc5 = etActDedPerc5.getText().toString().trim();
                String oDedPerc5 = etOrgDedPerc5.getText().toString().trim();

                dedPriceForJewel = dedPriceForJewel + calcValuesOfAddDed(mrp, dedPerc5, oDedPerc5, ded5, oDed5);


                /////////////////////////////////////////////////////////


                // Calc Additional

                // if actual is empty then calc original
                String add1 = etActAddPrice1.getText().toString().trim();
                String oAdd1 = etOrgAddPrice1.getText().toString().trim();
                String addPerc1 = etActAddPerc1.getText().toString().trim();
                String oAddPerc1 = etOrgAddPerc1.getText().toString().trim();

                additionalPriceForJewel = additionalPriceForJewel + calcValuesOfAddDed(mrp, addPerc1, oAddPerc1, add1, oAdd1);


                String add2 = etActAddPrice2.getText().toString().trim();
                String oAdd2 = etOrgAddPrice2.getText().toString().trim();
                String addPerc2 = etActAddPerc2.getText().toString().trim();
                String oAddPerc2 = etOrgAddPerc2.getText().toString().trim();

                additionalPriceForJewel = additionalPriceForJewel + calcValuesOfAddDed(mrp, addPerc2, oAddPerc2, add2, oAdd2);


                String add3 = etActAddPrice3.getText().toString().trim();
                String oAdd3 = etOrgAddPrice3.getText().toString().trim();
                String addPerc3 = etActAddPerc3.getText().toString().trim();
                String oAddPerc3 = etOrgAddPerc3.getText().toString().trim();

                additionalPriceForJewel = additionalPriceForJewel + calcValuesOfAddDed(mrp, addPerc3, oAddPerc3, add3, oAdd3);


                String add4 = etActAddPrice4.getText().toString().trim();
                String oAdd4 = etOrgAddPrice4.getText().toString().trim();
                String addPerc4 = etActAddPerc4.getText().toString().trim();
                String oAddPerc4 = etOrgAddPerc4.getText().toString().trim();

                additionalPriceForJewel = additionalPriceForJewel + calcValuesOfAddDed(mrp, addPerc4, oAddPerc4, add4, oAdd4);


                String add5 = etActAddPrice5.getText().toString().trim();
                String oAdd5 = etOrgAddPrice5.getText().toString().trim();
                String addPerc5 = etActAddPerc5.getText().toString().trim();
                String oAddPerc5 = etOrgAddPerc5.getText().toString().trim();

                additionalPriceForJewel = additionalPriceForJewel + calcValuesOfAddDed(mrp, addPerc5, oAddPerc5, add5, oAdd5);


                /////////////////////////////////////////////////////////


                Double ded_price = 0.0;
                Double ded_perc = 0.0;

                String ded_val = et_ded_val.getText().toString();
                if (!ded_val.isEmpty())
                    ded_price = Double.parseDouble(et_ded_val.getText().toString());

                String ded_per = et_ded_per.getText().toString();
                if (!ded_per.isEmpty())
                    ded_perc = Double.parseDouble(et_ded_per.getText().toString());

                ded_price = dedPriceForJewel;

                ded_mrp = Double.valueOf(getValues(mrp, ded_perc, ded_price));

                tvTotalDed.setText("- " + currency + Inad.getCurrencyDecimal(ded_mrp, c));

                Double addi_price = 0.0;
                Double addi_perc = 0.0;

                String addi_val = et_additional_val.getText().toString();
                if (!addi_val.isEmpty())
                    addi_price = Double.parseDouble(et_additional_val.getText().toString());

                String addi_per = et_additional_per.getText().toString();
                if (!addi_per.isEmpty())
                    addi_perc = Double.parseDouble(et_additional_per.getText().toString());

                addi_price = additionalPriceForJewel;

                additional_mrp = Double.valueOf(getValues(mrp, addi_perc, addi_price));

                tvTotalAdditional.setText("+ " + currency + Inad.getCurrencyDecimal(additional_mrp, c));

            }


            Double dis_price = 0.0;
            Double dis_perc = 0.0;

            String dis_val = et_dis_val.getText().toString();
            if (!dis_val.isEmpty()) dis_price = Double.parseDouble(et_dis_val.getText().toString());

            String dis_per = et_dis_per.getText().toString();
            if (!dis_per.isEmpty()) dis_perc = Double.parseDouble(et_dis_per.getText().toString());


            llDisPerc.setVisibility(View.GONE);
            if (dis_perc.doubleValue() > 0) llDisPerc.setVisibility(View.VISIBLE);
            llDisPrice.setVisibility(View.GONE);
            if (dis_price.doubleValue() > 0) llDisPrice.setVisibility(View.VISIBLE);


            Double dis_mrp = Double.valueOf(getdiscountmrp(mrp, dis_perc, dis_price).toString());
            tvTotalDiscPerc.setVisibility(View.GONE);
            if (dis_mrp > 0) {
                tvTotalDiscPerc.setVisibility(View.VISIBLE);
            }

            if (dis_mrp.doubleValue() != mrp.doubleValue()) {
                tv_calc_dis.setVisibility(View.VISIBLE);
                tv_calc_dis.setText(currency + Inad.getCurrencyDecimal(dis_mrp, c));
            } else {
                //tv_calc_dis.setVisibility(View.GONE);
            }

            //tv_calc_dis.setText(currency + Inad.getCurrencyDecimal(dis_mrp, c));

           /* if (doctor_template_type.equalsIgnoreCase("RPD")) {
                dis_mrp = dis_mrp - ded_mrp + additional_mrp + Amrp;
            } else {
                dis_mrp = dis_mrp - ded_mrp + additional_mrp;
            }
*/

            dis_mrp = dis_mrp - ded_mrp + additional_mrp;
            tvTotalWoTax.setText("Sub Total : " + currency + Inad.getCurrencyDecimal(dis_mrp, c));

            removeStrikeLineIfNoDiscValuew(tv_calc_dis);
            if (dis_perc == 0 && dis_price > 0) {
                strikeLineIfDiscValuew(tv_calc_dis);
                tvTotalDiscPerc.setText("+ " + currency + Inad.getCurrencyDecimal(dis_price, c));
            }

            tv_calc_dis.setText("+ " + currency + Inad.getCurrencyDecimal(mrp, c));


            /******************** Tax from Discount *************************/

            Double sgst = 0.0;
            Double cgst = 0.0;
            String s_cgst = et_cgst.getText().toString();
            if (!s_cgst.isEmpty()) cgst = Double.parseDouble(et_cgst.getText().toString());

            String s_sgst = et_sgst.getText().toString();
            if (!s_sgst.isEmpty()) sgst = Double.parseDouble(et_sgst.getText().toString());

            Double stategst = (dis_mrp / 100.0f) * sgst;
            Double centralgst = (dis_mrp / 100.0f) * cgst;


            //tv_calc_ctax.setText(currency + String.format("%.2f", centralgst));
            tv_calc_ctax.setText(currency + Inad.getCurrencyDecimal(centralgst, c));
            //tv_calc_stax.setText(currency + String.format("%.2f", stategst));
            tv_calc_stax.setText(currency + Inad.getCurrencyDecimal(stategst, c));

            if (dis_mrp > 0) {
                Double total = dis_mrp + stategst + centralgst;
                //tv_total.setText(currency + String.format("%.2f", total) + "" + "");
                tv_total.setText(currency + Inad.getCurrencyDecimal(total, c) + "" + "");
            } else {
                Double total = mrp + stategst + centralgst;
                //tv_total.setText(currency + String.format("%.2f", total) + "" + "");
                tv_total.setText(currency + Inad.getCurrencyDecimal(total, c) + "" + "");
            }

        } catch (Exception e) {

        }


    }


    SessionManager sessionManager;


    Userprofile userProfileData;


    public void setTax(String field_json_data) throws JSONException {

        Double dis_price = 0.0;
        Double dis_perc = 0.0;

        if (!obj.getDiscounted_price().equals(""))
            dis_price = Double.valueOf(obj.getDiscounted_price());
        if (!obj.getDiscount_percentage().equals(""))
            dis_perc = Double.valueOf(obj.getDiscount_percentage());

        llDisPerc.setVisibility(View.GONE);
        if (dis_perc.doubleValue() > 0) llDisPerc.setVisibility(View.VISIBLE);
        llDisPrice.setVisibility(View.GONE);
        if (dis_price.doubleValue() > 0) llDisPrice.setVisibility(View.VISIBLE);


        //et_dis_per.setText(String.format("%.2f", dis_perc) + "");
        et_dis_per.setText(Inad.getCurrencyDecimal(dis_perc, c) + "");
        //et_dis_val.setText(String.format("%.2f", dis_price) + "");
        et_dis_val.setText(Inad.getCurrencyDecimal(dis_price, c) + "");


        Double sgst = 0.0;
        Double cgst = 0.0;

        if (field_json_data != null) {
            String Tax = JsonObjParse.getValueEmpty(field_json_data, "Tax");
            if (!Tax.isEmpty()) {
                JSONObject joTax = new JSONObject(field_json_data);
                JSONArray arr = joTax.getJSONArray("Tax");
                JSONObject element;
                et_cgst.setText("");
                tvTaxField1.setText("");
                et_sgst.setText("");
                tvTaxField2.setText("");
                llCgst.setVisibility(View.GONE);
                llSgst.setVisibility(View.GONE);

                for (int i = 0; i < arr.length(); i++) {
                    element = arr.getJSONObject(i); // which for example will be Types,TotalPoints,ExpiringToday in the case of the first array(All_Details)

                    Iterator keys = element.keys();

                    while (keys.hasNext()) {
                        try {
                            String key = (String) keys.next();
                            String taxVal = JsonObjParse.getValueFromJsonObj(element, key);
                            if (et_cgst.getText().toString().isEmpty()) {
                                et_cgst.setText(taxVal);
                                tvTaxField1.setText(key);
                                llCgst.setVisibility(View.VISIBLE);
                                cgst = Double.valueOf(taxVal);
                            } else {
                                et_sgst.setText(taxVal);
                                tvTaxField2.setText(key);
                                llSgst.setVisibility(View.VISIBLE);
                                sgst = Double.valueOf(taxVal);
                            }
                            Log.e("key", key);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                /*  Charges  */
            } else {
                // Update
                et_cgst.setText("");
                tvTaxField1.setText("");
                et_sgst.setText("");
                tvTaxField2.setText("");

                llCgst.setVisibility(View.GONE);
                llSgst.setVisibility(View.GONE);


                JSONObject joTax = new JSONObject(field_json_data);

                Iterator keys = joTax.keys();

                while (keys.hasNext()) {
                    try {
                        String key = (String) keys.next();

                        String taxVal = JsonObjParse.getValueFromJsonObj(joTax, key);

                        if (et_cgst.getText().toString().isEmpty()) {
                            //sgst = Double.valueOf(jo_tax.getString("SGST"));
                            //cgst = Double.valueOf(jo_tax.getString("CGST"));
                            cgst = Double.valueOf(taxVal);
                            et_cgst.setText(taxVal);
                            tvTaxField1.setText(key);
                            llCgst.setVisibility(View.VISIBLE);

                        } else {
                            sgst = Double.valueOf(taxVal);
                            et_sgst.setText(taxVal);
                            tvTaxField2.setText(key);
                            llSgst.setVisibility(View.VISIBLE);
                        }

                        Log.e("key", key);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        //et_cgst.setText(String.format("%.2f", cgst) + "");
        et_cgst.setText(Inad.getCurrencyDecimal(cgst, c) + "");
        //et_sgst.setText(String.format("%.2f", sgst) + "");
        et_sgst.setText(Inad.getCurrencyDecimal(sgst, c) + "");

        Double mrp = 0.0;
        if (!obj.getMrp().isEmpty()) mrp = Double.valueOf(obj.getMrp());

        doctor_template_type = JsonObjParse.getValueEmpty(obj.getInternal_json_data(), "doctor_template_type");
        Double Amrp = 0.0;
        if (!JsonObjParse.getValueEmpty(obj.getInternal_json_data(), "additionalPrice_rpd").isEmpty())
            Amrp = Double.valueOf(JsonObjParse.getValueEmpty(obj.getInternal_json_data(), "additionalPrice_rpd"));


        Double dis_mrp = Double.valueOf(getdiscountmrp(mrp, dis_perc, dis_price).toString());
        if (dis_mrp.doubleValue() != mrp.doubleValue()) {
            //tv_calc_dis.setText(currency + String.format("%.2f", dis_mrp));
            tv_calc_dis.setText(currency + Inad.getCurrencyDecimal(dis_mrp, c));
        } else {
            //tv_calc_dis.setVisibility(View.GONE);
        }
        //tv_calc_dis.setText(currency + String.format("%.2f", dis_mrp));
        tv_calc_dis.setText(currency + Inad.getCurrencyDecimal(mrp, c));
        tv_calc_Adis.setText(currency + Inad.getCurrencyDecimal(Amrp, c));

        stategst = (dis_mrp / 100.0f) * sgst;
        centralgst = (dis_mrp / 100.0f) * cgst;


        //et_mrp.setText(String.format("%.2f", mrp) + "");
        et_mrp.setText(Inad.getCurrencyDecimal(mrp, c) + "");
        et_Amrp.setText(Inad.getCurrencyDecimal(Amrp, c) + "");
        if (doctor_template_type.equalsIgnoreCase("RPD")||doctor_template_type.equalsIgnoreCase("Precision Attachment")) {
            llAdditionalPrice.setVisibility(View.VISIBLE);
        } else {
            llAdditionalPrice.setVisibility(View.GONE);
        }
        Double total = dis_mrp + stategst + centralgst;
        //tv_total.setText(currency + String.format("%.2f", total) + "" + "");
        tv_total.setText(currency + Inad.getCurrencyDecimal(total, c) + "" + "");


    }

    Double stategst = 0.0;
    Double centralgst = 0.0;

    @BindView(R.id.etStockPos)
    EditText etStockPos;

    @BindView(R.id.llStock)
    LinearLayout llStock;


    public void setAdditional(String availableStock) { // from user profile

        llStock.setVisibility(View.GONE);
        etStockPos.setText("");

        if (availableStock != null) {
            etStockPos.setText(availableStock + "");
        }

        if (userProfileData.field_json_data != null) {

            String show_stock_position = JsonObjParse.getValueEmpty(userProfileData.field_json_data, "show_stock_position");

            if (show_stock_position.equalsIgnoreCase("yes")) {
                llStock.setVisibility(View.GONE);
            } else {
                llStock.setVisibility(View.GONE);
                etStockPos.setText("");
            }
        }

    }

    @BindView(R.id.llQty)
    LinearLayout llQty;

    @BindView(R.id.et_qty)
    EditText et_qty;

    @BindView(R.id.tvAddTocart)
    TextView tvAddTocart;

    @BindView(R.id.tvDeleteFromCart)
    TextView tvDeleteFromCart;

    @BindView(R.id.tvPriceRangeWholesale)
    TextView tvPriceRangeWholesale;

    @BindView(R.id.tvQtyWholesale)
    TextView tvQtyWholesale;

    int qty = 1;

    String addUpdate = "Add ";

    public void checkWholeSale() {

        tvDeleteFromCart.setVisibility(View.GONE);

        if (obj.getCartcount() > 0) {
            addUpdate = "Edit ";
        }

        tvPriceRangeWholesale.setVisibility(View.GONE);
        tvQtyWholesale.setVisibility(View.GONE);

        // For property
        llPropertyFinancialBlock.setVisibility(View.GONE);
        llPropertyInfoBlock.setVisibility(View.GONE);
        llNotProperty.setVisibility(View.VISIBLE);

        // internal_json_data
        String wholesale = JsonObjParse.getValueEmpty(obj.getInternal_json_data(), "wholesale");
        String property = JsonObjParse.getValueEmpty(obj.getInternal_json_data(), "property");


        if (!wholesale.isEmpty() && isAdd) {


            tvPriceRangeWholesale.setVisibility(View.VISIBLE);
            tvQtyWholesale.setVisibility(View.VISIBLE);


            et_mrp.setEnabled(true);


            final String minQuantity = JsonObjParse.getValueEmpty(wholesale, "minQ");
            final String minPrice = JsonObjParse.getValueEmpty(wholesale, "minPrice");
            final String maxPrice = JsonObjParse.getValueEmpty(wholesale, "maxPrice");
            String minDay = JsonObjParse.getValueEmpty(wholesale, "minDay");
            String maxDay = JsonObjParse.getValueEmpty(wholesale, "maxDay");

            tvQtyWholesale.setText("Min. quantity : #" + minQuantity);


            if (!minPrice.isEmpty() && !maxPrice.isEmpty()) {
                Double minP = Double.valueOf(minPrice);
                Double maxP = Double.valueOf(maxPrice);
                tvPriceRangeWholesale.setText("Price range : " + currency + Inad.getCurrencyDecimal(minP, c) + "" + " - " + currency + Inad.getCurrencyDecimal(maxP, c));
            } else if (!minPrice.isEmpty()) {
                Double minP = Double.valueOf(minPrice);
                tvPriceRangeWholesale.setText("Minimum price : " + currency + Inad.getCurrencyDecimal(minP, c) + "");
            }


            et_qty.addTextChangedListener(this);


            llQty.setVisibility(View.GONE);
            if (obj.getCartcount() > 0) {
                tvDeleteFromCart.setVisibility(View.VISIBLE);
                et_mrp.setText(obj.getMrp());
            } else {
                et_mrp.setText(minPrice);
            }
            llQty.setVisibility(View.VISIBLE);
            if (obj.getCartcount() > 0) {
                et_qty.setText(obj.getCartcount() + "");
            } else {
                et_qty.setText(minQuantity);
            }


            tvAddTocart.setVisibility(View.VISIBLE);
            tvAddTocart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String p = et_mrp.getText().toString().trim();
                    String q = et_qty.getText().toString().trim();
                    int EnterQ = Integer.parseInt(q);
                    int minQ = Integer.parseInt(minQuantity);


                    Double EnterMrp = Double.valueOf(p);

                    Double minMrp = Double.valueOf(minPrice);
                    Double maxMrp = -1d;
                    if (!maxPrice.isEmpty()) {
                        maxMrp = Double.valueOf(maxPrice);
                    }


                    if (maxMrp > 0 && (EnterMrp < minMrp || EnterMrp > maxMrp)) {
                        Toast.makeText(c, "Please enter price in between range", Toast.LENGTH_LONG).show();
                    } else if (EnterMrp < minMrp) {
                        Toast.makeText(c, "Minimum price must be " + minPrice + " required", Toast.LENGTH_LONG).show();
                    } else if (EnterQ < minQ) {
                        Toast.makeText(c, "Please enter minimum " + minQuantity + " items", Toast.LENGTH_LONG).show();
                    } else {
                        obj.setMrp(p);
                        obj.setCartcount(EnterQ);
                        addWholesale.addItems(obj);
                        dismiss();
                    }


                }
            });

            tvDeleteFromCart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    addWholesale.deleteItems(obj);
                    dismiss();
                }
            });

        } else if (!property.isEmpty()) {


            //llDiscBlck.setVisibility(View.GONE);
            llPropertyFinancialBlock.setVisibility(View.VISIBLE);
            llPropertyInfoBlock.setVisibility(View.VISIBLE);
            llNotProperty.setVisibility(View.GONE);

            llQty.setVisibility(View.GONE);
            et_mrp.setEnabled(false);
            et_mrp.setBackground(null);
            tvAddTocart.setVisibility(View.GONE);
            tvDeleteFromCart.setVisibility(View.GONE);


            String proprty_sqft = JsonObjParse.getValueEmpty(property, "proprty_sqft");
            etSqFt.setText(proprty_sqft);

            String uds_sqft = JsonObjParse.getValueEmpty(property, "uds_sqft");
            etUds.setText(uds_sqft);

            String carpet_arae_sqft = JsonObjParse.getValueEmpty(property, "carpet_arae_sqft");
            etCarpetArea.setText(carpet_arae_sqft);

            String bed = JsonObjParse.getValueEmpty(property, "bed");
            etBr.setText(bed);

            String bath = JsonObjParse.getValueEmpty(property, "bath");
            etBath.setText(bath);

            String kitchen = JsonObjParse.getValueEmpty(property, "kitchen");
            etKitchen.setText(kitchen);

            try {
                String ExtraInfoPrice = JsonObjParse.getValueEmpty(property, "ExtraInfoPrice");
                setExtraPropertyInfo(ExtraInfoPrice);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            //calc_tax_value();


        } else {
            llQty.setVisibility(View.GONE);
            et_mrp.setEnabled(false);
            et_mrp.setBackground(null);
            tvAddTocart.setVisibility(View.GONE);
            tvDeleteFromCart.setVisibility(View.GONE);


        }

    }

    @BindView(R.id.llNotProperty)
    LinearLayout llNotProperty;

    @BindView(R.id.llPropertyFinancialBlock)
    LinearLayout llPropertyFinancialBlock;

    @BindView(R.id.llPropertyInfoBlock)
    LinearLayout llPropertyInfoBlock;

    @BindView(R.id.llWholesale)
    LinearLayout llWholesale;

    @BindView(R.id.etSqFt)
    TextView etSqFt;
    @BindView(R.id.etBr)
    TextView etBr;
    @BindView(R.id.etUds)
    TextView etUds;
    @BindView(R.id.etCarpetArea)
    TextView etCarpetArea;
    @BindView(R.id.etBath)
    TextView etBath;
    @BindView(R.id.etKitchen)
    TextView etKitchen;

    @BindView(R.id.etInfo1)
    TextView etInfo1;
    @BindView(R.id.tv_cur3)
    TextView tv_cur3;

    @BindView(R.id.etInfoVal1)
    TextView etInfoVal1;
    @BindView(R.id.etInfo2)
    TextView etInfo2;
    @BindView(R.id.tv_cur4)
    TextView tv_cur4;
    @BindView(R.id.etInfoVal2)
    TextView etInfoVal2;
    @BindView(R.id.etInfo3)
    TextView etInfo3;
    @BindView(R.id.tv_cur5)
    TextView tv_cur5;
    @BindView(R.id.etInfoVal3)
    TextView etInfoVal3;
    @BindView(R.id.etInfo4)
    TextView etInfo4;
    @BindView(R.id.tv_cur6)
    TextView tv_cur6;
    @BindView(R.id.etInfoVal4)
    TextView etInfoVal4;
    @BindView(R.id.etInfo5)
    TextView etInfo5;
    @BindView(R.id.tv_cur7)
    TextView tv_cur7;
    @BindView(R.id.etInfoVal5)
    TextView etInfoVal5;

    @BindView(R.id.rlInfo1)
    RelativeLayout rlInfo1;
    @BindView(R.id.rlInfo2)
    RelativeLayout rlInfo2;
    @BindView(R.id.rlInfo3)
    RelativeLayout rlInfo3;
    @BindView(R.id.rlInfo4)
    RelativeLayout rlInfo4;
    @BindView(R.id.rlInfo5)
    RelativeLayout rlInfo5;

    public void setExtraPropertyInfo(String extraInfo) throws JSONException {

        rlInfo1.setVisibility(View.GONE);
        rlInfo2.setVisibility(View.GONE);
        rlInfo3.setVisibility(View.GONE);
        rlInfo4.setVisibility(View.GONE);
        rlInfo5.setVisibility(View.GONE);

      /*  if (extraInfo != null && !extraInfo.isEmpty()) {
            JSONObject joExtra = new JSONObject(extraInfo);
            Iterator keys = joExtra.keys();
            int i = 0;
            while (keys.hasNext()) {
                try {
                    String key = (String) keys.next();
                    String info = JsonObjParse.getValueFromJsonObj(joExtra, key);
                    switch (i) {
                        case 0:
                            etInfo1.setText(key);
                            etInfoVal1.setText(info);
                            if (!key.isEmpty() || !info.isEmpty()) {
                                rlInfo1.setVisibility(View.VISIBLE);
                            }

                            break;
                        case 1:
                            etInfo2.setText(key);
                            etInfoVal2.setText(info);
                            if (!key.isEmpty() || !info.isEmpty()) {
                                rlInfo2.setVisibility(View.VISIBLE);
                            }

                            break;
                        case 2:
                            etInfo3.setText(key);
                            etInfoVal3.setText(info);
                            rlInfo3.setVisibility(View.VISIBLE);
                            if (!key.isEmpty() || !info.isEmpty()) {
                                rlInfo3.setVisibility(View.VISIBLE);
                            }

                            break;
                        case 3:
                            etInfo4.setText(key);
                            etInfoVal4.setText(info);
                            rlInfo4.setVisibility(View.VISIBLE);

                            if (!key.isEmpty() || !info.isEmpty()) {
                                rlInfo4.setVisibility(View.VISIBLE);
                            }

                            break;
                        case 4:
                            etInfo5.setText(key);
                            etInfoVal5.setText(info);
                            rlInfo5.setVisibility(View.VISIBLE);

                            if (!key.isEmpty() || !info.isEmpty()) {
                                rlInfo5.setVisibility(View.VISIBLE);
                            }

                            break;
                    }
                    i++;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }*/

        if (extraInfo != null && !extraInfo.isEmpty()) {

            JSONArray jaExtraInfo = new JSONArray(extraInfo);

            for (int i = 0; i < jaExtraInfo.length(); i++) {
                JSONObject jsonObject = jaExtraInfo.getJSONObject(i);

                Iterator keys = jsonObject.keys();
                while (keys.hasNext()) {
                    try {
                        String key = (String) keys.next();
                        String info = JsonObjParse.getValueFromJsonObj(jsonObject, key);
                        switch (i) {
                            case 0:
                                etInfo1.setText(key);
                                etInfoVal1.setText(info);

                                if (!info.isEmpty()) {
                                    rlInfo1.setVisibility(View.VISIBLE);
                                }


                                break;
                            case 1:
                                if (!info.isEmpty()) {
                                    rlInfo2.setVisibility(View.VISIBLE);
                                }

                                etInfo2.setText(key);
                                etInfoVal2.setText(info);
                                break;
                            case 2:
                                if (!info.isEmpty()) {
                                    rlInfo3.setVisibility(View.VISIBLE);
                                }

                                etInfo3.setText(key);
                                etInfoVal3.setText(info);
                                break;
                            case 3:
                                if (!info.isEmpty()) {
                                    rlInfo4.setVisibility(View.VISIBLE);
                                }

                                etInfo4.setText(key);
                                etInfoVal4.setText(info);
                                break;
                            case 4:
                                if (!info.isEmpty()) {
                                    rlInfo5.setVisibility(View.VISIBLE);
                                }

                                etInfo5.setText(key);
                                etInfoVal5.setText(info);
                                break;
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }
        }


    }

    // Material

    String jewel = "", metal = "gold", weight = "";
    int metalPosId = 0;
    @BindView(R.id.rvMetals)
    RecyclerView rvMetals;

    @BindView(R.id.llJewelInfo)
    LinearLayout llJewelInfo;

    @BindView(R.id.etJewelWeight)
    EditText etJewelWeight;

    @BindView(R.id.et_ded_val)
    EditText et_ded_val;

    @BindView(R.id.tv_cur_11)
    TextView tv_cur_11;

    @BindView(R.id.et_ded_per)
    EditText et_ded_per;

    @BindView(R.id.et_additional_val)
    EditText et_additional_val;

    @BindView(R.id.tv_cur_12)
    TextView tv_cur_12;

    @BindView(R.id.txtDedPriceCs)
    TextView txtDedPriceCs;


    @BindView(R.id.txtDedOrgPriceCs)
    TextView txtDedOrgPriceCs;

    @BindView(R.id.txtAddOrgPriceCs)
    TextView txtAddOrgPriceCs;

    @BindView(R.id.txtAddPriceCs)
    TextView txtAddPriceCs;

    @BindView(R.id.tvTotalDed)
    TextView tvTotalDed;

    @BindView(R.id.tvTotalAdditional)
    TextView tvTotalAdditional;

    @BindView(R.id.et_additional_per)
    EditText et_additional_per;

    MetalListAdapter metalListAdapter;
    ArrayList<Metal> alMetal = new ArrayList<>();

    @BindView(R.id.txtWeight)
    TextView txtWeight;

    public void checkjewel() {
        alMetal = new ArrayList<>();
        llJewelInfo.setVisibility(View.GONE);

        jewel = JsonObjParse.getValueEmpty(obj.getInternal_json_data(), "jewel");

        metal = JsonObjParse.getValueEmpty(jewel, "metal");

        weight = JsonObjParse.getValueEmpty(jewel, "weight");
        etJewelWeight.setText(weight);

        String ded_perc = JsonObjParse.getValueEmpty(jewel, "ded_perc");
        et_ded_per.setText(ded_perc);

        String ded_val = JsonObjParse.getValueEmpty(jewel, "ded_val");
        et_ded_val.setText(ded_val);

        String additional_perc = JsonObjParse.getValueEmpty(jewel, "additional_perc");
        et_additional_per.setText(additional_perc);

        String additional_val = JsonObjParse.getValueEmpty(jewel, "additional_val");
        et_additional_val.setText(additional_val);


        if (!jewel.isEmpty())// edit , copy
        {
            et_mrp.setEnabled(false);
            et_mrp.setBackground(null);
            llJewelInfo.setVisibility(View.VISIBLE);
        }

        if (llJewelInfo.getVisibility() == View.VISIBLE) {

            etJewelWeight.addTextChangedListener(this);
            et_ded_per.addTextChangedListener(this);
            et_ded_val.addTextChangedListener(this);
            et_additional_per.addTextChangedListener(this);
            et_additional_val.addTextChangedListener(this);

            alMetal.addAll(ShareModel.getI().metal.alMetal);


            try {
                String DeductionJewelPrice = JsonObjParse.getValueEmpty(jewel, "DeductionJewelPrice");
                setDeductionJewelInfo(DeductionJewelPrice);

                String AdditionalJewelPrice = JsonObjParse.getValueEmpty(jewel, "AdditionalJewelPrice");
                setAdditionalJewelInfo(AdditionalJewelPrice);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            //calc_tax_value();
        }


        metalListAdapter = new MetalListAdapter(alMetal, c, new MetalListAdapter.OnItemClickListener() {
            @Override
            public void onDelete(int item) {

            }

            @Override
            public void onEdit(int item) {


                for (int i = 0; i < alMetal.size(); i++) {
                    alMetal.get(i).isSelect = false;
                }

                alMetal.get(item).isSelect = true;

                metal = alMetal.get(item).metalNm;
                metalPosId = item;

                et_mrp.setText(alMetal.get(item).metalPrice);

                metalListAdapter.setItem(alMetal);
            }
        }, currency);

        rvMetals.setLayoutManager(new LinearLayoutManager(c, LinearLayoutManager.HORIZONTAL, false));
        //rvMetals.setLayoutManager(new GridLayoutManager(c, 2));
        rvMetals.setAdapter(metalListAdapter);
        rvMetals.setNestedScrollingEnabled(false);


        String metalPos = JsonObjParse.getValueEmpty(jewel, "metalPosId");
        metalPosId = 0;
        if (!metalPos.isEmpty()) metalPosId = Integer.parseInt(metalPos);

        for (int i = 0; i < alMetal.size(); i++) {
            alMetal.get(i).isSelect = false;
        }

        if (alMetal.size() > metalPosId) {
            et_mrp.setText(alMetal.get(metalPosId).metalPrice);
            alMetal.get(metalPosId).isSelect = true;
            txtWeight.setText("Weight (" + alMetal.get(metalPosId).metalNm + " in " + alMetal.get(metalPosId).unit + ")");
        }
        metalListAdapter.setItem(alMetal);
    }


    /// Deductions for jewellery


    @BindView(R.id.etDedInfo1)
    EditText etDedInfo1;

    @BindView(R.id.etOrgDedPerc1)
    EditText etOrgDedPerc1;

    @BindView(R.id.etActDedPerc1)
    EditText etActDedPerc1;

    @BindView(R.id.etOrgDedPrice1)
    EditText etOrgDedPrice1;

    @BindView(R.id.etActDedPrice1)
    EditText etActDedPrice1;

    //

    @BindView(R.id.etDedInfo2)
    EditText etDedInfo2;

    @BindView(R.id.etOrgDedPerc2)
    EditText etOrgDedPerc2;

    @BindView(R.id.etActDedPerc2)
    EditText etActDedPerc2;

    @BindView(R.id.etOrgDedPrice2)
    EditText etOrgDedPrice2;

    @BindView(R.id.etActDedPrice2)
    EditText etActDedPrice2;

    //

    @BindView(R.id.etDedInfo3)
    EditText etDedInfo3;

    @BindView(R.id.etOrgDedPerc3)
    EditText etOrgDedPerc3;

    @BindView(R.id.etActDedPerc3)
    EditText etActDedPerc3;

    @BindView(R.id.etOrgDedPrice3)
    EditText etOrgDedPrice3;

    @BindView(R.id.etActDedPrice3)
    EditText etActDedPrice3;

    //

    @BindView(R.id.etDedInfo4)
    EditText etDedInfo4;

    @BindView(R.id.etOrgDedPerc4)
    EditText etOrgDedPerc4;

    @BindView(R.id.etActDedPerc4)
    EditText etActDedPerc4;

    @BindView(R.id.etOrgDedPrice4)
    EditText etOrgDedPrice4;

    @BindView(R.id.etActDedPrice4)
    EditText etActDedPrice4;

    //

    @BindView(R.id.etDedInfo5)
    EditText etDedInfo5;

    @BindView(R.id.etOrgDedPerc5)
    EditText etOrgDedPerc5;

    @BindView(R.id.etActDedPerc5)
    EditText etActDedPerc5;

    @BindView(R.id.etOrgDedPrice5)
    EditText etOrgDedPrice5;

    @BindView(R.id.etActDedPrice5)
    EditText etActDedPrice5;

    //

    @BindView(R.id.llDedInfoPrice)
    LinearLayout llDedInfoPrice;

    @BindView(R.id.llDedShowHideClk)
    LinearLayout llDedShowHideClk;

    @BindView(R.id.ivDownLlDedInfo)
    ImageView ivDownLlDedInfo;

    @OnClick(R.id.llDedShowHideClk)
    public void llDedShowHideClk(View v) {
        if (llDedInfoPrice.getVisibility() == View.VISIBLE) {
            ivDownLlDedInfo.setRotation(270);
            llDedInfoPrice.setVisibility(View.GONE);
        } else {
            ivDownLlDedInfo.setRotation(0);
            llDedInfoPrice.setVisibility(View.VISIBLE);
        }
    }

    public void setDeductionJewelInfo(String deductionInfo) throws JSONException {
        if (deductionInfo != null && !deductionInfo.isEmpty()) {

            JSONArray jaExtraInfo = new JSONArray(deductionInfo);

            for (int i = 0; i < jaExtraInfo.length(); i++) {
                JSONObject jsonObject = jaExtraInfo.getJSONObject(i);

                Iterator keys = jsonObject.keys();
                while (keys.hasNext()) {
                    try {
                        String key = (String) keys.next();
                        String info = JsonObjParse.getValueFromJsonObj(jsonObject, key);
                        switch (i) {
                            case 0:
                                etDedInfo1.setText(key);
                                etOrgDedPerc1.setText(JsonObjParse.getValueEmpty(info, "org_perc") + "");
                                etActDedPerc1.setText(JsonObjParse.getValueEmpty(info, "act_perc") + "");
                                etOrgDedPrice1.setText(JsonObjParse.getValueEmpty(info, "org_price") + "");
                                etActDedPrice1.setText(JsonObjParse.getValueEmpty(info, "act_price") + "");
                                break;
                            case 1:
                                etDedInfo2.setText(key);
                                etOrgDedPerc2.setText(JsonObjParse.getValueEmpty(info, "org_perc") + "");
                                etActDedPerc2.setText(JsonObjParse.getValueEmpty(info, "act_perc") + "");
                                etOrgDedPrice2.setText(JsonObjParse.getValueEmpty(info, "org_price") + "");
                                etActDedPrice2.setText(JsonObjParse.getValueEmpty(info, "act_price") + "");
                                break;
                            case 2:
                                etDedInfo3.setText(key);
                                etOrgDedPerc3.setText(JsonObjParse.getValueEmpty(info, "org_perc") + "");
                                etActDedPerc3.setText(JsonObjParse.getValueEmpty(info, "act_perc") + "");
                                etOrgDedPrice3.setText(JsonObjParse.getValueEmpty(info, "org_price") + "");
                                etActDedPrice3.setText(JsonObjParse.getValueEmpty(info, "act_price") + "");
                                break;
                            case 3:
                                etDedInfo4.setText(key);
                                etOrgDedPerc4.setText(JsonObjParse.getValueEmpty(info, "org_perc") + "");
                                etActDedPerc4.setText(JsonObjParse.getValueEmpty(info, "act_perc") + "");
                                etOrgDedPrice4.setText(JsonObjParse.getValueEmpty(info, "org_price") + "");
                                etActDedPrice4.setText(JsonObjParse.getValueEmpty(info, "act_price") + "");
                                break;
                            case 4:
                                etDedInfo5.setText(key);
                                etOrgDedPerc5.setText(JsonObjParse.getValueEmpty(info, "org_perc") + "");
                                etActDedPerc5.setText(JsonObjParse.getValueEmpty(info, "act_perc") + "");
                                etOrgDedPrice5.setText(JsonObjParse.getValueEmpty(info, "org_price") + "");
                                etActDedPrice5.setText(JsonObjParse.getValueEmpty(info, "act_price") + "");
                                break;
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }
        }

        etActDedPrice1.addTextChangedListener(this);
        etActDedPrice2.addTextChangedListener(this);
        etActDedPrice3.addTextChangedListener(this);
        etActDedPrice4.addTextChangedListener(this);
        etActDedPrice5.addTextChangedListener(this);
        etOrgDedPrice1.addTextChangedListener(this);
        etOrgDedPrice2.addTextChangedListener(this);
        etOrgDedPrice3.addTextChangedListener(this);
        etOrgDedPrice4.addTextChangedListener(this);
        etOrgDedPrice5.addTextChangedListener(this);
        etOrgDedPerc1.addTextChangedListener(this);
        etOrgDedPerc2.addTextChangedListener(this);
        etOrgDedPerc3.addTextChangedListener(this);
        etOrgDedPerc4.addTextChangedListener(this);
        etOrgDedPerc5.addTextChangedListener(this);
        etActDedPerc1.addTextChangedListener(this);
        etActDedPerc2.addTextChangedListener(this);
        etActDedPerc3.addTextChangedListener(this);
        etActDedPerc4.addTextChangedListener(this);
        etActDedPerc5.addTextChangedListener(this);

        disableEt(etJewelWeight);
        disableEt(etDedInfo1);
        disableEt(etDedInfo2);
        disableEt(etDedInfo3);
        disableEt(etDedInfo4);
        disableEt(etDedInfo5);
        disableEt(etAddInfo1);
        disableEt(etAddInfo2);
        disableEt(etAddInfo3);
        disableEt(etAddInfo4);
        disableEt(etAddInfo5);

        disableEt(etActDedPrice1);
        disableEt(etActDedPrice2);
        disableEt(etActDedPrice3);
        disableEt(etActDedPrice4);
        disableEt(etActDedPrice5);
        disableEt(etOrgDedPrice1);
        disableEt(etOrgDedPrice2);
        disableEt(etOrgDedPrice3);
        disableEt(etOrgDedPrice4);
        disableEt(etOrgDedPrice5);
        disableEt(etOrgDedPerc1);
        disableEt(etOrgDedPerc2);
        disableEt(etOrgDedPerc3);
        disableEt(etOrgDedPerc4);
        disableEt(etOrgDedPerc5);
        disableEt(etActDedPerc1);
        disableEt(etActDedPerc2);
        disableEt(etActDedPerc3);
        disableEt(etActDedPerc4);
        disableEt(etActDedPerc5);


    }

    /// Additional for jewellery


    // Additional

    @BindView(R.id.etAddInfo1)
    EditText etAddInfo1;

    @BindView(R.id.etOrgAddPerc1)
    EditText etOrgAddPerc1;

    @BindView(R.id.etActAddPerc1)
    EditText etActAddPerc1;

    @BindView(R.id.etOrgAddPrice1)
    EditText etOrgAddPrice1;

    @BindView(R.id.etActAddPrice1)
    EditText etActAddPrice1;

    //

    @BindView(R.id.etAddInfo2)
    EditText etAddInfo2;

    @BindView(R.id.etOrgAddPerc2)
    EditText etOrgAddPerc2;

    @BindView(R.id.etActAddPerc2)
    EditText etActAddPerc2;

    @BindView(R.id.etOrgAddPrice2)
    EditText etOrgAddPrice2;

    @BindView(R.id.etActAddPrice2)
    EditText etActAddPrice2;

    //

    @BindView(R.id.etAddInfo3)
    EditText etAddInfo3;

    @BindView(R.id.etOrgAddPerc3)
    EditText etOrgAddPerc3;

    @BindView(R.id.etActAddPerc3)
    EditText etActAddPerc3;

    @BindView(R.id.etOrgAddPrice3)
    EditText etOrgAddPrice3;

    @BindView(R.id.etActAddPrice3)
    EditText etActAddPrice3;

    //

    @BindView(R.id.etAddInfo4)
    EditText etAddInfo4;

    @BindView(R.id.etOrgAddPerc4)
    EditText etOrgAddPerc4;

    @BindView(R.id.etActAddPerc4)
    EditText etActAddPerc4;

    @BindView(R.id.etOrgAddPrice4)
    EditText etOrgAddPrice4;

    @BindView(R.id.etActAddPrice4)
    EditText etActAddPrice4;

    //

    @BindView(R.id.etAddInfo5)
    EditText etAddInfo5;

    @BindView(R.id.etOrgAddPerc5)
    EditText etOrgAddPerc5;

    @BindView(R.id.etActAddPerc5)
    EditText etActAddPerc5;

    @BindView(R.id.etOrgAddPrice5)
    EditText etOrgAddPrice5;

    @BindView(R.id.etActAddPrice5)
    EditText etActAddPrice5;

    //

    @BindView(R.id.llAddInfoPrice)
    LinearLayout llAddInfoPrice;

    @BindView(R.id.llAddShowHideClk)
    LinearLayout llAddShowHideClk;

    @BindView(R.id.ivDownLlAddInfo)
    ImageView ivDownLlAddInfo;

    @OnClick(R.id.llAddShowHideClk)
    public void llAddShowHideClk(View v) {
        if (llAddInfoPrice.getVisibility() == View.VISIBLE) {
            ivDownLlAddInfo.setRotation(270);
            llAddInfoPrice.setVisibility(View.GONE);
        } else {
            ivDownLlAddInfo.setRotation(0);
            llAddInfoPrice.setVisibility(View.VISIBLE);
        }
    }

    public void setAdditionalJewelInfo(String deductionInfo) throws JSONException {
        if (deductionInfo != null && !deductionInfo.isEmpty()) {

            JSONArray jaExtraInfo = new JSONArray(deductionInfo);

            for (int i = 0; i < jaExtraInfo.length(); i++) {
                JSONObject jsonObject = jaExtraInfo.getJSONObject(i);

                Iterator keys = jsonObject.keys();
                while (keys.hasNext()) {
                    try {
                        String key = (String) keys.next();
                        String info = JsonObjParse.getValueFromJsonObj(jsonObject, key);

                        switch (i) {
                            case 0:
                                etAddInfo1.setText(key);
                                etOrgAddPerc1.setText(JsonObjParse.getValueEmpty(info, "org_perc") + "");
                                etActAddPerc1.setText(JsonObjParse.getValueEmpty(info, "act_perc") + "");
                                etOrgAddPrice1.setText(JsonObjParse.getValueEmpty(info, "org_price") + "");
                                etActAddPrice1.setText(JsonObjParse.getValueEmpty(info, "act_price") + "");
                                break;
                            case 1:
                                etAddInfo2.setText(key);
                                etOrgAddPerc2.setText(JsonObjParse.getValueEmpty(info, "org_perc") + "");
                                etActAddPerc2.setText(JsonObjParse.getValueEmpty(info, "act_perc") + "");
                                etOrgAddPrice2.setText(JsonObjParse.getValueEmpty(info, "org_price") + "");
                                etActAddPrice2.setText(JsonObjParse.getValueEmpty(info, "act_price") + "");
                                break;
                            case 2:
                                etAddInfo3.setText(key);
                                etOrgAddPerc3.setText(JsonObjParse.getValueEmpty(info, "org_perc") + "");
                                etActAddPerc3.setText(JsonObjParse.getValueEmpty(info, "act_perc") + "");
                                etOrgAddPrice3.setText(JsonObjParse.getValueEmpty(info, "org_price") + "");
                                etActAddPrice3.setText(JsonObjParse.getValueEmpty(info, "act_price") + "");
                                break;
                            case 3:
                                etAddInfo4.setText(key);
                                etOrgAddPerc4.setText(JsonObjParse.getValueEmpty(info, "org_perc") + "");
                                etActAddPerc4.setText(JsonObjParse.getValueEmpty(info, "act_perc") + "");
                                etOrgAddPrice4.setText(JsonObjParse.getValueEmpty(info, "org_price") + "");
                                etActAddPrice4.setText(JsonObjParse.getValueEmpty(info, "act_price") + "");
                                break;
                            case 4:
                                etAddInfo5.setText(key);
                                etOrgAddPerc5.setText(JsonObjParse.getValueEmpty(info, "org_perc") + "");
                                etActAddPerc5.setText(JsonObjParse.getValueEmpty(info, "act_perc") + "");
                                etOrgAddPrice5.setText(JsonObjParse.getValueEmpty(info, "org_price") + "");
                                etActAddPrice5.setText(JsonObjParse.getValueEmpty(info, "act_price") + "");
                                break;
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }
        }

        etActAddPrice1.addTextChangedListener(this);
        etActAddPrice2.addTextChangedListener(this);
        etActAddPrice3.addTextChangedListener(this);
        etActAddPrice4.addTextChangedListener(this);
        etActAddPrice5.addTextChangedListener(this);
        etOrgAddPrice1.addTextChangedListener(this);
        etOrgAddPrice2.addTextChangedListener(this);
        etOrgAddPrice3.addTextChangedListener(this);
        etOrgAddPrice4.addTextChangedListener(this);
        etOrgAddPrice5.addTextChangedListener(this);
        etOrgAddPerc1.addTextChangedListener(this);
        etOrgAddPerc2.addTextChangedListener(this);
        etOrgAddPerc3.addTextChangedListener(this);
        etOrgAddPerc4.addTextChangedListener(this);
        etOrgAddPerc5.addTextChangedListener(this);
        etActAddPerc1.addTextChangedListener(this);
        etActAddPerc2.addTextChangedListener(this);
        etActAddPerc3.addTextChangedListener(this);
        etActAddPerc4.addTextChangedListener(this);
        etActAddPerc5.addTextChangedListener(this);

        disableEt(etActAddPrice1);
        disableEt(etActAddPrice2);
        disableEt(etActAddPrice3);
        disableEt(etActAddPrice4);
        disableEt(etActAddPrice5);
        disableEt(etOrgAddPrice1);
        disableEt(etOrgAddPrice2);
        disableEt(etOrgAddPrice3);
        disableEt(etOrgAddPrice4);
        disableEt(etOrgAddPrice5);
        disableEt(etOrgAddPerc1);
        disableEt(etOrgAddPerc2);
        disableEt(etOrgAddPerc3);
        disableEt(etOrgAddPerc4);
        disableEt(etOrgAddPerc5);
        disableEt(etActAddPerc1);
        disableEt(etActAddPerc2);
        disableEt(etActAddPerc3);
        disableEt(etActAddPerc4);
        disableEt(etActAddPerc5);

    }

    public Double calcValuesOfAddDed(Double mrp, String act_perc, String org_perc, String act_price, String org_price) {

        Double dedPriceForJewel = 0.0;

        if (!act_perc.isEmpty()) {
            Double ded1Val = Double.valueOf(act_perc);
            dedPriceForJewel = dedPriceForJewel + Double.valueOf(getValues(mrp, ded1Val));
        } else if (!org_perc.isEmpty()) {
            Double ded1Val = Double.valueOf(org_perc);
            dedPriceForJewel = dedPriceForJewel + Double.valueOf(getValues(mrp, ded1Val));
        } else if (!act_price.isEmpty()) {
            Double ded1Val = Double.valueOf(act_price);
            dedPriceForJewel = dedPriceForJewel + ded1Val;
        } else if (!org_price.isEmpty()) {
            Double ded1Val = Double.valueOf(org_price);
            dedPriceForJewel = dedPriceForJewel + ded1Val;
        }

        return dedPriceForJewel;
    }


    public String getValues(Double mrpprice, Double discount_percentage) {
        Double discountprice = 0.0;

        if (discount_percentage > 0) {
            if (mrpprice > 0) {
                Double totalDisc = (mrpprice * (discount_percentage / 100));
                discountprice = totalDisc;
            }
        }

        return String.valueOf(discountprice);
    }

    public String getValues(Double mrpprice, Double discount_percentage, Double discounted_price) {
        Double discountprice = 0.0;

        if (discount_percentage > 0) {
            if (mrpprice > 0) {
                Double totalDisc = (mrpprice * (discount_percentage / 100));
                discountprice = totalDisc;
            }
        } else if (discounted_price > 0) {
            discountprice = discounted_price;
        }

        return String.valueOf(discountprice);
    }

    public void disableEt(EditText editText) {
        editText.setBackground(null);
        editText.setEnabled(false);
    }

    @BindView(R.id.tvTotalDiscPerc)
    TextView tvTotalDiscPerc;

    @BindView(R.id.tvTotalWoTax)
    TextView tvTotalWoTax;


    public void strikeLineIfDiscValuew(TextView textView) {
        textView.setPaintFlags(textView.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
    }

    public void removeStrikeLineIfNoDiscValuew(TextView textView) {
        textView.setPaintFlags(textView.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
    }


}
