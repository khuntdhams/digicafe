package com.cab.digicafe.Activities;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.cab.digicafe.Database.SettingDB;
import com.cab.digicafe.Database.SettingModel;
import com.cab.digicafe.Dialogbox.AddAddress;
import com.cab.digicafe.Helper.SessionManager;
import com.cab.digicafe.R;
import com.cab.digicafe.Rest.ApiClient;
import com.cab.digicafe.Rest.ApiInterface;
import com.google.gson.JsonObject;
import com.zcw.togglebutton.ToggleButton;

import org.json.JSONObject;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SettingsActivity extends MyAppBaseActivity {


    @BindView(R.id.tb_date)
    ToggleButton tb_date;

    @BindView(R.id.tb_add)
    ToggleButton tb_add;

    @BindView(R.id.tb_remark)
    ToggleButton tb_remark;

    @BindView(R.id.tb_google)
    ToggleButton tb_google;

    @BindView(R.id.tb_payu)
    ToggleButton tb_payu;

    @BindView(R.id.tb_receipt)
    ToggleButton tb_receipt;

    @BindView(R.id.tb_ismytask)
    ToggleButton tb_ismytask;

    @BindView(R.id.tb_isalltask)
    ToggleButton tb_isalltask;

    @BindView(R.id.tb_search)
    ToggleButton tb_search;

    @BindView(R.id.tb_concatoffer)
    ToggleButton tb_concatoffer;

    @BindView(R.id.tb_printreceipt)
    ToggleButton tb_printreceipt;

    @BindView(R.id.tb_printtoken)
    ToggleButton tb_printtoken;

    @BindView(R.id.tb_printdatadisplay)
    ToggleButton tb_printdatadisplay;

    @BindView(R.id.tb_imgdetect)
    ToggleButton tb_imgdetect;

    @BindView(R.id.tv_openclose)
    TextView tv_openclose;
    SessionManager sessionManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);


        ButterKnife.bind(this);

        final SettingDB db = new SettingDB(this);
        sessionManager = new SessionManager(this);
        shop_status = sessionManager.getbusi_status();
        tv_openclose.setText(shop_status);

        if (sessionManager.getcurrentu_nm().matches("[0-9.]*")) { // for customer
            tv_openclose.setVisibility(View.GONE);
        }
        tv_openclose.setVisibility(View.GONE);

        List<SettingModel> list_setting = db.GetItems();
        SettingModel sm = new SettingModel();
        sm = list_setting.get(0);

        tb_imgdetect.setOnToggleChanged(new ToggleButton.OnToggleChanged() {
            @Override
            public void onToggle(boolean on) {
                if (on) {
                    db.UpdateItem("isvisiondetect", "1");
                } else {
                    db.UpdateItem("isvisiondetect", "0");
                }

            }
        });


        tb_concatoffer.setOnToggleChanged(new ToggleButton.OnToggleChanged() {
            @Override
            public void onToggle(boolean on) {
                if (on) {
                    db.UpdateItem("isconcateoffer", "1");
                } else {
                    db.UpdateItem("isconcateoffer", "0");
                }

            }
        });

        tb_date.setOnToggleChanged(new ToggleButton.OnToggleChanged() {
            @Override
            public void onToggle(boolean on) {
                if (on) {
                    db.UpdateItem("isdate", "1");
                } else {
                    db.UpdateItem("isdate", "0");
                }

            }
        });

        tb_add.setOnToggleChanged(new ToggleButton.OnToggleChanged() {
            @Override
            public void onToggle(boolean on) {
                if (on) {
                    db.UpdateItem("isaddress", "1");
                } else {
                    db.UpdateItem("isaddress", "0");
                }

            }
        });

        tb_remark.setOnToggleChanged(new ToggleButton.OnToggleChanged() {
            @Override
            public void onToggle(boolean on) {
                if (on) {
                    db.UpdateItem("isremark", "1");
                } else {
                    db.UpdateItem("isremark", "0");
                }

            }
        });

        tb_payu.setOnToggleChanged(new ToggleButton.OnToggleChanged() {
            @Override
            public void onToggle(boolean on) {
                if (on) {
                    db.UpdateItem("ispayumoney", "1");
                } else {
                    db.UpdateItem("ispayumoney", "0");
                }

            }
        });

        tb_google.setOnToggleChanged(new ToggleButton.OnToggleChanged() {
            @Override
            public void onToggle(boolean on) {
                if (on) {
                    db.UpdateItem("isgoogleplace", "1");
                } else {
                    db.UpdateItem("isgoogleplace", "0");
                }

            }
        });

        tb_receipt.setOnToggleChanged(new ToggleButton.OnToggleChanged() {
            @Override
            public void onToggle(boolean on) {
                if (on) {
                    db.UpdateItem("isreceipt", "1");
                } else {
                    db.UpdateItem("isreceipt", "0");
                }

            }
        });

        tb_ismytask.setOnToggleChanged(new ToggleButton.OnToggleChanged() {
            @Override
            public void onToggle(boolean on) {
                if (on) {
                    db.UpdateItem("ismytask", "1");
                } else {
                    db.UpdateItem("ismytask", "0");
                }

            }
        });

        tb_isalltask.setOnToggleChanged(new ToggleButton.OnToggleChanged() {
            @Override
            public void onToggle(boolean on) {
                if (on) {
                    db.UpdateItem("isalltask", "1");
                } else {
                    db.UpdateItem("isalltask", "0");
                }

            }
        });


        tb_search.setOnToggleChanged(new ToggleButton.OnToggleChanged() {
            @Override
            public void onToggle(boolean on) {
                if (on) {
                    db.UpdateItem("issearchalltask", "1");
                } else {
                    db.UpdateItem("issearchalltask", "0");
                }

            }
        });


        tb_printreceipt.setOnToggleChanged(new ToggleButton.OnToggleChanged() {
            @Override
            public void onToggle(boolean on) {
                if (on) {
                    db.UpdateItem("isprintreceipt", "1");
                } else {
                    db.UpdateItem("isprintreceipt", "0");
                }

            }
        });

        tb_printtoken.setOnToggleChanged(new ToggleButton.OnToggleChanged() {
            @Override
            public void onToggle(boolean on) {
                if (on) {
                    db.UpdateItem("isprinttoken", "1");
                } else {
                    db.UpdateItem("isprinttoken", "0");
                }

            }
        });


        tb_printdatadisplay.setOnToggleChanged(new ToggleButton.OnToggleChanged() {
            @Override
            public void onToggle(boolean on) {
                if (on) {
                    db.UpdateItem("isprintdisplaydata", "1");
                } else {
                    db.UpdateItem("isprintdisplaydata", "0");
                }

            }
        });


        if (sm.getIsdate().equals("1")) {
            tb_date.setToggleOn();
        } else {
            tb_date.setToggleOff();
        }

        if (sm.getIsaddress().equals("1")) {
            tb_add.setToggleOn();
        } else {
            tb_add.setToggleOff();
        }

        if (sm.getIsremark().equals("1")) {
            tb_remark.setToggleOn();
        } else {
            tb_remark.setToggleOff();
        }

        if (sm.getIspayumoney().equals("1")) {
            tb_payu.setToggleOn();
        } else {
            tb_payu.setToggleOff();
        }

        if (sm.getIsreceipt().equals("1")) {
            tb_receipt.setToggleOn();
        } else {
            tb_receipt.setToggleOff();
        }

        if (sm.getIsmytask().equals("1")) {
            tb_ismytask.setToggleOn();
        } else {
            tb_ismytask.setToggleOff();
        }

        if (sm.getIsgoogleplace().equals("1")) {
            tb_google.setToggleOn();
        } else {
            tb_google.setToggleOff();
        }

        if (sm.getIsalltask().equals("1")) {
            tb_isalltask.setToggleOn();
        } else {
            tb_isalltask.setToggleOff();
        }

        if (sm.getIssearchalltask().equals("1")) {
            tb_search.setToggleOn();
        } else {
            tb_search.setToggleOff();
        }

        if (sm.getIsconcateoffer().equals("1")) {
            tb_concatoffer.setToggleOn();
        } else {
            tb_concatoffer.setToggleOff();
        }


        if (sm.getIsprintreceipt().equals("1")) {
            tb_printreceipt.setToggleOn();
        } else {
            tb_printreceipt.setToggleOff();
        }

        if (sm.getIsprinttoken().equals("1")) {
            tb_printtoken.setToggleOn();
        } else {
            tb_printtoken.setToggleOff();
        }


        if (sm.getIsprintdisplaydata().equals("1")) {
            tb_printdatadisplay.setToggleOn();
        } else {
            tb_printdatadisplay.setToggleOff();
        }

        if (sm.getIsvisiondetect().equals("1")) {
            tb_imgdetect.setToggleOn();
        } else {
            tb_imgdetect.setToggleOff();
        }


        findViewById(R.id.ll_receipt).setVisibility(View.GONE);

        if (sessionManager.getcurrentu_nm().contains("@") || sessionManager.getisBusinesslogic())  // employee
        {
            findViewById(R.id.ll_mytask).setVisibility(View.VISIBLE);
            findViewById(R.id.ll_alltask).setVisibility(View.VISIBLE);
            findViewById(R.id.ll_receipt).setVisibility(View.GONE);
            findViewById(R.id.ll_search).setVisibility(View.VISIBLE);
            findViewById(R.id.ll_printreceipt).setVisibility(View.VISIBLE);
            findViewById(R.id.ll_printtoken).setVisibility(View.VISIBLE);
            findViewById(R.id.ll_printdatafromsearch).setVisibility(View.VISIBLE);
            findViewById(R.id.ll_img_detect).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.ll_mytask).setVisibility(View.GONE);
            findViewById(R.id.ll_alltask).setVisibility(View.GONE);
            findViewById(R.id.ll_search).setVisibility(View.GONE);
            //findViewById(R.id.ll_receipt).setVisibility(View.VISIBLE);
            findViewById(R.id.ll_printreceipt).setVisibility(View.GONE);
            findViewById(R.id.ll_printtoken).setVisibility(View.GONE);
            findViewById(R.id.ll_printdatafromsearch).setVisibility(View.GONE);
            findViewById(R.id.ll_img_detect).setVisibility(View.GONE);

        }


        tv_openclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               /* new CustomDialogClass(SettingsActivity.this, new CustomDialogClass.OnDialogClickListener() {
                    @Override
                    public void onDialogImageRunClick(int positon) {
                        if (positon == 0) {

                        } else if (positon == 1) {
                            //str_actionid = str_actionid.replaceAll(",$", "");






                        }
                    }
                }, "You are changing the status!").show();*/


                new AddAddress(SettingsActivity.this, new AddAddress.OnDialogClickListener() {
                    @Override
                    public void onDialogImageRunClick(int positon, String add) {

                        if (positon == 0) {

                        } else if (positon == 1) {
                            SetStatus();
                            //str_actionid = str_actionid.replaceAll(",$", "");
                        }

                    }
                }, sessionManager.getPasswd()).show();


            }
        });


        findViewById(R.id.iv_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    String shop_status = "";

    public void SetStatus() {

        if (shop_status.equalsIgnoreCase("open")) {
            shop_status = "2";  // for closed
        } else {
            shop_status = "1";  // for open
        }

        findViewById(R.id.rl_pb).setVisibility(View.VISIBLE);

        ApiInterface apiService =
                ApiClient.getClient(this).create(ApiInterface.class);

        JsonObject jo_st = new JsonObject();
        jo_st.addProperty("shop_status", shop_status);
        Call call;
        call = apiService.SetBusinessStatus(sessionManager.getsession(), jo_st);


        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                findViewById(R.id.rl_pb).setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    if (shop_status.equals("1")) {
                        shop_status = "Open";
                        sessionManager.setbus_status("Open");
                    } else {
                        shop_status = "Closed";
                        sessionManager.setbus_status("Closed");
                    }
                    tv_openclose.setText(sessionManager.getbusi_status());
                } else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Toast.makeText(SettingsActivity.this, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {

                    }
                }


            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // Log error here since request failed
                findViewById(R.id.rl_pb).setVisibility(View.GONE);
            }
        });


    }

}
