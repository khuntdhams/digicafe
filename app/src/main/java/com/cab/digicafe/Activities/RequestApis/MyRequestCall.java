package com.cab.digicafe.Activities.RequestApis;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.cab.digicafe.Adapter.traction.ModelUserTraction;
import com.cab.digicafe.Database.SettingDB;
import com.cab.digicafe.Database.SettingModel;
import com.cab.digicafe.Database.SqlLiteDbHelper;
import com.cab.digicafe.Dialogbox.DeliveryDialog;
import com.cab.digicafe.Helper.SessionManager;
import com.cab.digicafe.Helper.SharedPrefUserDetail;
import com.cab.digicafe.Model.Catelog;
import com.cab.digicafe.Model.ModelFile;
import com.cab.digicafe.Model.ShareModel;
import com.cab.digicafe.Model.SupplierNew;
import com.cab.digicafe.Model.Userprofile;
import com.cab.digicafe.MyCustomClass.JsonObjParse;
import com.cab.digicafe.R;
import com.cab.digicafe.Rest.ApiClient;
import com.cab.digicafe.Rest.ApiInterface;
import com.cab.digicafe.Rest.Request.SupplierRequest;
import com.cab.digicafe.Timeline.DateTimeUtils;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.cab.digicafe.MyCustomClass.Constants.my_referral;
import static com.cab.digicafe.MyCustomClass.Constants.referrer_detail_node;
import static com.cab.digicafe.MyCustomClass.Constants.share_shop_node;
import static com.cab.digicafe.services.RefererDataReciever.REFERRER_DATA;

public class MyRequestCall {

    public interface CallRequest {
        public void onGetResponse(String response);
    }

    public void getCategoryUnderMaster(final Context c, MyRequst myRequst, final CallRequest callRequest) {

        ApiInterface apiService =
                ApiClient.getClient(c).create(ApiInterface.class);


        Call call;
        call = apiService.getCategoryUnderMaster(myRequst.cookie, myRequst.page);


        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                if (response != null && response.isSuccessful()) {
                    callRequest.onGetResponse(new Gson().toJson(response.body()));
                } else {
                    callRequest.onGetResponse("");

                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Toast.makeText(c, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {

                    }

                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                callRequest.onGetResponse("");

            }
        });


    }

    public void addCategoryUnderMaster(final Context c, MyRequst myRequst, JsonObject jsonObject, final CallRequest callRequest) {

        ApiInterface apiService =
                ApiClient.getClient(c).create(ApiInterface.class);


        Call call;
        call = apiService.addCategoryUnderMaster(myRequst.cookie, jsonObject);


        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                if (response != null && response.isSuccessful()) {
                    callRequest.onGetResponse(new Gson().toJson(response.body()));
                } else {
                    callRequest.onGetResponse("");

                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Toast.makeText(c, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(c, "something went wrong", Toast.LENGTH_LONG).show(); // getting gtml 500
                    }

                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                callRequest.onGetResponse("");

            }
        });


    }

    public void deleteCategoryUnderMaster(final Context c, MyRequst myRequst, final CallRequest callRequest) {

        ApiInterface apiService =
                ApiClient.getClient(c).create(ApiInterface.class);


        Call call;
        call = apiService.deleteCategoryUnderMaster(myRequst.cookie, myRequst.deleteIds);


        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                if (response != null && response.isSuccessful()) {
                    callRequest.onGetResponse(new Gson().toJson(response.body()));
                } else {
                    callRequest.onGetResponse("");

                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Toast.makeText(c, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(c, "something went wrong", Toast.LENGTH_LONG).show(); // getting gtml 500
                    }

                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                callRequest.onGetResponse("");

            }
        });


    }

    public void getEmployeeUnderMaster(final Context c, MyRequst myRequst, final CallRequest callRequest) {

        ApiInterface apiService =
                ApiClient.getClient(c).create(ApiInterface.class);


        Call call;
        call = apiService.getEmployeeUnderMaster(myRequst.cookie, myRequst.page);


        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                if (response != null && response.isSuccessful()) {
                    callRequest.onGetResponse(new Gson().toJson(response.body()));
                } else {
                    callRequest.onGetResponse("");

                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Toast.makeText(c, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {

                    }

                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                callRequest.onGetResponse("");

            }
        });


    }

    public void addEmployeeUnderMaster(final Context c, MyRequst myRequst, JsonObject jsonObject, final CallRequest callRequest) {

        ApiInterface apiService =
                ApiClient.getClient(c).create(ApiInterface.class);


        Call call;
        call = apiService.addEmployeeUnderMaster(myRequst.cookie, jsonObject);


        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                if (response != null && response.isSuccessful()) {
                    callRequest.onGetResponse(new Gson().toJson(response.body()));
                } else {
                    callRequest.onGetResponse("");

                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Toast.makeText(c, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(c, "something went wrong", Toast.LENGTH_LONG).show(); // getting gtml 500
                    }

                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                callRequest.onGetResponse("");

            }
        });


    }

    public void deleteEmployeeUnderMaster(final Context c, MyRequst myRequst, final CallRequest callRequest) {

        ApiInterface apiService =
                ApiClient.getClient(c).create(ApiInterface.class);


        Call call;
        call = apiService.deleteEmpUnderMaster(myRequst.cookie, myRequst.deleteIds);


        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                if (response != null && response.isSuccessful()) {
                    callRequest.onGetResponse(new Gson().toJson(response.body()));
                } else {
                    callRequest.onGetResponse("");

                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Toast.makeText(c, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(c, "something went wrong", Toast.LENGTH_LONG).show(); // getting gtml 500
                    }

                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                callRequest.onGetResponse("");

            }
        });


    }

    public void getCatalogueList(final Context c, MyRequst myRequst, final CallRequest callRequest) {

        ApiInterface apiService =
                ApiClient.getClient(c).create(ApiInterface.class);


        Call call;

        call = apiService.getCatalougelist(myRequst.cookie, myRequst.aggre, myRequst.frombridgeidval, myRequst.bridgeidval, myRequst.page, "", myRequst.currency_code, "");


        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                if (response != null && response.isSuccessful()) {
                    callRequest.onGetResponse(new Gson().toJson(response.body()));
                } else {
                    callRequest.onGetResponse("");

                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Toast.makeText(c, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(c, "something went wrong", Toast.LENGTH_LONG).show(); // getting gtml 500
                    }

                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                callRequest.onGetResponse("");

            }
        });


    }


    AmazonS3 s3;
    TransferUtility transferUtility;
    String namegsxsax = "";

    public void uploadAwsS3(String unm, final Context c, String picturePath, final CallRequest callRequest) {
        s3 = new AmazonS3Client(new BasicAWSCredentials(c.getString(R.string.accesskey), c.getString(R.string.secretkey)));
        transferUtility = new TransferUtility(s3, c);

        File file = new File(picturePath);
        unm = unm.toUpperCase();
        String extension = picturePath.substring(picturePath.lastIndexOf("."));

        if (picturePath.endsWith(".png") || picturePath.endsWith(".jpg") || picturePath.endsWith(".jpeg")) {

            namegsxsax = "IMG_" + unm + System.currentTimeMillis() + "" + extension;

        } else { // pdf
            namegsxsax = "PDF_" + unm + System.currentTimeMillis() + "" + extension;
        }


        TransferObserver transferObserver = transferUtility.upload("cab-videofiles", namegsxsax, file);

        transferObserver.setTransferListener(new TransferListener() {
            @Override
            public void onStateChanged(int id, TransferState state) {
                if (state.name().equals("COMPLETED")) {
                    //str_img.put(getString(R.string.baseaws)+namegsxsax);
                    String picturePathNew = c.getString(R.string.baseaws) + namegsxsax;
                    callRequest.onGetResponse(picturePathNew);
                } else if (state.name().equals("FAILED")) {
                    callRequest.onGetResponse("");

                    Toast.makeText(c, "Failed Uploading !", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {

            }

            @Override
            public void onError(int id, Exception ex) {
                callRequest.onGetResponse("");

                Toast.makeText(c, "Failed Uploading !", Toast.LENGTH_LONG).show();

            }

        });


    }

    public void uploadCatAwsS3(String unm, final Context c, int i, String picturePath, final CallRequest callRequest) {
        s3 = new AmazonS3Client(new BasicAWSCredentials(c.getString(R.string.accesskey), c.getString(R.string.secretkey)));
        transferUtility = new TransferUtility(s3, c);


        File file = new File(picturePath);
        unm = unm.toUpperCase();
        String extension = picturePath.substring(picturePath.lastIndexOf("."));

        namegsxsax = "IMG_CAT_" + i + "_" + unm + System.currentTimeMillis() + "" + extension;


        TransferObserver transferObserver = transferUtility.upload("cab-videofiles", namegsxsax, file);

        transferObserver.setTransferListener(new TransferListener() {
            @Override
            public void onStateChanged(int id, TransferState state) {
                if (state.name().equals("COMPLETED")) {
                    //str_img.put(getString(R.string.baseaws)+namegsxsax);
                    String picturePathNew = c.getString(R.string.baseaws) + namegsxsax;
                    callRequest.onGetResponse(picturePathNew);
                } else if (state.name().equals("FAILED")) {
                    callRequest.onGetResponse("");

                    Toast.makeText(c, "Failed Uploading !", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {

            }

            @Override
            public void onError(int id, Exception ex) {
                callRequest.onGetResponse("");

                Toast.makeText(c, "Failed Uploading !", Toast.LENGTH_LONG).show();

            }

        });


    }

    public void uploadSamplesFromCartAwsS3(String unm, Catelog catelog, final Context c, int i, String picturePath, final CallRequest callRequest) {
        s3 = new AmazonS3Client(new BasicAWSCredentials(c.getString(R.string.accesskey), c.getString(R.string.secretkey)));
        transferUtility = new TransferUtility(s3, c);


        File file = new File(picturePath);
        unm = unm.toUpperCase();
        String extension = picturePath.substring(picturePath.lastIndexOf("."));

        namegsxsax = unm + "_" + catelog.getDefault_name() + "_" + catelog.getEntry_id() + "_" + System.currentTimeMillis() + "" + extension;


        TransferObserver transferObserver = transferUtility.upload("cab-videofiles", namegsxsax, file);

        transferObserver.setTransferListener(new TransferListener() {
            @Override
            public void onStateChanged(int id, TransferState state) {
                if (state.name().equals("COMPLETED")) {
                    //str_img.put(getString(R.string.baseaws)+namegsxsax);
                    String picturePathNew = c.getString(R.string.baseaws) + namegsxsax;
                    callRequest.onGetResponse(picturePathNew);
                } else if (state.name().equals("FAILED")) {
                    callRequest.onGetResponse("");

                    Toast.makeText(c, "Failed Uploading !", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {

            }

            @Override
            public void onError(int id, Exception ex) {
                callRequest.onGetResponse("");

                Toast.makeText(c, "Failed Uploading !", Toast.LENGTH_LONG).show();

            }

        });


    }

    public void addNewNwForAggregator(final Context context, String cookie, JsonObject jo_cat, final CallRequest callRequest) {


        ApiInterface apiService =
                ApiClient.getClient(context).create(ApiInterface.class);

        Call call = apiService.addNetwork(cookie, jo_cat);

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {

                if (response != null && response.isSuccessful()) {
                    callRequest.onGetResponse(new Gson().toJson(response.body()));
                } else {
                    callRequest.onGetResponse("");

                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Toast.makeText(context, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(context, "something went wrong", Toast.LENGTH_LONG).show(); // getting gtml 500
                    }

                }


            }

            @Override
            public void onFailure(Call call, Throwable t) {
                callRequest.onGetResponse("");

            }
        });
    }


    public void updateOnlyFieldJsonOfBusinessInfo(final Context context, String cookie, Map<String, RequestBody> stringRequestBodyHashMap, final CallRequest callRequest) {

        ApiInterface apiService =
                ApiClient.getClient(context).create(ApiInterface.class);

        Call call = apiService.updateBusinessProfile(cookie, stringRequestBodyHashMap);

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {

                if (response != null && response.isSuccessful()) {
                    callRequest.onGetResponse(new Gson().toJson(response.body()));
                } else {
                    callRequest.onGetResponse("");

                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Toast.makeText(context, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(context, "something went wrong", Toast.LENGTH_LONG).show(); // getting gtml 500
                    }

                }


            }

            @Override
            public void onFailure(Call call, Throwable t) {
                callRequest.onGetResponse("");

            }
        });
    }


    public ArrayList<ModelUserTraction> getTraction(Context context) {
        String json = SharedPrefUserDetail.getString(context, SharedPrefUserDetail.user_traction_list, "");
        Type type = new TypeToken<ArrayList<ModelUserTraction>>() {
        }.getType();
        ArrayList<ModelUserTraction> alUserTraction = new Gson().fromJson(json, type);
        if (alUserTraction == null) {
            alUserTraction = new ArrayList<>();
        }
        return alUserTraction;
    }

    public void setTraction(Context context, SupplierNew supplierNew, String field_json_data) {
        ArrayList<ModelUserTraction> alUserTraction = getTraction(context);
        boolean isAny = false;
        for (int i = 0; i < alUserTraction.size(); i++) {
            String bId = alUserTraction.get(i).business_bridge_id;
            if (bId.equalsIgnoreCase(supplierNew.getBridgeId())) {
                isAny = true;
                alUserTraction.get(i).total_visited = alUserTraction.get(i).total_visited + 1;
                alUserTraction.get(i).date = new Date();
                break;
            }
        }
        if (!isAny) {
            SessionManager sessionManager = new SessionManager(context);

            JsonObject jsonObject = new JsonObject();

            String userprofile = sessionManager.getUserprofile();
            Userprofile obj = new Gson().fromJson(userprofile, Userprofile.class);

            if (obj != null) {
                jsonObject.addProperty("contact_no", obj.getContact_no() + "");
                jsonObject.addProperty("firstname", obj.getFirstname() + "");
                jsonObject.addProperty("location", obj.getLocation() + "");
            }


            //alUserTraction.add(new ModelUserTraction(1, sessionManager.getBridgeUserId(), supplierNew.getBridgeId(), sessionManager.getFstNm(), supplierNew.getFirstname(), field_json_data));
            alUserTraction.add(new ModelUserTraction(1, sessionManager.getBridgeUserId(), supplierNew.getBridgeId(), sessionManager.getUserWithAggregator(), supplierNew.getUsername(), jsonObject.toString(), field_json_data));
        }
        SharedPrefUserDetail.setString(context, SharedPrefUserDetail.user_traction_list, new Gson().toJson(alUserTraction));
    }

    public void clearTraction(Context context) {
        ArrayList<ModelUserTraction> alUserTraction = new ArrayList<>();
        SharedPrefUserDetail.setString(context, SharedPrefUserDetail.user_traction_list, new Gson().toJson(alUserTraction));
    }

    public void getPropertyEnquiry(final Context c, MyRequst myRequst, final CallRequest callRequest) {

        ApiInterface apiService = ApiClient.getClient(c).create(ApiInterface.class);
        Call call;
        call = apiService.getPropertyEnq(myRequst.cookie, myRequst.chit_name, "", "", "3,4,8,99", myRequst.contact_number, myRequst.page);
        //call = apiService.searchAlltask(myRequst.cookie, "", "3,4,8,99", 1);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                if (response != null && response.isSuccessful()) {
                    callRequest.onGetResponse(new Gson().toJson(response.body()));
                } else {
                    callRequest.onGetResponse("");

                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Toast.makeText(c, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {

                    }

                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                callRequest.onGetResponse("");

            }
        });


    }


    // Survey

    String headernotes = "", frombridgeidval = "";
    CallRequest callRequestForSurvey;

    private ProgressDialog dialog;


    public void callCreateChitFoSurvey(final AppCompatActivity c, final CallRequest callRequest, String frombridgeidval) {


        callRequestForSurvey = callRequest;

        this.frombridgeidval = frombridgeidval;

        final SessionManager sessionManager = new SessionManager(c);
        final SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy hh:mm aaa");
        final Calendar cal_order = Calendar.getInstance();


        new DeliveryDialog(c, new DeliveryDialog.OnDialogClickListener() {
            @Override
            public void onDialogImageRunClick(int positon, final String value, String remark, String phone) {
                if (positon == 0) {

                    headernotes = remark;
                    final String date_ = df.format(cal_order.getTime());

                    sessionManager.setDate(date_);
                    sessionManager.setAddress(value);
                    sessionManager.setRemark(headernotes);
                    sessionManager.setT_Phone(phone);

                    try {
                        userdetails = new JSONObject(sessionManager.getUserprofile());
                        loggedin = new Gson().fromJson(userdetails.toString(), Userprofile.class);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                    usersignin(c, sessionManager.getsession(), date_, value);

                    //   uplaodQueue(sessionString, cartdetails, userdetails, date_, value);

                }
            }
        }, sessionManager.getAddress(), df.format(cal_order.getTime()), "", sessionManager.getRemark(), sessionManager.getT_Phone()).show();


    }

    ArrayList<ModelFile> al_selet = new ArrayList<>();
    Userprofile loggedin;
    JSONObject userdetails;
    int productcount;

    ArrayList<Catelog> productlist;
    String field_json_data = "", field_json_data_info = "", business_type_info = "";

    Double price = 0.0;

    public void usersignin(final Context c, final String usersession, final String dt, final String address) {

        dialog = new ProgressDialog(c);
        dialog.setMessage("Sending...");
        dialog.show();


        final SessionManager sessionManager = new SessionManager(c);


        field_json_data = SharedPrefUserDetail.getString(c, SharedPrefUserDetail.field_json_data, "");
        field_json_data_info = SharedPrefUserDetail.getString(c, SharedPrefUserDetail.field_json_data_info, "");
        business_type_info = SharedPrefUserDetail.getString(c, SharedPrefUserDetail.business_type, "");


        SqlLiteDbHelper dbHelper = new SqlLiteDbHelper(c);

        productcount = dbHelper.getcartcount();

        productlist = dbHelper.getAllcaretproduct();

        final JsonObject sendobj = new JsonObject();
        JsonArray a = new JsonArray();
        JsonArray cartproduct = new JsonArray();
        price = 0.0;

        for (int i = 0; i < productlist.size(); i++) {
            Catelog product = productlist.get(i);
            JsonObject productsync = new JsonObject();

            final SettingDB db = new SettingDB(c);

            List<SettingModel> list_setting = db.GetItems();
            SettingModel sm = new SettingModel();
            sm = list_setting.get(0);


            String particular = product.getDefault_name();


            String cross_reference = product.getCross_reference();
            if (cross_reference != null && !cross_reference.equals("")) {
                particular = particular + "; [" + cross_reference + "]";
            }
            String offer = product.getOffer();
            if (offer != null && !offer.equals("") && sm.getIsconcateoffer().equals("1")) {
                particular = particular + "; " + product.getOffer();
            }
            al_selet = product.getAl_selet();
            if (al_selet.size() > 0) {
                String samples = new Gson().toJson(al_selet);
                //particular = particular + ";;; " + samples;
            }

            if (!product.getMrp().isEmpty()) price = price + Double.valueOf(product.getMrp());

            productsync.addProperty("entry_id", product.getEntry_id());
            productsync.addProperty("bridge_id", product.getBridge_id());
            productsync.addProperty("flag", "add");
            productsync.addProperty("quantity", product.getCartcount());
            productsync.addProperty("particulars", particular);
            //productsync.addProperty("price", String.valueOf(cal_price(i)));
            productsync.addProperty("price", product.getMrp());
            productsync.addProperty("offer", product.getOffer());
            cartproduct.add(productsync);


        }


        try {
            JsonObject a1 = new JsonObject();

            a1.addProperty("email", loggedin.getEmail_id());
            a1.addProperty("contact_number", loggedin.getContact_no());
            a1.addProperty("location", loggedin.getLocation());
            a1.addProperty("chit_name", loggedin.getFirstname());
            a1.addProperty("header_note", headernotes);

            //a1.addProperty("footer_note", jo_footer.toString());
            sendobj.add("newdata", cartproduct);
            sendobj.add("olddata", a);
            sendobj.add("chitHeader", a1);

        } catch (JsonParseException e) {
            e.printStackTrace();
        }

        //price = price / dbHelper.getcartcount();

        ApiInterface apiService =
                ApiClient.getClient(c).create(ApiInterface.class);
        String currency_code = SharedPrefUserDetail.getString(c, SharedPrefUserDetail.chit_currency_code, "");
        String currency_code_id = SharedPrefUserDetail.getString(c, SharedPrefUserDetail.chit_currency_code_id, "");

        Call call = apiService.createchit(usersession, sendobj, sessionManager.getAggregatorprofile(), frombridgeidval, "d97be841a6d6cbb66bc3ae165b31b85f", currency_code, currency_code_id, "2");
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {

                if (response.isSuccessful()) {
                    try {
                        String a = new Gson().toJson(response.body());
                        Log.e("response", a);
                        JSONObject responseobj = new JSONObject(a);
                        JSONObject responseval = responseobj.getJSONObject("response");

                        JSONObject data = responseval.getJSONObject("data");
                        String chithashid = data.getString("chitHashId");
                        try {
                            JsonObject jObj = new JsonObject();
                            jObj.addProperty("email", loggedin.getEmail_id());
                            jObj.addProperty("contact_number", sessionManager.getT_Phone());
                            jObj.addProperty("location", sessionManager.getAddress());
                            jObj.addProperty("act", sessionManager.getcurrentcaterername());
                            jObj.addProperty("chit_name", "");
                            jObj.addProperty("chitHashId", chithashid);
                            jObj.addProperty("latitude", "");
                            jObj.addProperty("longtitude", "");
                            jObj.addProperty("expected_delivery_time", "");
                            jObj.addProperty("for_non_bridge_name", "");
                            jObj.addProperty("subject", sessionManager.getcurrentcatereraliasname());

                            String currency_code = SharedPrefUserDetail.getString(c, SharedPrefUserDetail.chit_currency_code, "");
                            String symbol_native = SharedPrefUserDetail.getString(c, SharedPrefUserDetail.chit_symbol_native, "");
                            String currency_code_id = SharedPrefUserDetail.getString(c, SharedPrefUserDetail.chit_currency_code_id, "");
                            String symbol = SharedPrefUserDetail.getString(c, SharedPrefUserDetail.chit_symbol, "");

                            jObj.addProperty("currency_code", currency_code);
                            jObj.addProperty("symbol_native", symbol_native);
                            jObj.addProperty("currency_code_id", currency_code_id);
                            jObj.addProperty("symbol", symbol);


                            boolean iss = SharedPrefUserDetail.getBoolean(c, SharedPrefUserDetail.isfromsupplier, false);
                            if (iss) {
                                jObj.addProperty("info", "");
                                StringBuilder stringBuilder = new StringBuilder();
                                if (field_json_data != null) {

                                    String delivery_partner_info = JsonObjParse.getValueEmpty(field_json_data, "delivery_partner_info");
                                    if (!delivery_partner_info.isEmpty()) {
                                        stringBuilder.append(delivery_partner_info + " , ");
                                        jObj.addProperty("info", stringBuilder.toString());
                                    }
                                }
                            } else {

                                String info = SharedPrefUserDetail.getString(c, SharedPrefUserDetail.infoMainShop, "");
                                String act = sessionManager.getcurrentcaterername();
                                StringBuilder stringBuilder = new StringBuilder(); // old is correct

                                if (field_json_data_info != null) {
                                    String pass_info_circle = JsonObjParse.getValueEmpty(field_json_data_info, "pass_info_circle");
                                    String pass_info_aggregator = JsonObjParse.getValueEmpty(field_json_data_info, "pass_info_aggregator");
                                    if (!pass_info_circle.equalsIgnoreCase("no") && business_type_info.equalsIgnoreCase("circle")) {
                                        if (!info.isEmpty()) stringBuilder.append(info + " , ");
                                    }
                                    if (!pass_info_aggregator.equalsIgnoreCase("no") && business_type_info.equalsIgnoreCase("aggregator")) {
                                        if (!info.isEmpty()) stringBuilder.append(info + " , ");
                                    }
                                }

                                if (!act.isEmpty()) stringBuilder.append(act + " , ");

                                if (field_json_data != null) {
                                    String delivery_partner_info = JsonObjParse.getValueEmpty(field_json_data, "delivery_partner_info");
                                    if (!delivery_partner_info.isEmpty()) {
                                        stringBuilder.append(delivery_partner_info + " , ");
                                    }
                                }

                                Log.d("okhttp info", stringBuilder.toString());
                                jObj.addProperty("info", stringBuilder.toString());
                                //jObj.addProperty("info", sessionManager.getAggregator_ID());


                            }


                            jObj.addProperty("header_note", headernotes);


                            jObj.addProperty("purpose", "Survey");

                            jObj.addProperty("chit_item_count", productcount);


                            jObj.addProperty("total_chit_item_value", price + "");
                            JsonObject jo_footer = new JsonObject();
                          /*  jo_footer.addProperty("SGST", String.valueOf(totalstategst));
                            jo_footer.addProperty("VAT", String.valueOf(totalVat));
                            jo_footer.addProperty("CGST", String.valueOf(totalcentralgst));
                            jo_footer.addProperty("TOTALBILL", String.valueOf(totalbill));
                            jo_footer.addProperty("GRANDTOTAL", String.valueOf(grandtotal));*/
                            jo_footer.addProperty("NAME", loggedin.getFirstname());
                            jo_footer.addProperty("PAYMENT_MODE", "NONE");


                            jo_footer.addProperty("DATE", dt);
                            jo_footer.addProperty("ADDRESS", address);
                            jo_footer.addProperty("DOB", sessionManager.getDobForSurvey());
                            jObj.addProperty("footer_note", jo_footer.toString());

                            Log.e("chitheadervalue", jObj.toString());
                            confirmorder(c, chithashid);

                        } catch (JsonParseException e) {
                            e.printStackTrace();
                        }
                    } catch (Exception e) {
                        Toast.makeText(c, e.getMessage(), Toast.LENGTH_LONG).show();
                    }

                } else {
                    if (dialog != null) dialog.dismiss();

                    Log.e("test", " trdds1");

                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Toast.makeText(c, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(c, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }

            }

            @Override
            public void onFailure(Call call, Throwable t) {
                if (dialog != null) dialog.dismiss();

                Log.e("error message", t.toString());
            }
        });
    }

    public void confirmorder(Context context, String hashidval) {

        final SessionManager sessionManager = new SessionManager(context);
        SqlLiteDbHelper dbHelper = new SqlLiteDbHelper(context);


        try {
            JsonObject jObj = new JsonObject();
            jObj.addProperty("email", loggedin.getEmail_id());
            jObj.addProperty("contact_number", sessionManager.getT_Phone());
            jObj.addProperty("location", sessionManager.getAddress());
            jObj.addProperty("act", sessionManager.getcurrentcaterername());


            jObj.addProperty("chit_name", ""); //


            jObj.addProperty("chitHashId", hashidval);
            jObj.addProperty("latitude", "");
            jObj.addProperty("longtitude", "");

            String expected_delivery_time = DateTimeUtils.parseDateTime(sessionManager.getDate(), "dd MMM yyyy hh:mm aaa", "yyyy/MM/dd kk:mm");

            jObj.addProperty("expected_delivery_time", expected_delivery_time);
            jObj.addProperty("for_non_bridge_name", "");
            jObj.addProperty("subject", sessionManager.getcurrentcatereraliasname());

            String currency_code = SharedPrefUserDetail.getString(context, SharedPrefUserDetail.chit_currency_code, "");
            String symbol_native = SharedPrefUserDetail.getString(context, SharedPrefUserDetail.chit_symbol_native, "");
            String currency_code_id = SharedPrefUserDetail.getString(context, SharedPrefUserDetail.chit_currency_code_id, "");
            String symbol = SharedPrefUserDetail.getString(context, SharedPrefUserDetail.chit_symbol, "");

            jObj.addProperty("currency_code", currency_code);
            jObj.addProperty("symbol_native", symbol_native);
            jObj.addProperty("currency_code_id", currency_code_id);
            jObj.addProperty("symbol", symbol);


            boolean iss = SharedPrefUserDetail.getBoolean(context, SharedPrefUserDetail.isfromsupplier, false);
            if (iss) { // if its business
                jObj.addProperty("info", "");

                StringBuilder stringBuilder = new StringBuilder();

                if (field_json_data != null) {

                    String delivery_partner_info = JsonObjParse.getValueEmpty(field_json_data, "delivery_partner_info");
                    if (!delivery_partner_info.isEmpty()) {
                        stringBuilder.append(delivery_partner_info + " , ");
                        jObj.addProperty("info", stringBuilder.toString());
                    }
                }


            } else { // if its aggre / circle
                // blrmc1(delivery flag of this) under blrmcc(info flag of this)
                String info = SharedPrefUserDetail.getString(context, SharedPrefUserDetail.infoMainShop, "");
                //String act = sessionManager.getAggregator_ID();
                String act = sessionManager.getcurrentcaterername();
                //String infoPass = info + " , " + act + " , ";
                StringBuilder stringBuilder = new StringBuilder(); // old is correct
                //if(!info.isEmpty())stringBuilder.append(info+" , ");
                //if(!act.isEmpty())stringBuilder.append(act+" , ");
                if (field_json_data_info != null) {
                    String pass_info_circle = JsonObjParse.getValueEmpty(field_json_data_info, "pass_info_circle");
                    String pass_info_aggregator = JsonObjParse.getValueEmpty(field_json_data_info, "pass_info_aggregator");
                    if (!pass_info_circle.equalsIgnoreCase("no") && business_type_info.equalsIgnoreCase("circle")) {
                        if (!info.isEmpty()) stringBuilder.append(info + " , ");
                    }
                    if (!pass_info_aggregator.equalsIgnoreCase("no") && business_type_info.equalsIgnoreCase("aggregator")) {
                        if (!info.isEmpty()) stringBuilder.append(info + " , ");
                    }
                }

                if (!act.isEmpty()) stringBuilder.append(act + " , ");

                if (field_json_data != null) {

                    String delivery_partner_info = JsonObjParse.getValueEmpty(field_json_data, "delivery_partner_info");
                    if (!delivery_partner_info.isEmpty()) {
                        stringBuilder.append(delivery_partner_info + " , ");
                    }
                }

                Log.d("okhttp info", stringBuilder.toString());
                jObj.addProperty("info", stringBuilder.toString());
                //jObj.addProperty("info", sessionManager.getAggregator_ID());
            }


            jObj.addProperty("header_note", sessionManager.getRemark());


            jObj.addProperty("purpose", "Survey");


            jObj.addProperty("chit_item_count", dbHelper.getcartcount());
            jObj.addProperty("total_chit_item_value", price + "");
            JsonObject jo_footer = new JsonObject();
           /* jo_footer.addProperty("SGST", String.valueOf(totalstategst));
            jo_footer.addProperty("CGST", String.valueOf(totalcentralgst));
            jo_footer.addProperty("VAT", String.valueOf(totalVat));
            jo_footer.addProperty("TOTALBILL", String.valueOf(totalbill));
            jo_footer.addProperty("TOTLREQDISC", String.valueOf(totalReqDisc));
            jo_footer.addProperty("REQ_DISC", String.valueOf(ReqDisc));
            jo_footer.addProperty("TOTLPRODISC", String.valueOf(totalProductDisc));
            jo_footer.addProperty("GRANDTOTAL", String.valueOf(grandtotal));
            jo_footer.addProperty("businessId", String.valueOf(grandtotal));*/


            jo_footer.addProperty("NAME", loggedin.getFirstname());
            jo_footer.addProperty("DOB", sessionManager.getDobForSurvey());


            /*String taxArray = new Gson().toJson(alTax);
            jo_footer.addProperty("taxArray", taxArray);
*/
            /*if (al_uploadedimg.size() > 0) {
                String imgjson = new Gson().toJson(al_uploadedimg);
                jo_footer.addProperty("img", imgjson);
            } else {

                jo_footer.addProperty("img", "");

            }
*/
           /* if (rb_cod.isChecked()) {
                iscod = true;
                jo_footer.addProperty("PAYMENT_MODE", "COD");
            } else {
                iscod = false;
            }*/

            jo_footer.addProperty("PAYMENT_MODE", "NONE");

            // Consumer Array

            JsonArray jaConsumerContact = new JsonArray();
            JsonObject joConsumer = new JsonObject();
            String sms_customer = JsonObjParse.getValueEmpty(field_json_data, "sms_customer");
            joConsumer.addProperty("sms", sms_customer);
            joConsumer.addProperty("customer_nm", loggedin.getFirstname());
            joConsumer.addProperty("mobile", sessionManager.getT_Phone());
            jaConsumerContact.add(joConsumer);
            jo_footer.add("ConsumerInfo", jaConsumerContact);

            // Business Array

            JsonArray jaBusinessContact = new JsonArray();
            JsonObject joBusiness = new JsonObject();
            String sms_business = JsonObjParse.getValueEmpty(field_json_data, "sms_status");
            joBusiness.addProperty("sms", sms_business);
            String mobile_business = JsonObjParse.getValueEmpty(field_json_data, "mobile_business");
            joBusiness.addProperty("mobile", mobile_business);
            joBusiness.addProperty("customer_nm", sessionManager.getcurrentcatereraliasname());
            jaBusinessContact.add(joBusiness);

            jo_footer.add("BusinessInfo", jaBusinessContact);

            // Delivery Array

           /* JsonArray jaDeliveryContact = new JsonArray();
            JsonObject joDelivery = new JsonObject();

            String mobile_delivery = JsonObjParse.getValueEmpty(field_json_data, "mobile_delivery");
            joDelivery.addProperty("mobile", mobile_delivery);
            String companynm = JsonObjParse.getValueEmpty(field_json_data, "customer_nm_delivery");
            joDelivery.addProperty("customer_nm", companynm + "");
            //String pick_up_only = JsonObjParse.getValueEmpty(field_json_data, "pick_up_only");

            String sms_delivery = JsonObjParse.getValueEmpty(field_json_data, "sms_delivery");
            joDelivery.addProperty("sms", sms_delivery);
            joDelivery.addProperty("pick_up_only", "No");

            jaDeliveryContact.add(joDelivery);
            jo_footer.add("DeliveryInfo", jaDeliveryContact);*/

            jo_footer.addProperty("DATE", sessionManager.getDate());
            jo_footer.addProperty("ADDRESS", sessionManager.getAddress());


            jo_footer.addProperty("Cust_Nm", sessionManager.getCustNmForProperty());

            jo_footer.addProperty("payuResponse", "");
            jo_footer.addProperty("chithashid", hashidval);


            jo_footer.addProperty("field_json_data", field_json_data);


            jObj.addProperty("footer_note", jo_footer.toString());
            Log.i("okhttp", jo_footer.toString());
            updatechit(context, sessionManager.getsession(), jObj, hashidval);

        } catch (JsonParseException e) {
            if (dialog != null) dialog.dismiss();

            e.printStackTrace();
        }
    }

    public void updatechit(final Context c, String usersession, JsonObject chitdetails, String hashid) {

        SessionManager sessionManager = new SessionManager(c);

        ApiInterface apiService = ApiClient.getClient(c).create(ApiInterface.class);


        String currency_code = SharedPrefUserDetail.getString(c, SharedPrefUserDetail.chit_currency_code, "");
        String currency_code_id = SharedPrefUserDetail.getString(c, SharedPrefUserDetail.chit_currency_code_id, "");

        Call call = apiService.updatechit(usersession, chitdetails, sessionManager.getAggregatorprofile(), hashid, "d97be841a6d6cbb66bc3ae165b31b85f", currency_code, currency_code_id, "2");
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {

                if (dialog != null) dialog.dismiss();


                if (response.isSuccessful()) {

                    try {

                        String a = new Gson().toJson(response.body());

                        callRequestForSurvey.onGetResponse("");

                    } catch (Exception e) {

                        Toast.makeText(c, e.getMessage(), Toast.LENGTH_LONG).show();
                    }

                } else {

                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Log.e("test", " trdds1" + jObjErrorresponse.toString());
                        String msg = jObjErrorresponse.getString("errormsg");
                        if (msg.equalsIgnoreCase("To is empty")) {
                            msg = "You cannot surveying to yourselves.";
                        }
                        Toast.makeText(c, msg, Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(c, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }

            }

            @Override
            public void onFailure(Call call, Throwable t) {
                if (dialog != null) dialog.dismiss();

            }
        });
    }

    // Traction


    public void postCustomerTraction(final Context c, JsonObject jsonObject, final CallRequest callRequest) {

        ApiInterface apiService = ApiClient.getClient(c).create(ApiInterface.class);

        SessionManager sessionManager = new SessionManager(c);
        Call call;
        call = apiService.postCustomerTraction(sessionManager.getsession(), jsonObject);

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                if (response != null && response.isSuccessful()) {

                    callRequest.onGetResponse(new Gson().toJson(response.body()));
                } else {
                    callRequest.onGetResponse("");

                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Toast.makeText(c, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(c, "something went wrong", Toast.LENGTH_LONG).show(); // getting gtml 500
                    }

                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                callRequest.onGetResponse("");

            }
        });


    }

    int indexOfTraction;

    ProgressDialog progressDialogLogout;

    public void checkTraction(final boolean isLogout, final int pos, final Context context, final CallRequest callFromLogout) {


        indexOfTraction = pos;

        final ArrayList<ModelUserTraction> alUserTraction = getTraction(context);
        if (alUserTraction.size() == 0) {
            // Traction finished / have not
            if (progressDialogLogout != null && progressDialogLogout.isShowing()) {
                progressDialogLogout.dismiss();
            }

            callFromLogout.onGetResponse("");
        } else if (indexOfTraction == alUserTraction.size()) { // Daily
            // Traction finished
        } else {

            if (progressDialogLogout == null && isLogout) {
                progressDialogLogout = new ProgressDialog(context);
                progressDialogLogout.setMessage("Please wait...");
                progressDialogLogout.show();
            }


            ModelUserTraction modelUserTraction = alUserTraction.get(indexOfTraction);
            int tractionId = 0;
            if (modelUserTraction.total_visited != null && modelUserTraction.total_visited > 0) {
                String tractionIdStr = JsonObjParse.getValueEmpty(modelUserTraction.field_json_data_of_shop, "tractionId");
                if (!tractionIdStr.isEmpty()) tractionId = Integer.parseInt(tractionIdStr);
                if (tractionId == 0) { // Daily

                    boolean isDaily = false; // diff. date with current

                    Date cDate = new Date();
                    Date tDate = modelUserTraction.date;

                    if (!DateTimeUtils.getDateFromDateTime(cDate).equals(DateTimeUtils.getDateFromDateTime(tDate))) {
                        isDaily = true;
                    }

                    if (isDaily || isLogout) {

                        JsonObject jsonObject = new JsonObject();
                        jsonObject.addProperty("business_bridge_id", modelUserTraction.business_bridge_id);
                        jsonObject.addProperty("consumer_bridge_id", modelUserTraction.consumer_bridge_id);
                        jsonObject.addProperty("business_username", modelUserTraction.business_username);
                        jsonObject.addProperty("consumer_username", modelUserTraction.consumer_username);
                        jsonObject.addProperty("total_visited", modelUserTraction.total_visited);
                        jsonObject.addProperty("field_json_data", modelUserTraction.field_json_data);
                        postCustomerTraction(context, jsonObject, new CallRequest() {
                            @Override
                            public void onGetResponse(String response) {
                                alUserTraction.remove(indexOfTraction);
                                SharedPrefUserDetail.setString(context, SharedPrefUserDetail.user_traction_list, new Gson().toJson(alUserTraction));
                                checkTraction(isLogout, indexOfTraction, context, callFromLogout);
                            }
                        });

                    } else {
                        indexOfTraction++;
                        checkTraction(isLogout, indexOfTraction, context, callFromLogout);
                    }


                } else if (tractionId == 1) { // Immediate


                    JsonObject jsonObject = new JsonObject();
                    jsonObject.addProperty("business_bridge_id", modelUserTraction.business_bridge_id);
                    jsonObject.addProperty("consumer_bridge_id", modelUserTraction.consumer_bridge_id);
                    jsonObject.addProperty("business_username", modelUserTraction.business_username);
                    jsonObject.addProperty("consumer_username", modelUserTraction.consumer_username);
                    jsonObject.addProperty("total_visited", modelUserTraction.total_visited);
                    jsonObject.addProperty("field_json_data", modelUserTraction.field_json_data);
                    postCustomerTraction(context, jsonObject, new CallRequest() {
                        @Override
                        public void onGetResponse(String response) {
                            alUserTraction.remove(indexOfTraction);
                            SharedPrefUserDetail.setString(context, SharedPrefUserDetail.user_traction_list, new Gson().toJson(alUserTraction));
                            checkTraction(isLogout, indexOfTraction, context, callFromLogout);
                        }
                    });

                }
            }
        }


    }

    public void getTractionData(final Context c, final CallRequest callRequest) {

        ApiInterface apiService = ApiClient.getClient(c).create(ApiInterface.class);

        SessionManager sessionManager = new SessionManager(c);
        Call call;
        call = apiService.getCustomerTraction(sessionManager.getsession());

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                if (response != null && response.isSuccessful()) {
                    callRequest.onGetResponse(new Gson().toJson(response.body()));
                } else {
                    callRequest.onGetResponse("");
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Toast.makeText(c, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(c, "something went wrong", Toast.LENGTH_LONG).show(); // getting gtml 500
                    }
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                callRequest.onGetResponse("");

            }
        });


    }

    // Taskpreference - MRP

    public void addTaskPreferencr(final Context c, MyRequst myRequst, JsonObject jsonObject, final CallRequest callRequest) {

        ApiInterface apiService = ApiClient.getClient(c).create(ApiInterface.class);

        Call call;
        call = apiService.posttaskreference(myRequst.cookie, jsonObject);

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                if (response != null && response.isSuccessful()) {
                    callRequest.onGetResponse(new Gson().toJson(response.body()));
                } else {
                    callRequest.onGetResponse("");
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Toast.makeText(c, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(c, "something went wrong" + getClass(), Toast.LENGTH_LONG).show(); // getting html 500
                    }
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                callRequest.onGetResponse("");

            }
        });


    }

    public void editTaskPreferencr(final Context c, MyRequst myRequst, JsonObject jsonObject, final CallRequest callRequest) {

        ApiInterface apiService = ApiClient.getClient(c).create(ApiInterface.class);

        Call call;
        call = apiService.posttaskreference(myRequst.cookie, jsonObject);

        if (progressDialogLogout == null) {
            progressDialogLogout = new ProgressDialog(c);
            progressDialogLogout.setMessage("Loading...");
            progressDialogLogout.show();
        }

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {

                if (progressDialogLogout != null && progressDialogLogout.isShowing()) {
                    progressDialogLogout.dismiss();
                }


                if (response != null && response.isSuccessful()) {
                    callRequest.onGetResponse(new Gson().toJson(response.body()));
                } else {
                    callRequest.onGetResponse("");
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Toast.makeText(c, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(c, "something went wrong" + getClass(), Toast.LENGTH_LONG).show(); // getting html 500
                    }
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {

                if (progressDialogLogout != null && progressDialogLogout.isShowing()) {
                    progressDialogLogout.dismiss();
                }


                callRequest.onGetResponse("");

            }
        });


    }


    public void getTaskreference(final Context c, final CallRequest callRequest, JsonObject jsonObject, MyRequst myRequst) {

        ApiInterface apiService = ApiClient.getClient(c).create(ApiInterface.class);
        SessionManager sessionManager = new SessionManager(c);
        Call call;
        call = apiService.getTaskPreferenceByChitId(sessionManager.getsession(), myRequst.chit_id, myRequst.to_chit_hash_id, myRequst.task_purpose, myRequst.page);

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                if (response != null && response.isSuccessful()) {
                    callRequest.onGetResponse(new Gson().toJson(response.body()));
                } else {
                    callRequest.onGetResponse("");
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Toast.makeText(c, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(c, "something went wrong", Toast.LENGTH_LONG).show(); // getting gtml 500
                    }
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                callRequest.onGetResponse("");
            }
        });

    }

    public void searchCaseId(final Context c, final CallRequest callRequest, JsonObject jsonObject, MyRequst myRequst) {


        if (progressDialogLogout == null) {
            progressDialogLogout = new ProgressDialog(c);
            progressDialogLogout.setMessage("Loading...");
            progressDialogLogout.show();
        }

        ApiInterface apiService = ApiClient.getClient(c).create(ApiInterface.class);

        SessionManager sessionManager = new SessionManager(c);
        Call call = null;

        if (ShareModel.getI().isFromAllTask) { // serach order api
            call = apiService.getChitFromCaseIdFromAllTask(sessionManager.getsession(), myRequst.to_chit_hash_id, myRequst.chit_id, myRequst.page);
        } else { // from order search task unassign api
//            call = apiService.getChitFromCaseId(sessionManager.getsession(), myRequst.search, myRequst.page);

            call = apiService.getChitFromCaseIdFromAllTask(sessionManager.getsession(), myRequst.to_chit_hash_id, myRequst.chit_id, myRequst.page);

        }

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {

                if (progressDialogLogout != null && progressDialogLogout.isShowing()) {
                    progressDialogLogout.dismiss();
                }

                if (response != null && response.isSuccessful()) {
                    callRequest.onGetResponse(new Gson().toJson(response.body()));
                } else {
                    callRequest.onGetResponse("");
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Toast.makeText(c, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(c, "something went wrong", Toast.LENGTH_LONG).show(); // getting gtml 500
                    }
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {

                if (progressDialogLogout != null && progressDialogLogout.isShowing()) {
                    progressDialogLogout.dismiss();
                }

                callRequest.onGetResponse("");

            }
        });

    }

    public void searchSupplier(final Context c, final String searchs, final MyRequst myRequst) {
        ApiInterface apiService = ApiClient.getClient(c).create(ApiInterface.class);
        Call call;
        call = apiService.SearchSupplier(myRequst.cookie, searchs);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                try {
                    if (response.isSuccessful()) {

                        String a = new Gson().toJson(response.body());
                        ShareModel.getI().isAddShopAuto = "";
                        JSONObject jObjRes = new JSONObject(a);
                        JSONObject jObjdata = jObjRes.getJSONObject("response");
                        JSONObject jObjResponse = jObjdata.getJSONObject("data");
                        JSONArray ja_con = jObjResponse.getJSONArray("content");
                        SupplierNew supplierNew = new Gson().fromJson(ja_con.get(0).toString(), SupplierNew.class);
                        addAutoSupplierForConsumer(c, supplierNew, searchs, myRequst);

                    } else {
                        EventBus.getDefault().post("");

                        try {
                            JSONObject jObjError = new JSONObject(response.errorBody().string());
                            JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                            String jo_error = jObjErrorresponse.getString("errormsg");
                            SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(c);
                            sp.edit().putString(REFERRER_DATA, "").apply();
                            // Toast.makeText(MySupplierActivity.this, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // Log error here since request failed

                EventBus.getDefault().post("");

            }
        });


    }

    public void addAutoSupplierForConsumer(final Context c, SupplierNew supplierNew, final String refee, final MyRequst myRequst) {

        try {

            JSONObject jo = new JSONObject();
            JSONArray ja_new = new JSONArray();

            JSONObject jsonObj;
            jsonObj = new JSONObject();
            jsonObj.put("preference", "Yes");
            jsonObj.put("firstname", supplierNew.getFirstname());
            jsonObj.put("lastname", supplierNew.getLastname());
            jsonObj.put("contact_no", supplierNew.getContactNo());
            jsonObj.put("email_id", supplierNew.getEmailId());
            jsonObj.put("location", supplierNew.getLocation());
            jsonObj.put("role", "");

            jsonObj.put("contact_bridge_id", supplierNew.getBridgeId());
            jsonObj.put("account_type", supplierNew.getAccountType());
            jsonObj.put("username", refee);
            jsonObj.put("flag", "add");

            ja_new.put(jsonObj);
            jo.put("newdata", ja_new);
            JSONArray ja_old = new JSONArray();
            jo.put("olddata", ja_old);

            SupplierRequest catRequest = new SupplierRequest();
            catRequest = new Gson().fromJson(jo.toString(), SupplierRequest.class);
            ApiInterface apiService = ApiClient.getClient(c).create(ApiInterface.class);
            Call call = apiService.PutSupplier(myRequst.cookie, catRequest);
            call.enqueue(new Callback() {
                @Override
                public void onResponse(Call call, Response response) {

                    if (response.isSuccessful()) {
                       /* try {
                            postExternalContact(c, myRequst, refee);
                            if (!myRequst.userid_share.isEmpty()) {
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }*/
                        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(c);
                        sp.edit().putString(REFERRER_DATA, "").apply();
                        EventBus.getDefault().post("");


                        insertShareLink(c, myRequst, new CallRequest() {
                            @Override
                            public void onGetResponse(String response) {

                            }
                        });
                    } else {
                        EventBus.getDefault().post("");

                    }
                }

                @Override
                public void onFailure(Call call, Throwable t) {
                    // Log error here since request failed
                    EventBus.getDefault().post("");

                }
            });
        } catch (JSONException e) {
            EventBus.getDefault().post("");

            e.printStackTrace();
        }
    }

    public void postExternalContact(final Context context, MyRequst myRequst, String refer) throws JSONException {

        JsonObject joNewData = new JsonObject();
        joNewData.addProperty("flag", "add");
        joNewData.addProperty("purpose_type", "referrer");

        String userprofile = new SessionManager().getUserprofile();
        try {
            Userprofile userProfileData = new Gson().fromJson(userprofile, Userprofile.class);
            joNewData.addProperty("contact_number", userProfileData.getContact_no());
            joNewData.addProperty("contact_name", userProfileData.getUsername());

        } catch (Exception e) {
            e.printStackTrace();
        }

        joNewData.addProperty("contact_visit_count", "0"); // for new
        joNewData.addProperty("contact_visit_value", "0");

        JSONObject joField = new JSONObject();
        //joField.put("last_visit", DateTimeUtils.formatCurrentDateToStringWithTime());
        //joField.put("referrer_channel_id", spn_ref_channel.getSelectedItemPosition());

        joField.put("user_id", new SessionManager().getcurrentu_nm());
        joField.put("share_user_id", myRequst.userid_share);
        joField.put("referrer", refer);
        joNewData.addProperty("field_json_data", joField.toString()); // last + current

        JsonArray jsonElements = new JsonArray();
        jsonElements.add(joNewData);

        JsonObject jsonObject = new JsonObject();
        jsonObject.add("newdata", jsonElements);


        ApiInterface apiService = ApiClient.getClient(context).create(ApiInterface.class);
        Call call;
        call = apiService.postExternalContactByNo(myRequst.cookie, jsonObject);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                //myView.hideProgress();
                if (response != null && response.isSuccessful()) {
                    //myView.onGetInsertedContact(response);
                } else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Toast.makeText(context, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        try {
                            Toast.makeText(context, "something went wrong " + context.getPackageResourcePath(), Toast.LENGTH_LONG).show();
                        } catch (Exception ee) {
                            ee.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                //myView.hideProgress();

                try {
                    Toast.makeText(context, "Failed " + context.getPackageResourcePath(), Toast.LENGTH_LONG).show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


    }


    private DatabaseReference mFirebaseDatabase;
    private FirebaseDatabase mFirebaseInstance;


    public void insertShareLink(final Context context, final MyRequst myRequest, final CallRequest onGet) {

        mFirebaseInstance = FirebaseDatabase.getInstance();
        // get reference to 'users' node
        mFirebaseDatabase = mFirebaseInstance.getReference();

        DatabaseReference databaseReference = null;

        if (context.getString(R.string.BASEURL).equalsIgnoreCase("http://Dev.chitandbridge.com/prerelease/"))  // Dev
        {
            databaseReference = mFirebaseDatabase.child("test").child(referrer_detail_node).child(share_shop_node).child(myRequest.userid_share);
        } else { // prod
            databaseReference = mFirebaseDatabase.child("live").child(referrer_detail_node).child(share_shop_node).child(myRequest.userid_share);
        }

        try {
           /* JSONObject jo = new JSONObject();
            jo.put("sortby", new Date().getTime());
            jo.put("date", DateTimeUtils.formatCurrentDateToStringWithTime());// DateTimeUtils.formatCurrentDateToStringWithTime()
            jo.put("to", myRequest.share_detail);*/

            //FBViewModel fbViewModel = new FBViewModel(new Date().getTime(), DateTimeUtils.formatCurrentDateToStringWithTime(), myRequest.share_detail);
            //String req = new Gson().toJson(fbViewModel);
            databaseReference.push().setValue(myRequest.share_detail);


        } catch (Exception e) {
            e.printStackTrace();
        }

        Query q_who_ref = databaseReference.getRef();
        //databaseReference.push().setValue(myRequest.share_detail);
        q_who_ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                // onGet.onGetResponse("");
                String a = "";

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                // onGet.onGetResponse("");
                Toast.makeText(context, databaseError.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        databaseReference = null;

        if (context.getString(R.string.BASEURL).equalsIgnoreCase("http://Dev.chitandbridge.com/prerelease/"))  // Dev
        {
            databaseReference = mFirebaseDatabase.child("test").child(my_referral).child(myRequest.userid_share).child(share_shop_node);
        } else { // prod
            databaseReference = mFirebaseDatabase.child("live").child(my_referral).child(myRequest.userid_share).child(share_shop_node);
        }

        try {
            /*JSONObject jo = new JSONObject();
            jo.put("sortby", new Date().getTime());
            jo.put("date", DateTimeUtils.formatCurrentDateToStringWithTime());// DateTimeUtils.formatCurrentDateToStringWithTime()
            jo.put("to", myRequest.share_detail);
*/

            //FBViewModel fbViewModel = new FBViewModel(new Date().getTime(), DateTimeUtils.formatCurrentDateToStringWithTime(), myRequest.share_detail);
            //String req = new Gson().toJson(fbViewModel);
            databaseReference.push().setValue(myRequest.share_detail);


        } catch (Exception e) {
            e.printStackTrace();
        }
        //query = databaseReference;
        Query q_my_ref = databaseReference.getRef();
        q_my_ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                // onGet.onGetResponse("");
                String a = "";
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                // onGet.onGetResponse("");
                Toast.makeText(context, databaseError.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void getMyReferFromSupplier(final Context context, final MyRequst myRequest, final CallRequest onGet) {

        dialog = new ProgressDialog(context);
        dialog.setMessage("Fetching...");
        dialog.show();

        mFirebaseInstance = FirebaseDatabase.getInstance();
        // get reference to 'users' node
        mFirebaseDatabase = mFirebaseInstance.getReference();

        DatabaseReference databaseReference = null;

        if (context.getString(R.string.BASEURL).equalsIgnoreCase("http://Dev.chitandbridge.com/prerelease/"))  // Dev
        {
            databaseReference = mFirebaseDatabase.child("test").child(referrer_detail_node).child(myRequest.shop_id).child(myRequest.userid_share);
        } else { // prod
            databaseReference = mFirebaseDatabase.child("live").child(referrer_detail_node).child(myRequest.shop_id).child(myRequest.userid_share);
        }

        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                // onGet.onGetResponse("");

                if (dialog != null && dialog.isShowing()) dialog.dismiss();


                String list = "";
                if (dataSnapshot != null) {
                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                        list = list + " - " + postSnapshot.getValue() + "\n";
                    }
                }


                onGet.onGetResponse(list);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                // onGet.onGetResponse("");

                if (dialog != null && dialog.isShowing()) dialog.dismiss();


                onGet.onGetResponse("");
                Toast.makeText(context, databaseError.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void getWhoReferMeFromMenu(final Context context, final MyRequst myRequest, final CallRequest onGet) {

        dialog = new ProgressDialog(context);
        dialog.setMessage("Fetching...");
        dialog.show();

        mFirebaseInstance = FirebaseDatabase.getInstance();
        // get reference to 'users' node
        mFirebaseDatabase = mFirebaseInstance.getReference();

        DatabaseReference databaseReference = null;


        if (context.getString(R.string.BASEURL).equalsIgnoreCase("http://Dev.chitandbridge.com/prerelease/"))  // Dev
        {
            databaseReference = mFirebaseDatabase.child("test").child(referrer_detail_node).child(myRequest.shop_id).getRef();
        } else { // prod
            databaseReference = mFirebaseDatabase.child("live").child(referrer_detail_node).child(myRequest.shop_id).getRef();
        }


        Query whoRefMe = databaseReference.orderByKey();
        whoRefMe.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                // onGet.onGetResponse("");

                if (dialog != null && dialog.isShowing()) dialog.dismiss();

                String list = "";
                if (dataSnapshot != null) {
                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                        // TODO: handle the post
                        try {

                            try {

                                String sharer = postSnapshot.getKey();

                                list = list + "\n• " + sharer + "\n";

                                for (DataSnapshot childEntry : postSnapshot.getChildren()) {

                                    try {
                                        list = list + " - " + childEntry.getValue() + "\n";
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }


                /*Map<String, Map<String, String>> data = (Map<String, Map<String, String>>) dataSnapshot.getValue();

                String list = "";

                if (data != null) {
                    for (Map.Entry<String, Map<String, String>> entry : data.entrySet()) {
                        Map<String, String> value = null;
                        try {
                            value = entry.getValue();

                            String sharer = entry.getKey();

                            list = list + "\n• " + sharer + "\n";

                            for (Map.Entry<String, String> childEntry : value.entrySet()) {

                                try {
                                    list = list + " - " + childEntry.getValue() + "\n";
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }*/

                if (list.isEmpty()) list = "referred list is empty.";

                ShareModel.getI().whoReferMe = list;

                onGet.onGetResponse("");

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                // onGet.onGetResponse("");
                if (dialog != null && dialog.isShowing()) dialog.dismiss();

                onGet.onGetResponse("");

                Toast.makeText(context, databaseError.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

}