package com.cab.digicafe.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cab.digicafe.Activities.ProductDetiailsDialogue;
import com.cab.digicafe.Activities.healthTemplate.MyDoctorData;
import com.cab.digicafe.Activities.master.EnquiryOfPropertyActivity;
import com.cab.digicafe.Adapter.ctlg.SurveyOptionsListAdapter;
import com.cab.digicafe.Adapter.master.ImagePagerAdapter;
import com.cab.digicafe.Apputil;
import com.cab.digicafe.BaseActivity;
import com.cab.digicafe.Callback.OnLoadMoreListener;
import com.cab.digicafe.Database.SqlLiteDbHelper;
import com.cab.digicafe.Helper.Inad;
import com.cab.digicafe.Helper.LoadImg;
import com.cab.digicafe.Helper.SessionManager;
import com.cab.digicafe.Helper.SharedPrefUserDetail;
import com.cab.digicafe.Model.Catelog;
import com.cab.digicafe.Model.Metal;
import com.cab.digicafe.Model.ShareModel;
import com.cab.digicafe.Model.SurveyOptions;
import com.cab.digicafe.MyCustomClass.JsonObjParse;
import com.cab.digicafe.R;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import me.relex.circleindicator.CircleIndicator;

import static com.cab.digicafe.CatelogActivity.company_name;
import static com.cab.digicafe.CatelogActivity.username;

/**
 * Created by CSS on 02-11-2017.
 */

public class CatalogAdapter extends RecyclerView.Adapter<CatalogAdapter.MyViewHolder> {
    private List<Catelog> catererList;
    static boolean isstoreopen = false;
    private Context mContext;
    private SqlLiteDbHelper dbHelper;
    SessionManager sessionManager;
    private OnLoadMoreListener mOnLoadMoreListener;
    String currency = "";
    ImagePagerAdapter imagePagerAdapter;
    int pos = 0;
    MyDoctorData myDoctorData = new MyDoctorData();

    public interface OnItemClickListener {
        void onItemClick(Catelog item);

        void onShowPropComment(Catelog item, int pos);

        void onViewClick(Catelog item);

        void onadditem(Catelog qty);

        void ondecreasecount(Catelog qty);

        void onSurveyComment(Catelog qty, int pos);
    }

    private OnItemClickListener listener;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name, offer, email, mobile, qty, discountprice, tv_count, tvStock, tvTmp,
                tvPriceRangeWholesale, tvQtyWholesale, tvDaysWholesale, tvEnq, tvSurvey,
                tvSqFt, tvUds, tvCarpetArea, tvBedRoom, tvBathRoom, tvKitchen, tvfacing, tvPropPrice, tvPropertyStatus, discountPer;
        public ImageView addbtn, removebtn, ivVdoIndicator, ivEdit, ivDelete, ivPlus;
        private ImageView imageView, ivImgTemplate, ivWholeSale;
        RelativeLayout rl_bottom, rlMediaView, llEdit, llAddCatalogue, llShow;
        LinearLayout llStock, llCb, llWholeSale, llPropertyInfo, llSurveyInfo, llDis, llAddCart;
        CheckBox cbShopping;
        RadioButton rbProperty;
        ViewPager pager;
        RecyclerView rvSurveyOptions;
        CircleIndicator indicator;

        ImageView ivKitchen, ivBath, ivBed, ivShare, ivInfo;
        LinearLayout llBtn;
        FrameLayout FLMain;
        RelativeLayout RlPlus;
        TextView tvTemplateType;

        public MyViewHolder(View view) {
            super(view);
            ivKitchen = (ImageView) view.findViewById(R.id.ivKitchen);
            ivBath = (ImageView) view.findViewById(R.id.ivBath);
            ivBed = (ImageView) view.findViewById(R.id.ivBed);

            addbtn = (ImageView) view.findViewById(R.id.addbtn);
            removebtn = (ImageView) view.findViewById(R.id.removebtn);
            qty = (TextView) view.findViewById(R.id.qty);
            tv_count = (TextView) view.findViewById(R.id.tv_count);
            name = (TextView) view.findViewById(R.id.name);
            tvTmp = (TextView) view.findViewById(R.id.tvTmp);
            offer = (TextView) view.findViewById(R.id.offer);
            rl_bottom = (RelativeLayout) view.findViewById(R.id.rl_bottom);
            rlMediaView = (RelativeLayout) view.findViewById(R.id.rlMediaView);
            llStock = (LinearLayout) view.findViewById(R.id.llStock);
            llCb = (LinearLayout) view.findViewById(R.id.llCb);
            llWholeSale = (LinearLayout) view.findViewById(R.id.llWholeSale);
            llDis = (LinearLayout) view.findViewById(R.id.llDis);

            email = (TextView) view.findViewById(R.id.email);
            discountPer = (TextView) view.findViewById(R.id.discountPer);
            tvEnq = (TextView) view.findViewById(R.id.tvEnq);
            tvSurvey = (TextView) view.findViewById(R.id.tvSurvey);
            tvStock = (TextView) view.findViewById(R.id.tvStock);
            discountprice = (TextView) view.findViewById(R.id.discountprice);
            mobile = (TextView) view.findViewById(R.id.mobile);
            imageView = (ImageView) view.findViewById(R.id.profile_image);
            ivImgTemplate = (ImageView) view.findViewById(R.id.ivImgTemplate);
            ivWholeSale = (ImageView) view.findViewById(R.id.ivWholeSale);
            ivVdoIndicator = (ImageView) view.findViewById(R.id.ivVdoIndicator);
            cbShopping = (CheckBox) view.findViewById(R.id.cbShopping);
            rbProperty = (RadioButton) view.findViewById(R.id.rbProperty);
            pager = (ViewPager) view.findViewById(R.id.pager);
            indicator = (CircleIndicator) view.findViewById(R.id.indicator);

            tvPriceRangeWholesale = (TextView) view.findViewById(R.id.tvPriceRangeWholesale);
            tvQtyWholesale = (TextView) view.findViewById(R.id.tvQtyWholesale);
            tvDaysWholesale = (TextView) view.findViewById(R.id.tvDaysWholesale);


            llPropertyInfo = (LinearLayout) view.findViewById(R.id.llPropertyInfo);
            llSurveyInfo = (LinearLayout) view.findViewById(R.id.llSurveyInfo);
            tvSqFt = (TextView) view.findViewById(R.id.tvSqFt);
            tvUds = (TextView) view.findViewById(R.id.tvUds);
            tvCarpetArea = (TextView) view.findViewById(R.id.tvCarpetArea);
            tvBedRoom = (TextView) view.findViewById(R.id.tvBedRoom);
            tvBathRoom = (TextView) view.findViewById(R.id.tvBathRoom);
            tvKitchen = (TextView) view.findViewById(R.id.tvKitchen);
            tvfacing = (TextView) view.findViewById(R.id.tvfacing);
            tvPropPrice = (TextView) view.findViewById(R.id.tvPropPrice);
            tvPropertyStatus = (TextView) view.findViewById(R.id.tvPropertyStatus);
            rvSurveyOptions = (RecyclerView) view.findViewById(R.id.rvSurveyOptions);

            ivShare = (ImageView) view.findViewById(R.id.ivShare);
            llBtn = (LinearLayout) view.findViewById(R.id.llBtn);
            FLMain = (FrameLayout) view.findViewById(R.id.FLMain);
            RlPlus = (RelativeLayout) view.findViewById(R.id.RlPlus);

            tvTemplateType = (TextView) view.findViewById(R.id.tvTemplateType);

            llEdit = (RelativeLayout) view.findViewById(R.id.llEdit);
            ivEdit = (ImageView) view.findViewById(R.id.ivEdit);
            ivDelete = (ImageView) view.findViewById(R.id.ivDelete);
            ivPlus = (ImageView) view.findViewById(R.id.ivPlus);

            llAddCatalogue = (RelativeLayout) view.findViewById(R.id.llAddCatalogue);
            llShow = (RelativeLayout) view.findViewById(R.id.llShow);
            llAddCart = (LinearLayout) view.findViewById(R.id.llAddCart);

            ivInfo = (ImageView) view.findViewById(R.id.ivInfo);

        }
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }

    public CatalogAdapter(List<Catelog> moviesList, Context context, OnItemClickListener listener) {
        this.catererList = moviesList;
        this.mContext = context;
        dbHelper = new SqlLiteDbHelper(context);
        sessionManager = new SessionManager(context);
        this.listener = listener;
        currency = SharedPrefUserDetail.getString(context, SharedPrefUserDetail.chit_symbol_native, "") + " ";
        setHasStableIds(false);
    }

    String field_json_data = "", template_type = "";
    String service_type_of = "";
    Double sgst = 0.0;
    Double cgst = 0.0;
    int serviceId = -1;

    public void checkWholesaleFst(MyViewHolder holder, Catelog movie, int p) {
        holder.ivWholeSale.setVisibility(View.GONE);
        holder.llWholeSale.setVisibility(View.GONE);
        holder.llPropertyInfo.setVisibility(View.GONE);
        holder.tvPropertyStatus.setVisibility(View.GONE);
        holder.tvSurvey.setVisibility(View.GONE);
        holder.llSurveyInfo.setVisibility(View.GONE);
        holder.tvSurvey.setText("");
        String wholesale = JsonObjParse.getValueEmpty(movie.getInternal_json_data(), "wholesale");
        String property = JsonObjParse.getValueEmpty(movie.getInternal_json_data(), "property");
        String survey = JsonObjParse.getValueEmpty(movie.getInternal_json_data(), "survey");
        String doctor_template_type = JsonObjParse.getValueEmpty(movie.getInternal_json_data(), "doctor_template_type");

        holder.discountprice.setVisibility(View.VISIBLE);

        if (!wholesale.isEmpty()) {

            holder.addbtn.setVisibility(View.VISIBLE);

            holder.discountprice.setVisibility(View.VISIBLE);
            if (Double.parseDouble(movie.getMrp()) == 0.0) {
                holder.discountprice.setVisibility(View.INVISIBLE);
            }


            String minQ = JsonObjParse.getValueEmpty(wholesale, "minQ");
            holder.tvQtyWholesale.setText("Min. quantity : #" + minQ);

            String minPrice = JsonObjParse.getValueEmpty(wholesale, "minPrice");
            String maxPrice = JsonObjParse.getValueEmpty(wholesale, "maxPrice");

            if (!minPrice.isEmpty() && !maxPrice.isEmpty()) {
                Double minP = Double.valueOf(minPrice);
                Double maxP = Double.valueOf(maxPrice);

                holder.tvPriceRangeWholesale.setText("Price range : " + currency + Inad.getCurrencyDecimal(minP, mContext) + "" + " - " + currency + Inad.getCurrencyDecimal(maxP, mContext));

            } else if (!minPrice.isEmpty()) {
                Double minP = Double.valueOf(minPrice);
                holder.tvPriceRangeWholesale.setText("Minimum price : " + currency + Inad.getCurrencyDecimal(minP, mContext) + "");
            }

            String minDay = JsonObjParse.getValueEmpty(wholesale, "minDay");
            String maxDay = JsonObjParse.getValueEmpty(wholesale, "maxDay");

            if (minDay.isEmpty()) {
                holder.tvDaysWholesale.setText("Deliverd within " + maxDay + " day");
            } else if (maxDay.isEmpty()) {
                holder.tvDaysWholesale.setText("Deliverd within " + minDay + " day");
            } else {
                holder.tvDaysWholesale.setText("Deliverd within " + minDay + " - " + maxDay + " days");
            }

            holder.ivWholeSale.setVisibility(View.VISIBLE);
            holder.llWholeSale.setVisibility(View.VISIBLE);
            holder.ivWholeSale.setImageResource(R.drawable.ic_wholesale);

        } else if (!property.isEmpty()) {
            holder.ivWholeSale.setVisibility(View.VISIBLE);
            holder.ivWholeSale.setImageResource(R.drawable.ic_house);
            holder.llPropertyInfo.setVisibility(View.VISIBLE);

            if (sessionManager.getIsEmployess()) {
                holder.tvPropertyStatus.setVisibility(View.VISIBLE);
            }

            // holder.tvEnq.setVisibility(View.VISIBLE);

            String proprty_sqft = JsonObjParse.getValueEmpty(property, "proprty_sqft");
            holder.tvSqFt.setText("Sq.ft : " + proprty_sqft);

            String uds_sqft = JsonObjParse.getValueEmpty(property, "uds_sqft");
            holder.tvUds.setText("UDS(sq.ft) : " + uds_sqft);

            holder.tvUds.setVisibility(View.VISIBLE);
            if (uds_sqft.isEmpty()) holder.tvUds.setVisibility(View.GONE);

            String carpet_arae_sqft = JsonObjParse.getValueEmpty(property, "carpet_arae_sqft");
            holder.tvCarpetArea.setText("Carpet area(sq.ft) : " + carpet_arae_sqft);

            holder.tvCarpetArea.setVisibility(View.VISIBLE);
            if (carpet_arae_sqft.isEmpty()) holder.tvCarpetArea.setVisibility(View.GONE);


            String bed = JsonObjParse.getValueEmpty(property, "bed");
            holder.tvBedRoom.setText("BedRoom : " + bed);

            holder.tvBedRoom.setVisibility(View.VISIBLE);
            holder.ivBed.setVisibility(View.VISIBLE);
            if (bed.isEmpty()) {
                holder.tvBedRoom.setVisibility(View.GONE);
                holder.ivBed.setVisibility(View.GONE);

            }

            String bath = JsonObjParse.getValueEmpty(property, "bath");
            holder.tvBathRoom.setText("Bathroom : " + bath);

            holder.tvBathRoom.setVisibility(View.VISIBLE);
            holder.ivBath.setVisibility(View.VISIBLE);
            if (bath.isEmpty()) {
                holder.tvBathRoom.setVisibility(View.GONE);
                holder.ivBath.setVisibility(View.GONE);
            }
            String kitchen = JsonObjParse.getValueEmpty(property, "kitchen");
            holder.tvKitchen.setText("Kitchen : " + kitchen);

            holder.tvKitchen.setVisibility(View.VISIBLE);
            holder.ivKitchen.setVisibility(View.VISIBLE);
            if (kitchen.isEmpty()) {
                holder.tvKitchen.setVisibility(View.GONE);
                holder.ivKitchen.setVisibility(View.GONE);
            }

            String facing = JsonObjParse.getValueEmpty(property, "facing");
            holder.tvfacing.setText("Facing : " + facing);

            holder.tvPropPrice.setVisibility(View.GONE);
            Double mrpProp = 0.0;
            if (!movie.getMrp().isEmpty()) {
                mrpProp = Double.valueOf(movie.getMrp());
                holder.tvPropPrice.setVisibility(View.VISIBLE);
            }
            holder.tvPropPrice.setText("Price : " + currency + Inad.getCurrencyDecimal(mrpProp, mContext) + "");


            String status_property = JsonObjParse.getValueEmpty(property, "status_property");
            switch (status_property) {
                case ("red"):
                    holder.tvPropertyStatus.setTextColor(ContextCompat.getColor(mContext, R.color.rd));
                    break;
                case ("green"):
                    holder.tvPropertyStatus.setTextColor(ContextCompat.getColor(mContext, R.color.grn));
                    break;
                case ("amber"):
                    holder.tvPropertyStatus.setTextColor(ContextCompat.getColor(mContext, R.color.amber));
                    break;
                default:
                    holder.tvPropertyStatus.setTextColor(ContextCompat.getColor(mContext, R.color.rd));
                    break;
            }

            // Enquiry

            String currentUser = sessionManager.getcurrentu_nm();
            String userNmOfShop = JsonObjParse.getValueEmpty(field_json_data, "username");

            holder.tvEnq.setVisibility(View.GONE);
            if (sessionManager.getIsConsumer()) {
                holder.tvEnq.setVisibility(View.GONE); // for properties
            } else if (sessionManager.getisBusinesslogic()) {
                if (userNmOfShop.equalsIgnoreCase(currentUser)) {
                    holder.tvEnq.setVisibility(View.VISIBLE);
                }
            } else {
                int indexOfannot = currentUser.indexOf("@") + 1;
                currentUser = currentUser.substring(indexOfannot, currentUser.indexOf("."));

                if (userNmOfShop.equalsIgnoreCase(currentUser)) {
                    holder.tvEnq.setVisibility(View.VISIBLE);
                }
            }
        } else if (!survey.isEmpty()) {
            holder.ivWholeSale.setVisibility(View.VISIBLE);
            //holder.tvSurvey.setVisibility(View.VISIBLE);
            holder.llSurveyInfo.setVisibility(View.VISIBLE);
            holder.ivWholeSale.setImageResource(R.drawable.ic_survey);

            String commentBox = JsonObjParse.getValueEmpty(survey, "CommentBox");
            if (commentBox.equalsIgnoreCase("1")) holder.tvSurvey.setVisibility(View.VISIBLE);

            setSurveyInfo(holder, p, movie, survey);
        } else if (!doctor_template_type.isEmpty()) {
            holder.tvTemplateType.setVisibility(View.GONE);
            holder.tvTemplateType.setText(doctor_template_type);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = null;

        field_json_data = SharedPrefUserDetail.getString(mContext, SharedPrefUserDetail.field_json_data, "");
        template_type = JsonObjParse.getValueEmpty(field_json_data, "template_type");

        service_type_of = JsonObjParse.getValueEmpty(field_json_data, "service_type_of");

        if (template_type.equalsIgnoreCase("image")) {
            itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.catalog_list_row_image, parent, false);
        } else if (service_type_of.equalsIgnoreCase(mContext.getString(R.string.property))) {
            itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.catalog_list_row_image, parent, false);
        } else {
            itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.catalog_list_row, parent, false);
        }

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final Catelog movie = catererList.get(position);
        currency = movie.getSymbol_native() + " ";

        // wholesale
        holder.tvDaysWholesale.setText("");
        holder.tvQtyWholesale.setText("");
        holder.tvPriceRangeWholesale.setText("");

        checkWholesaleFst(holder, movie, position);

        holder.tvEnq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ShareModel.getI().purpose = mContext.getString(R.string.property);
                //ShareModel.getI().chit_name = movie.getDefault_name();
                ShareModel.getI().chit_name = movie.getCross_reference();

                Intent i = new Intent(mContext, EnquiryOfPropertyActivity.class);
                mContext.startActivity(i);
            }
        });

        holder.tvSurvey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onSurveyComment(catererList.get(position), position);
                /*ShareModel.getI().catelog = movie;

                Intent i = new Intent(mContext, SendSurveyActivity.class);
                mContext.startActivity(i);*/
            }
        });


        holder.tvPropertyStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onShowPropComment(movie, position);
            }
        });


        /////////////////////////////////////////////////

        int c_pos = position + 1;
        holder.tv_count.setText("[ " + c_pos + " ]");

        if (position == catererList.size() - 1) {
            holder.rl_bottom.setVisibility(View.VISIBLE);
        } else {
            holder.rl_bottom.setVisibility(View.GONE);
        }

        holder.name.setText(movie.getDefault_name());

        String surveyoption[] = movie.getDefault_name().split("///");

        if (surveyoption.length > 1) {
            holder.name.setText(surveyoption[0]);
            //holder.txt_serialno.setText("  "+surveyoption[1]);
        }

        if (surveyoption.length > 2) {

            //holder.txt_serialno.setText("  "+surveyoption[1]);
        }


        boolean isanyjsondata = false;

        try {

            Object json = new JSONTokener(movie.getTax_json_data()).nextValue();
            if (json instanceof JSONObject) {
                JSONObject jo_tax = new JSONObject(movie.getTax_json_data());
                if (movie.getTax_json_data() != null && jo_tax.length() > 0) {
                    isanyjsondata = true;
                }
            }

            json = new JSONTokener(movie.getField_json_data()).nextValue();
            if (json instanceof JSONObject) {
                JSONObject jo_field = new JSONObject(movie.getField_json_data());
                if (movie.getField_json_data() != null && jo_field.length() > 0) {
                    isanyjsondata = true;
                }
            }

            json = new JSONTokener(movie.getAdditional_json_data()).nextValue();
            if (json instanceof JSONObject) {
                JSONObject jo_add = new JSONObject(movie.getAdditional_json_data());
                if (movie.getAdditional_json_data() != null && jo_add.length() > 0) {
                    isanyjsondata = true;
                }
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
        /*holder.name.setPaintFlags(holder.name.getPaintFlags() & (~ Paint.UNDERLINE_TEXT_FLAG));
        if (isanyjsondata) {
            holder.name.setTextColor(Color.parseColor("#0000EE"));
            holder.name.setPaintFlags(holder.name.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            holder.name.setEnabled(true);

        } else {
            holder.name.setPaintFlags(holder.name.getPaintFlags() & (~ Paint.UNDERLINE_TEXT_FLAG));
            holder.name.setTextColor(Color.parseColor("#000000"));
            holder.name.setEnabled(false);

        }*/

        String caterorstatus = sessionManager.getcurrentcatererstatus();
        /*if (caterorstatus.equalsIgnoreCase("open"))
        {
            holder.qty.setVisibility(View.VISIBLE);
            holder.removebtn.setVisibility(View.VISIBLE);
            holder.addbtn.setVisibility(View.VISIBLE);
        }else
        {
            holder.qty.setVisibility(View.GONE);
            holder.removebtn.setVisibility(View.GONE);
            holder.addbtn.setVisibility(View.GONE);
        }*/

        holder.addbtn.setVisibility(View.VISIBLE);

        if (isstoreopen) {
            holder.addbtn.setVisibility(View.VISIBLE);
            holder.removebtn.setVisibility(View.VISIBLE);
        } else {
            holder.addbtn.setVisibility(View.INVISIBLE);
            holder.removebtn.setVisibility(View.INVISIBLE);
            holder.qty.setText("");
        }


        if (TextUtils.isEmpty(movie.getOffer())) {
            holder.offer.setVisibility(View.GONE);
        } else {
            holder.offer.setVisibility(View.VISIBLE);
            holder.offer.setText(movie.getOffer());
        }


        if (Double.parseDouble(movie.getMrp()) == Double.parseDouble(movie.getdiscountmrp())) {
            holder.email.setVisibility(View.GONE);
            holder.llDis.setVisibility(View.GONE);
            holder.email.setText("");

        } else {
            holder.email.setVisibility(View.VISIBLE);
            holder.llDis.setVisibility(View.VISIBLE);
        }

        int qty = catererList.get(position).getCartcount();

        String doctor_template_type = JsonObjParse.getValueEmpty(movie.getInternal_json_data(), "doctor_template_type");

        Double dis_mrp = Double.valueOf(movie.getdiscountmrp().toString());
       /* if (doctor_template_type.equalsIgnoreCase("RPD")) {
            Double Amrp = 0.0;
            Log.d("additional", JsonObjParse.getValueEmpty(catererList.get(position).getInternal_json_data(), "additionalPrice_rpd") + "");
            if (!JsonObjParse.getValueEmpty(catererList.get(position).getInternal_json_data(), "additionalPrice_rpd").isEmpty()) {
                Amrp = Double.valueOf(JsonObjParse.getValueEmpty(catererList.get(position).getInternal_json_data(), "additionalPrice_rpd"));

                dis_mrp = dis_mrp + Amrp;
            }
        }*/

        String property = JsonObjParse.getValueEmpty(movie.getInternal_json_data(), "property");
        String jewel = JsonObjParse.getValueEmpty(movie.getInternal_json_data(), "jewel");


        Double ded_price_val = 0.0, addi_price_val = 0.0;
        Double ded_mrp = 0.0, addi_mrp = 0.0;
        Double ded_perc_val = 0.0, addi_perc_val = 0.0;
        Double dis_perc = 0.0;

        if (!movie.getDiscount_percentage().equals(""))
            dis_perc = Double.valueOf(movie.getDiscount_percentage());

        if (!jewel.isEmpty()) {
            //String gold_today_price_catalogue = ShareModel.getI().gold_today_price;
            //String silver_today_price_catalogue = ShareModel.getI().silver_today_price;
            //String metal = JsonObjParse.getValueEmpty(jewel, "metal");

           /* if(metal.isEmpty())
            {

            }
            else if(!gold_today_price_catalogue.isEmpty()&&metal.equalsIgnoreCase("gold")){
                dis_mrp = Double.valueOf(gold_today_price_catalogue);
            }
            else if(!silver_today_price_catalogue.isEmpty()&&metal.equalsIgnoreCase("silver")){
                dis_mrp = Double.valueOf(silver_today_price_catalogue);
            }*/

            /*String metalPosId = JsonObjParse.getValueEmpty(jewel, "metalPosId");
            int posId = 0;
            if(!metalPosId.isEmpty()) posId = Integer.parseInt(metalPosId);

            try {
                Metal metal = ShareModel.getI().metal.alMetal.get(posId);
                if(!metal.metalPrice.isEmpty())
                {
                    dis_mrp = Double.valueOf(metal.metalPrice);
                }
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }


            String weight = JsonObjParse.getValueEmpty(jewel, "weight");

            if (!weight.isEmpty()) {
                float wt = Float.parseFloat(weight);
                //dis_mrp = (dis_mrp * wt)/10; // 10 gram
                dis_mrp = (dis_mrp * wt)/1; // 1 unit
            }*/

            try {
                String DeductionJewelPrice = JsonObjParse.getValueEmpty(jewel, "DeductionJewelPrice");
                ded_price_val = movie.getAdditionalDeductinalInfo(DeductionJewelPrice, dis_mrp);
                ded_mrp = Double.valueOf(getValues(dis_mrp, ded_perc_val, ded_price_val));
            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {
                String AdditionalJewelPrice = JsonObjParse.getValueEmpty(jewel, "AdditionalJewelPrice");
                addi_price_val = movie.getAdditionalDeductinalInfo(AdditionalJewelPrice, dis_mrp);
                addi_mrp = Double.valueOf(getValues(dis_mrp, addi_perc_val, addi_price_val));
            } catch (JSONException e) {
                e.printStackTrace();
            }

            //dis_mrp = dis_mrp - ded_price_val + addi_price_val;
            dis_mrp = dis_mrp - ded_mrp + addi_mrp;

        }

        sgst = 0.0;
        cgst = 0.0;

        Double price = 0.0;
        try {
            String tax = movie.getTax_json_data();
            if (tax != null) {
                setTax(tax);
               /*
               JSONObject obj = new JSONObject(tax);
               if (obj.has("VAT")) {
                    VAT = Double.valueOf(obj.getString("VAT"));
                    VAT = (dis_mrp / 100.0f) * VAT;
                } else {
                    sgst = Double.valueOf(obj.getString("SGST"));
                    cgst = Double.valueOf(obj.getString("CGST"));

                    sgst = (dis_mrp / 100.0f) * sgst;
                    cgst = (dis_mrp / 100.0f) * cgst;

                }*/

            }
            Log.e("stategst", String.valueOf(sgst));
            Log.e("stategst", String.valueOf(cgst));
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        sgst = (dis_mrp / 100.0f) * sgst;
        cgst = (dis_mrp / 100.0f) * cgst;


        //sgst = Math.round(sgst * 100.0) / 100.0; // 0.224 - > 0.22
        //cgst = Math.round(cgst * 100.0) / 100.0; // 0.224 - > 0.22


        //final NumberFormat format = NumberFormat.getCurrencyInstance();

        Double mrp = Double.valueOf(movie.getMrp());

        if (!property.isEmpty()) {
            mrp = Double.valueOf(movie.getMrpWithProperty().toString());

            if (Double.parseDouble(movie.getMrpWithProperty()) == Double.parseDouble(movie.getdiscountmrp())) {
                holder.email.setVisibility(View.GONE);
                holder.llDis.setVisibility(View.GONE);
                holder.email.setText("");
                holder.discountPer.setText("");
            } else {
                holder.email.setVisibility(View.VISIBLE);
                holder.llDis.setVisibility(View.VISIBLE);
            }
        }

        if (!jewel.isEmpty()) {
            mrp = Double.valueOf(movie.getMrpWithJewel().toString());
            if (Double.parseDouble(movie.getMrpWithJewel()) == Double.parseDouble(movie.getdiscountmrp())) {
                holder.email.setVisibility(View.GONE);
                holder.llDis.setVisibility(View.GONE);
                holder.email.setText("");
                holder.discountPer.setText("");
            } else {
                holder.email.setVisibility(View.VISIBLE);
                holder.llDis.setVisibility(View.VISIBLE);
            }
            mrp = mrp + cgst + sgst - ded_price_val + addi_price_val;
        } else {
            mrp = cgst + sgst + mrp;
        }
        //holder.email.setText(currency + String.format("%.2f", mrp));
        holder.email.setText(currency + Inad.getCurrencyDecimal(mrp, mContext));

        holder.email.setPaintFlags(holder.email.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        try {

            if (Integer.valueOf(dis_perc.intValue()) == 0) {
                holder.discountPer.setVisibility(View.GONE);
            }

            holder.discountPer.setText(Integer.valueOf(dis_perc.intValue()) + "%" + " off");
        } catch (Exception e) {
            e.printStackTrace();
        }

        price = dis_mrp + cgst + sgst;
        //holder.discountprice.setText(currency + String.format("%.2f", price));
        holder.discountprice.setText(currency + Inad.getCurrencyDecimal(price, mContext));
        movie.setCartcount(dbHelper.getproductcartcount(movie));
        if (movie.getCartcount() > 0) {
            holder.qty.setText(String.valueOf(movie.getCartcount()));
            holder.qty.setVisibility(View.VISIBLE);
            holder.removebtn.setVisibility(View.VISIBLE);
        } else {
            movie.setCartcount(0);
            holder.qty.setText(String.valueOf(movie.getCartcount()));
            holder.qty.setVisibility(View.INVISIBLE);
            holder.removebtn.setVisibility(View.INVISIBLE);
        }

        holder.addbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String wholesale = JsonObjParse.getValueEmpty(movie.getInternal_json_data(), "wholesale");

                if (!wholesale.isEmpty()) {

                    final int ii = position;

                    ProductDetiailsDialogue productDetiailsDialogue = new ProductDetiailsDialogue((Activity) mContext, catererList.get(ii), true, new ProductDetiailsDialogue.AddWholesale() {
                        @Override
                        public void addItems(Catelog catelog) {


                            holder.cbShopping.setChecked(true);
                            movie.setCartcount(catelog.getCartcount());
                            movie.setMrp(catelog.getMrp());
                            holder.qty.setText(String.valueOf(movie.getCartcount()));
                            holder.qty.setVisibility(View.VISIBLE);
                            holder.removebtn.setVisibility(View.INVISIBLE);

                            listener.onadditem(movie);

                        }

                        @Override
                        public void deleteItems(Catelog catelog) {

                            movie.setCartcount(0);

                            if (movie.getCartcount() >= 1) {

                                holder.cbShopping.setChecked(true);
                                holder.qty.setText(String.valueOf(movie.getCartcount()));
                                holder.qty.setVisibility(View.VISIBLE);

                            } else {
                                holder.cbShopping.setChecked(false);
                                holder.qty.setVisibility(View.INVISIBLE);
                                holder.removebtn.setVisibility(View.INVISIBLE);
                            }
                            listener.ondecreasecount(movie);

                            holder.discountprice.setVisibility(View.INVISIBLE);

                        }
                    });
                    productDetiailsDialogue.show();

                } else {
                    holder.cbShopping.setChecked(true);
                    movie.setCartcount(movie.getCartcount() + 1);
                    holder.qty.setText(String.valueOf(movie.getCartcount()));
                    holder.qty.setVisibility(View.VISIBLE);
                    holder.removebtn.setVisibility(View.VISIBLE);
                    listener.onadditem(movie);
                }


            }
        });

        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(movie);
            }
        });

        holder.removebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                movie.setCartcount(movie.getCartcount() - 1);

                if (movie.getCartcount() >= 1) {

                    holder.cbShopping.setChecked(true);
                    holder.qty.setText(String.valueOf(movie.getCartcount()));
                    holder.qty.setVisibility(View.VISIBLE);

                } else {
                    holder.cbShopping.setChecked(false);
                    holder.qty.setVisibility(View.INVISIBLE);
                    holder.removebtn.setVisibility(View.INVISIBLE);
                }
                listener.ondecreasecount(movie);
            }
        });
        holder.cbShopping.setChecked(false);
        holder.rbProperty.setChecked(false);
        if (movie.getCartcount() == 1) {
            holder.cbShopping.setChecked(true);
            holder.rbProperty.setChecked(true);
        }
        if (service_type_of.equalsIgnoreCase(mContext.getString(R.string.jewel))) {

            String metal_list = JsonObjParse.getValueEmpty(field_json_data, "metal_list");

            Metal metal = ShareModel.getI().metal;

            if (metal == null) metal = new Metal();
            serviceId = metal.serviceId;
        }


        if (serviceId == 3 || service_type_of.equalsIgnoreCase(mContext.getString(R.string.catalogue_only))) {
            holder.addbtn.setVisibility(View.INVISIBLE);
            holder.removebtn.setVisibility(View.INVISIBLE);
            holder.qty.setText("");

            holder.discountprice.setVisibility(View.VISIBLE);
            if (Double.parseDouble(movie.getMrp()) == 0.0) {
                holder.discountprice.setVisibility(View.INVISIBLE);
            }
        }

        holder.rbProperty.setVisibility(View.GONE);
        holder.cbShopping.setVisibility(View.GONE);

        // shopping []

        if (serviceId == 2 || service_type_of.equalsIgnoreCase(mContext.getString(R.string.shopping_cart_cb))) {
            holder.cbShopping.setVisibility(View.VISIBLE);
            holder.addbtn.setVisibility(View.INVISIBLE);
            holder.removebtn.setVisibility(View.INVISIBLE);
            holder.qty.setText("");
        } else if (serviceId == 1 || service_type_of.equalsIgnoreCase(mContext.getString(R.string.property))) {
            holder.rbProperty.setVisibility(View.VISIBLE);
            holder.addbtn.setVisibility(View.INVISIBLE);
            holder.removebtn.setVisibility(View.INVISIBLE);
            holder.qty.setText("");
        } else if (serviceId == 1 || service_type_of.equalsIgnoreCase(mContext.getString(R.string.delivery))) {
            holder.rbProperty.setVisibility(View.VISIBLE);
            holder.addbtn.setVisibility(View.INVISIBLE);
            holder.removebtn.setVisibility(View.INVISIBLE);
            holder.qty.setText("");
        }

        holder.name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String survey = JsonObjParse.getValueEmpty(movie.getInternal_json_data(), "survey");

                if (!survey.isEmpty())
                    return;

                int ii = position;

                ProductDetiailsDialogue productDetiailsDialogue = new ProductDetiailsDialogue((Activity) mContext, catererList.get(ii), false, new ProductDetiailsDialogue.AddWholesale() {
                    @Override
                    public void addItems(Catelog catelog) {

                    }

                    @Override
                    public void deleteItems(Catelog catelog) {
                    }
                });
                productDetiailsDialogue.show();

              /*  new CatelogDetailDialog((Activity) mContext, new CatelogDetailDialog.OnDialogClickListener() {
                    @Override
                    public void onDialogImageRunClick(int positon) {
                        if (positon == 0) {
                        }
                    }
                }, catererList.get(ii)).show();*/
            }
        });

        if (doctor_template_type.equalsIgnoreCase("RPD") || doctor_template_type.equalsIgnoreCase("Precision Attachment")) {
            holder.ivInfo.setVisibility(View.VISIBLE);
        } else {
            holder.ivInfo.setVisibility(View.GONE);
        }

        holder.ivInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String survey = JsonObjParse.getValueEmpty(movie.getInternal_json_data(), "survey");

                if (!survey.isEmpty())
                    return;

                int ii = position;

                ProductDetiailsDialogue productDetiailsDialogue = new ProductDetiailsDialogue((Activity) mContext, catererList.get(ii), false, new ProductDetiailsDialogue.AddWholesale() {
                    @Override
                    public void addItems(Catelog catelog) {

                    }

                    @Override
                    public void deleteItems(Catelog catelog) {
                    }
                });
                productDetiailsDialogue.show();
            }
        });

        holder.llStock.setVisibility(View.GONE);
        if (movie.getAvailable_stock() != null && !movie.getAvailable_stock().isEmpty()) {

            holder.tvStock.setText("#" + movie.getAvailable_stock() + "");

            String show_stock_position = JsonObjParse.getValueEmpty(field_json_data, "show_stock_position");

            if (show_stock_position.equalsIgnoreCase("yes")) {
                holder.llStock.setVisibility(View.VISIBLE);
            } else {
                holder.llStock.setVisibility(View.GONE);
            }
        }

        holder.imageView.setVisibility(View.GONE);
        String imagreurl = "";

        holder.ivVdoIndicator.setVisibility(View.GONE);

        ArrayList<String> pb = new ArrayList<>();
        if (movie.getImageArr() != null) {
            if (movie.getImageArr().size() > 0) {
                pb = movie.getImageArr();
                holder.imageView.setVisibility(View.GONE);
                imagreurl = pb.get(0).trim();

               /* for (int i=0;i<pb.size();i++){
                    if (pb.get(i).contains("youtube")) {
                        holder.ivVdoIndicator.setVisibility(View.VISIBLE);
                        break;
                    }
                }*/
            }
            Log.d("pb", pb.size() + "");
        }


        if (template_type.equalsIgnoreCase("image")) {
            holder.imageView.setVisibility(View.GONE);

            // loadImg.loadCategoryImg(mContext, imagreurl, holder.ivImgTemplate);

            try {
                imagePagerAdapter = new ImagePagerAdapter(mContext, pb, movie.getDefault_name());
                holder.pager.setAdapter(imagePagerAdapter);
                holder.indicator.setViewPager(holder.pager);

            } catch (Exception e) {
                e.printStackTrace();
            }

            //holder.llCb.setBackground(null);
            //holder.llCb.setBackground(ContextCompat.getDrawable(mContext, R.drawable.shadow));
        } else {
            try {
                loadImg.loadCategoryImg(mContext, imagreurl, holder.imageView);
               /* Picasso.with(mContext)
                        .load(imagreurl)
                        .into(holder.imageView, new Callback() {
                            @Override
                            public void onSuccess() {
                                holder.imageView.setVisibility(View.VISIBLE);
                            }

                            @Override
                            public void onError() {
                                holder.imageView.setVisibility(View.GONE);
                            }
                        });*/
            } catch (Exception e) {
                e.printStackTrace();
                holder.imageView.setVisibility(View.GONE);
            }
            holder.ivShare.setVisibility(View.GONE);
            holder.llBtn.setVisibility(View.GONE);
        }

        if (pb != null) {
            holder.ivImgTemplate.setVisibility(View.VISIBLE);
            if (template_type.equalsIgnoreCase("image")) {
                holder.ivShare.setVisibility(View.VISIBLE);
                holder.llBtn.setVisibility(View.VISIBLE);
            } else {
                holder.ivShare.setVisibility(View.GONE);
                holder.llBtn.setVisibility(View.GONE);
            }

        } else {
            holder.ivImgTemplate.setVisibility(View.GONE);
            holder.ivShare.setVisibility(View.GONE);
            holder.llBtn.setVisibility(View.GONE);
        }

        String wholesale = JsonObjParse.getValueEmpty(movie.getInternal_json_data(), "wholesale");
        String survey = JsonObjParse.getValueEmpty(movie.getInternal_json_data(), "survey");

        if (!wholesale.isEmpty()) {
            holder.removebtn.setVisibility(View.INVISIBLE);
            holder.discountprice.setVisibility(View.INVISIBLE);

            if (movie.getCartcount() > 0) {
                Catelog c = Apputil.getCatelog(dbHelper, movie);
                movie.setMrp(c.getMrp());
            }
        }

        if (!survey.isEmpty()) {
            holder.addbtn.setVisibility(View.GONE);
            holder.removebtn.setVisibility(View.GONE);
            holder.discountprice.setVisibility(View.GONE);
            holder.email.setVisibility(View.GONE);
            holder.llDis.setVisibility(View.GONE);
            holder.qty.setVisibility(View.GONE);
        }

        holder.ivShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                holder.llBtn.setVisibility(View.GONE);
                holder.RlPlus.setVisibility(View.GONE);

                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
                String currentDateandTime = sdf.format(new Date());
                try {
                    holder.FLMain.setDrawingCacheEnabled(true);
                    Bitmap b = Bitmap.createBitmap(holder.FLMain.getDrawingCache());
                    holder.FLMain.setDrawingCacheEnabled(false);
                    String path = Environment.getExternalStorageDirectory().toString() + "/" + mContext.getString(R.string.app_name);
                    File root = new File(path);///*Environment.getExternalStorageState()+*/"/DCIM/Camera/" + File.separator + "Birthday Song with Name" + File.separator + getIntent().getStringExtra("from") + File.separator);
                    root.mkdirs();

                    File sdImageMainDirectory = null;
                    sdImageMainDirectory = new File(root, mContext.getString(R.string.app_name) + currentDateandTime + ".jpg");

                    FileOutputStream fOut = new FileOutputStream(sdImageMainDirectory);
                    b.compress(Bitmap.CompressFormat.PNG, 90, fOut);
                    fOut.flush();
                    fOut.close();

                    MediaScannerConnection.scanFile(mContext,
                            new String[]{sdImageMainDirectory.toString()}, null,
                            new MediaScannerConnection.OnScanCompletedListener() {
                                public void onScanCompleted(String path, Uri uri) {

                                }
                            });
                    //   Toast.makeText(mContext, "Download Completed", Toast.LENGTH_SHORT).show();
                    Uri uri = FileProvider.getUriForFile(mContext, mContext.getPackageName() + ".provider", new File(sdImageMainDirectory.getAbsolutePath()));

/*                  ArrayList<Uri> imageUriArray = new ArrayList<Uri>();
                    imageUriArray.add(uri);
                    imageUriArray.add(uri2);*/

                    String shareUrl = "http://play.google.com/store/apps/details?id=" + mContext.getString(R.string.consumer_link) + "&referrer=" + username.toLowerCase() + "-" + new SessionManager().getUserWithAggregator();

                    String extTxt = shareUrl;

                    Uri shareUri = ((BaseActivity) mContext).buildDeepLink(Uri.parse(shareUrl), 0);
                    extTxt = shareUri.toString();

                    Intent share = new Intent(Intent.ACTION_SEND);
                    share.setType("image/jpg");
                    share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                    share.putExtra(Intent.EXTRA_SUBJECT, company_name);
                    // share.putExtra(Intent.EXTRA_TEXT, extTxt);
                    // share.setAction(Intent.ACTION_SEND_MULTIPLE);
                    share.putExtra(Intent.EXTRA_STREAM, uri);
                    share.putExtra(Intent.EXTRA_TEXT, extTxt);
                    mContext.startActivity(Intent.createChooser(share, "Share"));
                    holder.llBtn.setVisibility(View.VISIBLE);
                    holder.RlPlus.setVisibility(View.VISIBLE);

                } catch (Exception e) {
                    e.printStackTrace();
                    holder.llBtn.setVisibility(View.VISIBLE);
                    holder.RlPlus.setVisibility(View.VISIBLE);
                }


              /*  String name = "Name : " + movie.getDefault_name();

                String offer = "";
                if (!movie.getOffer().equalsIgnoreCase("")) {
                    offer = "\nOffer : " + movie.getOffer();
                } else {
                    offer = "";
                }

                Double dis = 0.0;
                String discount = "";
                if (!movie.getDiscount_percentage().equals("")) {
                    discount = "\nDiscount : " + holder.discountPer.getText().toString();
                    dis = Double.valueOf(movie.getDiscount_percentage());
                } else {
                    discount = "";
                }

                String mrp = "\nPrice : " + holder.discountprice.getText().toString();

                String imgurl = "";

                try {
                    imgurl = movie.getImageArr().get(holder.pager.getCurrentItem());
                } catch (Exception e) {
                }

                String url = "https://play.google.com/store/apps/details?id=com.cab.digimart";
                String shareUrl = "";
                if (Integer.valueOf(dis.intValue()) == 0) {
                    shareUrl = name + offer + mrp;
                } else {
                    shareUrl = name + offer + discount + mrp;
                }


                String extTxt = url + "\n\n" + shareUrl + "\n\n" + imgurl;*/

                // Uri shareUri = ((BaseActivity) mContext).buildDeepLink(Uri.parse(shareUrl), 0);

            }
        });

        if (!doctor_template_type.isEmpty()) {
            if (doctor_template_type.equalsIgnoreCase("None")) {
                holder.addbtn.setVisibility(View.VISIBLE);
                if (movie.getCartcount() > 0) {
                    holder.qty.setVisibility(View.VISIBLE);
                } else {
                    holder.qty.setVisibility(View.GONE);
                }
            } else {
                holder.addbtn.setVisibility(View.GONE);
                holder.qty.setVisibility(View.GONE);
                holder.RlPlus.setVisibility(View.GONE);
                holder.llEdit.setVisibility(View.VISIBLE);
                if (movie.getCartcount() > 0) {
                    holder.ivEdit.setVisibility(View.VISIBLE);
                    holder.ivDelete.setVisibility(View.VISIBLE);
                    holder.ivPlus.setVisibility(View.GONE);
                } else {
                    holder.ivEdit.setVisibility(View.GONE);
                    holder.ivDelete.setVisibility(View.GONE);
                    holder.ivPlus.setVisibility(View.VISIBLE);
                }
            }
        }

        if (!ShareModel.getI().dbUnderMaster.isEmpty()) {
            holder.qty.setVisibility(View.INVISIBLE);
            holder.addbtn.setVisibility(View.INVISIBLE);
            holder.removebtn.setVisibility(View.INVISIBLE);
            holder.llEdit.setVisibility(View.INVISIBLE);
            holder.llAddCatalogue.setVisibility(View.VISIBLE);
            holder.llShow.setVisibility(View.VISIBLE);
            holder.llAddCart.setVisibility(View.VISIBLE);

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onViewClick(catererList.get(position));
                }
            });

        } else {
            holder.llAddCatalogue.setVisibility(View.INVISIBLE);
            holder.llShow.setVisibility(View.INVISIBLE);
            holder.llAddCart.setVisibility(View.INVISIBLE);
            holder.llCb.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (serviceId == 2 || service_type_of.equalsIgnoreCase(mContext.getString(R.string.shopping_cart_cb))) {
                        if (catererList.get(position).getCartcount() == 1) {
                            holder.removebtn.performClick();
                        } else {

                            String max = JsonObjParse.getValueEmpty(field_json_data, "max");
                            if (max.isEmpty()) {
                                holder.addbtn.performClick();
                            } else {
                                int maxI = Integer.parseInt(max);
                                if (dbHelper.getcartcount() >= maxI) {
                                    Toast.makeText(mContext, "Max limit is " + maxI, Toast.LENGTH_LONG).show();
                                } else {
                                    holder.addbtn.performClick();
                                }
                            }
                        }
                        holder.qty.setVisibility(View.INVISIBLE);
                        holder.removebtn.setVisibility(View.INVISIBLE);
                    } else if (serviceId == 1 || service_type_of.equalsIgnoreCase(mContext.getString(R.string.property))) {
                        if (catererList.get(position).getCartcount() != 1) {
                            dbHelper.deleteallfromcart();
                            holder.addbtn.performClick();
                            notifyDataSetChanged();
                        }
                    } else if (serviceId == 1 || service_type_of.equalsIgnoreCase(mContext.getString(R.string.delivery))) {
                        if (catererList.get(position).getCartcount() != 1) {
                            dbHelper.deleteallfromcart();
                            holder.addbtn.performClick();
                            notifyDataSetChanged();
                        }
                    }
                   /* String doctor_template_type = JsonObjParse.getValueEmpty(catererList.get(position).getInternal_json_data(), "doctor_template_type");

                    if (!doctor_template_type.isEmpty()) {

                        if (doctor_template_type.equalsIgnoreCase("None")) {

                        } else {
                            for (int i = 0; i < myDoctorData.getTemplateList().size(); i++) {
                                if (myDoctorData.getTemplateList().get(i).getTemplateName().equalsIgnoreCase(doctor_template_type)) {
                                    Intent intent = new Intent(mContext, myDoctorData.getTemplateList().get(i).getRedirectionActivity());
                                    intent.putExtra("Catelog", new Gson().toJson(catererList.get(position)));
                                    mContext.startActivity(intent);
                                    break;
                                }
                            }
                        }
                    }*/
                }
            });
        }

        holder.llShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String doctor_template_type = JsonObjParse.getValueEmpty(catererList.get(position).getInternal_json_data(), "doctor_template_type");

                if (!doctor_template_type.isEmpty()) {

                    if (doctor_template_type.equalsIgnoreCase("None")) {

                    } else {
                        for (int i = 0; i < myDoctorData.getTemplateList().size(); i++) {
                            if (myDoctorData.getTemplateList().get(i).getTemplateName().equalsIgnoreCase(doctor_template_type)) {
                                Intent intent = new Intent(mContext, myDoctorData.getTemplateList().get(i).getRedirectionActivity());
                                //  intent.putExtra("isOnlyShow", true);
                                intent.putExtra("Catelog", new Gson().toJson(catererList.get(position)));
                                intent.putExtra("Database", true);
                                mContext.startActivity(intent);
                                break;
                            }
                        }
                    }
                }
            }
        });
        holder.ivPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String doctor_template_type = JsonObjParse.getValueEmpty(catererList.get(position).getInternal_json_data(), "doctor_template_type");

                if (!doctor_template_type.isEmpty()) {

                    if (doctor_template_type.equalsIgnoreCase("None")) {

                    } else {
                        for (int i = 0; i < myDoctorData.getTemplateList().size(); i++) {
                            if (myDoctorData.getTemplateList().get(i).getTemplateName().equalsIgnoreCase(doctor_template_type)) {
                                Intent intent = new Intent(mContext, myDoctorData.getTemplateList().get(i).getRedirectionActivity());
                                intent.putExtra("Catelog", new Gson().toJson(catererList.get(position)));
                                mContext.startActivity(intent);
                                break;
                            }
                        }
                    }
                }
            }
        });
        holder.ivEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String doctor_template_type = JsonObjParse.getValueEmpty(catererList.get(position).getInternal_json_data(), "doctor_template_type");

                if (!doctor_template_type.isEmpty()) {

                    if (doctor_template_type.equalsIgnoreCase("None")) {

                    } else {
                        for (int i = 0; i < myDoctorData.getTemplateList().size(); i++) {
                            if (myDoctorData.getTemplateList().get(i).getTemplateName().equalsIgnoreCase(doctor_template_type)) {
                                Intent intent = new Intent(mContext, myDoctorData.getTemplateList().get(i).getRedirectionActivity());
                                intent.putExtra("Catelog", new Gson().toJson(catererList.get(position)));
                                mContext.startActivity(intent);
                                break;
                            }
                        }
                    }
                }
            }
        });

        holder.ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                catererList.get(position).setCartcount(0);
                listener.ondecreasecount(catererList.get(position));
                notifyDataSetChanged();
            }
        });

    }

    LoadImg loadImg = new LoadImg();

    @Override
    public int getItemCount() {
        return catererList.size();
    }

    public void setFilter(List<Catelog> countryModels) {
        catererList = new ArrayList<>();
        catererList.addAll(countryModels);
        notifyDataSetChanged();
    }

    public void setitem(List<Catelog> products, boolean isstoreopen) {
        this.isstoreopen = isstoreopen;
        catererList = new ArrayList<>();
        catererList.addAll(products);
        Log.e("aliassearch", catererList.size() + "");
        notifyDataSetChanged();

    }

    public void setitem(int pos, String comment) {

        Catelog c = catererList.get(pos);
        //c.setMrp(surveyOptions.value + "");
        c.setCartcount(1);
        c.commentForfSurevey = comment;
        c.setDefault_name(c.getDefault_name());

       /* String commentList[] = c.getDefault_name().split(";/;");

        if(commentList.length>1)
        {
            c.setDefault_name(commentList[0]+";/;"+comment);
        }
        else {
            c.setDefault_name(c.getDefault_name()+";/;"+comment);
        }*/


        //Apputil.inserttocartWholesale(dbHelper, c, "");
        //notifyDataSetChanged();

        listener.onadditem(c);
        notifyItemChanged(pos);

    }

    public void setTax(String field_json_data) throws JSONException {

        if (field_json_data != null) {

            boolean isF1 = true;

            JSONObject joTax = new JSONObject(field_json_data);

            Iterator keys = joTax.keys();

            while (keys.hasNext()) {
                try {
                    String key = (String) keys.next();

                    String taxVal = JsonObjParse.getValueFromJsonObj(joTax, key);

                    if (isF1) {
                        isF1 = false;
                        sgst = Double.valueOf(taxVal);


                    } else {
                        cgst = Double.valueOf(taxVal);

                    }

                    Log.e("key", key);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public int getExtraPropertyInfo(String extraInfo) throws JSONException {
        int totalInfoVal = 0;
        if (extraInfo != null && !extraInfo.isEmpty()) {
            JSONObject joExtra = new JSONObject(extraInfo);
            Iterator keys = joExtra.keys();
            int i = 0;
            while (keys.hasNext()) {
                try {
                    String key = (String) keys.next();
                    String info = JsonObjParse.getValueFromJsonObj(joExtra, key);

                    if (!info.isEmpty()) {
                        int infoVal = Integer.parseInt(info);
                        totalInfoVal = totalInfoVal + infoVal;
                    }
                   /* switch (i) {
                        case 0:


                            break;
                        case 1:

                            break;
                        case 2:


                            break;
                        case 3:

                            break;
                        case 4:
                            break;
                    }
                    i++;*/
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return totalInfoVal;
    }

    ArrayList<SurveyOptions> alOptions = new ArrayList<>();

    public void setSurveyInfo(final MyViewHolder holder, int pos, final Catelog catelog, String survey) {

        String optionsList = JsonObjParse.getValueEmpty(survey, "SurveyOptions");

        // Fst enter to db at time of selection and then check here with DB

        SurveyOptions surveyOptions = new Gson().fromJson(optionsList, SurveyOptions.class);
        alOptions = surveyOptions.alOptions;

        if (alOptions == null) alOptions = new ArrayList<>();


        if (Apputil.isCatExist(dbHelper, catelog)) {
            Catelog c = Apputil.getCatelog(dbHelper, catelog);
            String surveyoption[] = c.getDefault_name().split("///");
            if (surveyoption.length > 2) holder.tvSurvey.setText(surveyoption[2]);

            float val = Float.parseFloat(c.getMrp()); // value should be uniqe
            for (int i = 0; i < alOptions.size(); i++) {
                int str_val = alOptions.get(i).value;
                if (str_val == val) {
                    alOptions.get(i).isEdit = true;
                    break;
                }
            }
        }

        final SurveyOptionsListAdapter surveyOptionsListAdapter = new SurveyOptionsListAdapter(pos, alOptions, mContext, new SurveyOptionsListAdapter.OnItemClickListener() {
            @Override
            public void onDelete(int item) {

            }

            @Override
            public void onEdit(int item, ArrayList<SurveyOptions> movies) {

                SurveyOptions surveyOptions = movies.get(item);
                Catelog c = catererList.get(surveyOptions.catPos);
                c.setMrp(surveyOptions.value + "");
                c.setCartcount(1);
                c.optionForSurvey = surveyOptions.option;

               /* String surveyoption[] = c.getDefault_name().split("///");

                if(surveyoption.length>1)
                {
                    c.setDefault_name(surveyoption[0]+"///"+surveyOptions.option);
                }
                else {
                    c.setDefault_name(c.getDefault_name()+"///"+surveyOptions.option);
                }*/

                c.setDefault_name(c.getDefault_name());

                //Apputil.inserttocartWholesale(dbHelper, c, "");
                //notifyDataSetChanged();

                holder.cbShopping.setChecked(true);
                listener.onadditem(c);
                notifyItemChanged(surveyOptions.catPos);
                //notifyDataSetChanged();
            }
        }, false);

        //rvSurveyOptions.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        holder.rvSurveyOptions.setLayoutManager(new GridLayoutManager(mContext, 2));
        holder.rvSurveyOptions.setAdapter(surveyOptionsListAdapter);
        holder.rvSurveyOptions.setNestedScrollingEnabled(false);
    }

    public String getValues(Double mrpprice, Double discount_percentage, Double discounted_price) {
        Double discountprice = 0.0;

        if (discount_percentage > 0) {
            if (mrpprice > 0) {
                Double totalDisc = (mrpprice * (discount_percentage / 100));
                discountprice = totalDisc;
            }
        } else if (discounted_price > 0) {
            discountprice = discounted_price;
        }

        return String.valueOf(discountprice);
    }
}