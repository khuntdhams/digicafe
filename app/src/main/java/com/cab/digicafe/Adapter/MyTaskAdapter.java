package com.cab.digicafe.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cab.digicafe.Database.SettingDB;
import com.cab.digicafe.Database.SettingModel;
import com.cab.digicafe.Helper.Inad;
import com.cab.digicafe.Model.Chit;
import com.cab.digicafe.Model.ChitDetails;
import com.cab.digicafe.Model.Chitlog;
import com.cab.digicafe.Model.Chitproducts;
import com.cab.digicafe.MyCustomClass.GetProductDetail;
import com.cab.digicafe.MyCustomClass.JsonObjParse;
import com.cab.digicafe.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by CSS on 02-11-2017.
 */

public class MyTaskAdapter extends RecyclerView.Adapter<MyTaskAdapter.MyViewHolder> {

    private List<Chit> catererList;
    private Context mContext;
    public static String tab = "";
    public static boolean isbusiness = false;
    SettingModel sm = new SettingModel();
    String currency = "";

    public interface OnItemClickListener {
        void onItemClick(Chit item);

        void onItemLongClick(Chit item, int pos);
    }

    private MyTaskAdapter.OnItemClickListener listener;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_mob, caterername, txt_orderid, txt_orderdate, txt_itemcount, txt_chitamount, txt_paymentmode, txt_status, txt_purpose;
        public RelativeLayout viewForeground, view_background_left, rl_address, rl_del_date;
        private ImageView iv_status, iv_status_left;
        LinearLayout mainlayout;

        TextView tv_date, tv_address, tv_status, tv_status_left, tv_count;
        FrameLayout fl;

        @BindView(R.id.ll_bottom)
        LinearLayout ll_bottom;

        // Detal
        @BindView(R.id.ll_detail)
        LinearLayout ll_detail;

        @BindView(R.id.ll_detail_show)
        LinearLayout ll_detail_show;

        @BindView(R.id.tv_remark)
        TextView tv_remark;

        @BindView(R.id.txt_qty)
        TextView txt_qty;


        @BindView(R.id.txt_price)
        TextView txt_price;

        @BindView(R.id.amountlbl)
        TextView amountlbl;

        @BindView(R.id.rv_detail)
        RecyclerView rv_detail;

        @BindView(R.id.rl_pb)
        RelativeLayout rl_pb;

        // Status
        @BindView(R.id.ll_status)
        LinearLayout ll_status;

        @BindView(R.id.ll_status_show)
        LinearLayout ll_status_show;

        @BindView(R.id.rv_status)
        RecyclerView rv_status;

        // Status

        @BindView(R.id.ll_summary)
        LinearLayout ll_summary;

        @BindView(R.id.ll_summary_show)
        LinearLayout ll_summary_show;

        @BindView(R.id.ll_payu)
        LinearLayout ll_payu;

        @BindView(R.id.rl_pay_status)
        RelativeLayout rl_pay_status;

        @BindView(R.id.txt_ps)
        TextView txt_ps;

        @BindView(R.id.tv_ps)
        TextView tv_ps;

        @BindView(R.id.rl_tid)
        RelativeLayout rl_tid;

        @BindView(R.id.txt_tid)
        TextView txt_tid;

        @BindView(R.id.tv_tid)
        TextView tv_tid;

        @BindView(R.id.totalamount)
        TextView totalamount;

        @BindView(R.id.stategstamount)
        TextView stategstamount;

        @BindView(R.id.centralgstamount)
        TextView centralgstamount;

        @BindView(R.id.grandtotalamount)
        TextView grandtotalamount;

        @BindView(R.id.iv_detailarrow)
        ImageView iv_detailarrow;

        @BindView(R.id.rl_nm)
        RelativeLayout rl_nm;

        @BindView(R.id.tv_nm)
        TextView tv_nm;
        // pick up only

        @BindView(R.id.llBusinessInfo)
        LinearLayout llBusinessInfo;

        @BindView(R.id.llDeliveryInfo)
        LinearLayout llDeliveryInfo;

        @BindView(R.id.llConsInfo)
        LinearLayout llConsInfo;

        @BindView(R.id.tvDelContactNo)
        TextView tvDelContactNo;

        @BindView(R.id.txt_add)
        TextView txt_add;

        @BindView(R.id.txt_nm)
        TextView txt_nm;

        @BindView(R.id.tvEmpNm)
        TextView tvEmpNm;

        @BindView(R.id.txt_mob)
        TextView txt_mob;

        @BindView(R.id.tvBusContactNo)
        TextView tvBusContactNo;


        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

            txt_purpose = (TextView) view.findViewById(R.id.name);
            txt_chitamount = (TextView) view.findViewById(R.id.amount);
            mainlayout = (LinearLayout) view.findViewById(R.id.mainlayout);
            txt_paymentmode = (TextView) view.findViewById(R.id.payment);
            txt_status = (TextView) view.findViewById(R.id.status);
            caterername = (TextView) view.findViewById(R.id.caterername);
            tv_mob = (TextView) view.findViewById(R.id.tv_mob);
            txt_orderid = (TextView) view.findViewById(R.id.orderid);
            txt_orderdate = (TextView) view.findViewById(R.id.dateval);
            txt_itemcount = (TextView) view.findViewById(R.id.itemcount);
            viewForeground = (RelativeLayout) view.findViewById(R.id.view_foreground);
            view_background_left = (RelativeLayout) view.findViewById(R.id.view_background_left);
            rl_del_date = (RelativeLayout) view.findViewById(R.id.rl_del_date);
            rl_address = (RelativeLayout) view.findViewById(R.id.rl_address);
            rl_address = (RelativeLayout) view.findViewById(R.id.rl_address);
            iv_status = (ImageView) view.findViewById(R.id.iv_status);
            iv_status_left = (ImageView) view.findViewById(R.id.iv_status_left);
            tv_date = (TextView) view.findViewById(R.id.tv_date);
            tv_address = (TextView) view.findViewById(R.id.tv_address);
            tv_status = (TextView) view.findViewById(R.id.tv_status);
            tv_status_left = (TextView) view.findViewById(R.id.tv_status_left);
            tv_count = (TextView) view.findViewById(R.id.tv_count);
            fl = (FrameLayout) view.findViewById(R.id.fl);
        }
    }

    public MyTaskAdapter() {
    }

    public MyTaskAdapter(List<Chit> moviesList, Context context, OnItemClickListener listener, String tab, boolean isbusiness) {
        this.catererList = moviesList;
        this.mContext = context;
        this.listener = listener;
        this.tab = tab;
        this.isbusiness = isbusiness;

        final SettingDB db = new SettingDB(mContext);
        List<SettingModel> list_setting = db.GetItems();
        sm = list_setting.get(0);

        //currency = SharedPrefUserDetail.getString(context, SharedPrefUserDetail.symbol_native,"") + " ";
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.hiwtory_list_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final Chit chit_obj = catererList.get(position);

        currency = chit_obj.getSymbol_native() + " ";
        //  holder.name.setText(movie.getName());0
        int c_pos = position + 1;
        holder.tv_count.setText("[ " + c_pos + " ]");

        String t_status = chit_obj.getTransaction_status();
        if (tab.equalsIgnoreCase("new")) {

            holder.tv_status.setText("All Task");
            holder.tv_status_left.setText("In Progress");
            if (t_status.equalsIgnoreCase("ACTIVE") || t_status.equalsIgnoreCase("ACCEPTED") || t_status.equalsIgnoreCase("HOLD")) {
                holder.fl.setVisibility(View.VISIBLE);
            } else {

                // holder.fl.setVisibility(View.GONE);
            }

        } else if (tab.equalsIgnoreCase("inprogress")) {
            holder.tv_status.setText("Active");
            holder.tv_status_left.setText("Closing");


            if (t_status.equalsIgnoreCase("INPROGRESS")) {
                holder.fl.setVisibility(View.VISIBLE);
            } else {
                // holder.fl.setVisibility(View.GONE);
            }

        } else if (tab.equalsIgnoreCase("close")) {
            holder.tv_status.setText("In Progress");
            holder.tv_status_left.setText("");


            if (t_status.equalsIgnoreCase("FINISHED")) {
                holder.fl.setVisibility(View.VISIBLE);
            } else {
                //holder.fl.setVisibility(View.GONE);
            }

        } else if (tab.equalsIgnoreCase("new_all")) {
            holder.tv_status.setText("My Task");
            holder.tv_status_left.setText("Closing");


            if (t_status.equalsIgnoreCase("ACTIVE") || t_status.equalsIgnoreCase("ACCEPTED") || t_status.equalsIgnoreCase("HOLD") || t_status.equalsIgnoreCase("INPROGRESS")) {
                holder.fl.setVisibility(View.VISIBLE);
            } else {
                //holder.fl.setVisibility(View.GONE);
            }

        } else if (tab.equalsIgnoreCase("close_all")) {
            holder.tv_status.setText("Active");
            holder.tv_status_left.setText("");


            if (t_status.equalsIgnoreCase("FINISHED")) {
                holder.fl.setVisibility(View.VISIBLE);
            } else {
                //holder.fl.setVisibility(View.GONE);
            }

        } else if (tab.equalsIgnoreCase("search")) {
            holder.tv_status.setText("");
            holder.tv_status_left.setText("Finished");


            if (t_status.equalsIgnoreCase("FINISHED") || t_status.equalsIgnoreCase("CLOSED") || t_status.equalsIgnoreCase("Completed") || t_status.equalsIgnoreCase("Withdrawn")) {
                holder.fl.setVisibility(View.VISIBLE);
            } else {
                //holder.fl.setVisibility(View.GONE);
            }

        } else if (tab.equalsIgnoreCase("closedtask")) // Cancelled Task
        {

            holder.tv_status_left.setText("Finish");

            if (t_status.equalsIgnoreCase("Withdrawn")) {
                holder.fl.setVisibility(View.VISIBLE);
            } else {
                //holder.fl.setVisibility(View.GONE);
            }
        }

        holder.iv_status.setImageResource(R.mipmap.progress);
        holder.iv_status.setVisibility(View.INVISIBLE);
        if (chit_obj.isIslongpress()) {
            holder.mainlayout.setBackgroundColor(Color.parseColor("#FF7777"));
        } else {
            holder.mainlayout.setBackgroundColor(Color.parseColor("#ffffff"));
        }
        if (chit_obj.getSubject().length() > 0) {
            holder.caterername.setText(chit_obj.getSubject());
        } else {
            if (chit_obj.getTo_bridgelist_friendlyname().size() > 0) {
                holder.caterername.setText(chit_obj.getTo_bridgelist_friendlyname().get(0).toString());

            }
        }
        holder.txt_purpose.setText(chit_obj.getPurpose() + " / " + "OTP");
        holder.txt_orderid.setText(chit_obj.getRef_id() + " / " + chit_obj.getCase_id());
        holder.txt_orderid.setTextColor(Color.parseColor("#0000EE"));
        holder.txt_orderid.setPaintFlags(holder.txt_orderid.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);


        holder.txt_orderdate.setText(chit_obj.getCreated_date());

        //api key value should be change
        //   holder.txt_itemcount.setText("# "+chit_obj.getChit_item_count()+"");
        //Double itemprice = Double.parseDouble(chit_obj.getTotal_chit_item_count());
        int itempriceval = chit_obj.getChit_item_count();

        holder.txt_itemcount.setText("# " + itempriceval);
        DecimalFormat twoDForm = new DecimalFormat("#.##");
        Double formatedprice = 0.00;

        try {
            // change api key
            //    Double price = Double.parseDouble(chit_obj.getTotal_chit_item_value()); // Make use of autoboxing.  It's also easier to read.
            Double price = Double.parseDouble(chit_obj.getTotal_chit_item_value());
            formatedprice = Double.valueOf(twoDForm.format(price));

        } catch (NumberFormatException e) {
            // p did not contain a valid double
        }
        //need to add currency symbol
        //holder.txt_chitamount.setText(chit_obj.getSymbol_native()+" "+String.format("%.2f", formatedprice)+"");


        //holder.txt_chitamount.setText(currency + String.format("%.2f", formatedprice) + "");
        holder.txt_chitamount.setText(currency + Inad.getCurrencyDecimal(formatedprice, mContext));
        holder.txt_status.setText(tab);


        holder.txt_status.setText(chit_obj.getTransaction_status());


        holder.tv_mob.setText(chit_obj.getContact_number());

        holder.llBusinessInfo.setVisibility(View.GONE);
        holder.llDeliveryInfo.setVisibility(View.GONE);


        try {

            JSONObject jo_summary = new JSONObject(chit_obj.getFooter_note());

            String fieldjsn = JsonObjParse.getValueEmpty(chit_obj.getFooter_note(), "field_json_data");

            String delContactNo = JsonObjParse.getValueEmpty(fieldjsn, "mobile_delivery");
            if (!delContactNo.isEmpty()) {
                holder.llDeliveryInfo.setVisibility(View.VISIBLE);
                holder.tvDelContactNo.setText(delContactNo);
            }

            String busContactNo = JsonObjParse.getValueEmpty(fieldjsn, "mobile_business");
            if (!busContactNo.isEmpty()) {
                holder.llBusinessInfo.setVisibility(View.VISIBLE);
                holder.tvBusContactNo.setText(busContactNo);
            }

            if (jo_summary.has("PAYMENT_MODE")) {
                holder.txt_paymentmode.setText(jo_summary.getString("PAYMENT_MODE"));
            }

            if (jo_summary.has("DATE")) {
                holder.tv_date.setText(jo_summary.getString("DATE"));
                if (jo_summary.getString("DATE").equals("") || sm.getIsdate().equals("0")) {

                    holder.rl_del_date.setVisibility(View.GONE);
                } else {
                    holder.rl_del_date.setVisibility(View.VISIBLE);
                }
            } else {
                holder.rl_del_date.setVisibility(View.GONE);
            }

            if (jo_summary.has("ADDRESS")) {

                holder.tv_address.setText(jo_summary.getString("ADDRESS"));

                if (jo_summary.getString("ADDRESS").equals("") || sm.getIsaddress().equals("0")) {

                    holder.rl_address.setVisibility(View.GONE);
                } else {
                    holder.rl_address.setVisibility(View.VISIBLE);
                }
            } else {
                holder.rl_address.setVisibility(View.GONE);
            }

            String pick_up_only = "";
            JSONObject jsonObject = new JSONObject(chit_obj.getFooter_note());
            if (jsonObject.has("field_json_data")) {
                String field_json_data = JsonObjParse.getValueEmpty(chit_obj.getFooter_note(), "field_json_data");
                pick_up_only = JsonObjParse.getValueEmpty(field_json_data, "pick_up_only");
            }

            holder.txt_nm.setText("Name : ");
            holder.txt_mob.setText("Mobile No. : ");
            holder.txt_nm.setText("Customer Name : ");

            holder.llConsInfo.setVisibility(View.GONE);

            if (chit_obj.getPurpose().equalsIgnoreCase(mContext.getString(R.string.property))) {
                holder.txt_add.setText("Customer AddAddress : ");
                holder.txt_nm.setText("Customer Name : ");
                holder.txt_mob.setText("Customer Mobile No. : ");
                holder.rl_del_date.setVisibility(View.GONE); // For prop : note required
                holder.llDeliveryInfo.setVisibility(View.GONE);

                holder.llConsInfo.setVisibility(View.VISIBLE);
                String ConsumerInfo = JsonObjParse.getValueEmpty(chit_obj.getFooter_note(), "ConsumerInfo");
                if (!ConsumerInfo.isEmpty()) {
                    JSONArray jaConsumerInfo = new JSONArray(ConsumerInfo);
                    if (jaConsumerInfo.length() > 0) {
                        JSONObject joConsInfo = jaConsumerInfo.getJSONObject(0);
                        holder.tvEmpNm.setText(joConsInfo.getString("customer_nm"));
                        //tvConsNo.setText(joConsInfo.getString("mobile"));
                    }
                }

            } else if (chit_obj.getPurpose().equalsIgnoreCase(mContext.getString(R.string.survey))) {
                holder.llDeliveryInfo.setVisibility(View.GONE);
                holder.rl_del_date.setVisibility(View.GONE);
                holder.amountlbl.setText("");
            } else if (pick_up_only.equalsIgnoreCase("Yes")) {
                holder.txt_add.setText("Delivery AddAddress : Pickup only");
                holder.tv_address.setText("");
                holder.llDeliveryInfo.setVisibility(View.GONE);
            } else {
                String DeliveryInfo = JsonObjParse.getValueEmpty(chit_obj.getFooter_note(), "DeliveryInfo");
                if (!DeliveryInfo.isEmpty()) {
                    JSONArray jaDeliveryInfo = new JSONArray(DeliveryInfo);
                    if (jaDeliveryInfo.length() > 0) {
                        JSONObject joConsInfo = jaDeliveryInfo.getJSONObject(0);
                        pick_up_only = JsonObjParse.getValueEmpty(joConsInfo.toString(), "pick_up_only");
                        if (pick_up_only.equalsIgnoreCase("Yes")) {
                            holder.txt_add.setText("Delivery AddAddress : Self Pickup");
                            holder.tv_address.setText("");
                            holder.llDeliveryInfo.setVisibility(View.GONE);
                        }
                    }
                }
            }

            //if (chit_obj.getPurpose().equalsIgnoreCase(mContext.getString(R.string.property))&&jo_summary.has("Cust_Nm")) {
            if (jo_summary.has("Cust_Nm")) {

                holder.tv_nm.setText(jo_summary.getString("Cust_Nm"));

                if (jo_summary.getString("Cust_Nm").equals("")) {

                    holder.rl_nm.setVisibility(View.GONE);
                } else {
                    holder.rl_nm.setVisibility(View.VISIBLE);
                }

            } else if (jo_summary.has("NAME")) {

                holder.tv_nm.setText(jo_summary.getString("NAME"));

                if (jo_summary.getString("NAME").equals("")) {

                    holder.rl_nm.setVisibility(View.GONE);
                } else {
                    holder.rl_nm.setVisibility(View.VISIBLE);
                }

            } else {
                holder.rl_nm.setVisibility(View.GONE);
            }


        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        holder.mainlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(chit_obj);
            }
        });

        holder.mainlayout.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                listener.onItemLongClick(chit_obj, position);

                return true;
            }
        });


        if (tab.equalsIgnoreCase("search")) {
            fillbottomview(holder, position);
            holder.ll_bottom.setVisibility(View.VISIBLE);
        } else {
            holder.ll_bottom.setVisibility(View.GONE);
        }

        holder.tv_mob.setTextColor(ContextCompat.getColor(mContext, R.color.callclr));
        holder.tvBusContactNo.setTextColor(ContextCompat.getColor(mContext, R.color.callclr));
        holder.tvDelContactNo.setTextColor(ContextCompat.getColor(mContext, R.color.callclr));

        holder.tv_mob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Inad.callIntent(holder.tv_mob, mContext);
            }
        });
        holder.tvBusContactNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Inad.callIntent(holder.tvBusContactNo, mContext);
            }
        });
        holder.tvDelContactNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Inad.callIntent(holder.tvDelContactNo, mContext);
            }
        });

    }

    @Override
    public int getItemCount() {
        return catererList.size();
    }

    public void removeItem(int position) {
        catererList.remove(position);
        // notify the item removed by position
        // to perform recycler view delete animations
        // NOTE: don't call notifyDataSetChanged()
        notifyItemRemoved(position);
    }

    public void restoreItem(Chit item, int position) {
        catererList.add(position, item);
        // notify item added by position
        notifyItemInserted(position);
    }

    public void setFilter(List<Chit> countryModels) {
        catererList = new ArrayList<>();
        catererList.addAll(countryModels);
        notifyDataSetChanged();
    }

    float x1, y1, t1, t2, x2, y2;

    public void fillbottomview(final MyViewHolder holder, final int p) {

        final int pos = p;
        final Chit item = catererList.get(pos);


        // Detail

        if (item.getHeader_note() != null && !item.getHeader_note().equals("")) {
            holder.tv_remark.setText("Note:\n" + item.getHeader_note());
            holder.tv_remark.setVisibility(View.VISIBLE);
        } else {
            holder.tv_remark.setVisibility(View.GONE);
        }

        if (item.isIsdetailshow()) {
            holder.iv_detailarrow.setRotation(180);
            holder.ll_detail_show.setVisibility(View.VISIBLE);
        } else {
            holder.iv_detailarrow.setRotation(0);
            holder.ll_detail_show.setVisibility(View.GONE);
        }

        if (item.isIsstatusshow()) {
            holder.ll_status_show.setVisibility(View.VISIBLE);
        } else {
            holder.ll_status_show.setVisibility(View.GONE);
        }

        if (item.isIssummaryshow()) {
            holder.ll_summary_show.setVisibility(View.VISIBLE);
        } else {
            holder.ll_summary_show.setVisibility(View.GONE);
        }

        String chititemcount = "" + item.getChit_item_count();

        String chititemprice = "" + item.getTotal_chit_item_value();


        Double itemprice = Double.parseDouble(chititemcount);
        int itempriceval = Integer.valueOf(itemprice.intValue());

        holder.txt_qty.setText("# " + itempriceval);

        DecimalFormat twoDForm = new DecimalFormat("#.##");
        Double formatedprice = 0.00;

        try {
            Double price = Double.parseDouble(chititemprice);
            formatedprice = Double.valueOf(twoDForm.format(price));

        } catch (NumberFormatException e) {

        }

        //holder.txt_price.setText(currency + String.format("%.2f", formatedprice) + "");
        holder.txt_price.setText(currency + Inad.getCurrencyDecimal(formatedprice, mContext) + "");

        holder.rl_pb.setVisibility(View.GONE);
        holder.ll_detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (item.isIsdetailshow()) {
                    catererList.get(pos).setIsdetailshow(false);
                } else {
                    catererList.get(pos).setIsdetailshow(true);
                }

                if (catererList.get(pos).getChitDetails() == null) {

                    GetProductDetail getdet = new GetProductDetail(pos, mContext, item.getChit_hash_id(), item.getChit_id() + "", new GetProductDetail.Apicallback() {
                        @Override
                        public void onGetResponse(ChitDetails a, boolean issuccess, int p) {
                            catererList.get(p).setChitDetails(a);
                            notifyDataSetChanged();
                        }

                        @Override
                        public void onUpdate(boolean isupdateforce, String ischeck) {

                        }
                    });
                } else {
                    notifyDataSetChanged();
                }
            }
        });

        holder.ll_status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (item.isIsstatusshow()) {
                    catererList.get(pos).setIsstatusshow(false);
                } else {
                    catererList.get(pos).setIsstatusshow(true);
                }

                if (catererList.get(pos).getChitDetails() == null) {

                    GetProductDetail getdet = new GetProductDetail(pos, mContext, item.getChit_hash_id(), item.getChit_id() + "", new GetProductDetail.Apicallback() {
                        @Override
                        public void onGetResponse(ChitDetails a, boolean issuccess, int p) {
                            catererList.get(p).setChitDetails(a);
                            notifyDataSetChanged();
                        }

                        @Override
                        public void onUpdate(boolean isupdateforce, String ischeck) {

                        }
                    });
                } else {
                    notifyDataSetChanged();
                }
            }
        });

        holder.ll_summary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (item.isIssummaryshow()) {
                    catererList.get(pos).setIssummaryshow(false);
                } else {
                    catererList.get(pos).setIssummaryshow(true);
                }

                if (catererList.get(pos).getChitDetails() == null) {


                    GetProductDetail getdet = new GetProductDetail(pos, mContext, item.getChit_hash_id(), item.getChit_id() + "", new GetProductDetail.Apicallback() {
                        @Override
                        public void onGetResponse(ChitDetails a, boolean issuccess, int p) {
                            catererList.get(p).setChitDetails(a);
                            notifyDataSetChanged();

                        }

                        @Override
                        public void onUpdate(boolean isupdateforce, String ischeck) {

                        }
                    });
                } else {
                    notifyDataSetChanged();
                }
            }
        });

        if (catererList.get(pos).getChitDetails() == null) {

            final int detailpos = pos;

            GetProductDetail getdet = new GetProductDetail(detailpos, mContext, item.getChit_hash_id(), item.getChit_id() + "", new GetProductDetail.Apicallback() {
                @Override
                public void onGetResponse(ChitDetails a, boolean issuccess, int p) {
                    catererList.get(p).setIsdetailshow(true);
                    catererList.get(p).setChitDetails(a);
                    notifyDataSetChanged();
                }

                @Override
                public void onUpdate(boolean isupdateforce, String ischeck) {

                }
            });
        }

        ArrayList<Chitproducts> productlist = new ArrayList<>();
        List<Chitlog> mDataList = new ArrayList<>();

        ChitDetails chitDetails = item.getChitDetails();
        if (chitDetails != null) {
            productlist = chitDetails.getContent();
            mDataList = chitDetails.getHeaderLog();
        }

        if (productlist == null) {
            productlist = new ArrayList<>();
        }

        holder.rv_detail.setHasFixedSize(true);
        holder.rv_detail.setLayoutManager(new LinearLayoutManager(mContext));//Linear Items

        ChititemAdapter adapter = new ChititemAdapter(productlist, mContext, currency);
        holder.rv_detail.setAdapter(adapter);// set adapter on recyclerview
        holder.rv_detail.setNestedScrollingEnabled(false);

        holder.rv_status.setHasFixedSize(true);
        holder.rv_status.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));

        if (mDataList == null) {
            mDataList = new ArrayList<>();
        }

        //TimeLineAdapter mTimeLineAdapter = new TimeLineAdapter(mDataList, Orientation.VERTICAL, false);
        //holder.rv_status.setAdapter(mTimeLineAdapter);

        // Summary

        try {
            JSONObject jo_summary = new JSONObject(item.getFooter_note());

            Double totalcentralgst = Double.valueOf(jo_summary.getString("CGST"));
            //holder.stategstamount.setText(currency+ String.format("%.2f", totalcentralgst));
            holder.stategstamount.setText(currency + Inad.getCurrencyDecimal(totalcentralgst, mContext));

            Double totalstategst = Double.valueOf(jo_summary.getString("SGST"));
            holder.centralgstamount.setText(currency + Inad.getCurrencyDecimal(totalstategst, mContext));

            Double grandtotal = Double.valueOf(jo_summary.getString("GRANDTOTAL"));
            holder.grandtotalamount.setText(currency + Inad.getCurrencyDecimal(grandtotal, mContext));

            Double total = Double.valueOf(jo_summary.getString("TOTALBILL"));
            holder.totalamount.setText(currency + Inad.getCurrencyDecimal(total, mContext));

            if (jo_summary.getString("PAYMENT_MODE").equals("RAZORPAY")) {
                holder.ll_payu.setVisibility(View.VISIBLE);

                String payure = jo_summary.getString("payuResponse");

                JSONObject jo_pu = new JSONObject(payure);

                String result = jo_pu.getString("result");


                JSONObject jo_res = new JSONObject(result);

                holder.tv_tid.setText(jo_res.getString("txnid"));
                holder.tv_ps.setText(jo_res.getString("status"));

            } else {
                holder.ll_payu.setVisibility(View.GONE);
            }
           /* tv_date.setText(jo_summary.getString("DATE"));
            tv_address.setText(jo_summary.getString("ADDRESS"));

            if(getString(R.string.isaddress).equals("1"))
            {

                v.findViewById(R.id.rl_address).setVisibility(View.VISIBLE);
            }
            else {
                v.findViewById(R.id.rl_address).setVisibility(View.GONE);
            }

            if(getString(R.string.isdate).equals("1"))
            {

                v.findViewById(R.id.rl_del_date).setVisibility(View.VISIBLE);
            }
            else {
                v.findViewById(R.id.rl_del_date).setVisibility(View.GONE);
            }
*/

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}