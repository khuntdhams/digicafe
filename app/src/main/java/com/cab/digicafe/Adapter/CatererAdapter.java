package com.cab.digicafe.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cab.digicafe.Activities.ImagePreviewActivity;
import com.cab.digicafe.Activities.MySupplierUnderSubSHopActivity;
import com.cab.digicafe.Helper.Inad;
import com.cab.digicafe.Helper.LoadImg;
import com.cab.digicafe.Model.ModelFile;
import com.cab.digicafe.Model.Supplier;
import com.cab.digicafe.MyCustomClass.JsonObjParse;
import com.cab.digicafe.R;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by CSS on 02-11-2017.
 */

public class CatererAdapter extends RecyclerView.Adapter<CatererAdapter.MyViewHolder> {
    private List<Supplier> catererList;
    private Context mContext;
    boolean isDatabase = false;

    public interface OnItemClickListener {
        void onItemClick(Supplier item, int pos);

        void onItemLongClick(Supplier item, int pos);

        void onDisplyLongDesc(Supplier item);

        void onDisplyInfo(Supplier item);
    }

    private OnItemClickListener listener;
    String baseurl = "";


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name, email, mobile, tv_storestatus, tvAlpha;
        public ImageView caterrerprofile, iv_del, iv_update, ivLoc, ivShopWeb, ivofr, ivCall;
        RelativeLayout rl_lock, rlAlpha;
        LinearLayout llBtn;
        RelativeLayout rvBottomDetail;

        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.name);
            email = (TextView) view.findViewById(R.id.email);
            mobile = (TextView) view.findViewById(R.id.mobile);
            tv_storestatus = (TextView) view.findViewById(R.id.tv_storestatus);

            caterrerprofile = (ImageView) view.findViewById(R.id.catererprofile);
            iv_update = (ImageView) view.findViewById(R.id.iv_update);
            iv_del = (ImageView) view.findViewById(R.id.iv_del);
            rl_lock = (RelativeLayout) view.findViewById(R.id.rl_lock);
            rlAlpha = (RelativeLayout) view.findViewById(R.id.rlAlpha);
            tvAlpha = (TextView) view.findViewById(R.id.tvAlpha);
            ivLoc = (ImageView) view.findViewById(R.id.ivLoc);
            ivShopWeb = (ImageView) view.findViewById(R.id.ivShopWeb);
            llBtn = (LinearLayout) view.findViewById(R.id.llBtn);

            ivofr = (ImageView) view.findViewById(R.id.ivofr);
            ivCall = (ImageView) view.findViewById(R.id.ivCall);

            rvBottomDetail = (RelativeLayout) view.findViewById(R.id.rvBottomDetail);

        }

        public void bind(final Supplier item, final OnItemClickListener listener) {


        }

    }

    public void setFilter(List<Supplier> countryModels) {
        catererList = new ArrayList<>();
        catererList.addAll(countryModels);
        notifyDataSetChanged();
    }


    public CatererAdapter(List<Supplier> moviesList, OnItemClickListener listener, Context context, boolean isDatabase) {
        this.catererList = moviesList;
        this.listener = listener;
        this.mContext = context;
        baseurl = mContext.getString(R.string.BASEURL);
        this.isDatabase = isDatabase;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.caterer_list_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        Supplier movie = catererList.get(position);
        holder.name.setText(movie.getAlias_name());
        holder.email.setText(movie.getEmail_id());
        holder.mobile.setText(movie.getContact_no());

        if (movie.isIslongpress()) {
            holder.iv_del.setVisibility(View.VISIBLE);
        } else {
            holder.iv_del.setVisibility(View.GONE);
        }
        holder.bind(movie, listener);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Log.e("item clieced", "" + item.getAlias_name());
                int pos = position;
                listener.onItemClick(catererList.get(pos), pos);
            }
        });


        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                int pos = position;
                listener.onItemLongClick(catererList.get(pos), pos);
                return true;
            }
        });


        if (movie.getBusiness_status().equalsIgnoreCase("open")) {
            holder.tv_storestatus.setVisibility(View.GONE);
            holder.rl_lock.setVisibility(View.GONE);

        } else if (movie.getBusiness_status().equalsIgnoreCase("closed")) {
            holder.tv_storestatus.setText("Closed");
            holder.tv_storestatus.setVisibility(View.GONE);
            holder.rl_lock.setVisibility(View.VISIBLE);

        }

        if (TextUtils.isEmpty(movie.getCompany_image())) {
            // holder.name.setTextColor(Color.parseColor("#FFFF00"));
            // holder.email.setTextColor(Color.parseColor("#FFFF00"));
            // holder.mobile.setTextColor(Color.parseColor("#FFFF00"));
            // holder.tv_storestatus.setTextColor(Color.parseColor("#FFFF00"));

        } else {
            String endurl = "assets/uploadimages/";

            String url = baseurl + endurl + movie.getCompany_image();

            holder.name.setTextColor(Color.parseColor("#FFFFFF"));
            holder.email.setTextColor(Color.parseColor("#FFFFFF"));
            holder.mobile.setTextColor(Color.parseColor("#FFFFFF"));
            holder.tv_storestatus.setTextColor(Color.parseColor("#FFFFFF"));
           /* Picasso.with(mContext)
                    .load(url)
                    .fit()                        // optional
                    .into(holder.caterrerprofile);*/
        }


        holder.rlAlpha.setVisibility(View.GONE);
        if (movie.getAccountType() != null && movie.getBusiness_type() != null &&
                movie.getAccountType().equalsIgnoreCase("Business") &&
                movie.getBusiness_type().equalsIgnoreCase("Aggregator")) {


            holder.tvAlpha.setText("A");
            holder.rlAlpha.setVisibility(View.VISIBLE);

        } else if (movie.getAccountType() != null && movie.getBusiness_type() != null &&
                movie.getAccountType().equalsIgnoreCase("Business") &&
                movie.getBusiness_type().equalsIgnoreCase("circle")) {


            holder.tvAlpha.setText("C");
            holder.rlAlpha.setVisibility(View.VISIBLE);

        } else if (movie.getAccountType() != null && movie.getBusiness_type() != null &&
                movie.getAccountType().equalsIgnoreCase("Business") &&
                movie.getBusiness_type().equalsIgnoreCase("business")) {


            holder.tvAlpha.setText("B");
            holder.rlAlpha.setVisibility(View.VISIBLE);

        } else {

            holder.tvAlpha.setText("");

        }

        holder.rlAlpha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onDisplyLongDesc(catererList.get(position));
            }
        });

        if (isDatabase) {
            holder.rvBottomDetail.setVisibility(View.GONE);
        } else {
            holder.rvBottomDetail.setVisibility(View.VISIBLE);
        }


        String company_images_field = JsonObjParse.getValueEmpty(movie.getField_json_data(), "company_images_field");
        TypeToken<ArrayList<ModelFile>> token = new TypeToken<ArrayList<ModelFile>>() {
        };
        ArrayList<ModelFile> al_selet = new Gson().fromJson(company_images_field, token.getType());
        if (al_selet == null) al_selet = new ArrayList<>();
        holder.iv_update.setVisibility(View.GONE);
        holder.llBtn.setVisibility(View.GONE);

        //holder.caterrerprofile.setImageDrawable(null);
        loadImg.loadCategoryImg(mContext, "", holder.caterrerprofile);
        if (al_selet.size() > 0) {
            holder.llBtn.setVisibility(View.VISIBLE);
            holder.iv_update.setVisibility(View.VISIBLE);
            loadImg.loadCategoryImg(mContext, al_selet.get(0).getImgpath(), holder.caterrerprofile);
        }


        holder.iv_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //int pos = position;
                //listener.onItemClick(catererList.get(pos),pos);

                String company_images_field = JsonObjParse.getValueEmpty(catererList.get(position).getField_json_data(), "company_images_field");
                TypeToken<ArrayList<ModelFile>> token = new TypeToken<ArrayList<ModelFile>>() {
                };
                ArrayList<ModelFile> al_selet = new Gson().fromJson(company_images_field, token.getType());
                if (al_selet == null) al_selet = new ArrayList<>();

                String imgarray = new Gson().toJson(al_selet);


                Intent intent = new Intent(mContext, ImagePreviewActivity.class);
                intent.putExtra("imgarray", imgarray);
                intent.putExtra("pos", 0);
                intent.putExtra("img_title", catererList.get(position).getFirstname());
                mContext.startActivity(intent);

            }
        });

        holder.ivLoc.setVisibility(View.GONE);
        if (movie.getLatitude() != null && !movie.getLatitude().isEmpty()) {
            holder.llBtn.setVisibility(View.VISIBLE);
            holder.ivLoc.setVisibility(View.VISIBLE);
        }

        holder.ivLoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //https://developers.google.com/maps/documentation/urls/android-intents
                try {
                    double lati = Double.parseDouble(catererList.get(position).getLatitude());
                    double longi = Double.parseDouble(catererList.get(position).getLongitude());
                    String q = catererList.get(position).getLocation();
                    String uri = String.format(Locale.ENGLISH, "geo:%f,%f", lati, longi);
                    uri = uri + "?q=" + q;
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                    intent.setPackage("com.google.android.apps.maps");
                    mContext.startActivity(intent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        holder.ivShopWeb.setVisibility(View.GONE);
        String web_link_of_shop = JsonObjParse.getValueEmpty(movie.getField_json_data(), "web_link_of_shop");
        if (web_link_of_shop.length() > 0) holder.ivShopWeb.setVisibility(View.VISIBLE);

        holder.ivShopWeb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //https://developers.google.com/maps/documentation/urls/android-intents
                String web_link_of_shop = JsonObjParse.getValueEmpty(catererList.get(position).getField_json_data(), "web_link_of_shop");

                try {

                    Intent i = new Intent(Intent.ACTION_VIEW);

                    if (!web_link_of_shop.startsWith("http://") && !web_link_of_shop.startsWith("https://")) {
                        web_link_of_shop = "http://" + web_link_of_shop;
                    }

                    i.setData(Uri.parse(web_link_of_shop));


                    mContext.startActivity(i);

                } catch (Exception e) {
                    e.printStackTrace();
                    Inad.alerterInfo("Message", "Can not load " + web_link_of_shop, (Activity) mContext);
                }
            }
        });


        holder.ivofr.setVisibility(View.GONE);
        String disc_info = JsonObjParse.getValueEmpty(movie.getField_json_data(), "disc_info");
        if (disc_info.length() > 2 && mContext instanceof MySupplierUnderSubSHopActivity)
            holder.ivofr.setVisibility(View.VISIBLE);
        holder.ivofr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //https://developers.google.com/maps/documentation/urls/android-intents
                try {
                    listener.onDisplyInfo(catererList.get(position));

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        holder.mobile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Inad.callIntent(holder.mobile, mContext);
            }
        });

        holder.ivCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Inad.callIntent(holder.mobile, mContext);
            }
        });


        if (mContext.getString(R.string.app_name_condition).equalsIgnoreCase(mContext.getString(R.string.c1_omantex))) {
            holder.name.setVisibility(View.INVISIBLE);
            holder.mobile.setVisibility(View.INVISIBLE);
            holder.ivCall.setVisibility(View.INVISIBLE);
        }

    }

    LoadImg loadImg = new LoadImg();


    @Override
    public int getItemCount() {
        return catererList.size();
    }
}
