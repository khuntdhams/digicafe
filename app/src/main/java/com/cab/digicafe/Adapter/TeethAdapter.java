package com.cab.digicafe.Adapter;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cab.digicafe.Model.Teeth;
import com.cab.digicafe.R;

import java.util.ArrayList;

public class TeethAdapter extends RecyclerView.Adapter<TeethAdapter.MyViewHolder> {

    private ArrayList<Teeth> teethList;
    private OnItemClickListener listener;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView teethTop, teethBottom;

        public MyViewHolder(View view) {
            super(view);
            teethTop = (TextView) view.findViewById(R.id.teethTop);
            teethBottom = (TextView) view.findViewById(R.id.teethBottom);
        }
    }

    public TeethAdapter(ArrayList<Teeth> teethList, OnItemClickListener listener) {
        this.teethList = teethList;
        this.listener = listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.teeth_list_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        Teeth movie = teethList.get(position);
        holder.teethTop.setText(movie.getNumberTop());
        holder.teethBottom.setText(movie.getNumberBottom());

        if (teethList.get(position).isCheckTop() && teethList.get(position).isCheckBottom()) {
            holder.teethTop.setBackgroundResource(R.drawable.teeth_back2);
            holder.teethTop.setTextColor(Color.parseColor("#FFFFFF"));
            holder.teethBottom.setBackgroundResource(R.drawable.teeth_back2);
            holder.teethBottom.setTextColor(Color.parseColor("#FFFFFF"));
        } else if (teethList.get(position).isCheckTop() && !teethList.get(position).isCheckBottom()) {
            holder.teethTop.setBackgroundResource(R.drawable.teeth_back2);
            holder.teethTop.setTextColor(Color.parseColor("#FFFFFF"));
            holder.teethBottom.setBackgroundResource(R.drawable.teeth_back);
            holder.teethBottom.setTextColor(Color.parseColor("#000000"));
        } else if (!teethList.get(position).isCheckTop() && teethList.get(position).isCheckBottom()) {
            holder.teethBottom.setBackgroundResource(R.drawable.teeth_back2);
            holder.teethBottom.setTextColor(Color.parseColor("#FFFFFF"));
            holder.teethTop.setBackgroundResource(R.drawable.teeth_back);
            holder.teethTop.setTextColor(Color.parseColor("#000000"));
        } else {
            holder.teethTop.setBackgroundResource(R.drawable.teeth_back);
            holder.teethTop.setTextColor(Color.parseColor("#000000"));
            holder.teethBottom.setBackgroundResource(R.drawable.teeth_back);
            holder.teethBottom.setTextColor(Color.parseColor("#000000"));
        }

        holder.teethTop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (teethList.get(position).isCheckTop()) {
                    teethList.get(position).setCheckTop(false);
                } else {
                    teethList.get(position).setCheckTop(true);
                }
                listener.onItemClick();
                notifyDataSetChanged();
            }
        });
        holder.teethBottom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (teethList.get(position).isCheckBottom()) {
                    teethList.get(position).setCheckBottom(false);
                } else {
                    teethList.get(position).setCheckBottom(true);
                }
                listener.onItemClick();
                notifyDataSetChanged();
            }
        });
    }

    public interface OnItemClickListener {
        void onItemClick();

    }


    @Override
    public int getItemCount() {
        return teethList.size();
    }
}