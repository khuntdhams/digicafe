package com.cab.digicafe.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Paint;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.cab.digicafe.Activities.ProductDetiailsDialogueTab;
import com.cab.digicafe.Activities.master.EnquiryOfPropertyActivity;
import com.cab.digicafe.Adapter.ctlg.SurveyOptionsListAdapter;
import com.cab.digicafe.Adapter.master.ImagePagerAdapter;
import com.cab.digicafe.BaseActivity;
import com.cab.digicafe.Callback.OnLoadMoreListener;
import com.cab.digicafe.Database.SqlLiteDbHelper;
import com.cab.digicafe.Fragments.CatalougeContent;
import com.cab.digicafe.Helper.Inad;
import com.cab.digicafe.Helper.LoadImg;
import com.cab.digicafe.Helper.SessionManager;
import com.cab.digicafe.Helper.SharedPrefUserDetail;
import com.cab.digicafe.Model.Metal;
import com.cab.digicafe.Model.ShareModel;
import com.cab.digicafe.Model.SurveyOptions;
import com.cab.digicafe.MyCustomClass.JsonObjParse;
import com.cab.digicafe.R;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import me.relex.circleindicator.CircleIndicator;


/**
 * Created by CSS on 02-11-2017.
 */

public class CatalogTabAdapter extends RecyclerView.Adapter<CatalogTabAdapter.MyViewHolder> {
    private List<CatalougeContent> catererList;

    private Context mContext;
    private SqlLiteDbHelper dbHelper;
    SessionManager sessionManager;
    private OnLoadMoreListener mOnLoadMoreListener;
    String currency = "";

    public interface OnItemClickListener {
        void onItemClick(CatalougeContent item);

        void onImgClick(CatalougeContent item);

        void onItemLongClick(CatalougeContent item);

        void onadditem(CatalougeContent qty);

        void ondecreasecount(CatalougeContent qty);
    }

    private OnItemClickListener listener;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name, offer, email, mobile, qty, discountprice, tv_status, tv_totaltax, tv_count, tvStock, tvPriceRangeWholesale, tvQtyWholesale, tvDaysWholesale, tvEnq,
                tvSqFt, tvUds, tvCarpetArea, tvBedRoom, tvBathRoom, tvKitchen, tvfacing, tvPropPrice,
                tvPropertyStatus, tvTmp, discountPer;
        public ImageView addbtn, removebtn, ivImgTemplate, ivVdoIndicator;
        ImageView ivKitchen, ivBath, ivBed;
        private ImageView imageView, ivWholeSale;
        public RelativeLayout rl_bottom, viewForeground, view_background_right, rlMediaView;
        public LinearLayout rl_base_cat;
        LinearLayout llStock, llWholeSale, llPTax, llPropertyInfo, llSurveyInfo, llEmail;
        ViewPager pager;
        CircleIndicator indicator;
        RecyclerView rvSurveyOptions;
        ImageView iv_update, ivShare;
        LinearLayout llBtn, llMain;
        TextView tvTemplateType;


        public MyViewHolder(View view) {
            super(view);
            addbtn = (ImageView) view.findViewById(R.id.addbtn);
            ivKitchen = (ImageView) view.findViewById(R.id.ivKitchen);
            ivBath = (ImageView) view.findViewById(R.id.ivBath);
            ivBed = (ImageView) view.findViewById(R.id.ivBed);

            removebtn = (ImageView) view.findViewById(R.id.removebtn);
            ivImgTemplate = (ImageView) view.findViewById(R.id.ivImgTemplate);
            ivVdoIndicator = (ImageView) view.findViewById(R.id.ivVdoIndicator);
            qty = (TextView) view.findViewById(R.id.qty);
            tvTmp = (TextView) view.findViewById(R.id.tvTmp);
            name = (TextView) view.findViewById(R.id.name);
            offer = (TextView) view.findViewById(R.id.offer);
            rl_bottom = (RelativeLayout) view.findViewById(R.id.rl_bottom);
            rlMediaView = (RelativeLayout) view.findViewById(R.id.rlMediaView);
            viewForeground = (RelativeLayout) view.findViewById(R.id.viewForeground);
            view_background_right = (RelativeLayout) view.findViewById(R.id.view_background_right);
            rl_base_cat = (LinearLayout) view.findViewById(R.id.rl_base_cat);
            llStock = (LinearLayout) view.findViewById(R.id.llStock);
            llWholeSale = (LinearLayout) view.findViewById(R.id.llWholeSale);
            llPTax = (LinearLayout) view.findViewById(R.id.llPTax);
            llPropertyInfo = (LinearLayout) view.findViewById(R.id.llPropertyInfo);
            llSurveyInfo = (LinearLayout) view.findViewById(R.id.llSurveyInfo);

            llEmail = (LinearLayout) view.findViewById(R.id.llEmail);

            email = (TextView) view.findViewById(R.id.email);
            discountPer = (TextView) view.findViewById(R.id.discountPer);
            tvPriceRangeWholesale = (TextView) view.findViewById(R.id.tvPriceRangeWholesale);
            tvQtyWholesale = (TextView) view.findViewById(R.id.tvQtyWholesale);
            tvDaysWholesale = (TextView) view.findViewById(R.id.tvDaysWholesale);
            tv_totaltax = (TextView) view.findViewById(R.id.tv_totaltax);
            discountprice = (TextView) view.findViewById(R.id.discountprice);
            mobile = (TextView) view.findViewById(R.id.mobile);
            tv_status = (TextView) view.findViewById(R.id.tv_status);
            imageView = (ImageView) view.findViewById(R.id.profile_image);
            ivWholeSale = (ImageView) view.findViewById(R.id.ivWholeSale);
            tv_count = (TextView) view.findViewById(R.id.tv_count);
            tvStock = (TextView) view.findViewById(R.id.tvStock);


            tvSqFt = (TextView) view.findViewById(R.id.tvSqFt);
            tvUds = (TextView) view.findViewById(R.id.tvUds);
            tvCarpetArea = (TextView) view.findViewById(R.id.tvCarpetArea);
            tvBedRoom = (TextView) view.findViewById(R.id.tvBedRoom);
            tvBathRoom = (TextView) view.findViewById(R.id.tvBathRoom);
            tvKitchen = (TextView) view.findViewById(R.id.tvKitchen);
            tvfacing = (TextView) view.findViewById(R.id.tvfacing);
            tvPropPrice = (TextView) view.findViewById(R.id.tvPropPrice);
            tvPropertyStatus = (TextView) view.findViewById(R.id.tvPropertyStatus);
            tvEnq = (TextView) view.findViewById(R.id.tvEnq);

            pager = (ViewPager) view.findViewById(R.id.pager);
            indicator = (CircleIndicator) view.findViewById(R.id.indicator);
            rvSurveyOptions = (RecyclerView) view.findViewById(R.id.rvSurveyOptions);

            iv_update = (ImageView) view.findViewById(R.id.iv_update);
            ivShare = (ImageView) view.findViewById(R.id.ivShare);
            llBtn = (LinearLayout) view.findViewById(R.id.llBtn);
            llMain = (LinearLayout) view.findViewById(R.id.llMain);

            tvTemplateType = (TextView) view.findViewById(R.id.tvTemplateType);

        }
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }

    public CatalogTabAdapter(List<CatalougeContent> moviesList, Context context, OnItemClickListener listener) {
        this.catererList = moviesList;
        this.mContext = context;
        dbHelper = new SqlLiteDbHelper(context);
        sessionManager = new SessionManager(context);
        this.listener = listener;

        currency = SharedPrefUserDetail.getString(context, SharedPrefUserDetail.symbol_native, "") + " ";

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.catalog_drawer, parent, false);
        return new MyViewHolder(itemView);
    }

    Double sgst = 0.0;
    Double cgst = 0.0;

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final CatalougeContent movie = catererList.get(position);

        currency = movie.getSymbol_native() + " ";

        int c_pos = position + 1;
        holder.tv_count.setText("[ " + c_pos + " ]");
        holder.ivWholeSale.setVisibility(View.GONE);
        holder.llWholeSale.setVisibility(View.GONE);
        holder.llPTax.setVisibility(View.VISIBLE);

        holder.tvDaysWholesale.setText("");
        holder.tvQtyWholesale.setText("");
        holder.tvPriceRangeWholesale.setText("");

        holder.llPropertyInfo.setVisibility(View.GONE);
        holder.tvPropertyStatus.setVisibility(View.GONE);

        holder.tvEnq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ShareModel.getI().purpose = mContext.getString(R.string.property);
                //ShareModel.getI().chit_name = movie.getName();
                ShareModel.getI().chit_name = movie.getCrossReference();

                Intent i = new Intent(mContext, EnquiryOfPropertyActivity.class);
                mContext.startActivity(i);
            }
        });

        String wholesale = JsonObjParse.getValueEmpty(movie.getInternalJsonData(), "wholesale");
        String property = JsonObjParse.getValueEmpty(movie.getInternalJsonData(), "property");
        String survey = JsonObjParse.getValueEmpty(movie.getInternalJsonData(), "survey");
        String jewel = JsonObjParse.getValueEmpty(movie.getInternalJsonData(), "jewel");
        String doctor_template_type = JsonObjParse.getValueEmpty(movie.getInternalJsonData(), "doctor_template_type");

        if (!doctor_template_type.isEmpty()) {
            holder.tvTemplateType.setVisibility(View.VISIBLE);
            holder.tvTemplateType.setText(doctor_template_type);
        }


        holder.discountprice.setVisibility(View.VISIBLE);
        holder.tv_totaltax.setVisibility(View.VISIBLE);
        holder.llSurveyInfo.setVisibility(View.GONE);

        if (!wholesale.isEmpty()) {

            holder.llPTax.setVisibility(View.GONE);


            String minQ = JsonObjParse.getValueEmpty(wholesale, "minQ");
            holder.tvQtyWholesale.setText("Min. quantity : #" + minQ);

            String minPrice = JsonObjParse.getValueEmpty(wholesale, "minPrice");
            String maxPrice = JsonObjParse.getValueEmpty(wholesale, "maxPrice");

            if (!minPrice.isEmpty() && !maxPrice.isEmpty()) {
                Double minP = Double.valueOf(minPrice);
                Double maxP = Double.valueOf(maxPrice);
                holder.tvPriceRangeWholesale.setText("Price range : " + currency + Inad.getCurrencyDecimal(minP, mContext) + "" + " - " + currency + Inad.getCurrencyDecimal(maxP, mContext));
            } else if (!minPrice.isEmpty()) {
                Double minP = Double.valueOf(minPrice);
                holder.tvPriceRangeWholesale.setText("Minimum price : " + currency + Inad.getCurrencyDecimal(minP, mContext) + "");

            }

            String minDay = JsonObjParse.getValueEmpty(wholesale, "minDay");
            String maxDay = JsonObjParse.getValueEmpty(wholesale, "maxDay");

            if (minDay.isEmpty()) {
                holder.tvDaysWholesale.setText("Deliverd within " + maxDay + " day");
            } else if (maxDay.isEmpty()) {
                holder.tvDaysWholesale.setText("Deliverd within " + minDay + " day");
            } else {
                holder.tvDaysWholesale.setText("Deliverd within " + minDay + " - " + maxDay + " days");
            }

            holder.ivWholeSale.setImageResource(R.drawable.ic_wholesale);
            holder.ivWholeSale.setVisibility(View.VISIBLE);
            holder.llWholeSale.setVisibility(View.VISIBLE);
        } else if (!property.isEmpty()) {
            holder.ivWholeSale.setVisibility(View.VISIBLE);
            holder.llPropertyInfo.setVisibility(View.VISIBLE);
            holder.tvPropertyStatus.setVisibility(View.VISIBLE);

            holder.ivWholeSale.setImageResource(R.drawable.ic_house);

            String proprty_sqft = JsonObjParse.getValueEmpty(property, "proprty_sqft");
            holder.tvSqFt.setText("Sq.ft : " + proprty_sqft);

            String uds_sqft = JsonObjParse.getValueEmpty(property, "uds_sqft");
            holder.tvUds.setText("UDS(sq.ft) : " + uds_sqft);

            holder.tvUds.setVisibility(View.VISIBLE);
            if (uds_sqft.isEmpty()) holder.tvUds.setVisibility(View.GONE);

            String carpet_arae_sqft = JsonObjParse.getValueEmpty(property, "carpet_arae_sqft");
            holder.tvCarpetArea.setText("Carpet area(sq.ft) : " + carpet_arae_sqft);

            holder.tvCarpetArea.setVisibility(View.VISIBLE);
            if (carpet_arae_sqft.isEmpty()) holder.tvCarpetArea.setVisibility(View.GONE);


            String bed = JsonObjParse.getValueEmpty(property, "bed");
            holder.tvBedRoom.setText("BedRoom : " + bed);

            holder.tvBedRoom.setVisibility(View.VISIBLE);
            holder.ivBed.setVisibility(View.VISIBLE);
            if (bed.isEmpty()) {
                holder.tvBedRoom.setVisibility(View.GONE);
                holder.ivBed.setVisibility(View.GONE);

            }


            String bath = JsonObjParse.getValueEmpty(property, "bath");
            holder.tvBathRoom.setText("Bathroom : " + bath);

            holder.tvBathRoom.setVisibility(View.VISIBLE);
            holder.ivBath.setVisibility(View.VISIBLE);
            if (bath.isEmpty()) {
                holder.tvBathRoom.setVisibility(View.GONE);
                holder.ivBath.setVisibility(View.GONE);
            }

            String kitchen = JsonObjParse.getValueEmpty(property, "kitchen");
            holder.tvKitchen.setText("Kitchen : " + kitchen);

            holder.tvKitchen.setVisibility(View.VISIBLE);
            holder.ivKitchen.setVisibility(View.VISIBLE);
            if (kitchen.isEmpty()) {
                holder.tvKitchen.setVisibility(View.GONE);
                holder.ivKitchen.setVisibility(View.GONE);
            }


            String facing = JsonObjParse.getValueEmpty(property, "facing");
            holder.tvfacing.setText("Facing : " + facing);

            holder.tvPropPrice.setVisibility(View.GONE);
            Double mrpProp = 0.0;
            if (!movie.getPrice().isEmpty()) {
                mrpProp = Double.valueOf(movie.getPrice());
                holder.tvPropPrice.setVisibility(View.VISIBLE);
            }
            holder.tvPropPrice.setText("Price : " + currency + Inad.getCurrencyDecimal(mrpProp, mContext) + "");

            String status_property = JsonObjParse.getValueEmpty(property, "status_property");
            switch (status_property) {
                case ("red"):
                    holder.tvPropertyStatus.setTextColor(ContextCompat.getColor(mContext, R.color.rd));
                    break;
                case ("green"):
                    holder.tvPropertyStatus.setTextColor(ContextCompat.getColor(mContext, R.color.grn));
                    break;
                case ("amber"):
                    holder.tvPropertyStatus.setTextColor(ContextCompat.getColor(mContext, R.color.amber));
                    break;
                default:
                    holder.tvPropertyStatus.setTextColor(ContextCompat.getColor(mContext, R.color.rd));
                    break;
            }

        } else if (!survey.isEmpty()) {
            holder.ivWholeSale.setImageResource(R.drawable.ic_survey);
            holder.ivWholeSale.setVisibility(View.VISIBLE);
            holder.discountprice.setVisibility(View.INVISIBLE);
            holder.tv_totaltax.setVisibility(View.INVISIBLE);
            holder.llSurveyInfo.setVisibility(View.VISIBLE);
            holder.ivWholeSale.setImageResource(R.drawable.ic_survey);

            setSurveyInfo(holder, position, movie, survey);
        }


        if (position == catererList.size() - 1) {
            holder.rl_bottom.setVisibility(View.VISIBLE);
        } else {
            holder.rl_bottom.setVisibility(View.GONE);
        }

        holder.name.setText(movie.getName());
       /* if(!survey.isEmpty())
        {
            String surveyQPos = JsonObjParse.getValueEmpty(survey, "SurveyQPos");
            holder.name.setText(movie.getName()+ " - " + surveyQPos);
        }*/

        if (movie.getOutOfStock().equalsIgnoreCase("Yes")) {
            holder.viewForeground.setBackgroundColor(Color.parseColor("#BCC0C5"));
            holder.tv_status.setText("Stock NOT available");
        } else {
            holder.tv_status.setText("Stock available");
            holder.viewForeground.setBackgroundColor(Color.parseColor("#ffffff"));
        }

        boolean isanyjsondata = false;

        try {
            Object json = new JSONTokener(movie.getAdditionalJsonData()).nextValue();
            if (json instanceof JSONObject) {
                JSONObject jo_add = new JSONObject(movie.getAdditionalJsonData());
                if (movie.getAdditionalJsonData() != null && jo_add.length() > 0) {
                    isanyjsondata = true;
                }
            }

            json = new JSONTokener(movie.getTaxJsonData()).nextValue();
            if (json instanceof JSONObject) {
                JSONObject jo_tax = new JSONObject(movie.getTaxJsonData());
                if (movie.getTaxJsonData() != null && jo_tax.length() > 0) {
                    isanyjsondata = true;
                }
            }

            json = new JSONTokener(movie.getFieldJsonData()).nextValue();
            if (json instanceof JSONObject) {
                JSONObject jo_field = new JSONObject(movie.getFieldJsonData());

                if (movie.getFieldJsonData() != null && jo_field.length() > 0) {
                    isanyjsondata = true;
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        /*holder.name.setPaintFlags(holder.name.getPaintFlags() & (~ Paint.UNDERLINE_TEXT_FLAG));
        if (isanyjsondata) {
            holder.name.setTextColor(Color.parseColor("#0000EE"));
            holder.name.setPaintFlags(holder.name.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            holder.name.setEnabled(true);

        } else {
            holder.name.setPaintFlags(holder.name.getPaintFlags() & (~ Paint.UNDERLINE_TEXT_FLAG));
            holder.name.setTextColor(Color.parseColor("#000000"));
            holder.name.setEnabled(false);

        }*/

        String caterorstatus = sessionManager.getcurrentcatererstatus();
        /*if (caterorstatus.equalsIgnoreCase("open"))
        {
            holder.qty.setVisibility(View.VISIBLE);
            holder.removebtn.setVisibility(View.VISIBLE);
            holder.addbtn.setVisibility(View.VISIBLE);
        }else
        {
            holder.qty.setVisibility(View.GONE);
            holder.removebtn.setVisibility(View.GONE);
            holder.addbtn.setVisibility(View.GONE);
        }*/


        if (TextUtils.isEmpty(movie.getOffer())) {
            holder.offer.setVisibility(View.GONE);
        } else {
            holder.offer.setVisibility(View.VISIBLE);
            holder.offer.setText(movie.getOffer());
        }

        //final NumberFormat format = NumberFormat.getCurrencyInstance();
        Double mrp = Double.valueOf(movie.getPrice().toString());


        if (!property.isEmpty()) {

            String sqFt = JsonObjParse.getValueEmpty(property, "proprty_sqft");
            if (!sqFt.isEmpty()) {
                int sqFtVal = Integer.parseInt(sqFt);
                mrp = mrp * sqFtVal;
            }

            try {
                String ExtraInfoPrice = JsonObjParse.getValueEmpty(property, "ExtraInfoPrice");
                mrp = mrp + getExtraPropertyInfo(ExtraInfoPrice);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } else if (!jewel.isEmpty()) {

            //String metal = JsonObjParse.getValueEmpty(jewel, "metal");
            String metalPosId = JsonObjParse.getValueEmpty(jewel, "metalPosId");
            int posId = 0;
            if (!metalPosId.isEmpty()) posId = Integer.parseInt(metalPosId);

            try {
                if (ShareModel.getI().metal.alMetal.size() > posId) {
                    Metal metal = ShareModel.getI().metal.alMetal.get(posId);
                    if (!metal.metalPrice.isEmpty()) {
                        mrp = Double.valueOf(metal.metalPrice);
                    }
                } else {
                    Log.d("okhttp jewel", movie.getName() + "");
                }

            } catch (NumberFormatException e) {
                e.printStackTrace();
            }


            String gold_today_price_catalogue = SharedPrefUserDetail.getString(mContext, SharedPrefUserDetail.gold_today_price_catalogue, "");
            String silver_today_price_catalogue = SharedPrefUserDetail.getString(mContext, SharedPrefUserDetail.silver_today_price_catalogue, "");
           /*
            if(metal.isEmpty())
            {

            }
            else if(!gold_today_price_catalogue.isEmpty()&&metal.equalsIgnoreCase("gold")){
                mrp = Double.valueOf(gold_today_price_catalogue);
            }
            else if(!silver_today_price_catalogue.isEmpty()&&metal.equalsIgnoreCase("silver")){
                mrp = Double.valueOf(silver_today_price_catalogue);
            }*/


            String weight = JsonObjParse.getValueEmpty(jewel, "weight");


            if (!weight.isEmpty()) {
                float wt = Float.parseFloat(weight);
                //mrp = (mrp * wt)/10; // 10 gram
                mrp = (mrp * wt) / 1; // 10 gram
            }


        }


        Double dis_price = 0.0;
        Double dis_perc = 0.0;

        if (!movie.getDiscountedPrice().equals(""))
            dis_price = Double.valueOf(movie.getDiscountedPrice());
        if (!movie.getDiscountPercentage().equals(""))
            dis_perc = Double.valueOf(movie.getDiscountPercentage());

        Double dis_mrp = Double.valueOf(getdiscountmrp(mrp, dis_perc, dis_price).toString());
       /* if (doctor_template_type.equalsIgnoreCase("RPD")) {
            Double Amrp = 0.0;
            Log.d("additional", JsonObjParse.getValueEmpty(catererList.get(position).getInternalJsonData(), "additionalPrice_rpd") + "");
            if (!JsonObjParse.getValueEmpty(catererList.get(position).getInternalJsonData(), "additionalPrice_rpd").isEmpty()) {
                Amrp = Double.valueOf(JsonObjParse.getValueEmpty(catererList.get(position).getInternalJsonData(), "additionalPrice_rpd"));

                dis_mrp = dis_mrp  + Amrp;
            }
        }
*/


      /*  if (dis_mrp == mrp)holder.discountprice.setVisibility(View.GONE);
        else holder.discountprice.setText(mContext.getString(R.string.currency) + String.format("%.2f", dis_mrp));


        holder.discountprice.setText(mContext.getString(R.string.currency) + String.format("%.2f", dis_mrp));
*/


        // holder.discountprice.setVisibility(View.GONE);


        // holder.imageView.setVisibility(View.GONE);

        holder.addbtn.setVisibility(View.GONE);
        holder.removebtn.setVisibility(View.GONE);
        holder.qty.setVisibility(View.GONE);


        holder.name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String survey = JsonObjParse.getValueEmpty(movie.getInternalJsonData(), "survey");

                if (!survey.isEmpty()) {
                    holder.rl_base_cat.performClick();
                } else {
                    int ii = position;

                    ProductDetiailsDialogueTab productDetiailsDialogue = new ProductDetiailsDialogueTab((Activity) mContext, catererList.get(ii));
                    productDetiailsDialogue.show();
                }

               /* new CatelogDetailDialogTab((Activity) mContext, new CatelogDetailDialogTab.OnDialogClickListener() {
                    @Override
                    public void onDialogImageRunClick(int positon) {
                        if (positon == 0) {

                        }
                    }
                }, catererList.get(ii)).show();*/
            }
        });

        switch (filter) {
            case "all":
                holder.rl_base_cat.setVisibility(View.VISIBLE);
                break;
            case "avail":
                if (movie.getOutOfStock().equals("No")) {
                    holder.rl_base_cat.setVisibility(View.VISIBLE);
                } else {
                    //holder.rl_base_cat.setVisibility(View.GONE);
                }
                break;
            case "notavail":
                if (movie.getOutOfStock().equals("Yes")) {
                    holder.rl_base_cat.setVisibility(View.VISIBLE);
                } else {
                    //holder.rl_base_cat.setVisibility(View.GONE);
                }
                break;
            default:
                break;
        }


        holder.rl_base_cat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(catererList.get(position));
            }
        });

        holder.rl_base_cat.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                listener.onItemLongClick(catererList.get(position));

                return true;
            }
        });

        if (mrp.doubleValue() == dis_mrp.doubleValue()) {
            holder.email.setVisibility(View.GONE);
            holder.discountPer.setVisibility(View.GONE);
            holder.llEmail.setVisibility(View.GONE);
            holder.email.setText("");
            holder.discountPer.setText("");

        } else {
            holder.email.setVisibility(View.VISIBLE);
            holder.discountPer.setVisibility(View.VISIBLE);
            holder.llEmail.setVisibility(View.VISIBLE);
        }

        holder.email.setPaintFlags(holder.email.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        //holder.discountprice.setText(currency + String.format("%.2f", dis_mrp));

        Double ded_price_val = 0.0, addi_price_val = 0.0;
        Double ded_perc_val = 0.0, addi_perc_val = 0.0;
        Double ded_mrp = 0.0, addi_mrp = 0.0;

        if (!jewel.isEmpty()) {

           /* String ded_val = JsonObjParse.getValueEmpty(jewel, "ded_val");
            if (!ded_val.isEmpty()) ded_price_val = Double.parseDouble(ded_val);
            String ded_perc = JsonObjParse.getValueEmpty(jewel, "ded_perc");
            if (!ded_perc.isEmpty()) ded_perc_val = Double.parseDouble(ded_perc);

            ded_mrp = Double.valueOf(getValues(mrp, ded_perc_val, ded_price_val));*/

            try {
                String DeductionJewelPrice = JsonObjParse.getValueEmpty(jewel, "DeductionJewelPrice");
                ded_price_val = getAdditionalDeductinalInfo(DeductionJewelPrice, mrp);
                ded_mrp = Double.valueOf(getValues(mrp, ded_perc_val, ded_price_val));
            } catch (JSONException e) {
                e.printStackTrace();
            }


           /* String additional_val = JsonObjParse.getValueEmpty(jewel, "additional_val");
            if (!additional_val.isEmpty()) addi_price_val = Double.parseDouble(additional_val);
            String additional_perc = JsonObjParse.getValueEmpty(jewel, "additional_perc");
            if (!additional_perc.isEmpty()) addi_perc_val = Double.parseDouble(additional_perc);
            addi_mrp = Double.valueOf(getValues(mrp, addi_perc_val, addi_price_val));*/

            try {
                String AdditionalJewelPrice = JsonObjParse.getValueEmpty(jewel, "AdditionalJewelPrice");
                addi_price_val = getAdditionalDeductinalInfo(AdditionalJewelPrice, mrp);
                addi_mrp = Double.valueOf(getValues(mrp, addi_perc_val, addi_price_val));
            } catch (JSONException e) {
                e.printStackTrace();
            }

            dis_mrp = dis_mrp - ded_mrp + addi_mrp;

        }
        holder.discountprice.setText(currency + Inad.getCurrencyDecimal(dis_mrp, mContext));

        sgst = 0.0;
        cgst = 0.0;
        try {
            String tax = movie.getTaxJsonData();
            if (tax != null) {
                setTax(tax);
                //JSONObject jo_tax = new JSONObject(tax);
                //sgst = Double.valueOf(jo_tax.getString("SGST"));
                //cgst = Double.valueOf(jo_tax.getString("CGST"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Double stategst = (dis_mrp / 100.0f) * sgst;
        Double centralgst = (dis_mrp / 100.0f) * cgst;

        //stategst = Math.round(stategst * 100.0) / 100.0; // 0.224 - > 0.22
        //centralgst = Math.round(centralgst * 100.0) / 100.0; // 0.224 - > 0.22


        if (dis_mrp > 0) {
            Double total = dis_mrp + stategst + centralgst;
            //holder.tv_totaltax.setText(currency+String.format("%.2f", total) + ""+"");
            holder.tv_totaltax.setText(currency + Inad.getCurrencyDecimal(total, mContext) + "" + "");
        } else {
            Double total = mrp + stategst + centralgst;
            //holder.tv_totaltax.setText(currency+String.format("%.2f", total) + ""+"");
            holder.tv_totaltax.setText(currency + Inad.getCurrencyDecimal(total, mContext) + "" + "");
        }



        if (!jewel.isEmpty()) {
            mrp = mrp + stategst + centralgst - ded_mrp + addi_mrp;

        } else {
            mrp = mrp + stategst + centralgst;

        }

        holder.email.setText(currency + Inad.getCurrencyDecimal(mrp, mContext));
        holder.discountPer.setText(Integer.valueOf(dis_perc.intValue()) + "%" + " off");

        try {

            if (Integer.valueOf(dis_perc.intValue()) == 0) {
                holder.discountPer.setVisibility(View.GONE);
            }

            holder.discountPer.setText(Integer.valueOf(dis_perc.intValue()) + "%" + " off");
        } catch (Exception e) {
            e.printStackTrace();
        }

        String imageJsonData = movie.getImageJsonData();
        final ArrayList<String> al_img = new ArrayList<>();
        String imagreurl = "";

        try {
            holder.imageView.setImageDrawable(null);
            JSONArray ja_img = new JSONArray(imageJsonData.toString());
            if (ja_img.length() > 0) {
                imagreurl = ja_img.get(0).toString();
                if (!imagreurl.isEmpty()) {
                    holder.imageView.setVisibility(View.GONE);
                    //   holder.imageView.setVisibility(View.GONE);

                    try {

                        Glide.with(mContext).
                                load(imagreurl).
                                diskCacheStrategy(DiskCacheStrategy.ALL).
                                placeholder(R.drawable.defaultplace).into(holder.imageView);


                    } catch (Exception e) {
                        e.printStackTrace();
                        holder.imageView.setVisibility(View.GONE);
                    }
                } else {
                    holder.imageView.setVisibility(View.GONE);
                }

            } else {
                holder.imageView.setVisibility(View.GONE);
            }

        } catch (JSONException e) {
            e.printStackTrace();
            holder.imageView.setVisibility(View.GONE);
        }

        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onImgClick(movie);
            }
        });

        holder.llStock.setVisibility(View.GONE);
        if (movie.getAvailableStock() != null && !movie.getAvailableStock().isEmpty()) {

            holder.tvStock.setText("#" + movie.getAvailableStock() + "");
            holder.llStock.setVisibility(View.VISIBLE);

            //String field_json_data = SharedPrefUserDetail.getString(mContext, SharedPrefUserDetail.field_json_data, "");
            ///String show_stock_position = JsonObjParse.getValueEmpty(field_json_data, "show_stock_position");

           /* if(show_stock_position.equalsIgnoreCase("yes")){
                holder.llStock.setVisibility(View.VISIBLE);
            }
            else {
                holder.llStock.setVisibility(View.GONE);

            }*/

        }

        holder.rlMediaView.setVisibility(View.GONE);

      /*  if (!property.isEmpty()) {
            holder.rlMediaView.setVisibility(View.VISIBLE);
            setMedia(holder, movie, imagreurl);
        }*/
        holder.rlMediaView.setVisibility(View.VISIBLE);
        setMedia(holder, movie, imagreurl);

        holder.iv_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onImgClick(movie);

               /* ShareModel.getI().isImgPrev = true;

                try {
                    JSONArray ja_img = new JSONArray(movie.getImageJsonData().toString());
                    for (int i = 0; i < ja_img.length(); i++) {
                        al_img.add(ja_img.get(i).toString());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                ArrayList<ModelFile> al_model = new ArrayList<>();
                try {
                    for (int i = 0; i < al_img.size(); i++) {
                        al_model.add(new ModelFile(al_img.get(i), false));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                String imgarray = new Gson().toJson(al_model);

                Intent intent = new Intent(mContext, ImagePreviewActivity.class);
                intent.putExtra("imgarray", imgarray);
                intent.putExtra("pos", position);
                intent.putExtra("img_title", movie.getName());
                mContext.startActivity(intent);*/

            }
        });
        holder.ivShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.llBtn.setVisibility(View.GONE);

                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
                String currentDateandTime = sdf.format(new Date());
                try {
                    holder.llMain.setDrawingCacheEnabled(true);
                    Bitmap b = Bitmap.createBitmap(holder.llMain.getDrawingCache());
                    holder.llMain.setDrawingCacheEnabled(false);
                    String path = Environment.getExternalStorageDirectory().toString() + "/" + mContext.getString(R.string.app_name);
                    File root = new File(path);///*Environment.getExternalStorageState()+*/"/DCIM/Camera/" + File.separator + "Birthday Song with Name" + File.separator + getIntent().getStringExtra("from") + File.separator);
                    root.mkdirs();

                    File sdImageMainDirectory = null;
                    sdImageMainDirectory = new File(root, mContext.getString(R.string.app_name) + currentDateandTime + ".jpg");

                    FileOutputStream fOut = new FileOutputStream(sdImageMainDirectory);
                    b.compress(Bitmap.CompressFormat.PNG, 90, fOut);
                    fOut.flush();
                    fOut.close();

                    MediaScannerConnection.scanFile(mContext,
                            new String[]{sdImageMainDirectory.toString()}, null,
                            new MediaScannerConnection.OnScanCompletedListener() {
                                public void onScanCompleted(String path, Uri uri) {

                                }
                            });
                    //   Toast.makeText(mContext, "Download Completed", Toast.LENGTH_SHORT).show();
                    Uri uri = FileProvider.getUriForFile(mContext, mContext.getPackageName() + ".provider", new File(sdImageMainDirectory.getAbsolutePath()));

                    String shareUrl = "http://play.google.com/store/apps/details?id=" + mContext.getString(R.string.consumer_link) + "&referrer=" + sessionManager.getcurrentu_nm();
                    //String link = getString(R.string.BASEURL) + "request-form/" + username + "/" + bridge_id + "/INR";
                    //String extTxt = shareUrl + "\n\nAdd '" + username + "' in My Supplier option to connect to '" + firstname + "'" + "\n\n";

                    Uri shareUri = ((BaseActivity) mContext).buildDeepLink(Uri.parse(shareUrl), 0);
                    shareUrl = shareUri.toString();

                    Intent share = new Intent(android.content.Intent.ACTION_SEND);
                    share.setType("image/jpg");
                    share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                    share.putExtra(Intent.EXTRA_SUBJECT, "Catalogue");
                    // share.putExtra(Intent.EXTRA_TEXT, extTxt);
                    //    share.putExtra("android.intent.extra.STREAM", uri);
                    share.putExtra(Intent.EXTRA_STREAM, uri);
                    share.putExtra(Intent.EXTRA_TEXT, shareUrl);
                    mContext.startActivity(Intent.createChooser(share, "Share"));
                    holder.llBtn.setVisibility(View.VISIBLE);

                } catch (Exception e) {
                    e.printStackTrace();
                    holder.llBtn.setVisibility(View.VISIBLE);

                }

            }
        });

    }

    @Override
    public int getItemCount() {
        return catererList.size();
    }

    public void setFilter(List<CatalougeContent> countryModels) {
        catererList = new ArrayList<>();
        catererList.addAll(countryModels);
        notifyDataSetChanged();
    }

    public void setitem(List<CatalougeContent> products, String filter) {
        this.filter = filter;
        catererList = new ArrayList<>();
        catererList.addAll(products);
        notifyDataSetChanged();

    }

    public void removeItem(int position) {
        catererList.remove(position);
        // notify the item removed by position
        // to perform recycler view delete animations
        // NOTE: don't call notifyDataSetChanged()
        notifyItemRemoved(position);
    }

    String filter = "";

    public String getValues(Double mrpprice, Double discount_percentage, Double discounted_price) {
        Double discountprice = 0.0;

        if (discount_percentage > 0) {
            if (mrpprice > 0) {
                Double totalDisc = (mrpprice * (discount_percentage / 100));
                discountprice = totalDisc;
            }
        } else if (discounted_price > 0) {
            discountprice = discounted_price;
        }

        return String.valueOf(discountprice);
    }

    public String getdiscountmrp(Double mrpprice, Double discount_percentage, Double discounted_price) {
        Double discountprice = mrpprice;
        if (discount_percentage > 0) {
            if (mrpprice > 0) {
                discountprice = mrpprice - (mrpprice * (discount_percentage / 100));
            }
        } else if (discounted_price > 0) {
            discountprice = discounted_price;
        }

        return String.valueOf(discountprice);
    }

    public void setTax(String field_json_data) throws JSONException {

        if (field_json_data != null) {

            String Tax = JsonObjParse.getValueEmpty(field_json_data, "Tax");
            if (!Tax.isEmpty()) {

                JSONObject joTax = new JSONObject(field_json_data);

                JSONArray arr = joTax.getJSONArray("Tax");

                JSONObject element;

                boolean isF1 = true;

                for (int i = 0; i < arr.length(); i++) {
                    element = arr.getJSONObject(i); // which for example will be Types,TotalPoints,ExpiringToday in the case of the first array(All_Details)

                    Iterator keys = element.keys();

                    while (keys.hasNext()) {
                        try {
                            String key = (String) keys.next();

                            String taxVal = JsonObjParse.getValueFromJsonObj(element, key);

                            if (isF1) {
                                isF1 = false;
                                sgst = Double.valueOf(taxVal);


                            } else {
                                cgst = Double.valueOf(taxVal);

                            }

                            Log.e("key", key);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                /*  Charges  */
            } else { // Update

                boolean isF1 = true;

                JSONObject joTax = new JSONObject(field_json_data);

                Iterator keys = joTax.keys();

                while (keys.hasNext()) {
                    try {
                        String key = (String) keys.next();

                        String taxVal = JsonObjParse.getValueFromJsonObj(joTax, key);

                        if (isF1) {
                            isF1 = false;
                            sgst = Double.valueOf(taxVal);


                        } else {
                            cgst = Double.valueOf(taxVal);

                        }

                        Log.e("key", key);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }


            }
        }
    }

   /* public int getExtraPropertyInfo(String extraInfo) throws JSONException {
        int totalInfoVal = 0;
        if (extraInfo != null && !extraInfo.isEmpty()) {
            JSONObject joExtra = new JSONObject(extraInfo);
            Iterator keys = joExtra.keys();
            int i = 0;
            while (keys.hasNext()) {
                try {
                    String key = (String) keys.next();
                    String info = JsonObjParse.getValueFromJsonObj(joExtra, key);

                    if (!info.isEmpty()) {
                        int infoVal = Integer.parseInt(info);
                        totalInfoVal = totalInfoVal + infoVal;
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return totalInfoVal;
    }*/

    public double getExtraPropertyInfo(String extraInfo) throws JSONException {
        double totalInfoVal = 0;
        if (extraInfo != null && !extraInfo.isEmpty()) {

            JSONArray jaExtraInfo = new JSONArray(extraInfo);

            for (int i = 0; i < jaExtraInfo.length(); i++) {
                JSONObject jsonObject = jaExtraInfo.getJSONObject(i);

                Iterator keys = jsonObject.keys();
                while (keys.hasNext()) {
                    try {
                        String key = (String) keys.next();
                        String info = JsonObjParse.getValueFromJsonObj(jsonObject, key);

                        if (!info.isEmpty()) {
                            int infoVal = Integer.parseInt(info);
                            totalInfoVal = totalInfoVal + infoVal;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }
        }

        return totalInfoVal;
    }

    public double getAdditionalDeductinalInfo(String extraInfo, Double mrp) throws JSONException {
        double totalInfoVal = 0;
        if (extraInfo != null && !extraInfo.isEmpty()) {

            JSONArray jaExtraInfo = new JSONArray(extraInfo);

            for (int i = 0; i < jaExtraInfo.length(); i++) {
                JSONObject jsonObject = jaExtraInfo.getJSONObject(i);

                Iterator keys = jsonObject.keys();
                while (keys.hasNext()) {
                    try {
                        String key = (String) keys.next();
                        String info = JsonObjParse.getValueFromJsonObj(jsonObject, key);


                        String ded2 = JsonObjParse.getValueEmpty(info, "act_price");
                        String oDed2 = JsonObjParse.getValueEmpty(info, "org_price");
                        String dedPerc2 = JsonObjParse.getValueEmpty(info, "act_perc");
                        String oDedPerc2 = JsonObjParse.getValueEmpty(info, "org_perc");

                        totalInfoVal = totalInfoVal + calcValuesOfAddDed(mrp, dedPerc2, oDedPerc2, ded2, oDed2);

                        /* if (!info.isEmpty()) {
                             int infoVal = Integer.parseInt(info);
                             totalInfoVal = totalInfoVal + infoVal;
                         }*/
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }
        }

        return totalInfoVal;
    }

    public Double calcValuesOfAddDed(Double mrp, String act_perc, String org_perc, String act_price, String org_price) {

        Double dedPriceForJewel = 0.0;

        if (!act_perc.isEmpty()) {
            Double ded1Val = Double.valueOf(act_perc);
            dedPriceForJewel = dedPriceForJewel + Double.valueOf(getValues(mrp, ded1Val));
        } else if (!org_perc.isEmpty()) {
            Double ded1Val = Double.valueOf(org_perc);
            dedPriceForJewel = dedPriceForJewel + Double.valueOf(getValues(mrp, ded1Val));
        } else if (!act_price.isEmpty()) {
            Double ded1Val = Double.valueOf(act_price);
            dedPriceForJewel = dedPriceForJewel + ded1Val;
        } else if (!org_price.isEmpty()) {
            Double ded1Val = Double.valueOf(org_price);
            dedPriceForJewel = dedPriceForJewel + ded1Val;
        }

        return dedPriceForJewel;
    }

    public String getValues(Double mrpprice, Double discount_percentage) {
        Double discountprice = 0.0;

        if (discount_percentage > 0) {
            if (mrpprice > 0) {
                Double totalDisc = (mrpprice * (discount_percentage / 100));
                discountprice = totalDisc;
            }
        }

        return String.valueOf(discountprice);
    }


    LoadImg loadImg = new LoadImg();
    ImagePagerAdapter imagePagerAdapter;

    public void setMedia(MyViewHolder holder, CatalougeContent movie, String imagreurl) {

        try {

            loadImg.loadCategoryImg(mContext, imagreurl, holder.ivImgTemplate);
            ArrayList<String> al_img = new ArrayList<>();

            try {
                JSONArray ja_img = new JSONArray(movie.getImageJsonData().toString());
                for (int i = 0; i < ja_img.length(); i++) {
                    al_img.add(ja_img.get(i).toString());
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (al_img.size() == 0 || al_img == null) {
                holder.iv_update.setVisibility(View.GONE);
                holder.iv_update.setVisibility(View.GONE);
                holder.ivShare.setVisibility(View.VISIBLE);
                holder.llBtn.setVisibility(View.VISIBLE);
            } else {
                holder.iv_update.setVisibility(View.VISIBLE);
                holder.iv_update.setVisibility(View.VISIBLE);
                holder.ivShare.setVisibility(View.VISIBLE);
                holder.llBtn.setVisibility(View.VISIBLE);
            }

            imagePagerAdapter = new ImagePagerAdapter(mContext, al_img, movie.getName());
            holder.pager.setAdapter(imagePagerAdapter);
            //holder.indicator.setViewPager(holder.pager);
            holder.imageView.setVisibility(View.GONE);
            holder.pager.setVisibility(View.GONE);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    ArrayList<SurveyOptions> alOptions = new ArrayList<>();

    public void setSurveyInfo(final MyViewHolder holder, int pos, final CatalougeContent catelog, String survey) {

        String optionsList = JsonObjParse.getValueEmpty(survey, "SurveyOptions");

        // Fst enter to db at time of selection and then check here with DB

        SurveyOptions surveyOptions = new Gson().fromJson(optionsList, SurveyOptions.class);
        alOptions = surveyOptions.alOptions;

        if (alOptions == null) alOptions = new ArrayList<>();


        final SurveyOptionsListAdapter surveyOptionsListAdapter = new SurveyOptionsListAdapter(pos, alOptions, mContext, new SurveyOptionsListAdapter.OnItemClickListener() {
            @Override
            public void onDelete(int item) {

            }

            @Override
            public void onEdit(int item, ArrayList<SurveyOptions> movies) {
                holder.rl_base_cat.performClick();
            }

        }, false);

        //rvSurveyOptions.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        holder.rvSurveyOptions.setLayoutManager(new GridLayoutManager(mContext, 2));
        holder.rvSurveyOptions.setAdapter(surveyOptionsListAdapter);
        holder.rvSurveyOptions.setNestedScrollingEnabled(false);
    }
}
