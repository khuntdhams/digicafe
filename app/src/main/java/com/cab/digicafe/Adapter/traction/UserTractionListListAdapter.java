package com.cab.digicafe.Adapter.traction;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cab.digicafe.MyCustomClass.JsonObjParse;
import com.cab.digicafe.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class UserTractionListListAdapter extends RecyclerView.Adapter<UserTractionListListAdapter.MyViewHolder> {


    ArrayList<ModelUserTraction> alUserTraction;
    private Context mContext;

    OnItemClickListener onItemClickListener;

    public interface OnItemClickListener {
        void onDelete(int item);

        void onEdit(int item);
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvUserNm)
        TextView tvUserNm;

        @BindView(R.id.tvShopNm)
        TextView tvShopNm;

        @BindView(R.id.tvTime)
        TextView tvTime;

        @BindView(R.id.tvClicks)
        TextView tvClicks;


        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

        }
    }


    public UserTractionListListAdapter(ArrayList<ModelUserTraction> moviesList, Context context, OnItemClickListener onItemClickListener) {
        this.alUserTraction = moviesList;
        this.onItemClickListener = onItemClickListener;
        this.mContext = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_ad_user_traction, parent, false);

        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {


        ModelUserTraction productTotalDiscount = alUserTraction.get(position);
        holder.tvClicks.setText("Total view : " + productTotalDiscount.total_visited + "");

        String firstname = JsonObjParse.getValueEmpty(productTotalDiscount.field_json_data, "firstname");
        String location = JsonObjParse.getValueEmpty(productTotalDiscount.field_json_data, "location");
        String contact_no = JsonObjParse.getValueEmpty(productTotalDiscount.field_json_data, "contact_no");


        holder.tvShopNm.setVisibility(View.GONE);
        holder.tvTime.setText("Last view date : " + productTotalDiscount.updated_date + "");
        holder.tvUserNm.setText("Viewed  by : " + firstname + "\nLocation : " + location + "\nContact No. : " + contact_no);


    }

    @Override
    public int getItemCount() {
        return alUserTraction.size();
    }

    public void setItem(ArrayList<ModelUserTraction> path) {
        alUserTraction = new ArrayList<>();
        alUserTraction.addAll(path);
        notifyDataSetChanged();
    }

}
