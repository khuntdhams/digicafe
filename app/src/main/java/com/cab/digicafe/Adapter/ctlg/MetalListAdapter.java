package com.cab.digicafe.Adapter.ctlg;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cab.digicafe.Model.Metal;
import com.cab.digicafe.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class MetalListAdapter extends RecyclerView.Adapter<MetalListAdapter.MyViewHolder> {


    ArrayList<Metal> alRange;
    private Context mContext;

    OnItemClickListener onItemClickListener;

    public interface OnItemClickListener {
        void onDelete(int item);

        void onEdit(int item);
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvOptions)
        TextView tvOptions;

        @BindView(R.id.tvEmoji)
        TextView tvEmoji;

        @BindView(R.id.ivDelete)
        ImageView ivDelete;

        @BindView(R.id.llBs)
        LinearLayout llBs;


        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

        }
    }


    String currency = "";

    public MetalListAdapter(ArrayList<Metal> moviesList, Context context, OnItemClickListener onItemClickListener, String currency) {
        this.alRange = moviesList;
        this.onItemClickListener = onItemClickListener;
        this.mContext = context;
        this.currency = currency;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_ad_metals, parent, false);

        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {


        Metal productTotalDiscount = alRange.get(position);
        int p = position + 65;

        //String c = Character.toString((char) p);


        holder.llBs.setBackground(null);
        if (productTotalDiscount.isSelect) {

            holder.llBs.setBackground(ContextCompat.getDrawable(mContext, R.drawable.strokes_sq_green));
            holder.ivDelete.setColorFilter(ContextCompat.getColor(mContext, R.color.white));
            holder.tvOptions.setTextColor(ContextCompat.getColor(mContext, R.color.infoclr));

        } else {

            holder.llBs.setBackground(ContextCompat.getDrawable(mContext, R.drawable.strokes_sq_red));
            holder.tvOptions.setTextColor(ContextCompat.getColor(mContext, R.color.white));
            holder.ivDelete.setColorFilter(ContextCompat.getColor(mContext, R.color.infoclr));

        }


        holder.ivDelete.setVisibility(View.VISIBLE);
        holder.ivDelete.setImageResource(R.drawable.ic_tick);


        holder.tvOptions.setTextColor(ContextCompat.getColor(mContext, R.color.c2));
        holder.tvEmoji.setTextColor(ContextCompat.getColor(mContext, R.color.infoclr));

        holder.tvOptions.setText(productTotalDiscount.metalNm);
        holder.tvEmoji.setText(currency + " " + productTotalDiscount.metalPrice + " / " + productTotalDiscount.unit + "");


        holder.llBs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onItemClickListener.onEdit(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return alRange.size();
    }

    public void setItem(ArrayList<Metal> path) {
        alRange = new ArrayList<>();
        alRange = path;
        notifyDataSetChanged();
    }

}
