package com.cab.digicafe.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cab.digicafe.Callback.OnLoadMoreListener;
import com.cab.digicafe.Helper.Inad;
import com.cab.digicafe.Helper.SharedPrefUserDetail;
import com.cab.digicafe.Model.Catelog;
import com.cab.digicafe.Model.Chitproducts;
import com.cab.digicafe.R;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by CSS on 02-11-2017.
 */

public class SummaryAdapter extends RecyclerView.Adapter<SummaryAdapter.MyViewHolder> {
    private List<Chitproducts> productlist;
    private Context mContext;
    String currencysymbol;
    String roundofvalue;
    String currency = "";
    private OnLoadMoreListener mOnLoadMoreListener;

    public interface OnItemClickListener {
        void onItemClick(Catelog item);


    }

    private OnItemClickListener listener;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txt_serialno, txt_productname, txt_productqty, txt_productprice, tv_offer;


        public MyViewHolder(View view) {
            super(view);
            txt_serialno = (TextView) view.findViewById(R.id.serialno);
            txt_productname = (TextView) view.findViewById(R.id.productname);
            txt_productqty = (TextView) view.findViewById(R.id.productqty);
            txt_productprice = (TextView) view.findViewById(R.id.productprice);
            tv_offer = (TextView) view.findViewById(R.id.tv_offer);

        }
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }

    public SummaryAdapter(List<Chitproducts> chititemlist, Context context, OnItemClickListener listener, String currencysymbol, String roundofvalue) {
        this.productlist = chititemlist;
        this.mContext = context;
        this.listener = listener;
        this.currencysymbol = currencysymbol;
        this.roundofvalue = roundofvalue;
        currency = SharedPrefUserDetail.getString(context, SharedPrefUserDetail.symbol_native, "") + " ";

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_summary, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final Chitproducts chitproduct = productlist.get(position);
        Double itemprice = Double.parseDouble(chitproduct.getQuantity());
        int itempriceval = Integer.valueOf(itemprice.intValue());
        holder.txt_serialno.setText("# " + itempriceval);
        holder.txt_productname.setText(chitproduct.getParticulars());
        if (holder.tv_offer.getText().equals("")) {

        }
        holder.tv_offer.setText(chitproduct.getOffer());
        DecimalFormat twoDForm = new DecimalFormat("#.##");
        Double formatedprice = 0.00;
        Double formatedtotal = 0.00;

        try {
            Double price = Double.parseDouble(chitproduct.getPrice()); // Make use of autoboxing.  It's also easier to read.
            Double total = Double.parseDouble(chitproduct.getTotal()); // Make use of autoboxing.  It's also easier to read.

            formatedprice = Double.valueOf(twoDForm.format(price));
            formatedtotal = Double.valueOf(twoDForm.format(total));

        } catch (NumberFormatException e) {
            // p did not contain a valid double
        }
//        holder.txt_productqty.setText(currencysymbol+" "+String.format("%.2f", formatedprice));
//        holder.txt_productprice.setText(currencysymbol+" "+String.format("%.2f", formatedtotal));

        //holder.txt_productqty.setText(currency+String.format("%.2f", formatedprice));
        holder.txt_productqty.setText(currency + Inad.getCurrencyDecimal(formatedprice, mContext));
        //holder.txt_productprice.setText(currency+String.format("%.2f", formatedtotal));
        holder.txt_productprice.setText(currency + Inad.getCurrencyDecimal(formatedtotal, mContext));
    }

    @Override
    public int getItemCount() {
        return productlist.size();
    }

    public void setFilter(List<Chitproducts> countryModels) {
        productlist = new ArrayList<>();
        productlist.addAll(countryModels);
        notifyDataSetChanged();
    }

    public void setitem(List<Chitproducts> products) {
        this.productlist = products;
        notifyDataSetChanged();

    }
}
