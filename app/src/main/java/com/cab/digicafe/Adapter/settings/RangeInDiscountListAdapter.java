package com.cab.digicafe.Adapter.settings;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cab.digicafe.Model.ProductTotalDiscount;
import com.cab.digicafe.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class RangeInDiscountListAdapter extends RecyclerView.Adapter<RangeInDiscountListAdapter.MyViewHolder> {


    ArrayList<ProductTotalDiscount> alRange;
    private Context mContext;

    OnItemClickListener onItemClickListener;

    public interface OnItemClickListener {
        void onDelete(int item);

        void onEdit(int item);
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvMin)
        TextView tvMin;

        @BindView(R.id.tvMax)
        TextView tvMax;

        @BindView(R.id.tvDisc)
        TextView tvDisc;

        @BindView(R.id.ivDel)
        ImageView ivDel;

        @BindView(R.id.ivEdit)
        ImageView ivEdit;

        @BindView(R.id.rlDel)
        RelativeLayout rlDel;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

        }
    }


    public RangeInDiscountListAdapter(ArrayList<ProductTotalDiscount> moviesList, Context context, String c, OnItemClickListener onItemClickListener) {
        this.alRange = moviesList;
        this.onItemClickListener = onItemClickListener;
        this.mContext = context;
        currency = c + " ";

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_ad_range_minmax, parent, false);

        return new MyViewHolder(itemView);
    }

    String currency = "";

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {


        ProductTotalDiscount productTotalDiscount = alRange.get(position);
        holder.tvMin.setText(productTotalDiscount.min_range);
        holder.tvMax.setText(productTotalDiscount.max_range);
        holder.tvDisc.setText(productTotalDiscount.disc_range);

        holder.ivDel.setVisibility(View.GONE);

        if (position == alRange.size() - 1) {
            holder.ivDel.setVisibility(View.VISIBLE);
        }

        holder.ivDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onItemClickListener.onDelete(position);
            }
        });


        holder.ivEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onItemClickListener.onEdit(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return alRange.size();
    }

    public void setItem(ArrayList<ProductTotalDiscount> path) {
        alRange = new ArrayList<>();
        alRange.addAll(path);
        notifyDataSetChanged();
    }

}
