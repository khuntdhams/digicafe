package com.cab.digicafe.Adapter.mrp;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cab.digicafe.R;
import com.cab.digicafe.Timeline.DateTimeUtils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class PaymentListAdapter extends RecyclerView.Adapter<PaymentListAdapter.MyViewHolder> {


    ArrayList<PaymentViewModel> alUserTraction;
    private Context mContext;

    OnItemClickListener onItemClickListener;

    public interface OnItemClickListener {
        void onDelete(int item);

        void onEdit(int item);
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.llPayView)
        LinearLayout llPayView;

        @BindView(R.id.ivEdit)
        ImageView ivEdit;

        @BindView(R.id.ivDelete)
        ImageView ivDelete;

        @BindView(R.id.tvPayDetails)
        TextView tvPayDetails;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

        }
    }

    String symbol_native = "";

    public PaymentListAdapter(ArrayList<PaymentViewModel> moviesList, Context context, OnItemClickListener onItemClickListener, String symbol_native) {
        this.alUserTraction = moviesList;
        this.onItemClickListener = onItemClickListener;
        this.mContext = context;
        this.symbol_native = symbol_native;
        setHasStableIds(true);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_ad_payment, parent, false);

        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        PaymentViewModel paymentViewModel = alUserTraction.get(position);


        String pay_date = DateTimeUtils.parseDateTime(paymentViewModel.payment_date, "dd MMM yyyy hh:mm aaa", "dd MMM yyyy");


        holder.tvPayDetails.setText("Payment of " + symbol_native + "" + paymentViewModel.payment_amount + " will paying by " + paymentViewModel.payment_reference + " at " + pay_date + ".");

        holder.ivEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.onEdit(position);

            }
        });

        holder.ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.onDelete(position);
            }
        });


    }

    @Override
    public int getItemCount() {
        return alUserTraction.size();
    }

    public void setItem(ArrayList<PaymentViewModel> path) {
        alUserTraction = new ArrayList<>();
        alUserTraction.addAll(path);
        notifyDataSetChanged();
    }

}
