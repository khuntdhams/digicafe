package com.cab.digicafe.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.cab.digicafe.Model.ModelFile;
import com.cab.digicafe.R;

import java.util.ArrayList;

public class TeethImageAdapter extends RecyclerView.Adapter<TeethImageAdapter.MyViewHolder> {

    private ArrayList<ModelFile> teethImageList;
    Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView ivTeethImage;

        public MyViewHolder(View view) {
            super(view);
            ivTeethImage = (ImageView) view.findViewById(R.id.ivTeethImage);
        }
    }

    public TeethImageAdapter(ArrayList<ModelFile> teethImageList, Context context) {
        this.teethImageList = teethImageList;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.teeth_image_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        ModelFile movie = teethImageList.get(position);
        Glide.with(context).load(movie.getImgpath()).diskCacheStrategy(DiskCacheStrategy.SOURCE).into(holder.ivTeethImage);

    }


    @Override
    public int getItemCount() {
        return teethImageList.size();
    }
}