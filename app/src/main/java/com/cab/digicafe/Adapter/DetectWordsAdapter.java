package com.cab.digicafe.Adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cab.digicafe.Database.SqlLiteDbHelper;
import com.cab.digicafe.Model.ModelFile;
import com.cab.digicafe.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by CSS on 02-11-2017.
 */

public class DetectWordsAdapter extends RecyclerView.Adapter<DetectWordsAdapter.MyViewHolder> {
    private List<ModelFile> data;
    private Context mContext;
    private SqlLiteDbHelper dbHelper;
    boolean isvdo = false;

    public interface OnItemClickListener {
        void onItemClick(String item, int pos);

        void onDelete(int pos);

    }

    private OnItemClickListener listener;

    public class MyViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.iv_img)
        ImageView iv_img;

        @BindView(R.id.tv_word)
        TextView tv_word;


        @BindView(R.id.ll_word)
        LinearLayout ll_word;


        public MyViewHolder(View view) {
            super(view);

            ButterKnife.bind(this, view);

        }
    }


    public DetectWordsAdapter(List<ModelFile> moviesList, Context context, OnItemClickListener listener, boolean isvdo) {
        this.data = moviesList;
        this.mContext = context;
        this.isvdo = isvdo;
        this.listener = listener;
        dbHelper = new SqlLiteDbHelper(context);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_detect_words, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        ModelFile modelFile = data.get(position);

        if (modelFile.isIstitle()) {
            holder.tv_word.setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
            holder.iv_img.setVisibility(View.GONE);
        } else {
            holder.tv_word.setTextColor(ContextCompat.getColor(mContext, R.color.black));
            holder.iv_img.setVisibility(View.VISIBLE);
        }

        if (modelFile.isIschecked()) {
            holder.iv_img.setVisibility(View.VISIBLE);
        } else {
            holder.iv_img.setVisibility(View.GONE);
        }

        holder.tv_word.setText(modelFile.getDetctstring());

        holder.ll_word.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!data.get(position).isIstitle()) {
                    listener.onItemClick(data.get(position).getDetctstring(), data.get(position).getPos());

                    for (int i = 0; i < data.size(); i++) {
                        data.get(i).setIschecked(false);
                    }
                    data.get(position).setIschecked(true);
                    notifyDataSetChanged();
                }
            }
        });


    }

    public void setItem(ArrayList<ModelFile> path) {
        data = new ArrayList<>();
        data.addAll(path);
        notifyDataSetChanged();

    }


    @Override
    public int getItemCount() {
        return data.size();
    }
}
