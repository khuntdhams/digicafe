package com.cab.digicafe.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cab.digicafe.ChitlogActivity;
import com.cab.digicafe.Database.SettingDB;
import com.cab.digicafe.Database.SettingModel;
import com.cab.digicafe.DetailsFragment;
import com.cab.digicafe.Fragments.SummaryDetailFragmnet;
import com.cab.digicafe.Helper.Inad;
import com.cab.digicafe.Model.Chit;
import com.cab.digicafe.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by CSS on 02-11-2017.
 */

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.MyViewHolder> {
    private List<Chit> catererList;
    private Context mContext;
    SettingModel sm = new SettingModel();
    String currency = "";

    public interface OnItemClickListener {
        void onItemClick(Chit item);

        void onItemLongClick(Chit item, int pos);

    }

    private HistoryAdapter.OnItemClickListener listener;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_mob, caterername, txt_orderid, txt_orderdate, txt_itemcount, txt_chitamount, txt_paymentmode, txt_status, txt_purpose, statuslbl;
        public RelativeLayout viewForeground, rl_address, rl_del_date;
        private ImageView imageView;
        TextView tv_date, tv_address, tv_count;
        LinearLayout mainlayout;

        public MyViewHolder(View view) {
            super(view);
            txt_purpose = (TextView) view.findViewById(R.id.name);
            tv_mob = (TextView) view.findViewById(R.id.tv_mob);
            txt_chitamount = (TextView) view.findViewById(R.id.amount);
            mainlayout = (LinearLayout) view.findViewById(R.id.mainlayout);
            txt_paymentmode = (TextView) view.findViewById(R.id.payment);
            txt_status = (TextView) view.findViewById(R.id.status);
            caterername = (TextView) view.findViewById(R.id.caterername);
            txt_orderid = (TextView) view.findViewById(R.id.orderid);
            txt_orderdate = (TextView) view.findViewById(R.id.dateval);
            txt_itemcount = (TextView) view.findViewById(R.id.itemcount);
            viewForeground = (RelativeLayout) view.findViewById(R.id.view_foreground);
            rl_del_date = (RelativeLayout) view.findViewById(R.id.rl_del_date);
            rl_address = (RelativeLayout) view.findViewById(R.id.rl_address);

            imageView = (ImageView) view.findViewById(R.id.profile_image);

            tv_date = (TextView) view.findViewById(R.id.tv_date);
            tv_address = (TextView) view.findViewById(R.id.tv_address);
            statuslbl = (TextView) view.findViewById(R.id.statuslbl);
            tv_count = (TextView) view.findViewById(R.id.tv_count);
        }
    }


    public HistoryAdapter(List<Chit> moviesList, Context context, OnItemClickListener listener) {
        this.catererList = moviesList;
        this.mContext = context;
        this.listener = listener;

        final SettingDB db = new SettingDB(mContext);
        List<SettingModel> list_setting = db.GetItems();
        sm = list_setting.get(0);

        //currency = SharedPrefUserDetail.getString(context, SharedPrefUserDetail.symbol_native,"") + " ";


    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.hiwtory_list_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final Chit chit_obj = catererList.get(position);
        //  holder.name.setText(movie.getName());0

        currency = chit_obj.getSymbol_native() + " ";

        int c_pos = position + 1;
        holder.tv_count.setText("[ " + c_pos + " ]");

        if (chit_obj.isIslongpress()) {
            holder.mainlayout.setBackgroundColor(Color.parseColor("#FF7777"));
        } else {
            holder.mainlayout.setBackgroundColor(Color.parseColor("#ffffff"));
        }

        if (chit_obj.getSubject().length() > 0) {
            holder.caterername.setText(chit_obj.getSubject());
        } else {
            if (chit_obj.getTo_bridgelist_friendlyname().size() > 0) {
                holder.caterername.setText(chit_obj.getTo_bridgelist_friendlyname().get(0).toString());
            }
        }
        //holder.txt_purpose.setText(chit_obj.getPurpose() + " / " + "OTP");
        holder.txt_purpose.setText("OTP");
        holder.txt_orderid.setText(chit_obj.getCase_id());
        holder.txt_orderid.setTextColor(Color.parseColor("#0000EE"));
        holder.txt_orderid.setPaintFlags(holder.txt_orderid.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        /// holder.txt_orderid.setText(chit_obj.getRef_id() + " / " + chit_obj.getCase_id());
        holder.txt_orderdate.setText(chit_obj.getCreated_date());

        //api key value should be change
        //   holder.txt_itemcount.setText("# "+chit_obj.getChit_item_count()+"");
        //Double itemprice = Double.parseDouble(chit_obj.getTotal_chit_item_count());
        int itempriceval = chit_obj.getChit_item_count();

        holder.txt_itemcount.setText("# " + itempriceval);
        DecimalFormat twoDForm = new DecimalFormat("#.##");
        Double formatedprice = 0.00;

        try {
            // change api key
            //    Double price = Double.parseDouble(chit_obj.getTotal_chit_item_value()); // Make use of autoboxing.  It's also easier to read.
            Double price = Double.parseDouble(chit_obj.getTotal_chit_item_value());
            formatedprice = Double.valueOf(twoDForm.format(price));

        } catch (NumberFormatException e) {
            // p did not contain a valid double
        }
        //need to add currency symbol
        //holder.txt_chitamount.setText(chit_obj.getSymbol_native()+" "+String.format("%.2f", formatedprice)+"");
        //holder.txt_chitamount.setText(currency + String.format("%.2f", formatedprice) + "");
        holder.txt_chitamount.setText(currency + Inad.getCurrencyDecimal(formatedprice, mContext) + "");
        //holder.txt_status.setText(chit_obj.getTransaction_status());
        holder.txt_status.setText(chit_obj.getRef_id());


        holder.statuslbl.setText(chit_obj.getPurpose() + "");

        holder.tv_mob.setText(chit_obj.getContact_number());

        try {
            JSONObject jo_summary = new JSONObject(chit_obj.getFooter_note());
            holder.txt_paymentmode.setText(jo_summary.getString("PAYMENT_MODE"));

            if (jo_summary.has("DATE")) {
                holder.tv_date.setText(jo_summary.getString("DATE"));
                if (jo_summary.getString("DATE").equals("") || sm.getIsdate().equals("0")) {

                    holder.rl_del_date.setVisibility(View.GONE);
                } else {
                    holder.rl_del_date.setVisibility(View.VISIBLE);
                }
            } else {
                holder.rl_del_date.setVisibility(View.GONE);
            }

            if (jo_summary.has("ADDRESS")) {

                holder.tv_address.setText(jo_summary.getString("ADDRESS"));

                if (jo_summary.getString("ADDRESS").equals("") || sm.getIsaddress().equals("0")) {

                    holder.rl_address.setVisibility(View.GONE);
                } else {
                    holder.rl_address.setVisibility(View.VISIBLE);
                }
            } else {
                holder.rl_address.setVisibility(View.GONE);
            }


        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        holder.mainlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // listener.onItemClick(chit_obj);

                Intent i = new Intent(mContext, ChitlogActivity.class);
                i.putExtra("chit_hash_id", chit_obj.getChit_hash_id());
                i.putExtra("chit_id", chit_obj.getChit_id() + "");
                i.putExtra("transtactionid", chit_obj.getChit_name() + "");
                i.putExtra("productqty", chit_obj.getTotal_chit_item_value() + "");
                i.putExtra("productprice", chit_obj.getChit_item_count() + "");
                i.putExtra("purpose", chit_obj.getPurpose());
                i.putExtra("Refid", chit_obj.getRef_id());
                i.putExtra("Caseid", chit_obj.getCase_id());
                DetailsFragment.chititemcount = "" + chit_obj.getChit_item_count();
                DetailsFragment.chititemprice = "" + chit_obj.getTotal_chit_item_value();
                SummaryDetailFragmnet.item = chit_obj;
                DetailsFragment.item_detail = chit_obj;
                mContext.startActivity(i);
            }
        });

        holder.mainlayout.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                listener.onItemLongClick(chit_obj, position);
                return true;
            }
        });


    }

    @Override
    public int getItemCount() {
        return catererList.size();
    }

    public void removeItem(int position) {
        catererList.remove(position);
        // notify the item removed by position
        // to perform recycler view delete animations
        // NOTE: don't call notifyDataSetChanged()
        notifyItemRemoved(position);
    }

    public void restoreItem(Chit item, int position) {
        catererList.add(position, item);
        // notify item added by position
        notifyItemInserted(position);
    }

    public void setFilter(List<Chit> countryModels) {
        catererList = new ArrayList<>();
        catererList.addAll(countryModels);
        notifyDataSetChanged();
    }

}
