package com.cab.digicafe.Adapter.mrp;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cab.digicafe.Activities.mrp.AnimDialogueWithField;
import com.cab.digicafe.Activities.mrp.PlanningDialogue;
import com.cab.digicafe.Helper.Inad;
import com.cab.digicafe.Model.ShareModel;
import com.cab.digicafe.MyCustomClass.JsonObjParse;
import com.cab.digicafe.R;
import com.google.gson.JsonObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class PlanningListAdapter extends RecyclerView.Adapter<PlanningListAdapter.MyViewHolder> {


    ArrayList<TaskPrefViewModel> alUserTraction;
    private Context mContext;
    OnItemClickListener onItemClickListener;

    public interface OnItemClickListener {
        void onDelete(int item);

        void onEdit(int item);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvActAmt)
        TextView tvActAmt;

        @BindView(R.id.tvPlanAmt)
        TextView tvPlanAmt;

        @BindView(R.id.tvTaskStatus)
        TextView tvTaskStatus;

        @BindView(R.id.tvFromNm)
        TextView tvFromNm;

        @BindView(R.id.tvEditDelDtFromOrTo)
        TextView tvEditDelDtFromOrTo;

        @BindView(R.id.tvActualDate)
        TextView tvActualDate;

        @BindView(R.id.llMrpView)
        LinearLayout llMrpView;

        @BindView(R.id.llBottomView)
        LinearLayout llBottomView;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public PlanningListAdapter(ArrayList<TaskPrefViewModel> moviesList, Context context, OnItemClickListener onItemClickListener) {
        this.alUserTraction = moviesList;
        this.onItemClickListener = onItemClickListener;
        this.mContext = context;
        //setHasStableIds(true);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_ad_planning, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        holder.llBottomView.setVisibility(View.GONE);
        if (position == alUserTraction.size() - 1) {
            holder.llBottomView.setVisibility(View.VISIBLE);
        }

        TaskPrefViewModel productTotalDiscount = alUserTraction.get(position);


        holder.tvActAmt.setVisibility(View.VISIBLE);
        holder.tvPlanAmt.setVisibility(View.VISIBLE);
        holder.llMrpView.setBackgroundColor(ContextCompat.getColor(mContext, R.color.mrp_link));

        //holder.llMrpView.setVisibility(View.VISIBLE);

        String MRP_TAB = productTotalDiscount.getToFieldJsonData();
        if (!MRP_TAB.equalsIgnoreCase("") && MRP_TAB.length() > 2) { // {}
            try {

                String forWhich = productTotalDiscount.getFromFieldJsonData();

                if (ShareModel.getI().isFromAllTask) {
                    //forWhich = JsonObjParse.getValueEmpty(jsonObject.toString(), "To");
                    forWhich = productTotalDiscount.getToFieldJsonData();
                } else {
                    //forWhich = JsonObjParse.getValueEmpty(jsonObject.toString(), "From");
                    forWhich = productTotalDiscount.getFromFieldJsonData();
                }

                //String link_status = JsonObjParse.getValueEmpty(forWhich, "status");
                String link_status = productTotalDiscount.getStatus();

                switch (link_status.toLowerCase()) {
                    case "link":
                        holder.llMrpView.setBackgroundColor(ContextCompat.getColor(mContext, R.color.mrp_link));
                        break;
                    case "delink":
                        holder.llMrpView.setBackgroundColor(ContextCompat.getColor(mContext, R.color.mrp_dlink));
                        break;
                    case "complete":
                        holder.llMrpView.setBackgroundColor(ContextCompat.getColor(mContext, R.color.mrp_complete));
                        break;
                    case "issue":
                        holder.llMrpView.setBackgroundColor(ContextCompat.getColor(mContext, R.color.mrp_issue));
                        break;
                    case "deleted":
                        //holder.llMrpView.setVisibility(View.GONE);
                        holder.llMrpView.setBackgroundColor(ContextCompat.getColor(mContext, R.color.mrp_issue));
                        break;
                    default:
                        break;
                }

                String planning = JsonObjParse.getValueEmpty(forWhich, "planning");

                String activity_title = JsonObjParse.getValueEmpty(planning, "task_title");
                holder.tvFromNm.setText(activity_title + "");

               /*
                String purpose = JsonObjParse.getValueEmpty(forWhich, "purpose");
                holder.tvPurposeOrOtp.setText(purpose + " / " + "OTP");

                String ref_id = JsonObjParse.getValueEmpty(forWhich, "ref_id");
                String case_id = JsonObjParse.getValueEmpty(forWhich, "case_id");
                holder.tvRefId.setText(ref_id);
                holder.tvCaseId.setText(case_id);

                String chit_item_count = JsonObjParse.getValueEmpty(forWhich, "chit_item_count");
                holder.tv_chit_item_count.setText("# " + chit_item_count);

                String transaction_status = JsonObjParse.getValueEmpty(forWhich, "transaction_status");
                holder.tv_transaction_status.setText(transaction_status);

                String location = JsonObjParse.getValueEmpty(forWhich, "location");
                holder.tv_location.setText(location);*/

                // Date , Amount


                String plan_start_date = JsonObjParse.getValueEmpty(planning, "plan_start_date");
                String plan_end_date = JsonObjParse.getValueEmpty(planning, "plan_end_date");
                String plan = "Plan : " + plan_start_date + " - " + plan_end_date;
                holder.tvEditDelDtFromOrTo.setText(plan + "");


                String act_end_date = JsonObjParse.getValueEmpty(planning, "act_end_date");
                String act_start_date = JsonObjParse.getValueEmpty(planning, "act_start_date");
                String act = "Actual : " + act_start_date + " - " + act_end_date;
                holder.tvActualDate.setText(act + "");

                String plan_amount = JsonObjParse.getValueEmpty(planning, "plan_amount");
                String act_amount = JsonObjParse.getValueEmpty(planning, "act_amount");


                Double plan_amount_val = 0.0;
                if (!plan_amount.isEmpty()) {
                    plan_amount_val = Double.valueOf(plan_amount);
                    String symbol_native = JsonObjParse.getValueEmpty(forWhich, "symbol_native") + " ";
                    holder.tvPlanAmt.setText(symbol_native + "" + Inad.getCurrencyDecimal(plan_amount_val, mContext));
                } else {
                    holder.tvPlanAmt.setVisibility(View.GONE);
                }

                holder.tvPlanAmt.setPaintFlags(holder.tvPlanAmt.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));


                Double act_amount_val = 0.0;
                if (!act_amount.isEmpty()) {
                    holder.tvPlanAmt.setPaintFlags(holder.tvPlanAmt.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                    act_amount_val = Double.valueOf(act_amount);
                    String symbol_native = JsonObjParse.getValueEmpty(forWhich, "symbol_native") + " ";
                    holder.tvActAmt.setText(symbol_native + "" + Inad.getCurrencyDecimal(act_amount_val, mContext));
                } else {
                    holder.tvActAmt.setVisibility(View.GONE);
                }


                String task_status = JsonObjParse.getValueEmpty(planning, "task_status");
                holder.tvTaskStatus.setText(task_status + "");

                Drawable mDrawable = mContext.getResources().getDrawable(R.drawable.fill_round);

                switch (task_status.toLowerCase()) {
                    case "planned":
                        mDrawable.setColorFilter(new PorterDuffColorFilter(ContextCompat.getColor(mContext, R.color.status1), PorterDuff.Mode.MULTIPLY));
                        break;
                    case "doing":
                        mDrawable.setColorFilter(new PorterDuffColorFilter(ContextCompat.getColor(mContext, R.color.status2), PorterDuff.Mode.MULTIPLY));
                        break;
                    case "done":
                        mDrawable.setColorFilter(new PorterDuffColorFilter(ContextCompat.getColor(mContext, R.color.status3), PorterDuff.Mode.MULTIPLY));
                        break;
                    default:
                        break;

                }

                holder.tvTaskStatus.setBackground(mDrawable);
            } catch (Exception e) {
                e.printStackTrace();
            }


        }


        holder.llMrpView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ShareModel.getI().taskPrefViewModel = alUserTraction.get(position);

                PlanningDialogue planningDialogue = new PlanningDialogue(mContext, new PlanningDialogue.OnDialogClickListener() {
                    @Override
                    public void onDialogImageRunClick(int joMrp) {

                        if (joMrp == 2) {
                            onItemClickListener.onEdit(0);
                        }


                    }
                }, " Update sub-task", mContext.getString(R.string.planning));

                planningDialogue.show();


            }

        });


        holder.llMrpView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                ShareModel.getI().taskPrefViewModel = alUserTraction.get(position);
                AnimDialogueWithField commonConfirmDialogue = new AnimDialogueWithField(mContext, new AnimDialogueWithField.OnDialogClickListener() {
                    @Override
                    public void onDialogImageRunClick(JsonObject joMrp) {
                        onItemClickListener.onEdit(0);
                    }
                }, " Change status", mContext.getString(R.string.status_selection));
                commonConfirmDialogue.show();
                return true;

            }

        });

    }

    @Override
    public int getItemCount() {
        return alUserTraction.size();
    }

    public void setItem(ArrayList<TaskPrefViewModel> path) {
        alUserTraction = new ArrayList<>();
        alUserTraction.addAll(path);
        notifyDataSetChanged();
    }

}
