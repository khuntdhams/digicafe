package com.cab.digicafe.Adapter;

import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.cab.digicafe.Fragments.CatalougeContent;
import com.cab.digicafe.Model.templateModel;
import com.cab.digicafe.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TemplateAdapter extends RecyclerView.Adapter<TemplateAdapter.MyViewHolder> {

    ArrayList<templateModel> templateList = new ArrayList<>();
    Context context;
    private OnItemClickListener listener;

    public interface OnItemClickListener {

        void onImgClick(int pos);

    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.ivImageTemplate)
        ImageView ivImageTemplate;
        @BindView(R.id.tvTemplateName)
        TextView tvTemplateName;
        @BindView(R.id.iv_update)
        ImageView iv_update;
        @BindView(R.id.llBtn)
        LinearLayout llBtn;

        public MyViewHolder(View view) {
            super(view);

            ButterKnife.bind(this, view);
        }
    }

    public TemplateAdapter(ArrayList<templateModel> templateList, Context context, OnItemClickListener listener) {
        this.templateList = templateList;
        this.context = context;
        this.listener = listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.template_item_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        final templateModel modelFile = templateList.get(position);

        holder.tvTemplateName.setText(modelFile.getTemplateName());
        if (modelFile.getImage().isEmpty()) {
            holder.llBtn.setVisibility(View.GONE);
            Glide.with(context).load(modelFile.getTemplateImage()).diskCacheStrategy(DiskCacheStrategy.SOURCE).into(holder.ivImageTemplate);
        } else {
            Glide.with(context).load(modelFile.getImage()).diskCacheStrategy(DiskCacheStrategy.SOURCE).into(holder.ivImageTemplate);
            holder.llBtn.setVisibility(View.VISIBLE);
        }


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (modelFile.getRedirectionActivity() != null) {
                    Intent i = new Intent(context, modelFile.getRedirectionActivity());
                    i.putExtra("isOnlyShow", true);
                    context.startActivity(i);
                }
            }
        });

        holder.iv_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onImgClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return templateList.size();
    }
}