package com.cab.digicafe.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.cab.digicafe.Activities.ChageCircle;
import com.cab.digicafe.Callback.OnLoadMoreListener;
import com.cab.digicafe.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by CSS on 02-11-2017.
 */

public class CircleListAdapter extends RecyclerView.Adapter<CircleListAdapter.MyViewHolder> {
    private ArrayList<ChageCircle.industrySuppliermodel> catererList;

    private Context mContext;
    private boolean onBind;
    private OnLoadMoreListener mOnLoadMoreListener;

    public interface OnItemClickListener {
        void onItemClick(ChageCircle.industrySuppliermodel item);

    }

    private OnItemClickListener listener;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_site)
        TextView tv_companyinfo;

        @BindView(R.id.ll_base)
        public LinearLayout ll_base;

        @BindView(R.id.rb)
        RadioButton rb;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

        }
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }

    public CircleListAdapter(ArrayList<ChageCircle.industrySuppliermodel> moviesList, Context context, OnItemClickListener listener) {
        this.catererList = moviesList;
        this.mContext = context;
        this.listener = listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_circlesearch, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        ChageCircle.industrySuppliermodel sp = catererList.get(position);

        String compinf = sp.getName();
        holder.tv_companyinfo.setText(compinf);

        holder.ll_base.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                int t = position;
                listener.onItemClick(catererList.get(t));

                if (!catererList.get(t).isIschecked()) {
                    for (int i = 0; i < catererList.size(); i++) {
                        catererList.get(i).setIschecked(false);
                    }
                    catererList.get(t).setIschecked(true);
                    notifyDataSetChanged();
                }

            }
        });


        holder.rb.setVisibility(View.VISIBLE);


        if (sp.isIschecked()) {
            holder.rb.setChecked(true);
        } else {
            holder.rb.setChecked(false);
        }


    }

    @Override
    public int getItemCount() {
        return catererList.size();
    }

    public void setitem(List<ChageCircle.industrySuppliermodel> products) {
        catererList = new ArrayList<>();
        catererList.addAll(products);
        Log.e("aliassearch", catererList.size() + "");
        notifyDataSetChanged();

    }


  /*  public void setFilter(List<CatalougeContent> countryModels) {
        catererList = new ArrayList<>();
        catererList.addAll(countryModels);
        notifyDataSetChanged();
    }*/


}
