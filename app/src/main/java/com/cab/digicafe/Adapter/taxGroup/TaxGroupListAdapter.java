package com.cab.digicafe.Adapter.taxGroup;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cab.digicafe.Activities.RequestApis.MyResponse.CategoryViewModel;
import com.cab.digicafe.Helper.Inad;
import com.cab.digicafe.Helper.SharedPrefUserDetail;
import com.cab.digicafe.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class TaxGroupListAdapter extends RecyclerView.Adapter<TaxGroupListAdapter.MyViewHolder> {


    ArrayList<TaxGroupModel> alTax;
    private Context mContext;

    public interface OnItemClickListener {
        void onItemClick(CategoryViewModel item);

        void onCheckChange(int pos, boolean isChecked);

        void onDrag(int firstPosition, int secondPosition);

    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvKey)
        TextView tvKey;

        @BindView(R.id.tvTaxVal)
        TextView tvTaxVal;


        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

        }
    }


    public TaxGroupListAdapter(ArrayList<TaxGroupModel> moviesList, Context context) {
        this.alTax = moviesList;
        this.mContext = context;
        currency = SharedPrefUserDetail.getString(context, SharedPrefUserDetail.chit_symbol_native, "") + " ";

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_ad_taxgroup, parent, false);

        return new MyViewHolder(itemView);
    }

    String currency = "";

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {


        TaxGroupModel taxGroupModel = alTax.get(position);
        holder.tvKey.setText(taxGroupModel.taxKey + " @ " + taxGroupModel.taxValPercentage + " %");
        holder.tvTaxVal.setText("+ " + currency + Inad.getCurrencyDecimal(taxGroupModel.taxVal, mContext));


    }

    @Override
    public int getItemCount() {
        return alTax.size();
    }


}
