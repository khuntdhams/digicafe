package com.cab.digicafe.Adapter;

import android.media.Image;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.cab.digicafe.Model.AddAddress;
import com.cab.digicafe.R;

import java.util.ArrayList;


public class AddressAdapter extends RecyclerView.Adapter<AddressAdapter.MyViewHolder> {

    private ArrayList<AddAddress> addressList;
    private OnItemClickListener listener;
    boolean isAdd = false;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txt_address_type, txt_address;
        ImageView ivEdit;
        CardView cdAddress;

        public MyViewHolder(View view) {
            super(view);
            txt_address_type = (TextView) view.findViewById(R.id.txt_address_type);
            txt_address = (TextView) view.findViewById(R.id.txt_address);
            ivEdit = (ImageView) view.findViewById(R.id.ivEdit);
            cdAddress = (CardView) view.findViewById(R.id.cdAddress);
        }
    }

    public AddressAdapter(ArrayList<AddAddress> addressList, OnItemClickListener listener, boolean isAdd) {
        this.addressList = addressList;
        this.listener = listener;
        this.isAdd = isAdd;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.address_list_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        AddAddress address = addressList.get(position);

        holder.txt_address_type.setText(address.getAddressType());
        holder.txt_address.setText(address.getAddress());

        holder.ivEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(position);
            }
        });
        if (isAdd) {
            holder.ivEdit.setVisibility(View.GONE);
        } else {
            holder.ivEdit.setVisibility(View.VISIBLE);
        }

        holder.cdAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isAdd) {
                    listener.onItemClick(position);
                }
            }
        });

    }

    public interface OnItemClickListener {
        void onItemClick(int pos);

    }


    @Override
    public int getItemCount() {
        return addressList.size();
    }
}