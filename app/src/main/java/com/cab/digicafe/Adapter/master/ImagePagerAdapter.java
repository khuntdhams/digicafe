package com.cab.digicafe.Adapter.master;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cab.digicafe.Activities.ImagePreviewActivity;
import com.cab.digicafe.Activities.player.PlayerActivity;
import com.cab.digicafe.Helper.LoadImg;
import com.cab.digicafe.Model.ModelFile;
import com.cab.digicafe.Model.ShareModel;
import com.cab.digicafe.R;
import com.google.gson.Gson;

import java.util.ArrayList;

public class ImagePagerAdapter extends PagerAdapter {

    Context mContext;
    LayoutInflater mLayoutInflater;
    ArrayList<String> al_img;

    String title;

    public ImagePagerAdapter(Context context, ArrayList<String> al_img, String title) {
        this.al_img = al_img;
        mContext = context;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.title = title;
    }

    @Override
    public int getCount() {
        return al_img.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(final ViewGroup container, final int position) {
        View itemView = mLayoutInflater.inflate(R.layout.search_pager_item, container, false);
        ImageView imageView = (ImageView) itemView.findViewById(R.id.imageView);
        final TextView tvTmp = (TextView) itemView.findViewById(R.id.tvTmp);
        ImageView iv_play = (ImageView) itemView.findViewById(R.id.iv_play);

        tvTmp.setText(title);

        imageView.setImageDrawable(null);

        iv_play.setVisibility(View.GONE);
        if (al_img == null) al_img = new ArrayList<>();
        if (al_img.get(position) == null) al_img.set(position, "");
        if (al_img.get(position).contains("youtube")) {
            iv_play.setVisibility(View.VISIBLE);
            Uri uri = Uri.parse(al_img.get(position));
            //Set<String> args = uri.getQueryParameterNames();
            String v = uri.getQueryParameter("v");
            iv_play.setVisibility(View.VISIBLE);
            String url = "" + mContext.getString(R.string.utube_base) + v + mContext.getString(R.string.utube_end);
            Log.e("okhttp utube", url + "");

            loadImg.loadCategoryImg(mContext, url, imageView);
        } else {
            loadImg.loadCategoryImg(mContext, al_img.get(position), imageView);
        }

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (tvTmp.getText().toString().isEmpty()) {
                    return;
                }

                ShareModel.getI().isImgPrev = true;

                if (al_img.get(position).contains("youtube")) {

                    Uri uri = Uri.parse(al_img.get(position));
                    String v = uri.getQueryParameter("v");
                    ShareModel.getI().utubeId = v;
                    mContext.startActivity(new Intent(mContext, PlayerActivity.class));

                } else {

                    ArrayList<ModelFile> al_model = new ArrayList<>();
                    try {

                        for (int i = 0; i < al_img.size(); i++) {
                            al_model.add(new ModelFile(al_img.get(i), false));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    String imgarray = new Gson().toJson(al_model);

                    Intent intent = new Intent(mContext, ImagePreviewActivity.class);
                    intent.putExtra("imgarray", imgarray);
                    intent.putExtra("pos", position);
                    intent.putExtra("img_title", tvTmp.getText().toString());
                    mContext.startActivity(intent);

                }
            }
        });

        container.addView(itemView);
        return itemView;
    }

    LoadImg loadImg = new LoadImg();


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }
}