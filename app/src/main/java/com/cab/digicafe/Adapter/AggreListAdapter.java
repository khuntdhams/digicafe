package com.cab.digicafe.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cab.digicafe.Callback.OnLoadMoreListener;
import com.cab.digicafe.Model.ServiceProvider;
import com.cab.digicafe.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by CSS on 02-11-2017.
 */

public class AggreListAdapter extends RecyclerView.Adapter<AggreListAdapter.MyViewHolder> {
    private ArrayList<ServiceProvider> catererList;

    private Context mContext;
    private boolean onBind;
    private OnLoadMoreListener mOnLoadMoreListener;

    public interface OnItemClickListener {
        void onItemClick(ServiceProvider item);

    }

    private OnItemClickListener listener;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_companyinfo)
        TextView tv_companyinfo;

        @BindView(R.id.iv_img)
        public ImageView iv_img;

        @BindView(R.id.rl_img)
        public RelativeLayout rl_img;

        @BindView(R.id.ll_base)
        public LinearLayout ll_base;


        @BindView(R.id.rb)
        RadioButton rb;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

        }
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }

    public AggreListAdapter(ArrayList<ServiceProvider> moviesList, Context context, OnItemClickListener listener) {
        this.catererList = moviesList;
        this.mContext = context;
        this.listener = listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_aggregator, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        ServiceProvider sp = catererList.get(position);

        String compinf = sp.getCompany_name() + "\n" + sp.getLocation();
        holder.tv_companyinfo.setText(compinf);

        holder.ll_base.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                int t = position;
                listener.onItemClick(catererList.get(t));

                if (!catererList.get(t).isIschecked()) {
                    catererList.get(t).setIschecked(true);

                    for (int i = 0; i < catererList.size(); i++) {
                        if (i != t) {
                            catererList.get(i).setIschecked(false);
                        }
                    }
                    notifyDataSetChanged();
                }

            }
        });


        if (sp.isIschecked()) {
            holder.rb.setChecked(true);
        } else {
            holder.rb.setChecked(false);
        }


        try {
            String endurl = "assets/uploadimages/";

            String company_image = sp.getCompany_image();
            String url = mContext.getString(R.string.BASEURL) + endurl + company_image;

            if (company_image != null && !company_image.equals("")) {
                Picasso.with(mContext)
                        .load(url)
                        .into(holder.iv_img);
                holder.rl_img.setVisibility(View.VISIBLE);
            } else {
                holder.rl_img.setVisibility(View.GONE);
            }

        } catch (Exception e) {

            holder.rl_img.setVisibility(View.GONE);
        }


    }

    @Override
    public int getItemCount() {
        return catererList.size();
    }

    public void setitem(List<ServiceProvider> products) {
        catererList = new ArrayList<>();
        catererList.addAll(products);
        Log.e("aliassearch", catererList.size() + "");
        notifyDataSetChanged();

    }


  /*  public void setFilter(List<CatalougeContent> countryModels) {
        catererList = new ArrayList<>();
        catererList.addAll(countryModels);
        notifyDataSetChanged();
    }*/


}
