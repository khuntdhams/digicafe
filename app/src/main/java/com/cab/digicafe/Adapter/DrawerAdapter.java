package com.cab.digicafe.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cab.digicafe.Model.DrawerItem;
import com.cab.digicafe.R;

import java.util.ArrayList;

/**
 * Created by CSS on 01-12-2017.
 */

public class DrawerAdapter extends RecyclerView.Adapter<DrawerAdapter.DrawerViewHolder> {
    private ArrayList<DrawerItem> drawerMenuList;
    boolean isemployee = false;

    public interface OnItemClickListener {
        void onItemClick(DrawerItem item);
    }

    private DrawerAdapter.OnItemClickListener listener;

    public DrawerAdapter(ArrayList<DrawerItem> drawerMenuList, DrawerAdapter.OnItemClickListener listener, boolean isemployee) {
        this.drawerMenuList = drawerMenuList;
        this.listener = listener;
        this.isemployee = isemployee;
    }

    @Override
    public DrawerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.drawer, parent, false);
        return new DrawerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DrawerViewHolder holder, int position) {

        DrawerItem drawerItem = drawerMenuList.get(position);
        holder.ivSub.setVisibility(View.GONE);
        holder.ivSub.setRotation(270);
        if (drawerItem.getTitle().equalsIgnoreCase("master")) {
            if (drawerMenuList.get(position + 1).mstrDtl.equals("show")) {
                holder.ivSub.setRotation(0);
            }
            holder.ivSub.setVisibility(View.VISIBLE);
        }

        if (drawerItem.getTitle().equalsIgnoreCase("other")) {
            if (drawerMenuList.get(position + 1).otrDtl.equals("show")) {
                holder.ivSub.setRotation(0);
            }
            holder.ivSub.setVisibility(View.VISIBLE);
        }
        if (drawerItem.getTitle().equalsIgnoreCase("product")) {
            if (drawerMenuList.get(position + 1).proDtl.equals("show")) {
                holder.ivSub.setRotation(0);
            }
            holder.ivSub.setVisibility(View.VISIBLE);
        }

        holder.icon.setVisibility(View.INVISIBLE);

        holder.tvDashline.setVisibility(View.GONE);

        if (drawerItem.otrDtl.equals("show")) {
            holder.tvDashline.setVisibility(View.VISIBLE);
            holder.llBs.setVisibility(View.VISIBLE);

        } else if (drawerItem.otrDtl.equals("hide")) {
            holder.llBs.setVisibility(View.GONE);
        } else if (drawerItem.mstrDtl.equals("show")) {
            holder.tvDashline.setVisibility(View.VISIBLE);
            holder.llBs.setVisibility(View.VISIBLE);
        } else if (drawerItem.mstrDtl.equals("hide")) {
            holder.llBs.setVisibility(View.GONE);
        }  else if (drawerItem.proDtl.equals("show")) {
            holder.tvDashline.setVisibility(View.VISIBLE);
            holder.llBs.setVisibility(View.VISIBLE);
        } else if (drawerItem.proDtl.equals("hide")) {
            holder.llBs.setVisibility(View.GONE);
        }else {
            holder.icon.setVisibility(View.VISIBLE);
            holder.llBs.setVisibility(View.VISIBLE);
            //holder.icon.setImageResource(drawerMenuList.get(position).getIcon());
            holder.icon.setImageResource(R.mipmap.ic_circle);
        }


        String title = drawerMenuList.get(position).getTitle();
        if (title.equalsIgnoreCase("Cancelled Task") || title.equalsIgnoreCase("Receipts") || title.equalsIgnoreCase("My Order")) {

            holder.tv_line.setVisibility(View.VISIBLE);
        } else {
            holder.tv_line.setVisibility(View.GONE);
        }
        holder.title.setText(title);

        holder.bind(drawerMenuList.get(position), listener);
    }

    @Override
    public int getItemCount() {
        return drawerMenuList.size();
    }

    class DrawerViewHolder extends RecyclerView.ViewHolder {
        TextView title, tv_line, tvDashline;
        ImageView icon, ivSub;
        LinearLayout llBs;

        public DrawerViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
            tv_line = (TextView) itemView.findViewById(R.id.tv_line);
            tvDashline = (TextView) itemView.findViewById(R.id.tvDashline);
            icon = (ImageView) itemView.findViewById(R.id.icon);
            ivSub = (ImageView) itemView.findViewById(R.id.ivSub);
            llBs = (LinearLayout) itemView.findViewById(R.id.llBs);


        }

        public void bind(final DrawerItem item, final OnItemClickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (item.getTitle().equalsIgnoreCase("master")) {
                        for (int i = 0; i < drawerMenuList.size(); i++) {
                            if (drawerMenuList.get(i).mstrDtl.equals("show")) {
                                drawerMenuList.get(i).mstrDtl = "hide";
                            } else if (drawerMenuList.get(i).mstrDtl.equals("hide")) {
                                drawerMenuList.get(i).mstrDtl = "show";
                            }
                        }
                        notifyDataSetChanged();
                    } else if (item.getTitle().equalsIgnoreCase("other")) {
                        for (int i = 0; i < drawerMenuList.size(); i++) {
                            if (drawerMenuList.get(i).otrDtl.equals("show")) {
                                drawerMenuList.get(i).otrDtl = "hide";
                            } else if (drawerMenuList.get(i).otrDtl.equals("hide")) {
                                drawerMenuList.get(i).otrDtl = "show";
                            }
                        }
                        notifyDataSetChanged();
                    } else if (item.getTitle().equalsIgnoreCase("product")) {
                        for (int i = 0; i < drawerMenuList.size(); i++) {
                            if (drawerMenuList.get(i).proDtl.equals("show")) {
                                drawerMenuList.get(i).proDtl = "hide";
                            } else if (drawerMenuList.get(i).proDtl.equals("hide")) {
                                drawerMenuList.get(i).proDtl = "show";
                            }
                        }
                        notifyDataSetChanged();
                    } else {
                        listener.onItemClick(item);
                    }

                }
            });
        }
    }
}