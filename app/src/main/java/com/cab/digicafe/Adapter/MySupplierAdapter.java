package com.cab.digicafe.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cab.digicafe.Activities.ImagePreviewActivity;
import com.cab.digicafe.Activities.RequestApis.MyRequestCall;
import com.cab.digicafe.Activities.RequestApis.MyRequst;
import com.cab.digicafe.BaseActivity;
import com.cab.digicafe.Helper.Constants;
import com.cab.digicafe.Helper.Inad;
import com.cab.digicafe.Helper.LoadImg;
import com.cab.digicafe.Helper.SessionManager;
import com.cab.digicafe.Helper.SharedPrefUserDetail;
import com.cab.digicafe.HistoryActivity;
import com.cab.digicafe.Model.Metal;
import com.cab.digicafe.Model.ModelFile;
import com.cab.digicafe.Model.ShareModel;
import com.cab.digicafe.Model.Supplier;
import com.cab.digicafe.Model.SupplierNew;
import com.cab.digicafe.MyCustomClass.JsonObjParse;
import com.cab.digicafe.R;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.relex.circleindicator.CircleIndicator;

import static com.cab.digicafe.Activities.MySupplierActivity.isAppHyperlocalMode;
import static com.cab.digicafe.Activities.MySupplierActivity.isFavourite;

/**
 * Created by CSS on 02-11-2017.
 */

public class MySupplierAdapter extends RecyclerView.Adapter<MySupplierAdapter.MyViewHolder> {
    private List<SupplierNew> catererList;
    private Context mContext;
    private SessionManager sessionManager;

    public interface OnItemClickListener {
        void onItemClick(SupplierNew item, int pos);

        void onShopClick(SupplierNew item, int pos);

        void onItemLongClick(SupplierNew item, int pos);

        void onDisplyLongDesc(SupplierNew item);

        void onDisplyInfo(SupplierNew item);

        void onDisplayRefer(SupplierNew item);
    }

    private OnItemClickListener listener;
    String baseurl = "";


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name, email, mobile, tv_storestatus, tvAlpha;
        public ImageView caterrerprofile, iv_del, iv_update, ivShare, ivLoc, ivShopWeb, ivofr, ivCall, ivRefer;
        RelativeLayout rl_lock, rlAlpha;
        LinearLayout llBtn;
        TextView tvMyOrder;

        @BindView(R.id.pager)
        ViewPager pager;

        @BindView(R.id.indicator)
        CircleIndicator indicator;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            name = (TextView) view.findViewById(R.id.name);
            tvAlpha = (TextView) view.findViewById(R.id.tvAlpha);
            email = (TextView) view.findViewById(R.id.email);
            mobile = (TextView) view.findViewById(R.id.mobile);
            tv_storestatus = (TextView) view.findViewById(R.id.tv_storestatus);

            caterrerprofile = (ImageView) view.findViewById(R.id.catererprofile);
            ivShare = (ImageView) view.findViewById(R.id.ivShare);
            ivofr = (ImageView) view.findViewById(R.id.ivofr);
            iv_update = (ImageView) view.findViewById(R.id.iv_update);
            ivLoc = (ImageView) view.findViewById(R.id.ivLoc);
            iv_del = (ImageView) view.findViewById(R.id.iv_del);
            ivShopWeb = (ImageView) view.findViewById(R.id.ivShopWeb);
            ivCall = (ImageView) view.findViewById(R.id.ivCall);
            ivRefer = (ImageView) view.findViewById(R.id.ivRefer);
            rl_lock = (RelativeLayout) view.findViewById(R.id.rl_lock);
            rlAlpha = (RelativeLayout) view.findViewById(R.id.rlAlpha);

            llBtn = (LinearLayout) view.findViewById(R.id.llBtn);
            tvMyOrder = (TextView) view.findViewById(R.id.tvMyOrder);
        }

        public void bind(final Supplier item, final OnItemClickListener listener) {
        }
    }

    public void setFilter(List<SupplierNew> countryModels) {
        catererList = new ArrayList<>();
        catererList.addAll(countryModels);
        notifyDataSetChanged();
    }

    public MySupplierAdapter(List<SupplierNew> moviesList, OnItemClickListener listener, Context context) {
        this.catererList = moviesList;
        this.listener = listener;
        this.mContext = context;
        baseurl = mContext.getString(R.string.BASEURL);
        sessionManager = new SessionManager(mContext);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (sessionManager.getIsConsumer() && !isAppHyperlocalMode) {
            if (isFavourite) {
                View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.caterer_list_row, parent, false);

                return new MyViewHolder(itemView);
            } else {
                View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.caterer_list_row_copy, parent, false);
                return new MyViewHolder(itemView);
            }
        } else {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.caterer_list_row, parent, false);

            return new MyViewHolder(itemView);
        }
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        SupplierNew movie = catererList.get(position);

        holder.rlAlpha.setVisibility(View.GONE);
        if (movie.getAccountType() != null && movie.getBusiness_type() != null &&
                movie.getAccountType().equalsIgnoreCase("Business") &&
                movie.getBusiness_type().equalsIgnoreCase("Aggregator")) {

            holder.name.setText(movie.getFirstname());
            holder.tvAlpha.setText("A");
            holder.rlAlpha.setVisibility(View.VISIBLE);

        } else if (movie.getAccountType() != null && movie.getBusiness_type() != null &&
                movie.getAccountType().equalsIgnoreCase("Business") &&
                movie.getBusiness_type().equalsIgnoreCase("circle")) {

            holder.name.setText(movie.getFirstname());
            holder.tvAlpha.setText("C");
            holder.rlAlpha.setVisibility(View.VISIBLE);

        } else if (movie.getAccountType() != null && movie.getBusiness_type() != null &&
                movie.getAccountType().equalsIgnoreCase("Business") &&
                movie.getBusiness_type().equalsIgnoreCase("business")) {

            holder.name.setText(movie.getFirstname());
            holder.tvAlpha.setText("B");
            holder.rlAlpha.setVisibility(View.VISIBLE);

        } else {
            holder.name.setText(movie.getFirstname());
            holder.tvAlpha.setText("");
        }

        holder.rlAlpha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onDisplyLongDesc(catererList.get(position));
            }
        });

        holder.email.setText(movie.getEmailId());
        holder.mobile.setText(movie.getContactNo());

        if (movie.isIslongpress()) {
            holder.iv_del.setVisibility(View.VISIBLE);
        } else {
            holder.iv_del.setVisibility(View.GONE);
        }
        //holder.bind(movie, listener);

      /*  holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // Log.e("item clieced", "" + item.getAlias_name());

            }
        });*/

        holder.iv_update.setVisibility(View.VISIBLE);

        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                int pos = position;
                listener.onItemLongClick(catererList.get(pos), pos);
                return true;
            }
        });

        if (sessionManager.getIsConsumer() && !isAppHyperlocalMode) {
            holder.llBtn.setVisibility(View.VISIBLE);
            holder.tvMyOrder.setVisibility(View.VISIBLE);
        } else {
            holder.llBtn.setVisibility(View.VISIBLE);
            holder.tvMyOrder.setVisibility(View.GONE);
        }
        holder.ivShare.setVisibility(View.VISIBLE);
        SessionManager sm = new SessionManager();
        if (sm.getcurrentu_nm().matches("[0-9.]*")) {  // consumer
            // holder.ivShare.setVisibility(View.GONE);
        }

        holder.tvMyOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ShareModel.getI().dbUnderMaster = "";

                ShareModel.getI().metal = new Metal();
                Constants.chit_id_from_mrp = 0;

                sessionManager.setAggregator_ID(mContext.getString(R.string.Aggregator));
                SharedPrefUserDetail.setBoolean(mContext, SharedPrefUserDetail.isfromsupplier, false);
                SharedPrefUserDetail.setString(mContext, SharedPrefUserDetail.isfrommyaggregator, "");
                SharedPrefUserDetail.setBoolean(mContext, SharedPrefUserDetail.isMrp, false); // pass flag

                Intent i = new Intent(mContext, HistoryActivity.class);
                i.putExtra("session", sessionManager.getsession());
                //    i.putExtra("fromAT", true);
                i.putExtra("fromHome", true);
                i.putExtra("isBk", true);
                mContext.startActivity(i);
            }
        });

        holder.ivShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String bridge_id = catererList.get(position).getContact_bridge_id();
                String username = catererList.get(position).getUsername();
                String shopNm = catererList.get(position).getFirstname();

                String shareUrl = "http://play.google.com/store/apps/details?id=" + mContext.getString(R.string.consumer_link) + "&referrer=" + username + "-" + new SessionManager().getUserWithAggregator();

                //shareUrl = shareUrl + "\n\nAdd '"+username+"' in My Supplier option to connect to '"+shopNm +"'\n\n";

                String extTxt = shareUrl;

                Uri shareUri = ((BaseActivity) mContext).buildDeepLink(Uri.parse(shareUrl), 0);
                extTxt = shareUri.toString();

                Intent share = new Intent(android.content.Intent.ACTION_SEND);
                share.setType("text/plain");
                share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);

                // Add data to the intent, the receiving app will decide
                // what to do with it.

                share.putExtra(Intent.EXTRA_SUBJECT, mContext.getString(R.string.consumer));
                share.putExtra(Intent.EXTRA_TEXT, extTxt);
                mContext.startActivity(Intent.createChooser(share, "Share"));

              /*  Intent share = new Intent(android.content.Intent.ACTION_SEND);
                share.setType("text/plain");
                share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);

                // Add data to the intent, the receiving app will decide
                // what to do with it.
                share.putExtra(Intent.EXTRA_SUBJECT, mContext.getString(R.string.app_name));
                share.putExtra(Intent.EXTRA_TEXT, shareUrl);
                mContext.startActivity(Intent.createChooser(share, "Share"));*/
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int pos = position;
                listener.onShopClick(catererList.get(pos), pos);
            }
        });

        holder.iv_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //int pos = position;
                //listener.onItemClick(catererList.get(pos),pos);
                String company_images_field = JsonObjParse.getValueEmpty(catererList.get(position).getField_json_data(), "company_images_field");
                TypeToken<ArrayList<ModelFile>> token = new TypeToken<ArrayList<ModelFile>>() {
                };
                ArrayList<ModelFile> al_selet = new Gson().fromJson(company_images_field, token.getType());
                if (al_selet == null) al_selet = new ArrayList<>();

                String imgarray = new Gson().toJson(al_selet);

                Intent intent = new Intent(mContext, ImagePreviewActivity.class);
                intent.putExtra("imgarray", imgarray);
                intent.putExtra("pos", 0);
                intent.putExtra("img_title", catererList.get(position).getFirstname());
                mContext.startActivity(intent);
            }
        });

        if (movie.getBusinessStatus().equalsIgnoreCase("open")) {
            holder.tv_storestatus.setVisibility(View.GONE);
            holder.rl_lock.setVisibility(View.GONE);

        } else if (movie.getBusinessStatus().equalsIgnoreCase("closed")) {
            holder.tv_storestatus.setText("Closed");
            holder.tv_storestatus.setVisibility(View.GONE);
            holder.rl_lock.setVisibility(View.VISIBLE);
        }

        //holder.caterrerprofile.setImageDrawable(null);

        if (TextUtils.isEmpty(movie.getCompanyImage())) {

           /* holder.name.setTextColor(Color.parseColor("#FFFF00"));
            holder.email.setTextColor(Color.parseColor("#FFFF00"));
            holder.mobile.setTextColor(Color.parseColor("#FFFF00"));
            holder.tv_storestatus.setTextColor(Color.parseColor("#FFFF00"));*/

        } else {
            String endurl = "assets/uploadimages/";
            String url = baseurl + endurl + movie.getCompanyImage();

            holder.name.setTextColor(Color.parseColor("#FFFFFF"));
            holder.email.setTextColor(Color.parseColor("#FFFFFF"));
            holder.mobile.setTextColor(Color.parseColor("#FFFFFF"));
            holder.tv_storestatus.setTextColor(Color.parseColor("#FFFFFF"));

            //imagePagerAdapter = new ImagePagerAdapter(mContext, al_pager);
            //holder.pager.setAdapter(imagePagerAdapter);
            //holder.indicator.setViewPager(holder.pager);
           /* Picasso.with(mContext)
                    .load(url)
                    .fit()                        // optional
                    .into(holder.caterrerprofile);*/
        }

        String company_images_field = JsonObjParse.getValueEmpty(movie.getField_json_data(), "company_images_field");
        TypeToken<ArrayList<ModelFile>> token = new TypeToken<ArrayList<ModelFile>>() {
        };
        ArrayList<ModelFile> al_selet = new Gson().fromJson(company_images_field, token.getType());
        if (al_selet == null) al_selet = new ArrayList<>();
        holder.iv_update.setVisibility(View.GONE);

        loadImg.loadCategoryImg(mContext, "", holder.caterrerprofile);

        if (al_selet.size() > 0) {
            holder.iv_update.setVisibility(View.VISIBLE);
            loadImg.loadCategoryImg(mContext, al_selet.get(0).getImgpath(), holder.caterrerprofile);
        }

        holder.ivLoc.setVisibility(View.GONE);
        if (movie.getLatitude() != null && !movie.getLatitude().isEmpty()) {
            holder.ivLoc.setVisibility(View.VISIBLE);
        }

        holder.ivLoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //https://developers.google.com/maps/documentation/urls/android-intents
                try {
                    double lati = Double.parseDouble(catererList.get(position).getLatitude());
                    double longi = Double.parseDouble(catererList.get(position).getLongitude());
                    String q = catererList.get(position).getLocation();
                    String uri = String.format(Locale.ENGLISH, "geo:%f,%f", lati, longi);
                    uri = uri + "?q=" + q;
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                    intent.setPackage("com.google.android.apps.maps");
                    mContext.startActivity(intent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        holder.ivShopWeb.setVisibility(View.GONE);
        String web_link_of_shop = JsonObjParse.getValueEmpty(movie.getField_json_data(), "web_link_of_shop");
        if (web_link_of_shop.length() > 0) holder.ivShopWeb.setVisibility(View.VISIBLE);
        holder.ivShopWeb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //https://developers.google.com/maps/documentation/urls/android-intents
                String web_link_of_shop = JsonObjParse.getValueEmpty(catererList.get(position).getField_json_data(), "web_link_of_shop");

                try {

                    Intent i = new Intent(Intent.ACTION_VIEW);
                    if (!web_link_of_shop.startsWith("http://") && !web_link_of_shop.startsWith("https://")) {
                        web_link_of_shop = "http://" + web_link_of_shop;
                    }

                    i.setData(Uri.parse(web_link_of_shop));

                    mContext.startActivity(i);

                } catch (Exception e) {
                    e.printStackTrace();
                    Inad.alerterInfo("Message", "Can not load " + web_link_of_shop, (Activity) mContext);
                }
            }
        });

        holder.ivofr.setVisibility(View.GONE);
        String disc_info = JsonObjParse.getValueEmpty(movie.getField_json_data(), "disc_info");
        if (disc_info.length() > 2) holder.ivofr.setVisibility(View.VISIBLE);
        holder.ivofr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //https://developers.google.com/maps/documentation/urls/android-intents
                try {
                    listener.onDisplyInfo(catererList.get(position));

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        holder.mobile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Inad.callIntent(holder.mobile, mContext);
            }
        });

        holder.ivCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Inad.callIntent(holder.mobile, mContext);
            }
        });

        holder.ivRefer.setVisibility(View.VISIBLE);
        holder.ivRefer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final int i = position;
                if (catererList.get(i).getMyReferal().isEmpty()) {
                    MyRequst myRequst = new MyRequst();
                    String user = new SessionManager().getUserWithAggregator();
                    user = user.replace("@", "_");
                    user = user.replace(".", "_");

                    myRequst.userid_share = user;
                    myRequst.shop_id = catererList.get(i).getUsername();

                    myRequestCall.getMyReferFromSupplier(mContext, myRequst, new MyRequestCall.CallRequest() {
                        @Override
                        public void onGetResponse(String response) {
                            if (response.isEmpty()) {
                                catererList.get(i).setMyReferal("Refered list is empty.");
                            } else {
                                catererList.get(i).setMyReferal(response);
                            }
                            listener.onDisplayRefer(catererList.get(i));
                        }
                    });
                } else {
                    listener.onDisplayRefer(catererList.get(i));
                }
            }
        });

    /*    if (mContext.getString(R.string.app_name_condition).equalsIgnoreCase(mContext.getString(R.string.c1_omantex))) {
            holder.name.setVisibility(View.INVISIBLE);
            holder.mobile.setVisibility(View.INVISIBLE);
            holder.ivCall.setVisibility(View.INVISIBLE);
        }*/

        if (sessionManager.getIsConsumer() && !isAppHyperlocalMode) {
            holder.name.setVisibility(View.INVISIBLE);
            holder.mobile.setVisibility(View.INVISIBLE);
            holder.ivCall.setVisibility(View.INVISIBLE);
        }
    }

    LoadImg loadImg = new LoadImg();

    @Override
    public int getItemCount() {
        if (sessionManager.getIsConsumer() && !isAppHyperlocalMode) {
            if (isFavourite) {
                return catererList.size();
            } else {
                if (catererList.size() == 0) {
                    return 0;
                } else {
                    return 1;
                }
            }
        } else {
            return catererList.size();
        }

    }

    MyRequestCall myRequestCall = new MyRequestCall();
}