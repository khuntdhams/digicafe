package com.cab.digicafe.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.cab.digicafe.Callback.OnLoadMoreListener;
import com.cab.digicafe.Model.Employee;
import com.cab.digicafe.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by CSS on 02-11-2017.
 */

public class EmployessListAdapter extends RecyclerView.Adapter<EmployessListAdapter.MyViewHolder> {
    private ArrayList<Employee> catererList;

    private Context mContext;
    private boolean onBind;
    private OnLoadMoreListener mOnLoadMoreListener;

    public interface OnItemClickListener {
        void onItemClick(Employee item);

    }

    private OnItemClickListener listener;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_employee)
        TextView tv_employee;

        @BindView(R.id.ll_base)
        public LinearLayout ll_base;


        @BindView(R.id.rb)
        RadioButton rb;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

        }
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }

    public EmployessListAdapter(ArrayList<Employee> moviesList, Context context, OnItemClickListener listener) {
        this.catererList = moviesList;
        this.mContext = context;
        this.listener = listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_employee, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        Employee sp = catererList.get(position);

        holder.tv_employee.setText(sp.getEmployee_name());

        holder.ll_base.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                int t = position;
                listener.onItemClick(catererList.get(t));

                if (!catererList.get(t).isChecked()) {
                    catererList.get(t).setChecked(true);

                    for (int i = 0; i < catererList.size(); i++) {
                        if (i != t) {
                            catererList.get(i).setChecked(false);
                        }
                    }
                    notifyDataSetChanged();
                }

            }
        });


        if (sp.isChecked()) {
            holder.rb.setChecked(true);
        } else {
            holder.rb.setChecked(false);
        }


    }

    @Override
    public int getItemCount() {
        return catererList.size();
    }

    public void setitem(List<Employee> products) {
        catererList = new ArrayList<>();
        catererList.addAll(products);
        Log.e("aliassearch", catererList.size() + "");
        notifyDataSetChanged();

    }


  /*  public void setFilter(List<CatalougeContent> countryModels) {
        catererList = new ArrayList<>();
        catererList.addAll(countryModels);
        notifyDataSetChanged();
    }*/


}
