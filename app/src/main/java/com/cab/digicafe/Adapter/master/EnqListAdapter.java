package com.cab.digicafe.Adapter.master;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cab.digicafe.Callback.OnLoadMoreListener;
import com.cab.digicafe.Model.Chit;
import com.cab.digicafe.MyCustomClass.JsonObjParse;
import com.cab.digicafe.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by CSS on 02-11-2017.
 */

public class EnqListAdapter extends RecyclerView.Adapter<EnqListAdapter.MyViewHolder> {
    private ArrayList<Chit> catererList;

    private Context mContext;
    private boolean onBind;
    private OnLoadMoreListener mOnLoadMoreListener;

    public interface OnItemClickListener {
        void onItemClick(Chit item);

        void onTextSearch(String search);

        void onCheckChange(int pos, boolean isChecked);

        void onDrag();

    }

    private OnItemClickListener listener;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvConsumrNm)
        TextView tvConsumrNm;

        @BindView(R.id.tvConsNo)
        TextView tvConsNo;

        @BindView(R.id.tvEnqDate)
        TextView tvEnqDate;

        @BindView(R.id.tvCustAdd)
        TextView tvCustAdd;

        @BindView(R.id.tvReqBy)
        TextView tvReqBy;

        @BindView(R.id.tvAssignedto)
        TextView tvAssignedto;

        @BindView(R.id.tvChitNm)
        TextView tvChitNm;

        @BindView(R.id.tvRefenceNumber)
        TextView tvRefenceNumber;

        @BindView(R.id.llBs)
        LinearLayout llBs;

        @BindView(R.id.llAssignedTo)
        LinearLayout llAssignedTo;


        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }

    public EnqListAdapter(ArrayList<Chit> moviesList, Context context, OnItemClickListener listener) {
        this.catererList = moviesList;
        this.mContext = context;
        this.listener = listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_ad_enquirylist, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        final Chit chit = catererList.get(position);

        try {
            String purpose = chit.getPurpose();
            holder.tvAssignedto.setText("");
            holder.tvReqBy.setText("");
            holder.tvConsumrNm.setText("");

            holder.tvConsNo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onTextSearch(holder.tvConsNo.getText().toString() + "");
                }
            });

            holder.llAssignedTo.setVisibility(View.GONE);

            if (chit.getAssign_to_bridge_id() != null && chit.getAssign_to_bridge_id().length() > 0) {
                holder.llAssignedTo.setVisibility(View.VISIBLE);
                holder.tvAssignedto.setText(chit.getAssign_to_bridge_id());
            }


            JSONObject jsonObject = new JSONObject(chit.getFooter_note());

            String ConsumerInfo = JsonObjParse.getValueEmpty(chit.getFooter_note(), "ConsumerInfo");
            if (!ConsumerInfo.isEmpty()) {
                JSONArray jaConsumerInfo = new JSONArray(ConsumerInfo);
                if (jaConsumerInfo.length() > 0) {
                    JSONObject joConsInfo = jaConsumerInfo.getJSONObject(0);
                    holder.tvConsumrNm.setText(joConsInfo.getString("customer_nm"));
                    holder.tvReqBy.setText(joConsInfo.getString("customer_nm"));
                    holder.tvConsNo.setText(joConsInfo.getString("mobile"));
                }
            }

            //holder.tvChitNm.setText(chit.getChit_name());
            String property_name = JsonObjParse.getValueEmpty(chit.getFooter_note(), "property_name");
            holder.tvChitNm.setText(property_name);

            if (jsonObject.has("Cust_Nm")) {

                holder.tvEnqDate.setText(chit.getCreated_date());

                holder.tvConsumrNm.setText(jsonObject.getString("Cust_Nm"));

                if (jsonObject.getString("Cust_Nm").equals("")) {

                } else {

                }
            }

            String ADDRESS = JsonObjParse.getValueEmpty(chit.getFooter_note(), "ADDRESS");
            holder.tvCustAdd.setText(ADDRESS);

            //holder.tvSortBy.setText(sp.sortBy);

        } catch (Exception e) {
            e.printStackTrace();
        }

        holder.llBs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onItemClick(chit);
            }
        });

        holder.tvRefenceNumber.setText("");
        try {
            holder.tvRefenceNumber.setText(chit.getRef_id() + " / " + chit.getCase_id()); // Req no and otp
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public int getItemCount() {
        return catererList.size();
    }


    public void setitem(List<Chit> products) {

        catererList = new ArrayList<>();
        catererList.addAll(products);
        Log.e("aliassearch", catererList.size() + "");
        notifyDataSetChanged();

    }

    public void remove(int position) {
        catererList.remove(position);
        notifyItemRemoved(position);
    }

    public void swap(int firstPosition, int secondPosition) {

        Collections.swap(catererList, firstPosition, secondPosition);
        notifyItemMoved(firstPosition, secondPosition);
    }

    public void onSwap() {

        listener.onDrag();
    }


}
