package com.cab.digicafe.Adapter.settings;

import android.graphics.Canvas;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;


/**
 * Created by adammcneilly on 9/8/15.
 */
public class ShopDragTouchHelperIssue extends ItemTouchHelper.SimpleCallback {
    private ShopImgFileAdapter imgPrevAdapter;

    public ShopDragTouchHelperIssue(ShopImgFileAdapter imgPrevAdapter) {
        super(ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT, 0);
        this.imgPrevAdapter = imgPrevAdapter;
    }


    @Override
    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
        imgPrevAdapter.swap(viewHolder.getAdapterPosition(), target.getAdapterPosition());
        return true;
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
        imgPrevAdapter.remove(viewHolder.getAdapterPosition());
    }

    @Override
    public void onChildDraw(Canvas c, RecyclerView recyclerView,
                            RecyclerView.ViewHolder viewHolder, float dX, float dY,
                            int actionState, boolean isCurrentlyActive) {

        final View v = ((ShopImgFileAdapter.MyViewHolder) viewHolder).itemView;
        if (actionState == ItemTouchHelper.ACTION_STATE_DRAG) {
            ((ShopImgFileAdapter.MyViewHolder) viewHolder).tv_stroke.setVisibility(View.GONE);

        }

        if (!isCurrentlyActive) {
            ((ShopImgFileAdapter.MyViewHolder) viewHolder).tv_stroke.setVisibility(View.VISIBLE);
        }


        getDefaultUIUtil().onDraw(c, recyclerView, v, dX, dY, actionState, isCurrentlyActive);


    }


    @Override
    public void onChildDrawOver(Canvas c, RecyclerView recyclerView,
                                RecyclerView.ViewHolder viewHolder, float dX, float dY,
                                int actionState, boolean isCurrentlyActive) {
        final View v = ((ShopImgFileAdapter.MyViewHolder) viewHolder).itemView;
        if (actionState == ItemTouchHelper.ACTION_STATE_DRAG) {

            ((ShopImgFileAdapter.MyViewHolder) viewHolder).tv_stroke.setVisibility(View.GONE);

        }

        if (!isCurrentlyActive) {
            ((ShopImgFileAdapter.MyViewHolder) viewHolder).tv_stroke.setVisibility(View.VISIBLE);
        }


        getDefaultUIUtil().onDrawOver(c, recyclerView, v, dX, dY, actionState, isCurrentlyActive);


    }


    @Override
    public void clearView(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
        final View foregroundView = ((ShopImgFileAdapter.MyViewHolder) viewHolder).itemView;
        // ((ImgPrevAdapter.MyViewHolder) viewHolder).tv_stroke.setVisibility(View.GONE);
        getDefaultUIUtil().clearView(foregroundView);

        // super.clearView(recyclerView, viewHolder);

    }


}
