package com.cab.digicafe.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cab.digicafe.Activities.ImagePreviewActivity;
import com.cab.digicafe.Adapter.settings.SampleImgInCartAdapter;
import com.cab.digicafe.Callback.OnLoadMoreListener;
import com.cab.digicafe.Helper.Inad;
import com.cab.digicafe.Helper.SessionManager;
import com.cab.digicafe.Model.Catelog;
import com.cab.digicafe.Model.Chitproducts;
import com.cab.digicafe.Model.Dental;
import com.cab.digicafe.Model.ModelFile;
import com.cab.digicafe.MyCustomClass.JsonObjParse;
import com.cab.digicafe.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by CSS on 02-11-2017.
 */

public class ChititemAdapter extends RecyclerView.Adapter<ChititemAdapter.MyViewHolder> {
    private List<Chitproducts> productlist;
    private Context mContext;
    String currencysymbol;
    String roundofvalue;
    String currency = "";
    Dental dental = new Dental();
    private OnLoadMoreListener mOnLoadMoreListener;
    ImgPrevAdapter imgPrevAdapter;
    List<Dental> templateList = new ArrayList<>();
    StringBuilder abutment_detail, impression_detail, implant_detail;
    JSONObject joDelivery;

    public interface OnItemClickListener {
        void onItemClick(Catelog item);
    }

    private OnItemClickListener listener;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txt_serialno, txt_productname, txt_productqty, txt_productprice, tv_offer, tvCommentForSurvey;

        RecyclerView rvSamples;
        LinearLayout llDentalImage, llShade, llTray, llModel, llBite, llTemp, llFrontal, llFrontalSmile, llLeftLateral, llLeftLateralSmile, llRightLateral, llRightLateralSmile,
                llIntraFrontal, llIntraRightLateral, llIntraLeftLateral, llUpperOcclusal, llLeftOcclusal, llColour, llInstruction,
                llImpression, llInstructionName, llAbutment, llImpressionList, llImplant, llChief, llTreatment;
        TextView tvShade, tvPatientName, tvPatientAge, tvPatientSex, tvSelectedTeeth, tvPatientInstruction, tvPreference,
                tvImpression, tvAbutment, tvImpression_list, tvImplant, tvChief, tvTreatment, tvColour, tvInstruction;
        RecyclerView rv_selectfile_shade, rv_selectfile_tray, rv_selectfile_model, rv_selectfile_bite, rv_selectfile_temp, rv_selectfile_frontal,
                rv_selectfile_frontalSmile, rv_selectfile_leftLateral, rv_selectfile_leftLateralSmile, rv_selectfile_rightLateral, rv_selectfile_rightLateralSmile,
                rv_selectfile_intraFrontal, rv_selectfile_intraRightLateral, rv_selectfile_intraLeftLateral, rv_selectfile_upperOcclusal, rv_selectfile_leftOcclusal,
                rv_selectfile_colour, rv_selectfile_instruction;

        LinearLayout llAddDetail;
        TextView Addserialno, Addproductqty, Addproductprice;
        LinearLayout llPurpose;
        TextView tvFromName, tvFromAddress, tvToName, tvToAddress, tvFromContact, tvToContact, tvDistance, tvDeliveryDate, tvRefId, tvReference;

        public MyViewHolder(View view) {
            super(view);
            txt_serialno = (TextView) view.findViewById(R.id.serialno);
            tvCommentForSurvey = (TextView) view.findViewById(R.id.tvCommentForSurvey);
            txt_productname = (TextView) view.findViewById(R.id.productname);
            txt_productqty = (TextView) view.findViewById(R.id.productqty);
            txt_productprice = (TextView) view.findViewById(R.id.productprice);
            tv_offer = (TextView) view.findViewById(R.id.tv_offer);
            rvSamples = (RecyclerView) view.findViewById(R.id.rvSamples);

            llDentalImage = (LinearLayout) view.findViewById(R.id.llDentalImage);
            llShade = (LinearLayout) view.findViewById(R.id.llShade);
            llTray = (LinearLayout) view.findViewById(R.id.llTray);
            llModel = (LinearLayout) view.findViewById(R.id.llModel);
            llBite = (LinearLayout) view.findViewById(R.id.llBite);
            llTemp = (LinearLayout) view.findViewById(R.id.llTemp);
            llFrontal = (LinearLayout) view.findViewById(R.id.llFrontal);
            llFrontalSmile = (LinearLayout) view.findViewById(R.id.llFrontalSmile);
            llLeftLateral = (LinearLayout) view.findViewById(R.id.llLeftLateral);
            llLeftLateralSmile = (LinearLayout) view.findViewById(R.id.llLeftLateralSmile);
            llRightLateral = (LinearLayout) view.findViewById(R.id.llRightLateral);
            llRightLateralSmile = (LinearLayout) view.findViewById(R.id.llRightLateralSmile);
            llIntraFrontal = (LinearLayout) view.findViewById(R.id.llIntraFrontal);
            llIntraRightLateral = (LinearLayout) view.findViewById(R.id.llIntraRightLateral);
            llIntraLeftLateral = (LinearLayout) view.findViewById(R.id.llIntraLeftLateral);
            llUpperOcclusal = (LinearLayout) view.findViewById(R.id.llUpperOcclusal);
            llLeftOcclusal = (LinearLayout) view.findViewById(R.id.llLeftOcclusal);
            llColour = (LinearLayout) view.findViewById(R.id.llColour);
            llInstruction = (LinearLayout) view.findViewById(R.id.llInstruction);
            llImpression = (LinearLayout) view.findViewById(R.id.llImpression);
            llInstructionName = (LinearLayout) view.findViewById(R.id.llInstructionName);
            llAbutment = (LinearLayout) view.findViewById(R.id.llAbutment);
            llImpressionList = (LinearLayout) view.findViewById(R.id.llImpressionList);
            llImplant = (LinearLayout) view.findViewById(R.id.llImplant);
            llChief = (LinearLayout) view.findViewById(R.id.llChief);
            llTreatment = (LinearLayout) view.findViewById(R.id.llTreatment);

            tvShade = (TextView) view.findViewById(R.id.tvShade);
            tvPatientName = (TextView) view.findViewById(R.id.tvPatientName);
            tvPatientAge = (TextView) view.findViewById(R.id.tvPatientAge);
            tvPatientSex = (TextView) view.findViewById(R.id.tvPatientSex);
            tvSelectedTeeth = (TextView) view.findViewById(R.id.tvSelectedTeeth);
            tvPatientInstruction = (TextView) view.findViewById(R.id.tvPatientInstruction);
            tvPreference = (TextView) view.findViewById(R.id.tvPreference);
            tvImpression = (TextView) view.findViewById(R.id.tvImpression);
            tvAbutment = (TextView) view.findViewById(R.id.tvAbutment);
            tvImpression_list = (TextView) view.findViewById(R.id.tvImpression_list);
            tvImplant = (TextView) view.findViewById(R.id.tvImplant);
            tvChief = (TextView) view.findViewById(R.id.tvChief);
            tvTreatment = (TextView) view.findViewById(R.id.tvTreatment);
            tvColour = (TextView) view.findViewById(R.id.tvColour);
            tvInstruction = (TextView) view.findViewById(R.id.tvInstruction);

            rv_selectfile_shade = (RecyclerView) view.findViewById(R.id.rv_selectfile_shade);
            rv_selectfile_tray = (RecyclerView) view.findViewById(R.id.rv_selectfile_tray);
            rv_selectfile_model = (RecyclerView) view.findViewById(R.id.rv_selectfile_model);
            rv_selectfile_bite = (RecyclerView) view.findViewById(R.id.rv_selectfile_bite);
            rv_selectfile_temp = (RecyclerView) view.findViewById(R.id.rv_selectfile_temp);
            rv_selectfile_frontal = (RecyclerView) view.findViewById(R.id.rv_selectfile_frontal);
            rv_selectfile_frontalSmile = (RecyclerView) view.findViewById(R.id.rv_selectfile_frontalSmile);
            rv_selectfile_leftLateral = (RecyclerView) view.findViewById(R.id.rv_selectfile_leftLateral);
            rv_selectfile_leftLateralSmile = (RecyclerView) view.findViewById(R.id.rv_selectfile_leftLateralSmile);
            rv_selectfile_rightLateral = (RecyclerView) view.findViewById(R.id.rv_selectfile_rightLateral);
            rv_selectfile_rightLateralSmile = (RecyclerView) view.findViewById(R.id.rv_selectfile_rightLateralSmile);
            rv_selectfile_intraFrontal = (RecyclerView) view.findViewById(R.id.rv_selectfile_intraFrontal);
            rv_selectfile_intraRightLateral = (RecyclerView) view.findViewById(R.id.rv_selectfile_intraRightLateral);
            rv_selectfile_intraLeftLateral = (RecyclerView) view.findViewById(R.id.rv_selectfile_intraLeftLateral);
            rv_selectfile_upperOcclusal = (RecyclerView) view.findViewById(R.id.rv_selectfile_upperOcclusal);
            rv_selectfile_leftOcclusal = (RecyclerView) view.findViewById(R.id.rv_selectfile_leftOcclusal);
            rv_selectfile_colour = (RecyclerView) view.findViewById(R.id.rv_selectfile_colour);
            rv_selectfile_instruction = (RecyclerView) view.findViewById(R.id.rv_selectfile_instruction);

            llAddDetail = (LinearLayout) view.findViewById(R.id.llAddDetail);
            Addserialno = (TextView) view.findViewById(R.id.Addserialno);
            Addproductqty = (TextView) view.findViewById(R.id.Addproductqty);
            Addproductprice = (TextView) view.findViewById(R.id.Addproductprice);


            llPurpose = (LinearLayout) view.findViewById(R.id.llPurpose);
            tvFromName = (TextView) view.findViewById(R.id.tvFromName);
            tvFromAddress = (TextView) view.findViewById(R.id.tvFromAddress);
            tvToName = (TextView) view.findViewById(R.id.tvToName);
            tvToAddress = (TextView) view.findViewById(R.id.tvToAddress);
            tvFromContact = (TextView) view.findViewById(R.id.tvFromContact);
            tvToContact = (TextView) view.findViewById(R.id.tvToContact);
            tvDistance = (TextView) view.findViewById(R.id.tvDistance);
            tvDeliveryDate = (TextView) view.findViewById(R.id.tvDeliveryDate);
            tvRefId = (TextView) view.findViewById(R.id.tvRefId);
            tvReference = (TextView) view.findViewById(R.id.tvReference);
        }
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }

    public ChititemAdapter(List<Chitproducts> chititemlist, Context context, OnItemClickListener listener, String currencysymbol, String roundofvalue) {
        this.productlist = chititemlist;
        this.mContext = context;
        this.listener = listener;
        this.currency = currencysymbol + " ";
        this.roundofvalue = roundofvalue;
        sessionManager = new SessionManager(context);
        //currency = SharedPrefUserDetail.getString(context, SharedPrefUserDetail.symbol_native,"") + " ";
    }

    public ChititemAdapter(List<Chitproducts> chititemlist, Context context, String currencysymbol) {
        this.productlist = chititemlist;
        this.mContext = context;
        this.currency = currencysymbol + " ";
        sessionManager = new SessionManager(context);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.chititem_row, parent, false);

        return new MyViewHolder(itemView);
    }

    SessionManager sessionManager;

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final Chitproducts chitproduct = productlist.get(position);
        Double itemprice = Double.parseDouble(chitproduct.getQuantity());
        int itempriceval = Integer.valueOf(itemprice.intValue());
        holder.txt_serialno.setText("# " + itempriceval);


        String part = chitproduct.getParticulars();
        String parti[] = part.split(";;;");
        String partiImage[] = part.split(";;;;");
        if (parti.length > 0) {
            part = parti[0];

        }

      /*  if(parti.length>1){
            initMultiImg(holder,parti[1]);
        }
        else {
            initMultiImg(holder,"");

        }*/

        holder.tv_offer.setText(chitproduct.getOffer());
        DecimalFormat twoDForm = new DecimalFormat("#.##");
        Double formatedprice = 0.00;
        Double formatedtotal = 0.00;

        try {
            Double price = Double.parseDouble(chitproduct.getPrice()); // Make use of autoboxing.  It's also easier to read.
            Double total = Double.parseDouble(chitproduct.getTotal()); // Make use of autoboxing.  It's also easier to read.

            Double quantity = Double.parseDouble(chitproduct.getQuantity());

           /* if (doctor_template_type.equalsIgnoreCase("RPD")) {
                price = price + Amrp;
                total = total + (Amrp * quantity);
            }*/

            formatedprice = Double.valueOf(twoDForm.format(price));
            formatedtotal = Double.valueOf(twoDForm.format(total));

        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

        holder.tvCommentForSurvey.setVisibility(View.GONE);
        String surveyoption[] = part.split("///");
        if (surveyoption.length > 2) {
            holder.txt_productname.setText(surveyoption[0]);
            holder.txt_serialno.setText("  " + surveyoption[1]);
            holder.tvCommentForSurvey.setText("Comment:  " + surveyoption[2]);
            holder.tvCommentForSurvey.setVisibility(View.VISIBLE);

            if (sessionManager.getIsConsumer()) {
                holder.txt_productqty.setVisibility(View.INVISIBLE);
                holder.txt_productprice.setVisibility(View.INVISIBLE);
            }
        } else if (surveyoption.length > 1) {
            holder.txt_productname.setText(surveyoption[0]);
            holder.txt_serialno.setText("  " + surveyoption[1]);

            if (sessionManager.getIsConsumer()) {
                holder.txt_productqty.setVisibility(View.INVISIBLE);
                holder.txt_productprice.setVisibility(View.INVISIBLE);
            }

        } else {
            holder.txt_productname.setText(part);

        }


        holder.txt_productqty.setText(currency + Inad.getCurrencyDecimal(formatedprice, mContext));
        //holder.txt_productprice.setText(currency+String.format("%.2f", formatedtotal));
        holder.txt_productprice.setText(currency + Inad.getCurrencyDecimal(formatedtotal, mContext));


        try {
            JSONObject obj = new JSONObject(chitproduct.getParticulars());

            String name = JsonObjParse.getValueEmpty(obj.toString(), "name");
            holder.txt_productname.setText(name);

            String xRef = JsonObjParse.getValueEmpty(obj.toString(), "xRef");
            String samples = JsonObjParse.getValueEmpty(obj.toString(), "samples");
            try {
                joDelivery = new JSONObject(JsonObjParse.getValueEmpty(obj.toString(), "delivery_service"));
                Log.d("delivery_detail", joDelivery.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }

            String purpose = JsonObjParse.getValueEmpty(obj.toString(), "purpose");
            String fromName = "";
            String fromContact = "";
            String fromAddress = "";
            String toName = "";
            String toContact = "";
            String toAddress = "";
            String distance = "";
            String delivery_date = "";
            String delivery_time = "";
            String reference = "";
            String date = "";
            String ref_id = "";
            String case_id = "";
            try {
                fromName = JsonObjParse.getValueEmpty(joDelivery.toString(), "fromName");
                fromContact = JsonObjParse.getValueEmpty(joDelivery.toString(), "fromContact");
                fromAddress = JsonObjParse.getValueEmpty(joDelivery.toString(), "fromAddress");
                toName = JsonObjParse.getValueEmpty(joDelivery.toString(), "toName");
                toContact = JsonObjParse.getValueEmpty(joDelivery.toString(), "toContact");
                toAddress = JsonObjParse.getValueEmpty(joDelivery.toString(), "toAddress");
                distance = JsonObjParse.getValueEmpty(joDelivery.toString(), "delivery_distance");
                delivery_date = JsonObjParse.getValueEmpty(joDelivery.toString(), "delivery_date");
                delivery_time = JsonObjParse.getValueEmpty(joDelivery.toString(), "delivery_time");
                reference = JsonObjParse.getValueEmpty(joDelivery.toString(), "reference");
                date = delivery_date + " " + delivery_time;

                ref_id = JsonObjParse.getValueEmpty(joDelivery.toString(), "ref_id");
                case_id = JsonObjParse.getValueEmpty(joDelivery.toString(), "case_id");
            } catch (Exception e) {
                e.printStackTrace();
            }


            String id_no = "";
            if (ref_id.equalsIgnoreCase("")) {
                id_no = "";
            } else {
                id_no = "/ " + ref_id + " / " + case_id;
            }

            if (purpose.equalsIgnoreCase("delivery")) {
                holder.llPurpose.setVisibility(View.VISIBLE);
                holder.txt_productname.setText("Delivery Charges");
            } else {
                holder.llPurpose.setVisibility(View.GONE);
            }

            holder.tvFromName.setText(fromName + "");
            holder.tvFromAddress.setText(fromAddress + "");
            holder.tvToName.setText(toName + "");
            holder.tvToAddress.setText(toAddress + "");
            holder.tvFromContact.setText(fromContact + "");
            holder.tvToContact.setText(toContact + "");
            holder.tvDistance.setText(distance + "");
            holder.tvDeliveryDate.setText(date + "");
            holder.tvRefId.setText(id_no + "");
            holder.tvReference.setText(reference + "");

            initMultiImg(holder, samples);

        } catch (JSONException e) {
            e.printStackTrace();
        }
      /*  if(partiImage.length>1){
            initMultiImg(holder,partiImage[1]);
        }
        else {
            initMultiImg(holder,"");

        }*/

        try {
            JSONObject obj = new JSONObject(chitproduct.getParticulars());

            String internal_json = JsonObjParse.getValueEmpty(obj.toString(), "internal_json_data");
            String doctor_template_type = JsonObjParse.getValueEmpty(internal_json, "doctor_template_type");


            if (doctor_template_type.equalsIgnoreCase("RPD") || doctor_template_type.equalsIgnoreCase("Precision Attachment")) {
                Double add_price = 0.0;
                try {
                    add_price = Double.parseDouble(JsonObjParse.getValueEmpty(obj.toString(), "additional_price"));
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Log.d("add_price", add_price + "");
                if (add_price != 0.0) {
                    if (itempriceval > 1) {
                        holder.llAddDetail.setVisibility(View.VISIBLE);
                        formatedtotal = Double.valueOf(twoDForm.format(add_price));
                        Double addPrice = formatedtotal - formatedprice;
                        Double add_qty = addPrice / (itempriceval - 1);
                        holder.Addproductqty.setText(currency + Inad.getCurrencyDecimal(add_qty, mContext));
                        holder.Addproductprice.setText(currency + Inad.getCurrencyDecimal(addPrice, mContext));
                        holder.txt_productprice.setText(currency + Inad.getCurrencyDecimal(formatedprice, mContext));
                        holder.txt_serialno.setText("# " + 1);
                        holder.Addserialno.setText("# " + (itempriceval - 1));
                    }
                }
            } else {
                holder.llAddDetail.setVisibility(View.GONE);
            }

            if (doctor_template_type.equalsIgnoreCase("")) {
                holder.llDentalImage.setVisibility(View.GONE);
            } else {
                holder.llDentalImage.setVisibility(View.VISIBLE);
            }

            String d = JsonObjParse.getValueEmpty(internal_json, "dental");
            Log.d("dental_d", d);
            dental = new

                    Dental();

            dental = new

                    Gson().

                    fromJson(d, Dental.class);

            if (dental == null) dental = new

                    Dental();
            templateList.add(dental);
            try {
                holder.tvPatientName.setText(dental.getName() + "");
            } catch (
                    Exception e) {
                e.printStackTrace();
            }
            try {
                holder.tvPatientAge.setText(dental.getAge() + "");
            } catch (
                    Exception e) {
                e.printStackTrace();
            }
            try {
                holder.tvPatientSex.setText(dental.getSex() + "");
            } catch (
                    Exception e) {
                e.printStackTrace();
            }
            try {
                holder.tvSelectedTeeth.setText(dental.getTeeth() + "");
            } catch (
                    Exception e) {
                e.printStackTrace();
            }
            try {
                if (dental.getInstructioon().equalsIgnoreCase("")) {
                    holder.llInstructionName.setVisibility(View.GONE);
                } else {
                    holder.llInstructionName.setVisibility(View.VISIBLE);
                    holder.tvPatientInstruction.setText(dental.getInstructioon() + "");
                }
            } catch (
                    Exception e) {
                e.printStackTrace();
                holder.llInstructionName.setVisibility(View.GONE);
            }
            try {
                holder.tvPreference.setText(dental.getPreference() + "");
            } catch (
                    Exception e) {
                e.printStackTrace();
            }
            try {
                if (dental.getImpression().equalsIgnoreCase("")) {
                    holder.llImpression.setVisibility(View.GONE);
                } else {
                    holder.tvImpression.setText(dental.getImpression() + "");
                    holder.llImpression.setVisibility(View.VISIBLE);
                }

            } catch (
                    Exception e) {
                e.printStackTrace();
                holder.llImpression.setVisibility(View.GONE);
            }
            try {
                if (dental.getChief_complaint().equalsIgnoreCase("")) {
                    holder.llChief.setVisibility(View.GONE);
                } else {
                    holder.llChief.setVisibility(View.VISIBLE);
                    holder.tvChief.setText(dental.getChief_complaint() + "");
                }
            } catch (
                    Exception e) {
                e.printStackTrace();
                holder.llChief.setVisibility(View.GONE);
            }
            try {
                if (dental.getTreatment().equalsIgnoreCase("")) {
                    holder.llTreatment.setVisibility(View.GONE);
                } else {
                    holder.llTreatment.setVisibility(View.VISIBLE);
                    holder.tvTreatment.setText(dental.getTreatment() + "");
                }
            } catch (
                    Exception e) {
                e.printStackTrace();
                holder.llTreatment.setVisibility(View.GONE);
            }
            try {
                abutment_detail = new StringBuilder();
                for (int i = 0; i < dental.getAbutmentList().size(); i++) {
                    if (i == 0) {

                    } else {
                        if (dental.getAbutmentList().get(i).getValueD().equalsIgnoreCase("") && dental.getAbutmentList().get(i).getValueL().equalsIgnoreCase("")) {
                        } else {
                            abutment_detail.append("(" + dental.getAbutmentList().get(i).getValueD() + "*" + dental.getAbutmentList().get(i).getValueL() + ")");
                        }
                    }
                }
                if (abutment_detail.length() == 0) {
                    holder.llAbutment.setVisibility(View.GONE);
                } else {
                    holder.llAbutment.setVisibility(View.VISIBLE);
                    holder.tvAbutment.setText(abutment_detail);
                }

            } catch (
                    Exception e) {
                e.printStackTrace();
            }
            try {
                impression_detail = new StringBuilder();
                for (int i = 0; i < dental.getImpressionList().size(); i++) {
                    if (i == 0) {

                    } else {
                        if (dental.getImpressionList().get(i).getValueD().equalsIgnoreCase("") && dental.getImpressionList().get(i).getValueL().equalsIgnoreCase("")) {
                        } else {
                            impression_detail.append("(" + dental.getImpressionList().get(i).getValueD() + "*" + dental.getImpressionList().get(i).getValueL() + ")");
                        }
                    }

                }
                if (impression_detail.length() == 0) {
                    holder.llImpressionList.setVisibility(View.GONE);
                } else {
                    holder.llImpressionList.setVisibility(View.VISIBLE);
                    holder.tvImpression_list.setText(impression_detail);
                }

            } catch (
                    Exception e) {
                e.printStackTrace();
            }
            try {
                implant_detail = new StringBuilder();
                for (int i = 0; i < dental.getImplantList().size(); i++) {
                    if (i == 0) {

                    } else {
                        if (dental.getImplantList().get(i).getValueD().equalsIgnoreCase("") && dental.getImplantList().get(i).getValueL().equalsIgnoreCase("")) {
                        } else {
                            implant_detail.append("(" + dental.getImplantList().get(i).getValueD() + "*" + dental.getImplantList().get(i).getValueL() + ")");
                        }
                    }

                }
                if (implant_detail.length() == 0) {
                    holder.llImplant.setVisibility(View.GONE);
                } else {
                    holder.llImplant.setVisibility(View.VISIBLE);
                    holder.tvImplant.setText(implant_detail);
                }

            } catch (
                    Exception e) {
                e.printStackTrace();
            }

            try {
                if (dental.getShade().equalsIgnoreCase("")) {
                    holder.llShade.setVisibility(View.GONE);
                } else {
                    holder.llShade.setVisibility(View.VISIBLE);
                    holder.tvShade.setText(dental.getShade() + "");
                }
            } catch (
                    Exception e) {
                e.printStackTrace();
                holder.llShade.setVisibility(View.GONE);
            }
            if (dental.getAl_selet().

                    size() == 0) {
                holder.rv_selectfile_shade.setVisibility(View.GONE);
            } else {
                holder.rv_selectfile_shade.setVisibility(View.VISIBLE);
                LinearLayoutManager mLayoutManager1 = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
                holder.rv_selectfile_shade.setLayoutManager(mLayoutManager1);
                imgPrevAdapter = new ImgPrevAdapter(dental.getAl_selet(), mContext, new ImgPrevAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(int pos) {
                        String imgarray = new Gson().toJson(templateList.get(position).getAl_selet());
                        Intent intent = new Intent(mContext, ImagePreviewActivity.class);
                        intent.putExtra("imgarray", imgarray);
                        intent.putExtra("pos", pos);
                        intent.putExtra("isdrag", false);
                        intent.putExtra("img_title", "Shade");
                        mContext.startActivity(intent);
                    }

                    @Override
                    public void onDelete(int pos) {
                    }

                    @Override
                    public void onLongClick(View pos) {
                    }
                }, false);
                holder.rv_selectfile_shade.setAdapter(imgPrevAdapter);
            }

            if (!dental.isTray && templateList.get(position).

                    getAl_selet_tray().

                    size() == 0) {
                holder.llTray.setVisibility(View.GONE);
            } else {
                holder.llTray.setVisibility(View.VISIBLE);
                LinearLayoutManager mLayoutManager2 = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
                holder.rv_selectfile_tray.setLayoutManager(mLayoutManager2);
                imgPrevAdapter = new ImgPrevAdapter(dental.getAl_selet_tray(), mContext, new ImgPrevAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(int pos) {

                        String imgarray = new Gson().toJson(templateList.get(position).getAl_selet_tray());
                        Intent intent = new Intent(mContext, ImagePreviewActivity.class);
                        intent.putExtra("imgarray", imgarray);
                        intent.putExtra("pos", pos);
                        intent.putExtra("isdrag", false);
                        intent.putExtra("img_title", "Tray");
                        mContext.startActivity(intent);
                    }

                    @Override
                    public void onDelete(int pos) {
                    }

                    @Override
                    public void onLongClick(View pos) {
                    }
                }, false);
                holder.rv_selectfile_tray.setAdapter(imgPrevAdapter);

            }

            if (!dental.isModel && templateList.get(position).

                    getAl_selet_model().

                    size() == 0) {
                holder.llModel.setVisibility(View.GONE);
            } else {
                holder.llModel.setVisibility(View.VISIBLE);
                LinearLayoutManager mLayoutManager3 = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
                holder.rv_selectfile_model.setLayoutManager(mLayoutManager3);
                imgPrevAdapter = new ImgPrevAdapter(dental.getAl_selet_model(), mContext, new ImgPrevAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(int pos) {

                        String imgarray = new Gson().toJson(templateList.get(position).getAl_selet_model());
                        Intent intent = new Intent(mContext, ImagePreviewActivity.class);
                        intent.putExtra("imgarray", imgarray);
                        intent.putExtra("pos", pos);
                        intent.putExtra("isdrag", false);
                        intent.putExtra("img_title", "Model");
                        mContext.startActivity(intent);
                    }

                    @Override
                    public void onDelete(int pos) {
                    }

                    @Override
                    public void onLongClick(View pos) {
                    }
                }, false);
                holder.rv_selectfile_model.setAdapter(imgPrevAdapter);
            }

            if (!dental.isBite && templateList.get(position).

                    getAl_selet_bite().

                    size() == 0) {
                holder.llBite.setVisibility(View.GONE);
            } else {
                holder.llBite.setVisibility(View.VISIBLE);
                LinearLayoutManager mLayoutManager4 = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
                holder.rv_selectfile_bite.setLayoutManager(mLayoutManager4);
                imgPrevAdapter = new ImgPrevAdapter(templateList.get(position).getAl_selet_bite(), mContext, new ImgPrevAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(int pos) {

                        String imgarray = new Gson().toJson(dental.getAl_selet_bite());
                        Intent intent = new Intent(mContext, ImagePreviewActivity.class);
                        intent.putExtra("imgarray", imgarray);
                        intent.putExtra("pos", pos);
                        intent.putExtra("isdrag", false);
                        intent.putExtra("img_title", "Bite");
                        mContext.startActivity(intent);
                    }

                    @Override
                    public void onDelete(int pos) {
                    }

                    @Override
                    public void onLongClick(View pos) {
                    }
                }, false);
                holder.rv_selectfile_bite.setAdapter(imgPrevAdapter);

            }

            if (!dental.isTemp && templateList.get(position).

                    getAl_selet_temp().

                    size() == 0) {
                holder.llTemp.setVisibility(View.GONE);
            } else {
                holder.llTemp.setVisibility(View.VISIBLE);
                LinearLayoutManager mLayoutManager5 = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
                holder.rv_selectfile_temp.setLayoutManager(mLayoutManager5);
                imgPrevAdapter = new ImgPrevAdapter(dental.getAl_selet_temp(), mContext, new ImgPrevAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(int pos) {

                        String imgarray = new Gson().toJson(templateList.get(position).getAl_selet_temp());
                        Intent intent = new Intent(mContext, ImagePreviewActivity.class);
                        intent.putExtra("imgarray", imgarray);
                        intent.putExtra("pos", pos);
                        intent.putExtra("isdrag", false);
                        intent.putExtra("img_title", "Temp");
                        mContext.startActivity(intent);
                    }

                    @Override
                    public void onDelete(int pos) {
                    }

                    @Override
                    public void onLongClick(View pos) {
                    }
                }, false);
                holder.rv_selectfile_temp.setAdapter(imgPrevAdapter);
            }
            if (!dental.isFrontal && templateList.get(position).

                    getAl_selet_frontal().

                    size() == 0) {
                holder.llFrontal.setVisibility(View.GONE);
            } else {
                holder.llFrontal.setVisibility(View.VISIBLE);
                LinearLayoutManager mLayoutManager5 = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
                holder.rv_selectfile_frontal.setLayoutManager(mLayoutManager5);
                imgPrevAdapter = new ImgPrevAdapter(dental.getAl_selet_frontal(), mContext, new ImgPrevAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(int pos) {

                        String imgarray = new Gson().toJson(templateList.get(position).getAl_selet_frontal());
                        Intent intent = new Intent(mContext, ImagePreviewActivity.class);
                        intent.putExtra("imgarray", imgarray);
                        intent.putExtra("pos", pos);
                        intent.putExtra("isdrag", false);
                        intent.putExtra("img_title", "Temp");
                        mContext.startActivity(intent);
                    }

                    @Override
                    public void onDelete(int pos) {
                    }

                    @Override
                    public void onLongClick(View pos) {
                    }
                }, false);
                holder.rv_selectfile_frontal.setAdapter(imgPrevAdapter);
            }
            if (!dental.isFrontalSmile && templateList.get(position).

                    getAl_selet_frontalSmile().

                    size() == 0) {
                holder.llFrontalSmile.setVisibility(View.GONE);
            } else {
                holder.llFrontalSmile.setVisibility(View.VISIBLE);
                LinearLayoutManager mLayoutManager5 = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
                holder.rv_selectfile_frontalSmile.setLayoutManager(mLayoutManager5);
                imgPrevAdapter = new ImgPrevAdapter(dental.getAl_selet_frontalSmile(), mContext, new ImgPrevAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(int pos) {

                        String imgarray = new Gson().toJson(templateList.get(position).getAl_selet_frontalSmile());
                        Intent intent = new Intent(mContext, ImagePreviewActivity.class);
                        intent.putExtra("imgarray", imgarray);
                        intent.putExtra("pos", pos);
                        intent.putExtra("isdrag", false);
                        intent.putExtra("img_title", "Temp");
                        mContext.startActivity(intent);
                    }

                    @Override
                    public void onDelete(int pos) {
                    }

                    @Override
                    public void onLongClick(View pos) {
                    }
                }, false);
                holder.rv_selectfile_frontalSmile.setAdapter(imgPrevAdapter);
            }
            if (!dental.isLeftLateral && templateList.get(position).

                    getAl_selet_leftLateral().

                    size() == 0) {
                holder.llLeftLateral.setVisibility(View.GONE);
            } else {
                holder.llLeftLateral.setVisibility(View.VISIBLE);
                LinearLayoutManager mLayoutManager5 = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
                holder.rv_selectfile_leftLateral.setLayoutManager(mLayoutManager5);
                imgPrevAdapter = new ImgPrevAdapter(dental.getAl_selet_leftLateral(), mContext, new ImgPrevAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(int pos) {

                        String imgarray = new Gson().toJson(templateList.get(position).getAl_selet_leftLateral());
                        Intent intent = new Intent(mContext, ImagePreviewActivity.class);
                        intent.putExtra("imgarray", imgarray);
                        intent.putExtra("pos", pos);
                        intent.putExtra("isdrag", false);
                        intent.putExtra("img_title", "Temp");
                        mContext.startActivity(intent);
                    }

                    @Override
                    public void onDelete(int pos) {
                    }

                    @Override
                    public void onLongClick(View pos) {
                    }
                }, false);
                holder.rv_selectfile_leftLateral.setAdapter(imgPrevAdapter);
            }
            if (!dental.isLeftLateralSmile && templateList.get(position).

                    getAl_selet_leftLateralSmile().

                    size() == 0) {
                holder.llLeftLateralSmile.setVisibility(View.GONE);
            } else {
                holder.llLeftLateralSmile.setVisibility(View.VISIBLE);
                LinearLayoutManager mLayoutManager5 = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
                holder.rv_selectfile_leftLateralSmile.setLayoutManager(mLayoutManager5);
                imgPrevAdapter = new ImgPrevAdapter(dental.getAl_selet_leftLateralSmile(), mContext, new ImgPrevAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(int pos) {

                        String imgarray = new Gson().toJson(templateList.get(position).getAl_selet_leftLateralSmile());
                        Intent intent = new Intent(mContext, ImagePreviewActivity.class);
                        intent.putExtra("imgarray", imgarray);
                        intent.putExtra("pos", pos);
                        intent.putExtra("isdrag", false);
                        intent.putExtra("img_title", "Temp");
                        mContext.startActivity(intent);
                    }

                    @Override
                    public void onDelete(int pos) {
                    }

                    @Override
                    public void onLongClick(View pos) {
                    }
                }, false);
                holder.rv_selectfile_leftLateralSmile.setAdapter(imgPrevAdapter);
            }
            if (!dental.isRightLateral && templateList.get(position).

                    getAl_selet_rightLateral().

                    size() == 0) {
                holder.llRightLateral.setVisibility(View.GONE);
            } else {
                holder.llRightLateral.setVisibility(View.VISIBLE);
                LinearLayoutManager mLayoutManager5 = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
                holder.rv_selectfile_rightLateral.setLayoutManager(mLayoutManager5);
                imgPrevAdapter = new ImgPrevAdapter(dental.getAl_selet_rightLateral(), mContext, new ImgPrevAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(int pos) {

                        String imgarray = new Gson().toJson(templateList.get(position).getAl_selet_rightLateral());
                        Intent intent = new Intent(mContext, ImagePreviewActivity.class);
                        intent.putExtra("imgarray", imgarray);
                        intent.putExtra("pos", pos);
                        intent.putExtra("isdrag", false);
                        intent.putExtra("img_title", "Temp");
                        mContext.startActivity(intent);
                    }

                    @Override
                    public void onDelete(int pos) {
                    }

                    @Override
                    public void onLongClick(View pos) {
                    }
                }, false);
                holder.rv_selectfile_rightLateral.setAdapter(imgPrevAdapter);
            }
            if (!dental.isRightLateralSmile && templateList.get(position).

                    getAl_selet_rightLateralSmile().

                    size() == 0) {
                holder.llRightLateralSmile.setVisibility(View.GONE);
            } else {
                holder.llRightLateralSmile.setVisibility(View.VISIBLE);
                LinearLayoutManager mLayoutManager5 = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
                holder.rv_selectfile_rightLateralSmile.setLayoutManager(mLayoutManager5);
                imgPrevAdapter = new ImgPrevAdapter(dental.getAl_selet_rightLateralSmile(), mContext, new ImgPrevAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(int pos) {

                        String imgarray = new Gson().toJson(templateList.get(position).getAl_selet_rightLateralSmile());
                        Intent intent = new Intent(mContext, ImagePreviewActivity.class);
                        intent.putExtra("imgarray", imgarray);
                        intent.putExtra("pos", pos);
                        intent.putExtra("isdrag", false);
                        intent.putExtra("img_title", "Temp");
                        mContext.startActivity(intent);
                    }

                    @Override
                    public void onDelete(int pos) {
                    }

                    @Override
                    public void onLongClick(View pos) {
                    }
                }, false);
                holder.rv_selectfile_rightLateralSmile.setAdapter(imgPrevAdapter);
            }
            if (!dental.isIntraFrontal && templateList.get(position).

                    getAl_selet_intraFrontal().

                    size() == 0) {
                holder.llIntraFrontal.setVisibility(View.GONE);
            } else {
                holder.llIntraFrontal.setVisibility(View.VISIBLE);
                LinearLayoutManager mLayoutManager5 = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
                holder.rv_selectfile_intraFrontal.setLayoutManager(mLayoutManager5);
                imgPrevAdapter = new ImgPrevAdapter(dental.getAl_selet_intraFrontal(), mContext, new ImgPrevAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(int pos) {

                        String imgarray = new Gson().toJson(templateList.get(position).getAl_selet_intraFrontal());
                        Intent intent = new Intent(mContext, ImagePreviewActivity.class);
                        intent.putExtra("imgarray", imgarray);
                        intent.putExtra("pos", pos);
                        intent.putExtra("isdrag", false);
                        intent.putExtra("img_title", "Temp");
                        mContext.startActivity(intent);
                    }

                    @Override
                    public void onDelete(int pos) {
                    }

                    @Override
                    public void onLongClick(View pos) {
                    }
                }, false);
                holder.rv_selectfile_intraFrontal.setAdapter(imgPrevAdapter);
            }
            if (!dental.isIntraRightLateral && templateList.get(position).

                    getAl_selet_intraRightLateral().

                    size() == 0) {
                holder.llIntraRightLateral.setVisibility(View.GONE);
            } else {
                holder.llIntraRightLateral.setVisibility(View.VISIBLE);
                LinearLayoutManager mLayoutManager5 = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
                holder.rv_selectfile_intraRightLateral.setLayoutManager(mLayoutManager5);
                imgPrevAdapter = new ImgPrevAdapter(dental.getAl_selet_intraRightLateral(), mContext, new ImgPrevAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(int pos) {

                        String imgarray = new Gson().toJson(templateList.get(position).getAl_selet_intraRightLateral());
                        Intent intent = new Intent(mContext, ImagePreviewActivity.class);
                        intent.putExtra("imgarray", imgarray);
                        intent.putExtra("pos", pos);
                        intent.putExtra("isdrag", false);
                        intent.putExtra("img_title", "Temp");
                        mContext.startActivity(intent);
                    }

                    @Override
                    public void onDelete(int pos) {
                    }

                    @Override
                    public void onLongClick(View pos) {
                    }
                }, false);
                holder.rv_selectfile_intraRightLateral.setAdapter(imgPrevAdapter);
            }
            if (!dental.isIntraLeftLateral && templateList.get(position).

                    getAl_selet_intraLeftLateral().

                    size() == 0) {
                holder.llIntraLeftLateral.setVisibility(View.GONE);
            } else {
                holder.llIntraLeftLateral.setVisibility(View.VISIBLE);
                LinearLayoutManager mLayoutManager5 = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
                holder.rv_selectfile_intraLeftLateral.setLayoutManager(mLayoutManager5);
                imgPrevAdapter = new ImgPrevAdapter(dental.getAl_selet_intraLeftLateral(), mContext, new ImgPrevAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(int pos) {

                        String imgarray = new Gson().toJson(templateList.get(position).getAl_selet_intraLeftLateral());
                        Intent intent = new Intent(mContext, ImagePreviewActivity.class);
                        intent.putExtra("imgarray", imgarray);
                        intent.putExtra("pos", pos);
                        intent.putExtra("isdrag", false);
                        intent.putExtra("img_title", "Temp");
                        mContext.startActivity(intent);
                    }

                    @Override
                    public void onDelete(int pos) {
                    }

                    @Override
                    public void onLongClick(View pos) {
                    }
                }, false);
                holder.rv_selectfile_intraLeftLateral.setAdapter(imgPrevAdapter);
            }
            if (!dental.isIntraUpperOcclusal && templateList.get(position).

                    getAl_selet_intraUpperOcclusal().

                    size() == 0) {
                holder.llUpperOcclusal.setVisibility(View.GONE);
            } else {
                holder.llUpperOcclusal.setVisibility(View.VISIBLE);
                LinearLayoutManager mLayoutManager5 = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
                holder.rv_selectfile_upperOcclusal.setLayoutManager(mLayoutManager5);
                imgPrevAdapter = new ImgPrevAdapter(dental.getAl_selet_intraUpperOcclusal(), mContext, new ImgPrevAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(int pos) {

                        String imgarray = new Gson().toJson(templateList.get(position).getAl_selet_intraUpperOcclusal());
                        Intent intent = new Intent(mContext, ImagePreviewActivity.class);
                        intent.putExtra("imgarray", imgarray);
                        intent.putExtra("pos", pos);
                        intent.putExtra("isdrag", false);
                        intent.putExtra("img_title", "Temp");
                        mContext.startActivity(intent);
                    }

                    @Override
                    public void onDelete(int pos) {
                    }

                    @Override
                    public void onLongClick(View pos) {
                    }
                }, false);
                holder.rv_selectfile_upperOcclusal.setAdapter(imgPrevAdapter);
            }
            if (!dental.isIntraLeftOcclusal && templateList.get(position).

                    getAl_selet_intraLeftOcclusal().

                    size() == 0) {
                holder.llLeftOcclusal.setVisibility(View.GONE);
            } else {
                holder.llLeftOcclusal.setVisibility(View.VISIBLE);
                LinearLayoutManager mLayoutManager5 = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
                holder.rv_selectfile_leftOcclusal.setLayoutManager(mLayoutManager5);
                imgPrevAdapter = new ImgPrevAdapter(dental.getAl_selet_intraLeftOcclusal(), mContext, new ImgPrevAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(int pos) {

                        String imgarray = new Gson().toJson(templateList.get(position).getAl_selet_intraLeftOcclusal());
                        Intent intent = new Intent(mContext, ImagePreviewActivity.class);
                        intent.putExtra("imgarray", imgarray);
                        intent.putExtra("pos", pos);
                        intent.putExtra("isdrag", false);
                        intent.putExtra("img_title", "Temp");
                        mContext.startActivity(intent);
                    }

                    @Override
                    public void onDelete(int pos) {
                    }

                    @Override
                    public void onLongClick(View pos) {
                    }
                }, false);
                holder.rv_selectfile_leftOcclusal.setAdapter(imgPrevAdapter);
            }
            try {
                if (dental.getColour().equalsIgnoreCase("")) {
                    holder.llColour.setVisibility(View.GONE);
                } else {
                    holder.llColour.setVisibility(View.VISIBLE);
                    holder.tvColour.setText(dental.getColour() + "");
                }
            } catch (
                    Exception e) {
                holder.llColour.setVisibility(View.GONE);
                e.printStackTrace();
            }
            if (dental.getAl_selet_colour().

                    size() == 0) {
                holder.rv_selectfile_colour.setVisibility(View.GONE);
            } else {
                holder.rv_selectfile_colour.setVisibility(View.VISIBLE);
                LinearLayoutManager mLayoutManager1 = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
                holder.rv_selectfile_colour.setLayoutManager(mLayoutManager1);
                imgPrevAdapter = new ImgPrevAdapter(dental.getAl_selet_colour(), mContext, new ImgPrevAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(int pos) {

                        String imgarray = new Gson().toJson(templateList.get(position).getAl_selet_colour());
                        Intent intent = new Intent(mContext, ImagePreviewActivity.class);
                        intent.putExtra("imgarray", imgarray);
                        intent.putExtra("pos", pos);
                        intent.putExtra("isdrag", false);
                        intent.putExtra("img_title", "Shade");
                        mContext.startActivity(intent);

                    }

                    @Override
                    public void onDelete(int pos) {

                    }

                    @Override
                    public void onLongClick(View pos) {

                    }
                }, false);
                holder.rv_selectfile_colour.setAdapter(imgPrevAdapter);
            }

            try {
                if (dental.getPhotoInstruction().equalsIgnoreCase("")) {
                    holder.llInstruction.setVisibility(View.GONE);
                } else {
                    holder.llInstruction.setVisibility(View.VISIBLE);
                    holder.tvInstruction.setText(dental.getPhotoInstruction() + "");
                }

            } catch (
                    Exception e) {
                e.printStackTrace();
                holder.llInstruction.setVisibility(View.GONE);
            }
            if (dental.getAl_selet_instruction().

                    size() == 0) {
                holder.rv_selectfile_instruction.setVisibility(View.GONE);
            } else {
                holder.rv_selectfile_instruction.setVisibility(View.VISIBLE);
                LinearLayoutManager mLayoutManager1 = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
                holder.rv_selectfile_instruction.setLayoutManager(mLayoutManager1);
                imgPrevAdapter = new ImgPrevAdapter(dental.getAl_selet_instruction(), mContext, new ImgPrevAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(int pos) {

                        String imgarray = new Gson().toJson(templateList.get(position).getAl_selet_instruction());
                        Intent intent = new Intent(mContext, ImagePreviewActivity.class);
                        intent.putExtra("imgarray", imgarray);
                        intent.putExtra("pos", pos);
                        intent.putExtra("isdrag", false);
                        intent.putExtra("img_title", "Shade");
                        mContext.startActivity(intent);

                    }

                    @Override
                    public void onDelete(int pos) {

                    }

                    @Override
                    public void onLongClick(View pos) {

                    }
                }, false);
                holder.rv_selectfile_instruction.setAdapter(imgPrevAdapter);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return productlist.size();
    }

    public void setFilter(List<Chitproducts> countryModels) {
        productlist = new ArrayList<>();
        productlist.addAll(countryModels);
        notifyDataSetChanged();
    }

    public void setitem(List<Chitproducts> products) {
        this.productlist = products;
        notifyDataSetChanged();

    }

    public void initMultiImg(final MyViewHolder holder, String img) {

       /* TypeToken<ArrayList<ModelFile>> ttSamples = new TypeToken<ArrayList<ModelFile>>() {};
        ArrayList<ModelFile> alSamples = new Gson().fromJson(img, ttSamples.getType());*/

        TypeToken<ArrayList<String>> ttSamples = new TypeToken<ArrayList<String>>() {
        };

        ArrayList<String> allImage = new Gson().fromJson(img, ttSamples.getType());

        if (allImage == null) {
            allImage = new ArrayList<>();
        }

        ArrayList<ModelFile> alSamples = new ArrayList<>();

        for (int i = 0; i < allImage.size(); i++) {
            alSamples.add(new ModelFile(allImage.get(i), false));

        }

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        holder.rvSamples.setLayoutManager(mLayoutManager);

        SampleImgInCartAdapter sampleImgInCartAdapter = new SampleImgInCartAdapter(alSamples, mContext, new SampleImgInCartAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(String item) {

            }

            @Override
            public void onDelete(int pos, ModelFile modelFile) {


            }


            @Override
            public void onClickString(String pos) {

            }

            @Override
            public void onImgClick(List<ModelFile> data, int pos) {

                String imgarray = new Gson().toJson(data);
                Intent intent = new Intent(mContext, ImagePreviewActivity.class);
                intent.putExtra("imgarray", imgarray);
                intent.putExtra("pos", pos);
                intent.putExtra("img_title", holder.txt_productname.getText().toString());
                mContext.startActivity(intent);
            }

            @Override
            public void onAddEditInfo(ModelFile item, int pos) {

            }
        }, false, false, false);

        holder.rvSamples.setAdapter(sampleImgInCartAdapter);

    /*    ItemTouchHelper.Callback callback = new ShopDragTouchHelperIssue(shopImgFileAdapter);
        ItemTouchHelper helper = new ItemTouchHelper(callback);
        helper.attachToRecyclerView(holder.rvSamples);*/

    }
}
