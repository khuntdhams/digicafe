package com.cab.digicafe.Adapter.healthAdapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.cab.digicafe.Model.implantModel;
import com.cab.digicafe.R;

import java.util.ArrayList;

public class ImplantAdapter extends RecyclerView.Adapter<ImplantAdapter.MyViewHolder> {

    private ArrayList<implantModel> implantList;
    Context context;
    /*  private OnItemClickListener listener;*/

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvImplantId;
        public EditText tvImplantValueD, tvImplantValueL;

        public MyViewHolder(View view) {
            super(view);
            tvImplantId = (TextView) view.findViewById(R.id.tvImplantId);
            tvImplantValueD = (EditText) view.findViewById(R.id.tvImplantValueD);
            tvImplantValueL = (EditText) view.findViewById(R.id.tvImplantValueL);

            tvImplantValueD.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    implantList.get(getAdapterPosition()).setValueD(s.toString());
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
            tvImplantValueL.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    implantList.get(getAdapterPosition()).setValueL(s.toString());
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
        }
    }

    public ImplantAdapter(ArrayList<implantModel> implantList, Context context) {
        this.implantList = implantList;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.implant_item_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        implantModel movie = implantList.get(position);
        if (movie.getId() == 0) {
            holder.tvImplantId.setText("");
        } else {
            holder.tvImplantId.setText(movie.getId() + "");
        }

        holder.tvImplantValueD.setText(movie.getValueD());
        holder.tvImplantValueL.setText(movie.getValueL());
    }

   /* public interface OnItemClickListener {
        void onItemClick1();

    }*/

    @Override
    public int getItemCount() {
        return implantList.size();
    }
}