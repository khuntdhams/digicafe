package com.cab.digicafe.Adapter.mrp;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.cab.digicafe.Activities.mrp.Plan;
import com.cab.digicafe.Callback.OnLoadMoreListener;
import com.cab.digicafe.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by CSS on 02-11-2017.
 */

public class TaskForPlanningListAdapter extends RecyclerView.Adapter<TaskForPlanningListAdapter.MyViewHolder> {
    private ArrayList<Plan> catererList;

    private Context mContext;
    private boolean onBind;
    private OnLoadMoreListener mOnLoadMoreListener;

    public interface OnItemClickListener {
        void onEdit(int pos);

        void onDelete(int pos);
    }

    int editdelPos = -1;


    private OnItemClickListener listener;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvCategory)
        TextView tvCategory;

        @BindView(R.id.ivEdit)
        ImageView ivEdit;

        @BindView(R.id.ivDelete)
        ImageView ivDelete;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

        }
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }

    public TaskForPlanningListAdapter(ArrayList<Plan> moviesList, Context context, OnItemClickListener listener) {
        this.catererList = moviesList;
        this.mContext = context;
        this.listener = listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_task_list, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        Plan sp = catererList.get(position);

        holder.ivEdit.setVisibility(View.VISIBLE);
        holder.ivDelete.setVisibility(View.VISIBLE);

        if (position == editdelPos) {
            holder.ivEdit.setVisibility(View.INVISIBLE);
            holder.ivDelete.setVisibility(View.INVISIBLE);
        }

        holder.tvCategory.setText(sp.getTask_title());

        holder.ivEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                editdelPos = position;
                listener.onEdit(position);
            }
        });

        holder.ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editdelPos = position;
                listener.onDelete(position);
            }
        });


    }

    @Override
    public int getItemCount() {
        return catererList.size();
    }

    public void setitem(List<Plan> products) {
        editdelPos = -1;
        catererList = new ArrayList<>();
        catererList.addAll(products);
        notifyDataSetChanged();
    }


  /*  public void setFilter(List<CatalougeContent> countryModels) {
        catererList = new ArrayList<>();
        catererList.addAll(countryModels);
        notifyDataSetChanged();
    }*/


}
