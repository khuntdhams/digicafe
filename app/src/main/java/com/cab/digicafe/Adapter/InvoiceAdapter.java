package com.cab.digicafe.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cab.digicafe.Callback.OnLoadMoreListener;
import com.cab.digicafe.Database.SqlLiteDbHelper;
import com.cab.digicafe.Helper.SessionManager;
import com.cab.digicafe.Model.Catelog;
import com.cab.digicafe.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by CSS on 02-11-2017.
 */

public class InvoiceAdapter extends RecyclerView.Adapter<InvoiceAdapter.MyViewHolder> {
    private List<Catelog> catererList;
    static boolean isstoreopen = false;
    private Context mContext;
    private SqlLiteDbHelper dbHelper;
    SessionManager sessionManager;
    private OnLoadMoreListener mOnLoadMoreListener;

    public interface OnItemClickListener {
        void onItemClick(Catelog item);

        void onadditem(Catelog qty);

        void ondecreasecount(Catelog qty);
    }

    private OnItemClickListener listener;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_billamt, txt_sgst, tv_sgst, txt_cgst, tv_cgst, tv_total;


        public MyViewHolder(View view) {
            super(view);

            txt_sgst = (TextView) view.findViewById(R.id.txt_sgst);
            tv_billamt = (TextView) view.findViewById(R.id.tv_billamt);
            tv_sgst = (TextView) view.findViewById(R.id.tv_sgst);

            txt_cgst = (TextView) view.findViewById(R.id.txt_cgst);
            tv_cgst = (TextView) view.findViewById(R.id.tv_cgst);
            tv_total = (TextView) view.findViewById(R.id.tv_total);


        }
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }

    public InvoiceAdapter(List<Catelog> moviesList, Context context, OnItemClickListener listener) {
        this.catererList = moviesList;
        this.mContext = context;
        dbHelper = new SqlLiteDbHelper(context);
        sessionManager = new SessionManager(context);
        this.listener = listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_invoicesummery, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final Catelog movie = catererList.get(position);
        int billamt = Integer.parseInt(movie.getMrp());

    }

    @Override
    public int getItemCount() {
        return catererList.size();
    }

    public void setFilter(List<Catelog> countryModels) {
        catererList = new ArrayList<>();
        catererList.addAll(countryModels);
        notifyDataSetChanged();
    }

    public void setitem(List<Catelog> products, boolean isstoreopen) {
        this.isstoreopen = isstoreopen;
        catererList = new ArrayList<>();
        catererList.addAll(products);
        Log.e("aliassearch", catererList.size() + "");
        notifyDataSetChanged();

    }
}
