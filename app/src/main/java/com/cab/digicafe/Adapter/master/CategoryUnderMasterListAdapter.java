package com.cab.digicafe.Adapter.master;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cab.digicafe.Activities.RequestApis.MyResponse.CategoryViewModel;
import com.cab.digicafe.Callback.OnLoadMoreListener;
import com.cab.digicafe.Helper.LoadImg;
import com.cab.digicafe.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by CSS on 02-11-2017.
 */

public class CategoryUnderMasterListAdapter extends RecyclerView.Adapter<CategoryUnderMasterListAdapter.MyViewHolder> {
    private ArrayList<CategoryViewModel> catererList;

    private Context mContext;
    private boolean onBind;
    private OnLoadMoreListener mOnLoadMoreListener;

    public interface OnItemClickListener {
        void onItemClick(CategoryViewModel item);

        void onCheckChange(int pos, boolean isChecked);

        void onDrag(int firstPosition, int secondPosition);

    }

    private OnItemClickListener listener;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvCatNm)
        TextView tvCatNm;

        @BindView(R.id.tvCatNmCopy)
        TextView tvCatNmCopy;


        @BindView(R.id.tvSortBy)
        TextView tvSortBy;

        @BindView(R.id.cbDelete)
        public CheckBox cbDelete;

        @BindView(R.id.llBs)
        public LinearLayout ll_base;

        @BindView(R.id.llDrag)
        public LinearLayout llDrag;

        @BindView(R.id.rlCb)
        public RelativeLayout rlCb;

        @BindView(R.id.ivCatImg)
        ImageView ivCatImg;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

        }
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }

    public CategoryUnderMasterListAdapter(ArrayList<CategoryViewModel> moviesList, Context context, OnItemClickListener listener) {
        this.catererList = moviesList;
        this.mContext = context;
        this.listener = listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_ad_categoryundrmstr, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        CategoryViewModel sp = catererList.get(position);

        try {
            holder.tvCatNm.setText(sp.pricelistCategoryName);
            holder.tvCatNmCopy.setText(sp.pricelistCategoryName);
            holder.tvSortBy.setText(sp.sortBy);

        } catch (Exception e) {
            e.printStackTrace();
        }


        holder.rlCb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (catererList.get(position).isSelect) {
                    holder.cbDelete.setChecked(false);
                    catererList.get(position).isSelect = false;
                    listener.onCheckChange(position, false);
                } else {
                    catererList.get(position).isSelect = true;
                    holder.cbDelete.setChecked(true);
                    listener.onCheckChange(position, true);

                }


            }
        });

        if (sp.isSelect) holder.cbDelete.setChecked(true);
        else holder.cbDelete.setChecked(false);

        holder.ll_base.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onItemClick(catererList.get(position));
            }
        });

        if (sp.pricelist_category_image == null) {
            loadImg.loadCategoryImg(mContext, "", holder.ivCatImg);
        } else {
            loadImg.loadCategoryImg(mContext, sp.pricelist_category_image, holder.ivCatImg);
        }

    }

    LoadImg loadImg = new LoadImg();

    @Override
    public int getItemCount() {
        return catererList.size();
    }


    public void setitem(List<CategoryViewModel> products) {

        catererList = new ArrayList<>();
        catererList.addAll(products);
        Log.e("aliassearch", catererList.size() + "");
        notifyDataSetChanged();

    }

    public void remove(int position) {
        catererList.remove(position);
        notifyItemRemoved(position);
    }

    public void swap(int firstPosition, int secondPosition) {
       /* Collections.swap(catererList, firstPosition, secondPosition);
        Log.e("PositionFst", firstPosition+"");
        Log.e("PositionSec", secondPosition+"");
        notifyDataSetChanged();*/
        listener.onDrag(firstPosition, secondPosition);
    }


}
