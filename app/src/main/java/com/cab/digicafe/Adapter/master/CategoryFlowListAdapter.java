package com.cab.digicafe.Adapter.master;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cab.digicafe.Activities.RequestApis.MyResponse.CategoryViewModel;
import com.cab.digicafe.Callback.OnLoadMoreListener;
import com.cab.digicafe.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by CSS on 02-11-2017.
 */

public class CategoryFlowListAdapter extends RecyclerView.Adapter<CategoryFlowListAdapter.MyViewHolder> {
    private ArrayList<CategoryViewModel> catererList;

    private Context mContext;
    private boolean onBind;
    private OnLoadMoreListener mOnLoadMoreListener;

    public interface OnItemClickListener {
        void onItemClick(CategoryViewModel item);

        void onDrag();

    }

    private OnItemClickListener listener;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvCategory)
        TextView tvCategory;

        @BindView(R.id.llDrag)
        public LinearLayout llDrag;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

        }
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }

    public CategoryFlowListAdapter(ArrayList<CategoryViewModel> moviesList, Context context, OnItemClickListener listener) {
        this.catererList = moviesList;
        this.mContext = context;
        this.listener = listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_category, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        CategoryViewModel sp = catererList.get(position);

        holder.tvCategory.setText(sp.pricelistCategoryName);

        holder.tvCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });

        holder.tvCategory.setBackground(ContextCompat.getDrawable(mContext, R.drawable.roundcorner_clrstrokecolor));
        holder.tvCategory.setTextColor(ContextCompat.getColor(mContext, R.color.black));


      /*  if(sp.isChecked())
        {
            holder.tvCategory.setBackground(ContextCompat.getDrawable(mContext,R.drawable.roundcorner_color));
            holder.tvCategory.setTextColor(ContextCompat.getColor(mContext,R.color.white));
        }
        else {
            holder.tvCategory.setBackground(ContextCompat.getDrawable(mContext,R.drawable.roundcorner_clrstrokecolor));
            holder.tvCategory.setTextColor(ContextCompat.getColor(mContext,R.color.black));
        }
*/


    }

    @Override
    public int getItemCount() {
        return catererList.size();
    }

    public void setitem(List<CategoryViewModel> products) {
        catererList = new ArrayList<>();
        catererList.addAll(products);
        notifyDataSetChanged();
    }


  /*  public void setFilter(List<CatalougeContent> countryModels) {
        catererList = new ArrayList<>();
        catererList.addAll(countryModels);
        notifyDataSetChanged();
    }*/


    public void remove(int position) {
        catererList.remove(position);
        notifyItemRemoved(position);
    }

    public void swap(int firstPosition, int secondPosition) {
        Collections.swap(catererList, firstPosition, secondPosition);
        notifyItemMoved(firstPosition, secondPosition);
    }

    public void onSwap() {

        listener.onDrag();
    }


}
