package com.cab.digicafe.Adapter.taxGroup;

import java.util.ArrayList;

public class TaxGroupModel {

    public Double taxVal = 0.0;
    public String taxKey = "";
    public String taxValPercentage = "";
    ArrayList<TaxGroupModel> alTax = new ArrayList<>();

    public TaxGroupModel(Double taxVal, String taxKey, String taxValPercentage) {

        this.taxVal = taxVal;
        this.taxKey = taxKey;
        this.taxValPercentage = taxValPercentage;


    }
}
