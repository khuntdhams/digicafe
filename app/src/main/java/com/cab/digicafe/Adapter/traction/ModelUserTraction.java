package com.cab.digicafe.Adapter.traction;

import java.util.Date;

public class ModelUserTraction {

    public Integer total_visited = 1;
    public String consumer_bridge_id = "", business_bridge_id = "", field_json_data = "",
            created_date = "", updated_date = "";
    public String consumer_username = "", business_username = "", field_json_data_of_shop;
    public Date date = new Date();

    public ModelUserTraction(int click_ctr, String uBridgeId, String shopBridgeId, String uNm, String shopNm, String field_json_data, String field_json_data_of_shop) {
        this.total_visited = click_ctr;
        this.consumer_bridge_id = uBridgeId;
        this.business_bridge_id = shopBridgeId;
        this.consumer_username = uNm;
        this.business_username = shopNm;
        this.field_json_data = field_json_data; // pass post
        this.field_json_data_of_shop = field_json_data_of_shop;
    }

}
