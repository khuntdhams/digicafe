package com.cab.digicafe.Adapter.settings;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.provider.MediaStore;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.cab.digicafe.Database.SqlLiteDbHelper;
import com.cab.digicafe.Model.ModelFile;
import com.cab.digicafe.R;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by CSS on 02-11-2017.
 */

public class SampleImgInCartAdapter extends RecyclerView.Adapter<SampleImgInCartAdapter.MyViewHolder> {
    private List<ModelFile> data;
    private Context mContext;
    private SqlLiteDbHelper dbHelper;
    boolean isvdo = false;
    boolean isdrag = false;
    boolean isedit = false;

    public interface OnItemClickListener {
        void onItemClick(String item);

        void onDelete(int pos, ModelFile modelFile);

        void onClickString(String pos);

        void onImgClick(List<ModelFile> data, int pos);

        void onAddEditInfo(ModelFile item, int pos);

    }

    private OnItemClickListener listener;

    public class MyViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.iv_img)
        ImageView iv_img;

        @BindView(R.id.iv_del)
        ImageView iv_del;

        @BindView(R.id.iv_play)
        ImageView iv_play;

        @BindView(R.id.rlLocInfo)
        RelativeLayout rlLocInfo;

        @BindView(R.id.tv_stroke)
        public TextView tv_stroke;


        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }


    public SampleImgInCartAdapter(List<ModelFile> moviesList, Context context, OnItemClickListener listener, boolean isvdo, boolean isedit, boolean isdrag) {
        this.data = moviesList;
        this.mContext = context;
        this.isvdo = isvdo;
        this.listener = listener;
        dbHelper = new SqlLiteDbHelper(context);
        this.isedit = isedit;
        this.isdrag = isdrag;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_sample_cart, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        ModelFile modelFile = data.get(position);


        if (isvdo) {
            Bitmap bmThumbnail;
            bmThumbnail = ThumbnailUtils.createVideoThumbnail(modelFile.getImgpath(), MediaStore.Video.Thumbnails.MINI_KIND);

            holder.iv_img.setImageDrawable(null);
            holder.iv_img.setImageBitmap(bmThumbnail);
            holder.iv_play.setVisibility(View.VISIBLE);

        } else {

            if (modelFile.isIsoffline()) {
                Glide.with(mContext).load(new File(modelFile.getImgpath())).diskCacheStrategy(DiskCacheStrategy.ALL).placeholder(R.drawable.defaultplace).into(holder.iv_img);

            } else {
                Glide.with(mContext).load(modelFile.getImgpath()).diskCacheStrategy(DiskCacheStrategy.ALL).placeholder(R.drawable.defaultplace).into(holder.iv_img);

            }
            holder.iv_play.setVisibility(View.GONE);

        }

       /* holder.iv_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // listener.onImgClick(data,position);

             *//*   ArrayList<String> al_img = new ArrayList<>();

                try {
                    for (int i = 0; i < data.size(); i++) {
                        al_img.add(data.get(i).getImgpath().toString());

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                GalleryActivity.images = al_img;
                Intent i = new Intent(mContext, GalleryActivity.class);
                mContext.startActivity(i);
*//*

            }
        });*/

        holder.rlLocInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //ModelFile model = data.get(position);
                listener.onImgClick(data, position);


            }
        });


        holder.iv_del.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onDelete(position, data.get(position));
            }
        });

        /*if(modelFile.getDescription()!=null&&modelFile.getDescription().length>0)
        {
            String[] desc = modelFile.getDescription();
            String dec =desc[0];
            for (int i=1;i<desc.length;i++)
            {
                dec = dec +", "+ desc[i];
                holder.tv_desc.setText(dec);
            }

        }

*/

        /*if (modelFile.isIsdetect()) {
        }*/


        holder.iv_del.setVisibility(View.GONE);
        if (isedit) holder.iv_del.setVisibility(View.VISIBLE);

        if (isdrag) holder.tv_stroke.setVisibility(View.VISIBLE);


    }

    public void setItem(ArrayList<ModelFile> path) {
        data = new ArrayList<>();
        data.addAll(path);
        notifyDataSetChanged();

    }

    public void assignItem(ArrayList<ModelFile> path) {
        data = new ArrayList<>();
        data = path;
        notifyDataSetChanged();

    }


    @Override
    public int getItemCount() {
        return data.size();
    }


    public void remove(int position) {
        data.remove(position);
        notifyItemRemoved(position);
    }

    public void swap(int firstPosition, int secondPosition) {

        Collections.swap(data, firstPosition, secondPosition);
        notifyItemMoved(firstPosition, secondPosition);
        listener.onItemClick("");
    }

}
