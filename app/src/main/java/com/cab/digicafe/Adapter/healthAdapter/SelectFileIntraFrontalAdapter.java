package com.cab.digicafe.Adapter.healthAdapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.cab.digicafe.Adapter.DetectWordsAdapter;
import com.cab.digicafe.Database.SqlLiteDbHelper;
import com.cab.digicafe.Helper.LoadImg;
import com.cab.digicafe.Model.ModelFile;
import com.cab.digicafe.R;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by CSS on 02-11-2017.
 */

public class SelectFileIntraFrontalAdapter extends RecyclerView.Adapter<SelectFileIntraFrontalAdapter.MyViewHolder> {
    private List<ModelFile> data;
    private Context mContext;
    private SqlLiteDbHelper dbHelper;
    boolean isvdo = false;
    boolean isdrag = false;
    DetectWordsAdapter detectWordsAdapter;
    ArrayList<ModelFile> al_detect = new ArrayList<>();
    boolean isedit = false;

    public interface OnItemClickListener {
        void onItemClick(String item);

        void onDelete(int pos);

        void onClickString(String pos);

        void onImgClick(List<ModelFile> data, int pos);

    }

    private OnItemClickListener listener;

    public class MyViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.iv_img)
        ImageView iv_img;

        @BindView(R.id.iv_del)
        ImageView iv_del;

        @BindView(R.id.iv_play)
        ImageView iv_play;

        @BindView(R.id.et_desc)
        EditText tv_desc;

        @BindView(R.id.tv_stroke)
        public TextView tv_stroke;

        @BindView(R.id.rv_word)
        RecyclerView rv_word;


        public MyViewHolder(View view) {
            super(view);

            ButterKnife.bind(this, view);

        }
    }


    public SelectFileIntraFrontalAdapter(List<ModelFile> moviesList, Context context, OnItemClickListener listener, boolean isvdo, boolean isedit, boolean isdrag) {
        this.data = moviesList;
        this.mContext = context;
        this.isvdo = isvdo;
        this.listener = listener;
        dbHelper = new SqlLiteDbHelper(context);
        this.isedit = isedit;
        this.isdrag = isdrag;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_select_media, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        ModelFile modelFile = data.get(position);

        if (isedit) {
            holder.tv_desc.setVisibility(View.GONE);
        } else {
            holder.tv_desc.setVisibility(View.GONE);
        }
        holder.iv_play.setVisibility(View.GONE);

        if (isvdo) {
            Bitmap bmThumbnail;
            bmThumbnail = ThumbnailUtils.createVideoThumbnail(modelFile.getImgpath(), MediaStore.Video.Thumbnails.MINI_KIND);

            holder.iv_img.setImageDrawable(null);
            holder.iv_img.setImageBitmap(bmThumbnail);
            holder.iv_play.setVisibility(View.VISIBLE);

        } else {


            if (modelFile.getImgpath().contains("youtube")) {

                holder.iv_play.setVisibility(View.VISIBLE);

                Uri uri = Uri.parse(modelFile.getImgpath());
                //Set<String> args = uri.getQueryParameterNames();
                String v = uri.getQueryParameter("v");

                String url = "" + mContext.getString(R.string.utube_base) + v + mContext.getString(R.string.utube_end);
                Log.e("okhttp utube", url + "");

                loadImg.loadCategoryImg(mContext, url, holder.iv_img);

            } else if (modelFile.isIsoffline()) {
                Glide.with(mContext).load(new File(modelFile.getImgpath())).diskCacheStrategy(DiskCacheStrategy.ALL).placeholder(R.drawable.defaultplace).into(holder.iv_img);

            } else {
                Glide.with(mContext).load(modelFile.getImgpath()).diskCacheStrategy(DiskCacheStrategy.ALL).placeholder(R.drawable.defaultplace).into(holder.iv_img);

            }


        }

        holder.iv_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                listener.onImgClick(data, position);

             /*   ArrayList<String> al_img = new ArrayList<>();

                try {
                    for (int i = 0; i < data.size(); i++) {
                        al_img.add(data.get(i).getImgpath().toString());

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                GalleryActivity.images = al_img;
                Intent i = new Intent(mContext, GalleryActivity.class);
                mContext.startActivity(i);
*/


            }
        });

        holder.iv_del.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onDelete(position);
            }
        });

        /*if(modelFile.getDescription()!=null&&modelFile.getDescription().length>0)
        {
            String[] desc = modelFile.getDescription();
            String dec =desc[0];
            for (int i=1;i<desc.length;i++)
            {
                dec = dec +", "+ desc[i];
                holder.tv_desc.setText(dec);
            }

        }

*/
        holder.tv_desc.setText(modelFile.getDetctstring());

        /*if (modelFile.isIsdetect()) {
        }*/

        al_detect = new ArrayList<>();

        detectWordsAdapter = new DetectWordsAdapter(al_detect, mContext, new DetectWordsAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(String item, int pos) {
                ArrayList<ModelFile> al_data = new ArrayList<>();
                al_data.addAll(data);
                al_data.get(position).setDetctstring(item);
                setItem(al_data);
                listener.onClickString(item);
            }

            @Override
            public void onDelete(int pos) {


            }
        }, false);


        LinearLayoutManager mLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        holder.rv_word.setLayoutManager(mLayoutManager);
        holder.rv_word.setAdapter(detectWordsAdapter);
        al_detect = modelFile.getAl_detect();
        detectWordsAdapter.setItem(al_detect);
        holder.rv_word.setNestedScrollingEnabled(false);


        if (isdrag) holder.tv_stroke.setVisibility(View.VISIBLE);

        if (al_detect.size() > 0) holder.rv_word.setVisibility(View.VISIBLE);
        else holder.rv_word.setVisibility(View.GONE);

    }

    LoadImg loadImg = new LoadImg();

    public void setItem(ArrayList<ModelFile> path) {
        data = new ArrayList<>();
        data.addAll(path);
        notifyDataSetChanged();

    }

    public void assignItem(ArrayList<ModelFile> path) {
        data = new ArrayList<>();
        data = path;
        notifyDataSetChanged();

    }


    @Override
    public int getItemCount() {
        return data.size();
    }


    public void remove(int position) {
        data.remove(position);
        notifyItemRemoved(position);
    }

    public void swap(int firstPosition, int secondPosition) {

        Collections.swap(data, firstPosition, secondPosition);
        notifyItemMoved(firstPosition, secondPosition);
        listener.onItemClick("");
    }

}
