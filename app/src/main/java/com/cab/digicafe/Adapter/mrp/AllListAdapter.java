package com.cab.digicafe.Adapter.mrp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cab.digicafe.Activities.RequestApis.MyRequestCall;
import com.cab.digicafe.Activities.RequestApis.MyRequst;
import com.cab.digicafe.Activities.mrp.AnimDialogueWithField;
import com.cab.digicafe.Activities.mrp.PlanningDialogue;
import com.cab.digicafe.ChitlogActivity;
import com.cab.digicafe.Helper.Inad;
import com.cab.digicafe.Helper.SessionManager;
import com.cab.digicafe.Model.ShareModel;
import com.cab.digicafe.MyCustomClass.JsonObjParse;
import com.cab.digicafe.R;
import com.google.gson.JsonObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class AllListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    ArrayList<TaskPrefViewModel> alUserTraction;
    private Context mContext;
    OnItemClickListener onItemClickListener;

    public interface OnItemClickListener {
        void onDelete(int item);

        void onEdit(int item);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.ivModifyCost)
        ImageView ivModifyCost;

        @BindView(R.id.ivPaymentComment)
        ImageView ivPaymentComment;

        @BindView(R.id.tvFromNm)
        TextView tvFromNm;

        @BindView(R.id.tvPurposeOrOtp)
        TextView tvPurposeOrOtp;

        @BindView(R.id.tv_original_total_chit_item_value)
        TextView tv_original_total_chit_item_value;

        @BindView(R.id.tvOriginalEditDelDtFromOrTo)
        TextView tvOriginalEditDelDtFromOrTo;

        @BindView(R.id.tvRefId)
        TextView tvRefId;

        @BindView(R.id.tvCaseId)
        TextView tvCaseId;

        @BindView(R.id.tv_chit_item_count)
        TextView tv_chit_item_count;

        @BindView(R.id.tv_total_chit_item_value)
        TextView tv_total_chit_item_value;

        @BindView(R.id.tv_transaction_status)
        TextView tv_transaction_status;

        @BindView(R.id.tv_location)
        TextView tv_location;

        @BindView(R.id.tvEditDelDtFromOrTo)
        TextView tvEditDelDtFromOrTo;

        @BindView(R.id.llMrpView)
        LinearLayout llMrpView;

        @BindView(R.id.llBottomView)
        LinearLayout llBottomView;

        @BindView(R.id.tvRecordType)
        TextView tvRecordType;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public class MyViewHolderPlanning extends RecyclerView.ViewHolder {

        @BindView(R.id.tvRecordType)
        TextView tvRecordType;

        @BindView(R.id.tvActAmt)
        TextView tvActAmt;

        @BindView(R.id.tvPlanAmt)
        TextView tvPlanAmt;

        @BindView(R.id.tvTaskStatus)
        TextView tvTaskStatus;

        @BindView(R.id.tvFromNm)
        TextView tvFromNm;

        @BindView(R.id.tvEditDelDtFromOrTo)
        TextView tvEditDelDtFromOrTo;

        @BindView(R.id.tvActualDate)
        TextView tvActualDate;

        @BindView(R.id.llMrpView)
        LinearLayout llMrpView;

        @BindView(R.id.llBottomView)
        LinearLayout llBottomView;

        public MyViewHolderPlanning(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public AllListAdapter(ArrayList<TaskPrefViewModel> moviesList, Context context, OnItemClickListener onItemClickListener) {
        this.alUserTraction = moviesList;
        this.onItemClickListener = onItemClickListener;
        this.mContext = context;
        //setHasStableIds(true);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_ad_mrp, parent, false);

        if (viewType == 0) // // MAT, RES
        {
            itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_ad_mrp, parent, false);
            return new MyViewHolder(itemView);
        } else if (viewType == 1) // //Plan
        {
            itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_ad_planning, parent, false);
            return new MyViewHolderPlanning(itemView);
        }

        return null;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holderBase, final int position) {

        if (holderBase.getItemViewType() == 0) // // MAT, RES
        {

            final MyViewHolder holder = (MyViewHolder) holderBase;
            holder.llBottomView.setVisibility(View.GONE);
            if (position == alUserTraction.size() - 1) {
                holder.llBottomView.setVisibility(View.VISIBLE);
            }

            TaskPrefViewModel productTotalDiscount = alUserTraction.get(position);
            holder.tvRecordType.setText("M");
            holder.tvRecordType.setVisibility(View.VISIBLE);
            holder.tvRecordType.setBackground(ContextCompat.getDrawable(mContext, R.drawable.view_mat_sq));
            if (productTotalDiscount.getTaskPurpose().equalsIgnoreCase("Resource")) {
                holder.tvRecordType.setText("R");
                holder.tvRecordType.setBackground(ContextCompat.getDrawable(mContext, R.drawable.view_res_circle));

            }

            holder.tvRefId.setText("");
            holder.tvCaseId.setText("");

            if (ShareModel.getI().isFromAllTask) {
                if (productTotalDiscount.getToRefId().length() > 1) // "0"
                {
                    holder.tvRefId.setText(productTotalDiscount.getToRefId() + " / ");
                    holder.tvCaseId.setText(productTotalDiscount.getToCaseId());
                }
            } else {
                holder.tvRefId.setText(productTotalDiscount.getFromRefId() + " / ");
                holder.tvCaseId.setText(productTotalDiscount.getFromCaseId());
            }


            holder.tvRefId.setEnabled(false);
            holder.tvRefId.setTextColor(ContextCompat.getColor(mContext, R.color.black));
            holder.tvRefId.setPaintFlags(holder.tvRefId.getPaintFlags() & (~Paint.UNDERLINE_TEXT_FLAG));
            holder.tvRefId.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    try {
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(holder.tvRefId.getText().toString()));
                        mContext.startActivity(browserIntent);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }
            });

            holder.llMrpView.setBackgroundColor(ContextCompat.getColor(mContext, R.color.mrp_link));
            holder.ivModifyCost.setVisibility(View.VISIBLE);
            holder.ivPaymentComment.setVisibility(View.VISIBLE);
            //holder.llMrpView.setVisibility(View.VISIBLE);

            String MRP_TAB = productTotalDiscount.getToFieldJsonData();
            if (!MRP_TAB.equalsIgnoreCase("") && MRP_TAB.length() > 2) { // {}
                try {

                    String forWhich = productTotalDiscount.getFromFieldJsonData();

                    if (ShareModel.getI().isFromAllTask) {
                        //forWhich = JsonObjParse.getValueEmpty(jsonObject.toString(), "To");
                        forWhich = productTotalDiscount.getToFieldJsonData();
                    } else {
                        //forWhich = JsonObjParse.getValueEmpty(jsonObject.toString(), "From");
                        forWhich = productTotalDiscount.getFromFieldJsonData();
                    }

                    //String link_status = JsonObjParse.getValueEmpty(forWhich, "status");
                    String link_status = productTotalDiscount.getStatus();

                    switch (link_status.toLowerCase()) {
                        case "link":
                            holder.llMrpView.setBackgroundColor(ContextCompat.getColor(mContext, R.color.mrp_link));
                            break;
                        case "delink":
                            holder.ivModifyCost.setVisibility(View.INVISIBLE);
                            holder.ivPaymentComment.setVisibility(View.INVISIBLE);
                            holder.llMrpView.setBackgroundColor(ContextCompat.getColor(mContext, R.color.mrp_dlink));
                            break;
                        case "complete":
                            holder.llMrpView.setBackgroundColor(ContextCompat.getColor(mContext, R.color.mrp_complete));
                            break;
                        case "issue":
                            holder.llMrpView.setBackgroundColor(ContextCompat.getColor(mContext, R.color.mrp_issue));
                            break;
                        case "deleted":
                            //holder.llMrpView.setVisibility(View.GONE);
                            holder.llMrpView.setBackgroundColor(ContextCompat.getColor(mContext, R.color.mrp_issue));
                            break;
                        default:
                            break;
                    }

                    String subject = JsonObjParse.getValueEmpty(forWhich, "subject");
                    holder.tvFromNm.setText(subject + "");

               /*
                String purpose = JsonObjParse.getValueEmpty(forWhich, "purpose");
                holder.tvPurposeOrOtp.setText(purpose + " / " + "OTP");

                String ref_id = JsonObjParse.getValueEmpty(forWhich, "ref_id");
                String case_id = JsonObjParse.getValueEmpty(forWhich, "case_id");
                holder.tvRefId.setText(ref_id);
                holder.tvCaseId.setText(case_id);

                String chit_item_count = JsonObjParse.getValueEmpty(forWhich, "chit_item_count");
                holder.tv_chit_item_count.setText("# " + chit_item_count);

                String transaction_status = JsonObjParse.getValueEmpty(forWhich, "transaction_status");
                holder.tv_transaction_status.setText(transaction_status);

                String location = JsonObjParse.getValueEmpty(forWhich, "location");
                holder.tv_location.setText(location);*/

                    // Date , Amount

                    if (productTotalDiscount.getToPurpose().equalsIgnoreCase("external")) {
                        String receipt_number = JsonObjParse.getValueEmpty(forWhich, "receipt_number");
                        holder.tvRefId.setText(receipt_number);
                        if (receipt_number.startsWith("http://") || receipt_number.startsWith("https://")) {
                            holder.tvRefId.setEnabled(true);
                            holder.tvRefId.setTextColor(ContextCompat.getColor(mContext, R.color.link));
                            holder.tvRefId.setPaintFlags(holder.tvRefId.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                        }
                    }

                    String EDIT_DATE = JsonObjParse.getValueEmpty(forWhich, "EDIT_DATE");

                    holder.tvEditDelDtFromOrTo.setText(EDIT_DATE + "");

                    String DATE = JsonObjParse.getValueEmpty(forWhich, "DATE");

                    holder.tvOriginalEditDelDtFromOrTo.setText(DATE + "");

                    holder.tvOriginalEditDelDtFromOrTo.setVisibility(View.VISIBLE);
                    if (EDIT_DATE.equalsIgnoreCase(DATE)) {
                        holder.tvOriginalEditDelDtFromOrTo.setVisibility(View.GONE);
                    }


                    String edit_total_chit_item_value = JsonObjParse.getValueEmpty(forWhich, "edit_total_chit_item_value");
                    Double modify_amount = 0.0, original_amt = 0.0;
                    if (!edit_total_chit_item_value.isEmpty()) {
                        modify_amount = Double.valueOf(edit_total_chit_item_value);

                        String symbol_native = JsonObjParse.getValueEmpty(forWhich, "symbol_native") + " ";
                        holder.tv_total_chit_item_value.setText(symbol_native + "" + Inad.getCurrencyDecimal(modify_amount, mContext));

                    }
                    String total_chit_item_value = JsonObjParse.getValueEmpty(forWhich, "total_chit_item_value");
                    if (!total_chit_item_value.isEmpty()) {
                        original_amt = Double.valueOf(total_chit_item_value);

                        String symbol_native = JsonObjParse.getValueEmpty(forWhich, "symbol_native") + " ";
                        holder.tv_original_total_chit_item_value.setText(symbol_native + "" + Inad.getCurrencyDecimal(original_amt, mContext));

                    }

                    holder.tv_original_total_chit_item_value.setVisibility(View.VISIBLE);
                    if (modify_amount.doubleValue() == original_amt.doubleValue()) {
                        holder.tv_original_total_chit_item_value.setVisibility(View.GONE);
                    }

                    holder.tv_original_total_chit_item_value.setPaintFlags(holder.tv_original_total_chit_item_value.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                    holder.tvOriginalEditDelDtFromOrTo.setPaintFlags(holder.tvOriginalEditDelDtFromOrTo.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);


                } catch (Exception e) {
                    e.printStackTrace();
                }


            }


            holder.ivModifyCost.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    ShareModel.getI().taskPrefViewModel = alUserTraction.get(position);

                    AnimDialogueWithField commonConfirmDialogue = new AnimDialogueWithField(mContext, new AnimDialogueWithField.OnDialogClickListener() {
                        @Override
                        public void onDialogImageRunClick(JsonObject joMrp) {

                            MyRequestCall myRequestCall = new MyRequestCall();
                            MyRequst myRequst = new MyRequst();
                            myRequst.cookie = new SessionManager().getsession();

                            myRequestCall.editTaskPreferencr(mContext, myRequst, joMrp, new MyRequestCall.CallRequest() {
                                @Override
                                public void onGetResponse(String response) {
                                    if (!response.isEmpty()) {
                                        onItemClickListener.onEdit(0);
                                    }
                                }
                            });


                        }
                    }, " Delivery details", mContext.getString(R.string.del_details));


                    commonConfirmDialogue.show();


                }
            });

            holder.ivPaymentComment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    ShareModel.getI().taskPrefViewModel = alUserTraction.get(position);

                    AnimDialogueWithField commonConfirmDialogue = new AnimDialogueWithField(mContext, new AnimDialogueWithField.OnDialogClickListener() {
                        @Override
                        public void onDialogImageRunClick(JsonObject joMrp) {
                            onItemClickListener.onEdit(0);

                        }
                    }, " Payment details", mContext.getString(R.string.pay_details));
                    commonConfirmDialogue.show();

                }
            });


            holder.llMrpView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    if (alUserTraction.get(position).getToPurpose().equalsIgnoreCase("external")) {
                        return;
                    }

                    if (alUserTraction.get(position).getToPurpose().equalsIgnoreCase("Resource")) {
                        return;
                    }

                    MyRequst myRequst = new MyRequst();

                    myRequst.search = "" + holder.tvCaseId.getText().toString().trim();

                    try {

                        if (ShareModel.getI().isFromAllTask && myRequst.search.isEmpty()) {
                            if (ShareModel.getI().isFromAllTask) {
                                Inad.alerterInfo("Message", "Please update it from order to see details", (Activity) mContext);
                            } else { // from order
                                Inad.alerterInfo("Message", "Please update it to see details", (Activity) mContext);
                                holder.ivModifyCost.performClick();
                            }
                            return;
                        }


                        if (ShareModel.getI().isFromAllTask) {
                            myRequst.to_chit_hash_id = alUserTraction.get(position).getToChitHashId();
                            myRequst.chit_id = alUserTraction.get(position).getToChitId();
                        } else {
                            myRequst.to_chit_hash_id = alUserTraction.get(position).getFromChitHashId();
                            myRequst.chit_id = alUserTraction.get(position).getFromChitId();
                        }

                        ((ChitlogActivity) mContext).callOdrOrTask(myRequst);


                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

            });


            holder.llMrpView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {

                    ShareModel.getI().taskPrefViewModel = alUserTraction.get(position);
                    AnimDialogueWithField commonConfirmDialogue = new AnimDialogueWithField(mContext, new AnimDialogueWithField.OnDialogClickListener() {
                        @Override
                        public void onDialogImageRunClick(JsonObject joMrp) {
                            onItemClickListener.onEdit(0);
                        }
                    }, " Change status", mContext.getString(R.string.status_selection));
                    commonConfirmDialogue.show();
                    return true;

                }

            });


        } else if (holderBase.getItemViewType() == 1) { // PLANING


            final MyViewHolderPlanning holder = (MyViewHolderPlanning) holderBase;

            holder.llBottomView.setVisibility(View.GONE);
            if (position == alUserTraction.size() - 1) {
                holder.llBottomView.setVisibility(View.VISIBLE);
            }
            holder.tvRecordType.setBackground(ContextCompat.getDrawable(mContext, R.drawable.ic_triangle));
            holder.tvRecordType.setVisibility(View.VISIBLE);
            holder.tvRecordType.setText("P");

            TaskPrefViewModel productTotalDiscount = alUserTraction.get(position);


            holder.tvActAmt.setVisibility(View.VISIBLE);
            holder.tvPlanAmt.setVisibility(View.VISIBLE);
            holder.llMrpView.setBackgroundColor(ContextCompat.getColor(mContext, R.color.mrp_link));

            //holder.llMrpView.setVisibility(View.VISIBLE);

            String MRP_TAB = productTotalDiscount.getToFieldJsonData();
            if (!MRP_TAB.equalsIgnoreCase("") && MRP_TAB.length() > 2) { // {}
                try {

                    String forWhich = productTotalDiscount.getFromFieldJsonData();

                    if (ShareModel.getI().isFromAllTask) {
                        //forWhich = JsonObjParse.getValueEmpty(jsonObject.toString(), "To");
                        forWhich = productTotalDiscount.getToFieldJsonData();
                    } else {
                        //forWhich = JsonObjParse.getValueEmpty(jsonObject.toString(), "From");
                        forWhich = productTotalDiscount.getFromFieldJsonData();
                    }

                    //String link_status = JsonObjParse.getValueEmpty(forWhich, "status");
                    String link_status = productTotalDiscount.getStatus();

                    switch (link_status.toLowerCase()) {
                        case "link":
                            holder.llMrpView.setBackgroundColor(ContextCompat.getColor(mContext, R.color.mrp_link));
                            break;
                        case "delink":
                            holder.llMrpView.setBackgroundColor(ContextCompat.getColor(mContext, R.color.mrp_dlink));
                            break;
                        case "complete":
                            holder.llMrpView.setBackgroundColor(ContextCompat.getColor(mContext, R.color.mrp_complete));
                            break;
                        case "issue":
                            holder.llMrpView.setBackgroundColor(ContextCompat.getColor(mContext, R.color.mrp_issue));
                            break;
                        case "deleted":
                            //holder.llMrpView.setVisibility(View.GONE);
                            holder.llMrpView.setBackgroundColor(ContextCompat.getColor(mContext, R.color.mrp_issue));
                            break;
                        default:
                            break;
                    }

                    String planning = JsonObjParse.getValueEmpty(forWhich, "planning");

                    String activity_title = JsonObjParse.getValueEmpty(planning, "task_title");
                    holder.tvFromNm.setText(activity_title + "");

               /*
                String purpose = JsonObjParse.getValueEmpty(forWhich, "purpose");
                holder.tvPurposeOrOtp.setText(purpose + " / " + "OTP");

                String ref_id = JsonObjParse.getValueEmpty(forWhich, "ref_id");
                String case_id = JsonObjParse.getValueEmpty(forWhich, "case_id");
                holder.tvRefId.setText(ref_id);
                holder.tvCaseId.setText(case_id);

                String chit_item_count = JsonObjParse.getValueEmpty(forWhich, "chit_item_count");
                holder.tv_chit_item_count.setText("# " + chit_item_count);

                String transaction_status = JsonObjParse.getValueEmpty(forWhich, "transaction_status");
                holder.tv_transaction_status.setText(transaction_status);

                String location = JsonObjParse.getValueEmpty(forWhich, "location");
                holder.tv_location.setText(location);*/

                    // Date , Amount


                    String plan_start_date = JsonObjParse.getValueEmpty(planning, "plan_start_date");
                    String plan_end_date = JsonObjParse.getValueEmpty(planning, "plan_end_date");
                    String plan = "Plan : " + plan_start_date + " - " + plan_end_date;
                    holder.tvEditDelDtFromOrTo.setText(plan + "");


                    String act_end_date = JsonObjParse.getValueEmpty(planning, "act_end_date");
                    String act_start_date = JsonObjParse.getValueEmpty(planning, "act_start_date");
                    String act = "Actual : " + act_start_date + " - " + act_end_date;
                    holder.tvActualDate.setText(act + "");

                    String plan_amount = JsonObjParse.getValueEmpty(planning, "plan_amount");
                    String act_amount = JsonObjParse.getValueEmpty(planning, "act_amount");


                    Double plan_amount_val = 0.0;
                    if (!plan_amount.isEmpty()) {
                        plan_amount_val = Double.valueOf(plan_amount);
                        String symbol_native = JsonObjParse.getValueEmpty(forWhich, "symbol_native") + " ";
                        holder.tvPlanAmt.setText(symbol_native + "" + Inad.getCurrencyDecimal(plan_amount_val, mContext));
                    } else {
                        holder.tvPlanAmt.setVisibility(View.GONE);
                    }

                    holder.tvPlanAmt.setPaintFlags(holder.tvPlanAmt.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));


                    Double act_amount_val = 0.0;
                    if (!act_amount.isEmpty()) {
                        holder.tvPlanAmt.setPaintFlags(holder.tvPlanAmt.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                        act_amount_val = Double.valueOf(act_amount);
                        String symbol_native = JsonObjParse.getValueEmpty(forWhich, "symbol_native") + " ";
                        holder.tvActAmt.setText(symbol_native + "" + Inad.getCurrencyDecimal(act_amount_val, mContext));
                    } else {
                        holder.tvActAmt.setVisibility(View.GONE);
                    }


                    String task_status = JsonObjParse.getValueEmpty(planning, "task_status");
                    holder.tvTaskStatus.setText(task_status + "");

                    Drawable mDrawable = mContext.getResources().getDrawable(R.drawable.fill_round);

                    switch (task_status.toLowerCase()) {
                        case "planned":
                            mDrawable.setColorFilter(new PorterDuffColorFilter(ContextCompat.getColor(mContext, R.color.status1), PorterDuff.Mode.MULTIPLY));
                            break;
                        case "doing":
                            mDrawable.setColorFilter(new PorterDuffColorFilter(ContextCompat.getColor(mContext, R.color.status2), PorterDuff.Mode.MULTIPLY));
                            break;
                        case "done":
                            mDrawable.setColorFilter(new PorterDuffColorFilter(ContextCompat.getColor(mContext, R.color.status3), PorterDuff.Mode.MULTIPLY));
                            break;
                        default:
                            break;

                    }

                    holder.tvTaskStatus.setBackground(mDrawable);
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }


            holder.llMrpView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    ShareModel.getI().taskPrefViewModel = alUserTraction.get(position);

                    PlanningDialogue planningDialogue = new PlanningDialogue(mContext, new PlanningDialogue.OnDialogClickListener() {
                        @Override
                        public void onDialogImageRunClick(int joMrp) {

                            if (joMrp == 2) {
                                onItemClickListener.onEdit(0);
                            }


                        }
                    }, " Update sub-task", mContext.getString(R.string.planning));

                    planningDialogue.show();


                }

            });


            holder.llMrpView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {

                    ShareModel.getI().taskPrefViewModel = alUserTraction.get(position);
                    AnimDialogueWithField commonConfirmDialogue = new AnimDialogueWithField(mContext, new AnimDialogueWithField.OnDialogClickListener() {
                        @Override
                        public void onDialogImageRunClick(JsonObject joMrp) {
                            onItemClickListener.onEdit(0);
                        }
                    }, " Change status", mContext.getString(R.string.status_selection));
                    commonConfirmDialogue.show();
                    return true;

                }

            });


        }


        switch (holderBase.getItemViewType()) {
            case 0: // MAT, RES


            case 1:

        }


    }

    @Override
    public int getItemCount() {
        return alUserTraction.size();
    }

    public void setItem(ArrayList<TaskPrefViewModel> path) {
        alUserTraction = new ArrayList<>();
        alUserTraction.addAll(path);
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        // Just as an example, return 0 or 2 depending on position
        // Note that unlike in ListView adapters, types don't have to be contiguous


        String task_purpose = alUserTraction.get(position).getTaskPurpose();
        if (task_purpose.equalsIgnoreCase("Planning")) {
            return 1;
        } else {
            return 0;
        }


    }

    public int MAT_RES_VT = 0;
    public int PLAN_VT = 1;


}
