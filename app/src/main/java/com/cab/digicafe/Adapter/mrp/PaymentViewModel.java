package com.cab.digicafe.Adapter.mrp;

public class PaymentViewModel {

    public String payment_reference = "", payment_amount = "", payment_date = "";
    boolean isSelect;


    public PaymentViewModel() {
    }

    public PaymentViewModel(String payment_reference, String amount, String date) {
        this.payment_reference = payment_reference;
        this.payment_amount = amount;
        this.payment_date = date;
    }
}
