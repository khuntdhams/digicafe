package com.cab.digicafe.Adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cab.digicafe.Callback.OnLoadMoreListener;
import com.cab.digicafe.Model.SupplierCategory;
import com.cab.digicafe.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by CSS on 02-11-2017.
 */

public class CategoryAddEditListAdapter extends RecyclerView.Adapter<CategoryAddEditListAdapter.MyViewHolder> {
    private ArrayList<SupplierCategory> catererList;

    private Context mContext;
    private boolean onBind;
    private OnLoadMoreListener mOnLoadMoreListener;

    public interface OnItemClickListener {
        void onItemClick(SupplierCategory item);

    }

    private OnItemClickListener listener;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvCategory)
        TextView tvCategory;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

        }
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }

    public CategoryAddEditListAdapter(ArrayList<SupplierCategory> moviesList, Context context, OnItemClickListener listener) {
        this.catererList = moviesList;
        this.mContext = context;
        this.listener = listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_category, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        SupplierCategory sp = catererList.get(position);

        holder.tvCategory.setText(sp.getPricelistCategoryName());

        holder.tvCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                int t = position;

              /*  if(!catererList.get(t).isChecked()) {

                    for (int i = 0; i < catererList.size(); i++) {
                        if (i != t) {
                            catererList.get(i).setChecked(false);
                        }
                    }
                    catererList.get(t).setChecked(true);
                    listener.onItemClick(catererList.get(t));
                    notifyDataSetChanged();
                }*/

                if (catererList.get(t).isChecked()) {
                    catererList.get(t).setChecked(false);
                } else {
                    catererList.get(t).setChecked(true);
                }
                listener.onItemClick(catererList.get(t));
                notifyDataSetChanged();


            }
        });


        if (sp.isChecked()) {
            holder.tvCategory.setBackground(ContextCompat.getDrawable(mContext, R.drawable.roundcorner_color));
            holder.tvCategory.setTextColor(ContextCompat.getColor(mContext, R.color.white));
        } else {
            holder.tvCategory.setBackground(ContextCompat.getDrawable(mContext, R.drawable.roundcorner_clrstrokecolor));
            holder.tvCategory.setTextColor(ContextCompat.getColor(mContext, R.color.black));
        }


    }

    @Override
    public int getItemCount() {
        return catererList.size();
    }

    public void setitem(List<SupplierCategory> products) {
        catererList = new ArrayList<>();
        catererList.addAll(products);
        notifyDataSetChanged();
    }


  /*  public void setFilter(List<CatalougeContent> countryModels) {
        catererList = new ArrayList<>();
        catererList.addAll(countryModels);
        notifyDataSetChanged();
    }*/


}
