package com.cab.digicafe.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;

import com.cab.digicafe.Model.templateModel;
import com.cab.digicafe.R;

import java.util.ArrayList;

public class TemplateSelectAdapter extends RecyclerView.Adapter<TemplateSelectAdapter.MyViewHolder> {

    private ArrayList<templateModel> templateList;
    private OnItemClickListener listener;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public RadioButton rbTemplateName;

        public MyViewHolder(View view) {
            super(view);
            rbTemplateName = (RadioButton) view.findViewById(R.id.rbTemplateName);

        }
    }

    public TemplateSelectAdapter(ArrayList<templateModel> templateList, OnItemClickListener listener) {
        this.templateList = templateList;
        this.listener = listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.template_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        templateModel movie = templateList.get(position);
        holder.rbTemplateName.setText(movie.getTemplateName());

        if (movie.isCheck()) {
            holder.rbTemplateName.setChecked(true);
        } else {
            holder.rbTemplateName.setChecked(false);
        }


        holder.rbTemplateName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i = 0; i < templateList.size(); i++) {
                    if (i == position) {
                        templateList.get(i).setCheck(true);
                    } else {
                        templateList.get(i).setCheck(false);
                    }
                }
                listener.onItemClick(position);
                notifyDataSetChanged();
            }
        });
    }

    public interface OnItemClickListener {
        void onItemClick(int position);

    }


    @Override
    public int getItemCount() {
        return templateList.size();
    }
}