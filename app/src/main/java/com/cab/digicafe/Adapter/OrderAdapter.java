package com.cab.digicafe.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cab.digicafe.Activities.ImagePreviewActivity;
import com.cab.digicafe.Activities.ProductDetiailsDialogue;
import com.cab.digicafe.Activities.healthTemplate.MyDoctorData;
import com.cab.digicafe.Adapter.master.ImagePagerAdapter;
import com.cab.digicafe.Adapter.settings.SampleImgInCartAdapter;
import com.cab.digicafe.Apputil;
import com.cab.digicafe.Database.SqlLiteDbHelper;
import com.cab.digicafe.Helper.Inad;
import com.cab.digicafe.Helper.LoadImg;
import com.cab.digicafe.Helper.SessionManager;
import com.cab.digicafe.Helper.SharedPrefUserDetail;
import com.cab.digicafe.Model.Catelog;
import com.cab.digicafe.Model.Metal;
import com.cab.digicafe.Model.ModelFile;
import com.cab.digicafe.Model.ShareModel;
import com.cab.digicafe.MyCustomClass.JsonObjParse;
import com.cab.digicafe.OrderActivity;
import com.cab.digicafe.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import me.relex.circleindicator.CircleIndicator;

/**
 * Created by CSS on 02-11-2017.
 */

public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.MyViewHolder> {
    private List<Catelog> catererList;
    private Context mContext;
    private SqlLiteDbHelper dbHelper;
    String currency = "";
    int serviceId = -1;

    String field_json_data = "", template_type = "", service_type_of = "";
    ImagePagerAdapter imagePagerAdapter;


    public interface OnItemClickListener {
        void onItemClick(Catelog item);

        void onadditem(Catelog qty);

        void ondecreasecount(Catelog qty);

        void onAddSample(int qty);
    }

    private OnItemClickListener listener;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name, email, mobile, offer, qty, eachprod, tvTmp, tvPriceRangeWholesale,
                tvQtyWholesale, tvDaysWholesale,
                tvSqFt, tvUds, tvCarpetArea, tvBedRoom, tvBathRoom, tvKitchen, tvfacing, tvPropPrice, tvPropertyStatus;

        private ImageView imageView, ivWholeSale;
        private Button addbtn, removebtn;
        LinearLayout llCb, llWholeSale, llPropertyInfo;
        CheckBox cbShopping;
        ViewPager pager;
        CircleIndicator indicator;
        RelativeLayout rlMediaView, llEdit;
        ImageView ivAddSampleImg, ivEdit, ivDelete, ivPlus;
        RecyclerView rvSamples;
        ImageView ivKitchen, ivBath, ivBed;


        public MyViewHolder(View view) {
            super(view);

            ivKitchen = (ImageView) view.findViewById(R.id.ivKitchen);
            ivBath = (ImageView) view.findViewById(R.id.ivBath);
            ivBed = (ImageView) view.findViewById(R.id.ivBed);


            qty = (TextView) view.findViewById(R.id.qty);
            tvTmp = (TextView) view.findViewById(R.id.tvTmp);
            name = (TextView) view.findViewById(R.id.name);
            email = (TextView) view.findViewById(R.id.descriptionlabel);
            offer = (TextView) view.findViewById(R.id.offer);

            eachprod = (TextView) view.findViewById(R.id.totalprice);
            addbtn = (Button) view.findViewById(R.id.addbtn);
            removebtn = (Button) view.findViewById(R.id.removebtn);
            mobile = (TextView) view.findViewById(R.id.mobile);
            imageView = (ImageView) view.findViewById(R.id.profile_image);
            llCb = (LinearLayout) view.findViewById(R.id.llCb);
            cbShopping = (CheckBox) view.findViewById(R.id.cbShopping);
            rlMediaView = (RelativeLayout) view.findViewById(R.id.rlMediaView);
            pager = (ViewPager) view.findViewById(R.id.pager);
            indicator = (CircleIndicator) view.findViewById(R.id.indicator);

            ivWholeSale = (ImageView) view.findViewById(R.id.ivWholeSale);
            ivAddSampleImg = (ImageView) view.findViewById(R.id.ivAddSampleImg);
            llWholeSale = (LinearLayout) view.findViewById(R.id.llWholeSale);
            tvPriceRangeWholesale = (TextView) view.findViewById(R.id.tvPriceRangeWholesale);
            tvQtyWholesale = (TextView) view.findViewById(R.id.tvQtyWholesale);
            tvDaysWholesale = (TextView) view.findViewById(R.id.tvDaysWholesale);
            rvSamples = (RecyclerView) view.findViewById(R.id.rvSamples);

            llPropertyInfo = (LinearLayout) view.findViewById(R.id.llPropertyInfo);
            tvSqFt = (TextView) view.findViewById(R.id.tvSqFt);
            tvUds = (TextView) view.findViewById(R.id.tvUds);
            tvCarpetArea = (TextView) view.findViewById(R.id.tvCarpetArea);
            tvBedRoom = (TextView) view.findViewById(R.id.tvBedRoom);
            tvBathRoom = (TextView) view.findViewById(R.id.tvBathRoom);
            tvKitchen = (TextView) view.findViewById(R.id.tvKitchen);
            tvfacing = (TextView) view.findViewById(R.id.tvfacing);
            tvPropPrice = (TextView) view.findViewById(R.id.tvPropPrice);
            tvPropertyStatus = (TextView) view.findViewById(R.id.tvPropertyStatus);

            llEdit = (RelativeLayout) view.findViewById(R.id.llEdit);
            ivEdit = (ImageView) view.findViewById(R.id.ivEdit);
            ivDelete = (ImageView) view.findViewById(R.id.ivDelete);
            ivPlus = (ImageView) view.findViewById(R.id.ivPlus);

        }
    }


    public OrderAdapter(List<Catelog> moviesList, Context context, OnItemClickListener listener) {
        this.catererList = moviesList;
        this.mContext = context;
        this.listener = listener;
        dbHelper = new SqlLiteDbHelper(context);
        currency = SharedPrefUserDetail.getString(context, SharedPrefUserDetail.chit_symbol_native, "") + " ";

        field_json_data = SharedPrefUserDetail.getString(mContext, SharedPrefUserDetail.field_json_data, "");
        template_type = JsonObjParse.getValueEmpty(field_json_data, "template_type");
        service_type_of = JsonObjParse.getValueEmpty(field_json_data, "service_type_of");

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.order_list_row, parent, false);

        return new MyViewHolder(itemView);
    }

    Double sgst = 0.0;
    Double cgst = 0.0;

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final Catelog movie = catererList.get(position);

        initMultiImg(holder, position);
        // wholesale
        holder.tvDaysWholesale.setText("");
        holder.tvQtyWholesale.setText("");
        holder.tvPriceRangeWholesale.setText("");

        checkWholesaleFst(holder, movie);


        /////////////////////////////////////////////////


        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(movie);
            }
        });


        holder.name.setText(movie.getDefault_name());
        boolean isanyjsondata = false;

        try {
/*
            JSONObject jo_add = new JSONObject(movie.getAdditional_json_data());
            JSONObject jo_tax = new JSONObject(movie.getTax_json_data());
            JSONObject jo_field = new JSONObject(movie.getField_json_data());


            if (movie.getAdditional_json_data() != null && jo_add.length() > 0) {
                isanyjsondata = true;
            }

            if (movie.getTax_json_data() != null && jo_tax.length() > 0) {
                isanyjsondata = true;
            }

            if (movie.getField_json_data() != null && jo_field.length() > 0) {
                isanyjsondata = true;
            }
*/

            Object json = new JSONTokener(movie.getTax_json_data()).nextValue();
            if (json instanceof JSONObject) {
                JSONObject jo_tax = new JSONObject(movie.getTax_json_data());
                if (movie.getTax_json_data() != null && jo_tax.length() > 0) {
                    isanyjsondata = true;
                }
            }

            json = new JSONTokener(movie.getField_json_data()).nextValue();
            if (json instanceof JSONObject) {
                JSONObject jo_field = new JSONObject(movie.getField_json_data());
                if (movie.getField_json_data() != null && jo_field.length() > 0) {
                    isanyjsondata = true;
                }
            }

            json = new JSONTokener(movie.getAdditional_json_data()).nextValue();
            if (json instanceof JSONObject) {
                JSONObject jo_add = new JSONObject(movie.getAdditional_json_data());
                if (movie.getAdditional_json_data() != null && jo_add.length() > 0) {
                    isanyjsondata = true;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
       /* if (isanyjsondata) {
            holder.name.setTextColor(Color.parseColor("#0000EE"));
            holder.name.setPaintFlags(holder.name.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            holder.name.setEnabled(true);

        } else {
            holder.name.setPaintFlags(holder.name.getPaintFlags() & (~ Paint.UNDERLINE_TEXT_FLAG));
            holder.name.setTextColor(Color.parseColor("#000000"));
            holder.name.setEnabled(false);

        }*/


        //final NumberFormat format = NumberFormat.getCurrencyInstance();
        movie.setCartcount(dbHelper.getproductcartcount(movie));
        if (TextUtils.isEmpty(movie.getOffer())) {
            holder.offer.setText("");

        } else {
            holder.offer.setVisibility(View.VISIBLE);
            holder.offer.setText(movie.getOffer());

        }
        holder.qty.setText("# " + String.valueOf(movie.getCartcount()));


        Double price = Double.parseDouble(movie.getdiscountmrp().toString());

        String doctor_template_type = JsonObjParse.getValueEmpty(movie.getInternal_json_data(), "doctor_template_type");

        int qty = movie.getCartcount();


        Double ded_price_val = 0.0, addi_price_val = 0.0;
        Double ded_perc_val = 0.0, addi_perc_val = 0.0;
        Double ded_mrp = 0.0, addi_mrp = 0.0;
        String jewel = JsonObjParse.getValueEmpty(movie.getInternal_json_data(), "jewel");

        if (!jewel.isEmpty()) {


            try {
                String DeductionJewelPrice = JsonObjParse.getValueEmpty(jewel, "DeductionJewelPrice");
                ded_price_val = movie.getAdditionalDeductinalInfo(DeductionJewelPrice, price);
                ded_mrp = Double.valueOf(movie.getValues(price, ded_perc_val, ded_price_val));
            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {
                String AdditionalJewelPrice = JsonObjParse.getValueEmpty(jewel, "AdditionalJewelPrice");
                addi_price_val = movie.getAdditionalDeductinalInfo(AdditionalJewelPrice, price);
                addi_mrp = Double.valueOf(movie.getValues(price, addi_perc_val, addi_price_val));
            } catch (JSONException e) {
                e.printStackTrace();
            }

            price = price - ded_mrp + addi_mrp;
        }


        if (doctor_template_type.equalsIgnoreCase("RPD")||doctor_template_type.equalsIgnoreCase("Precision Attachment")) {
            Double Amrp = 0.0;
            Log.d("additional", JsonObjParse.getValueEmpty(catererList.get(position).getInternal_json_data(), "additionalPrice_rpd") + "");
            if (!JsonObjParse.getValueEmpty(catererList.get(position).getInternal_json_data(), "additionalPrice_rpd").isEmpty()) {
                Amrp = Double.valueOf(JsonObjParse.getValueEmpty(catererList.get(position).getInternal_json_data(), "additionalPrice_rpd"));

                price = price + Amrp * (qty - 1);
            }
        } else {
            price = movie.getCartcount() * price;
        }


        sgst = 0.0;
        cgst = 0.0;
        //Double VAT = 0.0;

        try {
            String tax = movie.getTax_json_data();
            if (tax != null) {
                setTax(tax);
/*
                JSONObject obj = new JSONObject(tax);

                if(obj.has("VAT")){

                    VAT = Double.valueOf(obj.getString("VAT"));
                    VAT = (price / 100.0f) * VAT;
                    //price = price + VAT;


                }
                else {
                    sgst = Double.valueOf(obj.getString("SGST"));
                    cgst = Double.valueOf(obj.getString("CGST"));
                    sgst = (price / 100.0f) * sgst;
                    cgst = (price / 100.0f) * cgst;
                    //price = price + cgst + sgst;
                }
*/

            }

            Log.e("stategst", String.valueOf(sgst));
            Log.e("stategst", String.valueOf(cgst));
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        sgst = (price / 100.0f) * sgst;
        cgst = (price / 100.0f) * cgst;


        price = price + cgst + sgst;


        //currency = movie.getSymbol_native() + " ";
        //holder.eachprod.setText(currency+String.format("%.2f", price));
        holder.eachprod.setText(currency + Inad.getCurrencyDecimal(price, mContext));


        holder.email.setVisibility(View.GONE);

        //holder.email.setText(format.format(Double.parseDouble(movie.getdiscountmrp().toString())));
        // holder.email.setText(format.format(Double.parseDouble(movie.getMrp().toString())));
     /*   Double price = movie.getCartcount() * Double.parseDouble(movie.getMrp().toString());
                holder.qty.setText("# "+String.valueOf(movie.getCartcount()));
        holder.eachprod.setText(format.format(price));
        String imagreurl ="";
        if (movie.getImageArr().size() > 0)
        {
            imagreurl = movie.getImageArr().get(0).trim();
        }
        if (!TextUtils.isEmpty(imagreurl)) {
            Picasso.with(mContext)
                    .load(imagreurl)
                    .placeholder(R.drawable.imgeerr)   // optional
                    .error(R.drawable.imgeerr)      // optional
                    .fit()                        // optional
                    .into(holder.imageView)
            ;
        }
*/
        holder.addbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //   holder.qty.setText(Integer.parseInt(holder.qty.getText().toString().replace("#",""))+1);

                String wholesale = JsonObjParse.getValueEmpty(movie.getInternal_json_data(), "wholesale");

                if (!wholesale.isEmpty()) {

                    final int ii = position;

                    ProductDetiailsDialogue productDetiailsDialogue = new ProductDetiailsDialogue((Activity) mContext, catererList.get(ii), true, new ProductDetiailsDialogue.AddWholesale() {
                        @Override
                        public void addItems(Catelog catelog) {
                            movie.setCartcount(catelog.getCartcount());
                            movie.setMrp(catelog.getMrp());
                            listener.onadditem(movie);
                            notifyDataSetChanged();
                        }

                        @Override
                        public void deleteItems(Catelog catelog) {

                            movie.setCartcount(0);
                            listener.ondecreasecount(movie);


                            catererList = dbHelper.getAllcaretproduct();

                            notifyDataSetChanged();

                        }
                    });
                    productDetiailsDialogue.show();

                } else {
                    movie.setCartcount(movie.getCartcount() + 1);
                    listener.onadditem(movie);
                    notifyDataSetChanged();
                }


            }
        });

        holder.removebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //   holder.qty.setText(Integer.parseInt(holder.qty.getText().toString().replace("#",""))-1);
                movie.setCartcount(movie.getCartcount() - 1);
                listener.ondecreasecount(movie);
                if (movie.getCartcount() == 0) {
                    catererList = dbHelper.getAllcaretproduct();
                    notifyDataSetChanged();
                } else {
                    notifyItemChanged(position);
                }
                //  notifyDataSetChanged();
            }
        });

        holder.name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int ii = position;

                ProductDetiailsDialogue productDetiailsDialogue = new ProductDetiailsDialogue((Activity) mContext, catererList.get(ii), false, new ProductDetiailsDialogue.AddWholesale() {
                    @Override
                    public void addItems(Catelog catelog) {

                    }

                    @Override
                    public void deleteItems(Catelog catelog) {

                    }
                });
                productDetiailsDialogue.show();

              /*  new CatelogDetailDialog((Activity) mContext, new CatelogDetailDialog.OnDialogClickListener() {
                    @Override
                    public void onDialogImageRunClick(int positon) {
                        if (positon == 0) {

                        }
                    }
                }, catererList.get(ii)).show();*/
            }
        });

        if (service_type_of.equalsIgnoreCase(mContext.getString(R.string.jewel))) {

            //String metal_list = JsonObjParse.getValueEmpty(field_json_data, "metal_list");

            Metal metal = ShareModel.getI().metal;
            if (metal == null) metal = new Metal();
            serviceId = metal.serviceId;
        }


        field_json_data = SharedPrefUserDetail.getString(mContext, SharedPrefUserDetail.field_json_data, "");
        service_type_of = JsonObjParse.getValueEmpty(field_json_data, "service_type_of");
        holder.cbShopping.setVisibility(View.GONE);

        if (serviceId == 2 || service_type_of.equalsIgnoreCase(mContext.getString(R.string.shopping_cart_cb))) {
            holder.cbShopping.setVisibility(View.VISIBLE);
            holder.addbtn.setVisibility(View.INVISIBLE);
            holder.removebtn.setVisibility(View.INVISIBLE);
            holder.qty.setText("");
        } else if (serviceId == 1 || service_type_of.equalsIgnoreCase(mContext.getString(R.string.property))) {
            holder.addbtn.setVisibility(View.INVISIBLE);
            holder.removebtn.setVisibility(View.INVISIBLE);
            holder.qty.setText("");
        }else if (serviceId == 1 || service_type_of.equalsIgnoreCase(mContext.getString(R.string.delivery))) {
            holder.addbtn.setVisibility(View.INVISIBLE);
            holder.removebtn.setVisibility(View.INVISIBLE);
            holder.qty.setText("");
        }

        holder.cbShopping.setChecked(false);
        if (movie.getCartcount() == 1) {
            holder.cbShopping.setChecked(true);
        }


        holder.llCb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (serviceId == 2 || service_type_of.equalsIgnoreCase(mContext.getString(R.string.shopping_cart_cb))) {
                    if (catererList.get(position).getCartcount() == 1) {
                        holder.removebtn.performClick();
                    } else {

                        String max = JsonObjParse.getValueEmpty(field_json_data, "max");
                        if (max.isEmpty()) {
                            holder.addbtn.performClick();
                        } else {
                            int maxI = Integer.parseInt(max);
                            if (dbHelper.getcartcount() >= maxI) {
                                Toast.makeText(mContext, "Max limit is " + maxI, Toast.LENGTH_LONG).show();
                            } else {
                                holder.addbtn.performClick();
                            }
                        }


                    }

                    holder.qty.setVisibility(View.INVISIBLE);
                    holder.removebtn.setVisibility(View.INVISIBLE);

                }

                String doctor_template_type = JsonObjParse.getValueEmpty(catererList.get(position).getInternal_json_data(), "doctor_template_type");

                if (!doctor_template_type.isEmpty()) {
                    MyDoctorData myDoctorData = new MyDoctorData();
                    for (int i = 0; i < myDoctorData.getTemplateList().size(); i++) {
                        if (myDoctorData.getTemplateList().get(i).getTemplateName().equalsIgnoreCase(doctor_template_type)) {
                            Intent intent = new Intent(mContext, myDoctorData.getTemplateList().get(i).getRedirectionActivity());
                            intent.putExtra("Catelog", new Gson().toJson(catererList.get(position)));
                            mContext.startActivity(intent);
                            break;
                        }
                    }
                }
            }
        });

        String imagreurl = "";
        holder.rlMediaView.setVisibility(View.GONE);
        holder.imageView.setVisibility(View.GONE);
        ArrayList<String> pb = new ArrayList<>();
        if (movie.getImg_url() != null && !movie.getImg_url().equals("[]")) {
            Gson gson = new Gson();
            TypeToken<ArrayList<String>> token = new TypeToken<ArrayList<String>>() {
            };
            pb = gson.fromJson(movie.getImg_url(), token.getType());

            if (pb != null && pb.size() > 0) {
                imagreurl = pb.get(0);
                holder.imageView.setVisibility(View.VISIBLE);
            } else {
                pb = new ArrayList<>();
            }
     /*
            try {
                Picasso.with(mContext)
                        .load(pb.get(0))
                        .into(holder.imageView, new Callback() {
                            @Override
                            public void onSuccess() {
                                holder.imageView.setVisibility(View.VISIBLE);
                            }

                            @Override
                            public void onError() {
                                holder.imageView.setVisibility(View.GONE);
                            }
                        })
                ;
                //holder.imageView.set
            } catch (Exception e) {
                e.printStackTrace();
                holder.imageView.setVisibility(View.GONE);
            }*/
        }


        if (template_type.equalsIgnoreCase("image") || service_type_of.equalsIgnoreCase(mContext.getString(R.string.property))) {
            holder.imageView.setVisibility(View.GONE);
            holder.rlMediaView.setVisibility(View.VISIBLE);
            //loadImg.loadCategoryImg(mContext, imagreurl, holder.ivImgTemplate);

            try {

                imagePagerAdapter = new ImagePagerAdapter(mContext, pb, movie.getDefault_name());
                holder.pager.setAdapter(imagePagerAdapter);
                holder.indicator.setViewPager(holder.pager);

            } catch (Exception e) {
                e.printStackTrace();
            }


            //holder.llCb.setBackground(null);
            //holder.llCb.setBackground(ContextCompat.getDrawable(mContext, R.drawable.shadow));
        } else {

            try {

                loadImg.loadCategoryImg(mContext, imagreurl, holder.imageView);


               /* Picasso.with(mContext)
                        .load(imagreurl)
                        .into(holder.imageView, new Callback() {
                            @Override
                            public void onSuccess() {
                                holder.imageView.setVisibility(View.VISIBLE);
                            }

                            @Override
                            public void onError() {
                                holder.imageView.setVisibility(View.GONE);
                            }
                        });*/
            } catch (Exception e) {
                e.printStackTrace();

            }

        }

        String wholesale = JsonObjParse.getValueEmpty(movie.getInternal_json_data(), "wholesale");

        if (!wholesale.isEmpty()) {
            holder.removebtn.setVisibility(View.INVISIBLE);
        }

        holder.ivAddSampleImg.setEnabled(true);
        holder.ivAddSampleImg.setAlpha(1f);

        if (movie.getCartcount() == 0) {
            holder.ivAddSampleImg.setEnabled(false);
            holder.ivAddSampleImg.setAlpha(0.5f);
        }

        holder.ivAddSampleImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onAddSample(position);
            }
        });

        if (!doctor_template_type.isEmpty()) {
            if (doctor_template_type.equalsIgnoreCase("None")) {
                holder.addbtn.setVisibility(View.VISIBLE);
                holder.ivAddSampleImg.setVisibility(View.GONE);
                if (movie.getCartcount() > 0) {
                    holder.qty.setVisibility(View.VISIBLE);
                } else {
                    holder.qty.setVisibility(View.GONE);
                }
            } else {
                holder.addbtn.setVisibility(View.GONE);
                holder.qty.setVisibility(View.GONE);
                holder.removebtn.setVisibility(View.GONE);
                holder.ivAddSampleImg.setVisibility(View.GONE);
                holder.llEdit.setVisibility(View.VISIBLE);
                if (movie.getCartcount() > 0) {
                    holder.ivEdit.setVisibility(View.VISIBLE);
                    holder.ivDelete.setVisibility(View.VISIBLE);
                    holder.ivPlus.setVisibility(View.GONE);
                } else {
                    holder.ivEdit.setVisibility(View.GONE);
                    holder.ivDelete.setVisibility(View.GONE);
                    holder.ivPlus.setVisibility(View.VISIBLE);
                }
            }
        }
        holder.ivEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String doctor_template_type = JsonObjParse.getValueEmpty(catererList.get(position).getInternal_json_data(), "doctor_template_type");

                if (!doctor_template_type.isEmpty()) {
                    MyDoctorData myDoctorData = new MyDoctorData();
                    for (int i = 0; i < myDoctorData.getTemplateList().size(); i++) {

                        if (myDoctorData.getTemplateList().get(i).getTemplateName().equalsIgnoreCase(doctor_template_type)) {
                            OrderActivity.isEdit = true;
                            Intent intent = new Intent(mContext, myDoctorData.getTemplateList().get(i).getRedirectionActivity());
                            intent.putExtra("Catelog", new Gson().toJson(catererList.get(position)));
                            mContext.startActivity(intent);
                            break;
                        }
                    }
                }
            }
        });
        holder.ivPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String doctor_template_type = JsonObjParse.getValueEmpty(catererList.get(position).getInternal_json_data(), "doctor_template_type");

                if (!doctor_template_type.isEmpty()) {
                    MyDoctorData myDoctorData = new MyDoctorData();
                    for (int i = 0; i < myDoctorData.getTemplateList().size(); i++) {
                        if (myDoctorData.getTemplateList().get(i).getTemplateName().equalsIgnoreCase(doctor_template_type)) {
                            Intent intent = new Intent(mContext, myDoctorData.getTemplateList().get(i).getRedirectionActivity());
                            intent.putExtra("Catelog", new Gson().toJson(catererList.get(position)));
                            mContext.startActivity(intent);
                            break;
                        }
                    }
                }
            }
        });
        holder.ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                catererList.get(position).setCartcount(0);
                listener.ondecreasecount(catererList.get(position));
                notifyDataSetChanged();
            }
        });

    }

    LoadImg loadImg = new LoadImg();

    @Override
    public int getItemCount() {
        return catererList.size();
    }


    public void setTax(String field_json_data) throws JSONException {

        if (field_json_data != null) {

            boolean isF1 = true;

            JSONObject joTax = new JSONObject(field_json_data);

            Iterator keys = joTax.keys();

            while (keys.hasNext()) {
                try {
                    String key = (String) keys.next();

                    String taxVal = JsonObjParse.getValueFromJsonObj(joTax, key);

                    if (isF1) {
                        isF1 = false;
                        sgst = Double.valueOf(taxVal);


                    } else {
                        cgst = Double.valueOf(taxVal);

                    }

                    Log.e("key", key);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void checkWholesaleFst(MyViewHolder holder, Catelog movie) {
        holder.ivWholeSale.setVisibility(View.GONE);
        holder.llWholeSale.setVisibility(View.GONE);
        holder.llPropertyInfo.setVisibility(View.GONE);
        holder.tvPropertyStatus.setVisibility(View.GONE);

        String wholesale = JsonObjParse.getValueEmpty(movie.getInternal_json_data(), "wholesale");
        String property = JsonObjParse.getValueEmpty(movie.getInternal_json_data(), "property");

        if (!wholesale.isEmpty()) {

            holder.addbtn.setVisibility(View.VISIBLE);


            String minQ = JsonObjParse.getValueEmpty(wholesale, "minQ");
            holder.tvQtyWholesale.setText("Min. quantity : #" + minQ);

            String minPrice = JsonObjParse.getValueEmpty(wholesale, "minPrice");
            String maxPrice = JsonObjParse.getValueEmpty(wholesale, "maxPrice");

            if (!minPrice.isEmpty() && !maxPrice.isEmpty()) {
                Double minP = Double.valueOf(minPrice);
                Double maxP = Double.valueOf(maxPrice);

                holder.tvPriceRangeWholesale.setText("Price range : " + currency + Inad.getCurrencyDecimal(minP, mContext) + "" + " - " + currency + Inad.getCurrencyDecimal(maxP, mContext));

            } else if (!minPrice.isEmpty()) {
                Double minP = Double.valueOf(minPrice);
                holder.tvPriceRangeWholesale.setText("Minimum price : " + currency + Inad.getCurrencyDecimal(minP, mContext) + "");

            }

            String minDay = JsonObjParse.getValueEmpty(wholesale, "minDay");
            String maxDay = JsonObjParse.getValueEmpty(wholesale, "maxDay");

            if (minDay.isEmpty()) {
                holder.tvDaysWholesale.setText("Deliverd within " + maxDay + " day");
            } else if (maxDay.isEmpty()) {
                holder.tvDaysWholesale.setText("Deliverd within " + minDay + " day");
            } else {
                holder.tvDaysWholesale.setText("Deliverd within " + minDay + " - " + maxDay + " days");

            }


            holder.ivWholeSale.setVisibility(View.VISIBLE);
            holder.llWholeSale.setVisibility(View.VISIBLE);
            holder.ivWholeSale.setImageResource(R.drawable.ic_wholesale);

        } else if (!property.isEmpty()) {

            holder.ivWholeSale.setVisibility(View.VISIBLE);
            holder.ivWholeSale.setImageResource(R.drawable.ic_house);
            holder.llPropertyInfo.setVisibility(View.VISIBLE);
            //holder.tvPropertyStatus.setVisibility(View.VISIBLE);

            String proprty_sqft = JsonObjParse.getValueEmpty(property, "proprty_sqft");
            holder.tvSqFt.setText("Sq.ft : " + proprty_sqft);


            String uds_sqft = JsonObjParse.getValueEmpty(property, "uds_sqft");
            holder.tvUds.setText("UDS(sq.ft) : " + uds_sqft);

            holder.tvUds.setVisibility(View.VISIBLE);
            if (uds_sqft.isEmpty()) holder.tvUds.setVisibility(View.GONE);

            String carpet_arae_sqft = JsonObjParse.getValueEmpty(property, "carpet_arae_sqft");
            holder.tvCarpetArea.setText("Carpet area(sq.ft) : " + carpet_arae_sqft);

            holder.tvCarpetArea.setVisibility(View.VISIBLE);
            if (carpet_arae_sqft.isEmpty()) holder.tvCarpetArea.setVisibility(View.GONE);

            String bed = JsonObjParse.getValueEmpty(property, "bed");
            holder.tvBedRoom.setText("BedRoom : " + bed);

            holder.tvBedRoom.setVisibility(View.VISIBLE);
            holder.ivBed.setVisibility(View.VISIBLE);
            if (bed.isEmpty()) {
                holder.tvBedRoom.setVisibility(View.GONE);
                holder.ivBed.setVisibility(View.GONE);

            }

            String bath = JsonObjParse.getValueEmpty(property, "bath");
            holder.tvBathRoom.setText("Bathroom : " + bath);

            holder.tvBathRoom.setVisibility(View.VISIBLE);
            holder.ivBath.setVisibility(View.VISIBLE);
            if (bath.isEmpty()) {
                holder.tvBathRoom.setVisibility(View.GONE);
                holder.ivBath.setVisibility(View.GONE);
            }

            String kitchen = JsonObjParse.getValueEmpty(property, "kitchen");
            holder.tvKitchen.setText("Kitchen : " + kitchen);

            holder.tvKitchen.setVisibility(View.VISIBLE);
            holder.ivKitchen.setVisibility(View.VISIBLE);
            if (kitchen.isEmpty()) {
                holder.tvKitchen.setVisibility(View.GONE);
                holder.ivKitchen.setVisibility(View.GONE);
            }

            String facing = JsonObjParse.getValueEmpty(property, "facing");
            holder.tvfacing.setText("Facing : " + facing);

            holder.tvPropPrice.setVisibility(View.GONE);
            Double mrpProp = 0.0;
            if (!movie.getMrp().isEmpty()) {
                mrpProp = Double.valueOf(movie.getMrp());
                holder.tvPropPrice.setVisibility(View.VISIBLE);
            }
            holder.tvPropPrice.setText("Price : " + currency + Inad.getCurrencyDecimal(mrpProp, mContext) + "");

            String status_property = JsonObjParse.getValueEmpty(property, "status_property");
            switch (status_property) {
                case ("red"):
                    holder.tvPropertyStatus.setTextColor(ContextCompat.getColor(mContext, R.color.rd));
                    break;
                case ("green"):
                    holder.tvPropertyStatus.setTextColor(ContextCompat.getColor(mContext, R.color.grn));
                    break;
                case ("amber"):
                    holder.tvPropertyStatus.setTextColor(ContextCompat.getColor(mContext, R.color.amber));
                    break;
                default:
                    holder.tvPropertyStatus.setTextColor(ContextCompat.getColor(mContext, R.color.rd));
                    break;
            }

            SessionManager sessionManager = new SessionManager(mContext);
            if (sessionManager.getIsEmployess()) {
                holder.tvPropertyStatus.setVisibility(View.VISIBLE);
            }

        }
    }

    // Pass samples


    public void setitem(List<Catelog> products, int pos) {
        Apputil.updateImg(dbHelper, products.get(pos));
        catererList = dbHelper.getAllcaretproduct();
        notifyDataSetChanged();
    }


    public void initMultiImg(final MyViewHolder holder, int pos) {

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        holder.rvSamples.setLayoutManager(mLayoutManager);

        SampleImgInCartAdapter sampleImgInCartAdapter = new SampleImgInCartAdapter(catererList.get(pos).getAl_selet(), mContext, new SampleImgInCartAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(String item) {

            }

            @Override
            public void onDelete(int pos, ModelFile modelFile) {

                catererList.get(modelFile.getPos()).getAl_selet().remove(pos);
                Apputil.updateImg(dbHelper, catererList.get(modelFile.getPos()));
                notifyDataSetChanged();

            }


            @Override
            public void onClickString(String pos) {

            }

            @Override
            public void onImgClick(List<ModelFile> data, int pos) {

                String imgarray = new Gson().toJson(data);

                ModelFile modelFile = data.get(pos);
                Intent intent = new Intent(mContext, ImagePreviewActivity.class);
                intent.putExtra("imgarray", imgarray);
                intent.putExtra("pos", pos);
                intent.putExtra("img_title", catererList.get(modelFile.getPos()).getDefault_name());
                mContext.startActivity(intent);
            }

            @Override
            public void onAddEditInfo(ModelFile item, int pos) {

            }
        }, false, true, false);


        holder.rvSamples.setAdapter(sampleImgInCartAdapter);

    /*    ItemTouchHelper.Callback callback = new ShopDragTouchHelperIssue(shopImgFileAdapter);
        ItemTouchHelper helper = new ItemTouchHelper(callback);
        helper.attachToRecyclerView(holder.rvSamples);*/

    }


}
