package com.cab.digicafe.Adapter.mrp;

import com.cab.digicafe.Model.ShareModel;
import com.cab.digicafe.MyCustomClass.JsonObjParse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TaskPrefViewModel implements Serializable {

    @SerializedName("task_reference_id")
    @Expose
    private String taskReferenceId;
    @SerializedName("task_purpose")
    @Expose
    private String taskPurpose;
    @SerializedName("link_flag")
    @Expose
    private String linkFlag;
    @SerializedName("from_bridge_id")
    @Expose
    private String fromBridgeId;
    @SerializedName("from_chit_hash_id")
    @Expose
    private String fromChitHashId;
    @SerializedName("from_chit_id")
    @Expose
    private String fromChitId;
    @SerializedName("from_ref_id")
    @Expose
    private String fromRefId;
    @SerializedName("from_case_id")
    @Expose
    private String fromCaseId;
    @SerializedName("from_purpose")
    @Expose
    private String fromPurpose;
    @SerializedName("from_field_json_data")
    @Expose
    private String fromFieldJsonData;
    @SerializedName("to_bridge_id")
    @Expose
    private String toBridgeId;
    @SerializedName("to_chit_hash_id")
    @Expose
    private String toChitHashId;
    @SerializedName("to_chit_id")
    @Expose
    private String toChitId;
    @SerializedName("to_ref_id")
    @Expose
    private String toRefId;
    @SerializedName("to_case_id")
    @Expose
    private String toCaseId;
    @SerializedName("to_purpose")
    @Expose
    private String toPurpose;
    @SerializedName("to_field_json_data")
    @Expose
    private String toFieldJsonData;
    @SerializedName("created_date")
    @Expose
    private String createdDate;
    @SerializedName("updated_date")
    @Expose
    private String updatedDate;
    @SerializedName("status")
    @Expose
    private String status;
    private final static long serialVersionUID = -6882481239599558322L;

    public String getTaskReferenceId() {
        return taskReferenceId;
    }

    public void setTaskReferenceId(String taskReferenceId) {
        this.taskReferenceId = taskReferenceId;
    }

    public String getTaskPurpose() {
        return taskPurpose;
    }

    public void setTaskPurpose(String taskPurpose) {
        this.taskPurpose = taskPurpose;
    }

    public String getLinkFlag() {
        return linkFlag;
    }

    public void setLinkFlag(String linkFlag) {
        this.linkFlag = linkFlag;
    }

    public String getFromBridgeId() {
        return fromBridgeId;
    }

    public void setFromBridgeId(String fromBridgeId) {
        this.fromBridgeId = fromBridgeId;
    }

    public String getFromChitHashId() {
        return fromChitHashId;
    }

    public void setFromChitHashId(String fromChitHashId) {
        this.fromChitHashId = fromChitHashId;
    }

    public String getFromChitId() {
        return fromChitId;
    }

    public void setFromChitId(String fromChitId) {
        this.fromChitId = fromChitId;
    }

    public String getFromRefId() {
        return fromRefId;
    }

    public void setFromRefId(String fromRefId) {
        this.fromRefId = fromRefId;
    }

    public String getFromCaseId() {
        return fromCaseId;
    }

    public void setFromCaseId(String fromCaseId) {
        this.fromCaseId = fromCaseId;
    }

    public String getFromPurpose() {
        return fromPurpose;
    }

    public void setFromPurpose(String fromPurpose) {
        this.fromPurpose = fromPurpose;
    }

    public String getFromFieldJsonData() {
        return fromFieldJsonData;
    }

    public void setFromFieldJsonData(String fromFieldJsonData) {
        this.fromFieldJsonData = fromFieldJsonData;
    }

    public String getToBridgeId() {
        return toBridgeId;
    }

    public void setToBridgeId(String toBridgeId) {
        this.toBridgeId = toBridgeId;
    }

    public String getToChitHashId() {
        return toChitHashId;
    }

    public void setToChitHashId(String toChitHashId) {
        this.toChitHashId = toChitHashId;
    }

    public String getToChitId() {
        return toChitId;
    }

    public void setToChitId(String toChitId) {
        this.toChitId = toChitId;
    }

    public String getToRefId() {
        return toRefId;
    }

    public void setToRefId(String toRefId) {
        this.toRefId = toRefId;
    }

    public String getToCaseId() {
        return toCaseId;
    }

    public void setToCaseId(String toCaseId) {
        this.toCaseId = toCaseId;
    }

    public String getToPurpose() {
        return toPurpose;
    }

    public void setToPurpose(String toPurpose) {
        this.toPurpose = toPurpose;
    }

    public String getToFieldJsonData() {
        return toFieldJsonData;
    }

    public void setToFieldJsonData(String toFieldJsonData) {
        this.toFieldJsonData = toFieldJsonData;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getStatus() {
        return status;
    }


    public void setStatus(String status) {
        this.status = status;
    }

    Date datForSort;

    public Date getDateForSort(int sortBy) {

        datForSort = null;

        String forWhich = "";
        if (ShareModel.getI().isFromAllTask) {
            //forWhich = JsonObjParse.getValueEmpty(jsonObject.toString(), "To");
            forWhich = getToFieldJsonData();
        } else {
            //forWhich = JsonObjParse.getValueEmpty(jsonObject.toString(), "From");
            forWhich = getFromFieldJsonData();
        }
        String sortByDate = "";
        String planning = JsonObjParse.getValueEmpty(forWhich, "planning");
        if (planning.isEmpty()) // Mat, Res.
        {
            sortByDate = JsonObjParse.getValueEmpty(forWhich, "EDIT_DATE");
        } else { // Planning

            switch (sortBy) {
                case 0:
                    sortByDate = JsonObjParse.getValueEmpty(planning, "plan_start_date");
                    break;
                case 1:
                    sortByDate = JsonObjParse.getValueEmpty(planning, "plan_end_date");
                    break;
                case 2:
                    sortByDate = JsonObjParse.getValueEmpty(planning, "act_start_date");
                    break;
                case 3:
                    sortByDate = JsonObjParse.getValueEmpty(planning, "act_end_date");
                    break;
            }
        }


        //String EDIT_DATE = JsonObjParse.getValueEmpty(forWhich, "EDIT_DATE");
        String dtStart = sortByDate;
        SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy");
        try {
            if (!dtStart.equals("")) {
                datForSort = format.parse(dtStart);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return datForSort;
    }


}
