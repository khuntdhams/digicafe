package com.cab.digicafe.Adapter.master;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cab.digicafe.Activities.RequestApis.MyResponse.EmployeeViewModel;
import com.cab.digicafe.Callback.OnLoadMoreListener;
import com.cab.digicafe.MyCustomClass.JsonObjParse;
import com.cab.digicafe.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by CSS on 02-11-2017.
 */

public class EmployeeUnderMasterListAdapter extends RecyclerView.Adapter<EmployeeUnderMasterListAdapter.MyViewHolder> {
    private ArrayList<EmployeeViewModel> catererList;

    private Context mContext;
    private boolean onBind;
    private OnLoadMoreListener mOnLoadMoreListener;

    public interface OnItemClickListener {
        void onItemClick(EmployeeViewModel item);

        void onEditRate(EmployeeViewModel item);

        void onCheckChange(int pos, boolean isChecked);

        void onDrag();

    }

    private OnItemClickListener listener;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvCatNm)
        TextView tvCatNm;

        @BindView(R.id.tvUsrId)
        TextView tvUsrId;

        @BindView(R.id.tvUnitDetails)
        TextView tvUnitDetails;

        @BindView(R.id.cbDelete)
        public CheckBox cbDelete;

        @BindView(R.id.llBs)
        public LinearLayout ll_base;

        @BindView(R.id.rlCb)
        public RelativeLayout rlCb;

        @BindView(R.id.llEditUnit)
        public LinearLayout llEditUnit;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

        }
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }

    public EmployeeUnderMasterListAdapter(ArrayList<EmployeeViewModel> moviesList, Context context, OnItemClickListener listener) {
        this.catererList = moviesList;
        this.mContext = context;
        this.listener = listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_ad_emp_undrmstr, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        EmployeeViewModel sp = catererList.get(position);

        try {
            holder.tvCatNm.setText(sp.employee_name);
            holder.tvUsrId.setText(sp.username);

            holder.tvCatNm.setTextColor(ContextCompat.getColor(mContext, R.color.gry));
            if (sp.status.equalsIgnoreCase("active")) {
                holder.tvCatNm.setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
            }


            //holder.tvSortBy.setText(sp.sortBy);

        } catch (Exception e) {
            e.printStackTrace();
        }


        holder.rlCb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (catererList.get(position).isSelect) {
                    holder.cbDelete.setChecked(false);
                    catererList.get(position).isSelect = false;
                    listener.onCheckChange(position, false);
                } else {
                    catererList.get(position).isSelect = true;
                    holder.cbDelete.setChecked(true);
                    listener.onCheckChange(position, true);

                }

            }
        });

        if (sp.isSelect) holder.cbDelete.setChecked(true);
        else holder.cbDelete.setChecked(false);

        holder.ll_base.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onItemClick(catererList.get(position));
            }
        });
        String unit = JsonObjParse.getValueEmpty(sp.field_json_data, "unit");
        String amount = JsonObjParse.getValueEmpty(sp.field_json_data, "amount");
        String type = JsonObjParse.getValueEmpty(sp.field_json_data, "type");


        holder.tvUnitDetails.setText("Rate/Unit : " + unit + ",   Unit type : " + type);


        holder.llEditUnit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                listener.onEditRate(catererList.get(position));


            }
        });
    }

    @Override
    public int getItemCount() {
        return catererList.size();
    }


    public void setitem(List<EmployeeViewModel> products) {

        catererList = new ArrayList<>();
        catererList.addAll(products);
        Log.e("aliassearch", catererList.size() + "");
        notifyDataSetChanged();

    }

    public void remove(int position) {
        catererList.remove(position);
        notifyItemRemoved(position);
    }

    public void swap(int firstPosition, int secondPosition) {

        Collections.swap(catererList, firstPosition, secondPosition);
        notifyItemMoved(firstPosition, secondPosition);
    }

    public void onSwap() {

        listener.onDrag();
    }


}
