package com.cab.digicafe.Adapter.other;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cab.digicafe.Model.Coupon;
import com.cab.digicafe.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class CouponListAdapter extends RecyclerView.Adapter<CouponListAdapter.MyViewHolder> {


    ArrayList<Coupon> alRange;
    private Context mContext;

    OnItemClickListener onItemClickListener;

    public interface OnItemClickListener {
        void onDelete(int item);

        void onEdit(int item);

        void onShowTc(int item);
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvPromo)
        TextView tvPromo;

        @BindView(R.id.tvDiscOrVal)
        TextView tvDiscOrVal;

        @BindView(R.id.tvTc)
        TextView tvTc;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

        }
    }


    String currency = "";

    public CouponListAdapter(ArrayList<Coupon> moviesList, Context context, OnItemClickListener onItemClickListener, String currency) {
        this.alRange = moviesList;
        this.onItemClickListener = onItemClickListener;
        this.mContext = context;
        this.currency = currency;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_ad_promo, parent, false);

        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {


        Coupon productTotalDiscount = alRange.get(position);
        int p = position + 65;

        //String c = Character.toString((char) p);

        holder.tvPromo.setText(productTotalDiscount.couponCode + "");

        String discTxt = "";

        if (productTotalDiscount.promo_modulo > 0) {
            discTxt = "" + productTotalDiscount.promo_modulo + " % Discount ";
        } else if (productTotalDiscount.promo_value > 0) {
            discTxt = currency + " " + productTotalDiscount.promo_value + " Discount ";
            productTotalDiscount.min_purchase = productTotalDiscount.promo_value;
            productTotalDiscount.max_discount = productTotalDiscount.promo_value;
        }


        if (productTotalDiscount.min_purchase > 0) {

            discTxt = discTxt + " on\nMin. order of " + currency + " " + productTotalDiscount.min_purchase + ",";
        }


        if (productTotalDiscount.max_discount > 0) {

            discTxt = discTxt + "\nMax. discount " + currency + " " + productTotalDiscount.max_discount;
        }

        discTxt = discTxt + "\nFrom : " + productTotalDiscount.getStartDateInFormat() + "\nTo : " + productTotalDiscount.getEndDateInFormat();

        holder.tvTc.setVisibility(View.GONE);
        if (productTotalDiscount.terms.length() > 0) holder.tvTc.setVisibility(View.VISIBLE);

        holder.tvDiscOrVal.setText(productTotalDiscount.terms);

        holder.tvDiscOrVal.setText(discTxt);

        holder.tvTc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.onShowTc(position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return alRange.size();
    }

    public void setItem(ArrayList<Coupon> path) {
        //alRange = new ArrayList<>();
        //alRange = path;
        notifyItemChanged(alRange.size() - 1);
    }

}
