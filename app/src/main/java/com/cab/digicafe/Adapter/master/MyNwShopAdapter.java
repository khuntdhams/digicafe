package com.cab.digicafe.Adapter.master;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.cab.digicafe.Activities.ImagePreviewActivity;
import com.cab.digicafe.Helper.LoadImg;
import com.cab.digicafe.Model.ModelFile;
import com.cab.digicafe.Model.Supplier;
import com.cab.digicafe.MyCustomClass.JsonObjParse;
import com.cab.digicafe.MyCustomClass.PlayAnim;
import com.cab.digicafe.R;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by CSS on 02-11-2017.
 */

public class MyNwShopAdapter extends RecyclerView.Adapter<MyNwShopAdapter.MyViewHolder> {
    private List<Supplier> catererList;
    private Context mContext;
    boolean fromAT;

    public interface OnItemClickListener {
        void onItemClick(Supplier item, int pos);

        void onItemEdit(Supplier item, int pos);

        void onItemLongClick(Supplier item, int pos);
    }

    private OnItemClickListener listener;
    String baseurl = "";


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name, bridgeId, mobile, tv_storestatus, tvPass, tvEmpNm, name_shop;
        LinearLayout llMrp, llUpdateNw;
        public ImageView iv_del, ivCatImg, ivGallery;
        public RadioButton rbPassMrpFlag;


        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.name);
            bridgeId = (TextView) view.findViewById(R.id.bridgeId);
            mobile = (TextView) view.findViewById(R.id.mobile);
            tv_storestatus = (TextView) view.findViewById(R.id.tv_storestatus);
            iv_del = (ImageView) view.findViewById(R.id.iv_del);
            ivCatImg = (ImageView) view.findViewById(R.id.ivCatImg);
            ivGallery = (ImageView) view.findViewById(R.id.ivGallery);
            llMrp = (LinearLayout) view.findViewById(R.id.llMrp);
            llUpdateNw = (LinearLayout) view.findViewById(R.id.llUpdateNw);
            rbPassMrpFlag = (RadioButton) view.findViewById(R.id.rbPassMrpFlag);
            tvPass = (TextView) view.findViewById(R.id.tvPass);
            tvEmpNm = (TextView) view.findViewById(R.id.tvEmpNm);
            name_shop = (TextView) view.findViewById(R.id.name_shop);
        }

        public void bind(final Supplier item, final OnItemClickListener listener) {


        }

    }

    public void setFilter(List<Supplier> countryModels) {
        catererList = new ArrayList<>();
        catererList.addAll(countryModels);
        notifyDataSetChanged();
    }


    public void assignItem(List<Supplier> countryModels) {
        catererList = new ArrayList<>();
        catererList = countryModels;
        notifyDataSetChanged();
    }


    public MyNwShopAdapter(List<Supplier> moviesList, OnItemClickListener listener, Context context, boolean fromAT) {
        this.catererList = moviesList;
        this.listener = listener;
        this.mContext = context;
        baseurl = mContext.getString(R.string.BASEURL);
        this.fromAT = fromAT;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_my_nw_shop, parent, false);

        return new MyViewHolder(itemView);
    }

    LoadImg loadImg = new LoadImg();
    PlayAnim playAnim = new PlayAnim();

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final Supplier movie = catererList.get(position);


        holder.name.setText(movie.getAlias_name());
        holder.name_shop.setText(movie.getFirstname());

        if (movie.alias_name.isEmpty()) {
            holder.name_shop.setVisibility(View.GONE);
        } else {
            holder.name_shop.setVisibility(View.VISIBLE);
        }

        holder.name_shop.setPaintFlags(holder.name_shop.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);


        holder.bridgeId.setText("UserId : " + movie.getUsername());
        holder.mobile.setText(movie.getContact_no());
        holder.tvPass.setVisibility(View.GONE);
        holder.rbPassMrpFlag.setVisibility(View.GONE);
        holder.tvPass.setBackground(null);
        holder.tvPass.setText("PASS");
        holder.tvPass.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorPrimary));

        if (movie.getAssign_name().isEmpty()) {
            holder.tvEmpNm.setVisibility(View.GONE);
        } else {
            holder.tvEmpNm.setVisibility(View.VISIBLE);
        }


        holder.tvEmpNm.setText("Assigned to : " + movie.getAssign_name());


        if (fromAT) {

            holder.rbPassMrpFlag.setChecked(false);
            if (movie.isPassMrpSelect()) {
                holder.tvPass.setVisibility(View.VISIBLE);
                playAnim.slideLettTv(holder.tvPass);
                holder.rbPassMrpFlag.setChecked(true);
            }

            holder.rbPassMrpFlag.setVisibility(View.VISIBLE);
            holder.llMrp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!movie.isPassMrpSelect()) {
                        for (int i = 0; i < catererList.size(); i++) {
                            catererList.get(i).setPassMrpSelect(false);
                        }
                        catererList.get(position).setPassMrpSelect(true);
                        assignItem(catererList);
                    }
                }
            });

            holder.tvPass.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    loadImg.loadFitImg(mContext, holder.tvPass);
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            notifyDataSetChanged();
                        }
                    }, 2000);


                }
            });
        }
        if (movie.isIslongpress()) {
            holder.iv_del.setVisibility(View.VISIBLE);
        } else {
            holder.iv_del.setVisibility(View.GONE);
        }
        holder.bind(movie, listener);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Log.e("item clieced", "" + item.getAlias_name());
                int pos = position;
                listener.onItemClick(catererList.get(pos), pos);
            }
        });


        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                int pos = position;
                listener.onItemLongClick(catererList.get(pos), pos);
                return true;
            }
        });


        if (movie.getBusiness_status().equalsIgnoreCase("open")) {
            holder.tv_storestatus.setVisibility(View.GONE);


        } else if (movie.getBusiness_status().equalsIgnoreCase("closed")) {
            holder.tv_storestatus.setText("Closed");
            holder.tv_storestatus.setVisibility(View.GONE);


        }


        String company_images_field = JsonObjParse.getValueEmpty(movie.getField_json_data(), "company_images_field");
        TypeToken<ArrayList<ModelFile>> token = new TypeToken<ArrayList<ModelFile>>() {
        };
        ArrayList<ModelFile> al_selet = new Gson().fromJson(company_images_field, token.getType());
        if (al_selet == null) al_selet = new ArrayList<>();

        //holder.caterrerprofile.setImageDrawable(null);

        if (al_selet.size() > 0) {
            holder.ivCatImg.setVisibility(View.GONE);
            holder.ivGallery.setVisibility(View.VISIBLE);
            //loadImg.loadCategoryImg(mContext, al_selet.get(0).getImgpath(), holder.ivCatImg);
        } else {
            holder.ivCatImg.setVisibility(View.GONE);
            holder.ivGallery.setVisibility(View.GONE);
        }


        holder.llUpdateNw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemEdit(catererList.get(position), position);
            }
        });

        holder.ivGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String company_images_field = JsonObjParse.getValueEmpty(catererList.get(position).getField_json_data(), "company_images_field");
                TypeToken<ArrayList<ModelFile>> token = new TypeToken<ArrayList<ModelFile>>() {
                };
                ArrayList<ModelFile> al_selet = new Gson().fromJson(company_images_field, token.getType());
                if (al_selet == null) al_selet = new ArrayList<>();

                String imgarray = new Gson().toJson(al_selet);


                Intent intent = new Intent(mContext, ImagePreviewActivity.class);
                intent.putExtra("imgarray", imgarray);
                intent.putExtra("pos", 0);
                intent.putExtra("img_title", catererList.get(position).getFirstname());
                mContext.startActivity(intent);

            }
        });


    }

    @Override
    public int getItemCount() {
        return catererList.size();
    }
}
