package com.cab.digicafe.Adapter;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.cab.digicafe.Database.SqlLiteDbHelper;
import com.cab.digicafe.Model.ModelFile;
import com.cab.digicafe.R;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by CSS on 02-11-2017.
 */

public class ImgPrevAdapter extends RecyclerView.Adapter<ImgPrevAdapter.MyViewHolder> {
    private List<ModelFile> data;
    private Context mContext;
    private SqlLiteDbHelper dbHelper;
    DetectWordsAdapter detectWordsAdapter;
    ArrayList<ModelFile> al_detect = new ArrayList<>();
    boolean isdrag = false;

    public interface OnItemClickListener {
        void onItemClick(int pos);

        void onDelete(int pos);

        void onLongClick(View pos);

    }

    private OnItemClickListener listener;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv_img)
        ImageView iv_img;

        @BindView(R.id.iv_play)
        ImageView iv_play;

        @BindView(R.id.view_foreground)
        public RelativeLayout view_foreground;

        @BindView(R.id.tv_stroke)
        public TextView tv_stroke;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }


    public ImgPrevAdapter(List<ModelFile> moviesList, Context context, OnItemClickListener listener, boolean isdrag) {
        this.data = moviesList;
        this.mContext = context;
        this.isdrag = isdrag;
        this.listener = listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_select_media2, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        ModelFile modelFile = data.get(position);

        holder.iv_play.setVisibility(View.GONE);
        if (modelFile.getImgpath().contains("youtube")) {

            Uri uri = Uri.parse(modelFile.getImgpath());
            //Set<String> args = uri.getQueryParameterNames();
            String v = uri.getQueryParameter("v");

            holder.iv_play.setVisibility(View.VISIBLE);
            String url = "" + mContext.getString(R.string.utube_base) + v + mContext.getString(R.string.utube_end);
            Log.e("okhttp utube", url + "");
            Glide.with(mContext).load(url).diskCacheStrategy(DiskCacheStrategy.ALL).placeholder(R.drawable.defaultplace).into(holder.iv_img);

        } else if (modelFile.isIsoffline()) {
            Glide.with(mContext).load(new File(modelFile.getImgpath())).diskCacheStrategy(DiskCacheStrategy.ALL).placeholder(R.drawable.defaultplace).into(holder.iv_img);

        } else {
            Glide.with(mContext).load(modelFile.getImgpath()).diskCacheStrategy(DiskCacheStrategy.ALL).placeholder(R.drawable.defaultplace).into(holder.iv_img);

        }


        holder.iv_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(position);
            }
        });

        if (isdrag) holder.tv_stroke.setVisibility(View.VISIBLE);
    }

    public void setItem(ArrayList<ModelFile> path) {
        data = new ArrayList<>();
        data.addAll(path);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public void remove(int position) {
        data.remove(position);
        notifyItemRemoved(position);
    }

    public void swap(int firstPosition, int secondPosition) {
        Collections.swap(data, firstPosition, secondPosition);
        notifyItemMoved(firstPosition, secondPosition);
    }

}
