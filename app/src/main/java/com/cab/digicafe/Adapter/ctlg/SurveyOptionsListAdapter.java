package com.cab.digicafe.Adapter.ctlg;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cab.digicafe.Model.SurveyOptions;
import com.cab.digicafe.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import butterknife.BindView;
import butterknife.ButterKnife;


public class SurveyOptionsListAdapter extends RecyclerView.Adapter<SurveyOptionsListAdapter.MyViewHolder> {


    ArrayList<SurveyOptions> alRange;
    private Context mContext;

    OnItemClickListener onItemClickListener;

    public interface OnItemClickListener {
        void onDelete(int item);

        void onEdit(int item, ArrayList<SurveyOptions> moviesList);
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvOptions)
        TextView tvOptions;

        @BindView(R.id.tvEmoji)
        TextView tvEmoji;

        @BindView(R.id.tvVal)
        TextView tvVal;

        @BindView(R.id.ivDelete)
        ImageView ivDelete;

        @BindView(R.id.llBs)
        LinearLayout llBs;


        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

        }
    }

    boolean isDel = false;

    public SurveyOptionsListAdapter(ArrayList<SurveyOptions> moviesList, Context context, OnItemClickListener onItemClickListener, boolean isDel) {
        this.alRange = moviesList;
        this.onItemClickListener = onItemClickListener;
        this.mContext = context;

        this.isDel = isDel;

    }

    int p;

    public SurveyOptionsListAdapter(int pos, ArrayList<SurveyOptions> moviesList, Context context, OnItemClickListener onItemClickListener, boolean isDel) {
        this.alRange = moviesList;
        this.onItemClickListener = onItemClickListener;
        this.mContext = context;
        this.p = pos;
        this.isDel = isDel;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_ad_survey_options, parent, false);

        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {


        SurveyOptions productTotalDiscount = alRange.get(position);

        alRange.get(position).catPos = p;


        int p = position + 65;

        String c = Character.toString((char) p);

        holder.llBs.setBackground(null);
        if (productTotalDiscount.isEdit) {
            if (isDel) holder.ivDelete.setVisibility(View.GONE);
            holder.llBs.setBackground(ContextCompat.getDrawable(mContext, R.drawable.strokes_sq_green));
            holder.ivDelete.setColorFilter(ContextCompat.getColor(mContext, R.color.white));
            holder.tvOptions.setTextColor(ContextCompat.getColor(mContext, R.color.white));

        } else {
            holder.ivDelete.setVisibility(View.VISIBLE);
            holder.llBs.setBackground(ContextCompat.getDrawable(mContext, R.drawable.strokes_sq_red));
            holder.ivDelete.setColorFilter(ContextCompat.getColor(mContext, R.color.infoclr));
            holder.tvOptions.setTextColor(ContextCompat.getColor(mContext, R.color.infoclr));

        }


        holder.tvOptions.setText(c + ". " + productTotalDiscount.option);
        holder.tvEmoji.setText("   " + productTotalDiscount.emoji);
        holder.tvVal.setText("   " + productTotalDiscount.value);

        holder.tvEmoji.setVisibility(View.VISIBLE);
        holder.tvVal.setVisibility(View.VISIBLE);
        if (productTotalDiscount.emoji.isEmpty()) holder.tvEmoji.setVisibility(View.GONE);
        if (!isDel) holder.tvVal.setVisibility(View.GONE);

        holder.ivDelete.setEnabled(true);
        holder.ivDelete.setImageResource(R.drawable.delete);
        if (!isDel) {
            holder.ivDelete.setEnabled(false);
            holder.ivDelete.setImageResource(R.drawable.ic_tick);
        }

        holder.ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alRange.remove(position);
                setItem(alRange);
            }
        });


        holder.llBs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


               /* if (!isDel) {
                    for (int i = 0; i < alRange.size(); i++) {
                        alRange.get(i).isEdit = false;
                    }

                    alRange.get(position).isEdit = true;

                    setItem(alRange);
                } else {

                }*/
                onItemClickListener.onEdit(position, alRange);

            }
        });
    }

    @Override
    public int getItemCount() {
        return alRange.size();
    }

    public void setItem(ArrayList<SurveyOptions> path) {
        alRange = new ArrayList<>();
        alRange = path;
        if (isDel) {
            Collections.sort(alRange, new Comparator<SurveyOptions>() {
                @Override
                public int compare(SurveyOptions p1, SurveyOptions p2) {
                    return p2.getValue() - p1.getValue(); // Descending
                }

            });
        }
        notifyDataSetChanged();
    }

}
