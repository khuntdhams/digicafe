package com.cab.digicafe.MyCustomClass;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.util.Log;
import android.widget.Toast;

import com.cab.digicafe.Helper.SharedPrefUserDetail;
import com.cab.digicafe.Rest.ApiClient;
import com.cab.digicafe.Rest.ApiInterface;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by pc51 on 3/13/2018.
 */

public class CheckVersion {

    public CheckVersion(final Context c, String aggre, final Apicallback callback) {


        ApiInterface apiService =
                ApiClient.getClient(c).create(ApiInterface.class);


        Call call;

        call = apiService.getagregatorProfile(aggre);

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {

                if (response.isSuccessful()) {

                    try {
                        String a = new Gson().toJson(response.body());


                        JSONObject Jsonresponse = new JSONObject(a);
                        JSONObject Jsonresponseinfo = Jsonresponse.getJSONObject("response");
                        JSONArray aggregatorobj = Jsonresponseinfo.getJSONArray("data");
                        JSONObject aggregator = aggregatorobj.getJSONObject(0);

                        String priority_val = aggregator.getString("app_android_priority");

                        String app_android_version_number = "1";
                        if (aggregator.has("app_android_version_number")) {
                            app_android_version_number = aggregator.getString("app_android_version_number");
                        }


                        int c_versionCode = 1;
                        PackageInfo pInfo = null;
                        try {
                            pInfo = c.getPackageManager().getPackageInfo(c.getPackageName(), 0);
                            c_versionCode = pInfo.versionCode;

                        } catch (PackageManager.NameNotFoundException e) {
                            e.printStackTrace();
                        }


                        //int daysDifference=3;
                        int daysDifference = 3;


                        String olddate = SharedPrefUserDetail.getString(c, SharedPrefUserDetail.UPDATEOPENDATE, "");


                        if (!olddate.equalsIgnoreCase("")) {

                            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                            try {
                                Date dt_start = df.parse(olddate);
                                Calendar startDate = Calendar.getInstance();
                                startDate.setTime(dt_start);
                                long startDateMillis = startDate.getTimeInMillis();


                                Calendar currentdate = Calendar.getInstance();
                                String formattedDate = df.format(currentdate.getTime()); // 2018-03-13 15:32:45
                                //formattedDate = "2018-03-16 15:0:02"; // 2018-03-13 15:32:45
                                Date dt_current = df.parse(formattedDate);
                                currentdate.setTime(dt_current);
                                long currentDateMillis = currentdate.getTimeInMillis();


                                long diff = currentDateMillis - startDateMillis;
                                daysDifference = (int) (diff / (1000 * 60 * 60 * 24));

                                int halfhour = 30 * 60 * 1000;
                                if (diff > halfhour) {
                                    daysDifference = 3;
                                }


                            } catch (ParseException e) {
                                daysDifference = 0;
                                e.printStackTrace();
                            }


                        }


                        //app_android_version_number ="2";
                        //  if (!app_android_version_number.isEmpty()&&daysDifference>2) {
                        if (!app_android_version_number.isEmpty()) {

                            int webversioncode = Integer.parseInt(app_android_version_number);

                            int diff = webversioncode - c_versionCode;

                            if (diff > 1) { // high/low
                                // update
                                callback.onUpdate(true, "ys");
                            } else if (diff == 1) {

                                if (priority_val.equalsIgnoreCase("low") && daysDifference > 2) {
                                    // cancel/update
                                    callback.onUpdate(false, "ys");
                                } else if (priority_val.equalsIgnoreCase("high")) {
                                    // update
                                    callback.onUpdate(true, "ys");
                                }

                            } else {
                                callback.onUpdate(false, "no");
                            }

                        } else {
                            callback.onUpdate(false, "no");
                        }


                    } catch (Exception e) {


                    }
                } else {


                    try {

                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Toast.makeText(c, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        // Toast.makeText(CatelogActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }


            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // Log error here since request failed

                Log.e("error message", t.toString());
            }
        });
    }


    public interface Apicallback {
        void onGetResponse(String a, boolean issuccess);

        void onUpdate(boolean isupdateforce, String ischeck);
    }

}
