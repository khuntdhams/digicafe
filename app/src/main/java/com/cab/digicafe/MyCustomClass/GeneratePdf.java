package com.cab.digicafe.MyCustomClass;

import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.text.format.DateFormat;

import com.cab.digicafe.R;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;

/**
 * Created by pc51 on 4/10/2018.
 */

public class GeneratePdf {
    public static final String FONT1 = "main/assets/PlayfairDisplay-Regular.ttf";

    // https://www.epson.co.in/For-Work/Printers/POS-Printers/Epson-TM-m30-Bluetooth-Ethernet-Thermal-POS-Receipt-Printer/p/C31CE95311#2iVxoY6A2j8Sj0Q7.97
    // https://stackoverflow.com/questions/6674059/how-to-generate-pdf-file-with-image-in-android
    public Boolean write(String otp, String fcontent, AppCompatActivity c) {
        try {
            String s = otp + DateFormat.format("_MMddyy_hhmmss", new Date().getTime()) + ".pdf";

            //Create file path for Pdf
            //String fpath = "/sdcard/" + s + ".pdf";

            String path = Environment.getExternalStorageDirectory() + File.separator + c.getString(R.string.app_name);

            File filepath = new File(path);

            filepath.mkdir();
           /* if (!filepath.exists()) {
                filepath.createNewFile();
            }*/

            String fpath = Environment.getExternalStorageDirectory() + File.separator + c.getString(R.string.app_name)
                    + File.separator + s;
            File file = new File(filepath, s);


            // To customise the text of the pdf
            // we can use FontFamily
            Font bfBold12 = new Font(Font.FontFamily.TIMES_ROMAN,
                    12, Font.BOLD, new BaseColor(0, 0, 0));
            Font bf12 = new Font(Font.FontFamily.TIMES_ROMAN,
                    12);
            // create an instance of itext document
            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(file.getAbsoluteFile()));
            document.open();
            //using add method in document to insert a paragraph

            Paragraph pp1 = new Paragraph();
            Font smallBold1 = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);

            BaseFont bf_rupee = BaseFont.createFont("assets/Rupee_Foradian.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            Font font_rupee = new Font(bf_rupee, 12);

            BaseFont bf_reg = BaseFont.createFont("assets/arial.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            Font font_reg = new Font(bf_reg, 12);

            byte[] ptext = fcontent.getBytes("ISO-8859-1");
            String value = new String(ptext, "UTF-16");

            pp1.add(value);

           /* String[] str_array = fcontent.split("");

            for (int i = 0;i<str_array.length;i++)
            {

                String str_content = str_array[i];
                if(str_content.contains("₹"))
                {
                    pp1.setFont(font_rupee);
                }
                else {
                    pp1.setFont(font_reg);
                }

                pp1.add(str_content);
            }*/


            //pp1.add(fcontent);
            //BaseFont urName = BaseFont.createFont("assets/PlayfairDisplay-Regular.ttf", BaseFont.IDENTITY_H,BaseFont.EMBEDDED);


            //pp1.setAlignment(Paragraph.ALIGN_LEFT | Paragraph.ALIGN_MIDDLE);
            //pp1.setPaddingTop(30);
            document.add(pp1);


            // document.add(new Paragraph(fcontent));
            //document.add(new Paragraph("Hello World"));
            // close document
            document.close();


            try {
                Uri path1 = FileProvider.getUriForFile(c, c.getPackageName() + ".provider", file);
                Intent objIntent = new Intent(Intent.ACTION_VIEW);
                objIntent.setDataAndType(path1, "application/pdf");
                objIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                c.startActivity(objIntent);

            } catch (Exception e) {
                e.printStackTrace();

            }

            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        } catch (DocumentException e) {
            e.printStackTrace();
            return false;
        }

    }

}
