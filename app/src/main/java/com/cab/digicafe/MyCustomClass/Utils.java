package com.cab.digicafe.MyCustomClass;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ClickableSpan;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;

import com.cab.digicafe.App;
import com.cab.digicafe.BaseActivity;

import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;


public class Utils {
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static Typeface mTypeFace;

    public static final String CAMERA_IMAGE_BUCKET_NAME =
            Environment.getExternalStorageDirectory().toString()
                    + "/DCIM/Camera";
    public static final String CAMERA_IMAGE_BUCKET_ID =
            getBucketId(CAMERA_IMAGE_BUCKET_NAME);

    public static String getBucketId(String path) {
        return String.valueOf(path.toLowerCase().hashCode());
    }


    public static int convertFrequencyToChannel(int freq) {
        if (freq >= 2412 && freq <= 2484) {
            return (freq - 2412) / 5 + 1;
        } else if (freq >= 5170 && freq <= 5825) {
            return (freq - 5170) / 5 + 34;
        } else {
            return -1;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public static boolean isGoodWifi(WifiInfo wifi) {
        return wifi.getFrequency() <= 2484 && wifi.getFrequency() >= 2400 ? true : false;
    }

    public static void postDelayed(Runnable runnable, int time) {
        Handler handler = new Handler();
        handler.postDelayed(runnable, time);
    }

    public static String convertBytes(long size) {
        long Kb = 1 * 1024;
        long Mb = Kb * 1024;
        long Gb = Mb * 1024;
        long Tb = Gb * 1024;
        long Pb = Tb * 1024;
        long Eb = Pb * 1024;

        if (size < Kb) return floatForm(size) + " byte";
        if (size >= Kb && size < Mb) return floatForm((double) size / Kb) + " KB";
        if (size >= Mb && size < Gb) return floatForm((double) size / Mb) + " MB";
        if (size >= Gb && size < Tb) return floatForm((double) size / Gb) + " GB";
        if (size >= Tb && size < Pb) return floatForm((double) size / Tb) + " TB";
        if (size >= Pb && size < Eb) return floatForm((double) size / Pb) + " PB";
        if (size >= Eb) return floatForm((double) size / Eb) + " EB";

        return "anything...";
    }

    public static String floatForm(double d) {
        return new DecimalFormat("#.##").format(d);
    }


    public static List<String> getCameraImages(Context context) {
        final String[] projection = {MediaStore.Images.Media.DATA};
        final String selection = MediaStore.Images.Media.BUCKET_ID + " = ?";
        final String[] selectionArgs = {CAMERA_IMAGE_BUCKET_ID};
        final Cursor cursor = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                projection,
                selection,
                selectionArgs,
                null);
        ArrayList<String> result = new ArrayList<String>(cursor.getCount());
        if (cursor.moveToFirst()) {
            final int dataColumn = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            do {
                final String data = cursor.getString(dataColumn);
                result.add(0, data);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return result;
    }

    public static String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        }
        return capitalize(manufacturer) + " " + model;
    }

    private static String capitalize(String str) {
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        char[] arr = str.toCharArray();
        boolean capitalizeNext = true;

        StringBuilder phrase = new StringBuilder();
        for (char c : arr) {
            if (capitalizeNext && Character.isLetter(c)) {
                phrase.append(Character.toUpperCase(c));
                capitalizeNext = false;
                continue;
            } else if (Character.isWhitespace(c)) {
                capitalizeNext = true;
            }
            phrase.append(c);
        }

        return phrase.toString();
    }

    public static void ifFileExistsOnUrl(final String URLName, final Runnable executeAfter) {
        new Thread() {

            public void run() {
                //your "file checking code" goes here like this
                //write your results to log cat, since you cant do Toast from threads without handlers also...

                try {
                    HttpURLConnection.setFollowRedirects(false);
                    // note : you may also need
                    //HttpURLConnection.setInstanceFollowRedirects(false)

                    HttpURLConnection con = (HttpURLConnection) new URL(URLName).openConnection();
                    con.setRequestMethod("HEAD");
                    if ((con.getResponseCode() == HttpURLConnection.HTTP_OK))
                        if (executeAfter != null)
                            executeAfter.run();
                } catch (Exception e) {
                }
            }
        }.start();
    }


    public static String formatHumanDate(long milliseconds) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliseconds);
        TimeZone tz = TimeZone.getDefault();
        sdf.setTimeZone(tz);
        return sdf.format(calendar.getTime());
    }

    public static String displayDate(long unixSeconds) {
        Date date = new Date(unixSeconds * 1000L); // *1000 is to convert seconds to milliseconds
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd"); // the format of your date
        sdf.setTimeZone(TimeZone.getTimeZone("GMT-4")); // give a timezone reference for formating (see comment at the bottom
        String formattedDate = sdf.format(date);
        return formattedDate;
    }


    public static void highlightErrorRemove(View view) {
        RelativeLayout rectangle;
        if (view != null && view instanceof RelativeLayout)
            rectangle = (RelativeLayout) view;
        else
            return;
        if (rectangle.findViewWithTag(136) != null) {
            rectangle.removeView(rectangle.findViewWithTag(136));
            rectangle.setBackgroundResource(0);
        }
    }


    public static void startActivity(Context context, final Class<? extends Activity> activityToOpen) {
        Intent i = new Intent(context, activityToOpen);
        context.startActivity(i);
    }

    public static void startActivities(Context context, final Class<? extends Activity> activityToOpenParent, final Class<? extends Activity> activityToOpen) {
        Intent i = new Intent(context, activityToOpenParent);
        Intent i2 = new Intent(context, activityToOpen);
        context.startActivities(new Intent[]{i, i2});
    }


    public static int getAge(Date date) {
        Calendar dob = Calendar.getInstance();
        Calendar today = Calendar.getInstance();

        dob.setTime(date);

        int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);

        if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)) {
            age--;
        }

        Integer ageInt = new Integer(age);
        return ageInt;
    }

    public static boolean isTablet(Context context) {
        return (context.getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK)
                >= Configuration.SCREENLAYOUT_SIZE_LARGE;
    }

    public static String getAppVersion(Context context) {
        android.content.pm.PackageInfo appversion = null;

        try {
            appversion = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        if (appversion != null)
            return appversion.versionName;
        else
            return "";

    }


    public static String md5(final String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = MessageDigest
                    .getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return s;
    }

    public static byte[] md5(final byte[] s) {
        final String MD5 = "MD5";
        byte messageDigest[];
        try {
            // Create MD5 Hash
            MessageDigest digest = MessageDigest
                    .getInstance(MD5);
            digest.update(s);
            return digest.digest();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }


    public static int dpToPx(int dp) {
        return (int) (TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, App.getInstance().getResources().getDisplayMetrics()));
    }

    public static String decimalWithCommas(String decimalStringValue) {
        if (!decimalStringValue.contains("."))
            decimalStringValue = decimalStringValue + ".";
        decimalStringValue = decimalStringValue.replaceFirst("(\\d)(\\d)(\\d)(\\d)(?=\\.)", "$1,$2$3$4");
        while (!decimalStringValue.equals(decimalStringValue.replaceFirst("(\\d)(\\d)(\\d)(\\d)(?=\\,)", "$1,$2$3$4"))) {
            decimalStringValue = decimalStringValue.replaceFirst("(\\d)(\\d)(\\d)(\\d)(?=\\,)", "$1,$2$3$4");
        }
        if (decimalStringValue.substring(decimalStringValue.length() - 1).equals(".")) {
            decimalStringValue = decimalStringValue.substring(0, decimalStringValue.length() - 1);
        }
        decimalStringValue = decimalStringValue.replaceFirst("\\.(\\d)$", ".$1" + "0");

        return decimalStringValue;
    }


    public static boolean phoneNumberCorrect(String phone) {
        if (phone.equals(phone.replaceFirst("^\\+1-(\\d{3})-(\\d{3})-(\\d{4})$", ""))) {
            return false;
        }
        return true;
    }

    public static boolean isEmailValid(CharSequence email) {
        boolean asdfsadf = android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static String getCreditCardType(String ccNumber) {

        String visaRegex = "^4[0-9]{12}(?:[0-9]{3})?$";
        String masterRegex = "^5[1-5][0-9]{14}$";
        String amexRegex = "^3[47][0-9]{13}$";
        String dinersClubrRegex = "^3(?:0[0-5]|[68][0-9])[0-9]{11}$";
        String discoverRegex = "^6(?:011|5[0-9]{2})[0-9]{12}$";
        String jcbRegex = "^(?:2131|1800|35\\d{3})\\d{11}$";
        String commonRegex = "^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\\d{3})\\d{11})$";

        try {
            ccNumber = ccNumber.replaceAll("\\D", "");
            return (ccNumber.matches(visaRegex) ? "VISA" :
                    ccNumber.matches(masterRegex) ? "MASTERCARD" :
                            ccNumber.matches(amexRegex) ? "AMEX" :
                                    ccNumber.matches(dinersClubrRegex) ? "DINER" :
                                            ccNumber.matches(discoverRegex) ? "DISCOVER" :
                                                    ccNumber.matches(jcbRegex) ? "JCB" : null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String cardWithSpaces(String cardStringValue) {
        cardStringValue = cardStringValue.replaceFirst("^(\\d{4})(\\d{4})(\\d{4})(\\d{1,4})$", "$1 $2 $3 $4");
        cardStringValue = cardStringValue.replaceFirst("^(\\d{4})(\\d{4})(\\d{1,4})$", "$1 $2 $3");
        cardStringValue = cardStringValue.replaceFirst("^(\\d{4})(\\d{1,4})$", "$1 $2");
        return cardStringValue;
    }

    public static String cardHideVisual(String cardStringValue) {
        cardStringValue = cardStringValue.replaceFirst("^(\\d{4})(\\d{4})(\\d{4})(\\d{1,4})$", "**** **** **** $4");
        return cardStringValue;
    }

    public static String expirationDateWithSlash(String expStringValue) {
        expStringValue = expStringValue.replaceFirst("^(\\d{2})(\\d{1,2})$", "$1/$2");
        if (expStringValue.contains("/")) {
            String monthCheck = expStringValue.substring(0, expStringValue.indexOf("/"));
            if (Integer.parseInt(monthCheck) > 12) {
                expStringValue = expStringValue.replaceFirst("^(\\d{2})/", "12/");
            }
        }
        return expStringValue;
    }

    public static String firstUpperOtherLowerCase(String inString) {
        if (inString != null) {
            inString = inString.substring(0, 1).toUpperCase() + inString.substring(1).toLowerCase();
        }
        return inString;
    }


    public static void hideSoftKeyboard(Context context, EditText textEdit) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(
                Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(textEdit.getWindowToken(), 0);
    }

    public static void showSoftKeyboard(Context context) {

        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }


    public static boolean passwordProperEntered(String password) {
        boolean proper = true;
        if (password.length() < 4)
            proper = false;
        if (password.equals(password.replaceAll("([A-Z]+)", "")))
            proper = false;
        if (password.equals(password.replaceAll("([a-z]+)", "")))
            proper = false;
        if (password.equals(password.replaceAll("([0-9]+)", "")))
            proper = false;

        return proper;
    }

    public static SpannableString MakeSubStringClickable(String fullString, String subString, ClickableSpan clickableSpan) {
        if (fullString.indexOf(subString) < 0) {
            return null;
        } else {
            SpannableString ss = new SpannableString(fullString);

            SubStringIndices subStringIndices = GetSubStringIndices(fullString, subString);
            ss.setSpan(clickableSpan, subStringIndices.startIndex, subStringIndices.endIndex, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

            return ss;
        }
    }

    public static SubStringIndices GetSubStringIndices(String fullString, String subString) {
        int startIndex = fullString.indexOf(subString);
        int endIndex = startIndex + subString.length();
        return new SubStringIndices(startIndex, endIndex);
    }

    private static class SubStringIndices {
        private Integer startIndex;
        private Integer endIndex;

        private SubStringIndices(Integer a, Integer b) {
            this.startIndex = a;
            this.endIndex = b;
        }
    }


    public static RelativeLayout getMainContainerLayout(final Context context) {
        RelativeLayout mainContainerLayout = null;
//        if (context instanceof BaseActivity)
        //mainContainerLayout = (RelativeLayout) (((ViewGroup) ((ViewGroup) ((BaseActivity)context).findViewById(android.R.id.content))).findViewById(R.id.main_layout));
        //mainContainerLayout = (RelativeLayout) ((BaseActivity) context).getWindow().getDecorView().findViewById(android.R.id.content);
        return mainContainerLayout;
    }

    public static RelativeLayout getRootContainerLayout(final Context context) {
        RelativeLayout mainContainerLayout = null;
//        if (context instanceof BaseActivity)
        //   mainContainerLayout = (RelativeLayout) (((ViewGroup) ((ViewGroup) ((BaseActivity)context).findViewById(android.R.id.content))).findViewById(R.id.main_layout));
        //mainContainerLayout = (RelativeLayout) ((BaseActivity) context).getWindow().getDecorView().findViewById(android.R.id.content);
        return mainContainerLayout;
    }


    public static String nullToStringOrString(String string) {
        if (string == null)
            return "";
        return string;
    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }

    public static String dateVisualExtract(String date) {
        return date.replaceAll("^(\\d+)\\-(\\d+)\\-(\\d+)T(.+)$", "$2/$3/$1");
    }

    public static float displayWidth(Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();

        float dpHeight = displayMetrics.heightPixels / displayMetrics.density;
        return displayMetrics.widthPixels / displayMetrics.density;
    }

    public static float displayHeight(Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();

        return displayMetrics.heightPixels / displayMetrics.density;
    }

    public static DisplayMetrics screenMetrics(Context context) {
        Display display = ((BaseActivity) context).getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);
        return outMetrics;
    }


    private static int measureContentWidth(Context context, ListAdapter listAdapter) {
        ViewGroup mMeasureParent = null;
        int maxWidth = 0;
        View itemView = null;
        int itemType = 0;

        final ListAdapter adapter = listAdapter;
        final int widthMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        final int heightMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        final int count = adapter.getCount();
        for (int i = 0; i < count; i++) {
            final int positionType = adapter.getItemViewType(i);
            if (positionType != itemType) {
                itemType = positionType;
                itemView = null;
            }

            if (mMeasureParent == null) {
                mMeasureParent = new FrameLayout(context);
            }

            itemView = adapter.getView(i, itemView, mMeasureParent);
            itemView.measure(widthMeasureSpec, heightMeasureSpec);

            final int itemWidth = itemView.getMeasuredWidth();

            if (itemWidth > maxWidth) {
                maxWidth = itemWidth;
            }
        }

        return maxWidth;
    }

    public static Uri resourceToUri(Context context, int resID) {
        return Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" +
                context.getResources().getResourcePackageName(resID) + '/' +
                context.getResources().getResourceTypeName(resID) + '/' +
                context.getResources().getResourceEntryName(resID));
    }

    public static Bitmap getBitmapMax1000(Bitmap bitmap) {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        double newWidth = width;
        double newHeight = height;
        if (width > height && width > 1000) {
            double scale = width / 1000.0;
            newWidth = width / scale;
            newHeight = height / scale;
        }
        if (width <= height && height > 1000) {
            double scale = height / 1000.0;
            newWidth = width / scale;
            newHeight = height / scale;
        }
        return Bitmap.createScaledBitmap(bitmap, (int) newWidth, (int) newHeight, true);
    }

    public static Bitmap getBitmapMaxThumbnail(Bitmap bitmap) {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        return Bitmap.createScaledBitmap(bitmap, width / 3, height / 3, true);
    }

}
