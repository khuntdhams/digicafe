package com.cab.digicafe.MyCustomClass;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;

import com.cab.digicafe.Helper.SessionManager;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


@SuppressLint("ValidFragment")
public class DatePickerNew extends DialogFragment implements DatePickerDialog.OnDateSetListener {
    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);

    }

    PickDob pickDob;

    public interface PickDob {
        void onSelect(Date date);
    }


    SessionManager sessionManager;
    Context context;
    String selectedDt = "";

    public DatePickerNew(Context context, PickDob pickDob, String selectedDt) {
        this.context = context;
        this.selectedDt = selectedDt;
        sessionManager = new SessionManager(context);
        this.pickDob = pickDob;
    }

    Calendar c = Calendar.getInstance();

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        String dtStart = selectedDt;
        SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy");
        try {
            if (!dtStart.equals("")) {
                Date date = format.parse(dtStart);
                c.setTime(date);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        return new DatePickerDialog(getActivity(), this, year, month, day);
    }

    public void onDateSet(android.widget.DatePicker view, int year, int month, int day) {

        c.set(year, month, day);

        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);

        SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy");
        String date_ = format.format(c.getTime());
        pickDob.onSelect(c.getTime());


    }
}
