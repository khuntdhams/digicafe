package com.cab.digicafe.MyCustomClass;

import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.cab.digicafe.R;

import java.util.ArrayList;

public class AutoCompleteTv {

    public String name;

    public AutoCompleteTv(String name, String refNm, int serviceId, String ref_no) {
        this.name = name;
        this.refNm = refNm;
        this.serviceId = serviceId;
        this.ref_no = ref_no;
    }

    public String refNm;
    boolean isselcted;
    int serviceId = 0;

    public String ref_no;

    String field_json = "";

    public String getField_json() {
        return field_json;
    }

    public void setField_json(String field_json) {
        this.field_json = field_json;
    }

    public AutoCompleteTv(String name, int serviceId) {
        this.name = name;
        this.serviceId = serviceId;
    }

    public AutoCompleteTv(String name, int serviceId, String field_json, String temp) {
        this.name = name;
        this.serviceId = serviceId;
        this.field_json = field_json;
    }

    public AutoCompleteTv(String name, int serviceId, String ref_no) {
        this.name = name;
        this.serviceId = serviceId;
        this.ref_no = ref_no;
    }

    public AutoCompleteTv() {

    }

    @Override
    public String toString() {
        return name;
    }


    public void setautofill(Context context, Spinner act, ArrayList<AutoCompleteTv> al_fill) {
       /* View view = context.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) c.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }*/


        //act.setText("");
        act.clearFocus();
        // ArrayAdapter<Model> adapter = new ArrayAdapter<Model>(getActivity(),R.layout.spinner_item, al_fill);
        ArrayAdapter<AutoCompleteTv> adapter = new ArrayAdapter<AutoCompleteTv>(context, R.layout.spinner_item_dr_filter, al_fill);

        act.setAdapter(adapter);
        // et_nm.setInputType(InputType.TYPE_TEXT_FLAG_MULTI_LINE | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);


    }


}
