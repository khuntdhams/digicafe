package com.cab.digicafe.MyCustomClass;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.cab.digicafe.Helper.SessionManager;
import com.cab.digicafe.Rest.ApiClient;
import com.cab.digicafe.Rest.ApiInterface;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by pc51 on 3/14/2018.
 */

public class SendDeviceInfo {

    public SendDeviceInfo(final Context c, String session, String service_proivider_bridge_id, String device_id, String device_token, final ApiDeviceInfocallback callback) {


        SessionManager sessionManager = new SessionManager(c);
        ApiInterface apiService =
                ApiClient.getClient(c).create(ApiInterface.class);


        Call call;

        JsonObject a1 = new JsonObject();
        try {

            a1.addProperty("service_proivider_bridge_id", service_proivider_bridge_id);
            a1.addProperty("device_id", device_id);
            a1.addProperty("device_token", device_token);


        } catch (JsonParseException e) {
            e.printStackTrace();
        }

        call = apiService.DeviceInfo(session, a1);


        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {

                if (response.isSuccessful()) {

                    try {
                        String a = new Gson().toJson(response.body());


                        callback.onGetResponse(true);


                    } catch (Exception e) {


                    }
                } else {


                    try {

                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Toast.makeText(c, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        // Toast.makeText(CatelogActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }


            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // Log error here since request failed


                Log.e("error message", t.toString());
            }
        });
    }


    public interface ApiDeviceInfocallback {
        void onGetResponse(boolean issuccess);

        void onUpdate(boolean isupdateforce, String ischeck);
    }


}
