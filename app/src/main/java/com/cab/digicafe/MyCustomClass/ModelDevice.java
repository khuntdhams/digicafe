package com.cab.digicafe.MyCustomClass;

/**
 * Created by pc51 on 3/16/2018.
 */

public class ModelDevice {

    String usernm;
    String d_id;     // uniq for user
    String d_token;  // hardware  // change if reset device

    public ModelDevice(String usernm, String d_id, String ind_id, String build_id) {
        this.usernm = usernm;
        this.d_id = d_id;
        this.ind_id = ind_id;
        this.build_id = build_id;
    }

    public ModelDevice(String usernm, String d_id) {
        this.usernm = usernm;
        this.d_id = d_id;
    }

    public String getUsernm() {
        return usernm;
    }


    public void setUsernm(String usernm) {
        this.usernm = usernm;
    }

    public String getD_id() {
        return d_id;
    }

    public void setD_id(String d_id) {
        this.d_id = d_id;
    }

    public String getD_token() {
        return d_token;
    }

    public void setD_token(String d_token) {
        this.d_token = d_token;
    }

    String ind_id = "1", build_id = "";

    public String getInd_id() {
        return ind_id;
    }

    public void setInd_id(String ind_id) {
        this.ind_id = ind_id;
    }

    public String getBuild_id() {
        return build_id;
    }

    public void setBuild_id(String build_id) {
        this.build_id = build_id;
    }

    boolean isdummy = true;

    public boolean isIsdummy() {
        return isdummy;
    }

    public void setIsdummy(boolean isdummy) {
        this.isdummy = isdummy;
    }


}
