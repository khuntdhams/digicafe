package com.cab.digicafe.MyCustomClass.dialogeffect;

import com.cab.digicafe.MyCustomClass.dialogeffect.effects.BaseEffects;
import com.cab.digicafe.MyCustomClass.dialogeffect.effects.FadeIn;
import com.cab.digicafe.MyCustomClass.dialogeffect.effects.Fall;
import com.cab.digicafe.MyCustomClass.dialogeffect.effects.FlipH;
import com.cab.digicafe.MyCustomClass.dialogeffect.effects.FlipV;
import com.cab.digicafe.MyCustomClass.dialogeffect.effects.NewsPaper;
import com.cab.digicafe.MyCustomClass.dialogeffect.effects.RotateBottom;
import com.cab.digicafe.MyCustomClass.dialogeffect.effects.RotateLeft;
import com.cab.digicafe.MyCustomClass.dialogeffect.effects.Shake;
import com.cab.digicafe.MyCustomClass.dialogeffect.effects.SideFall;
import com.cab.digicafe.MyCustomClass.dialogeffect.effects.SlideBottom;
import com.cab.digicafe.MyCustomClass.dialogeffect.effects.SlideDownCat;
import com.cab.digicafe.MyCustomClass.dialogeffect.effects.SlideDownCatAnimDialog;
import com.cab.digicafe.MyCustomClass.dialogeffect.effects.SlideDownCatSms;
import com.cab.digicafe.MyCustomClass.dialogeffect.effects.SlideLeft;
import com.cab.digicafe.MyCustomClass.dialogeffect.effects.SlideRight;
import com.cab.digicafe.MyCustomClass.dialogeffect.effects.SlideTop;
import com.cab.digicafe.MyCustomClass.dialogeffect.effects.SlideTopAnimDialogue;
import com.cab.digicafe.MyCustomClass.dialogeffect.effects.SlideTopS;
import com.cab.digicafe.MyCustomClass.dialogeffect.effects.Slit;

/*
 * Copyright 2014 litao
 * https://github.com/sd6352051/NiftyDialogEffects
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public enum Effectstype {

    Fadein(FadeIn.class),
    Slideleft(SlideLeft.class),
    Slidetop(SlideTop.class),
    SlideDownCat(SlideDownCat.class),
    SlideDownCatAnim(SlideDownCatAnimDialog.class),
    SlideTopAnim(SlideTopAnimDialogue.class),
    SlideDownCatS(SlideDownCatSms.class),
    SlideTopSms(SlideTopS.class),
    SlideBottom(SlideBottom.class),
    Slideright(SlideRight.class),
    Fall(Fall.class),
    Newspager(NewsPaper.class),
    Fliph(FlipH.class),
    Flipv(FlipV.class),
    RotateBottom(RotateBottom.class),
    RotateLeft(RotateLeft.class),
    Slit(Slit.class),
    Shake(Shake.class),
    Sidefill(SideFall.class);
    private Class<? extends BaseEffects> effectsClazz;

    private Effectstype(Class<? extends BaseEffects> mclass) {
        effectsClazz = mclass;
    }

    public BaseEffects getAnimator() {
        BaseEffects bEffects = null;
        try {
            bEffects = effectsClazz.newInstance();
        } catch (ClassCastException e) {
            throw new Error("Can not init animatorClazz instance");
        } catch (InstantiationException e) {
            // TODO Auto-generated catch block
            throw new Error("Can not init animatorClazz instance");
        } catch (IllegalAccessException e) {
            // TODO Auto-generated catch block
            throw new Error("Can not init animatorClazz instance");
        }
        return bEffects;
    }
}
