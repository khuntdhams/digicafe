package com.cab.digicafe.MyCustomClass;

import android.widget.LinearLayout;
import android.widget.TextView;

import com.cab.digicafe.MyCustomClass.dialogeffect.Effectstype;
import com.cab.digicafe.MyCustomClass.dialogeffect.effects.BaseEffects;

public class PlayAnim {

    public void slideUpRl(LinearLayout relativeLayout) {
        BaseEffects animator = Effectstype.SlideDownCat.getAnimator();
        animator.start(relativeLayout);

    }

    public void slideDownRl(LinearLayout relativeLayout) {
        BaseEffects animator = Effectstype.Slidetop.getAnimator();
        animator.start(relativeLayout);
    }

    public void slideUpRlSms(LinearLayout relativeLayout) {
        BaseEffects animator = Effectstype.SlideDownCatS.getAnimator();
        animator.start(relativeLayout);

    }

    public void slideDownRlSms(LinearLayout relativeLayout) {
        BaseEffects animator = Effectstype.SlideTopSms.getAnimator();
        animator.start(relativeLayout);
    }

    public void slideLettTv(TextView relativeLayout) {
        BaseEffects animator = Effectstype.Slideright.getAnimator();
        animator.start(relativeLayout);

    }

    public void slideLeftLl(LinearLayout relativeLayout) {
        BaseEffects animator = Effectstype.Slideright.getAnimator();
        animator.start(relativeLayout);

    }

    public void slideRightLl(LinearLayout relativeLayout) {
        BaseEffects animator = Effectstype.Slideleft.getAnimator();
        animator.start(relativeLayout);

    }


    public void slideUpRlAnim(LinearLayout relativeLayout) {
        BaseEffects animator = Effectstype.SlideDownCatAnim.getAnimator();
        animator.start(relativeLayout);

    }

    public void slideDownRlAnim(LinearLayout relativeLayout) {
        BaseEffects animator = Effectstype.SlideTopAnim.getAnimator();
        animator.start(relativeLayout);
    }


}
