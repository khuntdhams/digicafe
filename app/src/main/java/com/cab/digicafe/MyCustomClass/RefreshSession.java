package com.cab.digicafe.MyCustomClass;

import android.content.Context;
import android.content.Intent;

import com.cab.digicafe.Helper.SessionManager;
import com.cab.digicafe.Rest.ApiClient;
import com.cab.digicafe.Rest.ApiInterface;
import com.cab.digicafe.SplashActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RefreshSession {

    public RefreshSession(final Context c, final SessionManager sessionManager) {
        ApiInterface apiService = ApiClient.getClient2(c).create(ApiInterface.class);

        if (sessionManager == null) {
            return;
        }
        String u_name = sessionManager.getUserWithAggregator();
        String pwd = sessionManager.getPasswd();

        Call call = apiService.getUserLogin(u_name, pwd);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                int statusCode = response.code();

                if (response.isSuccessful()) {

                    try {

                        Intent i = new Intent(c, SplashActivity.class);  // All
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        c.startActivity(i);

                        String cookie = response.headers().get("Set-Cookie");
                        String[] cookies = cookie.split(";");
                        sessionManager.create_Usersession(cookies[0]);

                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                } else {

                }

            }

            @Override
            public void onFailure(Call call, Throwable t) {

            }
        });


    }


}
