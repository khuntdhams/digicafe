package com.cab.digicafe.MyCustomClass;

import android.animation.Animator;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.CardView;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.OvershootInterpolator;
import android.view.animation.Transformation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cab.digicafe.BaseActivity;
import com.cab.digicafe.R;

import java.util.ArrayList;
import java.util.List;


public class AnimationUtils {


    public static void applyActivityAnimations(Context context) {
        BaseActivity base = ((BaseActivity) context);
        View animationView = null;
        // View animationView = base.findViewById(R.id.can_animate_container);
        if (animationView == null) {
            // animationView = base.findViewById(R.id.main_layout);
        }
        if (animationView == null)
            return;
        ViewGroup group = (ViewGroup) animationView;

        for (int i = 0; i < group.getChildCount(); i++) {
            View child = group.getChildAt(i);

            if (child instanceof ImageView) {
                applyAnimation(animationView.getContext(), child, R.anim.animation_appear_from_half, 50 * i);
            } else if (child instanceof TextView) {
                applyAnimation(animationView.getContext(), child, R.anim.animation_appear_from_nothing_tiny, 50 * i);
            } else if (child instanceof RelativeLayout) {
                applyAnimation(animationView.getContext(), child, R.anim.animation_appear_from_nothing_simple, 50 * i);
            } else if (child instanceof LinearLayout) {
                applyAnimation(animationView.getContext(), child, R.anim.animation_fade_out, 50 * i);
            } else if (child instanceof CardView) {
                applyAnimation(animationView.getContext(), child, R.anim.animation_appear_from_half, 50 * i);
            } else {
                applyAnimation(animationView.getContext(), child, R.anim.animation_tap, 50 * i);
            }
        }
    }


    public static void applyAnimation(Context context, View view, int animationResource, int animationOffsetMilisec) {
        Animation anim = android.view.animation.AnimationUtils.loadAnimation(context, animationResource);
        anim.setInterpolator(new AccelerateDecelerateInterpolator());
        anim.setStartOffset(animationOffsetMilisec);
        if (view != null) {
            view.setAnimation(anim);
            view.startAnimation(anim);
        }
    }

    public static void applyAnimationDecelerate(Context context, View view, int animationResource, int animationOffsetMilisec, final Runnable toRunAfterAnimation) {
        Animation anim = android.view.animation.AnimationUtils.loadAnimation(context, animationResource);
        anim.setStartOffset(animationOffsetMilisec);
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (toRunAfterAnimation != null) {
                    toRunAfterAnimation.run();
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        if (view != null) {
            anim.setInterpolator(new DecelerateInterpolator());
            //anim.setDuration(500L);
            view.setAnimation(anim);
            view.startAnimation(anim);
        }
    }

    public static void applyAnimationDecelerateWithRunnable(Context context, View view, int animationResource, int animationOffsetMilisec) {
        Animation anim = android.view.animation.AnimationUtils.loadAnimation(context, animationResource);
        anim.setStartOffset(animationOffsetMilisec);
        if (view != null) {
            anim.setInterpolator(new DecelerateInterpolator());
            anim.setDuration(300L);
            view.setAnimation(anim);
            view.startAnimation(anim);
        }
    }


    public static void applyAnimationRepeatless(Context context, final View view, int animationResource, int animationOffsetMilisec) {
        final Animation anim = android.view.animation.AnimationUtils.loadAnimation(context, animationResource);
        anim.setStartOffset(animationOffsetMilisec);
        anim.setInterpolator(new AccelerateDecelerateInterpolator());
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (view != null) {
                    view.setAnimation(anim);
                    view.startAnimation(anim);
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        if (view != null) {
            view.setAnimation(anim);
            view.startAnimation(anim);
        }
    }

    public static void applyAnimationWithRunnable(Context context, final View view, int animationResource, int animationOffsetMilisec, final Runnable toRunAfterAnimation) {
        final Animation anim = android.view.animation.AnimationUtils.loadAnimation(context, animationResource);
        anim.setStartOffset(animationOffsetMilisec);
        anim.setInterpolator(new AccelerateDecelerateInterpolator());
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (toRunAfterAnimation != null) {
                    toRunAfterAnimation.run();
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        if (view != null) {
            view.setAnimation(anim);
            view.startAnimation(anim);
        }
    }


    public static void translateAnimation(Context context, final View view, final int marginRight, final int marginBottom) {
        TranslateAnimation anim = new TranslateAnimation(0, marginRight, 0, marginBottom);
        anim.setDuration(100);
        anim.setInterpolator(new AccelerateDecelerateInterpolator());
        anim.setAnimationListener(new TranslateAnimation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                view.clearAnimation();
                LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) view.getLayoutParams();
                params.topMargin += marginBottom;
                params.leftMargin += marginRight;
                view.setLayoutParams(params);
            }
        });

        view.startAnimation(anim);

    }


    public static void applyAnimationRelocation(final Context context, final View view, int animationResource, int animationOffsetMilisec, final int relLeft, final int relTop, final int relRight, final int relBottom) {
        Animation anim = android.view.animation.AnimationUtils.loadAnimation(context, animationResource);
        anim.setStartOffset(animationOffsetMilisec);
        anim.setInterpolator(new AccelerateDecelerateInterpolator());
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
//                if(view.getLayoutParams() instanceof LinearLayout.LayoutParams){
//                    ((LoginActivity)context).runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
                view.clearAnimation();
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                        view.getLayoutParams().width, view.getLayoutParams().height);
                layoutParams.setMargins(Utils.dpToPx(relLeft), Utils.dpToPx(relTop), Utils.dpToPx((view instanceof ImageView ? 10 : relRight)), Utils.dpToPx(relBottom));
                view.setLayoutParams(layoutParams);
//                        }
//                    });
//
//                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });


        if (view != null) {
            view.setAnimation(anim);
            view.startAnimation(anim);
        }
    }


    public static void applyAnimationWithFinalHide(Context context, final View view, int animationResource, int animationOffsetMilisec) {
        Animation anim = android.view.animation.AnimationUtils.loadAnimation(context, animationResource);
        anim.setInterpolator(new AccelerateDecelerateInterpolator());
        anim.setStartOffset(animationOffsetMilisec);
        if (view != null) {
            view.setAnimation(anim);
            view.startAnimation(anim);
            anim.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    view.setVisibility(View.GONE);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
        }
    }

    public static void applyAnimationWithFinalShow(Context context, final View view, int animationResource, int animationOffsetMilisec) {
        view.setVisibility(View.VISIBLE);
        Animation anim = android.view.animation.AnimationUtils.loadAnimation(context, animationResource);
        anim.setInterpolator(new AccelerateDecelerateInterpolator());
        anim.setStartOffset(animationOffsetMilisec);
        if (view != null) {
            view.setAnimation(anim);
            view.startAnimation(anim);
            anim.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    view.setVisibility(View.VISIBLE);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
        }
    }

    public static void applyAnimationRepeatless(Context context, LinearLayout container, String text, int size, Typeface typeface, int color) {

        final char[] chars = text.toCharArray();
        int offsetDelay = 0;
        final List<TextView> viewsText = new ArrayList<>();
        final List<Animation> viewsAnimations = new ArrayList<>();
        for (char c : chars) {
            final TextView textView = new TextView(context);
            if (typeface != null)
                textView.setTypeface(typeface);
            textView.setTextSize(size);
            textView.setTextColor(color);
            textView.setText(c + "");
            textView.setPadding(Utils.dpToPx(1), 0, Utils.dpToPx(1), 0);
            container.addView(textView);
            viewsText.add(textView);
            final Animation anim = android.view.animation.AnimationUtils.loadAnimation(context, R.anim.animation_appear_from_nothing);
            anim.setStartOffset(50 * offsetDelay);
            final int finalOffsetDelay = offsetDelay;
            anim.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {

//                    if (textView != null && chars.length == finalOffsetDelay + 1) {
//                        int count = 0;
//                        for (TextView t : viewsText) {
//                            t.setAnimation(viewsAnimations.get(count));
//                            t.startAnimation(viewsAnimations.get(count));
//                            count++;
//                        }
//
//                        textView.setAnimation(anim);
//                        textView.startAnimation(anim);
//                    }
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            viewsAnimations.add(anim);
            if (textView != null) {
                textView.setAnimation(anim);
                textView.startAnimation(anim);
            }
            offsetDelay++;
        }


    }


    public static void applyAnimationSeed(Context context, View view, int paddingFrom, int paddingTo, int viewWidth, int viewHeight, int duration, int animationOffsetMilisec) {
        SeedAnimation anim = new SeedAnimation(view, paddingFrom, paddingTo, viewWidth, viewHeight);
        anim.setStartOffset(animationOffsetMilisec);
        anim.setDuration(duration);
        anim.setInterpolator(new OvershootInterpolator());
        if (view != null) {
            view.setAnimation(anim);
            view.startAnimation(anim);
        }
    }


    static class SeedAnimation extends Animation {
        private View view;
        private int mPaddingFrom;
        private int mPaddingTo;
        private int mViewWidth;
        private int mViewHeight;


        public SeedAnimation(View view, int paddingFrom, int paddingTo, int viewWIdth, int viewHeight) {
            this.view = view;
            this.mPaddingFrom = paddingFrom;
            this.mPaddingTo = paddingTo;
            this.mViewWidth = viewWIdth;
            this.mViewHeight = viewHeight;

        }

        @Override
        protected void applyTransformation(float interpolatedTime, Transformation t) {

            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(mViewWidth + (int) ((mPaddingTo - mPaddingFrom) * interpolatedTime + 0.5), mViewHeight + (int) ((mPaddingTo - mPaddingFrom) * interpolatedTime + 0.5));
            params.addRule(RelativeLayout.CENTER_IN_PARENT);
            view.setPadding(0, (int) ((mPaddingTo - mPaddingFrom) * interpolatedTime + 0.5), 0, 0);
            view.setLayoutParams(params);
            view.setAlpha(interpolatedTime);
            //super.applyTransformation(interpolatedTime, t);
        }

        @Override
        public boolean willChangeBounds() {
            return true;
        }

        @Override
        public boolean isFillEnabled() {
            return true;
        }
    }

    static class AnimateImageFromTo extends Animation {

        View view1;
        View view2;
        int fromX;
        int fromY;
        int toX;
        int toY;
        int width1;
        int height1;
        int width2;
        int height2;

        public AnimateImageFromTo(View view1, View view2, int fromX, int fromY, int toX, int toY, int width1, int height1, int width2, int height2) {
            this.view1 = view1;
            this.view2 = view2;
            this.fromX = fromX;
            this.fromY = fromY;
            this.toX = toX;
            this.toY = toY;
            this.width1 = width1;
            this.width2 = width2;
            this.height1 = height1;
            this.height2 = height2;

        }

        @Override
        protected void applyTransformation(float interpolatedTime, Transformation t) {
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) view1.getLayoutParams();
            params.setMargins(fromX + (int) ((toX - fromX) * interpolatedTime + 0.5), fromY + (int) ((toY - fromY) * interpolatedTime + 0.5), 0, 0);
            params.width = width1 + (int) ((width2 - width1) * interpolatedTime + 0.5);
            params.height = height1 + (int) ((height2 - height1) * interpolatedTime + 0.5);
            view1.setLayoutParams(params);
            view2.setLayoutParams(params);
            //view1.setAlpha(1-interpolatedTime);
            //view2.setAlpha(interpolatedTime);
        }

        @Override
        public boolean willChangeBounds() {
            return true;
        }

        @Override
        public boolean isFillEnabled() {
            return true;
        }
    }

    public static void resizeAnimation(final View view, int newWidth, int newHeight, int duration) {
        final int mCurrentHeight = view.getHeight();
        final int mNewHeight = newHeight;

        Animation a = new ResizeAnimation(view, newWidth, newHeight);
        a.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                view.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        a.setDuration(duration);
        a.setInterpolator(new AccelerateDecelerateInterpolator());
        view.setAnimation(a);
        view.startAnimation(a);

    }

    public static void resizeAnimationVerticalShow(final View view) {
        view.setVisibility(View.VISIBLE);
        final int mCurrentHeight = view.getHeight();
        final int mNewHeight = 0;
        if (view.getTag() == null || (int) view.getTag() == 0) {
            view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
            int height = view.getMeasuredHeight();
            view.setTag(height);
        }
        Animation a = new ResizeAnimation(view, view.getWidth(), (int) view.getTag());
        a.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        a.setDuration(100);
        a.setInterpolator(new AccelerateDecelerateInterpolator());
        view.setAnimation(a);
        view.startAnimation(a);

    }

    public static void resizeAnimationVerticalHide(final View view) {
        final int mCurrentHeight = view.getHeight();
        final int mNewHeight = 0;
        Animation a = new ResizeAnimation(view, view.getWidth(), 0);
        a.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                view.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        a.setDuration(100);
        a.setInterpolator(new AccelerateDecelerateInterpolator());
        view.setAnimation(a);
        view.startAnimation(a);

    }


//    static class ResizeAnimation extends Animation{
//        private View view;
//        private int mNewWidth;
//        private int mNewHeight;
//        private int mCurrentHeight;
//        private int mCurrentWidth;
//
//
//
//
//        public ResizeAnimation(View view, int newWIdth, int newHeight){
//            this.view = view;
//            this.mNewWidth = newWIdth;
//            this.mNewHeight = newHeight;
//            this.mCurrentWidth = view.getWidth();
//            this.mCurrentHeight = view.getHeight();
//
//        }
//
//        @Override
//        protected void applyTransformation(float interpolatedTime, Transformation t) {
//            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(mCurrentWidth+(int)((mNewWidth-mCurrentWidth)*interpolatedTime+0.5), mCurrentHeight+(int)((mNewHeight-mCurrentHeight)*interpolatedTime+0.5));
//            params.addRule(RelativeLayout.CENTER_IN_PARENT);
//            if(mNewHeight>mCurrentHeight)
//                view.setAlpha(interpolatedTime);
//            else
//                view.setAlpha(1 - interpolatedTime);
//
//            view.setLayoutParams(params);
//
//        }
//
//        @Override
//        public boolean willChangeBounds() {
//            return true;
//        }
//
//        @Override
//        public boolean isFillEnabled() {
//            return true;
//        }
//    }


    static class ResizeAnimation extends Animation {
        private View view;
        private int mNewWidth;
        private int mNewHeight;
        private int mCurrentHeight;
        private int mCurrentWidth;


        public ResizeAnimation(View view, int newWIdth, int newHeight) {
            this.view = view;
            this.mNewWidth = newWIdth;
            this.mNewHeight = newHeight;
            this.mCurrentWidth = view.getWidth();
            this.mCurrentHeight = view.getHeight();

        }

        @Override
        protected void applyTransformation(float interpolatedTime, Transformation t) {
            if (view.getLayoutParams() instanceof RelativeLayout.LayoutParams) {
                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) view.getLayoutParams();
                params.width = mCurrentWidth + (int) ((mNewWidth - mCurrentWidth) * interpolatedTime + 0.5);
                params.height = mCurrentHeight + (int) ((mNewHeight - mCurrentHeight) * interpolatedTime + 0.5);
                view.setLayoutParams(params);
            } else {
                LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) view.getLayoutParams();
                params.width = mCurrentWidth + (int) ((mNewWidth - mCurrentWidth) * interpolatedTime + 0.5);
                params.height = mCurrentHeight + (int) ((mNewHeight - mCurrentHeight) * interpolatedTime + 0.5);
                view.setLayoutParams(params);
            }
            view.requestLayout();

        }

        @Override
        public boolean willChangeBounds() {
            return true;
        }

        @Override
        public boolean isFillEnabled() {
            return true;
        }
    }


    static class AnimateMargins extends Animation {

        View view;
        int left;
        int top;
        int right;
        int bottom;
        int leftTo;
        int topTo;
        int rightTo;
        int bottomTo;

        public AnimateMargins(View view, int left, int top, int right, int bottom, int leftTo, int topTo, int rightTo, int bottomTo) {
            this.view = view;
            this.left = left;
            this.top = top;
            this.right = right;
            this.bottom = bottom;
            this.leftTo = leftTo;
            this.topTo = topTo;
            this.rightTo = rightTo;
            this.bottomTo = bottomTo;
        }

        @Override
        protected void applyTransformation(float interpolatedTime, Transformation t) {
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) view.getLayoutParams();
            params.setMargins(left + (int) ((leftTo - left) * interpolatedTime + 0.5), top + (int) ((topTo - top) * interpolatedTime + 0.5), right + (int) ((rightTo - right) * interpolatedTime + 0.5), bottom + (int) ((bottomTo - bottom) * interpolatedTime + 0.5));
            view.setLayoutParams(params);
        }

        @Override
        public boolean willChangeBounds() {
            return true;
        }

        @Override
        public boolean isFillEnabled() {
            return true;
        }
    }

    public static void marginsAnimation(final View view, int left, int top, int right, int bottom, int leftTo, int topTo, int rightTo, int bottomTo, int duration, int delay, final Runnable runnable) {

        Animation a = new AnimateMargins(view, left, top, right, bottom, leftTo, topTo, rightTo, bottomTo);
        a.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (runnable != null)
                    runnable.run();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        view.setAnimation(a);
        a.setDuration(duration);
        a.setInterpolator(new AccelerateDecelerateInterpolator());
        a.setStartOffset(delay);
        a.startNow();

    }

    public static abstract class AbstractAnimatorListener implements Animator.AnimatorListener {
        @Override
        public void onAnimationStart(Animator animator) {

        }

        @Override
        public void onAnimationEnd(Animator animator) {

        }

        @Override
        public void onAnimationCancel(Animator animator) {

        }

        @Override
        public void onAnimationRepeat(Animator animator) {

        }
    }

    public static void animateImageFromTo(Context context, View view1, final View view2, int duration) {
        RelativeLayout mainContainerLayout = Utils.getMainContainerLayout(context);
        if (mainContainerLayout != null) {
            int fromLoc[] = new int[2];
            view1.getLocationOnScreen(fromLoc);
            float startX = fromLoc[0];
            float startY = fromLoc[1];

            RelativeLayout checkAnimationLayoutExists = (RelativeLayout) mainContainerLayout.findViewById(R.id.animation_relative_layout);

            if (checkAnimationLayoutExists == null) {
                RelativeLayout animationLayout = (RelativeLayout) View.inflate(context, R.layout.animation_relative_layout, null);
                mainContainerLayout.addView(animationLayout);
                checkAnimationLayoutExists = (RelativeLayout) mainContainerLayout.findViewById(R.id.animation_relative_layout);
            }


            int toLoc[] = new int[2];
            view2.getLocationOnScreen(toLoc);
            float destX = toLoc[0];
            float destY = toLoc[1];

            final View sticker;
            final View sticker2;

            if (view1 instanceof ImageView) {
                sticker = new ImageView(context);
                ((ImageView) sticker).setImageDrawable(((ImageView) view1).getDrawable());
            } else if (view1 instanceof TextView) {
                sticker = new TextView(context);
                ((TextView) sticker).setText(((TextView) view1).getText().toString());
                ((TextView) sticker).setTypeface(((TextView) view1).getTypeface());
                ((TextView) sticker).setTextSize(Utils.dpToPx((int) ((TextView) view1).getTextSize()));
                ((TextView) sticker).setTextColor(((TextView) view1).getCurrentTextColor());
            } else {
                sticker = new View(context);
            }

            if (view2 instanceof ImageView) {
                sticker2 = new ImageView(context);
                ((ImageView) sticker2).setImageDrawable(((ImageView) view2).getDrawable());
            } else if (view2 instanceof TextView) {
                sticker2 = new TextView(context);
                ((TextView) sticker2).setText(((TextView) view2).getText().toString());
                ((TextView) sticker2).setTypeface(((TextView) view2).getTypeface());
                ((TextView) sticker2).setTextSize(Utils.dpToPx((int) ((TextView) view2).getTextSize()));
                ((TextView) sticker2).setTextColor(((TextView) view2).getCurrentTextColor());
            } else {
                sticker2 = new View(context);
            }


            sticker.setLayoutParams(new RelativeLayout.LayoutParams(view1.getWidth(), view1.getHeight()));


            sticker2.setLayoutParams(new RelativeLayout.LayoutParams(view1.getWidth(), view1.getHeight()));

            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) sticker.getLayoutParams();
            params.setMargins((int) startX, (int) startY, 0, 0);
            sticker.setLayoutParams(params);
            sticker2.setLayoutParams(params);
            checkAnimationLayoutExists.addView(sticker2);
            checkAnimationLayoutExists.addView(sticker);

            checkAnimationLayoutExists.setVisibility(View.VISIBLE);
            final RelativeLayout finalCheckAnimationLayoutExists = checkAnimationLayoutExists;
            Animation a = new AnimateImageFromTo(sticker, sticker2, (int) startX, (int) startY, (int) destX, (int) destY, view1.getWidth(), view1.getHeight(), view2.getWidth(), view2.getHeight());

            a.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    sticker.clearAnimation();
                    sticker2.clearAnimation();
                    finalCheckAnimationLayoutExists.removeView(sticker);
                    finalCheckAnimationLayoutExists.removeView(sticker2);
                    view2.setVisibility(View.VISIBLE);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            a.setInterpolator(new AccelerateDecelerateInterpolator());
            sticker.setAnimation(a);
            a.setDuration(duration);
            view1.setVisibility(View.INVISIBLE);
            view2.setVisibility(View.INVISIBLE);
            a.startNow();
        }
    }

}
