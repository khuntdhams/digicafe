package com.cab.digicafe.MyCustomClass;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import com.cab.digicafe.R;
import com.cab.digicafe.photopicker.activity.PickImageActivity;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static android.app.Activity.RESULT_OK;

public class ShowGalleryCamera {


    AppCompatActivity context;

    public ShowGalleryCamera(AppCompatActivity context) {
        this.context = context;
    }

    public void showPictureDialog() {
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(context);
        String[] pictureDialogItems = {"Photo Gallery", "Camera"};
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 1:
                                // openImagePickerIntent();

                                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                                File root = Environment.getExternalStoragePublicDirectory(context.getString(R.string.app_name) + "/Camera Image/");
                                root.mkdirs();

                                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
                                String currentTimeStamp = dateFormat.format(new Date());

                                dest = new File(root, "IMG" + currentTimeStamp + ".jpeg");// IMG001 image name should ne picture001, picture002,picture003 soon  ffmpeg takes as input valid

                                Uri photoURI = FileProvider.getUriForFile(context, context.getPackageName() + ".provider", dest);
                                intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                                context.startActivityForResult(intent, 102);

                                break;
                            case 0:
                                cameraIntent();
                                break;
                        }
                    }
                });
        pictureDialog.show();
    }

    public void showMultiPictureDialog() {
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(context);
        String[] pictureDialogItems = {"Photo Gallery", "Camera"};
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 1:
                                // openImagePickerIntent();

                                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                                File root = Environment.getExternalStoragePublicDirectory(context.getString(R.string.app_name) + "/Camera Image/");
                                root.mkdirs();

                                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
                                String currentTimeStamp = dateFormat.format(new Date());

                                dest = new File(root, "IMG" + currentTimeStamp + ".jpeg");// IMG001 image name should ne picture001, picture002,picture003 soon  ffmpeg takes as input valid

                                Uri photoURI = FileProvider.getUriForFile(context, context.getPackageName() + ".provider", dest);
                                intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                                context.startActivityForResult(intent, 102);

                                break;
                            case 0:
                                Intent mIntent = new Intent(context, PickImageActivity.class);
                                mIntent.putExtra(PickImageActivity.KEY_LIMIT_MAX_IMAGE, 30);
                                mIntent.putExtra(PickImageActivity.KEY_LIMIT_MIN_IMAGE, 1);
                                context.startActivityForResult(mIntent, 98);

                                break;
                        }
                    }
                });
        pictureDialog.show();
    }


    private void cameraIntent() {


        Intent pictureActionIntent = null;
        pictureActionIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        context.startActivityForResult(pictureActionIntent, 101);

    }

    File dest;
    CallGetImg callGetImg;

    public interface CallGetImg {
        public void onGetImg(String path);
    }


    public void onGetResult(int requestCode, int resultCode, Intent data, CallGetImg callGet) {

        this.callGetImg = callGet;
        if (requestCode == 102 && resultCode == RESULT_OK) { // Camera


            try {

               /* Bitmap bitmap = BitmapFactory.decodeFile(dest.getAbsolutePath());

                bitmap = Bitmap.createScaledBitmap(bitmap, 400, 400, true);

                int rotate = 0;
                try {
                    ExifInterface exif = new ExifInterface(dest.getAbsolutePath());
                    int orientation = exif.getAttributeInt(
                            ExifInterface.TAG_ORIENTATION,
                            ExifInterface.ORIENTATION_NORMAL);

                    switch (orientation) {
                        case ExifInterface.ORIENTATION_ROTATE_270:
                            rotate = 270;
                            break;
                        case ExifInterface.ORIENTATION_ROTATE_180:
                            rotate = 180;
                            break;
                        case ExifInterface.ORIENTATION_ROTATE_90:
                            rotate = 90;
                            break;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Matrix matrix = new Matrix();
                matrix.postRotate(rotate);
                bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(),
                        bitmap.getHeight(), matrix, true);*/

                if (dest != null && !dest.getAbsolutePath().isEmpty()) {
                    compressImg(dest.getAbsolutePath());
                    //callGetImg.onGetImg(dest.getAbsolutePath());
                }

                //storeImageTosdCard(bitmap);
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }


        } else if (requestCode == 101 && resultCode == RESULT_OK) { // Gallery

            Uri selectedImage = data.getData();
            String[] filePath = {MediaStore.Images.Media.DATA};
            Cursor c = context.getContentResolver().query(selectedImage, filePath,
                    null, null, null);
            if (c != null && c.getCount() > 0) {
                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePath[0]);
                String path = c.getString(columnIndex);
                c.close();

                if (path != null) {
                    //callGetImg.onGetImg(path);
                    compressImg(path);
                }
            }
        }
    }

    public void compressImg(final String path) {


        new AsyncTask<String, Void, String>() {
            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);

                callGetImg.onGetImg(s);
            }

            @Override
            protected String doInBackground(String... strings) {

                try {

                    try {


                        File compressedImageFile = null;
                        String strFile = "";
                        try {
                            compressedImageFile = new Compressor(context).compressToFile(new File(path));
                        } catch (IOException e) {
                        }
                        File newFile = new File(context.getCacheDir().getPath() + File.separator + "images" + File.separator + System.currentTimeMillis() + ".jpg");
                        if (rename(compressedImageFile, newFile)) {
                        } else {
                        }
                        strFile = String.valueOf(newFile);

                        return strFile;


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    //strFile = String.valueOf(compressedImageFile);

                } catch (Exception e) {
                    e.printStackTrace();
                }

                return new File(path).getAbsolutePath();
            }
        }.execute();


    }


    private boolean rename(File from, File to) {
        return from.getParentFile().exists() && from.exists() && from.renameTo(to);
    }


}
