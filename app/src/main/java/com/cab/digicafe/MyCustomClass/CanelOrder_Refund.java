package com.cab.digicafe.MyCustomClass;


import android.app.Activity;
import android.support.v7.app.AlertDialog;
import android.view.View;

import com.cab.digicafe.Dialogbox.AddRefundDescription;
import com.cab.digicafe.Dialogbox.RefundStatusDialog;
import com.cab.digicafe.R;
import com.razorpay.RazorpayClient;
import com.razorpay.RazorpayException;
import com.razorpay.Refund;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


/**
 * Created by pc51 on 4/21/2018.
 */

public class CanelOrder_Refund {


    String pay_id;

    public interface RefundCallback {
        public void showconfirmcancel();

        public void showdescription();

        public void refunded();
    }

    RefundCallback callback;

    AlertDialog showconfiramdialog;
    String notes;

    Activity activity;


    public CanelOrder_Refund(final Activity activity, String pay_id, RefundCallback callback) {


        this.pay_id = pay_id;
        this.activity = activity;
        this.callback = callback;

      /*  new CustomDialogClass(activity, new CustomDialogClass.OnDialogClickListener() {
            @Override
            public void onDialogImageRunClick(int positon) {
                if (positon == 0) {

                } else if (positon == 1) {

                    new AddRefundDescription(activity, new AddRefundDescription.OnDialogClickListener() {
                        @Override
                        public void onDialogImageRunClick(int positon,String note) {
                            if (positon == 0) {

                            } else if (positon == 1) {
                                notes =note;
                                //refundapi();
                                refund();

                            }
                        }
                    },"").show();


                }
            }
        },"Are you sure you want to cancel order ?").show();*/

        new AddRefundDescription(activity, new AddRefundDescription.OnDialogClickListener() {
            @Override
            public void onDialogImageRunClick(int positon, String note) {
                if (positon == 0) {

                } else if (positon == 1) {
                    notes = note;
                    //refundapi();
                    refund();

                }
            }
        }, "").show();


    }


    public void refund() {

        try {
            RazorpayClient razorpay = new RazorpayClient(activity.getString(R.string.api_key), activity.getString(R.string.key_secret));
            // Full Refund
            //Refund refund = razorpay.Payments.refund(pay_id);

            JSONObject refundRequest = new JSONObject();
            try {
                refundRequest.put("payment_id", pay_id);

                JSONArray ja_notes = new JSONArray();
                ja_notes.put(notes);

                refundRequest.put("notes", ja_notes);


            } catch (JSONException e) {
                e.printStackTrace();
            }
            Refund refund = razorpay.Payments.refund(refundRequest);

            // String str_refund = new Gson().toJson(refund);

            try {

                new RefundStatusDialog(activity, new RefundStatusDialog.OnDialogClickListener() {
                    @Override
                    public void onDialogImageRunClick(int pos) {
                        callback.refunded();
                    }

                }, refund.toString(), false).show();

            } catch (Exception e) {

            }


            //Log.e("","");


            // Partial Refund
            //JSONObject refundRequest = new JSONObject();
            //refundRequest.put("amount", amount); // Amount should be in paise
            //Refund refund = razorpay.Payments.refund("<payment_id>", refundRequest);
        } catch (RazorpayException e) {
            // Handle Exception

            View v = null;
            // Inad.showmsg(v, e.getMessage(), activity);

            new RefundStatusDialog(activity, new RefundStatusDialog.OnDialogClickListener() {
                @Override
                public void onDialogImageRunClick(int pos) {
                    callback.refunded();
                }

            }, e.getMessage() + "\nPayment id - " + pay_id + "\n" + "Please contact us if you have any issue.", false).show();
            //System.out.println(e.getMessage());
        }
    }


}