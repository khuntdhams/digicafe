package com.cab.digicafe.MyCustomClass;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.cab.digicafe.Helper.SessionManager;
import com.cab.digicafe.Model.ChitDetails;
import com.cab.digicafe.Rest.ApiClient;
import com.cab.digicafe.Rest.ApiInterface;
import com.google.gson.Gson;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by pc51 on 3/14/2018.
 */

public class GetProductDetail {

    public GetProductDetail(final int poss, final Context c, String chit_hash_id, String chit_id, final Apicallback callback) {


        SessionManager sessionManager = new SessionManager(c);
        ApiInterface apiService =
                ApiClient.getClient(c).create(ApiInterface.class);


        Call call;

        call = apiService.getchitDetail(sessionManager.getsession(), chit_hash_id, chit_id, "");

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {

                if (response.isSuccessful()) {

                    try {
                        String a = new Gson().toJson(response.body());
                        JSONObject Jsonresponse = new JSONObject(a);
                        JSONObject jObjdata = Jsonresponse.getJSONObject("response");
                        JSONObject jObjResponse = jObjdata.getJSONObject("data");
                        ChitDetails obj = new Gson().fromJson(jObjResponse.toString(), ChitDetails.class);
                        callback.onGetResponse(obj, true, poss);

                    } catch (Exception e) {


                    }
                } else {
                    try {

                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Toast.makeText(c, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        // Toast.makeText(CatelogActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }


            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // Log error here since request failed


                Log.e("error message", t.toString());
            }
        });
    }


    public interface Apicallback {
        void onGetResponse(ChitDetails a, boolean issuccess, int poss);

        void onUpdate(boolean isupdateforce, String ischeck);
    }


}
