package com.cab.digicafe.MyCustomClass;


/**
 * Create Application utility
 * Hold all scope variables.
 */


public class ApplicationUtil {


    public static int focusedId = 0;

    public static int getFocusedId() {
        return focusedId;
    }

    public static void setFocusedId(int id) {
        focusedId = id;
    }
}