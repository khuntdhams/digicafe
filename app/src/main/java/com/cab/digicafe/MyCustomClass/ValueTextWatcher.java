package com.cab.digicafe.MyCustomClass;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

public class ValueTextWatcher implements TextWatcher {

    private final EditText et1;
    private final EditText et2;

    Context con;

    public ValueTextWatcher(EditText editText1, EditText editText2, Context context) {

        this.et1 = editText1;
        this.et2 = editText2;
        this.con = context;
    }

    @Override
    public void afterTextChanged(Editable s) {
        try {


            // Initializes the double values and result

            if (ApplicationUtil.getFocusedId() == et1.getId()) {
                et2.setText(et1.getText().toString().trim());
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {


    }


    private String removeLastDot(String str) {
        int last = str.length() - 1;
        if (last > 0 && str.charAt(last) == '.') {
            str = str.substring(0, last);

        }
        return str;
    }


}
