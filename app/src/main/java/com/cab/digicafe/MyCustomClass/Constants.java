package com.cab.digicafe.MyCustomClass;

public class Constants {
    public static final int typingFalseDelayInMilisec = 3000;

    // Don't change below values
    public static final int DIALOG_MARGIN_IN_DP = 40;
    public static final int PICK_IMAGES_PERMISSION_REQUEST = 1;
    public static final int CAPTURE_IMAGES_PERMISSION_REQUEST = 2;
    public static final int CAPTURE_IMAGE_FROM_CAMERA = 3;
    public static final int PICK_IMAGE_FROM_LIBRARY = 4;
    public static final int IMAGE_CROP = 5;
    public static final int CHECK_RIGHTS_MICROPHONE = 6;
    public static final int CHECK_RIGHTS_WIFISEARCH = 7;
    public static final int CHECK_RIGHTS_WRITE_STORAGE = 8;
    public static final int CHECK_RIGHTS_READ_STORAGE = 8;


    public static final String referrer_detail_node = "who_referred_me";
    public static final String my_referral = "my_referral";
    public static final String app_environment = "";
    public static String share_shop_node = "";


}
