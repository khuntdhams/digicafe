package com.cab.digicafe.MyCustomClass;

import org.json.JSONException;
import org.json.JSONObject;

public abstract class JsonObjParse {

    // use this class just for weather its contain (has) string or not

    public static String getValueNull(String jsonstr, String key) {

        String val = "";

        if (jsonstr.equals("")) {
            return null;
        } else {
            JSONObject jObject;
            try {
                jObject = new JSONObject(jsonstr);
                if (jObject.has(key)) val = jObject.getString(key);
                else return null;
            } catch (JSONException e) {
                return null; // json has not key
            }
        }
        return val;

    }

    public static String getValueEmpty(String jsonstr, String key) {

        String val = "";
        if (jsonstr == null) {
            return "";
        } else if (jsonstr.equals("")) {
            return "";
        } else {
            JSONObject jObject;
            try {
                jObject = new JSONObject(jsonstr);
                if (jObject.has(key)) val = jObject.getString(key);
                else return "";
            } catch (JSONException e) {
                return ""; // json has not key
            }
        }
        return val;
    }

    public static int getValueEmptyInt(String jsonstr, String key) {

        int val = 0;
        if (jsonstr == null) {
            return 0;
        } else if (jsonstr.equals("")) {
            return 0;
        } else {
            JSONObject jObject;
            try {
                jObject = new JSONObject(jsonstr);
                if (jObject.has(key)) val = jObject.getInt(key);
                else return 0;
            } catch (JSONException e) {
                return 0; // json has not key
            }
        }
        return val;
    }

    public static int getValueZero(String jsonstr, String key) {

        int val = 0;
        if (jsonstr == null) {
            return 0;
        } else if (jsonstr.equals("")) {
            return 0;
        } else {
            JSONObject jObject;
            try {
                jObject = new JSONObject(jsonstr);
                if (jObject.has(key)) val = jObject.getInt(key);
                else return 0;
            } catch (JSONException e) {
                return 0; // json has not key
            }
        }
        return val;
    }

    public static String getValueFromJsonObj(JSONObject jsonstr, String key) {

        String val = "";
        try {
            if (jsonstr.has(key)) val = jsonstr.getString(key);
            else return "";
        } catch (JSONException e) {
            return ""; // json has not key
        }

        return val;
    }


}
