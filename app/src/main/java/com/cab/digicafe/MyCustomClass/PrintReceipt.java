package com.cab.digicafe.MyCustomClass;

import android.app.Activity;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.Toast;

import com.cab.digicafe.Activities.FileActivity;
import com.cab.digicafe.Activities.MyAppBaseActivity;
import com.cab.digicafe.ChitlogActivity;
import com.cab.digicafe.Database.SettingDB;
import com.cab.digicafe.Database.SettingModel;
import com.cab.digicafe.Dialogbox.AddComment;
import com.cab.digicafe.Fragments.SummaryDetailFragmnet;
import com.cab.digicafe.Helper.Inad;
import com.cab.digicafe.Helper.SessionManager;
import com.cab.digicafe.Model.Chit;
import com.cab.digicafe.Model.Chitproducts;
import com.cab.digicafe.Model.ModelFile;
import com.cab.digicafe.PrintView.BluetoothPrinter;
import com.cab.digicafe.PrintView.DeviceListActivity;
import com.cab.digicafe.R;
import com.cab.digicafe.Rest.ApiClient;
import com.cab.digicafe.Rest.ApiInterface;
import com.cab.digicafe.Timeline.DateTimeUtils;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import okhttp3.Headers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by pc51 on 4/18/2018.
 */

public class PrintReceipt extends MyAppBaseActivity {

    private static final int REQUEST_CONNECT_DEVICE = 1;
    private static final int REQUEST_ENABLE_BT = 2;
    private BluetoothAdapter mBluetoothAdapter;
    private UUID applicationUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    private ProgressDialog mBluetoothConnectProgressDialog;
    private BluetoothSocket mBluetoothSocket;
    BluetoothDevice mBluetoothDevice;
    static BluetoothPrinter mPrinter;

    View tv_nodata;


    Chit chitprint;
    ArrayList<Chitproducts> productlist;
    String address;
    String session = "";
    Bitmap icon;
    String symbol = "";

    public void PrintReceiptStart(String session, String address, Chit chitprint, ArrayList<Chitproducts> productlist, Mycall callback, String symbol) {

        this.chitprint = chitprint;
        this.productlist = productlist;
        this.address = address;
        this.session = session;
        this.callback = callback;
        this.symbol = symbol;

        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            Toast.makeText(this, "Not suppot for this device!", Toast.LENGTH_SHORT).show();
        } else {
            if (!mBluetoothAdapter.isEnabled()) {
                Intent enableBtIntent = new Intent(
                        BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent,
                        REQUEST_ENABLE_BT);
            } else {
                if (mBluetoothAdapter.getBondedDevices().size() == 0) {
                    ListPairedDevices();
                    Intent connectIntent = new Intent(PrintReceipt.this, DeviceListActivity.class);
                    startActivityForResult(connectIntent, REQUEST_CONNECT_DEVICE);
                } else {
                    mBluetoothDevice = mBluetoothAdapter.getBondedDevices().iterator().next();   // Get first paired device

                    mBluetoothConnectProgressDialog = ProgressDialog.show(this,
                            "Connecting...", mBluetoothDevice.getName() + " : "
                                    + mBluetoothDevice.getAddress(), true, false);

                    afterconnecet();

                }
            }
        }
    }

    private String leftRightAlign(String str1, String str2) {
        String ans = str1 + str2;
        if (str1.length() < 35) {
            int n = (35 - str1.length() + str2.length());
            ans = str1 + new String(new char[n]).replace("\0", " ") + str2;
        } else {
            String sub = str1.substring(0, 35);
            String remain = str1.substring(35);
            ans = "" + sub + "     " + str2 + "\n";


            while (remain.length() > 30) {
                String prevstr = remain.substring(0, 30);
                remain = remain.substring(30);
                ans = ans + "      " + prevstr + "\n";
            }

            if (remain.length() <= 30) {
                ans = ans + "      " + remain + "\n";
            }
        }
        return ans;
    }

    private void ListPairedDevices() {
        Set<BluetoothDevice> mPairedDevices = mBluetoothAdapter
                .getBondedDevices();
        if (mPairedDevices.size() > 0) {
            for (BluetoothDevice mDevice : mPairedDevices) {
                //   Log.v(TAG, "PairedDevices: " + mDevice.getName() + "  " + mDevice.getAddress());
            }
        }
    }


    public void onActivityResult(int mRequestCode, int mResultCode,
                                 Intent mDataIntent) {
        super.onActivityResult(mRequestCode, mResultCode, mDataIntent);

        switch (mRequestCode) {
            case REQUEST_CONNECT_DEVICE:
                if (mResultCode == Activity.RESULT_OK) {
                    Bundle mExtra = mDataIntent.getExtras();
                    String mDeviceAddress = mExtra.getString("DeviceAddress");
                    //Log.v(TAG, "Coming incoming address " + mDeviceAddress);
                    mBluetoothDevice = mBluetoothAdapter.getRemoteDevice(mDeviceAddress);

                    mBluetoothConnectProgressDialog = ProgressDialog.show(this,
                            "Connecting...", mBluetoothDevice.getName() + " : "
                                    + mBluetoothDevice.getAddress(), true, false);

                    afterconnecet();

                }
                break;

            case REQUEST_ENABLE_BT:
                if (mResultCode == Activity.RESULT_OK) {
                    mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
                    if (mBluetoothAdapter.getBondedDevices().size() == 0) {
                        ListPairedDevices();
                        Intent connectIntent = new Intent(PrintReceipt.this, DeviceListActivity.class);
                        startActivityForResult(connectIntent, REQUEST_CONNECT_DEVICE);
                    } else {
                        mBluetoothDevice = mBluetoothAdapter.getBondedDevices().iterator().next();   // Get first paired device
                        mBluetoothConnectProgressDialog = ProgressDialog.show(this,
                                "Connecting...", mBluetoothDevice.getName() + " : "
                                        + mBluetoothDevice.getAddress(), true, false);

                        afterconnecet();

                    }
                } else {
                    Toast.makeText(PrintReceipt.this, "Not Support !", Toast.LENGTH_SHORT).show();
                }
                break;

            case 7:
                if (mResultCode == Activity.RESULT_OK) {
                    Bitmap bitmap;
                    int orientation;
                    String albumnm = "";


                    try {
                        bitmap = BitmapFactory.decodeFile(this.f3f.getPath());
                        this.exif = new ExifInterface(this.f3f.getPath());
                        if (bitmap != null) {


                            String picturePath = f3f.getPath();
                            al_selet = new ArrayList<>();
                            al_selet.add(new ModelFile(picturePath, true));
                            addComment.onAddedImg(al_selet);


                        } else {
                            Toast.makeText(getApplicationContext(), "Picture not taken !", Toast.LENGTH_SHORT).show();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                break;


            case 8:
                if (mResultCode == Activity.RESULT_OK) {
                    String picturePath = mDataIntent.getExtras().getString("al_selet");
                    al_selet = new Gson().fromJson(picturePath, new TypeToken<ArrayList<ModelFile>>() {
                    }.getType());
                    addComment.onAddedImg(al_selet);
                }

                break;


        }


    }

    public void afterconnecet() {
        mPrinter = new BluetoothPrinter(mBluetoothDevice);
        mPrinter.connectPrinter(new BluetoothPrinter.PrinterConnectListener() {

            @Override
            public void onConnected() {
                mBluetoothConnectProgressDialog.dismiss();
                //print();
                //print_new();
                print_receipt();
                Inad.showmsg(tv_nodata, "Connected !", PrintReceipt.this);

            }

            @Override
            public void onFailed() {

                //print_receipt();
                mBluetoothConnectProgressDialog.dismiss();
                Inad.showmsg(tv_nodata, "Could Not Connect To Socket !", PrintReceipt.this);
                callback.printfail();
                //Log.d("BluetoothPrinter", "Conection failed");
            }
        });

    }

    public interface Mycall {
        public void printsuccess();

        public void printfail();

        public void assignarchived();
    }

    Mycall callback;

    boolean isEnquiry = false;
    boolean isdiaries = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle extras = getIntent().getExtras();
        isdiaries = extras.getBoolean("isdiaries", false);
        isEnquiry = extras.getBoolean("isEnquiry", false);


    }


    String str_sub_total = "Sub-Total", str_sub_total_value = "";
    String str_cgst = "CGST", str_cgstl_value = "";
    String str_sgst = "SGST", str_sgstl_value = "";
    String str_total = "Total", str_total_value = "";
    String str_duedate = "Due Date", str_duedate_value = "";
    String BillDt = "";
    String payorcod = "";
    String payorcodtxt = "";
    int totalitem = 0;
    SettingModel sm = new SettingModel();

    public void print_receipt() {

        try {

            final String sitenm = "www.dev.chitandbridge.com\n\n";


            final String add = address + "\n";


            Date c = Calendar.getInstance().getTime();
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            String formattedDate = df.format(c);


            final String BillNo = "Bill :   " + "INV " + chitprint.getRef_id() + " / " + chitprint.getCase_id() + "\n";
            BillDt = "Date :   " + formattedDate + "";
            BillDt = BillDt.replaceAll("-", "/");

            final String shopnm = "" + chitprint.getSubject() + "\n";

            String cust_add = "";

            JSONObject jo_summary_ = new JSONObject(chitprint.getFooter_note());
            if (jo_summary_.has("ADDRESS")) {
                cust_add = jo_summary_.getString("ADDRESS") + "\n";
            }

            final String cust_phone = chitprint.getContact_number() + "\n";


            final String line = "-----------------------------------------------" + "\n";


            final String order_title = "QTY     ITEM                            TOTAL" + "\n"
                    + "-----------------------------------------------" + "\n";


            //  order_title =   String.format("%1$-10s %2$10s %3$13s", "QTY", "ITEM", "TOTAL") + "\n";

            String o_data = "";
            DecimalFormat twoDForm = new DecimalFormat("#.##");

            for (int p = 0; p < productlist.size(); p++) {


                Double itemprice = Double.parseDouble(productlist.get(p).getQuantity());
                String particular = productlist.get(p).getParticulars();
                int itempriceval = Integer.valueOf(itemprice.intValue());
                totalitem = totalitem + itempriceval;

                Double total = Double.parseDouble(productlist.get(p).getTotal()); // Make use of autoboxing.  It's also easier to read.

                String item = leftRightAlign("  " + itempriceval + "   " + particular, Inad.getCurrencyDecimal(total, this) + "");

                o_data = o_data + item + "\n";

            }

            String sub = "", cgst = "", sgst = "", total_ = "", due_date = "";

            try {
                JSONObject jo_summary = new JSONObject(chitprint.getFooter_note());

                Double totalcentralgst = Double.valueOf(jo_summary.getString("CGST"));
                Double totalstategst = Double.valueOf(jo_summary.getString("SGST"));
                Double grandtotal = Double.valueOf(jo_summary.getString("GRANDTOTAL"));
                Double total = Double.valueOf(jo_summary.getString("TOTALBILL"));
                due_date = DateTimeUtils.parseDateTime(chitprint.getCreated_date(), "yyyy-MM-dd HH:mm", "dd-MMM-yyyy");

                str_sub_total_value = total + "";
                str_cgstl_value = totalcentralgst + "";
                str_sgstl_value = totalstategst + "";
                str_total_value = grandtotal + "";
                str_duedate_value = due_date + "";

                if (jo_summary.getString("PAYMENT_MODE").equals("RAZORPAY") || jo_summary.getString("PAYMENT_MODE").equals("PAYUMONEY")) {
                    payorcod = "Paid :   " + symbol + " " + Inad.getCurrencyDecimal(grandtotal, this) + " / #" + totalitem + "\n";
                    icon = BitmapFactory.decodeResource(getResources(), R.drawable.paid);
                    payorcodtxt = "PAID\n";
                } else {
                    payorcod = "COD :   " + symbol + " " + Inad.getCurrencyDecimal(grandtotal, this) + " / #" + totalitem + "\n";
                    icon = BitmapFactory.decodeResource(getResources(), R.drawable.cod);
                    payorcodtxt = "COD\n";
                }

            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }

            final String remark = "Remarks: \n" + "All items are tax inclusive" + "\n\n";
            final String thanks = "Thank You, Visit Again..!!";


            final String finalCust_add = cust_add;
            final String finalO_data = o_data;


            final SettingDB db = new SettingDB(this);
            SessionManager sessionManager = new SessionManager(this);

            List<SettingModel> list_setting = db.GetItems();

            sm = list_setting.get(0);


            Thread t = new Thread() {
                public void run() {

                    try {

                        // https://stackoverflow.com/questions/16832531/how-can-i-send-a-paper-cut-command-using-my-web-app

                        // https://stackoverflow.com/questions/30295927/how-print-invoice-receipt-using-bluetooth-thermal-printer/30440012

                        //https://github.com/MFori/Android-Bluetooth-Printer      -  This  Class
                        //https://stackoverflow.com/questions/47524508/how-use-thermal-printerusb-ethernet-on-android-without-using-vendor-sdk
                        // https://stackoverflow.com/questions/32697430/command-for-thermal-printer-in-java
                        // https://github.com/imrankst1221/Thermal-Printer-in-Android
                        //https://stackoverflow.com/questions/45972890/print-two-different-text-alignment-on-same-line-in-android-bluetooth-thermal-pri
                        // https://stackoverflow.com/questions/12257164/how-to-change-the-font-size-while-printing-in-androi

                        mPrinter.setAlign(BluetoothPrinter.ALIGN_CENTER);
                        //if(sm.getIsprinttoken().equals("0")) mPrinter.printText(sitenm);
                        if (sm.getIsprinttoken().equals("0")) mPrinter.printText(add);
                        if (sm.getIsprinttoken().equals("0")) mPrinter.printText(shopnm);

                        mPrinter.setAlign(BluetoothPrinter.ALIGN_LEFT);
                        mPrinter.printText(BillNo);
                        mPrinter.printText(BillDt);

                        mPrinter.setAlign(BluetoothPrinter.ALIGN_RIGHT);
                        mPrinter.setBold(true);
                        mPrinter.printText(payorcodtxt);

                        mPrinter.setBold(false);
                        mPrinter.setAlign(BluetoothPrinter.ALIGN_LEFT);
                        mPrinter.printText(payorcod);

                        //mPrinter.setAlign(BluetoothPrinter.ALIGN_LEFT);
                        if (!mPrinter.printText(line)) {
                            Inad.showmsg(tv_nodata, "line error", PrintReceipt.this);
                        }
                        if (!mPrinter.printText(order_title)) {
                            Inad.showmsg(tv_nodata, "order_title error", PrintReceipt.this);
                        }

                        if (!mPrinter.printText(finalO_data)) {
                            Inad.showmsg(tv_nodata, "Item Data error", PrintReceipt.this);
                        }

                        /*mPrinter.setAlign(BluetoothPrinter.ALIGN_LEFT);
                        mPrinter.printText(str_sub_total);
                        mPrinter.setAlign(BluetoothPrinter.ALIGN_RIGHT);
                        mPrinter.printText(str_sub_total_value);
                        mPrinter.addNewLine();

                        mPrinter.setAlign(BluetoothPrinter.ALIGN_LEFT);
                        mPrinter.printText(str_cgst);
                        mPrinter.setAlign(BluetoothPrinter.ALIGN_RIGHT);
                        mPrinter.printText(str_cgstl_value);
                        mPrinter.addNewLine();

                        mPrinter.setAlign(BluetoothPrinter.ALIGN_LEFT);
                        mPrinter.printText(str_sgst);
                        mPrinter.setAlign(BluetoothPrinter.ALIGN_RIGHT);
                        mPrinter.printText(str_sgstl_value);
                        mPrinter.addNewLine();

                        mPrinter.setAlign(BluetoothPrinter.ALIGN_CENTER);
                        mPrinter.printLine();
                        mPrinter.addNewLine();

                        mPrinter.setAlign(BluetoothPrinter.ALIGN_LEFT);
                        mPrinter.printText(str_total);
                        mPrinter.setAlign(BluetoothPrinter.ALIGN_RIGHT);
                        mPrinter.printText(str_total_value);
                        mPrinter.addNewLine();

                        mPrinter.setAlign(BluetoothPrinter.ALIGN_LEFT);
                        mPrinter.printText(str_duedate);
                        mPrinter.setAlign(BluetoothPrinter.ALIGN_RIGHT);
                        mPrinter.printText(str_duedate_value);
                        mPrinter.addNewLine();


                        mPrinter.setAlign(BluetoothPrinter.ALIGN_CENTER);
                        mPrinter.printLine();*/
                        mPrinter.addNewLine();


                       /* if(!mPrinter.printText(remark))
                        {
                            Inad.showmsg(tv_nodata, "Remarks Error!", PrintlistActivity.this);
                        }

                        mPrinter.setAlign(BluetoothPrinter.ALIGN_CENTER);
                        if(!mPrinter.printText(thanks))
                        {
                            Inad.showmsg(tv_nodata, "Thanks Error!", PrintlistActivity.this);
                        }*/

                        byte[] bytes = {27, 100, 3};

                        //mPrinter.printUnicode(bytes);


                        mPrinter.finish();

                        PrintReceipt.this.runOnUiThread(new Runnable() {
                            public void run() {
                                callback.printsuccess();
                            }
                        });


                    } catch (Exception e) {
                        callback.printfail();
                        e.printStackTrace();
                    }
                }
            };
            t.start();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public boolean isprintstatus(String status) {

        return false;
    }


    public void afterprint_finished(final Intent i, final boolean isonlyfinish)  // if employee(athi) thne 1- asiign to emp 2 - finish   // if business then only finish
    {
        mBluetoothConnectProgressDialog = ProgressDialog.show(this,
                "Loading...", "", true, false);

        SessionManager sessionManager = new SessionManager(PrintReceipt.this);

        if (sessionManager.getisBusinesslogic())  // is business
        {
            removearchived(i, isonlyfinish);
        } else {
            String chitIds = String.valueOf(chitprint.getChit_id());
            assignarchived(session, chitIds, sessionManager.getE_bridgeid(), new Mycall() {
                @Override
                public void printsuccess() {

                }

                @Override
                public void printfail() {

                }

                @Override
                public void assignarchived() {
                    removearchived(i, isonlyfinish);
                }
            });
        }
    }


    public void removearchived(final Intent i, final boolean isonlyfinish) {


        String chitIds = String.valueOf(chitprint.getChit_id());


        ApiInterface apiService = ApiClient.getClient(PrintReceipt.this).create(ApiInterface.class);
        JsonObject a1 = new JsonObject();
        try {

            a1.addProperty("chitIds", chitIds);
            a1.addProperty("actionCode", "5");

        } catch (JsonParseException e) {
            e.printStackTrace();
        }
        // Toast.makeText(MyTaskActivity.this, actionCode, Toast.LENGTH_LONG).show();

        //Call call = apiService.removechit(usersession,chitIds+",","10");
        Call call = apiService.removechit(session, a1);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                mBluetoothConnectProgressDialog.dismiss();
                if (response.isSuccessful()) {
                    try {
                        Toast.makeText(PrintReceipt.this, "FINISHED", Toast.LENGTH_LONG).show();
                        if (isonlyfinish)  // for printlist
                        {
                            finish();
                        } else { // for chitlog
                            finish();
                            startActivity(i.putExtra("isprint", false));
                        }
                    } catch (Exception e) {
                        Log.e("errortest", e.getMessage());

                    }

                } else {
                    Log.e("test", " trdds1");

                    try {
                        mBluetoothConnectProgressDialog.dismiss();
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Toast.makeText(PrintReceipt.this, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(PrintReceipt.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }


            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // Log error here since request failed
                mBluetoothConnectProgressDialog.dismiss();
                Log.e("errortest", t.getMessage());

            }
        });
    }

    public void assignarchived(final String usersession, String chitIds, String employee_bridge_id, final Mycall listener) {


        ApiInterface apiService =
                ApiClient.getClient(PrintReceipt.this).create(ApiInterface.class);
        JsonObject a1 = new JsonObject();
        try {

            a1.addProperty("chitIds", chitIds);
            a1.addProperty("employee_bridge_id", employee_bridge_id);


        } catch (JsonParseException e) {
            e.printStackTrace();
        }
        //   Toast.makeText(MyTaskActivity.this, employee_bridge_id, Toast.LENGTH_LONG).show();

        //Call call = apiService.removechit(usersession,chitIds+",","10");


        Call call = apiService.assigntask(usersession, a1);


        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                int statusCode = response.code();


                if (response.isSuccessful()) {
                    try {


                        listener.assignarchived();


                    } catch (Exception e) {
                        Log.e("errortest", e.getMessage());

                    }

                } else {


                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Toast.makeText(PrintReceipt.this, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(PrintReceipt.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }


            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // Log error here since request failed
                findViewById(R.id.rl_pb_ctask).setVisibility(View.GONE);
                Log.e("error message", t.toString());
            }
        });
    }


    public void showPictureDialog() {
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(this);
        String[] pictureDialogItems = {"Photo Gallery", "Camera"};
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                // openImagePickerIntent();

                                Intent i = new Intent(PrintReceipt.this, FileActivity.class);
                                i.putExtra("isdoc", false);
                                startActivityForResult(i, 8);

                               /* new FilePicker(CatalogeEditDataActivity.this, new FilePicker.OnDialogClickListener() {
                                    @Override
                                    public void onDialogImageRunClick(int pos, boolean isdoc) {

                                        Intent i = new Intent(CatalogeEditDataActivity.this, FileActivity.class);
                                        i.putExtra("isdoc", isdoc);
                                        startActivityForResult(i, 8);


                                    }
                                }).show();*/


                                break;
                            case 1:
                                cameraIntent();
                                break;
                        }
                    }
                });
        pictureDialog.show();
    }


    private void cameraIntent() {

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        this.screen_width = size.x;
        this.screen_height = size.y;

        File root = new File(Environment.getExternalStorageDirectory()
                + File.separator + getString(R.string.app_name) + File.separator + "Capture" + File.separator);
        root.mkdirs();
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        f3f = new File(root, "IMG" + timeStamp + ".jpg");

        try {
            imageUri = FileProvider.getUriForFile(
                    PrintReceipt.this, getApplicationContext()
                            .getPackageName() + ".provider", this.f3f);
        } catch (Exception e) {
            Toast.makeText(this, "Please check SD card! Image shot is impossible!", Toast.LENGTH_SHORT).show();
        }

        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        intent.putExtra("output", imageUri);
        try {
            startActivityForResult(intent, 7);
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
        }

    }


    ExifInterface exif;
    File f3f;
    int screen_height;
    int screen_width;
    private Uri imageUri;

    ArrayList<ModelFile> al_selet = new ArrayList<>();
    public AddComment addComment;

    public void showcommentdialog() {
        addComment = new AddComment(this, new AddComment.OnDialogClickListener() {
            @Override
            public void onDialogImageRunClick(int positon, String add) {

                if (positon == 0) {

                } else if (positon == 1) {


                    if (ChitlogActivity.productlist.size() > 0) {
                        changestatus(add);
                    } else {
                        Toast.makeText(PrintReceipt.this, "Please wait while getting data", Toast.LENGTH_LONG).show();
                    }


                }

            }

            @Override
            public void onImgClick(List<ModelFile> data, int pos) {

            }

            @Override
            public void onAddImg() {
                showPictureDialog();
            }
        }, "");

        addComment.show();

    }

    public void changestatus(String comment) {


        String status = SummaryDetailFragmnet.item.getTransaction_status();
        // String chitIds = ChitlogActivity.productlist.get(0).getChit_id() + "", actionCode = "";
        String chitIds = SummaryDetailFragmnet.item.getChit_id() + "", actionCode = "";

        actionCode = status.toUpperCase().trim();


        switch (actionCode) {

            case "ACTIVE":
                actionCode = "3";
                break;

            case "ACCEPTED":
                actionCode = "3";
                break;

            case "INPROGRESS":
                actionCode = "4";
                break;

            case "HOLD":
                actionCode = "8";
                break;

            case "FINISHED":
                actionCode = "5";
                break;

            case "CLOSED":
                actionCode = "9";
                break;

            case "COMPLETED":
                actionCode = "6";
                break;

            case "WITHDRAWN":
                actionCode = "7";
                break;

            case "DIARISED":
                actionCode = "2";
                break;


            default:
                actionCode = "";
                break;
        }


        if (isdiaries) {
            actionCode = "2";
        }


        ApiInterface apiService =
                ApiClient.getClient(PrintReceipt.this).create(ApiInterface.class);
        JsonObject a1 = new JsonObject();
        try {

            a1.addProperty("chitIds", chitIds);
            a1.addProperty("actionCode", actionCode);
            a1.addProperty("remark", comment);

        } catch (JsonParseException e) {
            e.printStackTrace();
        }
        // Toast.makeText(AllTaskActivity.this, actionCode, Toast.LENGTH_LONG).show();

        //Call call = apiService.removechit(usersession,chitIds+",","10");

        Call call = apiService.removechit(ChitlogActivity.usersession.getsession(), a1);
        if (isEnquiry) {
            call = apiService.chitComment(ChitlogActivity.usersession.getsession(), a1);
        }

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                int statusCode = response.code();
                Headers headerList = response.headers();
                Log.d("testhistory", statusCode + " ");

                if (response.isSuccessful()) {
                    try {

                        Log.e("response.body()", response.body().toString());
                        String a = new Gson().toJson(response.body());

                        JSONObject jo_res = new JSONObject(a);
                        String response_ = jo_res.getString("response");
                        JSONObject jo_txt = new JSONObject(response_);
                        String actionTxt = jo_txt.getString("actionTxt");


                        Toast.makeText(PrintReceipt.this, actionTxt, Toast.LENGTH_LONG).show();


                    } catch (Exception e) {
                        Log.e("errortest", e.getMessage());

                    }

                    Toast.makeText(PrintReceipt.this, "Comment Added", Toast.LENGTH_LONG).show();
                    finish();
                    Intent i = getIntent();
                    i.putExtra("isprint", ChitlogActivity.isprint);
                    i.putExtra("iscomment", ChitlogActivity.iscomment);
                    i.putExtra("isdiaries", isdiaries);
                    startActivity(i);


                } else {
                    Log.e("test", " trdds1");

                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Toast.makeText(PrintReceipt.this, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(PrintReceipt.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }


            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // Log error here since request failed
                findViewById(R.id.rl_pb_ctask).setVisibility(View.GONE);
                Log.e("error message", t.toString());
            }
        });
    }


}
