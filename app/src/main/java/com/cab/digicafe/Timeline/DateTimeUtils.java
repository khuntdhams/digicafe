package com.cab.digicafe.Timeline;

import android.text.format.DateUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by CSS on 25-11-2017.
 */

public class DateTimeUtils {

    public static String parseDateTime(String dateString, String originalFormat, String outputFromat) {

        SimpleDateFormat formatter = new SimpleDateFormat(originalFormat, Locale.US);
        Date date = null;
        try {
            date = formatter.parse(dateString);

            SimpleDateFormat dateFormat = new SimpleDateFormat(outputFromat, new Locale("US"));

            return dateFormat.format(date);

        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String formatDateToString(Date date) {
        SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy");
        String date_ = format.format(date);
        return date_;
    }

    public static String formatCurrentDateToStringWithTime() {
        SimpleDateFormat format = new SimpleDateFormat("dd MMM, yyyy hh:mm a");
        String date_ = format.format(new Date());
        return date_;
    }

    public static Date formatStringToDate(String date) throws ParseException {
        return new SimpleDateFormat("dd MMM, yyyy hh:mm a").parse(date);
    }

    public static String getDateFromDateTime(Date dateString) {
        SimpleDateFormat format = new SimpleDateFormat("dd");
        String date_ = format.format(dateString);
        return date_;
    }

    public static String getRelativeTimeSpan(String dateString, String originalFormat) {

        SimpleDateFormat formatter = new SimpleDateFormat(originalFormat, Locale.US);
        Date date = null;
        try {
            date = formatter.parse(dateString);

            return DateUtils.getRelativeTimeSpanString(date.getTime()).toString();

        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }
    }
}
