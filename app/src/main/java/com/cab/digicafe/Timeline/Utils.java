package com.cab.digicafe.Timeline;

import android.content.Context;
import android.content.res.Resources;
import android.util.TypedValue;

/**
 * Created by CSS on 25-11-2017.
 */

public class Utils {

    public static int dpToPx(float dp, Context context) {
        return dpToPx(dp, context.getResources());
    }

    public static int dpToPx(float dp, Resources resources) {
        float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, resources.getDisplayMetrics());
        return (int) px;
    }
}