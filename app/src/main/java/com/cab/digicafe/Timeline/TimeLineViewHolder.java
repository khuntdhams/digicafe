package com.cab.digicafe.Timeline;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.cab.digicafe.R;


/**
 * Created by HP-HP on 05-12-2015.
 */
public class TimeLineViewHolder extends RecyclerView.ViewHolder {

    public TextView mDate;
    public TextView mMessage;
    public TimelineView mTimelineView;
    public TextView tv_remark;
    public RecyclerView rv_attach;

    public TimeLineViewHolder(View itemView, int viewType) {
        super(itemView);
        mDate = (TextView) itemView.findViewById(R.id.text_timeline_date);
        mMessage = (TextView) itemView.findViewById(R.id.text_timeline_title);
        tv_remark = (TextView) itemView.findViewById(R.id.tv_remark);
        rv_attach = (RecyclerView) itemView.findViewById(R.id.rv_attach);
        mTimelineView = (TimelineView) itemView.findViewById(R.id.time_marker);
        mTimelineView.initLine(viewType);
    }
}
