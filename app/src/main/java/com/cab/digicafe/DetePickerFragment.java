package com.cab.digicafe;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;

import com.cab.digicafe.Helper.SessionManager;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DetePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    SessionManager sessionManager;
    private OnItemClickListener listener;

    public DetePickerFragment(OnItemClickListener listener) {
        this.listener = listener;
    }


  /*  public DetePickerFragment(OnItemClickListener listener){
        this.listener=listener;

    }*/

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        listener.onItemClick(true, year, month, dayOfMonth);
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        listener.onItemClick(false, 0, 0, 0);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar c = Calendar.getInstance();

        sessionManager = new SessionManager(getActivity());
        String dtStart = sessionManager.getDate();
        SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy hh:mm aaa");
        try {
            if (!dtStart.equals("")) {
                Date date = format.parse(dtStart);
                c.setTime(date);
                System.out.println(date);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        return new DatePickerDialog(getActivity(), this, year, month, day);
    }

    public interface OnItemClickListener {
        void onItemClick(Boolean isTrue, int year, int month, int dayOfMonth);
    }
}
