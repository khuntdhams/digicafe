package com.cab.digicafe;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.cab.digicafe.Activities.ChageCircle;
import com.cab.digicafe.Activities.IssueMgmtActivity;
import com.cab.digicafe.Adapter.CatererAdapter;
import com.cab.digicafe.Database.SqlLiteDbHelper;
import com.cab.digicafe.Dialogbox.CustomDialogClass;
import com.cab.digicafe.Dialogbox.UpdateDialogClass;
import com.cab.digicafe.Helper.SessionManager;
import com.cab.digicafe.Helper.SharedPrefUserDetail;
import com.cab.digicafe.Model.ShareModel;
import com.cab.digicafe.Model.Supplier;
import com.cab.digicafe.Model.Userprofile;
import com.cab.digicafe.MyCustomClass.CheckVersion;
import com.cab.digicafe.MyCustomClass.JsonObjParse;
import com.cab.digicafe.Rest.ApiClient;
import com.cab.digicafe.Rest.ApiInterface;
import com.cab.digicafe.serviceTypeClass.GetServiceTypeFromBusiUserProfile;
import com.google.gson.Gson;
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link CatererFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link CatererFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CatererFragment extends Fragment implements CatererAdapter.OnItemClickListener, SearchView.OnQueryTextListener, SwipyRefreshLayout.OnRefreshListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private List<Supplier> catererlist = new ArrayList<>();
    private RecyclerView recyclerView;
    private CatererAdapter mAdapter;
    private ProgressBar loadingview;
    private OnFragmentInteractionListener mListener;
    private SessionManager sessionManager;
    SqlLiteDbHelper dbHelper;
    boolean isDatabase=true;

    TextView tv_nodata;
    static boolean isdetach = false;

    public CatererFragment() {
        // Required empty public constructor
    }

    static String purpose = "";

    boolean isfrommyagree = false;
    String to_bridge_id = "", currency_code = "";

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CatererFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CatererFragment newInstance(String param1, String param2, String purp) {

        isdetach = false;
        purpose = purp;
        CatererFragment fragment = new CatererFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_caterer, container, false);
        recyclerView = (RecyclerView) v.findViewById(R.id.recycler_view);
        tv_nodata = (TextView) v.findViewById(R.id.tv_nodata);
        loadingview = (ProgressBar) v.findViewById(R.id.loadingview);
        sessionManager = new SessionManager(getContext());
        mAdapter = new CatererAdapter(catererlist, this, getContext(),isDatabase);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView.setAdapter(mAdapter);
        refreshlist();
        mSwipyRefreshLayout = (SwipyRefreshLayout) v.findViewById(R.id.swipyrefreshlayout);
        mSwipyRefreshLayout.setOnRefreshListener(CatererFragment.this);
        usersignin(mParam1);
        dbHelper = new SqlLiteDbHelper(getContext());

        SharedPrefUserDetail.setString(getActivity(), SharedPrefUserDetail.purpose, purpose);

        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setHasOptionsMenu(true);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main, menu);

        final MenuItem item = menu.findItem(R.id.action_search);

        /*MenuItem item1 = menu.findItem(R.id.action_filter);
        item1.setVisible(false);*/

        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setOnQueryTextListener(this);

        if (ismycircle) {
            String industry_id = sessionManager.getIndustry_id();
            String building_id = sessionManager.getBuilding_id();
            if (industry_id.isEmpty() || building_id.isEmpty()) {
                Intent i = new Intent(getActivity(), ChageCircle.class);
                //startActivity(i);
            }
            MenuItem i_circle = menu.findItem(R.id.action_circle);
            //i_circle.setVisible(true);
        } else {

        }

        MenuItemCompat.setOnActionExpandListener(item,
                new MenuItemCompat.OnActionExpandListener() {
                    @Override
                    public boolean onMenuItemActionCollapse(MenuItem item) {
// Do something when collapsed
                        mAdapter.setFilter(catererlist);
                        return true; // Return true to collapse action view
                    }

                    @Override
                    public boolean onMenuItemActionExpand(MenuItem item) {
// Do something when expanded
                        return true; // Return true to expand action view
                    }
                });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_circle) {
            Intent i = new Intent(getActivity(), ChageCircle.class);
            startActivity(i);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public boolean onQueryTextChange(String newText) {
        if (newText.length() > 3 || newText.length() == 0) {
            final List<Supplier> filteredModelList = filter(catererlist, newText);
            mAdapter.setFilter(filteredModelList);
        }
        return true;
    }

    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    private List<Supplier> filter(List<Supplier> models, String query) {
        query = query.toLowerCase();
        final List<Supplier> filteredModelList = new ArrayList<>();
        for (Supplier model : models) {
            final String text = model.getAlias_name().toLowerCase();
            if (text.contains(query)) {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            // throw new RuntimeException(context.toString()
            //       + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        isdetach = true;
        mListener = null;
    }

    @Override
    public void onItemClick(final Supplier item, int pos) {

        checkversioncode(item);

    }

    @Override
    public void onItemLongClick(Supplier item, int pos) {

    }

    @Override
    public void onDisplyLongDesc(Supplier item) {

    }

    @Override
    public void onDisplyInfo(Supplier item) {

    }

    String agree = "";


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    String bridge_id = "";
    boolean ismycircle = false;

    public void usersignin(String usersession) {

        Log.e("test", " trdds" + usersession);

        to_bridge_id = SharedPrefUserDetail.getString(getActivity(), SharedPrefUserDetail.isfrommyaggregator, "");
        if (!to_bridge_id.equals("")) {
            isfrommyagree = true;
            if (to_bridge_id.equals("noback")) // home
            {
                to_bridge_id = sessionManager.getAggregatorprofile();
            } else {
                to_bridge_id = to_bridge_id; // for aggre shops inside my supplier
            }
        } else {
            ismycircle = true; // for circle shops inside my supplier
        }


        ApiInterface apiService =
                ApiClient.getClient(getActivity()).create(ApiInterface.class);

        String industry_id = sessionManager.getIndustry_id();
        String building_id = sessionManager.getBuilding_id();

        Call call = null;
        if (isfrommyagree) {
            call = apiService.getCatererlist(usersession, to_bridge_id, "", pagecount);  // networkDownListByBridgeID // My Agree
        } else {
            call = apiService.getCatererlist(usersession, sessionManager.getAggregatorprofile(), industry_id, building_id, pagecount); // My Circle

        }
        //Call call = apiService.getCatererlist(usersession,sessionManager.getAggregatorprofile());  // networkDownListByBridgeID
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                //  String cookie = response.;
                //  Log.d("test", cookie+ " ");
                loadingview.setVisibility(View.GONE);
                mSwipyRefreshLayout.setRefreshing(false);
                if (response.isSuccessful()) {
                    try {

                        Log.e("caterrelist", response.body().toString());
                        String a = new Gson().toJson(response.body());

                        JSONObject jObjRes = new JSONObject(a);
                        JSONObject jObjdata = jObjRes.getJSONObject("response");
                        JSONObject jObjResponse = jObjdata.getJSONObject("data");
                        JSONArray jObjcontacts = jObjResponse.getJSONArray("content");


                        try {

                            String totalRecord = jObjResponse.getString("totalRecord");
                            String displayEndRecord = jObjResponse.getString("displayEndRecord");

                            int total = jObjResponse.getInt("totalRecord");
                            int end = jObjResponse.getInt("displayEndRecord");
                            if (total == end) {
                                isdataavailable = false;
                            } else {
                                isdataavailable = true;
                            }

                        } catch (Exception e) {
                            isdataavailable = false;
                        }


                        if (pagecount == 1) {
                            catererlist.clear();
                        }

                        for (int i = 0; i < jObjcontacts.length(); i++) {
                            if (jObjcontacts.get(i) instanceof JSONObject) {
                                JSONObject jsnObj = (JSONObject) jObjcontacts.get(i);
                                Log.e("aliasjsnObj", jsnObj.toString());

                                Supplier obj = new Gson().fromJson(jsnObj.toString(), Supplier.class);
                                catererlist.add(obj);
                            }
                        }


                        mAdapter.notifyDataSetChanged();
                        tv_nodata.setVisibility(View.GONE);

                        if (catererlist.size() == 1 && !isdetach) {
                            Supplier item = catererlist.get(0);
                            checkversioncode(item);
                        } else if (catererlist.size() == 0) {
                            tv_nodata.setVisibility(View.VISIBLE);
                        }
                    } catch (Exception e) {
                        Log.e("errortest", e.getMessage());

                        Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                    //  Intent i = new Intent(MainActivity.this, MainActivity.class);
                    //  startActivity(i);
                } else {
                    Log.e("test", " trdds1");

                    try {
                       /* JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Toast.makeText(MainActivity.this, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();*/
                    } catch (Exception e) {
                        Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }

                //  List<Movie> movies = response.body().getResults();
                //  recyclerView.setAdapter(new MoviesAdapter(movies, R.layout.list_item_movie, getApplicationContext()));
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // Log error here since request failed
                mSwipyRefreshLayout.setRefreshing(false);
                Log.e("error message", t.toString());
            }
        });
    }

    public void onRefresh(SwipyRefreshLayoutDirection direction) {
        //mSwipyRefreshLayout.setRefreshing(false);

        Log.e("scroll direction", "Refresh triggered at "
                + (direction == SwipyRefreshLayoutDirection.TOP ? "top" : "bottom"));
        if (direction == SwipyRefreshLayoutDirection.TOP) {
            isdataavailable = true;
            pagecount = 1;
            usersignin(mParam1);
        } else if (direction == SwipyRefreshLayoutDirection.BOTTOM) {
            if (isdataavailable) {
                pagecount++;
                usersignin(mParam1);
            } else {
                mSwipyRefreshLayout.setRefreshing(false);
                Toast.makeText(getActivity(), "No more data available", Toast.LENGTH_LONG).show();
            }

        }
    }

    SwipyRefreshLayout mSwipyRefreshLayout;
    int pagecount = 1;
    boolean isdataavailable = true;
    int pagesize = 25;

    public void refreshlist() {

    }


    public void checkversioncode(final Supplier item) {
        CheckVersion checkVersion = new CheckVersion(getActivity(), sessionManager.getAggregator_ID(), new CheckVersion.Apicallback() {
            @Override
            public void onGetResponse(String a, boolean issuccess) {

            }

            @Override
            public void onUpdate(boolean isupdateforce, String ischeck) {


                if (ischeck.equalsIgnoreCase("ys")) {

                    new UpdateDialogClass(getActivity(), new UpdateDialogClass.OnDialogClickListener() {
                        @Override
                        public void onDialogImageRunClick(int positon) {
                            if (positon == 0) {
                                shopselect(item);
                            } else if (positon == 1) {
                                final String appPackageName = getActivity().getPackageName(); // getPackageName() from Context or Activity object
                                try {
                                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                                } catch (android.content.ActivityNotFoundException anfe) {
                                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                                }


                            }
                        }
                    }, "Update is available !", isupdateforce).show();
                } else {
                    shopselect(item);
                }
            }
        });

    }

    public void shopselect(final Supplier item) {


        if (Apputil.isanothercateror(dbHelper, item)) {


            new CustomDialogClass(getActivity(), new CustomDialogClass.OnDialogClickListener() {
                @Override
                public void onDialogImageRunClick(int positon) {
                    if (positon == 0) {

                    } else if (positon == 1) {

                        sessionManager.setcurrentcaterer("");
                        sessionManager.setcurrentcaterername(item.getUsername());
                        sessionManager.setcurrentcaterer(item.getFrom_bridge_id());
                        sessionManager.setcurrentcatererstatus(item.getBusiness_status());
                        sessionManager.setRemark("");
                        sessionManager.setDate("");
                        sessionManager.setAddress("");
                        //   sessionManager.setCartproduct("");
                        //   sessionManager.setCartcount(0);
                        dbHelper.deleteallfromcart();
                        nextpage(item);

                    }
                }
            }, "You are moving out of " + sessionManager.getcurrentcatereraliasname() + ".").show();
        } else {
            sessionManager.setcurrentcatererstatus(item.getBusiness_status());
            sessionManager.setcurrentcaterername(item.getUsername());
            sessionManager.setcurrentcaterer(item.getFrom_bridge_id());
            nextpage(item);
        }
    }

    private void nextpage(final Supplier item) {

        SharedPrefUserDetail.setString(getActivity(), SharedPrefUserDetail.field_json_data, item.getField_json_data());


        to_bridge_id = SharedPrefUserDetail.getString(getActivity(), SharedPrefUserDetail.isfrommyaggregator, "");

        if (to_bridge_id.equals("noback"))  // shop from my agree
        {
            agree = item.getUsername();

            sessionManager.setAggregator_ID(agree);  // its my circle

            SharedPrefUserDetail.setString(getActivity(), SharedPrefUserDetail.isfrommyaggregator, item.getTo_bridge_id());

            Intent mainIntent = new Intent(getActivity(), MainActivity.class);
            mainIntent.putExtra("session", sessionManager.getsession());

            startActivity(mainIntent);


        } else { // for circle / aggre from my supplier - check again its business type


            new GetServiceTypeFromBusiUserProfile(getActivity(), item.getUsername(), new GetServiceTypeFromBusiUserProfile.Apicallback() {
                @Override
                public void onGetResponse(String a, String res, String sms_emp) {

                    purpose = a;
                    if (a.isEmpty()) purpose = "request";

                    purpose = purpose.toLowerCase();


                    SharedPrefUserDetail.setString(getActivity(), SharedPrefUserDetail.purpose, purpose);
                    SharedPrefUserDetail.setString(getActivity(), SharedPrefUserDetail.purpose_mysupplier, purpose);


                    SharedPrefUserDetail.setString(getActivity(), SharedPrefUserDetail.chit_currency_code, item.getCurrency_code());
                    SharedPrefUserDetail.setString(getActivity(), SharedPrefUserDetail.chit_symbol_native, item.getSymbol_native());
                    SharedPrefUserDetail.setString(getActivity(), SharedPrefUserDetail.chit_currency_code_id, item.getCurrency_code_id());
                    SharedPrefUserDetail.setString(getActivity(), SharedPrefUserDetail.chit_symbol, item.getSymbol());

                    sessionManager.setcurrentcatereraliasname(item.getAlias_name());

                    if (purpose.equalsIgnoreCase("service")) { // issue

                        if (to_bridge_id.equals("")) // if not circle
                            sessionManager.setAggregator_ID(item.getInfo());  // its my circle
                        else {


                            /*    Go to my aggregator option Choose MCC aggregator Select blrmc4 Create a request and submit  When we create a chit, chit goes to the shop and the aggregator. Example act is user - blrmc2 and info is shop - blrmcc.       */

                        }


                        String supl = new Gson().toJson(item);
                        Intent i = new Intent(getActivity(), IssueMgmtActivity.class);
                        i.putExtra("purpose", purpose);
                        i.putExtra("supplier", supl);
                        startActivity(i);
                    } else {

                        if (purpose.equals("Invoice")) {

                        }

                        if (to_bridge_id.equals(""))
                            sessionManager.setAggregator_ID(item.getInfo());  // its my circle
                        else {


                            /*    Go to my aggregator option Choose MCC aggregator Select blrmc4 Create a request and submit  When we create a chit, chit goes to the shop and the aggregator. Example act is user - blrmc2 and info is shop - blrmcc.       */

                        }

                        Log.e("bridge_id", bridge_id);
                        Intent i = new Intent(getContext(), CatelogActivity.class);
                        i.putExtra("session", mParam1);


                        if (!ShareModel.getI().dbUnderMaster.isEmpty()) // for db
                        {
                            // From is any shop under database - favouriteSelected
                            // To is the current login credentials - chit_hash_id

                            String getBridgeId = "", currency_code = "";
                            try {
                                JSONObject Jsonresponse = new JSONObject(res); // this is shop under db
                                JSONObject Jsonresponseinfo = Jsonresponse.getJSONObject("response");
                                JSONArray jaData = Jsonresponseinfo.getJSONArray("data");
                                if (jaData.length() > 0) {
                                    JSONObject data = jaData.getJSONObject(0);
                                    getBridgeId = JsonObjParse.getValueNull(data.toString(), "bridge_id");

                                }

                                i.putExtra("bridgeidval", getBridgeId);


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            /*****************************************/

                            // favouriteSelected - tobid of shop - veg shop
                            // chit_hash_id - - (login) henncy - current user

                            Userprofile userProfileData;
                            String userprofile = sessionManager.getUserprofile(); // this is current user
                            try {
                                userProfileData = new Gson().fromJson(userprofile, Userprofile.class);
                                String bridge_id = userProfileData.getBridge_id();
                                // favouriteSelected - From is any shop under database
                                i.putExtra("frombridgeidval", bridge_id); // current user
                                i.putExtra("currency_code", userProfileData.currency_code); // to display current user currency data only
                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                        } else {
                            i.putExtra("bridgeidval", item.getTo_bridge_id());
                            i.putExtra("frombridgeidval", item.getFrom_bridge_id());
                            i.putExtra("currency_code", item.getCurrency_code());

                        }

                        i.putExtra("aliasname", item.getAlias_name());
                        i.putExtra("username", item.getUsername());
                        i.putExtra("company_name", item.getAlias_name());
                        startActivity(i);
                    }

                }
            });

            // shops underneth - > From My Supplier


            //if(to_bridge_id.equals("noback")) SharedPrefUserDetail.setString(getActivity(), SharedPrefUserDetail.isfrommyaggregator,"ysback");

        }
    }


}
