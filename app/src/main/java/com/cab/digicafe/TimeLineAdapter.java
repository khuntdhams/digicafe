package com.cab.digicafe;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cab.digicafe.Activities.ImagePreviewActivity;
import com.cab.digicafe.Adapter.ImgPrevAdapter;
import com.cab.digicafe.Helper.SessionManager;
import com.cab.digicafe.Model.Chit;
import com.cab.digicafe.Model.Chitlog;
import com.cab.digicafe.Model.ModelFile;
import com.cab.digicafe.Timeline.DateTimeUtils;
import com.cab.digicafe.Timeline.Orientation;
import com.cab.digicafe.Timeline.TimeLineViewHolder;
import com.cab.digicafe.Timeline.TimelineView;
import com.cab.digicafe.Timeline.VectorDrawableUtils;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by CSS on 25-11-2017.
 */

public class TimeLineAdapter extends RecyclerView.Adapter<TimeLineViewHolder> {

    private List<Chitlog> mFeedList;
    private Context mContext;
    private Orientation mOrientation;
    private boolean mWithLinePadding;
    private LayoutInflater mLayoutInflater;

    Chit chit = DetailsFragment.item_detail;
    SessionManager sessionManager;

    public TimeLineAdapter(Context c, List<Chitlog> feedList, Orientation orientation, boolean withLinePadding) {
        mFeedList = feedList;
        mOrientation = orientation;
        mWithLinePadding = withLinePadding;
        sessionManager = new SessionManager(c);
    }

    @Override
    public int getItemViewType(int position) {
        return TimelineView.getTimeLineViewType(position, getItemCount());
    }

    @Override
    public TimeLineViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        mLayoutInflater = LayoutInflater.from(mContext);
        View view;

        if (mOrientation == Orientation.HORIZONTAL) {
            view = mLayoutInflater.inflate(mWithLinePadding ? R.layout.item_timeline_horizontal_line_padding : R.layout.item_timeline_horizontal, parent, false);
        } else {
            view = mLayoutInflater.inflate(mWithLinePadding ? R.layout.item_timeline_line_padding : R.layout.item_timeline, parent, false);
        }

        return new TimeLineViewHolder(view, viewType);
    }

    @Override
    public void onBindViewHolder(TimeLineViewHolder holder, final int position) {

        Chitlog timeLineModel = mFeedList.get(position);
        if (position % 3 == 0) {
            holder.mTimelineView.setMarker(VectorDrawableUtils.getDrawable(mContext, R.drawable.ic_marker_inactive, android.R.color.darker_gray));

        }
        if (position % 3 == 1) {
            holder.mTimelineView.setMarker(VectorDrawableUtils.getDrawable(mContext, R.drawable.ic_marker_active, R.color.colorPrimary));

        } else {
            holder.mTimelineView.setMarker(ContextCompat.getDrawable(mContext, R.drawable.ic_marker), ContextCompat.getColor(mContext, R.color.colorPrimary));

        }

     /*   if(timeLineModel.getStatus() == OrderStatus.INACTIVE) {
        } else if(timeLineModel.getStatus() == OrderStatus.ACTIVE) {
            holder.mTimelineView.setMarker(VectorDrawableUtils.getDrawable(mContext, R.drawable.ic_marker_active, R.color.colorPrimary));
        } else {
            holder.mTimelineView.setMarker(ContextCompat.getDrawable(mContext, R.drawable.ic_marker), ContextCompat.getColor(mContext, R.color.colorPrimary));
        }
*/
        if (!timeLineModel.getAction_date().isEmpty()) {
            holder.mDate.setVisibility(View.VISIBLE);
            holder.mDate.setText(DateTimeUtils.parseDateTime(timeLineModel.getAction_date(), "yyyy-MM-dd HH:mm", "dd-MMM-yyyy hh:mm a"));
        } else
            holder.mDate.setVisibility(View.GONE);


        String assignto = "";
        if (timeLineModel.getAction().equalsIgnoreCase("Assignee")) {
            if (timeLineModel.getRemark().trim().equalsIgnoreCase("Root user")) {
                assignto = " to " + chit.getSubject();
            } else {
                assignto = " to " + timeLineModel.getRemark();
            }


            holder.mMessage.setText("Assigned" + assignto);
        } else {
            holder.mMessage.setText(timeLineModel.getAction() + assignto);
        }

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        holder.rv_attach.setLayoutManager(mLayoutManager);


        imgPrevAdapter = new ImgPrevAdapter(al_selet, mContext, new ImgPrevAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int pos) {

                String[] separated = mFeedList.get(position).getRemark().split(",;t ");
                ArrayList<ModelFile> al_selet = new ArrayList<>();
                for (int i = 2; i < separated.length; i++) {
                    al_selet.add(new ModelFile(separated[i], false));
                }

                String imgarray = new Gson().toJson(al_selet);
                Intent intent = new Intent(mContext, ImagePreviewActivity.class);
                intent.putExtra("imgarray", imgarray);
                intent.putExtra("pos", pos);
                intent.putExtra("isdrag", false);
                intent.putExtra("img_title", "Remark");
                mContext.startActivity(intent);

            }

            @Override
            public void onDelete(int pos) {


            }

            @Override
            public void onLongClick(View v) {

            }

        }, false);


        holder.rv_attach.setAdapter(imgPrevAdapter);


        if (timeLineModel.getRemark() != null && !timeLineModel.getRemark().trim().equals("")) {
            String[] separated = timeLineModel.getRemark().split(",;t ");
            al_selet = new ArrayList<>();
            for (int i = 2; i < separated.length; i++) {
                al_selet.add(new ModelFile(separated[i], false));
            }

            //String comment = "Comment by : "+sessionManager.getcurrentu_nm()+" ; "+separated[0];
            String comment = null;
            try {
                comment = "Comment by : " + separated[1] + " ; " + separated[0];
            } catch (Exception e) {
                comment = "Comment by : " + sessionManager.getcurrentu_nm() + " ; " + separated[0];
                e.printStackTrace();
            }
            holder.tv_remark.setText(comment);
            imgPrevAdapter.setItem(al_selet);


            holder.tv_remark.setVisibility(View.VISIBLE);
            holder.mMessage.setVisibility(View.GONE);
            holder.rv_attach.setVisibility(View.VISIBLE);
        } else {
            holder.tv_remark.setVisibility(View.GONE);
            holder.mMessage.setVisibility(View.VISIBLE);
            holder.rv_attach.setVisibility(View.GONE);
        }


    }

    @Override
    public int getItemCount() {
        return (mFeedList != null ? mFeedList.size() : 0);
    }

    ArrayList<ModelFile> al_selet = new ArrayList<>();
    ImgPrevAdapter imgPrevAdapter;

}
