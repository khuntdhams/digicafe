package com.cab.digicafe;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cab.digicafe.Activities.ForgotPwdActivity;
import com.cab.digicafe.Activities.LogoActivity;
import com.cab.digicafe.Activities.MyAppBaseActivity;
import com.cab.digicafe.Activities.MySupplierActivity;
import com.cab.digicafe.Activities.SearchActivity;
import com.cab.digicafe.Activities.WebViewActivity;
import com.cab.digicafe.Dialogbox.CustomDialogClass;
import com.cab.digicafe.Dialogbox.UpdateDialogClass;
import com.cab.digicafe.Helper.Inad;
import com.cab.digicafe.Helper.SessionManager;
import com.cab.digicafe.Helper.SharedPrefUserDetail;
import com.cab.digicafe.Model.ModelCustom;
import com.cab.digicafe.Model.ShareModel;
import com.cab.digicafe.Model.Userprofile;
import com.cab.digicafe.MyCustomClass.CheckVersion;
import com.cab.digicafe.MyCustomClass.ModelDevice;
import com.cab.digicafe.MyCustomClass.SendDeviceInfo;
import com.cab.digicafe.Rest.ApiClient;
import com.cab.digicafe.Rest.ApiInterface;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.UUID;

import okhttp3.Headers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.cab.digicafe.services.RefererDataReciever.REFERRER_DATA;

public class LoginActivity extends MyAppBaseActivity implements GoogleApiClient.OnConnectionFailedListener {
    Button login;
    Button signup;
    CheckBox cbShowPwd;
    EditText username;
    TextInputEditText password;
    String str_username;
    String str_password;
    TextInputLayout usernamewrapper;
    TextInputLayout passwordwrapper;
    SessionManager sessionManager;
    private ProgressDialog dialog;
    ImageView logo;
    TextView tv_forgot_pwd, tv_prodordev, tvRef;
    boolean isfromsignup = false;
    RelativeLayout rl_login_with_google;
    private static final int RC_SIGN_IN = 420;
    private GoogleApiClient mGoogleApiClient;
    String gp_name, gp_email, gp_googleid;
    boolean isGoogle = false;
    String str_actype = "Personal";
    String str_firstname = "";
    String str_lastname = "";
    String str_area = "";
    String str_mobile = "";
    String str_bussinsee_user_id = "";
    String str_latitude = "";
    String str_longtitude = "";
    String str_countrycode = "";
    String str_email = "";
    String str_confirmpassword = "";
    String str_newsletter = "";
    String str_terms = "Y";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        TextView tv_version = (TextView) findViewById(R.id.tv_version);
        login = (Button) findViewById(R.id.btn);
        cbShowPwd = (CheckBox) findViewById(R.id.cbShowPwd);
        signup = (Button) findViewById(R.id.signup);
        dialog = new ProgressDialog(LoginActivity.this);
        username = (EditText) findViewById(R.id.username);
        password = (TextInputEditText) findViewById(R.id.password);
        tv_forgot_pwd = (TextView) findViewById(R.id.tv_forgot_pwd);
        tv_prodordev = (TextView) findViewById(R.id.tv_prodordev);
        tvRef = (TextView) findViewById(R.id.tvRef);
        logo = (ImageView) findViewById(R.id.logo);
        sessionManager = new SessionManager(this);
        usernamewrapper = (TextInputLayout) findViewById(R.id.usernameWrapper);
        passwordwrapper = (TextInputLayout) findViewById(R.id.passwordWrapper);
        rl_login_with_google = (RelativeLayout) findViewById(R.id.rl_login_with_google);

        //double v =  1111111.10;
        //tvRef.setText(CurrencyCommaSeperator.convertStringToCommaFraction(v) + "");

        if (getString(R.string.BASEURL).equalsIgnoreCase("http://Dev.chitandbridge.com/prerelease/"))  // Dev
        {
            tv_prodordev.setVisibility(View.VISIBLE);
        } else { // prod
            tv_prodordev.setVisibility(View.GONE);
        }

        tvRef.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this)
                        .setTitle("Coderminion.com Android Referer : ")
                        .setMessage("Recieved Data is :  " + getReferer(LoginActivity.this))
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                            }
                        });
                builder.show();

            }
        });
        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(LoginActivity.this, SignupActivity.class);
                startActivity(i);
            }
        });

        cbShowPwd.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    // show password
                    password.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                } else {
                    // hide password
                    password.setTransformationMethod(PasswordTransformationMethod.getInstance());
                }
                password.setSelection(password.getText().length());
            }
        });

        str_username = "";
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                str_username = null;

            } else {
                str_username = extras.getString("username");
                if (extras.containsKey("isfromsignup")) {
                    isfromsignup = extras.getBoolean("isfromsignup");
                }


            }
        } else {
            str_username = (String) savedInstanceState.getSerializable("username");

        }
        if (!ChangepasswordActivity.currentuser.equals("")) {
            str_username = ChangepasswordActivity.currentuser;
            ChangepasswordActivity.currentuser = "";

        }
        username.setText(str_username);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                str_username = username.getText().toString();
                str_password = password.getText().toString();

                if (str_username == null || str_username.length() == 0) {
                    usernamewrapper.setError("Please enter username");
                } else if (str_password == null || str_password.length() == 0) {
                    passwordwrapper.setError("Please enter password");
                } else {
                    Inad.hasActiveInternetConnection(LoginActivity.this, new Inad.Netcallback() {
                        @Override
                        public void netcall(boolean isnet) {

                            if (isnet) {
                                checkversioncode();
                            } else {
                                new CustomDialogClass(LoginActivity.this, new CustomDialogClass.OnDialogClickListener() {
                                    @Override
                                    public void onDialogImageRunClick(int positon) {
                                        if (positon == 0) {

                                        } else if (positon == 1) {

                                        }
                                    }
                                }, "Alert\nInternet connection is required!").show();
                            }
                        }
                    });
                }
            }
        });

        tv_forgot_pwd.setPaintFlags(tv_forgot_pwd.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        findViewById(R.id.tv_forgot_pwd).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LoginActivity.this, ForgotPwdActivity.class);
                startActivity(i);
            }
        });


        findViewById(R.id.rl_poweredby).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(LoginActivity.this, LogoActivity.class);
                i.putExtra("session", sessionManager.getsession());
                i.putExtra("isstatic", true);
                startActivity(i);
            }
        });

        int versionCode = 1;
        PackageInfo pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            versionCode = pInfo.versionCode;

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        tv_version.setText("V " + versionCode + ".0");


        findViewById(R.id.ll_business).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = "https://play.google.com/store/apps/details?id=com.cab.digimart";
               /* Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);*/
                String extTxt = url + "\n\n" + "For business registration";

                Intent share = new Intent(android.content.Intent.ACTION_SEND);
                share.setType("text/plain");
                share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                share.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.business));
                share.putExtra(Intent.EXTRA_TEXT, extTxt);
                startActivity(Intent.createChooser(share, "Share"));
            }
        });

        String[] PERMISSIONS = {android.Manifest.permission.CALL_PHONE, android.Manifest.permission.ACCESS_COARSE_LOCATION,
                android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                android.Manifest.permission.READ_EXTERNAL_STORAGE};
        if (!Inad.hasPermissions(this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, 22);
        } else {

        }

        findViewById(R.id.ll_pp).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LoginActivity.this, WebViewActivity.class);
                i.putExtra("url", "https://s3-ap-southeast-1.amazonaws.com/cab-policy/privacy_statement.pdf");
                i.putExtra("title", "Privacy Policy");
                startActivity(i);

            }
        });


        String ref = getReferer(this);
        if (ref != null && !ref.isEmpty()) {
            Inad.alerterInfo("Referrer found", "Please login/signup to view supplier.", this);
        }


        if (getString(R.string.app_name_condition).equalsIgnoreCase(getString(R.string.b1_digimart))) {
            findViewById(R.id.ll_business).setVisibility(View.INVISIBLE);
        }

        initializeGPlusSettings();

        rl_login_with_google.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //showProgressDialog();

                if (dialog != null && !dialog.isShowing()) {
                    dialog.setMessage("Logging In ...");
                    dialog.show();
                }

                signIn();
            }
        });

    }

    String u_name = "";

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    //  Login With G+
    private void initializeGPlusSettings() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);

        if (requestCode == RC_SIGN_IN) {

            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleGPlusSignInResult(result);
        }

        // fb -  callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void handleGPlusSignInResult(GoogleSignInResult result) {

        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            gp_name = acct.getDisplayName();
            // String personPhotoUrl = acct.getPhotoUrl().toString();
            gp_email = acct.getEmail();
            gp_googleid = acct.getId();
            String familyName = acct.getFamilyName();
            //client_google();
            Log.d("GoogleProfile", "Name: " + gp_name + ", email: " + gp_email + ", googleid: " + gp_googleid + ", Image: " + /*personPhotoUrl +*/ ", Family Name: " + familyName);
            // new loginwithgoogle().execute();
            str_username = gp_googleid;
            str_password = gp_googleid.substring(0, 9);
            isGoogle = true;

            Inad.hasActiveInternetConnection(LoginActivity.this, new Inad.Netcallback() {
                @Override
                public void netcall(boolean isnet) {

                    if (isnet) {
                        checkversioncode();
                    } else {
                        new CustomDialogClass(LoginActivity.this, new CustomDialogClass.OnDialogClickListener() {
                            @Override
                            public void onDialogImageRunClick(int positon) {
                                if (positon == 0) {

                                } else if (positon == 1) {

                                }
                            }
                        }, "Alert\nInternet connection is required!").show();
                    }
                }
            });
        } else {

            Log.d("GoogleProfile", "" + result.getStatus() + result.getSignInAccount());
            Toast.makeText(this, "error", Toast.LENGTH_SHORT).show();
            // Signed out, show unauthenticated UI.

        }
    }

    String pwd = "";

    public void usersignin() {

        if (dialog != null && !dialog.isShowing()) {
            dialog.setMessage("Logging In ...");
            dialog.show();
        }

        ApiInterface apiService = ApiClient.getClient(LoginActivity.this).create(ApiInterface.class);
        u_name = "";
        if (str_username.contains("@") && str_username.contains(".br")) {  // employee - athi@blrmc2.br
            u_name = str_username;
        } else if (str_username.matches("[0-9.]*"))   // consumer - all digits
        {
            u_name = str_username + "@" + sessionManager.getAggregator_ID() + ".cr";
        } else {  // business
            u_name = str_username;
        }

        if (isGoogle) {
            pwd = gp_googleid.substring(0, 9);
        } else {
            pwd = password.getText().toString();
        }

        Call call = apiService.getUserLogin(u_name, str_password);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                int statusCode = response.code();
                Headers headerList = response.headers();
                Log.d("testresponse", response.toString() + " ");
                if (response.isSuccessful()) {
                    String a = new Gson().toJson(response.body());
                    String cookie = "";
                    try {
                        cookie = response.headers().get("Set-Cookie");
                        String[] cookies = cookie.split(";");
                        Log.d("test", cookies[0] + " ");
                        cookie = cookies[0];

                    } catch (Exception e) {
                    }
                    sessionManager.setUserWithAggregator(u_name);
                    sessionManager.setcurrentuname(str_username);
                    sessionManager.setPasswd(pwd);
                    afterlogin(str_username, cookie);

                    /*  */
                } else {

                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        if (isGoogle) {
                            userregistration();
                        } else {
                            dialog.dismiss();
                            Toast.makeText(LoginActivity.this, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();
                        }
                    } catch (Exception e) {
                        Toast.makeText(LoginActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }

                //  List<Movie> movies = response.body().getResults();
                //  recyclerView.setAdapter(new MoviesAdapter(movies, R.layout.list_item_movie, getApplicationContext()));
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // Log error here since request failed
                // Log.e("error message", t.toString());
                dialog.dismiss();
            }
        });
    }

    String u_name1 = "";

    public void userregistration() {

        if (dialog != null && !dialog.isShowing()) {
            dialog.setMessage("Logging In ...");
            dialog.show();
        }

        ApiInterface apiService =
                ApiClient.getClient(LoginActivity.this).create(ApiInterface.class);
        //str_mobile = str_mobile+"@"+sessionManager.getAggregator_ID()+".cr";

        //u_name = str_mobile + "@" + sessionManager.getAggregator_ID() + ".cr";
        if (str_actype.equals("Personal")) {
            u_name1 = gp_googleid + "@" + sessionManager.getAggregator_ID() + ".cr";
        }

        Log.e("u_name1", u_name1);
        str_mobile = "";
        str_password = gp_googleid.substring(0, 9);
        str_confirmpassword = gp_googleid.substring(0, 9);
        str_firstname = "Temp";
        str_email = gp_email;
        str_lastname = "1";

        JSONObject joField = new JSONObject();
        try {
            joField.put("is_g_plus_sign", "yes");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        // for personal / business
        Call call = apiService.getUserRegistration(str_actype, str_firstname, str_lastname, str_latitude, str_longtitude, str_area, str_mobile, str_email, u_name1, str_password, str_confirmpassword, str_newsletter, str_terms, str_countrycode, joField);

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {

                int statusCode = response.code();
                Log.e("response_message", "" + statusCode);

                if (response.isSuccessful()) {

                    usersignin();

                    isGoogle = false;

                } else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Log.e("response_message", jObjError.toString());

                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        JSONObject jObjErrordetails = jObjErrorresponse.getJSONObject("errordetail");
                        String username = jObjErrordetails.getString("username");

                        Inad.alerter("Alert", "Username already register!", LoginActivity.this);


                        //Toast.makeText(SignupActivity.this, jObjErrordetails.toString(), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(LoginActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }

                //  List<Movie> movies = response.body().getResults();
                //  recyclerView.setAdapter(new MoviesAdapter(movies, R.layout.list_item_movie, getApplicationContext()));
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // Log error here since request failed
                // Log.e("error message", t.toString());
                dialog.dismiss();

            }
        });
    }

    public void checkversioncode() {

        if (dialog != null && !dialog.isShowing()) {
            dialog.setMessage("Logging In ...");
            dialog.show();
        }

        CheckVersion checkVersion = new CheckVersion(getApplicationContext(), sessionManager.getAggregator_ID(), new CheckVersion.Apicallback() {
            @Override
            public void onGetResponse(String a, boolean issuccess) {

            }

            @Override
            public void onUpdate(boolean isupdateforce, String ischeck) {


                if (ischeck.equalsIgnoreCase("ys")) {

                    if (isGoogle) {

                    } else {
                        dialog.dismiss();
                    }

                    new UpdateDialogClass(LoginActivity.this, new UpdateDialogClass.OnDialogClickListener() {
                        @Override
                        public void onDialogImageRunClick(int positon) {
                            if (positon == 0) {

                                usersignin();
                            } else if (positon == 1) {
                                final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                                try {
                                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                                } catch (android.content.ActivityNotFoundException anfe) {
                                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                                }


                            }
                        }
                    }, "Update is available !", isupdateforce).show();
                } else {
                    usersignin();
                }
            }
        });

    }

    ArrayList<ModelDevice> al_user = new ArrayList<>();

    boolean isuseravailable = false;
    int userpos = 0;
    String device_id = "";

    public void afterlogin(final String user, final String session) {
        final String Device_token = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);  // 16 length - change if reset

        device_id = UUID.randomUUID().toString();  // 32 length(Change everytime)


        final Gson gson = new Gson();
        //String array_flow_json = gson.toJson(array_flow);

        String json = SharedPrefUserDetail.getString(LoginActivity.this, SharedPrefUserDetail.loginuser, "");
        Type type = new TypeToken<ArrayList<ModelDevice>>() {
        }.getType();
        al_user = gson.fromJson(json, type);

        if (al_user == null) {
            al_user = new ArrayList<>();
        }

        isuseravailable = false;
        for (int i = 0; i < al_user.size(); i++) {
            String alluser = al_user.get(i).getUsernm();
            if (user.equals(alluser)) {
                isuseravailable = true;
                userpos = i;
                device_id = al_user.get(i).getD_id();
                break;
            }
        }


        // String u_name = str_username + "@" + sessionManager.getAggregator_ID() + ".cr";
        SendDeviceInfo getdet = new SendDeviceInfo(LoginActivity.this, session, sessionManager.getAggregatorprofile(), device_id, Device_token, new SendDeviceInfo.ApiDeviceInfocallback() {
            @Override
            public void onGetResponse(boolean issuccess) {
                if (issuccess) {

                    if (!isuseravailable) {
                        al_user.add(new ModelDevice(user, device_id));
                    }


                    sessionManager.create_Usersession(session);
                    sessionManager.setIsLogin(true);

                    String str_array = gson.toJson(al_user);
                    SharedPrefUserDetail.setString(LoginActivity.this, SharedPrefUserDetail.loginuser, str_array);
                    sessionManager.setTLoginuser(str_array);
                    checkisagree();

                } else {
                    if (dialog != null) dialog.dismiss();
                }
            }

            @Override
            public void onUpdate(boolean isupdateforce, String ischeck) {

            }
        });

    }

    public void redirecttohome(boolean isTmp) {

        setAppMode();

        findViewById(R.id.rl_pb).setVisibility(View.GONE);

        if (dialog != null) {
            dialog.dismiss();
        }

       /* if (!isuseravailable) {
            Intent i = new Intent(LoginActivity.this, ProfileActivity.class);
            i.putExtra("session", sessionManager.getsession());
            i.putExtra("isback", false);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
        } else {*/

        if (isuseravailable) {
            String building_id = al_user.get(userpos).getBuild_id();
            String industry_id = al_user.get(userpos).getInd_id();
            sessionManager.setBuilding_id(building_id);
            sessionManager.setIndustry_id(industry_id);
        } else {

        }

        if (str_username.contains("@") && str_username.contains(".br")) // Employee
        {
            sessionManager.setisBusinesslogic(false);
            sessionManager.setIsConsumer(false);
            sessionManager.setIsEmployee(true);


            Intent i = new Intent(LoginActivity.this, SearchActivity.class);  // All
            i.putExtra("session", sessionManager.getsession());
            i.putExtra("isfromsignup", isfromsignup);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
        } else if (str_username.matches("[0-9.]*")) {  // consumer
            sessionManager.setisBusinesslogic(false);
            sessionManager.setIsConsumer(true);
            sessionManager.setIsEmployee(false);


            ShareModel.getI().isAddShopAuto = "ys";

            if (isTmp) {
                Intent i = new Intent(LoginActivity.this, ProfileActivity.class);
                i.putExtra("session", sessionManager.getsession());
                i.putExtra("isback", false);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
            } else {
                Intent i = new Intent(LoginActivity.this, MySupplierActivity.class);
                i.putExtra("session", sessionManager.getsession());
                i.putExtra("isfromsignup", isfromsignup);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
            }

        } else  // Business
        {
            sessionManager.setisBusinesslogic(true);
            sessionManager.setIsConsumer(false);
            sessionManager.setIsEmployee(false);

            if (isTmp) {
                Intent i = new Intent(LoginActivity.this, ProfileActivity.class);
                i.putExtra("session", sessionManager.getsession());
                i.putExtra("isback", false);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
            } else {
                Intent i = new Intent(LoginActivity.this, SearchActivity.class);  // All
                i.putExtra("session", sessionManager.getsession());
                i.putExtra("isfromsignup", isfromsignup);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                startActivity(i);
            }
        }
    }

/*
    public static byte[] encrypt(String plainText, String string) throws Exception {
        //System.out.println("encrypting" + encryptionKey.length());


// Check length, in characters
        System.out.println("Length of string is " + string.length()); // prints "11"

// Check encoded sizes
        final byte[] utf8Bytes = string.getBytes("UTF-8");
        System.out.println(utf8Bytes.length); // prints "11"

        final byte[] utf16Bytes= string.getBytes("UTF-16");
        System.out.println(utf16Bytes.length); // prints "24"

        final byte[] utf32Bytes = string.getBytes("UTF-32");
        System.out.println(utf32Bytes.length); // prints "44"

        final byte[] isoBytes = string.getBytes("ISO-8859-1");
        System.out.println(isoBytes.length); // prints "11"

        final byte[] winBytes = string.getBytes("CP1252");
        System.out.println(winBytes.length); // prints "11"

        Cipher cipher = Cipher.getInstance("AES/CBC/NoPadding");
        System.out.println("enc 2");

        //SecretKeySpec key = new SecretKeySpec(encryptionKey.getBytes("UTF-8"), "AES");
        System.out.println("enc 3");

        //cipher.init(Cipher.ENCRYPT_MODE, key,new IvParameterSpec(IV.getBytes("UTF-8")));
        System.out.println("enc 4");
        return cipher.doFinal(plainText.getBytes("UTF-16"));
    }

    public static String byteArrayToHex(byte[] a) {
        StringBuilder sb = new StringBuilder(a.length * 2);
        for(byte b: a)
            sb.append(String.format("%02x", b & 0xff));
        return sb.toString();
    }
*/


    public void checkisagree() {

        ArrayList<ModelCustom> al_user = new ArrayList<>();

        final Gson gson = new Gson();


        boolean isuseravailable = false;

        String json = SharedPrefUserDetail.getString(LoginActivity.this, SharedPrefUserDetail.userwithagreeid, "");


        Type type = new TypeToken<ArrayList<ModelCustom>>() {
        }.getType();

        al_user = gson.fromJson(json, type);

        if (al_user == null) {
            al_user = new ArrayList<>();
        }

        String user = sessionManager.getcurrentu_nm();

        String agree = "";
        isuseravailable = false;
        for (int i = 0; i < al_user.size(); i++) {
            String alluser = al_user.get(i).getUsernm();
            if (user.equals(alluser)) {
                isuseravailable = true;
                agree = al_user.get(i).getAggreid();
                break;
            }

        }


        if (isuseravailable) {

            setaggregator(agree);
        } else {
            setaggregator(getString(R.string.Aggregator));
            //redirecttohome();
        }


    }


    public void setaggregator(final String aggre) {


        ApiInterface apiService =
                ApiClient.getClient(LoginActivity.this).create(ApiInterface.class);


        Call call;

        call = apiService.getagregatorProfile(aggre);

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                int statusCode = response.code();

                if (response.isSuccessful()) {

                    try {
                        String a = new Gson().toJson(response.body());
                        sessionManager.setProfUser(a);
                        JSONObject Jsonresponse = new JSONObject(a);
                        JSONObject Jsonresponseinfo = Jsonresponse.getJSONObject("response");


                        Log.e("test", " trdds" + Jsonresponse.toString());
                        JSONArray aggregatorobj = Jsonresponseinfo.getJSONArray("data");
                        JSONObject aggregator = aggregatorobj.getJSONObject(0);
                        String aggregatorbridgeid = aggregator.getString("bridge_id");
                        String isactive = aggregator.getString("business_status");
                        sessionManager.setAggregatorprofile(aggregatorbridgeid);
                        Log.e("aggregatorbridgeid", aggregatorbridgeid);

                        if (aggregator.has("company_detail_long")) {
                            String servicearea = aggregator.getString("company_detail_long");
                            sessionManager.setservicearea(servicearea);
                        }

                        try {
                            String company_image = aggregator.getString("company_image");
                            sessionManager.setCompImg(company_image);

                        } catch (Exception e) {

                        }
                        sessionManager.setAggregator_ID(aggre);
                        getuserprofile(sessionManager.getsession());
                        //redirecttohome();
                        // Toast.makeText(ProfileActivity.this, "Profile Updated Successfully!", Toast.LENGTH_LONG).show();


                    } catch (Exception e) {
                        findViewById(R.id.rl_pb).setVisibility(View.GONE);

                    }
                } else {

                    dialog.dismiss();
                    findViewById(R.id.rl_pb).setVisibility(View.GONE);
                }

                //  List<Movie> movies = response.body().getResults();
                //  recyclerView.setAdapter(new MoviesAdapter(movies, R.layout.list_item_movie, getApplicationContext()));
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                dialog.dismiss();
                findViewById(R.id.rl_pb).setVisibility(View.GONE);
            }
        });
    }


    public void getuserprofile(final String sessionvalue) {


        ApiInterface apiService =
                ApiClient.getClient(LoginActivity.this).create(ApiInterface.class);
        Call call;

        call = apiService.getProfile(sessionvalue, sessionManager.getAggregatorprofile());

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                findViewById(R.id.rl_pb).setVisibility(View.GONE);
                int statusCode = response.code();
                Log.e("test", " trdds");

                Headers headerList = response.headers();
                Log.d("test", statusCode + " ");

                //  String cookie = response.;
                //  Log.d("test", cookie+ " ");

                if (response.isSuccessful()) {
                    String a = new Gson().toJson(response.body());
                    Log.e("response", a);
                    try {
                        JSONObject jObjRes = new JSONObject(a);
                        JSONObject jObjdata = jObjRes.getJSONObject("response");
                        JSONObject jObjResponse = jObjdata.getJSONObject("data");
                        Log.d("test", jObjResponse.toString() + " ");
                        Userprofile obj = new Gson().fromJson(jObjResponse.toString(), Userprofile.class);

                        String b_id_for_employee = jObjResponse.getString("bridge_id");
                        String currency_code = jObjResponse.getString("currency_code");
                        String symbol_native = jObjResponse.getString("symbol_native");
                        String symbol = jObjResponse.getString("symbol");
                        String decimal_digits = jObjResponse.getString("decimal_digits");
                        String currency_code_id = jObjResponse.getString("currency_code_id");

                        sessionManager.setE_bridgeid(b_id_for_employee);
                        sessionManager.setuserprofile(jObjResponse.toString());


                        SharedPrefUserDetail.setString(LoginActivity.this, SharedPrefUserDetail.currency_code, currency_code);
                        SharedPrefUserDetail.setString(LoginActivity.this, SharedPrefUserDetail.symbol_native, symbol_native);
                        SharedPrefUserDetail.setString(LoginActivity.this, SharedPrefUserDetail.currency_code_id, currency_code_id);
                        SharedPrefUserDetail.setString(LoginActivity.this, SharedPrefUserDetail.symbol, symbol);
                        SharedPrefUserDetail.setString(LoginActivity.this, SharedPrefUserDetail.decimal_digits, decimal_digits);

                        if (obj.getEmail_id().equalsIgnoreCase("Temp@gmail.com")) {


                            redirecttohome(true);


                        } else { // check ageeid

                            ArrayList<ModelCustom> al_user = new ArrayList<>();

                            final Gson gson = new Gson();


                            boolean isuseravailable = false;

                            String json = SharedPrefUserDetail.getString(LoginActivity.this, SharedPrefUserDetail.userwithagreeid, "");


                            Type type = new TypeToken<ArrayList<ModelCustom>>() {
                            }.getType();

                            al_user = gson.fromJson(json, type);

                            if (al_user == null) {
                                al_user = new ArrayList<>();
                            }

                            String user = sessionManager.getcurrentu_nm();

                            String agrreid = "";
                            isuseravailable = false;
                            for (int i = 0; i < al_user.size(); i++) {
                                String alluser = al_user.get(i).getUsernm();
                                if (user.equals(alluser)) {
                                    isuseravailable = true;
                                    agrreid = al_user.get(i).getAggreid();
                                    break;
                                }

                            }

                           /* if(!isuseravailable)
                            {
                                Intent i = new Intent(LoginActivity.this, ProfileActivity.class);
                                i.putExtra("session", sessionManager.getsession());
                                i.putExtra("isback", false);
                                startActivity(i);
                            }
                            else {
                                //sessionManager.setAggregator_ID(agrreid);

                                redirecttohome();
                            }
*/

                            redirecttohome(false);
                        }


                    } catch (JSONException e) {

                    }

                } else {

                    if (dialog != null) dialog.dismiss();
                    Log.e("test", " trdds1");

                    try {
                       /* JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Toast.makeText(MainActivity.this, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();*/
                    } catch (Exception e) {
                        // Toast.makeText(LoginActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }

                //  List<Movie> movies = response.body().getResults();
                //  recyclerView.setAdapter(new MoviesAdapter(movies, R.layout.list_item_movie, getApplicationContext()));
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // Log error here since request failed
                Log.e("error message", t.toString());
            }
        });
    }


    private final BroadcastReceiver mUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

           /* AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this)
                    .setTitle("Coderminion.com Android Referer : ")
                    .setMessage("Recieved Data is :  " + getReferer(LoginActivity.this))
                    .setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                        }
                    });
            builder.show();*/
        }
    };

    public String getReferer(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
        if (!sp.contains(REFERRER_DATA)) {
            return "";
        }
        return sp.getString(REFERRER_DATA, null);
    }

    CustomDialogClass customDialogClass;

    @Override
    protected void onResume() {
        super.onResume();
        //LocalBroadcastManager.getInstance(this).registerReceiver(mUpdateReceiver, new IntentFilter(ACTION_UPDATE_DATA));

        // register connection status listener
        App.getInstance().setConnectivityListener(new ConnectivityReceiver.ConnectivityReceiverListener() {
            @Override
            public void onNetworkConnectionChanged(boolean isConnected) {

                App.getInstance().setConnectivityListener(new ConnectivityReceiver.ConnectivityReceiverListener() {
                    @Override
                    public void onNetworkConnectionChanged(boolean isConnected) {

                        if (isConnected) {

                            customDialogClass = new CustomDialogClass(LoginActivity.this, new CustomDialogClass.OnDialogClickListener() {
                                @Override
                                public void onDialogImageRunClick(int positon) {
                                    if (positon == 0) {

                                    } else if (positon == 1) {

                                    }
                                }
                            }, "Alert\nInternet connection is required!");
                            customDialogClass.show();

                        } else {
                            if (customDialogClass != null) customDialogClass.dismiss();

                        }


                    }
                });

            }
        });
    }

    @Override
    protected void onPause() {
        //LocalBroadcastManager.getInstance(this).unregisterReceiver(mUpdateReceiver);
        super.onPause();
    }

    public void setAppMode() {
        if (getString(R.string.app_name_condition).equalsIgnoreCase(getString(R.string.b1_digimart))) {
            SharedPrefUserDetail.setBoolean(this, SharedPrefUserDetail.isAppHyperlocalMode, true); // pass flag
        } else if (getString(R.string.app_name_condition).equalsIgnoreCase(getString(R.string.c2_digicafe))) {
            SharedPrefUserDetail.setBoolean(this, SharedPrefUserDetail.isAppHyperlocalMode, true); // pass flag
        } else { // ! Digimart, Digicafe
            SharedPrefUserDetail.setBoolean(this, SharedPrefUserDetail.isAppHyperlocalMode, false); // pass flag
        }
    }




}
