package com.cab.digicafe;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

public class ReportActivity extends AppCompatActivity {

    TextView error;
    String versionCode;
    String result = "NA";
    String deviceinfo = "NA";
    String userID;
    boolean is_login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);
        Log.e("report", "report");


        Intent i = getIntent();
        result = i.getStringExtra("error");
        deviceinfo = i.getStringExtra("device_info");
        versionCode = i.getStringExtra("versionCode");

        alert_report();

        Log.e("errorReport.toString()", "" + result);

    }

    public void alert_report() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("Report Crash");
        alertDialogBuilder.setMessage("The app has unfortunately stopped");

        alertDialogBuilder.setPositiveButton("Report", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                api_hit();
            }
        });

        alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                call_page();
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void call_page() {
        //     is_login = sessionManager.isLoggedIn();
        if (is_login) {
            Intent introIntent = new Intent(this, SplashActivity.class);
            startActivity(introIntent);
        } else {
            Intent introIntent = new Intent(this, SplashActivity.class);
            startActivity(introIntent);
        }
    }


    public void api_hit() {

     /*   Log.e("params","params is bug_report comi");
        Log.e("params","params is bug_report comi result"+result);
        Log.e("params","params is bug_report com deviceinfoi"+deviceinfo);
        Log.e("params","params is bug_report comi versionCode"+versionCode);

        Endpoints.urlactionname = "bug_report";
        JSONObject params = new JSONObject();
        try {
            params.put("tbl_user_profileunique_id", facebook_user_detail.getUnique_id());
            params.put("bug", result);
            params.put("device_info", deviceinfo);
            params.put("version", versionCode);
            Log.e("params","params is"+params);
} catch (JSONException e) {
        e.printStackTrace();
        }
        new TaskString(this, params).execute();*/
    }

  /*  @Override
    public void onString_is(String listMovies) {
        Toast.makeText(getBaseContext(),getResources().getString(R.string.report_success), Toast.LENGTH_SHORT).show();
        call_page();
    }*/

    @Override
    public void onBackPressed() {
        call_page();
    }
}
