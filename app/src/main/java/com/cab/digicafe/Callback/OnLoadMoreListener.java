package com.cab.digicafe.Callback;

/**
 * Created by crayond on 08/11/2017.
 */

public interface OnLoadMoreListener {
    void onLoadMore();
}