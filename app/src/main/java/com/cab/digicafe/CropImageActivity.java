package com.cab.digicafe;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.cab.digicafe.Helper.FileHelper;
import com.github.chrisbanes.photoview.PhotoView;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.ritesh.ratiolayout.RatioRelativeLayout;
import com.ritesh.ratiolayout.models.enums.FixedAttribute;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CropImageActivity extends AppCompatActivity {
    @BindView(R.id.ivImage)
    PhotoView ivImage;
    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.tvNext)
    ImageView tvNext;
    String image;
    Bitmap bitmap;
    @BindView(R.id.rlFrame)
    RatioRelativeLayout rlFrame;
    int rotateImage = 0;
    private FirebaseAnalytics mFirebaseAnalytics;
    Bitmap resizedBitmap=null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crop_image);
        ButterKnife.bind(this);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        image = getIntent().getStringExtra("ImageArray");

        rlFrame.setBackgroundResource(R.drawable.crop_back);


        Glide.with(CropImageActivity.this).load(image).diskCacheStrategy(DiskCacheStrategy.SOURCE).into(ivImage);

        File file = new File(image);
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = 2;
        bmOptions.inPurgeable = true;
        bitmap = BitmapFactory.decodeFile(file.getAbsolutePath(), bmOptions);
        //  ivImage.setOnTouchListener(new MultiTouchListener());

        Uri uri = FileProvider.getUriForFile(CropImageActivity.this, getPackageName() + ".provider", file);
        rotateImage = getCameraPhotoOrientation(CropImageActivity.this, uri, image);
        setViewWidth(7, 4);

        tvNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    ivImage.setDrawingCacheEnabled(true);
                    final Bitmap b = Bitmap.createBitmap(ivImage.getDrawingCache());
                    ivImage.setDrawingCacheEnabled(false);
                    int width = b.getWidth();
                    int height = b.getHeight();
                    float scaleWidth = ((float) 700) / width;
                    float scaleHeight = ((float) 400) / height;
                    // CREATE A MATRIX FOR THE MANIPULATION
                    Matrix matrix = new Matrix();
                    // RESIZE THE BIT MAP
                    matrix.postScale(scaleWidth, scaleHeight);
                    resizedBitmap = Bitmap.createBitmap(
                            b, 0, 0, width, height, matrix, false);
                    b.recycle();

                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {

                        }
                    }, 1000);

                    System.currentTimeMillis();
                    new AsyncTask<String, String, String>() {
                        @Override
                        protected String doInBackground(String... voids) {
                            String path = "";
                            try {
                                path = FileHelper.saveBitmapFileIntoExternalStorageWithTitle(resizedBitmap, "Img" + System.currentTimeMillis());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            return path;
                        }

                        @Override
                        protected void onPostExecute(String aVoid) {
                            if (aVoid != null && !aVoid.isEmpty()) {
                                Intent returnIntent = new Intent();
                                returnIntent.putExtra("result", aVoid);
                                setResult(Activity.RESULT_OK, returnIntent);
                                finish();
                            }

                            super.onPostExecute(aVoid);
                        }
                    }.execute();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 101) {
            if (resultCode == RESULT_OK) {
                setResult(RESULT_OK, data);
                finish();

            }
        }
    }

    public Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(
                bm, 0, 0, width, height, matrix, false);
        bm.recycle();
        return resizedBitmap;
    }

    public void setViewWidth(int x, int y) {
        rlFrame.setRatio(FixedAttribute.WIDTH, x, y);
        rlFrame.getLayoutParams().width = ViewGroup.LayoutParams.MATCH_PARENT;
        rlFrame.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;
        ivImage.getLayoutParams().width = ViewGroup.LayoutParams.MATCH_PARENT;
        ivImage.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;
        ivImage.setScaleType(ImageView.ScaleType.CENTER_CROP);
    }

    public void setViewHight(int x, int y) {
        //  rlFrame.setRatio(FixedAttribute.HEIGHT, x, y);
        rlFrame.getLayoutParams().width = ViewGroup.LayoutParams.MATCH_PARENT;
        rlFrame.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;
        ivImage.getLayoutParams().width = ViewGroup.LayoutParams.MATCH_PARENT;
        ivImage.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;
        ivImage.setScaleType(ImageView.ScaleType.CENTER_CROP);
    }

    public int getCameraPhotoOrientation(Context context, Uri imageUri, String imagePath) {
        int rotate = 0;
        try {
            context.getContentResolver().notifyChange(imageUri, null);
            File imageFile = new File(imagePath);

            ExifInterface exif = new ExifInterface(imageFile.getAbsolutePath());
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = 270;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate = 90;
                    break;
            }

            Log.i("RotateImage", "Exif orientation: " + orientation);
            Log.i("RotateImage", "Rotate value: " + rotate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rotate;
    }

    @Override
    protected void onResume() {
        super.onResume();
        mFirebaseAnalytics.setCurrentScreen(this, "CurrentScreen: " + getClass().getSimpleName(), null);
    }
}