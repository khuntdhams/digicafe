package com.cab.digicafe;

import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.andremion.counterfab.CounterFab;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.cab.digicafe.Activities.AllTaskActivity;
import com.cab.digicafe.Activities.ArchivedListActivity;
import com.cab.digicafe.Activities.CancelTaskActivity;
import com.cab.digicafe.Activities.CatalougeTabActivity;
import com.cab.digicafe.Activities.ChageCircle;
import com.cab.digicafe.Activities.EditCurrencyForBusinessActivity;
import com.cab.digicafe.Activities.LogoActivity;
import com.cab.digicafe.Activities.MyReciptsforCustomer;
import com.cab.digicafe.Activities.MyRequsetActivity;
import com.cab.digicafe.Activities.MySupplierActivity;
import com.cab.digicafe.Activities.MyTaskActivity;
import com.cab.digicafe.Activities.RequestApis.MyRequestCall;
import com.cab.digicafe.Activities.RequestApis.MyRequst;
import com.cab.digicafe.Activities.SearchActivity;
import com.cab.digicafe.Activities.SettingsActivity;
import com.cab.digicafe.Activities.healthTemplate.SelectTemplateActivity;
import com.cab.digicafe.Activities.master.AddShopsUnderMasterActivity;
import com.cab.digicafe.Activities.master.CategoryUnderMsterActivity;
import com.cab.digicafe.Activities.master.EmployeeUnderMasterActivity;
import com.cab.digicafe.Activities.other.PromoActivity;
import com.cab.digicafe.Activities.other.UserTractionActivity;
import com.cab.digicafe.Activities.other.walkin.WalkInConsumerActivity;
import com.cab.digicafe.Adapter.DrawerAdapter;
import com.cab.digicafe.Database.SettingDB;
import com.cab.digicafe.Database.SettingModel;
import com.cab.digicafe.Database.SqlLiteDbHelper;
import com.cab.digicafe.Dialogbox.AddAddress;
import com.cab.digicafe.Dialogbox.CustomDialogClass;
import com.cab.digicafe.Dialogbox.DialogLoading;
import com.cab.digicafe.Helper.Constants;
import com.cab.digicafe.Helper.Inad;
import com.cab.digicafe.Helper.SessionManager;
import com.cab.digicafe.Helper.SharedPrefUserDetail;
import com.cab.digicafe.Model.DrawerItem;
import com.cab.digicafe.Model.Metal;
import com.cab.digicafe.Model.ModelCustom;
import com.cab.digicafe.Model.ShareModel;
import com.cab.digicafe.Model.Userprofile;
import com.cab.digicafe.MyCustomClass.JsonObjParse;
import com.cab.digicafe.MyCustomClass.ModelDevice;
import com.cab.digicafe.MyCustomClass.RefreshSession;
import com.cab.digicafe.PrintView.PrintOTPActivity;
import com.cab.digicafe.Rest.ApiClient;
import com.cab.digicafe.Rest.ApiInterface;
import com.cab.digicafe.serviceTypeClass.GetServiceTypeFromBusiUserProfile;
import com.google.firebase.dynamiclinks.DynamicLink;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Headers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.cab.digicafe.services.RefererDataReciever.REFERRER_DATA;

public class BaseActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, DrawerAdapter.OnItemClickListener {

    boolean isAppHyperlocalMode = true;

    protected FrameLayout frameLayout;

    public Uri buildDeepLink(@NonNull Uri deepLink, int minVersion) {
        String uriPrefix = getString(R.string.link_prefix);

        // Set dynamic link parameters:
        //  * URI prefix (required)
        //  * Android Parameters (required)
        //  * Deep link
        // [START build_dynamic_link]


        DynamicLink.Builder builder = FirebaseDynamicLinks.getInstance()
                .createDynamicLink()
                .setDomainUriPrefix(uriPrefix)
                .setAndroidParameters(new DynamicLink.AndroidParameters.Builder(getString(R.string.consumer_link)).build())
                .setLink(deepLink);

        // Build the dynamic link
        DynamicLink link = builder.buildDynamicLink();
        // [END build_dynamic_link]

        // Return the dynamic link as a URI
        return link.getUri();
    }

    public String getReferer(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
        if (!sp.contains(REFERRER_DATA)) {
            return "";
        }
        return sp.getString(REFERRER_DATA, "");
    }

    protected ListView mDrawerList;
    protected ArrayList<DrawerItem> mDrawerItemList;

    protected String[] listArray = {"Item 1", "Item 2", "Item 3", "Item 4", "Item 5"};
    TextView name, tv_prodordev;
    TextView email;
    RecyclerView mRecyclerView;

    protected static int position;

    private static boolean isLaunch = true;

    private DrawerLayout mDrawerLayout;
    CounterFab counterFab;

    private ActionBarDrawerToggle actionBarDrawerToggle;
    SessionManager sessionManager;
    SettingModel sm = new SettingModel();
    boolean isfrommyagree = false;
    boolean isBk = false;
    String to_bridge_id = "";
    TextView tv_openclose;
    String field_json_data = "";

    public void showMsg(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.navigation_drawer_base_layout);

        overridePendingTransition(R.anim.fadein, R.anim.fadeout);

        isAppHyperlocalMode = SharedPrefUserDetail.getBoolean(this, SharedPrefUserDetail.isAppHyperlocalMode, true); // pass flag


        if (getIntent().getExtras() != null) {
            if (getIntent().getExtras().containsKey("isBk")) {
                isBk = getIntent().getExtras().getBoolean("isBk", false);
            }
        }

        Intent intent = getIntent();
        String action = intent.getAction();
        Uri data = intent.getData();

        if (data != null) {
            showMsg(action);
        }

        sessionManager = new SessionManager(this);


        SqlLiteDbHelper dbHelper = new SqlLiteDbHelper(this);
        dbHelper.openDataBase();

        to_bridge_id = SharedPrefUserDetail.getString(this, SharedPrefUserDetail.isfrommyaggregator, "");
        if (to_bridge_id.equals("noback") || to_bridge_id.equals("")) {
            isfrommyagree = false;
        } else {
            isfrommyagree = true;
        }


        SettingDB db = new SettingDB(this);

        if (db.GetItems().size() == 0) {
            SettingModel sm = new SettingModel();
            sm.setIsdate("1");
            sm.setId(1);
            sm.setIsaddress("1");
            sm.setIsremark("1");
            sm.setIspayumoney("1");
            sm.setIsgoogleplace("1");
            sm.setIsreceipt("1");
            sm.setIsmytask("1");
            sm.setIsalltask("1");
            sm.setIssearchalltask("1");
            sm.setIsconcateoffer("1");
            sm.setIsprinttoken("0");
            sm.setIsprintreceipt("1");
            sm.setIsprintdisplaydata("1");
            sm.setIsvisiondetect("0");
            db.InsertItem(sm);
        }


        frameLayout = (FrameLayout) findViewById(R.id.content_frame);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        //mDrawerList = (ListView) findViewById(R.id.left_drawer);
        counterFab = (CounterFab) findViewById(R.id.counter_fab);

        counterFab.show();
        if (!ShareModel.getI().dbUnderMaster.isEmpty()) counterFab.hide();
        setagreeid();
        // set a custom shadow that overlays the main content when the drawer opens
        //mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);

        // set up the drawer's list view with items and click listener
    /*	mDrawerList.setAdapter(new ArrayAdapter<String>(this, R.layout.drawer_list_item, listArray));
        mDrawerList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				
				openActivity(position);
			}
		});*/
        mDrawerItemList = new ArrayList<DrawerItem>();


        // enable ActionBar app icon to behave as action to toggle nav drawer
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        getSupportActionBar().setTitle(sessionManager.getKEY_CURRENTCOMPANYNAME());

        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        android.support.v7.app.ActionBarDrawerToggle toggle = new android.support.v7.app.ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();


        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View view = navigationView.getHeaderView(0);
        //View view=navigationView.inflateHeaderView(R.layout.nav_header_main);
        mRecyclerView = (RecyclerView) navigationView.findViewById(R.id.drawerRecyclerView);
        LinearLayout rl_poweredby = (LinearLayout) navigationView.findViewById(R.id.rl_poweredby);
        TextView tv_version = (TextView) navigationView.findViewById(R.id.tv_version);
        name = (TextView) view.findViewById(R.id.username);
        tv_prodordev = (TextView) view.findViewById(R.id.tv_prodordev);
        email = (TextView) view.findViewById(R.id.email);

        if (getString(R.string.BASEURL).equalsIgnoreCase("http://Dev.chitandbridge.com/prerelease/"))  // Dev
        {
            tv_prodordev.setVisibility(View.VISIBLE);
        } else { // prod
            tv_prodordev.setVisibility(View.GONE);
        }

        ImageView iv_setting = (ImageView) view.findViewById(R.id.iv_setting);
        ImageView iv_edit = (ImageView) view.findViewById(R.id.iv_edit);
        ImageView ivShare = (ImageView) view.findViewById(R.id.ivShare);

        tv_openclose = (TextView) view.findViewById(R.id.tv_openclose);
        tv_openclose.setVisibility(View.VISIBLE);

        if (sessionManager.getIsConsumer()) { // for customer
            tv_openclose.setVisibility(View.GONE);
            ivShare.setVisibility(View.GONE);
        } else if (sessionManager.getIsEmployess()) {
            tv_openclose.setEnabled(false);
        }

        tv_openclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new AddAddress(BaseActivity.this, new AddAddress.OnDialogClickListener() {
                    @Override
                    public void onDialogImageRunClick(int positon, String add) {

                        if (positon == 0) {

                        } else if (positon == 1) {
                            SetStatus();
                            //str_actionid = str_actionid.replaceAll(",$", "");
                        }

                    }
                }, sessionManager.getPasswd()).show();
            }
        });

        iv_app = (ImageView) view.findViewById(R.id.iv_app);

        iv_setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.closeDrawer(GravityCompat.START);
                Intent i = new Intent(BaseActivity.this, SettingsActivity.class);
                i.putExtra("session", sessionManager.getsession());
                startActivity(i);
            }
        });

        ivShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // For Employee, the share option should share the business user id and business name.  Not the employee user id.
                // The share option should not be there for consumer login as it is meaningless for the consumer to share his id.
                // Add ‘blrmc2’ in MY Supplier Option connect to ‘MCC@Wipro’  here blrmc2 and MCC@WIPRO should be dynamic.  Blrmc2 is the user id and MCC@WIPRO is the shop name, This should be taken from the user profile.

                String data = sessionManager.getProfUset();

                String firstname = "";
                String bridge_id = "";
                String username = "";
                try {
                    JSONObject Jsonresponse = new JSONObject(data);
                    JSONObject Jsonresponseinfo = Jsonresponse.getJSONObject("response");
                    Log.e("test", " trdds" + Jsonresponse.toString());
                    JSONArray aggregatorobj = Jsonresponseinfo.getJSONArray("data");
                    JSONObject aggregator = aggregatorobj.getJSONObject(0);
                    firstname = aggregator.getString("firstname");
                    bridge_id = aggregator.getString("bridge_id");
                    username = aggregator.getString("username");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                getEmployeeeDetailFromProfile(businessUserId);


               /* if (sessionManager.getisBusinesslogic()) { // share from its profile
                    firstname = businaesName;
                    username = businessUserId;

                    String shareUrl = "http://play.google.com/store/apps/details?id=" + getString(R.string.consumer_link);

                    Log.d("okhttp", field_json_data+"");
                    if (field_json_data != null) {
                        String shareId = JsonObjParse.getValueEmpty(field_json_data, "shareId");
                        if(!shareId.isEmpty())username = shareId;
                    }

                    //String link = getString(R.string.BASEURL) + "request-form/" + username + "/" + bridge_id + "/INR";
                    String extTxt = shareUrl + "\n\nAdd '" + username + "' in My Supplier option to connect to '" + firstname + "'" + "\n\n";

                    Intent share = new Intent(android.content.Intent.ACTION_SEND);
                    share.setType("text/plain");
                    share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                    // Add data to the intent, the receiving app will decide
                    // what to do with it.
                    share.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.consumer));
                    share.putExtra(Intent.EXTRA_TEXT, extTxt);
                    startActivity(Intent.createChooser(share, "Share"));

                }
                else { // share employee data
                    getEmployeeeDetailFromProfile(businessUserId);
                }*/

                /*String bridge_id = sessionManager.getE_bridgeid();
                String username = "'"+sessionManager.getcurrentu_nm()+"'";
                String shopNm = "'"+catererList.get(position).getFirstname()+"'";*/


                if (sessionManager.getIsEmployess()) {
                    //extTxt = shareUrl +"\n\n"+ businessUserId;
                }


            }
        });


        rl_poweredby.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                drawer.closeDrawer(GravityCompat.START);
                Intent i = new Intent(BaseActivity.this, LogoActivity.class);
                i.putExtra("session", sessionManager.getsession());
                i.putExtra("isstatic", true);
                startActivity(i);


            }
        });

        iv_app.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                drawer.closeDrawer(GravityCompat.START);
                Intent i = new Intent(BaseActivity.this, LogoActivity.class);
                i.putExtra("session", sessionManager.getsession());
                i.putExtra("isstatic", false);
                startActivity(i);


            }
        });


        int versionCode = 1;
        PackageInfo pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            versionCode = pInfo.versionCode;

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }


        name.setSelected(true);
        email.setSelected(true);
        tv_version.setSelected(true);
        tv_version.setText("V " + versionCode + ".0");
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {

            } else {
                if (extras.containsKey("isfromsignup")) {
                    isfromsignup = extras.getBoolean("isfromsignup");
                }
            }
        } else {

        }


        if (isfrommyagree || isBk) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        }

        iv_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                }

                Intent i = new Intent(BaseActivity.this, ProfileActivity.class);
                i.putExtra("session", sessionManager.getsession());
                startActivity(i);

            }
        });


        /**
         * As we are calling BaseActivity from manifest file and this base activity is intended just to add navigation drawer in our app.
         * We have to open some activity with layout on launch. So we are checking if this BaseActivity is called first time then we are opening our first activity.
         * */
        if (isLaunch) {
            /**
             *Setting this flag false so that next time it will not open our first activity.
             *We have to use this flag because we are using this BaseActivity as parent activity to our other activity.
             *In this case this base activity will always be call when any child activity will launch.
             */
            //	isLaunch = false;
            //openActivity(0);
        }


       /* if(isfromsignup)
        {
            Intent i = new Intent(BaseActivity.this, ProfileActivity.class);
            i.putExtra("session", sessionManager.getsession());
            startActivity(i);
        }
*/


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                if (isBk) {
                    super.onBackPressed();
                } else if (isfrommyagree) {
                    if (to_bridge_id.equals("noback")) {
                        mDrawerLayout.openDrawer(GravityCompat.START);  // OPEN DRAWER
                    }
                  /*  else if(to_bridge_id.length()>10){
                        mDrawerLayout.openDrawer(GravityCompat.START);  // OPEN DRAWER
                    }*/
                    else {
                        onBackPressed();
                    }


                } else {
                    mDrawerLayout.openDrawer(GravityCompat.START);  // OPEN DRAWER
                }


                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    protected void openActivity(int position) {

        /**
         * We can set title & itemChecked here but as this BaseActivity is parent for other activity,
         * So whenever any activity is going to launch this BaseActivity is also going to be called and
         * it will reset this value because of initialization in onCreate method.
         * So that we are setting this in child activity.
         */
//		mDrawerList.setItemChecked(position, true);
//		setTitle(listArray[position]);


        BaseActivity.position = position; //Setting currently selected position in this field so that it will be available in our child activities.

        switch (position) {
            case 0:
                //	startActivity(new Intent(this, MainActivity.class));
                break;
            case 1:
                //	startActivity(new Intent(this, OrderActivity.class));
                break;
            case 2:
                //	startActivity(new Intent(this, HistoryActivity.class));
                break;
            case 3:
                //startActivity(new Intent(this, Item4Activity.class));
                break;
            case 4:
                //startActivity(new Intent(this, Item5Activity.class));
                break;

            default:
                break;
        }

        Toast.makeText(this, "Selected Item Position::" + position, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onBackPressed() {

        try {
            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            } else if (isBk) {
                super.onBackPressed();
            } else {

                ActivityManager mngr = (ActivityManager) getSystemService(ACTIVITY_SERVICE);

                List<ActivityManager.RunningTaskInfo> taskList = mngr.getRunningTasks(10);
                Log.e("pagename", this.getClass().getName());
                Log.e("pagename", taskList.get(0).topActivity.getClassName());
                Log.e("pagename", taskList.get(0).numActivities + "");
                if (taskList.get(0).numActivities == 1 &&
                        taskList.get(0).topActivity.getClassName().equals(this.getClass().getName())) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.AlertDialogCustom));
                    builder
                            .setMessage("Are you sure you want to exit?")
                            .setTitle("")
                            .setCancelable(false)
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    finish();

                                }
                            })
                            .setNegativeButton("No", null)

                            .show();
                } else {
                    super.onBackPressed();
                }
            }
        } catch (Exception e) {
            super.onBackPressed();
            e.printStackTrace();
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Log.e("samplemenu", "" + id);


        if (id == R.id.nav_camera) {
            // Handle the camera action
            Log.e("samplemenu", "camera" + id);
            finish();
            Intent i = new Intent(this, HistoryActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            i.putExtra("session", sessionManager.getsession());
            startActivity(i);
        } else if (id == R.id.nav_archived) {
            finish();
            Intent i = new Intent(this, ArchivedListActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            i.putExtra("session", sessionManager.getsession());
            startActivity(i);

        } else if (id == R.id.nav_gallery) {
            Log.e("samplemenu", "gallery" + id);
            finish();
            Intent i = new Intent(this, MainActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);

        } else if (id == R.id.nav_slideshow) {

            Intent i = new Intent(this, ChangepasswordActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            i.putExtra("session", sessionManager.getsession());
            startActivity(i);
        } else if (id == R.id.nav_manage) {
            logoutuser();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void logoutuser() {
      /*  AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.AlertDialogCustom));
        builder
                .setMessage("Are you sure you want to logout?")
                .setTitle("")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        SqlLiteDbHelper dbHelper = new SqlLiteDbHelper(BaseActivity.this);
                        dbHelper.deleteallfromcart();
                        BaseActivity.this.finish();
                        sessionManager.logoutUser(BaseActivity.this);
                    }
                })
                .setNegativeButton("No", null)

                .show();
*/
        new CustomDialogClass(this, new CustomDialogClass.OnDialogClickListener() {
            @Override
            public void onDialogImageRunClick(int positon) {
                if (positon == 0) {

                } else if (positon == 1) {
                    SqlLiteDbHelper dbHelper = new SqlLiteDbHelper(BaseActivity.this);
                    dbHelper.deleteallfromcart();
                    sessionManager.logoutUser(BaseActivity.this, new SessionManager.IlogOut() {
                        @Override
                        public void logOut() {
                            BaseActivity.this.finish();
                        }
                    });

                }
            }
        }, "LOGOUT ?").show();
    }

    String businessUserId = "", businaesName = "", businaesBId = ""; // for employee share
    Userprofile obj;

    public void getuserprofile(final String sessionvalue) { //


        ApiInterface apiService =
                ApiClient.getClient(BaseActivity.this).create(ApiInterface.class);
        Call call;

        call = apiService.getProfile(sessionvalue, sessionManager.getAggregatorprofile());

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                int statusCode = response.code();
                Log.e("test", " trdds");

                Headers headerList = response.headers();
                Log.d("test", statusCode + " ");

                //  String cookie = response.;
                //  Log.d("test", cookie+ " ");


                if (statusCode == 401) {
                    refreshSession();
                }

                if (response.isSuccessful()) {
                    String a = new Gson().toJson(response.body());
                    Log.e("response", a);
                    try {
                        JSONObject jObjRes = new JSONObject(a);
                        JSONObject jObjdata = jObjRes.getJSONObject("response");
                        JSONObject jObjResponse = jObjdata.getJSONObject("data");

                        sessionManager.setUserId(jObjResponse.getString("id"));


                        Log.d("test", jObjResponse.toString() + " ");
                        obj = new Gson().fromJson(jObjResponse.toString(), Userprofile.class);


                        String b_id_for_employee = jObjResponse.getString("bridge_id");
                        sessionManager.setUserBridgeId(b_id_for_employee);

                        businaesBId = b_id_for_employee;
                        businessUserId = jObjResponse.getString("username");
                        businaesName = jObjResponse.getString("firstname");
                        sessionManager.setFstNm(businaesName);

                        name.setText(obj.getFirstname());

                        String isactive = jObjResponse.getString("business_status");
                        sessionManager.setbus_status(isactive);
                        tv_openclose.setText(isactive);
                        shop_status = isactive;

                        if (obj.getEmail_id().equalsIgnoreCase("Temp@gmail.com") || obj.getFirstname().equalsIgnoreCase("Temp")) {
                            name.setText("");
                            email.setText("");
                            Intent i = new Intent(BaseActivity.this, ProfileActivity.class);
                            i.putExtra("session", sessionManager.getsession());
                            i.putExtra("isback", false);
                            startActivity(i);
                        } else if (sessionManager.getisBusinesslogic() && (obj.field_json_data == null || (obj.field_json_data != null && obj.field_json_data.equalsIgnoreCase("[]")))) {
                            name.setText("");
                            email.setText("");
                            Intent i = new Intent(BaseActivity.this, ProfileActivity.class);
                            i.putExtra("session", sessionManager.getsession());
                            i.putExtra("isback", false);
                            startActivity(i);
                        }
                        //getuserbuildid_and_indid();

                        email.setText(obj.getContact_no());
                        sessionManager.setE_bridgeid(b_id_for_employee);
                        sessionManager.setuserprofile(jObjResponse.toString());

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    setMenuList();

                } else {
                    Log.e("test", " trdds1");

                    try {

                     /*   JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Toast.makeText(BaseActivity.this, jObjErrorresponse.getString("error"), Toast.LENGTH_LONG).show();
                 */
                    } catch (Exception e) {
                        // Toast.makeText(LoginActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }


                }

                //  List<Movie> movies = response.body().getResults();
                //  recyclerView.setAdapter(new MoviesAdapter(movies, R.layout.list_item_movie, getApplicationContext()));
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // Log error here since request failed
                Log.e("error message", t.toString());
            }
        });
    }

    @Override
    public void onItemClick(final DrawerItem item) {
        Log.e("menuclicked", "dasda");
        final int rowindex = item.getTag();

        DialogLoading.dialogLoading(this);
        Inad.hasActiveInternetConnection(this, new Inad.Netcallback() {
            @Override
            public void netcall(boolean isnet) {

                if (isnet) {

                    ShareModel.getI().isFromAllTask = false;
                    clickitem(rowindex);

                } else {
                    new CustomDialogClass(BaseActivity.this, new CustomDialogClass.OnDialogClickListener() {
                        @Override
                        public void onDialogImageRunClick(int positon) {
                            if (positon == 0) {

                            } else if (positon == 1) {

                            }
                        }
                    }, "Alert\nInternet connection is required!").show();

                }
            }
        });

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);
            }
        }, 1000);

    }

    private static final int REQUEST_EXTERNAL_STORAGE = 11;
    private static String[] PERMISSIONS_STORAGE = {
            android.Manifest.permission.READ_EXTERNAL_STORAGE,
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    ImageView iv_app;


    boolean isCall = false;

    public void setMenuList() {

        if (isCall) return;
        if (!isCall) {
            isCall = true;
        }
        boolean isemployee = false;
        SettingDB db = new SettingDB(this);
        List<SettingModel> list_setting = db.GetItems();
        sm = list_setting.get(0);

        mDrawerItemList.clear();


        if (sessionManager.getcurrentu_nm().contains("@") || !sessionManager.getcurrentu_nm().matches("[0-9.]*")) // Employee or business login
        {
            DrawerItem item11 = new DrawerItem();
            item11.setIcon(R.drawable.ic_search);
            item11.setTag(11);
            item11.setTitle("Search");
            mDrawerItemList.add(item11);


            isemployee = true;
            if (!getString(R.string.app_name).equalsIgnoreCase(getString(R.string.business2))) {
                if (sm.getIsalltask().equals("1")) {
                    DrawerItem item6 = new DrawerItem();
                    item6.setIcon(R.mipmap.alltask);
                    item6.setTag(8);
                    item6.setTitle("All task");
                    mDrawerItemList.add(item6);
                }
            } else {
                if (sm.getIsalltask().equals("1")) {
                    DrawerItem item6 = new DrawerItem();
                    item6.setIcon(R.mipmap.alltask);
                    item6.setTag(8);
                    item6.setTitle("Enquiry");
                    mDrawerItemList.add(item6);
                }
            }


            if (!getString(R.string.app_name).equalsIgnoreCase(getString(R.string.business2))) {
                if (sm.getIsmytask().equals("1")) {
                    DrawerItem item5 = new DrawerItem();
                    item5.setIcon(R.mipmap.mytask);
                    item5.setTag(6);
                    item5.setTitle("My task");
                    mDrawerItemList.add(item5);
                }
            }

            if (!getString(R.string.app_name).equalsIgnoreCase(getString(R.string.business2))) {
                DrawerItem item12 = new DrawerItem();
                item12.setIcon(R.mipmap.canceled);
                item12.setTag(12);
                item12.setTitle("Cancelled task");
                mDrawerItemList.add(item12);
            }
        } else if (!sessionManager.getcurrentu_nm().contains("@") && sm.getIsreceipt().equals("1")) { //// Customer
            DrawerItem item5 = new DrawerItem();
            item5.setIcon(R.mipmap.mytask);
            item5.setTag(9);
            item5.setTitle("Receipts");
            //mDrawerItemList.add(item5);
        }
        if (!getString(R.string.app_name).equalsIgnoreCase(getString(R.string.business2))) {
            DrawerItem item15 = new DrawerItem();
            item15.setIcon(R.mipmap.suppllist);
            item15.setTag(15);
            if (sessionManager.getIsConsumer() && !isAppHyperlocalMode) {
                item15.setTitle("Shop");
            } else {
                item15.setTitle("My supplier");
            }
            mDrawerItemList.add(item15);
        }

        if (sessionManager.getIsConsumer()) {
            DrawerItem item67 = new DrawerItem();
            item67.setIcon(R.drawable.orderhistory);
            item67.setTitle("Favourite");
            item67.setTag(67);
            mDrawerItemList.add(item67);
        }

        if (!getString(R.string.app_name).equalsIgnoreCase(getString(R.string.business2))) {
            DrawerItem item1 = new DrawerItem();
            item1.setIcon(R.drawable.orderhistory);
            item1.setTitle("My order");
            item1.setTag(2);
            mDrawerItemList.add(item1);
        }


        if (sessionManager.getcurrentu_nm().contains("@") || !sessionManager.getcurrentu_nm().matches("[0-9.]*")) // Employee or business login
        {
            if (sessionManager.getisBusinesslogic()) {

                DrawerItem item20 = new DrawerItem();
                item20.setIcon(R.mipmap.catbook);
                item20.setTag(66);
                item20.setTitle("Product");
                mDrawerItemList.add(item20);

                DrawerItem item13 = new DrawerItem();
                item13.setTitle("Catalogue");
                item13.setTag(13);
                item13.proDtl = "hide";
                mDrawerItemList.add(item13);

                DrawerItem item52 = new DrawerItem();
                item52.setTitle("Category");
                item52.setTag(52);
                item52.proDtl = "hide";
                mDrawerItemList.add(item52);

                String service_type_of = JsonObjParse.getValueEmpty(obj.field_json_data, "service_type_of");
                if (service_type_of.equalsIgnoreCase(getString(R.string.dental))) {
                    DrawerItem item65 = new DrawerItem();
                    item65.setTitle("Template");
                    item65.setTag(65);
                    item65.proDtl = "hide";
                    mDrawerItemList.add(item65);
                }

                if (!getString(R.string.app_name).equalsIgnoreCase(getString(R.string.business2))) {
                    DrawerItem item54 = new DrawerItem();
                    item54.setTitle("Database");
                    item54.setTag(54);
                    item54.proDtl = "hide";
                    mDrawerItemList.add(item54);
                }

                DrawerItem item51 = new DrawerItem();
                item51.setTitle("Master");
                item51.setIcon(R.mipmap.catbook);
                item51.setTag(51);
                mDrawerItemList.add(item51);

                if (obj != null && obj.getBusiness_type().equalsIgnoreCase("aggregator")) {
                    DrawerItem item56 = new DrawerItem();
                    item56.setTitle("Add shop");
                    item56.setTag(56);
                    item56.mstrDtl = "hide";
                    mDrawerItemList.add(item56);

                }
                if (!getString(R.string.app_name).equalsIgnoreCase(getString(R.string.business2))) {
                    DrawerItem item53 = new DrawerItem();
                    item53.setTitle("Employee");
                    item53.setTag(53);
                    item53.mstrDtl = "hide";
                    mDrawerItemList.add(item53);
                }

                DrawerItem item55 = new DrawerItem();
                item55.setTitle("Settings");
                item55.setTag(55);
                item55.mstrDtl = "hide";
                mDrawerItemList.add(item55);

              /*  if (obj != null && obj.getBusiness_type().equalsIgnoreCase("business")) {
                    DrawerItem item57 = new DrawerItem();
                    item57.setTitle("Profile");
                    item57.setTag(57);
                    item57.mstrDtl = "hide";
                    mDrawerItemList.add(item57);
                }*/
                DrawerItem item57 = new DrawerItem();
                item57.setTitle("Profile");
                item57.setTag(57);
                item57.mstrDtl = "hide";
                mDrawerItemList.add(item57);

                if (!getString(R.string.app_name).equalsIgnoreCase(getString(R.string.business2))) {
                    if (!getString(R.string.app_name_condition).equalsIgnoreCase("Space lattice")) {
                        DrawerItem item61 = new DrawerItem();
                        item61.setTitle(getString(R.string.other_buiness));
                        item61.setIcon(R.drawable.ic_analysis);
                        item61.setTag(61);
                        mDrawerItemList.add(item61);
                    }
                }

                DrawerItem item59 = new DrawerItem();
                item59.setTitle("Traction");
                item59.setTag(59);
                item59.otrDtl = "hide";
                mDrawerItemList.add(item59);

                String walk_in_details = getValueFromFieldJson("walk_in_details");
                String is_walkin_in_menu = JsonObjParse.getValueEmpty(walk_in_details, "is_walkin_in_menu");
                if (is_walkin_in_menu.equalsIgnoreCase("no")) {

                } else {
                    DrawerItem item62 = new DrawerItem();
                    item62.setTitle("Walk-in");
                    item62.setTag(62);
                    item62.otrDtl = "hide";
                    mDrawerItemList.add(item62);
                }


                DrawerItem item63 = new DrawerItem();
                item63.setTitle("Promo");
                item63.setTag(63);
                item63.otrDtl = "hide";
                mDrawerItemList.add(item63);

                DrawerItem item64 = new DrawerItem();
                item64.setTitle("Who referred me");
                item64.setTag(64);
                item64.otrDtl = "hide";
                mDrawerItemList.add(item64);

            } else {
                String walk_in_details = getValueFromFieldJson("walk_in_details");
                String is_walkin_in_menu = JsonObjParse.getValueEmpty(walk_in_details, "is_walkin_in_menu");
                if (is_walkin_in_menu.equalsIgnoreCase("no")) {

                } else {
                    DrawerItem item62 = new DrawerItem();
                    item62.setTitle("Walk-in");
                    item62.setIcon(R.drawable.visitor);
                    item62.setTag(62);
                    //item62.otrDtl = "hide";
                    mDrawerItemList.add(item62);
                }
            }
        }

        if (sessionManager.getcurrentu_nm().matches("[0-9.]*")) // Consumer/User
        {

        } else {
            DrawerItem item17 = new DrawerItem();
            item17.setIcon(R.drawable.aggre);
            item17.setTitle("My aggregator");
            item17.setTag(17);
            //mDrawerItemList.add(item17);
        }


        DrawerItem item = new DrawerItem();
        item.setIcon(R.drawable.cheficon);
        item.setTitle("My circle");
        item.setTag(1);
        //mDrawerItemList.add(item);

        if (sessionManager.getcurrentu_nm().matches("[0-9.]*")) // Consumer/User
        {

            DrawerItem item58 = new DrawerItem();
            item58.setIcon(R.mipmap.launcher);
            item58.setTitle("My clicks");
            item58.setTag(58);
            //mDrawerItemList.add(item58);
        }

        DrawerItem item4 = new DrawerItem();
        item4.setIcon(R.mipmap.b_archived);
        item4.setTag(3);
        item4.setTitle("Archived");
        //mDrawerItemList.add(item4);

        if (sessionManager.getisBusinesslogic() || sessionManager.getcurrentu_nm().contains("@")) {   //  employee and business both

          /*  DrawerItem item18 = new DrawerItem();  // man icon
            item18.setIcon(R.mipmap.request);
            item18.setTitle("My Request");
            item18.setTag(18);
            mDrawerItemList.add(item18);

            DrawerItem item19 = new DrawerItem();
            item19.setIcon(R.mipmap.issue);
            item19.setTitle("My Issue");
            item19.setTag(19);
            mDrawerItemList.add(item19);
*/
        }

        if (!getString(R.string.app_name).equalsIgnoreCase(getString(R.string.business2))) {
            if (sessionManager.getisBusinesslogic() || sessionManager.getcurrentu_nm().contains("@")) {
                if (sm.getIsprintreceipt().equals("1")) {
                    DrawerItem item14 = new DrawerItem();
                    item14.setIcon(R.drawable.print);
                    item14.setTitle("Print");
                    item14.setTag(14);
                    mDrawerItemList.add(item14);
                }
            }
        }

        DrawerItem item16 = new DrawerItem();
        item16.setIcon(R.drawable.circle);
        item16.setTag(16);
        item16.setTitle("Select circle");
        //mDrawerItemList.add(item16);

        String lastname = getValueFromFieldJson("lastname");

        if (lastname.equalsIgnoreCase("1")) {

        } else {

            if (obj != null) {
                String is_g_plus_sign = JsonObjParse.getValueEmpty(obj.field_json_data, "is_g_plus_sign");
                if (is_g_plus_sign.equalsIgnoreCase("yes")) {

                } else {
                    DrawerItem item2 = new DrawerItem();
                    item2.setIcon(R.drawable.changepassword);
                    item2.setTag(4);
                    item2.setTitle("Change password");
                    mDrawerItemList.add(item2);
                }
            }
        }

        DrawerItem item6 = new DrawerItem();
        item6.setIcon(R.mipmap.setting);
        item6.setTag(7);
        item6.setTitle("Setting");
        //mDrawerItemList.add(item6);


        DrawerItem item3 = new DrawerItem();
        item3.setIcon(R.drawable.logout);
        item3.setTag(5);
        item3.setTitle("Logout");
        mDrawerItemList.add(item3);

        DrawerAdapter adapter = new DrawerAdapter(mDrawerItemList, this, isemployee);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setAdapter(adapter);
    }

    boolean isfromsignup = false;

    ArrayList<ModelDevice> al_user = new ArrayList<>();
    boolean isuseravailable = false;
    int userpos = 0;
    String user = "";

    public void getuserbuildid_and_indid() {

        user = sessionManager.getcurrentu_nm();
        final Gson gson = new Gson();

        String json = SharedPrefUserDetail.getString(this, SharedPrefUserDetail.loginuser, "");


        Type type = new TypeToken<ArrayList<ModelDevice>>() {
        }.getType();

        al_user = gson.fromJson(json, type);

        if (al_user == null) {
            al_user = new ArrayList<>();
        }


        for (int i = 0; i < al_user.size(); i++) {
            String alluser = al_user.get(i).getUsernm();
            if (user.equals(alluser)) {

                userpos = i;

                break;
            }
        }
        String building_id = al_user.get(userpos).getBuild_id();
        String industry_id = al_user.get(userpos).getInd_id();

        if (building_id.isEmpty() || industry_id.isEmpty()) {
            Intent i = new Intent(BaseActivity.this, ProfileActivity.class);
            i.putExtra("session", sessionManager.getsession());
            i.putExtra("isback", false);
            startActivity(i);
        }


     /*   sessionManager.setBuilding_id(building_id);
        sessionManager.setIndustry_id(industry_id);*/

    }

    public void clickitem(int rowindex) {
        ShareModel.getI().dbUnderMaster = "";

        ShareModel.getI().metal = new Metal();
        Constants.chit_id_from_mrp = 0;

        sessionManager.setAggregator_ID(getString(R.string.Aggregator));
        SharedPrefUserDetail.setBoolean(BaseActivity.this, SharedPrefUserDetail.isfromsupplier, false);
        SharedPrefUserDetail.setString(BaseActivity.this, SharedPrefUserDetail.isfrommyaggregator, "");
        SharedPrefUserDetail.setBoolean(this, SharedPrefUserDetail.isMrp, false); // pass flag

        if (rowindex == 2) {
            // Handle the camera action
            Intent i = new Intent(this, HistoryActivity.class);
            i.putExtra("session", sessionManager.getsession());
            //  i.putExtra("fromAT", true);
            i.putExtra("fromHome", true);
            i.putExtra("isBk", true);
            startActivity(i);
        } else if (rowindex == 1) {
            //Intent i = new Intent(this, MainActivity.class);
            //startActivity(i);
            Intent mainIntent = new Intent(this, MainActivity.class);
            mainIntent.putExtra("session", sessionManager.getsession());
            startActivity(mainIntent);
        } else if (rowindex == 3) {
            Intent i = new Intent(this, ArchivedListActivity.class);
            i.putExtra("session", sessionManager.getsession());
            startActivity(i);
        } else if (rowindex == 6) {
            Intent i = new Intent(this, MyTaskActivity.class);
            i.putExtra("session", sessionManager.getsession());
            startActivity(i);

        } else if (rowindex == 8) {
            finish();
            Intent i = new Intent(this, AllTaskActivity.class);
            i.putExtra("session", sessionManager.getsession());
            startActivity(i);

        } else if (rowindex == 9) {
            Intent i = new Intent(this, MyReciptsforCustomer.class);
            i.putExtra("session", sessionManager.getsession());
            startActivity(i);

        } else if (rowindex == 7) {
            Intent i = new Intent(this, SettingsActivity.class);
            i.putExtra("session", sessionManager.getsession());
            startActivity(i);

        } else if (rowindex == 4) {
            Intent i = new Intent(this, ChangepasswordActivity.class);
            i.putExtra("session", sessionManager.getsession());
            startActivity(i);
        } else if (rowindex == 11) {
            Intent i = new Intent(this, SearchActivity.class);
            i.putExtra("session", sessionManager.getsession());
            startActivity(i);
        } else if (rowindex == 12) {
            Intent i = new Intent(this, CancelTaskActivity.class);
            i.putExtra("session", sessionManager.getsession());
            startActivity(i);
        } else if (rowindex == 13) {
            Intent i = new Intent(this, CatalougeTabActivity.class);
            i.putExtra("session", sessionManager.getsession());
            startActivity(i);
        } else if (rowindex == 52) {
            Intent i = new Intent(this, CategoryUnderMsterActivity.class);
            startActivity(i);
        } else if (rowindex == 53) {
            Intent i = new Intent(this, EmployeeUnderMasterActivity.class);
            startActivity(i);
        } else if (rowindex == 54) {

            String db = "database";
            if (getString(R.string.Aggregator).equalsIgnoreCase(getString(R.string.c6_evident))) {
                db = "edentdb";
            } else {
                db = "database";
            }

            new GetServiceTypeFromBusiUserProfile(BaseActivity.this, db, new GetServiceTypeFromBusiUserProfile.Apicallback() {
                @Override
                public void onGetResponse(String a, String response, String sms_emp) {

                    //SupplierNew item = null;

                    ShareModel.getI().dbUnderMaster = "Ys";

                    String purpose = a;
                    if (a.isEmpty()) purpose = "request";

                    String getUsername = "", getFirstname = "", getBridgeId = "";
                    try {
                        JSONObject Jsonresponse = new JSONObject(response);
                        JSONObject Jsonresponseinfo = Jsonresponse.getJSONObject("response");
                        JSONArray jaData = Jsonresponseinfo.getJSONArray("data");
                        if (jaData.length() > 0) {
                            JSONObject data = jaData.getJSONObject(0);
                            getUsername = JsonObjParse.getValueNull(data.toString(), "username");
                            getBridgeId = JsonObjParse.getValueNull(data.toString(), "bridge_id");
                            getFirstname = JsonObjParse.getValueNull(data.toString(), "firstname");

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    sessionManager.setAggregator_ID(getString(R.string.Aggregator)); // From Base Click
                    SharedPrefUserDetail.setBoolean(BaseActivity.this, SharedPrefUserDetail.isfromsupplier, false);


                    String agree = getUsername;
                    sessionManager.setAggregator_ID(agree);

                    SharedPrefUserDetail.setString(BaseActivity.this, SharedPrefUserDetail.isfrommyaggregator, getBridgeId);

                    SharedPrefUserDetail.setString(BaseActivity.this, SharedPrefUserDetail.infoMainShop, getUsername);

                    Intent mainIntent = new Intent(BaseActivity.this, MainActivity.class);
                    mainIntent.putExtra("session", sessionManager.getsession());
                    mainIntent.putExtra("SupplierTitle", getFirstname);
                    mainIntent.putExtra("purpose", purpose);

                    startActivity(mainIntent);


                }
            });


        } else if (rowindex == 55) {
            ShareModel.getI().bus_setting_from = "master";
            Intent i = new Intent(this, EditCurrencyForBusinessActivity.class);
            startActivity(i);
        } else if (rowindex == 57) {
            //ShareModel.getI().bus_setting_from = "master";

            Intent i = new Intent(BaseActivity.this, ProfileActivity.class);
            i.putExtra("session", sessionManager.getsession());
            startActivity(i);

        } else if (rowindex == 59) {
            //ShareModel.getI().bus_setting_from = "master";
            Intent i = new Intent(BaseActivity.this, UserTractionActivity.class);
            i.putExtra("session", sessionManager.getsession());
            startActivity(i);
        } else if (rowindex == 62) {
            //ShareModel.getI().bus_setting_from = "master";
            Intent i = new Intent(BaseActivity.this, WalkInConsumerActivity.class);
            i.putExtra("session", sessionManager.getsession());
            startActivity(i);
        } else if (rowindex == 63) {
            //ShareModel.getI().bus_setting_from = "master";
            Intent i = new Intent(BaseActivity.this, PromoActivity.class);
            i.putExtra("session", sessionManager.getsession());
            startActivity(i);
        } else if (rowindex == 64) {

            //ShareModel.getI().bus_setting_from = "master";

            if (ShareModel.getI().whoReferMe.isEmpty()) {
                MyRequst myRequst = new MyRequst();

                myRequst.shop_id = sessionManager.getcurrentu_nm(); // only shop nm - business

                MyRequestCall myRequestCall = new MyRequestCall();

                myRequestCall.getWhoReferMeFromMenu(BaseActivity.this, myRequst, new MyRequestCall.CallRequest() {
                    @Override
                    public void onGetResponse(String response) {
                        showRefer();
                    }
                });

            } else {
                showRefer();
            }


        } else if (rowindex == 56) {
            ShareModel.getI().bus_setting_from = "master";
            Intent i = new Intent(this, AddShopsUnderMasterActivity.class);
            startActivity(i);
        } else if (rowindex == 14) {
            Intent i = new Intent(this, PrintOTPActivity.class);
            i.putExtra("session", sessionManager.getsession());
            startActivity(i);
        } else if (rowindex == 15) { // My Supplier
            SharedPrefUserDetail.setBoolean(BaseActivity.this, SharedPrefUserDetail.isfromsupplier, true);
            Intent i = new Intent(this, MySupplierActivity.class);
            i.putExtra("isFavourite", false);
            startActivity(i);
           /* if(obj!=null){
                if(obj.account_type!=null&&obj.business_type!=null&&obj.account_type.equalsIgnoreCase("Business"))
                {

                   *//* if(obj.business_type.equalsIgnoreCase("Business"))
                    {
                        SharedPrefUserDetail.setBoolean(BaseActivity.this, SharedPrefUserDetail.isfromsupplier, true);
                        Intent i = new Intent(this, MySupplierActivity.class);
                        startActivity(i);
                    }
                    else if(obj.business_type.equalsIgnoreCase( "Aggregator"))
                    {
                        clickitem(17);
                    }
                    else if(obj.business_type.equalsIgnoreCase( "Circle"))
                    {
                        clickitem(1);
                    }*//*
                    if(obj.business_type.equalsIgnoreCase( "Aggregator"))
                    {
                        clickitem(17);
                    }
                    else {
                        SharedPrefUserDetail.setBoolean(BaseActivity.this, SharedPrefUserDetail.isfromsupplier, true);
                        Intent i = new Intent(this, MySupplierActivity.class);
                        startActivity(i);
                    }

                }
                else {
                    SharedPrefUserDetail.setBoolean(BaseActivity.this, SharedPrefUserDetail.isfromsupplier, true);
                    Intent i = new Intent(this, MySupplierActivity.class);
                    startActivity(i);
                }


            }*/


           /* SharedPrefUserDetail.setBoolean(BaseActivity.this, SharedPrefUserDetail.isfromsupplier, true);
            Intent i = new Intent(this, MySupplierActivity.class);
            startActivity(i);*/
        } else if (rowindex == 16) {

            Intent i = new Intent(BaseActivity.this, ChageCircle.class);
            startActivity(i);
        } else if (rowindex == 17) {

            SharedPrefUserDetail.setString(BaseActivity.this, SharedPrefUserDetail.isfrommyaggregator, "noback");

            Intent mainIntent = new Intent(BaseActivity.this, MainActivity.class);
            mainIntent.putExtra("session", sessionManager.getsession());
            startActivity(mainIntent);


        } else if (rowindex == 18) {
            Intent i = new Intent(this, MyRequsetActivity.class);
            i.putExtra("session", sessionManager.getsession());
            i.putExtra("purpose", "request");
            startActivity(i);
        } else if (rowindex == 19) {
            Intent i = new Intent(this, MyRequsetActivity.class);
            i.putExtra("session", sessionManager.getsession());
            i.putExtra("purpose", "issue");
            startActivity(i);
        } else if (rowindex == 5) {
            logoutuser();
        } else if (rowindex == 65) {
            Intent i = new Intent(this, SelectTemplateActivity.class);
            i.putExtra("session", sessionManager.getsession());
            startActivity(i);
        } else if (rowindex == 67) {
            SharedPrefUserDetail.setBoolean(BaseActivity.this, SharedPrefUserDetail.isfromsupplier, true);
            Intent i = new Intent(this, MySupplierActivity.class);
            i.putExtra("isFavourite", true);
            startActivity(i);
        }
    }

    public void setagreeid() {
        ArrayList<ModelCustom> al_user = new ArrayList<>();

        final Gson gson = new Gson();


        boolean isuseravailable = false;

        String json = SharedPrefUserDetail.getString(BaseActivity.this, SharedPrefUserDetail.userwithagreeid, "");


        Type type = new TypeToken<ArrayList<ModelCustom>>() {
        }.getType();

        al_user = gson.fromJson(json, type);

        if (al_user == null) {
            al_user = new ArrayList<>();
        }

        String user = sessionManager.getcurrentu_nm();

        String agrreid = "";
        isuseravailable = false;
        for (int i = 0; i < al_user.size(); i++) {
            String alluser = al_user.get(i).getUsernm();
            if (user.equals(alluser)) {
                isuseravailable = true;
                agrreid = al_user.get(i).getAggreid();
                break;
            }

        }

        if (!isuseravailable) {

        } else {
            //sessionManager.setAggregator_ID(agrreid);


        }
    }

    public void refreshSession() {
        /*sessionManager.logoutUser(this);
        Toast.makeText(BaseActivity.this, "Session expired login again...", Toast.LENGTH_LONG).show();
        Intent i = new Intent(this, SplashActivity.class);  // All
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);*/
        //logInIfExpired();
        new RefreshSession(this, sessionManager);
    }

    public void logInIfExpired() {

        new RefreshSession(this, sessionManager);
        /*ApiInterface apiService =
                ApiClient.getClient2(BaseActivity.this).create(ApiInterface.class);
        String u_name = sessionManager.getUserWithAggregator();
        String pwd = sessionManager.getPasswd();

        Call call = apiService.getUserLogin(u_name, pwd);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                int statusCode = response.code();

                if (response.isSuccessful()) {

                    try {

                        Intent i = new Intent(BaseActivity.this, SplashActivity.class);  // All
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(i);

                        String cookie = response.headers().get("Set-Cookie");
                        String[] cookies = cookie.split(";");
                        sessionManager.create_Usersession(cookies[0]);

                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                } else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Toast.makeText(BaseActivity.this, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(BaseActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }

            }

            @Override
            public void onFailure(Call call, Throwable t) {

            }
        });*/
    }

    String shop_status = "";
    private ProgressDialog dialog;

    public void SetStatus() {

        dialog = new ProgressDialog(BaseActivity.this);
        if (shop_status.equalsIgnoreCase("open")) {
            shop_status = "2";  // for closed
            dialog.setMessage("You are about to close the Shop");
            //dialog.setMessage("Shop Closing..");

        } else {
            //dialog.setMessage("Shop Opening..");
            dialog.setMessage("You are about to open the Shop");
            shop_status = "1";  // for open
        }


        dialog.show();

        ApiInterface apiService =
                ApiClient.getClient(this).create(ApiInterface.class);

        JsonObject jo_st = new JsonObject();
        jo_st.addProperty("shop_status", shop_status);
        Call call;
        call = apiService.SetBusinessStatus(sessionManager.getsession(), jo_st);

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                dialog.dismiss();

                if (response.isSuccessful()) {
                    if (shop_status.equals("1")) {
                        shop_status = "Open";
                        sessionManager.setbus_status("Open");
                    } else {
                        shop_status = "Closed";
                        sessionManager.setbus_status("Closed");
                    }
                    tv_openclose.setText(sessionManager.getbusi_status());
                } else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Toast.makeText(BaseActivity.this, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {

                    }
                }


            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // Log error here since request failed
                dialog.dismiss();

            }
        });


    }

    public void getEmployeeeDetailFromProfile(String useName) {

        dialog = new ProgressDialog(BaseActivity.this);
        dialog.setMessage("Please wait...");
        dialog.show();
        if (!sessionManager.getisBusinesslogic()) {
            int indexOfannot = useName.indexOf("@") + 1;
            useName = useName.substring(indexOfannot, useName.indexOf("."));
        }

        ApiInterface apiService = ApiClient.getClient(this).create(ApiInterface.class);


        Call call;

        call = apiService.getagregatorProfile(useName);

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                dialog.dismiss();


                if (response.isSuccessful()) {

                    try {
                        String a = new Gson().toJson(response.body());
                        JSONObject Jsonresponse = new JSONObject(a);
                        JSONObject Jsonresponseinfo = Jsonresponse.getJSONObject("response");
                        JSONArray jaData = Jsonresponseinfo.getJSONArray("data");
                        JSONObject joData = jaData.getJSONObject(0);
                        String username = joData.getString("username");
                        String firstname = joData.getString("firstname");
                        field_json_data = JsonObjParse.getValueEmpty(joData.toString(), "field_json_data");

                        Log.d("okhttp", field_json_data + "");
                        if (field_json_data != null) {
                            String shareId = JsonObjParse.getValueEmpty(field_json_data, "shareId");
                            if (!shareId.isEmpty()) username = shareId;
                        }

                        String shareUrl = "http://play.google.com/store/apps/details?id=" + getString(R.string.consumer_link) + "&referrer=" + username;
                        //String link = getString(R.string.BASEURL) + "request-form/" + username + "/" + bridge_id + "/INR";
                        //String extTxt = shareUrl + "\n\nAdd '" + username + "' in My Supplier option to connect to '" + firstname + "'" + "\n\n";

                        Uri shareUri = buildDeepLink(Uri.parse(shareUrl), 0);
                        shareUrl = shareUri.toString();

                        Intent share = new Intent(android.content.Intent.ACTION_SEND);
                        share.setType("text/plain");
                        share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                        // Add data to the intent, the receiving app will decide
                        // what to do with it.
                        share.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.consumer));
                        share.putExtra(Intent.EXTRA_TEXT, shareUrl);
                        startActivity(Intent.createChooser(share, "Share"));


                    } catch (Exception e) {


                    }
                } else {

                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                dialog.dismiss();

            }
        });
    }

    public void GetAddressBase(String search) {


        ApiInterface apiService =
                ApiClient.getClient(this).create(ApiInterface.class);


        Call call;
        call = apiService.getaddress(sessionManager.getsession(), search);

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {

                int statusCode = response.code();
                //  String cookie = response.;
                //  Log.d("test", cookie+ " ");

                if (response.isSuccessful()) {


                    try {
                        String a = new Gson().toJson(response.body());
                        JSONObject Jsonresponse = new JSONObject(a);
                        JSONObject jo_response = Jsonresponse.getJSONObject("response");

                        String data = jo_response.getString("data");
                        sessionManager.setIndustrySupplier(data);

                      /*  JSONObject jo_data = new JSONObject(data);
                        JSONArray ja_buildingArr = jo_data.getJSONArray("buildingArr");


                        al_building = new ArrayList();
                        //al_building.add(new industrySuppliermodel("Select Circle", ""));
                        for (int i = 0; i < ja_buildingArr.length(); i++) {
                            JSONObject jo_res = ja_buildingArr.getJSONObject(i);
                            String building_name = jo_res.getString("building_name");
                            String building_id = jo_res.getString("building_id");
                            al_building.add(new ChageCircle.industrySuppliermodel(building_name, building_id));
                        }



                        setbuild();

                        if(isfilter)
                        {
                            for (int i = 0; i < al_building.size(); i++) {
                                String t_id = al_building.get(i).getId();
                                if (t_id.trim().equals(building_id.trim())) {
                                    //spinner_building.setSelection(i);
                                    al_building.get(i).setIschecked(true);
                                    break;
                                }
                            }
                        }
                        else {
                            setuserbuildid_and_indid(false);
                        }


                        mAdapter.setitem(al_building);*/


                    } catch (Exception e) {
                        Log.e("test", " trdds" + e.getLocalizedMessage());

                    }
                } else {

                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Toast.makeText(BaseActivity.this, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        // Toast.makeText(CatelogActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }

                //  List<Movie> movies = response.body().getResults();
                //  recyclerView.setAdapter(new MoviesAdapter(movies, R.layout.list_item_movie, getApplicationContext()));
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // Log error here since request failed

            }
        });
    }

    public String getST() {
        String service_type_of = "";
        if (sessionManager == null) sessionManager = new SessionManager(this);
        String userprofile = sessionManager.getUserprofile();
        obj = new Gson().fromJson(userprofile, Userprofile.class);
        if (obj != null)
            service_type_of = JsonObjParse.getValueEmpty(obj.field_json_data, "service_type_of");
        return service_type_of;
    }

    public String getValueFromFieldJson(String key) {
        String service_type_of = "";
        if (sessionManager == null) sessionManager = new SessionManager(this);
        String userprofile = sessionManager.getUserprofile();
        obj = new Gson().fromJson(userprofile, Userprofile.class);
        if (obj != null)
            service_type_of = JsonObjParse.getValueEmpty(obj.field_json_data, key);
        return service_type_of;
    }


    CustomDialogClass customDialogClass;

    @Override
    protected void onResume() {
        super.onResume();


        // register connection status listener
        App.getInstance().setConnectivityListener(new ConnectivityReceiver.ConnectivityReceiverListener() {
            @Override
            public void onNetworkConnectionChanged(boolean isConnected) {

                if (isConnected) {

                    customDialogClass = new CustomDialogClass(BaseActivity.this, new CustomDialogClass.OnDialogClickListener() {
                        @Override
                        public void onDialogImageRunClick(int positon) {
                            if (positon == 0) {

                            } else if (positon == 1) {

                            }
                        }
                    }, "Alert\nInternet connection is required!");
                    customDialogClass.show();

                } else {
                    if (customDialogClass != null) customDialogClass.dismiss();

                }
            }
        });

        try {
            String company_image = sessionManager.getCompImg();

            String endurl = "assets/uploadimages/";

            String url = getString(R.string.BASEURL) + endurl + company_image;
            Log.d("imageUrl", url + "");

            if (company_image.equalsIgnoreCase("")) {
                //Picasso.with(this).load(R.mipmap.launcher).into(iv_app);

                Glide.with(this).load(R.mipmap.launcher).asBitmap().centerCrop().into(new BitmapImageViewTarget(iv_app) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        RoundedBitmapDrawable circularBitmapDrawable =
                                RoundedBitmapDrawableFactory.create(getResources(), resource);
                        circularBitmapDrawable.setCircular(true);
                        iv_app.setImageDrawable(circularBitmapDrawable);
                    }
                });


            } else {

                Glide.with(this).load(url).asBitmap().centerCrop().into(new BitmapImageViewTarget(iv_app) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        RoundedBitmapDrawable circularBitmapDrawable =
                                RoundedBitmapDrawableFactory.create(getResources(), resource);
                        circularBitmapDrawable.setCircular(true);
                        iv_app.setImageDrawable(circularBitmapDrawable);
                    }
                });

                //Picasso.with(this).load(url).into(iv_app);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        getuserprofile(sessionManager.getsession());
        MyRequestCall myRequestCall = new MyRequestCall();
        myRequestCall.checkTraction(false, 0, this, new MyRequestCall.CallRequest() {
            @Override
            public void onGetResponse(String response) {

            }
        });

        //    throw new RuntimeException("This is a crash");

    }

    public void showRefer() {
        Inad.showAnimDialogue(this, "Who referred me", ShareModel.getI().whoReferMe);
    }
}