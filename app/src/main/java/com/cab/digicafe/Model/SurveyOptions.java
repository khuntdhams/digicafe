package com.cab.digicafe.Model;

import java.io.Serializable;
import java.util.ArrayList;

public class SurveyOptions implements Serializable {

    public String option = "", emoji = "", que = "", userComment = "";
    public int value = 1;
    public boolean isEdit = false;
    public int catPos = 0;

    public SurveyOptions(String opt, String emo, int val) {
        this.value = val;
        this.option = opt;
        this.emoji = emo;
    }

    public ArrayList<SurveyOptions> alOptions = new ArrayList<>();

    public SurveyOptions() {

    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
