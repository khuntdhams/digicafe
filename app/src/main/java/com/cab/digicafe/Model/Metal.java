package com.cab.digicafe.Model;

import java.io.Serializable;
import java.util.ArrayList;

public class Metal implements Serializable {

    public String metalNm = "", metalPrice = "", unit = "";
    public int serviceId = 0;
    public boolean isSelect = false;

    public Metal(String metalNm, String metalPrice, boolean issel) {
        this.isSelect = issel;

        this.metalNm = metalNm;
        this.metalPrice = metalPrice;
    }

    // Add
    public Metal(String metalNm, String metalPrice, boolean issel, String unit, int id) {
        this.isSelect = issel;
        this.unit = unit;
        this.metalNm = metalNm;
        this.metalPrice = metalPrice;
        this.id = id;

    }

    public Metal() {

    }

    public ArrayList<Metal> alMetal = new ArrayList<>();
    public int id = 0; // must be same or primary key or incremental
}
