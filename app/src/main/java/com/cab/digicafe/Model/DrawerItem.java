package com.cab.digicafe.Model;

/**
 * Created by CSS on 01-12-2017.
 */

public class DrawerItem {
    private int icon;
    private int tag;
    private String title;

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public int getTag() {
        return tag;
    }

    public void setTag(int tag) {
        this.tag = tag;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String mstrDtl = "";
    public String otrDtl = "";
    public String proDtl = "";
}
