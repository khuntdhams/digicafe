package com.cab.digicafe.Model;

import java.util.ArrayList;

/**
 * Created by CSS on 25-11-2017.
 */

public class ChitDetails {

    public Chitcurrency getCurrencyHeaderArr() {
        return currencyHeaderArr;
    }

    public void setCurrencyHeaderArr(Chitcurrency currencyHeaderArr) {
        this.currencyHeaderArr = currencyHeaderArr;
    }

    public ArrayList<Chitlog> getHeaderLog() {
        return headerLog;
    }

    public void setHeaderLog(ArrayList<Chitlog> headerLog) {
        this.headerLog = headerLog;
    }

    public ArrayList<Chitproducts> getContent() {
        return content;
    }

    public void setContent(ArrayList<Chitproducts> content) {
        this.content = content;
    }

    ArrayList<Chitproducts> content;
    Chitcurrency currencyHeaderArr;
    ArrayList<Chitlog> headerLog;
}