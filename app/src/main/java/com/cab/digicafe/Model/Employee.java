package com.cab.digicafe.Model;

/**
 * Created by pc51 on 5/29/2018.
 */

public class Employee {

    String username;
    String employee_name;
    String employee_bridge_id = "";

    public String getField_json_data() {
        return field_json_data;
    }

    public void setField_json_data(String field_json_data) {
        this.field_json_data = field_json_data;
    }

    String field_json_data = "";

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmployee_name() {
        return employee_name;
    }

    public void setEmployee_name(String employee_name) {
        this.employee_name = employee_name;
    }

    public String getEmployee_bridge_id() {
        return employee_bridge_id;
    }

    public void setEmployee_bridge_id(String employee_bridge_id) {
        this.employee_bridge_id = employee_bridge_id;
    }

    boolean checked;

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    String pricelist_category_name;

    public String getPricelist_category_name() {
        return pricelist_category_name;
    }

    public void setPricelist_category_name(String pricelist_category_name) {
        this.pricelist_category_name = pricelist_category_name;
    }

    public Employee(String employee_name) {
        this.employee_name = employee_name;
    }
}
