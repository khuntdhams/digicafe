package com.cab.digicafe.Model;

import com.google.api.services.vision.v1.model.BatchAnnotateImagesResponse;

import java.util.ArrayList;

/**
 * Created by pc51 on 5/15/2018.
 */

public class ModelFile {
    String imgpath;
    String imgInfo = "";
    boolean isoffline;
    String[] description;

    public ModelFile(String imgpath, boolean isoffline) {
        this.imgpath = imgpath;
        this.isoffline = isoffline;
    }

    public ModelFile(String imgpath, boolean isoffline, int pos) {
        this.imgpath = imgpath;
        this.isoffline = isoffline;
        this.pos = pos;
    }

    public String getImgInfo() {
        return imgInfo;
    }

    public void setImgInfo(String imgInfo) {
        this.imgInfo = imgInfo;
    }

    public ModelFile(String imgpath) {
        this.imgpath = imgpath;
    }

    public String getImgpath() {
        return imgpath;
    }

    public void setImgpath(String imgpath) {
        this.imgpath = imgpath;
    }

    public boolean isIsoffline() {
        return isoffline;
    }

    public void setIsoffline(boolean isoffline) {
        this.isoffline = isoffline;
    }

    public String[] getDescription() {
        return description;
    }

    public void setDescription(String[] description) {
        this.description = description;
    }

    boolean isdetect;

    public boolean isIsdetect() {
        return isdetect;
    }

    public void setIsdetect(boolean isdetect) {
        this.isdetect = isdetect;
    }

    ArrayList<ModelFile> al_detect = new ArrayList<>();

    public ArrayList<ModelFile> getAl_detect() {
        return al_detect;
    }

    public void setAl_detect(ArrayList<ModelFile> al_detect) {
        this.al_detect = al_detect;
    }

    BatchAnnotateImagesResponse batchAnnotateImagesResponse;

    public BatchAnnotateImagesResponse getBatchAnnotateImagesResponse() {
        return batchAnnotateImagesResponse;
    }

    public void setBatchAnnotateImagesResponse(BatchAnnotateImagesResponse batchAnnotateImagesResponse) {
        this.batchAnnotateImagesResponse = batchAnnotateImagesResponse;
    }

    boolean istitle;

    public boolean isIstitle() {
        return istitle;
    }

    public void setIstitle(boolean istitle) {
        this.istitle = istitle;
    }

    public ModelFile() {
    }

    String detctstring;

    public String getDetctstring() {
        return detctstring;
    }

    public void setDetctstring(String detctstring) {
        this.detctstring = detctstring;
    }

    public ModelFile(boolean istitle, String detctstring, int pos) {
        this.istitle = istitle;
        this.detctstring = detctstring;
        this.pos = pos;
    }

    int pos;

    public int getPos() {
        return pos;
    }

    public void setPos(int pos) {
        this.pos = pos;
    }

    boolean ischecked;

    public boolean isIschecked() {
        return ischecked;
    }

    public void setIschecked(boolean ischecked) {
        this.ischecked = ischecked;
    }
}
