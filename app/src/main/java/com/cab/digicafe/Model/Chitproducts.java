package com.cab.digicafe.Model;

/**
 * Created by CSS on 25-11-2017.
 */

public class Chitproducts {
    String chit_id;
    String current_status;
    String entry_id;
    String particulars;
    String particulars_code;
    String previous_status;
    String price;
    String quantity;
    String reply_particulars;
    String reply_price;
    String reply_quantity;
    String reply_total;
    String total;

    public String getOffer() {
        return offer;
    }

    public void setOffer(String offer) {
        this.offer = offer;
    }

    String offer;

    public String getChit_id() {
        return chit_id;
    }

    public void setChit_id(String chit_id) {
        this.chit_id = chit_id;
    }

    public String getCurrent_status() {
        return current_status;
    }

    public void setCurrent_status(String current_status) {
        this.current_status = current_status;
    }

    public String getEntry_id() {
        return entry_id;
    }

    public void setEntry_id(String entry_id) {
        this.entry_id = entry_id;
    }

    public String getParticulars() {
        return particulars;
    }

    public void setParticulars(String particulars) {
        this.particulars = particulars;
    }

    public String getParticulars_code() {
        return particulars_code;
    }

    public void setParticulars_code(String particulars_code) {
        this.particulars_code = particulars_code;
    }

    public String getPrevious_status() {
        return previous_status;
    }

    public void setPrevious_status(String previous_status) {
        this.previous_status = previous_status;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getReply_particulars() {
        return reply_particulars;
    }

    public void setReply_particulars(String reply_particulars) {
        this.reply_particulars = reply_particulars;
    }

    public String getReply_price() {
        return reply_price;
    }

    public void setReply_price(String reply_price) {
        this.reply_price = reply_price;
    }

    public String getReply_quantity() {
        return reply_quantity;
    }

    public void setReply_quantity(String reply_quantity) {
        this.reply_quantity = reply_quantity;
    }

    public String getReply_total() {
        return reply_total;
    }

    public void setReply_total(String reply_total) {
        this.reply_total = reply_total;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }
}
