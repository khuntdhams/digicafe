package com.cab.digicafe.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pc51 on 6/21/2018.
 */

public class SupplierNew {

    public String getUser_bridge_contacts_id() {
        return user_bridge_contacts_id;
    }

    public void setUser_bridge_contacts_id(String user_bridge_contacts_id) {
        this.user_bridge_contacts_id = user_bridge_contacts_id;
    }

    @SerializedName("user_bridge_contacts_id")
    @Expose

    private String user_bridge_contacts_id;
    @SerializedName("contact_bridge_id")
    @Expose
    private String contact_bridge_id;
    @SerializedName("username")
    @Expose
    private String username;

    public String getContact_bridge_id() {
        return contact_bridge_id;
    }

    public void setContact_bridge_id(String contact_bridge_id) {
        this.contact_bridge_id = contact_bridge_id;
    }

    @SerializedName("entry_id")
    @Expose
    private String entryId;
    @SerializedName("bridge_id")
    @Expose
    private String bridgeId;
    @SerializedName("account_type")
    @Expose
    private String accountType;
    @SerializedName("firstname")
    @Expose
    private String firstname;
    @SerializedName("lastname")
    @Expose
    private String lastname;
    @SerializedName("contact_no")
    @Expose
    private String contactNo;
    @SerializedName("email_id")
    @Expose
    private String emailId;
    @SerializedName("company_name")
    @Expose
    private String companyName;

    @SerializedName("company_detail_short")
    @Expose
    private String company_detail_short;

    @SerializedName("company_detail_long")
    @Expose
    public String companyDetailLong = "";
    @SerializedName("company_image")
    @Expose
    private String companyImage;
    @SerializedName("profileimage")
    @Expose
    private String profileimage;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("geohash")
    @Expose
    private String geohash;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("currency_code")
    @Expose
    private String currencyCode;
    @SerializedName("currency_code_id")
    @Expose
    private String currencyCodeId;
    @SerializedName("time_zone_id")
    @Expose
    private String timeZoneId;
    @SerializedName("time_zone_offset")
    @Expose
    private String timeZoneOffset;
    @SerializedName("setting_row_per_page")
    @Expose
    private String settingRowPerPage;
    @SerializedName("app_android_version_number")
    @Expose
    private Object appAndroidVersionNumber;
    @SerializedName("app_android_priority")
    @Expose
    private String appAndroidPriority;
    @SerializedName("app_android_push_key")
    @Expose
    private Object appAndroidPushKey;
    @SerializedName("app_android_url")
    @Expose
    private Object appAndroidUrl;
    @SerializedName("lic1_type_name")
    @Expose
    private String lic1TypeName;
    @SerializedName("lic1_no")
    @Expose
    private String lic1No;
    @SerializedName("lic1_name")
    @Expose
    private String lic1Name;
    @SerializedName("lic1_exp_date")
    @Expose
    private String lic1ExpDate;
    @SerializedName("lic2_type_name")
    @Expose
    private String lic2TypeName;
    @SerializedName("lic2_no")
    @Expose
    private String lic2No;
    @SerializedName("lic2_name")
    @Expose
    private String lic2Name;
    @SerializedName("lic2_exp_date")
    @Expose
    private String lic2ExpDate;
    @SerializedName("lic3_type_name")
    @Expose
    private String lic3TypeName;
    @SerializedName("lic3_no")
    @Expose
    private String lic3No;
    @SerializedName("lic3_name")
    @Expose
    private String lic3Name;
    @SerializedName("lic3_exp_date")
    @Expose
    private String lic3ExpDate;
    @SerializedName("external_connection")
    @Expose
    private String externalConnection;
    @SerializedName("newsletter")
    @Expose
    private String newsletter;
    @SerializedName("terms_and_condition")
    @Expose
    private String termsAndCondition;
    @SerializedName("business_status")
    @Expose
    private String businessStatus;
    @SerializedName("sms_status")
    @Expose
    private String smsStatus;

    public String getBusiness_type() {
        return business_type;
    }

    public void setBusiness_type(String business_type) {
        this.business_type = business_type;
    }

    @SerializedName("business_type")
    @Expose
    private String business_type;


    @SerializedName("symbol_native")
    @Expose
    private String symbol_native;


    @SerializedName("symbol")
    @Expose
    private String symbol;

    @SerializedName("field_json_data")
    @Expose
    private String field_json_data;

    public String getField_json_data() {
        if (field_json_data == null) {
            return "";
        }
        return field_json_data;
    }

    public void setField_json_data(String field_json_data) {
        this.field_json_data = field_json_data;
    }

    public String getSymbol_native() {
        return symbol_native;
    }

    public void setSymbol_native(String symbol_native) {
        this.symbol_native = symbol_native;
    }


    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    private final static long serialVersionUID = -227402073037728690L;

    public String getEntryId() {
        return entryId;
    }

    public void setEntryId(String entryId) {
        this.entryId = entryId;
    }

    public String getBridgeId() {
        return bridgeId;
    }

    public void setBridgeId(String bridgeId) {
        this.bridgeId = bridgeId;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompany_detail_short() {
        return company_detail_short;
    }

    public void setCompany_detail_short(String company_detail_short) {
        this.company_detail_short = company_detail_short;
    }

    public String getCompanyDetailLong() {
        if (companyDetailLong == null) return "";
        return companyDetailLong;
    }

    public void setCompanyDetailLong(String companyDetailLong) {
        this.companyDetailLong = companyDetailLong;
    }

    public String getCompanyImage() {
        return companyImage;
    }

    public void setCompanyImage(String companyImage) {
        this.companyImage = companyImage;
    }

    public String getProfileimage() {
        return profileimage;
    }

    public void setProfileimage(String profileimage) {
        this.profileimage = profileimage;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getGeohash() {
        return geohash;
    }

    public void setGeohash(String geohash) {
        this.geohash = geohash;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getCurrencyCodeId() {
        return currencyCodeId;
    }

    public void setCurrencyCodeId(String currencyCodeId) {
        this.currencyCodeId = currencyCodeId;
    }

    public String getTimeZoneId() {
        return timeZoneId;
    }

    public void setTimeZoneId(String timeZoneId) {
        this.timeZoneId = timeZoneId;
    }

    public String getTimeZoneOffset() {
        return timeZoneOffset;
    }

    public void setTimeZoneOffset(String timeZoneOffset) {
        this.timeZoneOffset = timeZoneOffset;
    }

    public String getSettingRowPerPage() {
        return settingRowPerPage;
    }

    public void setSettingRowPerPage(String settingRowPerPage) {
        this.settingRowPerPage = settingRowPerPage;
    }

    public Object getAppAndroidVersionNumber() {
        return appAndroidVersionNumber;
    }

    public void setAppAndroidVersionNumber(Object appAndroidVersionNumber) {
        this.appAndroidVersionNumber = appAndroidVersionNumber;
    }

    public String getAppAndroidPriority() {
        return appAndroidPriority;
    }

    public void setAppAndroidPriority(String appAndroidPriority) {
        this.appAndroidPriority = appAndroidPriority;
    }

    public Object getAppAndroidPushKey() {
        return appAndroidPushKey;
    }

    public void setAppAndroidPushKey(Object appAndroidPushKey) {
        this.appAndroidPushKey = appAndroidPushKey;
    }

    public Object getAppAndroidUrl() {
        return appAndroidUrl;
    }

    public void setAppAndroidUrl(Object appAndroidUrl) {
        this.appAndroidUrl = appAndroidUrl;
    }

    public String getLic1TypeName() {
        return lic1TypeName;
    }

    public void setLic1TypeName(String lic1TypeName) {
        this.lic1TypeName = lic1TypeName;
    }

    public String getLic1No() {
        return lic1No;
    }

    public void setLic1No(String lic1No) {
        this.lic1No = lic1No;
    }

    public String getLic1Name() {
        return lic1Name;
    }

    public void setLic1Name(String lic1Name) {
        this.lic1Name = lic1Name;
    }

    public String getLic1ExpDate() {
        return lic1ExpDate;
    }

    public void setLic1ExpDate(String lic1ExpDate) {
        this.lic1ExpDate = lic1ExpDate;
    }

    public String getLic2TypeName() {
        return lic2TypeName;
    }

    public void setLic2TypeName(String lic2TypeName) {
        this.lic2TypeName = lic2TypeName;
    }

    public String getLic2No() {
        return lic2No;
    }

    public void setLic2No(String lic2No) {
        this.lic2No = lic2No;
    }

    public String getLic2Name() {
        return lic2Name;
    }

    public void setLic2Name(String lic2Name) {
        this.lic2Name = lic2Name;
    }

    public String getLic2ExpDate() {
        return lic2ExpDate;
    }

    public void setLic2ExpDate(String lic2ExpDate) {
        this.lic2ExpDate = lic2ExpDate;
    }

    public String getLic3TypeName() {
        return lic3TypeName;
    }

    public void setLic3TypeName(String lic3TypeName) {
        this.lic3TypeName = lic3TypeName;
    }

    public String getLic3No() {
        return lic3No;
    }

    public void setLic3No(String lic3No) {
        this.lic3No = lic3No;
    }

    public String getLic3Name() {
        return lic3Name;
    }

    public void setLic3Name(String lic3Name) {
        this.lic3Name = lic3Name;
    }

    public String getLic3ExpDate() {
        return lic3ExpDate;
    }

    public void setLic3ExpDate(String lic3ExpDate) {
        this.lic3ExpDate = lic3ExpDate;
    }

    public String getExternalConnection() {
        return externalConnection;
    }

    public void setExternalConnection(String externalConnection) {
        this.externalConnection = externalConnection;
    }

    public String getNewsletter() {
        return newsletter;
    }

    public void setNewsletter(String newsletter) {
        this.newsletter = newsletter;
    }

    public String getTermsAndCondition() {
        return termsAndCondition;
    }

    public void setTermsAndCondition(String termsAndCondition) {
        this.termsAndCondition = termsAndCondition;
    }

    public String getBusinessStatus() {
        return businessStatus;
    }

    public void setBusinessStatus(String businessStatus) {
        this.businessStatus = businessStatus;
    }

    public String getSmsStatus() {
        return smsStatus;
    }

    public void setSmsStatus(String smsStatus) {
        this.smsStatus = smsStatus;
    }


    boolean islongpress = false;

    public boolean isIslongpress() {
        return islongpress;
    }

    public void setIslongpress(boolean islongpress) {
        this.islongpress = islongpress;
    }


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    String myReferal = "";

    public String getMyReferal() {
        return myReferal;
    }

    public void setMyReferal(String myReferal) {
        this.myReferal = myReferal;
    }
}
