package com.cab.digicafe.Model;

/**
 * Created by pc51 on 4/5/2018.
 */

public class ModelCustom {

    String usernm;
    String aggreid;

    public String getUsernm() {
        return usernm;
    }

    public void setUsernm(String usernm) {
        this.usernm = usernm;
    }

    public String getAggreid() {
        return aggreid;
    }

    public void setAggreid(String aggreid) {
        this.aggreid = aggreid;
    }


    public ModelCustom(String usernm, String aggreid) {
        this.usernm = usernm;
        this.aggreid = aggreid;
    }


}
