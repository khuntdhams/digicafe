package com.cab.digicafe.Model;

import ir.mirrajabi.searchdialog.core.Searchable;

/**
 * Created by crayond on 15/11/2017.
 */

public class SampleModel implements Searchable {
    private String mTitle;

    public SampleModel(String title) {
        mTitle = title;
    }

    @Override
    public String getTitle() {
        return mTitle;
    }

    public SampleModel setTitle(String title) {
        mTitle = title;
        return this;
    }
}