package com.cab.digicafe.Model;

/**
 * Created by CSS on 25-11-2017.
 */

public class Chitlog {
    String action;
    String action_by;
    String action_date;
    String chit_created_bridge_id;
    String chit_hash_id;
    String chit_id;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getAction_by() {
        return action_by;
    }

    public void setAction_by(String action_by) {
        this.action_by = action_by;
    }

    public String getAction_date() {
        return action_date;
    }

    public void setAction_date(String action_date) {
        this.action_date = action_date;
    }

    public String getChit_created_bridge_id() {
        return chit_created_bridge_id;
    }

    public void setChit_created_bridge_id(String chit_created_bridge_id) {
        this.chit_created_bridge_id = chit_created_bridge_id;
    }

    public String getChit_hash_id() {
        return chit_hash_id;
    }

    public void setChit_hash_id(String chit_hash_id) {
        this.chit_hash_id = chit_hash_id;
    }

    public String getChit_id() {
        return chit_id;
    }

    public void setChit_id(String chit_id) {
        this.chit_id = chit_id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getFriendlyname() {
        return friendlyname;
    }

    public void setFriendlyname(String friendlyname) {
        this.friendlyname = friendlyname;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getOriginator_chit_hash_id() {
        return originator_chit_hash_id;
    }

    public void setOriginator_chit_hash_id(String originator_chit_hash_id) {
        this.originator_chit_hash_id = originator_chit_hash_id;
    }

    public String getParent_chit_hash_id() {
        return parent_chit_hash_id;
    }

    public void setParent_chit_hash_id(String parent_chit_hash_id) {
        this.parent_chit_hash_id = parent_chit_hash_id;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    String description;
    String firstname;
    String friendlyname;
    String id;
    String lastname;
    String originator_chit_hash_id;
    String parent_chit_hash_id;
    String remark;

}