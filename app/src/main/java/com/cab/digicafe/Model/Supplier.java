package com.cab.digicafe.Model;

import android.text.TextUtils;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by crayond on 07/11/2017.
 */

public class Supplier {
    int network_contact_id;
    String from_bridge_id, assign_name = "";
    String to_bridge_id;
    String assign_to_bridge_id;
    public String alias_name;

    public String getAssign_name() {
        return assign_name;
    }

    public void setAssign_name(String assign_name) {
        this.assign_name = assign_name;
    }

    String info = "";
    private String field_json_data = "";

    boolean isPassMrpSelect;

    public boolean isPassMrpSelect() {
        return isPassMrpSelect;
    }

    public void setPassMrpSelect(boolean passMrpSelect) {
        isPassMrpSelect = passMrpSelect;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public void setLatitude(String latitude) {

        this.latitude = latitude;
    }


    public String getField_json_data() {
        if (field_json_data == null) {
            return "";
        }
        return field_json_data;
    }

    public void setField_json_data(String field_json_data) {
        this.field_json_data = field_json_data;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getBusiness_status() {
        return business_status;
    }

    public void setBusiness_status(String business_status) {
        this.business_status = business_status;
    }

    String business_status;

    public String getProfileimage() {
        return profileimage;
    }

    public void setProfileimage(String profileimage) {
        this.profileimage = profileimage;
    }

    public String getCompany_image() {
        return company_image;
    }

    public void setCompany_image(String company_image) {
        this.company_image = company_image;
    }

    String profileimage;
    String company_image;

    public String getFlag_status() {
        return flag_status;
    }

    public void setFlag_status(String flag_status) {
        this.flag_status = flag_status;
    }

    public int getNetwork_contact_id() {
        return network_contact_id;
    }

    public void setNetwork_contact_id(int network_contact_id) {
        this.network_contact_id = network_contact_id;
    }

    public String getFrom_bridge_id() {
        return from_bridge_id;
    }

    public void setFrom_bridge_id(String from_bridge_id) {
        this.from_bridge_id = from_bridge_id;
    }

    public String getTo_bridge_id() {
        return to_bridge_id;
    }

    public void setTo_bridge_id(String to_bridge_id) {
        this.to_bridge_id = to_bridge_id;
    }

    public String getAssign_to_bridge_id() {
        return assign_to_bridge_id;
    }

    public void setAssign_to_bridge_id(String assign_to_bridge_id) {
        this.assign_to_bridge_id = assign_to_bridge_id;
    }

    public String getAlias_name() {

        if (TextUtils.isEmpty(alias_name)) {
            return firstname;
        } else {
            return alias_name;
        }


    }

    public void setAlias_name(String alias_name) {
        this.alias_name = alias_name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreated_date() {
        return created_date;
    }

    public void setCreated_date(String created_date) {
        this.created_date = created_date;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getContact_no() {
        return contact_no;
    }

    public void setContact_no(String contact_no) {
        this.contact_no = contact_no;
    }

    public String getEmail_id() {
        return email_id;
    }

    public void setEmail_id(String email_id) {
        this.email_id = email_id;
    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    String flag_status;
    String status;
    String created_date;
    String username;
    String firstname;
    String lastname;
    String contact_no;
    String email_id;
    String company_name;
    String location;

    boolean islongpress = false;
    String entry_id = "";

    public String getEntry_id() {
        return entry_id;
    }

    public void setEntry_id(String entry_id) {
        this.entry_id = entry_id;
    }

    public boolean isIslongpress() {
        return islongpress;
    }

    public void setIslongpress(boolean islongpress) {
        this.islongpress = islongpress;
    }


    String currency_code = "", currency_code_id = "", symbol_native = "", symbol = "", decimal_digits = "";

    public String getCurrency_code() {
        return currency_code;
    }

    public String getDecimal_digits() {
        return decimal_digits;
    }

    public void setDecimal_digits(String decimal_digits) {
        this.decimal_digits = decimal_digits;
    }

    public void setCurrency_code(String currency_code) {
        this.currency_code = currency_code;
    }


    public String getCurrency_code_id() {
        return currency_code_id;
    }

    public void setCurrency_code_id(String currency_code_id) {
        this.currency_code_id = currency_code_id;
    }

    public String getSymbol_native() {
        return symbol_native;
    }

    public void setSymbol_native(String symbol_native) {
        this.symbol_native = symbol_native;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }


    /********************* From Supplier New ***********************/


    public String getUser_bridge_contacts_id() {
        return user_bridge_contacts_id;
    }

    public void setUser_bridge_contacts_id(String user_bridge_contacts_id) {
        this.user_bridge_contacts_id = user_bridge_contacts_id;
    }

    @SerializedName("user_bridge_contacts_id")
    @Expose

    private String user_bridge_contacts_id;
    @SerializedName("contact_bridge_id")
    @Expose
    private String contact_bridge_id;

    public String getContact_bridge_id() {
        return contact_bridge_id;
    }

    public void setContact_bridge_id(String contact_bridge_id) {
        this.contact_bridge_id = contact_bridge_id;
    }


    @SerializedName("bridge_id")
    @Expose
    private String bridgeId;
    @SerializedName("account_type")
    @Expose
    private String accountType;


    @SerializedName("company_detail_short")
    @Expose
    private String company_detail_short;

    @SerializedName("company_detail_long")
    @Expose
    public String companyDetailLong;


    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("geohash")
    @Expose
    private String geohash;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("country")
    @Expose
    private String country;


    @SerializedName("time_zone_id")
    @Expose
    private String timeZoneId;
    @SerializedName("time_zone_offset")
    @Expose
    private String timeZoneOffset;
    @SerializedName("setting_row_per_page")
    @Expose
    private String settingRowPerPage;
    @SerializedName("app_android_version_number")
    @Expose
    private Object appAndroidVersionNumber;
    @SerializedName("app_android_priority")
    @Expose
    private String appAndroidPriority;
    @SerializedName("app_android_push_key")
    @Expose
    private Object appAndroidPushKey;
    @SerializedName("app_android_url")
    @Expose
    private Object appAndroidUrl;
    @SerializedName("lic1_type_name")
    @Expose
    private String lic1TypeName;
    @SerializedName("lic1_no")
    @Expose
    private String lic1No;
    @SerializedName("lic1_name")
    @Expose
    private String lic1Name;
    @SerializedName("lic1_exp_date")
    @Expose
    private String lic1ExpDate;
    @SerializedName("lic2_type_name")
    @Expose
    private String lic2TypeName;
    @SerializedName("lic2_no")
    @Expose
    private String lic2No;
    @SerializedName("lic2_name")
    @Expose
    private String lic2Name;
    @SerializedName("lic2_exp_date")
    @Expose
    private String lic2ExpDate;
    @SerializedName("lic3_type_name")
    @Expose
    private String lic3TypeName;
    @SerializedName("lic3_no")
    @Expose
    private String lic3No;
    @SerializedName("lic3_name")
    @Expose
    private String lic3Name;

    public String getBridgeId() {
        return bridgeId;
    }

    public void setBridgeId(String bridgeId) {
        this.bridgeId = bridgeId;
    }

    @SerializedName("lic3_exp_date")
    @Expose

    private String lic3ExpDate;
    @SerializedName("external_connection")
    @Expose
    private String externalConnection;
    @SerializedName("newsletter")
    @Expose
    private String newsletter;
    @SerializedName("terms_and_condition")
    @Expose
    private String termsAndCondition;

    public String getCompany_detail_short() {
        return company_detail_short;
    }

    public void setCompany_detail_short(String company_detail_short) {
        this.company_detail_short = company_detail_short;
    }

    @SerializedName("sms_status")
    @Expose
    private String smsStatus;

    public String getBusiness_type() {
        return business_type;
    }

    public void setBusiness_type(String business_type) {
        this.business_type = business_type;
    }

    @SerializedName("business_type")
    @Expose
    private String business_type;

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }


}
