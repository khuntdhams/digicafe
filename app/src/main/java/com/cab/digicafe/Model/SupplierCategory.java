package com.cab.digicafe.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SupplierCategory {

    @SerializedName("pricelist_category_id")
    @Expose
    private String pricelistCategoryId;
    @SerializedName("bridge_id")
    @Expose
    private String bridgeId;
    @SerializedName("currency_code")
    @Expose
    private String currencyCode;
    @SerializedName("currency_code_id")
    @Expose
    private String currencyCodeId;
    @SerializedName("pricelist_category_name")
    @Expose
    private String pricelistCategoryName;
    @SerializedName("sort_by")
    @Expose
    private String sortBy;

    @SerializedName("pricelist_category_image")
    @Expose
    public String pricelist_category_image = "";

    private final static long serialVersionUID = -1030089896387309467L;

    public String getPricelistCategoryId() {
        return pricelistCategoryId;
    }

    public void setPricelistCategoryId(String pricelistCategoryId) {
        this.pricelistCategoryId = pricelistCategoryId;
    }

    public String getBridgeId() {
        return bridgeId;
    }

    public void setBridgeId(String bridgeId) {
        this.bridgeId = bridgeId;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getCurrencyCodeId() {
        return currencyCodeId;
    }

    public void setCurrencyCodeId(String currencyCodeId) {
        this.currencyCodeId = currencyCodeId;
    }

    public String getPricelistCategoryName() {
        return pricelistCategoryName;
    }

    public void setPricelistCategoryName(String pricelistCategoryName) {
        this.pricelistCategoryName = pricelistCategoryName;
    }

    public String getSortBy() {
        return sortBy;
    }

    public void setSortBy(String sortBy) {
        this.sortBy = sortBy;
    }

    boolean checked;

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public SupplierCategory(String pricelistCategoryName) {
        this.pricelistCategoryName = pricelistCategoryName;
    }
}
