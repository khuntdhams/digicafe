package com.cab.digicafe.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by crayond on 05/11/2017.
 */

public class Result {
    @SerializedName("error")
    private Boolean error;

    @SerializedName("message")
    private String message;


    public Result(Boolean error, String message) {
        this.error = error;
        this.message = message;
    }

    public Boolean getError() {
        return error;
    }

    public String getMessage() {
        return message;
    }


}
