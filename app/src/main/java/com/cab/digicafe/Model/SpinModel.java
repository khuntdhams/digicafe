package com.cab.digicafe.Model;

import java.io.Serializable;

public class SpinModel implements Serializable {

    public String id = "", name = "", code = "", name_plural = "", rounding = "", decimal_digits = "", symbol_native = "", symbol = "", status = "";

    public int cPos;

    public SpinModel() {

    }

    @Override
    public String toString() {
        return name;
    }

    public SpinModel(String id, String name, int cPos) {
        this.id = id;
        this.name = name;
        this.cPos = cPos;
    }
}
