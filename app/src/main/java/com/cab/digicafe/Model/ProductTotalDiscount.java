package com.cab.digicafe.Model;

import java.util.ArrayList;

public class ProductTotalDiscount {
    public String product_total_discount_type_of = "", disc_perc = "", max_disc_val = "", min_purchase_val = "";
    public ArrayList<ProductTotalDiscount> alRange = new ArrayList<>();
    public String min_range = "", max_range = "", disc_range = "";


    public ProductTotalDiscount(String min_range, String max_range, String disc_range) {
        this.min_range = min_range;
        this.max_range = max_range;
        this.disc_range = disc_range;
    }

    public ProductTotalDiscount() {

    }
}
