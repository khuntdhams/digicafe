package com.cab.digicafe.Model;

import com.cab.digicafe.Adapter.mrp.TaskPrefViewModel;
import com.cab.digicafe.Rest.Request.CatRequest;

import java.util.ArrayList;

public class ShareModel {

    public static ShareModel shareModel;

    public static ShareModel getI() {
        if (shareModel == null) shareModel = new ShareModel();
        return shareModel;
    }

    public Metal metal = new Metal();
    public boolean isCheckStInCatalogue = false;


    public String alCategory = "";

    public CatRequest catRequest;
    public String dbUnderMaster = "";
    public String utubeId = "";
    public boolean isAddNew = false;

    public boolean isImgPrev = false;
    public String bus_setting_from = ""; // setting icon, from master, from sign up - master, signup, profile
    public Userprofile userprofile = new Userprofile();


    //Enquiry

    public String purpose = "", chit_name = "", parti_nm = "";

    //

    public String isAddShopAuto = ""; // At a time of login add shop texvalley

    public Catelog catelog = new Catelog();

    public String silver_today_price = "", gold_today_price = "";
    public int total_catalog = 0;

    // Promo

    public ArrayList<Coupon> alPromo = new ArrayList<>();

    public boolean isFromAllTask = false; // display To

    public TaskPrefViewModel taskPrefViewModel = new TaskPrefViewModel();

    public Chit chitOnlyOnce;

    public String field_json_data_of_bus_for_emp = "";

    public boolean isBackFromChitLog = false; // From Invoice, from MRP - material tab

    public int sortBy = -1, orderBy = 0; //
    public boolean isSortByMaterial = true, isSortByResource = true, isSortByPlanning = true;
    public boolean isLink = true, isDlink = true, isComplete = true, isIssue = true;
    public boolean isNavigateOnce = false;


    public String whoReferMe = "";

}
