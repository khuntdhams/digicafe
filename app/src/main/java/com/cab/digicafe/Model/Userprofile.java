package com.cab.digicafe.Model;

/**
 * Created by CSS on 19-11-2017.
 */

public class Userprofile {
    String username;
    String bridge_id;

    public String getBusiness_type() {
        return business_type;
    }

    public void setBusiness_type(String business_type) {
        this.business_type = business_type;
    }

    String firstname;
    String lastname;
    String contact_no;
    String email_id;
    String location;
    String profileimage;
    public String currency_code = "";

    public String getCurrency_code_id() {
        return currency_code_id;
    }

    public void setCurrency_code_id(String currency_code_id) {
        this.currency_code_id = currency_code_id;
    }

    public String currency_code_id = "";
    public String business_type; // isBusinessLogic
    public String account_type; //
    public String field_json_data = ""; //

    public String getAccount_type() {
        return account_type;
    }

    public void setAccount_type(String account_type) {
        this.account_type = account_type;
    }

    public String getUsername() {
        return username;
    }

    public String getCurrency_code() {
        return currency_code;
    }

    public void setCurrency_code(String currency_code) {
        this.currency_code = currency_code;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getBridge_id() {
        return bridge_id;
    }

    public void setBridge_id(String bridge_id) {
        this.bridge_id = bridge_id;
    }

    public String getFirstname() {
        if (firstname == null) firstname = "";
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        if (lastname == null) lastname = "";
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getContact_no() {
        if (contact_no == null) contact_no = "";
        return contact_no;
    }

    public void setContact_no(String contact_no) {
        this.contact_no = contact_no;
    }

    public String getEmail_id() {
        if (email_id == null) email_id = "";
        return email_id;
    }

    public void setEmail_id(String email_id) {
        this.email_id = email_id;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getProfileimage() {
        if (profileimage == null) profileimage = "";
        return profileimage;
    }

    public void setProfileimage(String profileimage) {
        this.profileimage = profileimage;
    }
}
