package com.cab.digicafe.Model;

import java.util.ArrayList;

/**
 * Created by CSS on 24-11-2017.
 */

public class Chit {
    int chit_id;
    String chit_hash_id;
    String chit_name;
    String contact_number;

    public String assignedToForProp = "";
    String assign_to_bridge_id;

    public String getAssign_to_bridge_id() {
        return assign_to_bridge_id;
    }

    public void setAssign_to_bridge_id(String assign_to_bridge_id) {
        this.assign_to_bridge_id = assign_to_bridge_id;
    }

    ChitDetails chitDetails;


    public ChitDetails getChitDetails() {
        return chitDetails;
    }

    public void setChitDetails(ChitDetails chitDetails) {
        this.chitDetails = chitDetails;
    }

    public String getContact_number() {
        return contact_number;
    }

    public void setContact_number(String contact_number) {
        this.contact_number = contact_number;
    }

    public String getRef_id() {
        return ref_id;
    }

    public void setRef_id(String ref_id) {
        this.ref_id = ref_id;
    }

    public String getCase_id() {
        return case_id;
    }

    public void setCase_id(String case_id) {
        this.case_id = case_id;
    }

    String ref_id;
    String case_id;

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    String purpose;

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    String subject;

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    String location;


    public String getCurrency_code() {
        return currency_code;
    }

    public void setCurrency_code(String currency_code) {
        this.currency_code = currency_code;
    }

    String currency_code = "";
    String currency_code_id = "";

    public String getCurrency_code_id() {
        return currency_code_id;
    }

    public void setCurrency_code_id(String currency_code_id) {
        this.currency_code_id = currency_code_id;
    }

    public ArrayList<String> getTo_bridgelist_friendlyname() {
        return to_bridgelist_friendlyname;
    }

    public void setTo_bridgelist_friendlyname(ArrayList<String> to_bridgelist_friendlyname) {
        this.to_bridgelist_friendlyname = to_bridgelist_friendlyname;
    }

    ArrayList<String> to_bridgelist_friendlyname;

    public String getTransaction_status() {
        return transaction_status;
    }

    public void setTransaction_status(String transaction_status) {
        this.transaction_status = transaction_status;
    }

    public String getTotal_chit_item_value() {
        return total_chit_item_value;
    }

    public void setTotal_chit_item_value(String total_chit_item_value) {
        this.total_chit_item_value = total_chit_item_value;
    }

    public String getCreated_date() {
        return created_date;
    }

    public void setCreated_date(String created_date) {
        this.created_date = created_date;
    }

    public int getChit_item_count() {
        return chit_item_count;
    }

    public void setChit_item_count(int chit_item_count) {
        this.chit_item_count = chit_item_count;
    }

    String transaction_status;
    String total_chit_item_value;
    String created_date;

    public String getSymbol_native() {
        return symbol_native;
    }

    public void setSymbol_native(String symbol_native) {
        this.symbol_native = symbol_native;
    }

    String symbol_native = "";
    int chit_item_count;

    public int getChit_id() {
        return chit_id;
    }

    public void setChit_id(int chit_id) {
        this.chit_id = chit_id;
    }

    public String getChit_hash_id() {
        return chit_hash_id;
    }

    public void setChit_hash_id(String chit_hash_id) {
        this.chit_hash_id = chit_hash_id;
    }

    public String getChit_name() {
        return chit_name;
    }

    public void setChit_name(String chit_name) {
        this.chit_name = chit_name;
    }

    boolean islongpress;

    public boolean isIslongpress() {
        return islongpress;
    }

    public void setIslongpress(boolean islongpress) {
        this.islongpress = islongpress;
    }

    String footer_note;

    public String getFooter_note() {
        return footer_note;
    }

    public void setFooter_note(String footer_note) {
        this.footer_note = footer_note;
    }

    String header_note;

    public String getHeader_note() {
        return header_note;
    }

    public void setHeader_note(String header_note) {
        this.header_note = header_note;
    }

    String payment_mode;

    public String getPayment_mode() {
        return payment_mode;
    }

    public void setPayment_mode(String payment_mode) {
        this.payment_mode = payment_mode;
    }

    String tab;

    public String getTab() {
        return tab;
    }

    public void setTab(String tab) {
        this.tab = tab;
    }


    public boolean isdetailshow = false;

    public boolean isIsstatusshow() {
        return isstatusshow;
    }

    public void setIsstatusshow(boolean isstatusshow) {
        this.isstatusshow = isstatusshow;
    }

    public boolean isstatusshow = false;

    public boolean isIsdetailshow() {
        return isdetailshow;
    }

    public void setIsdetailshow(boolean isdetailshow) {
        this.isdetailshow = isdetailshow;
    }

    public boolean issummaryshow = false;

    public boolean isIssummaryshow() {
        return issummaryshow;
    }

    public void setIssummaryshow(boolean issummaryshow) {
        this.issummaryshow = issummaryshow;
    }


    String to_bridge_id = "", from_bridge_id = "";

    public String getTo_bridge_id() {
        return to_bridge_id;
    }

    public void setTo_bridge_id(String to_bridge_id) {
        this.to_bridge_id = to_bridge_id;
    }

    public String getFrom_bridge_id() {
        return from_bridge_id;
    }

    public void setFrom_bridge_id(String from_bridge_id) {
        this.from_bridge_id = from_bridge_id;
    }
}
