package com.cab.digicafe.Model;

public class templateModel {

    String templateName;

    public templateModel(int templateId, String templateName, Class redirectionActivity, int templateImage,String image, boolean isCheck) {
        this.templateId = templateId;
        this.templateName = templateName;
        RedirectionActivity = redirectionActivity;
        this.templateImage = templateImage;
        this.image=image;
        this.isCheck = isCheck;
    }

    public void setRedirectionActivity(Class redirectionActivity) {
        RedirectionActivity = redirectionActivity;
    }

    public Class getRedirectionActivity() {
        return RedirectionActivity;
    }

    Class RedirectionActivity;
    int templateImage;

    public boolean isCheck() {
        return isCheck;
    }

    public void setCheck(boolean check) {
        isCheck = check;
    }

    boolean isCheck = false;

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }


    public int getTemplateImage() {
        return templateImage;
    }

    public void setTemplateImage(int templateImage) {
        this.templateImage = templateImage;
    }

    public int getTemplateId() {
        return templateId;
    }

    public void setTemplateId(int templateId) {
        this.templateId = templateId;
    }

    int templateId;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    String image;
}
