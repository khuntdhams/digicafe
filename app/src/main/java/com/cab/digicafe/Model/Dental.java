package com.cab.digicafe.Model;

import java.util.ArrayList;

public class Dental {

    public boolean isTray;
    public boolean isModel;
    public boolean isBite;
    public boolean isTemp;
    public boolean isCoingTrac;
    public boolean ispreScale;
    public boolean isCeramic;
    public boolean isDirect;
    public boolean ismale;
    public boolean isfemale;

    //template 2
    public boolean isFramework;

    //template 5
    public boolean isJig;
    String impression;
    ArrayList<implantModel> abutmentList = new ArrayList<>();
    ArrayList<implantModel> impressionList = new ArrayList<>();
    ArrayList<implantModel> implantList = new ArrayList<>();

    //template 6
    String chief_complaint;
    String treatment;
    public boolean isFrontal;
    ArrayList<ModelFile> al_selet_frontal = new ArrayList<>();
    public boolean isFrontalSmile;
    ArrayList<ModelFile> al_selet_frontalSmile = new ArrayList<>();
    public boolean isLeftLateral;
    ArrayList<ModelFile> al_selet_leftLateral = new ArrayList<>();
    public boolean isLeftLateralSmile;
    ArrayList<ModelFile> al_selet_leftLateralSmile = new ArrayList<>();
    public boolean isRightLateral;
    ArrayList<ModelFile> al_selet_rightLateral = new ArrayList<>();
    public boolean isRightLateralSmile;
    ArrayList<ModelFile> al_selet_rightLateralSmile = new ArrayList<>();
    public boolean isIntraFrontal;
    ArrayList<ModelFile> al_selet_intraFrontal = new ArrayList<>();
    public boolean isIntraRightLateral;
    ArrayList<ModelFile> al_selet_intraRightLateral = new ArrayList<>();
    public boolean isIntraLeftLateral;
    ArrayList<ModelFile> al_selet_intraLeftLateral = new ArrayList<>();
    public boolean isIntraUpperOcclusal;
    ArrayList<ModelFile> al_selet_intraUpperOcclusal = new ArrayList<>();
    public boolean isIntraLeftOcclusal;
    ArrayList<ModelFile> al_selet_intraLeftOcclusal = new ArrayList<>();


    //template 7
    String Colour = "";
    public boolean isColour;
    ArrayList<ModelFile> al_selet_colour = new ArrayList<>();

    //template 8
    String PhotoInstruction = "";
    public boolean isInstruction;
    ArrayList<ModelFile> al_selet_instruction = new ArrayList<>();


    public String getPhotoInstruction() {
        return PhotoInstruction;
    }

    public void setPhotoInstruction(String photoInstruction) {
        PhotoInstruction = photoInstruction;
    }

    public boolean isInstruction() {
        return isInstruction;
    }

    public void setInstruction(boolean instruction) {
        isInstruction = instruction;
    }

    public ArrayList<ModelFile> getAl_selet_instruction() {
        return al_selet_instruction;
    }

    public void setAl_selet_instruction(ArrayList<ModelFile> al_selet_instruction) {
        this.al_selet_instruction = al_selet_instruction;
    }


    public ArrayList<ModelFile> getAl_selet_colour() {
        return al_selet_colour;
    }

    public void setAl_selet_colour(ArrayList<ModelFile> al_selet_colour) {
        this.al_selet_colour = al_selet_colour;
    }

    public String getColour() {
        return Colour;
    }

    public void setColour(String colour) {
        Colour = colour;
    }


    public boolean isIntraLeftLateral() {
        return isIntraLeftLateral;
    }

    public void setIntraLeftLateral(boolean intraLeftLateral) {
        isIntraLeftLateral = intraLeftLateral;
    }

    public ArrayList<ModelFile> getAl_selet_intraLeftLateral() {
        return al_selet_intraLeftLateral;
    }

    public void setAl_selet_intraLeftLateral(ArrayList<ModelFile> al_selet_intraLeftLateral) {
        this.al_selet_intraLeftLateral = al_selet_intraLeftLateral;
    }

    public boolean isIntraUpperOcclusal() {
        return isIntraUpperOcclusal;
    }

    public void setIntraUpperOcclusal(boolean intraUpperOcclusal) {
        isIntraUpperOcclusal = intraUpperOcclusal;
    }

    public ArrayList<ModelFile> getAl_selet_intraUpperOcclusal() {
        return al_selet_intraUpperOcclusal;
    }

    public void setAl_selet_intraUpperOcclusal(ArrayList<ModelFile> al_selet_intraUpperOcclusal) {
        this.al_selet_intraUpperOcclusal = al_selet_intraUpperOcclusal;
    }

    public boolean isIntraLeftOcclusal() {
        return isIntraLeftOcclusal;
    }

    public void setIntraLeftOcclusal(boolean intraLeftOcclusal) {
        isIntraLeftOcclusal = intraLeftOcclusal;
    }

    public ArrayList<ModelFile> getAl_selet_intraLeftOcclusal() {
        return al_selet_intraLeftOcclusal;
    }

    public void setAl_selet_intraLeftOcclusal(ArrayList<ModelFile> al_selet_intraLeftOcclusal) {
        this.al_selet_intraLeftOcclusal = al_selet_intraLeftOcclusal;
    }


    public boolean isIntraFrontal() {
        return isIntraFrontal;
    }

    public void setIntraFrontal(boolean intraFrontal) {
        isIntraFrontal = intraFrontal;
    }

    public ArrayList<ModelFile> getAl_selet_intraFrontal() {
        return al_selet_intraFrontal;
    }

    public void setAl_selet_intraFrontal(ArrayList<ModelFile> al_selet_intraFrontal) {
        this.al_selet_intraFrontal = al_selet_intraFrontal;
    }

    public boolean isIntraRightLateral() {
        return isIntraRightLateral;
    }

    public void setIntraRightLateral(boolean intraRightLateral) {
        isIntraRightLateral = intraRightLateral;
    }

    public ArrayList<ModelFile> getAl_selet_intraRightLateral() {
        return al_selet_intraRightLateral;
    }

    public void setAl_selet_intraRightLateral(ArrayList<ModelFile> al_selet_intraRightLateral) {
        this.al_selet_intraRightLateral = al_selet_intraRightLateral;
    }

    public boolean isRightLateralSmile() {
        return isRightLateralSmile;
    }

    public void setRightLateralSmile(boolean rightLateralSmile) {
        isRightLateralSmile = rightLateralSmile;
    }

    public ArrayList<ModelFile> getAl_selet_rightLateralSmile() {
        return al_selet_rightLateralSmile;
    }

    public void setAl_selet_rightLateralSmile(ArrayList<ModelFile> al_selet_rightLateralSmile) {
        this.al_selet_rightLateralSmile = al_selet_rightLateralSmile;
    }

    public boolean isRightLateral() {
        return isRightLateral;
    }

    public void setRightLateral(boolean rightLateral) {
        isRightLateral = rightLateral;
    }

    public ArrayList<ModelFile> getAl_selet_rightLateral() {
        return al_selet_rightLateral;
    }

    public void setAl_selet_rightLateral(ArrayList<ModelFile> al_selet_rightLateral) {
        this.al_selet_rightLateral = al_selet_rightLateral;
    }

    public boolean isLeftLateralSmile() {
        return isLeftLateralSmile;
    }

    public void setLeftLateralSmile(boolean leftLateralSmile) {
        isLeftLateralSmile = leftLateralSmile;
    }

    public ArrayList<ModelFile> getAl_selet_leftLateralSmile() {
        return al_selet_leftLateralSmile;
    }

    public void setAl_selet_leftLateralSmile(ArrayList<ModelFile> al_selet_leftLateralSmile) {
        this.al_selet_leftLateralSmile = al_selet_leftLateralSmile;
    }

    public boolean isLeftLateral() {
        return isLeftLateral;
    }

    public void setLeftLateral(boolean leftLateral) {
        isLeftLateral = leftLateral;
    }

    public ArrayList<ModelFile> getAl_selet_leftLateral() {
        return al_selet_leftLateral;
    }

    public void setAl_selet_leftLateral(ArrayList<ModelFile> al_selet_leftLateral) {
        this.al_selet_leftLateral = al_selet_leftLateral;
    }


    public boolean isFrontalSmile() {
        return isFrontalSmile;
    }

    public void setFrontalSmile(boolean frontalSmile) {
        isFrontalSmile = frontalSmile;
    }

    public ArrayList<ModelFile> getAl_selet_frontalSmile() {
        return al_selet_frontalSmile;
    }

    public void setAl_selet_frontalSmile(ArrayList<ModelFile> al_selet_frontalSmile) {
        this.al_selet_frontalSmile = al_selet_frontalSmile;
    }


    public ArrayList<ModelFile> getAl_selet_frontal() {
        return al_selet_frontal;
    }

    public void setAl_selet_frontal(ArrayList<ModelFile> al_selet_frontal) {
        this.al_selet_frontal = al_selet_frontal;
    }


    public boolean isFrontal() {
        return isFrontal;
    }

    public void setFrontal(boolean frontal) {
        isFrontal = frontal;
    }


    public String getChief_complaint() {
        return chief_complaint;
    }

    public void setChief_complaint(String chief_complaint) {
        this.chief_complaint = chief_complaint;
    }

    public String getTreatment() {
        return treatment;
    }

    public void setTreatment(String treatment) {
        this.treatment = treatment;
    }

    public boolean isJig() {
        return isJig;
    }

    public void setJig(boolean jig) {
        isJig = jig;
    }

    public boolean isFramework() {
        return isFramework;
    }

    public void setFramework(boolean framework) {
        isFramework = framework;
    }


    String shade = "";

    public String getShade() {
        return shade;
    }

    public void setShade(String shade) {
        this.shade = shade;
    }

    public ArrayList<Teeth> getTeethList() {
        return teethList;
    }

    public void setTeethList(ArrayList<Teeth> teethList) {
        this.teethList = teethList;
    }

    ArrayList<Teeth> teethList = new ArrayList<>();
    ArrayList<ModelFile> al_selet = new ArrayList<>();

    public ArrayList<ModelFile> getAl_selet() {
        return al_selet;
    }

    public void setAl_selet(ArrayList<ModelFile> al_selet) {
        this.al_selet = al_selet;
    }

    public ArrayList<ModelFile> getAl_selet_tray() {
        return al_selet_tray;
    }

    public void setAl_selet_tray(ArrayList<ModelFile> al_selet_tray) {
        this.al_selet_tray = al_selet_tray;
    }

    public ArrayList<ModelFile> getAl_selet_model() {
        return al_selet_model;
    }

    public void setAl_selet_model(ArrayList<ModelFile> al_selet_model) {
        this.al_selet_model = al_selet_model;
    }

    public ArrayList<ModelFile> getAl_selet_bite() {
        return al_selet_bite;
    }

    public void setAl_selet_bite(ArrayList<ModelFile> al_selet_bite) {
        this.al_selet_bite = al_selet_bite;
    }

    public ArrayList<ModelFile> getAl_selet_temp() {
        return al_selet_temp;
    }

    public void setAl_selet_temp(ArrayList<ModelFile> al_selet_temp) {
        this.al_selet_temp = al_selet_temp;
    }

    ArrayList<ModelFile> al_selet_tray = new ArrayList<>();
    ArrayList<ModelFile> al_selet_model = new ArrayList<>();
    ArrayList<ModelFile> al_selet_bite = new ArrayList<>();
    ArrayList<ModelFile> al_selet_temp = new ArrayList<>();

    String name;
    String age;
    String sex;
    String instructioon;
    String preference;


    public ArrayList<implantModel> getAbutmentList() {
        return abutmentList;
    }

    public void setAbutmentList(ArrayList<implantModel> abutmentList) {
        this.abutmentList = abutmentList;
    }

    public ArrayList<implantModel> getImpressionList() {
        return impressionList;
    }

    public void setImpressionList(ArrayList<implantModel> impressionList) {
        this.impressionList = impressionList;
    }

    public ArrayList<implantModel> getImplantList() {
        return implantList;
    }

    public void setImplantList(ArrayList<implantModel> implantList) {
        this.implantList = implantList;
    }

    public String getImpression() {
        return impression;
    }

    public void setImpression(String impression) {
        this.impression = impression;
    }


    public StringBuilder getTeeth() {
        return teeth;
    }

    public void setTeeth(StringBuilder teeth) {
        this.teeth = teeth;
    }

    StringBuilder teeth;

    public boolean isOffline() {
        return isOffline;
    }

    public void setOffline(boolean offline) {
        isOffline = offline;
    }

    boolean isOffline;

    public String getInstructioon() {
        return instructioon;
    }

    public void setInstructioon(String instructioon) {
        this.instructioon = instructioon;
    }

    public String getPreference() {
        return preference;
    }

    public void setPreference(String preference) {
        this.preference = preference;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }


    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

}
