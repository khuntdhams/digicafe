package com.cab.digicafe.Model;

import com.cab.digicafe.Timeline.DateTimeUtils;

import java.io.Serializable;
import java.util.Date;

public class Coupon implements Serializable {

    public String couponCode = "";
    Date startDt, endDt;
    public String terms = "";
    public int couponId = 0;

    public double promo_value = 0.0, promo_modulo = 0.0, max_discount = 0.0, min_purchase = 0.0;

    public Coupon() {

    }

    public Coupon(String couponCode, double value, double modulo, double max_discount, double min_purchase, Date startDt, Date endDt, String terms) {
        this.couponCode = couponCode;
        this.promo_value = value;
        this.promo_modulo = modulo;
        this.terms = terms;
        this.startDt = startDt;
        this.endDt = endDt;
        this.max_discount = max_discount;
        this.min_purchase = min_purchase;
    }

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }


    public Date getStartDt() {
        return startDt;
    }

    public void setStartDt(Date startDt) {
        this.startDt = startDt;
    }

    public Date getEndDt() {
        return endDt;
    }

    public void setEndDt(Date endDt) {
        this.endDt = endDt;
    }

    public String getStartDateInFormat() {
        return DateTimeUtils.formatDateToString(startDt);
    }

    public String getEndDateInFormat() {
        return DateTimeUtils.formatDateToString(endDt);
    }
}
