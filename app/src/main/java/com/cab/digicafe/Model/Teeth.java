package com.cab.digicafe.Model;

public class Teeth {

    boolean isCheckTop = false;
    boolean isCheckBottom = false;
    String numberTop, numberBottom;

    public boolean isCheckTop() {
        return isCheckTop;
    }

    public void setCheckTop(boolean checkTop) {
        isCheckTop = checkTop;
    }

    public boolean isCheckBottom() {
        return isCheckBottom;
    }

    public void setCheckBottom(boolean checkBottom) {
        isCheckBottom = checkBottom;
    }

    public String getNumberTop() {
        return numberTop;
    }

    public void setNumberTop(String numberTop) {
        this.numberTop = numberTop;
    }

    public String getNumberBottom() {
        return numberBottom;
    }

    public void setNumberBottom(String numberBottom) {
        this.numberBottom = numberBottom;
    }

    public Teeth(String numberTop, String numberBottom, boolean isCheckTop, boolean isCheckBottom) {
        this.numberTop = numberTop;
        this.numberBottom = numberBottom;
        this.isCheckTop = isCheckTop;
        this.isCheckBottom = isCheckBottom;
    }
}
