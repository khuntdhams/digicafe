package com.cab.digicafe.Model;

/**
 * Created by CSS on 02-11-2017.
 */

public class Caterer {
    String id;
    String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobilenumber() {
        return mobilenumber;
    }

    public void setMobilenumber(String mobilenumber) {
        this.mobilenumber = mobilenumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    String mobilenumber;
    String email;

    public Caterer(String id, String name, String email, String mobilenumber) {
        this.id = id;

        this.name = name;
        this.email = email;
        this.mobilenumber = mobilenumber;
    }
}
