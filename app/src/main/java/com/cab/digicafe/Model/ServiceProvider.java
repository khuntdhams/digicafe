package com.cab.digicafe.Model;

/**
 * Created by pc51 on 4/5/2018.
 */

public class ServiceProvider {

    public String username;
    public String company_name;
    public String company_image;
    public String location;
    boolean ischecked;
    String from_bridge_id;
    String to_bridge_id;

    public String getCurrency_code() {
        return currency_code;
    }

    public void setCurrency_code(String currency_code) {
        this.currency_code = currency_code;
    }

    String currency_code;

    public boolean isIschecked() {
        return ischecked;
    }

    public void setIschecked(boolean ischecked) {
        this.ischecked = ischecked;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public String getCompany_image() {
        return company_image;
    }

    public void setCompany_image(String company_image) {
        this.company_image = company_image;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }


    public String getFrom_bridge_id() {
        return from_bridge_id;
    }

    public void setFrom_bridge_id(String from_bridge_id) {
        this.from_bridge_id = from_bridge_id;
    }

    public String getTo_bridge_id() {
        return to_bridge_id;
    }

    public void setTo_bridge_id(String to_bridge_id) {
        this.to_bridge_id = to_bridge_id;
    }
}
