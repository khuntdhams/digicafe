package com.cab.digicafe.Model;

import com.cab.digicafe.MyCustomClass.JsonObjParse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by CSS on 02-11-2017.
 */

public class Catelog {
    int pricelist_id;
    String bridge_id;
    String displayname;
    String default_name;
    String offer;
    String symbol_native;

    public int sortOrder = 0;

    public int getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(int sortOrder) {
        this.sortOrder = sortOrder;
    }

    ArrayList<ModelFile> al_selet = new ArrayList<>();

    public ArrayList<ModelFile> getAl_selet() {
        return al_selet;
    }

    public void setAl_selet(ArrayList<ModelFile> al_selet) {
        this.al_selet = al_selet;
    }

    public String getSymbol_native() {
        return symbol_native;
    }

    public void setSymbol_native(String symbol_native) {
        this.symbol_native = symbol_native;
    }

    String name;
    String old_measure;
    String old_unit;
    String old_count;
    String old_quality;
    String old_make;
    String old_color;
    String price;
    String old_description;
    String currency_code;
    String currency_code_id;
    String out_of_stock;
    String sales_time;

    public String getDecimal_digits() {
        return decimal_digits;
    }

    public void setDecimal_digits(String decimal_digits) {
        this.decimal_digits = decimal_digits;
    }

    String decimal_digits;

    int cartcount;

    String copy_flag;
    ArrayList<String> imageArr = new ArrayList<>();
    String tax_json_data;

    public Catelog() {
    }

    public String getTax_json_data() {
        return tax_json_data;
    }

    public void setTax_json_data(String tax_json_data) {
        this.tax_json_data = tax_json_data;
    }

    public Double getDiscounted_price() {
        return discounted_price;
    }

    public void setDiscounted_price(Double discounted_price) {
        this.discounted_price = discounted_price;
    }

    public Double getDiscount_percentage() {
        return discount_percentage;
    }

    public void setDiscount_percentage(Double discount_percentage) {
        this.discount_percentage = discount_percentage;
    }

    Double discounted_price;
    Double discount_percentage;


   /* public ArrayList<String> getDiscountPriceArr() {
        return discountPriceArr;
    }

    public void setDiscountPriceArr(ArrayList<String> discountPriceArr) {
        this.discountPriceArr = discountPriceArr;
    }

    public ArrayList<String> getDiscountPercentageArr() {
        return discountPercentageArr;
    }

    public void setDiscountPercentageArr(ArrayList<String> discountPercentageArr) {
        this.discountPercentageArr = discountPercentageArr;
    }*/

    public String getDefault_name() {
        return default_name;
    }

    public String optionForSurvey = "", commentForfSurevey = "";

    public void setDefault_name(String default_name) {

        String surveyoption[] = default_name.split("///");

        if (surveyoption.length > 0 && (commentForfSurevey.length() > 0 || optionForSurvey.length() > 0)) {
            default_name = surveyoption[0] + "///" + optionForSurvey + "///" + commentForfSurevey;
        } else if ((commentForfSurevey.length() > 0 || optionForSurvey.length() > 0)) {
            default_name = default_name + "///" + optionForSurvey + "///" + commentForfSurevey;
        }

        this.default_name = default_name;

    }

    public String getOffer() {
        return offer;
    }

    public void setOffer(String offer) {
        this.offer = offer;
    }


    public ArrayList<String> getVideolArr() {
        return videolArr;
    }

    public void setVideolArr(ArrayList<String> videolArr) {
        this.videolArr = videolArr;
    }

    ArrayList<String> videolArr;

    public int getCartcount() {
        return cartcount;
    }

    public void setCartcount(int cartcount) {
        this.cartcount = cartcount;
    }

    public ArrayList<String> getImageArr() {
        if (imageArr == null) imageArr = new ArrayList<>();
        return imageArr;
    }

    public void setImageArr(ArrayList<String> imageArr) {
        this.imageArr = imageArr;
    }

    public String getDisplayname() {
        return displayname;
    }

    public void setDisplayname(String displayname) {
        this.displayname = displayname;
    }

    public String getAvailable_stock() {
        return available_stock;
    }

    public void setAvailable_stock(String available_stock) {
        this.available_stock = available_stock;
    }

    public int getEntry_id() {
        return pricelist_id;
    }

    public void setEntry_id(int entry_id) {
        this.pricelist_id = entry_id;
    }

    public String getBridge_id() {
        return bridge_id;
    }

    public void setBridge_id(String bridge_id) {
        this.bridge_id = bridge_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMeasure() {
        return old_measure;
    }

    public void setMeasure(String measure) {
        this.old_measure = measure;
    }

    public String getUnit() {
        return old_unit;
    }

    public void setUnit(String unit) {
        this.old_unit = unit;
    }

    public String getCount() {
        return old_count;
    }

    public void setCount(String count) {
        this.old_count = count;
    }

    public String getQuality() {
        return old_quality;
    }

    public void setQuality(String quality) {
        this.old_quality = quality;
    }

    public String getMake() {
        return old_make;
    }

    public void setMake(String make) {
        this.old_make = make;
    }

    public String getColor() {
        return old_color;
    }

    public void setColor(String color) {
        this.old_color = color;
    }

    public String getMrp() {
        return price;
    }

    public String getMrpWithProperty() {

        Double mrpprice = Double.parseDouble(price);
        String p = price;
        String property = JsonObjParse.getValueEmpty(internal_json_data, "property");
        if (!property.isEmpty()) {

            String sqFt = JsonObjParse.getValueEmpty(property, "proprty_sqft");
            if (!sqFt.isEmpty()) {
                int sqFtVal = Integer.parseInt(sqFt);
                mrpprice = mrpprice * sqFtVal;
            }

            try {
                String ExtraInfoPrice = JsonObjParse.getValueEmpty(property, "ExtraInfoPrice");
                mrpprice = mrpprice + getExtraPropertyInfo(ExtraInfoPrice);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            p = mrpprice + "";

        }


        return p;
    }

    public String getMrpWithJewel() {

        Double mrpprice = Double.parseDouble(price);
        String p = price;
        String jewel = JsonObjParse.getValueEmpty(internal_json_data, "jewel");
        if (!jewel.isEmpty()) {


            String metalPosId = JsonObjParse.getValueEmpty(jewel, "metalPosId");
            int posId = 0;
            if (!metalPosId.isEmpty()) posId = Integer.parseInt(metalPosId);

            try {
                if (ShareModel.getI().metal.alMetal.size() > posId) {
                    Metal metal = ShareModel.getI().metal.alMetal.get(posId);
                    if (!metal.metalPrice.isEmpty()) {
                        mrpprice = Double.valueOf(metal.metalPrice);
                    }
                }
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }


            String weight = JsonObjParse.getValueEmpty(jewel, "weight");

            if (!weight.isEmpty()) {
                float wt = Float.parseFloat(weight);
                //dis_mrp = (dis_mrp * wt)/10; // 10 gram
                mrpprice = (mrpprice * wt) / 1; // 1 unit
            }

            p = mrpprice + "";


        }


        return p;
    }

    public String getdiscountmrp() {
        Double mrpprice = Double.parseDouble(price);


        String property = JsonObjParse.getValueEmpty(internal_json_data, "property");
        if (!property.isEmpty()) {

            String sqFt = JsonObjParse.getValueEmpty(property, "proprty_sqft");
            if (!sqFt.isEmpty()) {
                int sqFtVal = Integer.parseInt(sqFt);
                mrpprice = mrpprice * sqFtVal;
            }

            try {
                String ExtraInfoPrice = JsonObjParse.getValueEmpty(property, "ExtraInfoPrice");
                mrpprice = mrpprice + getExtraPropertyInfo(ExtraInfoPrice);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }


        String jewel = JsonObjParse.getValueEmpty(internal_json_data, "jewel");
        if (!jewel.isEmpty()) {


            String metalPosId = JsonObjParse.getValueEmpty(jewel, "metalPosId");
            int posId = 0;
            if (!metalPosId.isEmpty()) posId = Integer.parseInt(metalPosId);

            try {
                if (ShareModel.getI().metal.alMetal.size() > posId) {
                    Metal metal = ShareModel.getI().metal.alMetal.get(posId);
                    if (!metal.metalPrice.isEmpty()) {
                        mrpprice = Double.valueOf(metal.metalPrice);
                    }
                }

            } catch (NumberFormatException e) {
                e.printStackTrace();
            }


            String weight = JsonObjParse.getValueEmpty(jewel, "weight");

            if (!weight.isEmpty()) {
                float wt = Float.parseFloat(weight);
                //dis_mrp = (dis_mrp * wt)/10; // 10 gram
                mrpprice = (mrpprice * wt) / 1; // 1 unit
            }


        }


        Double discountprice = mrpprice;
        if (discount_percentage > 0) {
            if (mrpprice > 0) {
                discountprice = mrpprice - (mrpprice * (discount_percentage / 100));
            }
        } else if (discounted_price > 0) {
            discountprice = discounted_price;
        }





       /* if (discountPercentageArr.size()> 0)
        {

            String discountpricentage = discountPercentageArr.get(0).trim();
            Double precentage = Double.parseDouble(discountpricentage);
            if (mrpprice >0)
            {
                discountprice = mrpprice - ( mrpprice * (precentage/100));
            }
        }else if (discountPriceArr.size()> 0)
        {
            String discount_price = discountPriceArr.get(0).trim();
            discountprice = Double.parseDouble(discount_price);
        }else
        {
            discountprice = mrpprice;
        }*/


        return String.valueOf(discountprice);
    }


   /* public int getExtraPropertyInfo(String extraInfo) throws JSONException {
        int totalInfoVal = 0;
        if (extraInfo != null && !extraInfo.isEmpty()) {
            JSONObject joExtra = new JSONObject(extraInfo);
            Iterator keys = joExtra.keys();
            int i = 0;
            while (keys.hasNext()) {
                try {
                    String key = (String) keys.next();
                    String info = JsonObjParse.getValueFromJsonObj(joExtra, key);

                    if (!info.isEmpty()) {
                        int infoVal = Integer.parseInt(info);
                        totalInfoVal = totalInfoVal + infoVal;
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return totalInfoVal;
    }*/

    public Double getExtraPropertyInfo(String extraInfo) throws JSONException {
        Double totalInfoVal = 0.0;
        if (extraInfo != null && !extraInfo.isEmpty()) {

            JSONArray jaExtraInfo = new JSONArray(extraInfo);

            for (int i = 0; i < jaExtraInfo.length(); i++) {
                JSONObject jsonObject = jaExtraInfo.getJSONObject(i);

                Iterator keys = jsonObject.keys();
                while (keys.hasNext()) {
                    try {
                        String key = (String) keys.next();
                        String info = JsonObjParse.getValueFromJsonObj(jsonObject, key);

                        if (!info.isEmpty()) {
                            int infoVal = Integer.parseInt(info);
                            totalInfoVal = totalInfoVal + infoVal;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }
        }

        return totalInfoVal;
    }

    // Commodities


    public double getAdditionalDeductinalInfo(String extraInfo, Double mrp) throws JSONException {
        double totalInfoVal = 0;
        if (extraInfo != null && !extraInfo.isEmpty()) {

            JSONArray jaExtraInfo = new JSONArray(extraInfo);

            for (int i = 0; i < jaExtraInfo.length(); i++) {
                JSONObject jsonObject = jaExtraInfo.getJSONObject(i);

                Iterator keys = jsonObject.keys();
                while (keys.hasNext()) {
                    try {
                        String key = (String) keys.next();
                        String info = JsonObjParse.getValueFromJsonObj(jsonObject, key);


                        String ded2 = JsonObjParse.getValueEmpty(info, "act_price");
                        String oDed2 = JsonObjParse.getValueEmpty(info, "org_price");
                        String dedPerc2 = JsonObjParse.getValueEmpty(info, "act_perc");
                        String oDedPerc2 = JsonObjParse.getValueEmpty(info, "org_perc");

                        totalInfoVal = totalInfoVal + calcValuesOfAddDed(mrp, dedPerc2, oDedPerc2, ded2, oDed2);

                        /* if (!info.isEmpty()) {
                             int infoVal = Integer.parseInt(info);
                             totalInfoVal = totalInfoVal + infoVal;
                         }*/
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }
        }

        return totalInfoVal;
    }

    public Double calcValuesOfAddDed(Double mrp, String act_perc, String org_perc, String act_price, String org_price) {

        Double dedPriceForJewel = 0.0;

        if (!act_perc.isEmpty()) {
            Double ded1Val = Double.valueOf(act_perc);
            dedPriceForJewel = dedPriceForJewel + Double.valueOf(getValues(mrp, ded1Val));
        } else if (!org_perc.isEmpty()) {
            Double ded1Val = Double.valueOf(org_perc);
            dedPriceForJewel = dedPriceForJewel + Double.valueOf(getValues(mrp, ded1Val));
        } else if (!act_price.isEmpty()) {
            Double ded1Val = Double.valueOf(act_price);
            dedPriceForJewel = dedPriceForJewel + ded1Val;
        } else if (!org_price.isEmpty()) {
            Double ded1Val = Double.valueOf(org_price);
            dedPriceForJewel = dedPriceForJewel + ded1Val;
        }

        return dedPriceForJewel;
    }


    public String getValues(Double mrpprice, Double discount_percentage) {
        Double discountprice = 0.0;

        if (discount_percentage > 0) {
            if (mrpprice > 0) {
                Double totalDisc = (mrpprice * (discount_percentage / 100));
                discountprice = totalDisc;
            }
        }

        return String.valueOf(discountprice);
    }


    public String getValues(Double mrpprice, Double discount_percentage, Double discounted_price) {
        Double discountprice = 0.0;

        if (discount_percentage > 0) {
            if (mrpprice > 0) {
                Double totalDisc = (mrpprice * (discount_percentage / 100));
                discountprice = totalDisc;
            }
        } else if (discounted_price > 0) {
            discountprice = discounted_price;
        }

        return String.valueOf(discountprice);
    }


    //

    public void setMrp(String mrp) {
        this.price = mrp;
    }

    public String getDescription() {
        return old_description;
    }

    public void setDescription(String description) {
        this.old_description = description;
    }

    public String getCurrency_code() {
        return currency_code;
    }

    public void setCurrency_code(String currency_code) {
        this.currency_code = currency_code;
    }

    public String getCurrency_code_id() {
        return currency_code_id;
    }

    public void setCurrency_code_id(String currency_code_id) {
        this.currency_code_id = currency_code_id;
    }

    public String getOut_of_stock() {
        return out_of_stock;
    }

    public void setOut_of_stock(String out_of_stock) {
        this.out_of_stock = out_of_stock;
    }

    public String getSales_time() {
        return sales_time;
    }

    public void setSales_time(String sales_time) {
        this.sales_time = sales_time;
    }

    public String getCopy_flag() {
        return copy_flag;
    }

    public void setCopy_flag(String copy_flag) {
        this.copy_flag = copy_flag;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getGeohash() {
        return geohash;
    }

    public void setGeohash(String geohash) {
        this.geohash = geohash;
    }

    public String getCreated_date() {
        return created_date;
    }

    public void setCreated_date(String created_date) {
        this.created_date = created_date;
    }

    String available_stock = "";
    String group;
    String category;
    String total;
    String latitude;
    String longitude;
    String geohash;
    String created_date;


    String image_json_data;
    String field_json_data;
    String additional_json_data;

    public String getInternal_json_data() {
        return internal_json_data;
    }

    public void setInternal_json_data(String internal_json_data) {
        this.internal_json_data = internal_json_data;
    }

    String internal_json_data;

    public String getImage_json_data() {
        return image_json_data;
    }

    public void setImage_json_data(String image_json_data) {
        this.image_json_data = image_json_data;
    }

    String img_url;

    public String getImg_url() {
        return img_url;
    }

    public void setImg_url(String img_url) {
        this.img_url = img_url;
    }

    /**************/

    String cross_reference;

    public String getCross_reference() {
        return cross_reference;
    }

    public void setCross_reference(String cross_reference) {
        this.cross_reference = cross_reference;
    }


    public String getField_json_data() {
        return field_json_data;
    }

    public void setField_json_data(String field_json_data) {
        this.field_json_data = field_json_data;
    }

    public String getAdditional_json_data() {
        return additional_json_data;
    }

    public void setAdditional_json_data(String additional_json_data) {
        this.additional_json_data = additional_json_data;
    }


    String price_type = "", updated_date = "";  // for DB

    public Catelog(String internal_json_data) {
        this.internal_json_data = internal_json_data;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Catelog) {

            String template = JsonObjParse.getValueEmpty(((Catelog) obj).getInternal_json_data(), "doctor_template_type");
            String type = JsonObjParse.getValueEmpty(this.getInternal_json_data(), "doctor_template_type");
            return template.equals(type);
        } else {
            return super.equals(obj);
        }

    }
}
