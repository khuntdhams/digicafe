package com.cab.digicafe.Model;

public class implantModel {

    public implantModel(int id, String valueD, String valueL) {
        this.id = id;
        this.valueD = valueD;
        this.valueL = valueL;
    }

    int id;
    String valueD;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getValueD() {
        return valueD;
    }

    public void setValueD(String valueD) {
        this.valueD = valueD;
    }

    public String getValueL() {
        return valueL;
    }

    public void setValueL(String valueL) {
        this.valueL = valueL;
    }

    String valueL;
}
