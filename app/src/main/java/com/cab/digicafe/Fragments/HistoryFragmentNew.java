package com.cab.digicafe.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.cab.digicafe.Adapter.HistoryAdapter;
import com.cab.digicafe.Dialogbox.CustomDialogClass;
import com.cab.digicafe.Helper.RecyclerItemTouchHelper;
import com.cab.digicafe.Helper.SessionManager;
import com.cab.digicafe.Model.Chit;
import com.cab.digicafe.R;
import com.cab.digicafe.Rest.ApiClient;
import com.cab.digicafe.Rest.ApiInterface;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.Headers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HistoryFragmentNew extends Fragment implements RecyclerItemTouchHelper.RecyclerItemTouchHelperListener, SwipyRefreshLayout.OnRefreshListener {

    private RecyclerView recyclerView;
    static private HistoryAdapter mAdapter;

    private List<Chit> orderlist = new ArrayList<>();
    SessionManager sessionManager;
    String sessionString;

    int totalcount = 0;
    String str_actionid = "";

    public HistoryFragmentNew() {
        // Required empty public constructor
    }


    RelativeLayout rl_del;
    private boolean isLoaded = false;

    public static HistoryFragmentNew newInstance(String param1, String param2) {
        HistoryFragmentNew fragment = new HistoryFragmentNew();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.content_fragment_history, container, false);


        recyclerView = (RecyclerView) v.findViewById(R.id.recycler_view);
        rl_del = (RelativeLayout) v.findViewById(R.id.rl_del);
        // listen refresh event


        sessionManager = new SessionManager(getActivity());

        Log.e("i am here", "create");


        mAdapter = new HistoryAdapter(orderlist, getContext(), new HistoryAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Chit item) {

            }

            @Override
            public void onItemLongClick(Chit item, int pos) {
                Log.e("test", " trdds1");
                if (orderlist.get(pos).isIslongpress()) {
                    orderlist.get(pos).setIslongpress(false);
                } else {
                    orderlist.get(pos).setIslongpress(true);
                }

                boolean isanyitem = false;
                str_actionid = "";
                totalcount = 0;
                for (int i = 0; i < orderlist.size(); i++) {
                    if (orderlist.get(i).isIslongpress()) {
                        totalcount++;
                        isanyitem = true;
                        str_actionid = str_actionid + String.valueOf(orderlist.get(i).getChit_id()) + ",";
                    }
                }

                mAdapter.notifyDataSetChanged();

                if (totalcount > 0) {
                    rl_del.setVisibility(View.VISIBLE);

                } else {
                    rl_del.setVisibility(View.GONE);

                }
            }
        });
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new RecyclerItemTouchHelper(0, ItemTouchHelper.LEFT, new HistoryFragmentNew());
        new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(recyclerView);


        mSwipyRefreshLayout = (SwipyRefreshLayout) v.findViewById(R.id.swipyrefreshlayout);
        mSwipyRefreshLayout.setOnRefreshListener(this);
        // usersignin(sessionManager.getsession(), 1, false);


        rl_del.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new CustomDialogClass(getActivity(), new CustomDialogClass.OnDialogClickListener() {
                    @Override
                    public void onDialogImageRunClick(int positon) {
                        if (positon == 0) {

                        } else if (positon == 1) {
                            //str_actionid = str_actionid.replaceAll(",$", "");
                            Log.e("str_actionid", str_actionid);
                            removechit(sessionManager.getsession(), str_actionid, false);
                        }
                    }
                }, "Are you sure you want to archived " + totalcount + " chit?").show();


            }
        });

        if (!isLoaded) {
            isLoaded = true;
            usersignin(sessionManager.getsession(), 1, false);
        }

        return v;
    }

    public void usersignin(String usersession, final int pagecount, final boolean isremoved) {


        ApiInterface apiService =
                ApiClient.getClient(getActivity()).create(ApiInterface.class);
        Call call = apiService.getSenditems(usersession, pagecount, "", "");
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                mSwipyRefreshLayout.setRefreshing(false);
                int statusCode = response.code();


                Headers headerList = response.headers();
                Log.d("testhistory", statusCode + " ");
                Log.e("response.body()", response.body().toString());


                if (response.isSuccessful()) {
                    try {
                        String a = new Gson().toJson(response.body());

                        JSONObject jObjRes = new JSONObject(a);
                        JSONObject jObjdata = jObjRes.getJSONObject("response");
                        JSONObject jObjResponse = jObjdata.getJSONObject("data");
                        JSONArray jObjcontacts = jObjResponse.getJSONArray("content");
                        /*if (jObjcontacts.length() < pagesize) {
                            isdataavailable = false;
                        } else {
                            isdataavailable = true;
                        }*/

                        if (jObjcontacts.length() == pagesize) {
                            isdataavailable = true;
                        } else {
                            isdataavailable = false;
                        }

                        Log.e("responsehistory", jObjcontacts.toString());

                        List<Chit> temp_orderlist = new ArrayList<>();
                        for (int i = 0; i < jObjcontacts.length(); i++) {
                            if (jObjcontacts.get(i) instanceof JSONObject) {
                                JSONObject jsnObj = (JSONObject) jObjcontacts.get(i);
                                Chit obj = new Gson().fromJson(jsnObj.toString(), Chit.class);
                                Log.e("alias", obj.getChit_name());
                                temp_orderlist.add(obj);
                            }
                        }

                        if (pagecount == 1) {
                            orderlist.clear();
                        }
                        orderlist.addAll(temp_orderlist);
                        mAdapter.notifyDataSetChanged();
                    } catch (Exception e) {
                        Log.e("errortest", e.getMessage());

                        Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                } else {
                    Log.e("test", " trdds1");

                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Toast.makeText(getActivity(), jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // Log error here since request failed
                mSwipyRefreshLayout.setRefreshing(false);
                Log.e("error message", t.toString());
            }
        });
    }


    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {

    }

    @Override
    public void onRefresh(SwipyRefreshLayoutDirection direction) {

        Log.e("scroll direction", "Refresh triggered at "
                + (direction == SwipyRefreshLayoutDirection.TOP ? "top" : "bottom"));
        if (direction == SwipyRefreshLayoutDirection.TOP) {
            isdataavailable = true;
            pagecount = 1;
            usersignin(sessionManager.getsession(), pagecount, false);
        } else if (direction == SwipyRefreshLayoutDirection.BOTTOM) {
            if (isdataavailable) {
                pagecount++;
                usersignin(sessionManager.getsession(), pagecount, false);
            } else {
                mSwipyRefreshLayout.setRefreshing(false);
                Toast.makeText(getActivity(), "No more data available", Toast.LENGTH_LONG).show();
            }

        }


    }

    SwipyRefreshLayout mSwipyRefreshLayout;
    int pagecount = 1;
    boolean isdataavailable = true;
    int pagesize = 25;

    public void refreshlist() {

    }




    /*@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        if(id ==R.id.action_delete)
        {
            new CustomDialogClass(getActivity(), new CustomDialogClass.OnDialogClickListener() {
                @Override
                public void onDialogImageRunClick(int positon) {
                    if (positon == 0) {

                    } else if (positon == 1) {
                        //str_actionid = str_actionid.replaceAll(",$", "");
                        Log.e("str_actionid",str_actionid);
                        removechit(sessionString,str_actionid, false);
                    }
                }
            },"Are you sure you want to delete "+totalcount+" chit?").show();
        }

        return super.onOptionsItemSelected(item);
    }*/


    public void removechit(final String usersession, String chitIds, final boolean isswipe) {

        ApiInterface apiService =
                ApiClient.getClient(getActivity()).create(ApiInterface.class);
        JsonObject a1 = new JsonObject();
        try {

            a1.addProperty("chitIds", chitIds);
            a1.addProperty("actionCode", "10");


        } catch (JsonParseException e) {
            e.printStackTrace();
        }


        //Call call = apiService.removechit(usersession,chitIds+",","10");
        Call call = apiService.removechit(usersession, a1);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                int statusCode = response.code();
                Log.e("test", sessionString + "sessionstring");

                Headers headerList = response.headers();
                Log.d("testhistory", statusCode + " ");

                if (response.isSuccessful()) {
                    try {
                        Log.e("response.body()", response.body().toString());
                        String a = new Gson().toJson(response.body());
                        Toast.makeText(getActivity(), "Order Archived!", Toast.LENGTH_LONG).show();
                        //Inad.alerter("Order Removed!",HistoryActivity.this);

                    } catch (Exception e) {
                        Log.e("errortest", e.getMessage());
                        //Toast.makeText(HistoryActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }

                } else {
                    Log.e("test", " trdds1");

                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Toast.makeText(getActivity(), jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }

                totalcount = 0;
                str_actionid = "";
                rl_del.setVisibility(View.GONE);
              /*  MenuItem item1 = OrderTab.menu.findItem(R.id.action_delete);
                item1.setVisible(false);*/
                pagecount = 1;
                isdataavailable = true;
                usersignin(usersession, pagecount, true);

            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // Log error here since request failed
                Log.e("error message", t.toString());
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && totalcount > 0) {
                    if (totalcount > 0) {
                        try {
                            for (int i = 0; i < orderlist.size(); i++) {
                                if (orderlist.get(i).isIslongpress()) {
                                    orderlist.get(i).setIslongpress(false);
                                }
                            }

                            totalcount = 0;
                            str_actionid = "";
                            mAdapter.notifyDataSetChanged();
                            rl_del.setVisibility(View.GONE);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                    return true;
                }
                return false;
            }
        });

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && isLoaded) {
            usersignin(sessionManager.getsession(), 1, false);
        }
    }


}
