package com.cab.digicafe.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cab.digicafe.R;

public class TempGoneFragment extends Fragment {

    // TODO: Rename and change types and number of parameters
    public static TempGoneFragment newInstance() {
        TempGoneFragment fragment = new TempGoneFragment();
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.activity_temp_gone_fragment, container, false);
        return v;
    }
}
