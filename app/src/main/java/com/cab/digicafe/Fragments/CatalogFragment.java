package com.cab.digicafe.Fragments;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.cab.digicafe.Adapter.CatalogTabAdapter;
import com.cab.digicafe.Helper.SessionManager;
import com.cab.digicafe.R;
import com.cab.digicafe.Rest.ApiClient;
import com.cab.digicafe.Rest.ApiInterface;
import com.google.gson.Gson;
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CatalogFragment extends Fragment implements CatalogTabAdapter.OnItemClickListener, SwipyRefreshLayout.OnRefreshListener {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private RecyclerView recyclerView;
    private CatalogTabAdapter mAdapter;
    private OnFragmentInteractionListener mListener;
    SessionManager usersession;
    int pagecount;

    public CatalogFragment() {
        // Required empty public constructor
    }


    public static CatalogFragment newInstance(String param1, String param2) {
        CatalogFragment fragment = new CatalogFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_catalog, container, false);
        recyclerView = (RecyclerView) v.findViewById(R.id.recycler_view);
        mSwipyRefreshLayout = (SwipyRefreshLayout) v.findViewById(R.id.swipyrefreshlayout);
        usersession = new SessionManager(getActivity());
        pagecount = 1;
        pagesize = 25;

        mAdapter = new CatalogTabAdapter(productlist, getContext(), this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        DividerItemDecoration verticalDecoration = new DividerItemDecoration(recyclerView.getContext(),
                DividerItemDecoration.VERTICAL);
        Drawable verticalDivider = ContextCompat.getDrawable(getActivity(), R.drawable.horizontal_divider);
        verticalDecoration.setDrawable(verticalDivider);
        recyclerView.addItemDecoration(verticalDecoration);

        recyclerView.setAdapter(mAdapter);

        mSwipyRefreshLayout.setOnRefreshListener(this);
        usersignin(pagecount);
        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            // throw new RuntimeException(context.toString()
            //       + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onItemClick(CatalougeContent item) {

    }

    @Override
    public void onImgClick(CatalougeContent item) {

    }

    @Override
    public void onItemLongClick(CatalougeContent item) {

    }

    @Override
    public void onadditem(CatalougeContent qty) {

    }

    @Override
    public void ondecreasecount(CatalougeContent qty) {

    }

    @Override
    public void onRefresh(SwipyRefreshLayoutDirection direction) {

        mSwipyRefreshLayout.setRefreshing(false);

        if (direction == SwipyRefreshLayoutDirection.TOP) {
            isdataavailable = true;
            pagecount = 1;
            Log.e("scroll direction", "top");
            productlist = new ArrayList<>();

            usersignin(pagecount);
        } else if (direction == SwipyRefreshLayoutDirection.BOTTOM) {
            Log.e("scroll direction", "bottom");
            if (isdataavailable) {
                pagecount++;
                usersignin(pagecount);
            } else {
                Toast.makeText(getActivity(), "No more data available", Toast.LENGTH_LONG).show();
            }

        }


    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    public void usersignin(final int pagecount) {
        ApiInterface apiService =
                ApiClient.getClient(getActivity()).create(ApiInterface.class);

        Call call = apiService.getCatalouge(usersession.getsession(), pagecount, "", "", "", "");

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {

                mSwipyRefreshLayout.setRefreshing(false);
                if (response.isSuccessful()) {
                    try {
                        String a = new Gson().toJson(response.body());
                        Log.e("catalogresponse", a);
                        JSONObject jObjRes = new JSONObject(a);
                        JSONObject jObjdata = jObjRes.getJSONObject("response");
                        JSONObject jObjResponse = jObjdata.getJSONObject("data");
                        String totalRecord = jObjResponse.getString("totalRecord");
                        JSONArray jObjcontacts = jObjResponse.getJSONArray("content");

                        String displayEndRecord = jObjResponse.getString("displayEndRecord");
                        if (totalRecord.equals(displayEndRecord)) {
                            isdataavailable = false;
                        } else {
                            isdataavailable = true;
                        }

                        if (pagecount == 1) {
                            productlist = new ArrayList<CatalougeContent>();
                        }
                        for (int i = 0; i < jObjcontacts.length(); i++) {
                            if (jObjcontacts.get(i) instanceof JSONObject) {
                                JSONObject jsnObj = (JSONObject) jObjcontacts.get(i);

                                CatalougeContent obj = new Gson().fromJson(jsnObj.toString(), CatalougeContent.class);
                                productlist.add(obj);
                            }
                        }


                        //mAdapter.setitem(productlist);
                        //mAdapter.notifyDataSetChanged();
                    } catch (Exception e) {


                        Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
                    }

                } else {


                    try {
                    } catch (Exception e) {
                        Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }

                //  List<Movie> movies = response.body().getResults();
                //  recyclerView.setAdapter(new MoviesAdapter(movies, R.layout.list_item_movie, getApplicationContext()));
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // Log error here since request failed
                mSwipyRefreshLayout.setRefreshing(false);

            }
        });
    }

    int pagesize;
    private List<CatalougeContent> productlist = new ArrayList<>();
    SwipyRefreshLayout mSwipyRefreshLayout;
    boolean isdataavailable;
}
