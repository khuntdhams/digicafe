package com.cab.digicafe.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.cab.digicafe.Helper.SessionManager;
import com.cab.digicafe.MainActivity;
import com.cab.digicafe.Model.ChitDetails;
import com.cab.digicafe.R;

import java.util.ArrayList;
import java.util.List;

public class OrderTab extends AppCompatActivity {


    private static ViewPager viewPager;
    private static TabLayout tabLayout;
    SessionManager usersession;
    HistoryFragmentNew mDetailfragment;
    String datevalue;
    ViewPagerAdapter adapter;
    String chit_id;
    String chit_hash_id;
    String chit_count;
    String chit_price, purpose, Refid, Caseid;
    ChitDetails chitinfo;
    String chitname;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_tab);


        viewPager = (ViewPager) findViewById(R.id.viewPager);


        // setupViewPager(viewPager);
        usersession = new SessionManager(OrderTab.this);

        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                chit_hash_id = null;
                chit_id = null;
                chitname = null;
            } else {
                chit_hash_id = extras.getString("chit_hash_id");
                chit_id = extras.getString("chit_id");
                chitname = extras.getString("transtactionid");
                chit_count = extras.getString("productqty");
                chit_price = extras.getString("productprice");
                purpose = extras.getString("purpose");
                Refid = extras.getString("Refid");
                Caseid = extras.getString("Caseid");


            }
        } else {
            chit_id = (String) savedInstanceState.getSerializable("bridgeidval");

            chit_hash_id = (String) savedInstanceState.getSerializable("bridgeidval");
            chitname = (String) savedInstanceState.getSerializable("transtactionid");
            chit_count = (String) savedInstanceState.getSerializable("productqty");
            chit_price = (String) savedInstanceState.getSerializable("productprice");
            purpose = (String) savedInstanceState.getSerializable("purpose");

        }

        //getSupportActionBar().setTitle(chitname);
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        tabLayout.setupWithViewPager(viewPager);//setting tab over viewpager

        setupViewPager(viewPager);

        findViewById(R.id.iv_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }


    class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();//fragment arraylist
        private final List<String> mFragmentTitleList = new ArrayList<>();//title arraylist

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

        //adding fragments and title method
        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


    private void setupViewPager(ViewPager viewPager) {
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        mDetailfragment = new HistoryFragmentNew();

        mDetailfragment.newInstance(chit_count, chit_price);
        adapter.addFrag(mDetailfragment, "Order Created");

        ArchivedFragmentNew frag1 = new ArchivedFragmentNew();
        adapter.addFrag(frag1, "Order Archived");

        viewPager.setAdapter(adapter);
    }


    @Override
    public void onBackPressed() {
        Intent mainIntent = new Intent(OrderTab.this, MainActivity.class);
        mainIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        SessionManager sessionManager = new SessionManager(this);
        mainIntent.putExtra("session", sessionManager.getsession());

        startActivity(mainIntent);
        finish();
    }
}
