package com.cab.digicafe.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.cab.digicafe.Adapter.taxGroup.TaxGroupListAdapter;
import com.cab.digicafe.Adapter.taxGroup.TaxGroupModel;
import com.cab.digicafe.Helper.Inad;
import com.cab.digicafe.Model.Chit;
import com.cab.digicafe.Model.ChitDetails;
import com.cab.digicafe.MyCustomClass.JsonObjParse;
import com.cab.digicafe.R;
import com.cab.digicafe.Timeline.DateTimeUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SummaryDetailFragmnet extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    String currency = "";
    private String mParam1;
    private String mParam2;
    public static String footer_note;
    public static Chit item;
    public static ChitDetails chitinfoobj;
    TextView billamount, stategstamount, centralgstamount, grandtotalamount,
            caterername, tv_ps, tv_tid, txt_tid, tv_info, tvTax, tvReqDiscLbl;
    TableRow trSgst, trCgst, trTax;
    LinearLayout ll_payu;
    RelativeLayout rl_pay_status;

    // TextView tv_date, tv_address;
    public SummaryDetailFragmnet() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment LogFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SummaryDetailFragmnet newInstance(String param1, String param2) {
        SummaryDetailFragmnet fragment = new SummaryDetailFragmnet();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.activity_summary_detail_fragmnet, container, false);
        //currency = SharedPrefUserDetail.getString(getActivity(), SharedPrefUserDetail.symbol_native,"") + " ";

        ButterKnife.bind(this, v);
        currency = item.getSymbol_native() + " ";
        caterername = (TextView) v.findViewById(R.id.caterername);
        stategstamount = (TextView) v.findViewById(R.id.stategstamount);
        centralgstamount = (TextView) v.findViewById(R.id.centralgstamount);
        grandtotalamount = (TextView) v.findViewById(R.id.grandtotalamount);
        billamount = (TextView) v.findViewById(R.id.totalamount);
        tv_ps = (TextView) v.findViewById(R.id.tv_ps);
        tv_info = (TextView) v.findViewById(R.id.tv_info);
        tv_tid = (TextView) v.findViewById(R.id.tv_tid);
        txt_tid = (TextView) v.findViewById(R.id.txt_tid);
        ll_payu = (LinearLayout) v.findViewById(R.id.ll_payu);
        trSgst = (TableRow) v.findViewById(R.id.trSgst);
        trCgst = (TableRow) v.findViewById(R.id.trCgst);
        trTax = (TableRow) v.findViewById(R.id.trTax);
        tvTax = (TextView) v.findViewById(R.id.tvTax);
        tvReqDiscLbl = (TextView) v.findViewById(R.id.tvReqDiscLbl);
        rl_pay_status = (RelativeLayout) v.findViewById(R.id.rl_pay_status);

      /*  tv_date = (TextView) v.findViewById(R.id.tv_date);
        tv_address = (TextView) v.findViewById(R.id.tv_address);
*/
        try {
            JSONObject jo_summary = new JSONObject(item.getFooter_note());

            field_json_data = jo_summary.getString("field_json_data");
            Double totalcentralgst = Double.valueOf(jo_summary.getString("CGST"));
            //stategstamount.setText(currency+ String.format("%.2f", totalcentralgst));
            stategstamount.setText(currency + Inad.getCurrencyDecimal(totalcentralgst, getActivity()));

            Double totalstategst = Double.valueOf(jo_summary.getString("SGST"));
            //centralgstamount.setText(currency+ String.format("%.2f", totalstategst));
            centralgstamount.setText(currency + Inad.getCurrencyDecimal(totalstategst, getActivity()));

            Double vat = 0.0;
            if (jo_summary.has("VAT")) vat = Double.valueOf(jo_summary.getString("VAT"));
            tvTax.setText(currency + Inad.getCurrencyDecimal(vat, getActivity()));

            trCgst.setVisibility(View.GONE);
            trSgst.setVisibility(View.GONE);
            trTax.setVisibility(View.GONE);

            if (vat.doubleValue() == 0.0) {
                trTax.setVisibility(View.GONE);
            }
            if (totalcentralgst.doubleValue() == 0.0) trCgst.setVisibility(View.GONE);
            if (totalstategst.doubleValue() == 0.0) trSgst.setVisibility(View.GONE);


            Double grandtotal = Double.valueOf(jo_summary.getString("GRANDTOTAL"));
            //grandtotalamount.setText(currency+ String.format("%.2f", grandtotal));
            grandtotalamount.setText(currency + Inad.getCurrencyDecimal(grandtotal, getActivity()));
            trRoundOff.setVisibility(View.GONE);
            if (jo_summary.has("totalWithRoundOff")) {
                Double totalWithRoundOff = Double.valueOf(jo_summary.getString("totalWithRoundOff"));
                if (totalWithRoundOff > 0) {
                    trRoundOff.setVisibility(View.VISIBLE);
                    tvTotalRoundVal.setText(currency + Inad.getCurrencyDecimal(totalWithRoundOff, getActivity()));
                }
                //grandtotalamount.setText(currency+ String.format("%.2f", grandtotal));
            }

            Double total = Double.valueOf(jo_summary.getString("TOTALBILL"));
            //billamount.setText(currency+ String.format("%.2f", total));
            billamount.setText(currency + Inad.getCurrencyDecimal(total, getActivity()));

            if (jo_summary.getString("PAYMENT_MODE").equals("PAYUMONEY")) {
                ll_payu.setVisibility(View.VISIBLE);

                String payure = jo_summary.getString("payuResponse");

                JSONObject jo_pu = new JSONObject(payure);

                String result = jo_pu.getString("result");

                JSONObject jo_res = new JSONObject(result);

                tv_tid.setText(jo_res.getString("txnid"));
                tv_ps.setText(jo_res.getString("status"));

            } else if (jo_summary.getString("PAYMENT_MODE").equals("RAZORPAY")) {
                ll_payu.setVisibility(View.VISIBLE);
                rl_pay_status.setVisibility(View.GONE);
                String payure = jo_summary.getString("payuResponse");
                tv_tid.setText(payure);

            } else {
                ll_payu.setVisibility(View.GONE);
            }

            if (jo_summary.has("DeliveryInfo")) {
                JSONArray jsonArray = jo_summary.getJSONArray("DeliveryInfo");
                if (jsonArray.length() > 0) {
                    JSONObject joDelInfo = jsonArray.getJSONObject(0);
                    pick_up_only = joDelInfo.getString("pick_up_only");
                }
            }

            Double totalSavings = 0.0;
            trReqDiscount.setVisibility(View.GONE);
            if (jo_summary.has("TOTLREQDISC")) {
                Double totalReqDisc = Double.valueOf(jo_summary.getString("TOTLREQDISC"));
                totalSavings = totalReqDisc;
                if (totalReqDisc > 0) {
                    trReqDiscount.setVisibility(View.VISIBLE);
                    tvTotalReqDisc.setText("- " + currency + Inad.getCurrencyDecimal(totalReqDisc, getActivity()));
                }
            }

            if (jo_summary.has("TOTLPRODISC")) {
                Double totalProdDisc = Double.valueOf(jo_summary.getString("TOTLPRODISC"));
                totalSavings = totalSavings + totalProdDisc;
            }

            trCouponDiscount.setVisibility(View.GONE);

            if (jo_summary.has("COUPONDISCLBL")) {
                String couponLbl = jo_summary.getString("COUPONDISCLBL");
                tvCouponDiscLbl.setText(couponLbl);
                Double couponDisc = Double.valueOf(jo_summary.getString("COUPONDISC"));
                if (couponDisc > 0) {
                    totalSavings = totalSavings + couponDisc;
                    trCouponDiscount.setVisibility(View.VISIBLE);
                    tvTotalCouponDisc.setText("- " + currency + Inad.getCurrencyDecimal(couponDisc, getActivity()));
                }
            }

            tvSavings.setVisibility(View.GONE);
            if (totalSavings > 0) tvSavings.setVisibility(View.VISIBLE);
            tvSavings.setText("You have saved " + currency + "" + Inad.getCurrencyDecimal(totalSavings, getActivity()));

            if (jo_summary.has("REQ_DISC")) {
                Double ReqDisc = Double.valueOf(jo_summary.getString("REQ_DISC"));
                setReqDiscLbl(ReqDisc);
            }

            String taxArray = JsonObjParse.getValueEmpty(item.getFooter_note(), "taxArray");
            setTaxGroupRv(taxArray);

            checkConditions();

           /* tv_date.setText(jo_summary.getString("DATE"));
            tv_address.setText(jo_summary.getString("ADDRESS"));

            if(getString(R.string.isaddress).equals("1"))
            {

                v.findViewById(R.id.rl_address).setVisibility(View.VISIBLE);
            }
            else {
                v.findViewById(R.id.rl_address).setVisibility(View.GONE);
            }

            if(getString(R.string.isdate).equals("1"))
            {

                v.findViewById(R.id.rl_del_date).setVisibility(View.VISIBLE);
            }
            else {
                v.findViewById(R.id.rl_del_date).setVisibility(View.GONE);
            }
*/
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        caterername.setText(item.getSubject());

        //Chitlog cllog = chitinfoobj.getHeaderLog();
        if (chitinfoobj.getHeaderLog() != null && chitinfoobj.getHeaderLog().size() > 0) {
            String dtStart = chitinfoobj.getHeaderLog().get(0).getAction_date();

           /* SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            try {
                Date date = format.parse(dtStart);
                System.out.println(date);

                SimpleDateFormat todateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
                String dateTime = todateFormat.format(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            */
            dtStart = DateTimeUtils.parseDateTime(dtStart, "yyyy-MM-dd HH:mm", "dd-MMM-yyyy hh:mm a");
            //tv_date.setText(dtStart);
        }

        setCharges();

        return v;
    }

    /// Charges

    @BindView(R.id.trCharge1)
    TableRow trCharge1;
    @BindView(R.id.tvChargeNm1)
    TextView tvChargeNm1;
    @BindView(R.id.tvChargeVal1)
    TextView tvCharegVal1;

    @BindView(R.id.trCharge2)
    TableRow trCharge2;
    @BindView(R.id.tvChargeNm2)
    TextView tvChargeNm2;
    @BindView(R.id.tvChargeVal2)
    TextView tvCharegVal2;

    @BindView(R.id.trReqDiscount)
    TableRow trReqDiscount;
    @BindView(R.id.tvTotalReqDisc)
    TextView tvTotalReqDisc;

    @BindView(R.id.trCouponDiscount)
    TableRow trCouponDiscount;
    @BindView(R.id.tvTotalCouponDisc)
    TextView tvTotalCouponDisc;
    @BindView(R.id.tvCouponDiscLbl)
    TextView tvCouponDiscLbl;

    @BindView(R.id.tvSavings)
    TextView tvSavings;

    Double charge1 = 0.0, charge2 = 0.0;
    String field_json_data = "";

    public void setCharges() {

        try {

            trCharge2.setVisibility(View.GONE);
            trCharge1.setVisibility(View.GONE);

            if (field_json_data != null && !field_json_data.isEmpty()) {

                JSONObject joField = new JSONObject(field_json_data);
                JSONArray arr = joField.getJSONArray("charges");

                JSONObject element;

                tvChargeNm1.setText("");
                tvCharegVal1.setText("");

                for (int i = 0; i < arr.length(); i++) {
                    element = arr.getJSONObject(i); // which for example will be Types,TotalPoints,ExpiringToday in the case of the first array(All_Details)

                    Iterator keys = element.keys();
                    while (keys.hasNext()) {
                        try {
                            String key = (String) keys.next();

                            String taxVal = JsonObjParse.getValueFromJsonObj(element, key);

                            if (tvChargeNm1.getText().toString().isEmpty()) {
                                if (taxVal.isEmpty()) taxVal = "0";
                                charge1 = Double.valueOf(taxVal);
                                tvChargeNm1.setText(key);
                                if (charge1 > 0) trCharge1.setVisibility(View.VISIBLE);
                            } else {
                                if (taxVal.isEmpty()) taxVal = "0";
                                charge2 = Double.valueOf(taxVal);
                                tvChargeNm2.setText(key);
                                if (charge2 > 0) trCharge2.setVisibility(View.VISIBLE);
                            }

                            Log.e("key", key);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

                tvCharegVal1.setText("+ " + currency + Inad.getCurrencyDecimal(charge1, getActivity()));
                tvCharegVal2.setText("+ " + currency + Inad.getCurrencyDecimal(charge2, getActivity()));

            }

            if (pick_up_only.equalsIgnoreCase("yes")) {
                trCharge2.setVisibility(View.GONE);
                trCharge1.setVisibility(View.GONE);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    String pick_up_only = "";

    @BindView(R.id.rvTaxGroup)
    RecyclerView rvTaxGroup;

    TaxGroupListAdapter taxGroupListAdapter;
    ArrayList<TaxGroupModel> alTax = new ArrayList<>();

    public void setTaxGroupRv(String taxArray) {

        alTax = new Gson().fromJson(taxArray, new TypeToken<ArrayList<TaxGroupModel>>() {
        }.getType());
        if (alTax == null) alTax = new ArrayList<>();
        taxGroupListAdapter = new TaxGroupListAdapter(alTax, getActivity());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        rvTaxGroup.setLayoutManager(mLayoutManager);
        rvTaxGroup.setAdapter(taxGroupListAdapter);
    }
    //  round off

    @BindView(R.id.tvTotalRoundVal)
    TextView tvTotalRoundVal;

    @BindView(R.id.trRoundOff)
    TableRow trRoundOff;

    @BindView(R.id.tvConditions)
    TextView tvConditions;

    @BindView(R.id.llConditions)
    LinearLayout llConditions;

    public void checkConditions() {
        String conditions = JsonObjParse.getValueEmpty(field_json_data, "conditions");
        llConditions.setVisibility(View.GONE);
        if (!conditions.isEmpty()) {
            tvConditions.setText(conditions + "");
            llConditions.setVisibility(View.VISIBLE);
        }
    }

    public void setReqDiscLbl(double disc) {
        if (disc <= 0) {
            tvReqDiscLbl.setText("Discount" + " (" + "MAX." + ")");
        } else {
            tvReqDiscLbl.setText("Discount" + " @ " + disc + " %");
        }
    }
}