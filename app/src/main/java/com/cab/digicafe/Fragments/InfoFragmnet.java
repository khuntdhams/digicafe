package com.cab.digicafe.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cab.digicafe.Helper.Inad;
import com.cab.digicafe.Model.AddAddress;
import com.cab.digicafe.MyCustomClass.JsonObjParse;
import com.cab.digicafe.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class InfoFragmnet extends Fragment {


    public InfoFragmnet() {

    }


    public static InfoFragmnet newInstance(String param1, String param2) {
        InfoFragmnet fragment = new InfoFragmnet();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_info, container, false);
        //currency = SharedPrefUserDetail.getString(getActivity(), SharedPrefUserDetail.symbol_native,"") + " ";

        ButterKnife.bind(this, v);

        setInfoData();

        return v;
    }

    @BindView(R.id.llDelDt)
    LinearLayout llDelDt;

    @BindView(R.id.llDobSurvey)
    LinearLayout llDobSurvey;

    @BindView(R.id.tvDob)
    TextView tvDob;

    @BindView(R.id.llDelAdd)
    LinearLayout llDelAdd;

    @BindView(R.id.tvCaterername)
    TextView tvCaterername;

    @BindView(R.id.tvLiceInfo)
    TextView tvLiceInfo;


    @BindView(R.id.tvBusNm)
    TextView tvBusNm;

    @BindView(R.id.tvBusNo)
    TextView tvBusNo;

    @BindView(R.id.tvConsumrNm)
    TextView tvConsumrNm;

    @BindView(R.id.tvConsNo)
    TextView tvConsNo;

    @BindView(R.id.tvDelDate)
    TextView tvDelDate;

    @BindView(R.id.tvDelAdd)
    TextView tvDelAdd;

    @BindView(R.id.tvDelNm)
    TextView tvDelNm;

    @BindView(R.id.tvDelNo)
    TextView tvDelNo;

    @BindView(R.id.llLicence)
    LinearLayout llLicence;

    @BindView(R.id.llRequiredBy)
    LinearLayout llRequiredBy;

    @BindView(R.id.tvReqBy)
    TextView tvReqBy;

    @BindView(R.id.txtDelOrPropDate)
    TextView txtDelOrPropDate;

    @BindView(R.id.txtDelOrCustAdd)
    TextView txtDelOrCustAdd;

    ArrayList<AddAddress> addressList = new ArrayList<>();
    String purpose = "";

    @BindView(R.id.tvCustomer)
    TextView tvCustomer;

    @BindView(R.id.tvBusiness)
    TextView tvBusiness;

    public void setInfoData() {
        try {

            JSONObject jsonObject = new JSONObject(SummaryDetailFragmnet.item.getFooter_note());

            purpose = SummaryDetailFragmnet.item.getPurpose();

            if (purpose.equalsIgnoreCase(getString(R.string.delivery))) {
                tvBusiness.setText("From Details");
                tvCustomer.setText("To Details");
            } else {
                tvBusiness.setText("Business Details");
                tvCustomer.setText("Customer Details");
            }
            llRequiredBy.setVisibility(View.GONE);

            tvCaterername.setText(SummaryDetailFragmnet.item.getSubject());

            String ConsumerInfo = JsonObjParse.getValueEmpty(SummaryDetailFragmnet.item.getFooter_note(), "ConsumerInfo");
            if (!ConsumerInfo.isEmpty()) {
                JSONArray jaConsumerInfo = new JSONArray(ConsumerInfo);
                if (jaConsumerInfo.length() > 0) {
                    JSONObject joConsInfo = jaConsumerInfo.getJSONObject(0);
                    tvConsumrNm.setText(joConsInfo.getString("customer_nm"));
                    tvReqBy.setText(joConsInfo.getString("customer_nm"));
                    tvConsNo.setText(joConsInfo.getString("mobile"));
                }
            }

            String DATE = JsonObjParse.getValueEmpty(SummaryDetailFragmnet.item.getFooter_note(), "DATE");
            String DOB = JsonObjParse.getValueEmpty(SummaryDetailFragmnet.item.getFooter_note(), "DOB");
            tvDelDate.setText(DATE);
            tvDob.setText(DOB);

            txtDelOrCustAdd.setText("Delivery AddAddress");
            txtDelOrPropDate.setText("Delivery Date");

            tvConsumrNm.setText(jsonObject.getString("Cust_Nm"));

            if (purpose.equalsIgnoreCase(getActivity().getString(R.string.property))) {

                txtDelOrCustAdd.setText("AddAddress");
                txtDelOrPropDate.setText("Date");

                if (jsonObject.has("Cust_Nm")) {

                    llRequiredBy.setVisibility(View.VISIBLE);

                    tvDelDate.setText(SummaryDetailFragmnet.item.getCreated_date());

                    tvConsumrNm.setText(jsonObject.getString("Cust_Nm"));

                    if (jsonObject.getString("Cust_Nm").equals("")) {


                    } else {

                    }
                }
            }

            String BusinessInfo = JsonObjParse.getValueEmpty(SummaryDetailFragmnet.item.getFooter_note(), "BusinessInfo");
            String ADDRESS = JsonObjParse.getValueEmpty(SummaryDetailFragmnet.item.getFooter_note(), "ADDRESS");

            if (ADDRESS != null && !ADDRESS.isEmpty()) {
                tvDelAdd.setText(ADDRESS);
                try {
                    JSONArray array = new JSONArray(ADDRESS);
                    String type, address;
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject obj = array.getJSONObject(i);
                        type = obj.getString("addressType");
                        address = obj.getString("address");
                        addressList.add(new AddAddress(type, address));
                    }
                    tvDelAdd.setText(addressList.get(0).getAddress());
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {
                ADDRESS = "";
            }
            //tvDelAdd.setText(ADDRESS);

            if (!BusinessInfo.isEmpty()) {
                JSONArray jaBusinessInfo = new JSONArray(BusinessInfo);
                if (jaBusinessInfo.length() > 0) {
                    JSONObject joConsInfo = jaBusinessInfo.getJSONObject(0);
                    tvBusNm.setText(joConsInfo.getString("customer_nm"));
                    tvBusNo.setText(joConsInfo.getString("mobile"));
                }
            }

            String pick_up_only = "";

            if (jsonObject.has("field_json_data")) {
                String field_json_data = JsonObjParse.getValueEmpty(SummaryDetailFragmnet.item.getFooter_note(), "field_json_data");
                String licence_info = JsonObjParse.getValueEmpty(field_json_data, "licence_info");
                llLicence.setVisibility(View.VISIBLE);
                if (licence_info.isEmpty()) llLicence.setVisibility(View.GONE);
                tvLiceInfo.setText(licence_info);
                pick_up_only = JsonObjParse.getValueEmpty(field_json_data, "pick_up_only");
            }


            String DeliveryInfo = JsonObjParse.getValueEmpty(SummaryDetailFragmnet.item.getFooter_note(), "DeliveryInfo");
            if (!DeliveryInfo.isEmpty()) {
                JSONArray jaDeliveryInfo = new JSONArray(DeliveryInfo);
                if (jaDeliveryInfo.length() > 0) {
                    JSONObject joConsInfo = jaDeliveryInfo.getJSONObject(0);
                    tvDelNm.setText(joConsInfo.getString("customer_nm"));
                    tvDelNo.setText(joConsInfo.getString("mobile"));

                    if (pick_up_only.equalsIgnoreCase("Yes")) {
                        tvDelAdd.setText("Pickup Only");

                    } else {
                        pick_up_only = JsonObjParse.getValueEmpty(joConsInfo.toString(), "pick_up_only");
                        if (pick_up_only.equalsIgnoreCase("Yes")) {
                            tvDelAdd.setText("Self Pickup");
                        }
                    }


                }
            }

            llDobSurvey.setVisibility(View.GONE);

            if (purpose.equalsIgnoreCase(getActivity().getString(R.string.survey))) {
                llDobSurvey.setVisibility(View.VISIBLE);
                llDelDt.setVisibility(View.GONE);
                llDelAdd.setVisibility(View.GONE);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

        tvBusNo.setTextColor(ContextCompat.getColor(getActivity(), R.color.callclr));

        tvBusNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Inad.callIntent(tvBusNo, getActivity());
            }
        });


        tvConsNo.setTextColor(ContextCompat.getColor(getActivity(), R.color.callclr));

        tvConsNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Inad.callIntent(tvConsNo, getActivity());
            }
        });

    }

}
