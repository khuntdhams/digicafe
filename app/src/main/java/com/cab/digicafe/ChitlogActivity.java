package com.cab.digicafe;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.cab.digicafe.Activities.AllTaskActivity;
import com.cab.digicafe.Activities.MySupplierActivity;
import com.cab.digicafe.Activities.MyTaskActivity;
import com.cab.digicafe.Activities.RequestApis.MyRequestCall;
import com.cab.digicafe.Activities.RequestApis.MyRequst;
import com.cab.digicafe.Activities.mrp.AnimDialogueWithFieldForExternalLink;
import com.cab.digicafe.Activities.mrp.MrpFragmnet;
import com.cab.digicafe.Activities.mrp.PlanningDialogue;
import com.cab.digicafe.Activities.mrp.SortBySelectDialogue;
import com.cab.digicafe.Fragments.InfoFragmnet;
import com.cab.digicafe.Fragments.SummaryDetailFragmnet;
import com.cab.digicafe.Helper.Constants;
import com.cab.digicafe.Helper.SessionManager;
import com.cab.digicafe.Helper.SharedPrefUserDetail;
import com.cab.digicafe.Model.Chit;
import com.cab.digicafe.Model.ChitDetails;
import com.cab.digicafe.Model.Chitproducts;
import com.cab.digicafe.Model.ShareModel;
import com.cab.digicafe.MyCustomClass.JsonObjParse;
import com.cab.digicafe.MyCustomClass.PrintReceipt;
import com.cab.digicafe.Rest.ApiClient;
import com.cab.digicafe.Rest.ApiInterface;
import com.google.gson.Gson;
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.cab.digicafe.Activities.mrp.MrpFragmnet.vPos;

public class ChitlogActivity extends PrintReceipt {
    private static Toolbar toolbar;
    private static ViewPager viewPager;
    LinearLayout llAddSupplIfMrpForAT, llInternalLinkToOrder, llLinkToOther, llLink, llResource, llPlanning, llSortDateDesc;
    private static TabLayout tabLayout;
    public static SessionManager usersession;
    DetailsFragment mDetailfragment;
    String datevalue;
    ViewPagerAdapter adapter;
    String chit_id;
    String chit_hash_id;
    String chit_count;
    String chit_price, purpose, Refid, Caseid;
    ChitDetails chitinfo;
    String chitname;
    public static boolean isprint = false;
    public static boolean iscomment = false;
    boolean isloadaddress = false;
    boolean isMrp = false;
    boolean isMrpTabOnly = false;

    boolean isEnquiry = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chitlog);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        llAddSupplIfMrpForAT = (LinearLayout) findViewById(R.id.llAddSupplIfMrpForAT);
        llInternalLinkToOrder = (LinearLayout) findViewById(R.id.llInternalLinkToOrder);
        llLinkToOther = (LinearLayout) findViewById(R.id.llLinkToOther);
        llLink = (LinearLayout) findViewById(R.id.llLink);
        llPlanning = (LinearLayout) findViewById(R.id.llPlanning);
        llSortDateDesc = (LinearLayout) findViewById(R.id.llSortDateDesc);
        llResource = (LinearLayout) findViewById(R.id.llResource);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        // setupViewPager(viewPager);
        usersession = new SessionManager(ChitlogActivity.this);

        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                chit_hash_id = null;
                chit_id = null;
                chitname = null;
            } else {
                chit_hash_id = extras.getString("chit_hash_id");
                chit_id = extras.getString("chit_id");
                chitname = extras.getString("transtactionid");
                chit_count = extras.getString("productqty");
                chit_price = extras.getString("productprice");
                purpose = extras.getString("purpose");
                Refid = extras.getString("Refid");
                Caseid = extras.getString("Caseid");
                isprint = extras.getBoolean("isprint", false);
                iscomment = extras.getBoolean("iscomment", false);
                isEnquiry = extras.getBoolean("isEnquiry", false);
                isMrp = extras.getBoolean("isMrp", false);
                isMrpTabOnly = extras.getBoolean("isMrpTabOnly", false); // From Ordrr
            }
        } else {
            chit_id = (String) savedInstanceState.getSerializable("bridgeidval");
            chit_hash_id = (String) savedInstanceState.getSerializable("bridgeidval");
            chitname = (String) savedInstanceState.getSerializable("transtactionid");
            chit_count = (String) savedInstanceState.getSerializable("productqty");
            chit_price = (String) savedInstanceState.getSerializable("productprice");
            purpose = (String) savedInstanceState.getSerializable("purpose");
        }
        getproductdetails(usersession.getsession());
        //getSupportActionBar().setTitle(chitname);
        getSupportActionBar().setTitle(purpose + " / " + Refid + " / " + Caseid);
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        tabLayout.setupWithViewPager(viewPager);//setting tab over viewpager

        llAddSupplIfMrpForAT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                SharedPrefUserDetail.setBoolean(ChitlogActivity.this, SharedPrefUserDetail.isfromsupplier, true);
                Intent i = new Intent(ChitlogActivity.this, MySupplierActivity.class);
                i.putExtra("fromAT", true);
                i.putExtra("isBk", true);
                i.putExtra("isFromMrp", true);
                startActivity(i);
            }
        });

        llInternalLinkToOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(ChitlogActivity.this, HistoryActivity.class);
                i.putExtra("fromAT", true);
                i.putExtra("isBk", true);
                i.putExtra("session", sessionManager.getsession());
                startActivityForResult(i, 91);

            }
        });

        llLinkToOther.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AnimDialogueWithFieldForExternalLink commonConfirmDialogue = new AnimDialogueWithFieldForExternalLink(ChitlogActivity.this, new AnimDialogueWithFieldForExternalLink.OnDialogClickListener() {
                    @Override
                    public void onDialogImageRunClick(int joMrp) {

                        if (joMrp == 2) {
                            viewPager.setCurrentItem(mrpPos);
                            final Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    if (vPos == 1) {
                                        mrpFragmnet.onRefresh(SwipyRefreshLayoutDirection.TOP);
                                    } else if (vPos == 4) {
                                        mrpFragmnet.onRefreshDelivery(SwipyRefreshLayoutDirection.TOP);
                                    }


                                }
                            }, 500);
                        }

                    }
                }, " Add material", getString(R.string.link_other));

                commonConfirmDialogue.show();


            }
        });


        llResource.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AnimDialogueWithFieldForExternalLink commonConfirmDialogue = new AnimDialogueWithFieldForExternalLink(ChitlogActivity.this, new AnimDialogueWithFieldForExternalLink.OnDialogClickListener() {
                    @Override
                    public void onDialogImageRunClick(int joMrp) {

                        if (joMrp == 2) {
                            viewPager.setCurrentItem(mrpPos);
                            final Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    mrpFragmnet.onRefreshResource(SwipyRefreshLayoutDirection.TOP);

                                }
                            }, 500);
                        }


                    }
                }, " Add resource", getString(R.string.resources));

                commonConfirmDialogue.show();

            }
        });

        llPlanning.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PlanningDialogue planningDialogue = new PlanningDialogue(ChitlogActivity.this, new PlanningDialogue.OnDialogClickListener() {
                    @Override
                    public void onDialogImageRunClick(int joMrp) {

                        if (joMrp == 2) {
                            viewPager.setCurrentItem(mrpPos);
                            final Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    mrpFragmnet.onRefreshPlann(SwipyRefreshLayoutDirection.TOP);

                                }
                            }, 500);
                        }


                    }
                }, " Add sub-task", getString(R.string.planning));

                planningDialogue.show();

            }
        });


        llLink.setVisibility(View.GONE);


        llSortDateDesc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SortBySelectDialogue planningDialogue = new SortBySelectDialogue(ChitlogActivity.this, new SortBySelectDialogue.OnDialogClickListener() {
                    @Override
                    public void onDialogImageRunClick(int pos, int sort, int order) {

                        if (pos == 1) {
                            sortBy = sort;
                            orderBy = order;
                            ShareModel.getI().sortBy = sortBy;
                            ShareModel.getI().orderBy = orderBy;
                            mrpFragmnet.onRefreshAll(SwipyRefreshLayoutDirection.TOP, sortBy, orderBy);
                        }
                    }
                }, " Sorting", sortBy, orderBy);

                planningDialogue.show();

            }
        });


    }

    int sortBy = -1, orderBy = 0;
    SessionManager sessionManager = new SessionManager();

    public void checkMrpFlag(final ViewPager viewPager) {


        /*if (isMrpForAllTask) { // For Business and employee

            String username = "";

            String is_mrp = getValueFromFieldJson("is_mrp");
            if (is_mrp.equalsIgnoreCase("no")) {
                llLink.setVisibility(View.GONE);
            } else {
                llLink.setVisibility(View.VISIBLE);
                adapter.addFrag(mrpFragmnet, "M.R.P");

            }


        }*/


          /*  if(sessionManager.getisBusinesslogic())
            {
                username = sessionManager.getcurrentu_nm();
            }
            else {
                String empUseName = sessionManager.getcurrentu_nm();
                int indexOfannot = empUseName.indexOf("@") + 1;
                username = empUseName.substring(indexOfannot, empUseName.indexOf("."));
            }

            if (username.isEmpty()) {
                return;
            }


            new GetServiceTypeFromBusiUserProfile(this, username, new GetServiceTypeFromBusiUserProfile.Apicallback() {
                @Override
                public void onGetResponse(String a, String response, String sms_emp) {

                    String field_json_data = "";
                    try {

                        JSONObject Jsonresponse = new JSONObject(response);
                        JSONObject Jsonresponseinfo = Jsonresponse.getJSONObject("response");
                        JSONArray jaData = Jsonresponseinfo.getJSONArray("data");
                        if (jaData.length() > 0) {
                            JSONObject data = jaData.getJSONObject(0);
                            field_json_data = JsonObjParse.getValueEmpty(data.toString(), "field_json_data");

                            String is_mrp = JsonObjParse.getValueEmpty(field_json_data, "is_mrp");
                            if (is_mrp.equalsIgnoreCase("no")) {
                                llAddSupplIfMrpForAT.setVisibility(View.GONE);
                            }
                            else {
                                llAddSupplIfMrpForAT.setVisibility(View.VISIBLE);
                            }
                        } else {

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });*/

        if (sessionManager.getIsEmployess() && isMrp) { // MRP tabs and buttons

            getBusinessProfileForEmployee(new MyCallForFieldJson() {
                @Override
                public void call(String field_json) {


                    String is_mrp = JsonObjParse.getValueEmpty(field_json, "is_mrp");

                    if (is_mrp.equalsIgnoreCase("no")) {
                        llLink.setVisibility(View.GONE);
                    } else {

                        llLink.setVisibility(View.VISIBLE);

                        adapter.addFrag(mrpFragmnet, "M.R.P");
                        mrpPos = adapter.getCount() - 1;
                    }

                    viewPager.setAdapter(adapter);
                    if (isEnquiry) viewPager.setCurrentItem(2);
                    loadMaterial();


                }
            });

        } else if (sessionManager.getisBusinesslogic() && isMrp) {  // MRP tabs and buttons

            String is_mrp = getValueFromFieldJson("is_mrp");
            if (is_mrp.equalsIgnoreCase("no")) {
                llLink.setVisibility(View.GONE);
            } else {

                llLink.setVisibility(View.VISIBLE);


                //String MRP_TAB = JsonObjParse.getValueEmpty(DetailsFragment.item_detail.getFooter_note(), "MRP_TAB");
               /* if (!MRP_TAB.equalsIgnoreCase("") && !is_mrp.equalsIgnoreCase("no") ) adapter.addFrag(mrpFragmnet, "M.R.P");
                else if (ShareModel.getI().isFromAllTask && !is_mrp.equalsIgnoreCase("no")) adapter.addFrag(mrpFragmnet, "M.R.P");
*/
                adapter.addFrag(mrpFragmnet, "M.R.P");
                //mrpPos = adapter.getItemPosition(mrpFragmnet);
                mrpPos = adapter.getCount() - 1;

            }

            viewPager.setAdapter(adapter);
            if (isEnquiry) viewPager.setCurrentItem(2);
            loadMaterial();
        } else { // consumer

            viewPager.setAdapter(adapter);
            if (isEnquiry) viewPager.setCurrentItem(2);
            loadMaterial();

        }


    }

    private void setupViewPager(ViewPager viewPager) {
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        mDetailfragment = new DetailsFragment();


        mDetailfragment.newInstance(chit_count, chit_price);

        Log.e("productdetailsinfocount", chitinfo.getContent().size() + "");
        mDetailfragment.chitinfoobj = chitinfo;
        adapter.addFrag(mDetailfragment, "Det.");

        SummaryDetailFragmnet frag2 = new SummaryDetailFragmnet();
        frag2.newInstance("", "");
        SummaryDetailFragmnet summaryDetailFragmnet = new SummaryDetailFragmnet();
        summaryDetailFragmnet.chitinfoobj = chitinfo;
        if (!purpose.equalsIgnoreCase(getString(R.string.survey))) {
            adapter.addFrag(frag2, "Sum.");
        }

        LogFragment frag1 = new LogFragment();
        frag1.chitstatusinfoobj = chitinfo;
        frag1.newInstance("", "");
        Bundle bundle1 = new Bundle();
        bundle1.putString("datevalue", datevalue);
        bundle1.putString("chit_hash_id", chit_hash_id);
        bundle1.putString("chit_id", chit_id);
        bundle1.putString("sessionstring", usersession.getsession());
        frag1.setArguments(bundle1);
        adapter.addFrag(frag1, "Status");
        adapter.addFrag(new InfoFragmnet(), "Info.");
        checkMrpFlag(viewPager);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {

                Fragment fragment = adapter.getItem(i);
                if (fragment instanceof MrpFragmnet) {

                    if (isMrpTabOnly) {

                    } else {
                        llLink.setVisibility(View.VISIBLE);
                    }

                } else {
                    llLink.setVisibility(View.GONE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
        setLinkIcons(-1);
        llLink.setVisibility(View.GONE);
    }

    public void setLinkIcons(int mrpPos) {

        llPlanning.setVisibility(View.GONE);
        llSortDateDesc.setVisibility(View.GONE);
        llInternalLinkToOrder.setVisibility(View.GONE);
        llAddSupplIfMrpForAT.setVisibility(View.GONE);
        llResource.setVisibility(View.GONE);
        llLinkToOther.setVisibility(View.GONE);

        switch (mrpPos) {
            case 0:
                llSortDateDesc.setVisibility(View.VISIBLE);
                break;
            case 1:
                llLinkToOther.setVisibility(View.VISIBLE);
                llInternalLinkToOrder.setVisibility(View.VISIBLE);
                llAddSupplIfMrpForAT.setVisibility(View.VISIBLE);
                break;
            case 2:
                llResource.setVisibility(View.VISIBLE);
                break;
            case 3:
                llPlanning.setVisibility(View.VISIBLE);
                break;
            case 4:
                // llLinkToOther.setVisibility(View.VISIBLE);
                //  llInternalLinkToOrder.setVisibility(View.VISIBLE);
                llAddSupplIfMrpForAT.setVisibility(View.VISIBLE);
                break;
        }

    }

    class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();//fragment arraylist
        private final List<String> mFragmentTitleList = new ArrayList<>();//title arraylist

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

        //adding fragments and title method
        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    public void getproductdetails(final String sessionstring) {

        ApiInterface apiService =
                ApiClient.getClient(ChitlogActivity.this).create(ApiInterface.class);
        Call call = apiService.getchitDetail(sessionstring, chit_hash_id, chit_id, SummaryDetailFragmnet.item.getCurrency_code());
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {

                if (response.isSuccessful()) {
                    try {
                        String a = new Gson().toJson(response.body());

                        JSONObject jObjRes = new JSONObject(a);
                        JSONObject jObjdata = jObjRes.getJSONObject("response");
                        JSONObject jObjResponse = jObjdata.getJSONObject("data");
                        ChitDetails obj = new Gson().fromJson(jObjResponse.toString(), ChitDetails.class);
                        Log.e("chitdetaisresponse", obj.toString());
                        //  mDetailfragment.setchitdetails(obj);
                        chitinfo = obj;

                        productlist = chitinfo.getContent();

                        setupViewPager(viewPager);

                        getuserprofile();
                    } catch (Exception e) {
                        Log.e("errortest", e.getMessage());

                        Toast.makeText(ChitlogActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                } else {
                    Log.e("test", " trdds1");

                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Toast.makeText(ChitlogActivity.this, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(ChitlogActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }

            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // Log error here since request failed
                Log.e("error message", t.toString());
            }
        });
    }

    Menu menu;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);

        this.menu = menu;

        final MenuItem item = menu.findItem(R.id.action_search);
        item.setVisible(false);
        String status = chitprint.getTransaction_status();

        if (iscomment) {
            MenuItem item1 = menu.findItem(R.id.action_remark);
            item1.setVisible(true);
        }
        if (isprint) {
            if (status.equalsIgnoreCase("ACTIVE") || status.equalsIgnoreCase("ACCEPTED") || status.equalsIgnoreCase("INPROGRESS")) {
                MenuItem item1 = menu.findItem(R.id.action_print);
                item1.setVisible(true);
            }
        }

        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();


        if (isloadaddress && id == R.id.action_print && chitprint != null && productlist != null && productlist.size() > 0) {

            PrintReceiptStart(usersession.getsession(), address, chitprint, productlist, new Mycall() {
                @Override
                public void printsuccess() {

                    finish();
                    startActivity(getIntent().putExtra("isprint", false));
                    // after print from all task mytask noi assign task
                    //afterprint_finished(getIntent(),false);


                }

                @Override
                public void printfail() {

                }

                @Override
                public void assignarchived() {

                }
            }, symbol);
        } else if (id == R.id.action_print) {
            Toast.makeText(ChitlogActivity.this, "Wait while getting details...", Toast.LENGTH_LONG).show();

        }


        if (id == R.id.action_remark) {

            showcommentdialog();

        }


        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;


            default:
                return super.onOptionsItemSelected(item);
        }
    }


    String address = "", symbol = "";
    public static Chit chitprint = SummaryDetailFragmnet.item;
    public static ArrayList<Chitproducts> productlist = new ArrayList<>();

    public void getuserprofile() {


        ApiInterface apiService =
                ApiClient.getClient(ChitlogActivity.this).create(ApiInterface.class);
        Call call;

        call = apiService.getagregatorProfile(usersession.getAggregator_ID());

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {


                if (response.isSuccessful()) {
                    String a = new Gson().toJson(response.body());

                    try {

                        JSONObject jObjRes = new JSONObject(a);
                        JSONObject jObjdata = jObjRes.getJSONObject("response");
                        JSONArray aggregatorobj = jObjdata.getJSONArray("data");

                        JSONObject aggregator = aggregatorobj.getJSONObject(0);

                        String company_image = aggregator.getString("company_image");
                        String company_name = aggregator.getString("company_name");
                        String company_detail_short = aggregator.getString("company_detail_short");
                        String location = aggregator.getString("location");
                        String contact_no = aggregator.getString("contact_no");
                        symbol = aggregator.getString("symbol");
                        address = "" + company_name + "\n" + location + "\n" + "Ph-" + contact_no;
                        isloadaddress = true;

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                //  List<Movie> movies = response.body().getResults();
                //  recyclerView.setAdapter(new MoviesAdapter(movies, R.layout.list_item_movie, getApplicationContext()));
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // Log error here since request failed
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();


    }

    MrpFragmnet mrpFragmnet = new MrpFragmnet();


    public void loadMaterial() {
        if (Constants.chit_id_from_mrp != 0) {
            Constants.chit_id_from_mrp = 0;
            viewPager.setCurrentItem(mrpPos);
        }
    }

    static int mrpPos = -1;

    @Override
    public void onBackPressed() {


        if (ShareModel.getI().isBackFromChitLog) {

            ShareModel.getI().isBackFromChitLog = false;

            Class<?> cls = null;
            if (Constants.chit_id_to == 1) {
                cls = AllTaskActivity.class;
            } else if (Constants.chit_id_to == 2) {
                cls = MyTaskActivity.class;

            } else {
                cls = HistoryActivity.class; // for more than page 2 not redirect

            }

            Intent i = new Intent(this, cls);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            i.putExtra("session", sessionManager.getsession());
            startActivity(i);

        } else {
            super.onBackPressed();
        }
    }

    Context mContext;

    public void callOdrOrTask(final MyRequst myRequst) {

        mContext = this;
        MyRequestCall myRequestCall = new MyRequestCall();
        myRequestCall.searchCaseId(mContext, new MyRequestCall.CallRequest() {
            @Override
            public void onGetResponse(String response) {

                if (!response.isEmpty()) {
                    try {

                        JSONObject jObjRes = new JSONObject(response);
                        JSONObject jObjdata = jObjRes.getJSONObject("response");
                        JSONObject jObjResponse = jObjdata.getJSONObject("data");
                        //String totalRecord = jObjResponse.getString("totalRecord");
                        JSONArray jObjcontacts = jObjResponse.getJSONArray("chitHeader");


                        //String displayEndRecord = jObjResponse.getString("displayEndRecord");


                        for (int j = 0; j < jObjcontacts.length(); j++) {
                            if (jObjcontacts.get(j) instanceof JSONObject) {

                                if (!ShareModel.getI().isNavigateOnce) {
                                    ShareModel.getI().chitOnlyOnce = DetailsFragment.item_detail;
                                }

                                JSONObject jsnObj = (JSONObject) jObjcontacts.get(j);
                                Chit chit_obj = new Gson().fromJson(jsnObj.toString(), Chit.class);

                                Intent i = new Intent(mContext, ChitlogActivity.class);
                                i.putExtra("chit_hash_id", chit_obj.getChit_hash_id());
                                i.putExtra("chit_id", chit_obj.getChit_id() + "");
                                i.putExtra("transtactionid", chit_obj.getChit_name() + "");
                                i.putExtra("productqty", chit_obj.getTotal_chit_item_value() + "");
                                i.putExtra("productprice", chit_obj.getChit_item_count() + "");
                                i.putExtra("purpose", chit_obj.getPurpose());
                                i.putExtra("Refid", chit_obj.getRef_id());
                                i.putExtra("Caseid", chit_obj.getCase_id());
                                DetailsFragment.chititemcount = "" + chit_obj.getChit_item_count();
                                DetailsFragment.chititemprice = "" + chit_obj.getTotal_chit_item_value();
                                SummaryDetailFragmnet.item = chit_obj;
                                DetailsFragment.item_detail = chit_obj;

                                //boolean isMp = !isMrp;

                                i.putExtra("isMrp", true);
                                i.putExtra("isMrpTabOnly", true);

                                if (ShareModel.getI().isFromAllTask) {
                                    //showMsg("Going to Order " + chit_obj.getCase_id());
                                    showMsg("" + chit_obj.getPurpose() + " / " + chit_obj.getRef_id() + " / " + chit_obj.getCase_id() + " from order");
                                    ShareModel.getI().isFromAllTask = false;
                                } else { // from order
                                    //showMsg("Going to Task " + chit_obj.getCase_id());
                                    showMsg("" + chit_obj.getPurpose() + " / " + chit_obj.getRef_id() + " / " + chit_obj.getCase_id() + " from task");
                                    ShareModel.getI().isFromAllTask = true;
                                }

                                startActivityForResult(i, 51);
                                if (ShareModel.getI().isNavigateOnce) {
                                    finish();
                                }
                                ShareModel.getI().isNavigateOnce = true;

                                break;
                            }
                        }
                    } catch (Exception e) {
                        Toast.makeText(mContext, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }
        }, null, myRequst);


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 91) {
            if (resultCode == RESULT_OK) {
                viewPager.setCurrentItem(mrpPos);
                if (vPos == 1) {
                    mrpFragmnet.onRefresh(SwipyRefreshLayoutDirection.TOP);
                } else if (vPos == 4) {
                    mrpFragmnet.onRefreshDelivery(SwipyRefreshLayoutDirection.TOP);
                }
            }
        }
        if (requestCode == 51) {
            ShareModel.getI().isNavigateOnce = false;
            //Toast.makeText(mContext, "Call 51", Toast.LENGTH_LONG).show();

            Chit chit_obj = ShareModel.getI().chitOnlyOnce;
            DetailsFragment.chititemcount = "" + chit_obj.getChit_item_count();
            DetailsFragment.chititemprice = "" + chit_obj.getTotal_chit_item_value();
            SummaryDetailFragmnet.item = chit_obj;
            DetailsFragment.item_detail = chit_obj;

            //finish();

        }
    }
}
