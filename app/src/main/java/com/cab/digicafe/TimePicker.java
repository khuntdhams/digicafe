package com.cab.digicafe;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.format.DateFormat;

import com.cab.digicafe.Helper.SessionManager;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class TimePicker extends DialogFragment implements TimePickerDialog.OnTimeSetListener {

    SessionManager sessionManager;
    private OnItemTimerClickListener listener;

    public TimePicker(OnItemTimerClickListener listener) {
        this.listener = listener;
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        listener.onItemClick(false, 0, 0);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar c = Calendar.getInstance();
        sessionManager = new SessionManager(getActivity());
        String dtStart = sessionManager.getDate();
        SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy hh:mm aaa");
        try {
            if (!dtStart.equals("")) {
                Date date = format.parse(dtStart);
                c.setTime(date);
                System.out.println(date);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);
        return new TimePickerDialog(getActivity(), this, hour, minute, DateFormat.is24HourFormat(getActivity()));
    }

    @Override
    public void onTimeSet(android.widget.TimePicker view, int hourOfDay, int minute) {
        listener.onItemClick(true, hourOfDay, minute);
    }

    public interface OnItemTimerClickListener {
        void onItemClick(Boolean isTrue, int hourOfDay, int minute);
    }
}
