package com.cab.digicafe.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pc51 on 2/21/2018.
 */

public class SettingDB extends SQLiteOpenHelper {

    public static final String DATABASE = "SettingDb";
    public static final String TBL_SETTING = "Tbl_SETTING";


    public SettingDB(Context context) {
        super(context, DATABASE, null, 1);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE Tbl_SETTING (id INTEGER PRIMARY KEY,isdate TEXT, isaddress TEXT, isremark TEXT,ispayumoney TEXT,isgoogleplace TEXT,isreceipt TEXT,ismytask TEXT,isalltask TEXT,issearchalltask TEXT, isconcateoffer TEXT, isprinttoken TEXT, isprintreceipt TEXT, isprintdisplaydata TEXT, isvisiondetect TEXT)");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE item");
        onCreate(db);
    }

    public void InsertItem(SettingModel item) {
        SQLiteDatabase database = this.getReadableDatabase();
        ContentValues Values = new ContentValues();
        Values.put("id", item.getId());
        Values.put("isdate", item.getIsdate());
        Values.put("isaddress", item.getIsaddress());
        Values.put("isremark", item.getIsremark());
        Values.put("ispayumoney", item.getIspayumoney());
        Values.put("isgoogleplace", item.getIsgoogleplace());
        Values.put("isreceipt", item.getIsreceipt());
        Values.put("ismytask", item.getIsmytask());
        Values.put("isalltask", item.getIsalltask());
        Values.put("issearchalltask", item.getIssearchalltask());
        Values.put("isconcateoffer", item.getIsconcateoffer());
        Values.put("isprinttoken", item.getIsprinttoken());
        Values.put("isprintreceipt", item.getIsprintreceipt());
        Values.put("isprintdisplaydata", item.getIsprintdisplaydata());
        Values.put("isvisiondetect", item.getIsvisiondetect());
        database.insert(TBL_SETTING, null, Values);
        database.close();
    }


    public void UpdateItem(String c_nm, String c_val) {
        SQLiteDatabase database = this.getReadableDatabase();
        ContentValues Values = new ContentValues();
       /* Values.put("id", item.getId());
        Values.put("isdate", item.getIsdate());
        Values.put("isaddress", item.getIsaddress());
        Values.put("isremark", item.getIsremark());
        Values.put("ispayumoney", item.getIspayumoney());
        Values.put("isgoogleplace", item.getIsgoogleplace());
        Values.put("isreceipt", item.getIsreceipt());
        Values.put("ismytask", item.getIsmytask());*/
        //database.update(TBL_SETTING, Values, "id" + " = "+item.getId(),null);
        Values.put(c_nm, c_val);
        database.update(TBL_SETTING, Values, "id" + " = ?", new String[]{String.valueOf(1)});
        database.close();

    }

    public List<SettingModel> GetItems() {
        SQLiteDatabase DB = getWritableDatabase();
        List<SettingModel> items = new ArrayList();
        Cursor cursor = DB.rawQuery("SELECT * FROM Tbl_SETTING ORDER BY id", null);
        if (cursor.moveToFirst()) {
            do {
                items.add(new SettingModel(cursor.getInt(0), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getString(5), cursor.getString(6), cursor.getString(7), cursor.getString(8), cursor.getString(9), cursor.getString(10), cursor.getString(11), cursor.getString(12), cursor.getString(13), cursor.getString(14)));
            } while (cursor.moveToNext());
        }
        if (items.size() > 0) {
            SettingModel sm = items.get(0);

        }
        return items;
    }

    public void deleteall() {
        SQLiteDatabase db = this.getReadableDatabase();
        db.delete(TBL_SETTING, null, null);
        db.close();
    }


}
