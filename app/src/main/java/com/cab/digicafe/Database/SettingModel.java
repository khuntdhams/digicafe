package com.cab.digicafe.Database;

/**
 * Created by pc51 on 2/21/2018.
 */

public class SettingModel {

    String isdate;
    String isaddress;
    String isremark;
    String ispayumoney;
    String isgoogleplace;
    String isreceipt;
    String ismytask;
    String isalltask;
    String isconcateoffer;
    String isprinttoken;

    public String getIsprinttoken() {
        return isprinttoken;
    }

    public void setIsprinttoken(String isprinttoken) {
        this.isprinttoken = isprinttoken;
    }

    public String getIsprintreceipt() {
        return isprintreceipt;
    }

    public void setIsprintreceipt(String isprintreceipt) {
        this.isprintreceipt = isprintreceipt;
    }

    String isprintreceipt;
    String isprintdisplaydata;

    public String getIsprintdisplaydata() {
        return isprintdisplaydata;
    }

    public void setIsprintdisplaydata(String isprintdisplaydata) {
        this.isprintdisplaydata = isprintdisplaydata;
    }

    public String getIsconcateoffer() {
        return isconcateoffer;
    }

    public void setIsconcateoffer(String isconcateoffer) {
        this.isconcateoffer = isconcateoffer;
    }

    public String getIssearchalltask() {
        return issearchalltask;
    }

    public void setIssearchalltask(String issearchalltask) {
        this.issearchalltask = issearchalltask;
    }

    String issearchalltask;

    int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIsdate() {
        return isdate;
    }

    public void setIsdate(String isdate) {
        this.isdate = isdate;
    }

    public String getIsaddress() {
        return isaddress;
    }

    public void setIsaddress(String isaddress) {
        this.isaddress = isaddress;
    }

    public String getIsremark() {
        return isremark;
    }

    public void setIsremark(String isremark) {
        this.isremark = isremark;
    }

    public String getIspayumoney() {
        return ispayumoney;
    }

    public void setIspayumoney(String ispayumoney) {
        this.ispayumoney = ispayumoney;
    }

    public String getIsgoogleplace() {
        return isgoogleplace;
    }

    public void setIsgoogleplace(String isgoogleplace) {
        this.isgoogleplace = isgoogleplace;
    }

    public String getIsreceipt() {
        return isreceipt;
    }

    public void setIsreceipt(String isreceipt) {
        this.isreceipt = isreceipt;
    }

    public String getIsmytask() {
        return ismytask;
    }

    public void setIsmytask(String ismytask) {
        this.ismytask = ismytask;
    }

    public String getIsalltask() {
        return isalltask;
    }

    public void setIsalltask(String isalltask) {
        this.isalltask = isalltask;
    }

    public SettingModel(int id, String isdate, String isaddress, String isremark, String ispayumoney, String isgoogleplace, String isreceipt, String ismytask, String isalltask, String issearchalltask, String isconcateoffer, String isprinttoken, String isprintreceipt, String isprintdisplaydata, String isvisiondetect) {
        this.isdate = isdate;
        this.isaddress = isaddress;
        this.isremark = isremark;
        this.ispayumoney = ispayumoney;
        this.isgoogleplace = isgoogleplace;
        this.isreceipt = isreceipt;
        this.ismytask = ismytask;
        this.id = id;
        this.isalltask = isalltask;
        this.issearchalltask = issearchalltask;
        this.isconcateoffer = isconcateoffer;
        this.isprinttoken = isprinttoken;
        this.isprintreceipt = isprintreceipt;
        this.isprintdisplaydata = isprintdisplaydata;
        this.isvisiondetect = isvisiondetect;

    }

    public SettingModel() {
    }

    String isvisiondetect;

    public String getIsvisiondetect() {
        return isvisiondetect;
    }

    public void setIsvisiondetect(String isvisiondetect) {
        this.isvisiondetect = isvisiondetect;
    }
}
