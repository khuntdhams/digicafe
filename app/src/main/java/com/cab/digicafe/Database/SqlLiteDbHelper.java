package com.cab.digicafe.Database;

/**
 * Created by CSS on 14-01-2018.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.text.TextUtils;
import android.util.Log;

import com.cab.digicafe.Model.Catelog;
import com.cab.digicafe.Model.ModelFile;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Iterator;

public class SqlLiteDbHelper extends SQLiteOpenHelper {

    // All Static variables
// Database Version
    private static final int DATABASE_VERSION = 1;
    // Database Name
    private static final String DATABASE_NAME = "Digicafe.db";
    private static final String DB_PATH_SUFFIX = "/databases/";
    static Context ctx;
    private static final String TABLE_NAME = "cart";

    private static final String PRODUCTID = "product_id";
    private static final String PRODUCTNAME = "name";
    private static final String CATERERID = "caterer_id";
    private static final String COUNT = "count";
    private static final String UNITPRICE = "unitprice";
    private static final String PRICE = "price";
    private static final String DISC_PRICE = "dis_price";
    private static final String DISC_PERCENTAGE = "dis_perc";
    private static final String TAX = "tax";
    private static final String ADDITIONAL = "additional";
    private static final String INTERNAL = "intrernal";
    private static final String FIELD = "field";
    private static final String OFFER = "offer";
    private static final String IMG_ARRAY = "img_url";
    private static final String IMG_SAMPLE = "img_sample";
    private static final String Cross_Reference = "cross_reference";

    public SqlLiteDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        ctx = context;
    }

    public int getproductcartcount(Catelog c) {
        SQLiteDatabase db = this.getReadableDatabase();
        int itemcount = 0;
        Cursor cursor = db.rawQuery("SELECT SUM(count) FROM cart WHERE product_id = '" + c.getEntry_id() + "'", null);
        if (cursor != null && cursor.moveToFirst()) {
// return contac
            itemcount = cursor.getInt(0);
            cursor.close();
            db.close();

            return itemcount;

        }
        return itemcount;
    }

    // Getting single contact
    public int getcartcount() {
        SQLiteDatabase db = this.getReadableDatabase();
        int itemcount = 0;
        Cursor cursor = db.rawQuery("SELECT SUM(count) FROM cart", null);
        if (cursor != null && cursor.moveToFirst()) {
// return contac
            itemcount = cursor.getInt(0);
            cursor.close();
            db.close();

            return itemcount;

        }
        return itemcount;
    }

    public Double getcarttotal() {
        SQLiteDatabase db = this.getReadableDatabase();
        Double itemcount = 0.0;
        Cursor cursor = db.rawQuery("SELECT SUM(price) FROM cart", null);
        if (cursor != null && cursor.moveToFirst()) {
// return contact

            itemcount = cursor.getDouble(0);
            cursor.close();
            db.close();

            return itemcount;

        }
        return itemcount;
    }

    public ArrayList<Catelog> getAllcaretproduct() {
        SQLiteDatabase database = this.getReadableDatabase();
        Cursor cursor = database.rawQuery("SELECT * FROM " + TABLE_NAME + " WHERE count > 0", null);
        ArrayList<Catelog> products = new ArrayList<Catelog>();
        Catelog prodeuctModel;
        Log.e("ouputcursorvalue", "" + cursor.getCount());
        if (cursor.getCount() > 0) {
            for (int i = 0; i < cursor.getCount(); i++) {
                cursor.moveToNext();
                prodeuctModel = new Catelog();
                prodeuctModel.setEntry_id(Integer.parseInt(cursor.getString(0)));
                prodeuctModel.setDefault_name(cursor.getString(1));
                prodeuctModel.setCartcount(cursor.getInt(3));
                prodeuctModel.setMrp("" + cursor.getDouble(4));
                prodeuctModel.setDiscounted_price(cursor.getDouble(7));
                prodeuctModel.setDiscount_percentage(cursor.getDouble(8));
                prodeuctModel.setTax_json_data(cursor.getString(6));
                prodeuctModel.setOffer(cursor.getString(9));
                prodeuctModel.setImg_url(cursor.getString(10));
                prodeuctModel.setCross_reference(cursor.getString(11));
                prodeuctModel.setAdditional_json_data(cursor.getString(12));
                prodeuctModel.setField_json_data(cursor.getString(13));
                prodeuctModel.setInternal_json_data(cursor.getString(14));

                Gson gson = new Gson();
                TypeToken<ArrayList<String>> token = new TypeToken<ArrayList<String>>() {
                };
                ArrayList<String> pb = gson.fromJson(cursor.getString(10), token.getType());
                prodeuctModel.setImageArr(pb);

                TypeToken<ArrayList<ModelFile>> ttSamples = new TypeToken<ArrayList<ModelFile>>() {
                };
                ArrayList<ModelFile> alSamples = gson.fromJson(cursor.getString(15), ttSamples.getType());
                prodeuctModel.setAl_selet(alSamples);


                products.add(prodeuctModel);
            }
        }
        cursor.close();
        database.close();
        return products;
    }

    public JSONObject gettotaltaxamount() {
        SQLiteDatabase database = this.getReadableDatabase();
        Cursor cursor = database.rawQuery("SELECT * FROM " + TABLE_NAME + " WHERE count > 0", null);
        ArrayList<Catelog> products = new ArrayList<Catelog>();

        Double taxvalue = 0.0;
        Catelog prodeuctModel;
        JSONObject taxinfo = new JSONObject();
        Log.e("ouputcursorvalue", "" + cursor.getCount());
        JSONObject taxinfojson = new JSONObject();


        if (cursor.getCount() > 0) {
            for (int i = 0; i < cursor.getCount(); i++) {
                cursor.moveToNext();
                Log.e("ouputcursorvalue", "" + cursor.getString(6));
                String taxjson = cursor.getString(6);
                if (taxjson != null) {
                    if (!TextUtils.isEmpty(taxjson)) {
                        try {
                            JSONObject tax_info = null;

                            tax_info = new JSONObject(taxjson);
                            Iterator<String> iter = tax_info.keys();
                            while (iter.hasNext()) {

                                String key = iter.next();
                                if (taxinfojson.has(key)) {
                                    taxvalue = taxinfojson.getDouble(key);

                                }
                                Log.e("ouputturntaxvalue", "" + tax_info);

                                try {
                                    Object value = tax_info.get(key);
                                    Double taxpercent = new Double(value.toString());
                                    taxvalue = taxpercent + taxvalue;

                                    taxinfojson.put(key, taxvalue);

                                } catch (JSONException e) {
                                    // Something went wrong!
                                    Log.e("Tax_json_data", "" + e.getLocalizedMessage());

                                }
                            }
                            Log.e("Tax_json_data", "" + taxinfojson);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
                Log.e("ouputcursorvaluereturn", "" + taxinfojson);

               /* prodeuctModel = new Catelog();
                prodeuctModel.setEntry_id(Integer.parseInt(cursor.getString(0)));
                prodeuctModel.setDefault_name(cursor.getString(1));
                prodeuctModel.setCartcount(cursor.getInt(3));
                prodeuctModel.setMrp(""+cursor.getDouble(4));*/

                // products.add(prodeuctModel);
            }
        }
        cursor.close();
        database.close();
        return taxinfojson;
    }

    public ArrayList<Catelog> getAllRecordsAlternate() {
        SQLiteDatabase database = this.getReadableDatabase();
        Cursor cursor = database.rawQuery("SELECT * FROM " + TABLE_NAME, null);
        ArrayList<Catelog> products = new ArrayList<Catelog>();
        Catelog prodeuctModel;
        if (cursor.getCount() > 0) {
            for (int i = 0; i < cursor.getCount(); i++) {
                cursor.moveToNext();
                prodeuctModel = new Catelog();
                prodeuctModel.setEntry_id(Integer.parseInt(cursor.getString(0)));
                prodeuctModel.setDefault_name(cursor.getString(1));
                prodeuctModel.setCartcount(cursor.getInt(3));
                prodeuctModel.setMrp("" + cursor.getDouble(4));

                products.add(prodeuctModel);
            }
        }
        cursor.close();
        database.close();
        return products;
    }

    public void inserproduct(Catelog c, String aliasname) {

        //Double mrpvalue = Double.parseDouble(c.getMrp());
        Double mrpvalue = Double.parseDouble(c.getdiscountmrp());
        Double totalprice = mrpvalue * c.getCartcount();

        Log.i("okhttp", "" + mrpvalue);
        Log.e("Tax_json_data", "" + c.getTax_json_data());
        Double taxvalue = 0.0;
        JSONObject taxinfojson = new JSONObject();
        if (c.getTax_json_data() != null) {
            if (!TextUtils.isEmpty(c.getTax_json_data())) {
                try {
                    JSONObject tax_info = null;
                    tax_info = new JSONObject(c.getTax_json_data());
                    Log.e("Tax_json_data", "" + tax_info);
                    Iterator<String> iter = tax_info.keys();

                    while (iter.hasNext()) {

                        String key = iter.next();
                        try {
                            Object value = tax_info.get(key);
                            Double taxpercent = new Double(value.toString());

                            Double taxamount = totalprice * (taxpercent / 100);
                            taxinfojson.put(key, taxamount);
                            taxvalue = taxvalue + taxamount;
                            taxinfojson.put(key, taxamount);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    Log.e("Tax_json_data", "" + taxinfojson);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        Log.e("ouputcursorvalue", "" + taxinfojson.toString());

        Log.e("mrpvalue", "" + totalprice);
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("product_id", c.getEntry_id());
        values.put("name", c.getDefault_name());
        values.put("tax", taxinfojson.toString());
        values.put(COUNT, c.getCartcount());
        values.put(UNITPRICE, c.getMrp());
        values.put(PRICE, totalprice);
        values.put(DISC_PRICE, c.getDiscounted_price());
        values.put(DISC_PERCENTAGE, c.getDiscount_percentage());
        values.put(TAX, c.getTax_json_data());
        values.put(ADDITIONAL, c.getAdditional_json_data());
        values.put(INTERNAL, c.getInternal_json_data());
        values.put(FIELD, c.getField_json_data());
        values.put(OFFER, c.getOffer());
        Gson gson = new Gson();
        String json = gson.toJson(c.getImageArr());
        values.put(IMG_ARRAY, json);
        String samples = gson.toJson(c.getAl_selet());
        values.put(IMG_SAMPLE, samples);
        values.put(CATERERID, aliasname);
        values.put(Cross_Reference, c.getCross_reference());
        Log.e("responseclientname", "success");
        db.insert("cart", null, values);
        db.close();


    }

    public void deleteRecord(Catelog c) {
        SQLiteDatabase db = this.getReadableDatabase();
        db.delete("cart", "product_id" + " = " + c.getEntry_id(), null);
        db.close();
    }

    public void deleteallfromcart() {
        SQLiteDatabase db = this.getReadableDatabase();
        db.delete("cart", null, null);
        db.close();
    }

    public void updateRecord(Catelog c) {
        Double mrpvalue = Double.parseDouble(c.getdiscountmrp());
        //Double mrpvalue = Double.parseDouble(c.getdiscountmrp());
        Log.e("mrpvalue", "" + mrpvalue);
        Double totalprice = mrpvalue * c.getCartcount();

        Double taxvalue = 0.0;
        Log.e("mrpvalue", "" + totalprice);
        JSONObject taxinfojson = new JSONObject();
        if (!TextUtils.isEmpty(c.getTax_json_data())) {
            try {
                JSONObject tax_info = null;
                tax_info = new JSONObject(c.getTax_json_data().toString());
                Log.e("Tax_json_data", "" + tax_info);
                Iterator<String> iter = tax_info.keys();

                while (iter.hasNext()) {

                    String key = iter.next();

                    try {
                        Object value = tax_info.get(key);
                        Double taxpercent = new Double(value.toString());

                        Double taxamount = totalprice * (taxpercent / 100);
                        taxinfojson.put(key, taxamount);
                        taxvalue = taxvalue + taxamount;
                    } catch (JSONException e) {
                        // Something went wrong!
                        Log.e("Tax_json_data", "" + e.getLocalizedMessage());

                    }
                }
                Log.e("Tax_json_data", "" + taxinfojson);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        SQLiteDatabase database = this.getReadableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(PRODUCTID, c.getEntry_id());
        contentValues.put(PRODUCTNAME, c.getDefault_name());
        contentValues.put(COUNT, c.getCartcount());
        contentValues.put(UNITPRICE, c.getMrp());
        contentValues.put(PRICE, totalprice);
        contentValues.put(DISC_PRICE, c.getDiscounted_price());
        contentValues.put(DISC_PERCENTAGE, c.getDiscount_percentage());
        contentValues.put(TAX, c.getTax_json_data());
        contentValues.put(INTERNAL, c.getInternal_json_data());
        contentValues.put(OFFER, c.getOffer());
        contentValues.put(ADDITIONAL, c.getAdditional_json_data());
        contentValues.put(FIELD, c.getField_json_data());
        Gson gson = new Gson();
        String json = gson.toJson(c.getImageArr());
        contentValues.put(IMG_ARRAY, json);
        String samples = gson.toJson(c.getAl_selet());
        contentValues.put(IMG_SAMPLE, samples);
        contentValues.put(Cross_Reference, c.getCross_reference());
        //contentValues.put(DISC_PRICE, c.getDiscounted_price());
        //contentValues.put(DISC_PERCENTAGE, c.getDiscount_percentage());
        database.update(TABLE_NAME, contentValues, PRODUCTID + " = " + c.getEntry_id(), null);
        database.close();
    }

    public boolean isproductexistincart(String query) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery(query, null);
        if (cursor.getCount() > 0) {
// return contact
            cursor.close();
            db.close();

            return true;

        }
        return false;
    }

    public void CopyDataBaseFromAsset() throws IOException {
        Log.e("errormsg", "success2");

        InputStream myInput = ctx.getAssets().open(DATABASE_NAME);

// Path to the just created empty db
        String outFileName = getDatabasePath();

// if the path doesn't exist first, create it
        File f = new File(ctx.getApplicationInfo().dataDir + DB_PATH_SUFFIX);
        if (!f.exists())
            f.mkdir();

// Open the empty db as the output stream
        OutputStream myOutput = new FileOutputStream(outFileName);
        Log.e("errormsg", "success2");

// transfer bytes from the inputfile to the outputfile
        byte[] buffer = new byte[1024];
        int length;
        Log.e("errormsg", "success3");

        while ((length = myInput.read(buffer)) > 0) {
            myOutput.write(buffer, 0, length);
        }
        Log.e("errormsg", "successl");

// Close the streams
        myOutput.flush();
        myOutput.close();
        myInput.close();

    }

    private static String getDatabasePath() {
        return ctx.getApplicationInfo().dataDir + DB_PATH_SUFFIX
                + DATABASE_NAME;
    }

    public SQLiteDatabase openDataBase() throws SQLException {
        File dbFile = ctx.getDatabasePath(DATABASE_NAME);

        if (!dbFile.exists()) {
            try {
                CopyDataBaseFromAsset();
                System.out.println("Copying sucess from Assets folder");
            } catch (IOException e) {
                Log.e("errormsg", e.getLocalizedMessage());
                Log.e("errormsg", e.getMessage());
                throw new RuntimeException("Error creating source database", e);
            }
        }

        return SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.CREATE_IF_NECESSARY);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
// TODO Auto-generated method stub

    }
}

