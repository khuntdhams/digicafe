package com.cab.digicafe;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.cab.digicafe.Helper.SessionManager;
import com.cab.digicafe.Model.Userprofile;
import com.cab.digicafe.Rest.ApiClient;
import com.cab.digicafe.Rest.ApiInterface;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangepasswordActivity extends AppCompatActivity {
    TextInputLayout oldpassword;
    TextInputLayout password;
    TextInputLayout confirmpassword;
    String str_oldpassword = "";
    String str_password = "";
    String str_confirmpassword = "";
    String sessionString = "";
    Button submit;
    public static String currentuser = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_changepassword);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        oldpassword = (TextInputLayout) findViewById(R.id.oldpasssword);
        password = (TextInputLayout) findViewById(R.id.newpassword);
        confirmpassword = (TextInputLayout) findViewById(R.id.confirmpassword);
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                sessionString = null;

            } else {
                sessionString = extras.getString("session");


            }
        } else {
            sessionString = (String) savedInstanceState.getSerializable("session");

        }
        submit = (Button) findViewById(R.id.signup);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Boolean isvalid = true;
                str_oldpassword = oldpassword.getEditText().getText().toString();
                str_password = password.getEditText().getText().toString();
                str_confirmpassword = confirmpassword.getEditText().getText().toString();
                if (str_oldpassword.length() == 0) {
                    isvalid = false;
                    oldpassword.setError("Please Enter your old password");
                }
                if (str_password.length() == 0) {
                    isvalid = false;
                    password.setError("Please Enter your new password");
                }
                if (str_confirmpassword.length() == 0) {
                    isvalid = false;
                    confirmpassword.setError("Please Enter your new password again");
                } else if (!str_password.equalsIgnoreCase(str_confirmpassword)) {
                    isvalid = false;
                    confirmpassword.setError("Password and Confirm Password should be same");
                }

                if (isvalid) {
                    userregistration();
                }
            }
        });

    }

    public void userregistration() {

        JsonObject paswordquery = new JsonObject();
        paswordquery.addProperty("old_password", str_oldpassword);
        paswordquery.addProperty("new_password", str_password);
        paswordquery.addProperty("confirm_password", str_confirmpassword);

        ApiInterface apiService =
                ApiClient.getClient(ChangepasswordActivity.this).create(ApiInterface.class);

        Call call = apiService.getChangepassword(sessionString, paswordquery);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                int statusCode = response.code();
                Log.e("response_message", "" + statusCode);

                if (response.isSuccessful()) {
                    //passwordchanged();
                    try {
                        String a = new Gson().toJson(response.body());
                        Toast.makeText(ChangepasswordActivity.this, "Password Change successfully!", Toast.LENGTH_LONG).show();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    Userprofile obj = null;
                    SessionManager sessionManager = new SessionManager(ChangepasswordActivity.this);
                    try {
                        currentuser = sessionManager.getcurrentu_nm();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    sessionManager.logoutUser(ChangepasswordActivity.this, new SessionManager.IlogOut() {
                        @Override
                        public void logOut() {
                            finish();
                        }
                    });
                    Toast.makeText(ChangepasswordActivity.this, "Please login again!", Toast.LENGTH_LONG).show();


                } else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Log.e("response_message", jObjError.toString());

                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        String errormessage = jObjErrorresponse.getString("errormsg");
                        Toast.makeText(ChangepasswordActivity.this, errormessage, Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(ChangepasswordActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }

                //  List<Movie> movies = response.body().getResults();
                //  recyclerView.setAdapter(new MoviesAdapter(movies, R.layout.list_item_movie, getApplicationContext()));
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // Log error here since request failed
                // Log.e("error message", t.toString());
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void passwordchanged() {
        confirmpassword.getEditText().setText("");
        password.getEditText().setText("");
        oldpassword.getEditText().setText("");
        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(ChangepasswordActivity.this, R.style.AlertDialogCustom));
        builder
                .setMessage("Password Changed Successfully")
                .setTitle("")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //finish();
                        Intent i = new Intent(ChangepasswordActivity.this, MainActivity.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        i.putExtra("session", sessionString);


                        startActivity(i);
                    }
                })


                .show();
    }
}
