package com.cab.digicafe;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.TextUtils;
import android.util.Log;

import com.cab.digicafe.Database.SqlLiteDbHelper;
import com.cab.digicafe.Dialogbox.CustomDialogClass;
import com.cab.digicafe.Model.Catelog;
import com.cab.digicafe.Model.Supplier;
import com.cab.digicafe.Model.SupplierNew;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by crayond on 05/11/2017.
 */

public class Apputil {
    public static boolean isInternetAvailable(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork == null) return false;

        switch (activeNetwork.getType()) {
            case ConnectivityManager.TYPE_WIFI:
                if ((activeNetwork.getState() == NetworkInfo.State.CONNECTED ||
                        activeNetwork.getState() == NetworkInfo.State.CONNECTING) &&
                        isInternet())
                    return true;
                break;
            case ConnectivityManager.TYPE_MOBILE:
                if ((activeNetwork.getState() == NetworkInfo.State.CONNECTED ||
                        activeNetwork.getState() == NetworkInfo.State.CONNECTING) &&
                        isInternet())
                    return true;
                break;
            default:
                return false;
        }
        return false;
    }

    public static void showalert(String message, Context context) {
       /* AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(context, R.style.AlertDialogCustom));
        builder
                .setMessage(message)
                .setTitle("")
                .setCancelable(false)
                .setNegativeButton("Ok", null)

                .show();*/

        new CustomDialogClass((Activity) context, new CustomDialogClass.OnDialogClickListener() {
            @Override
            public void onDialogImageRunClick(int positon) {
                if (positon == 0) {

                } else if (positon == 1) {


                }
            }
        }, message).show();


    }

    public static void report_bug(Activity activity) {
        /* Bug report and send the bug to api */
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(activity));
    }

    public static String convertStandardJSONString(String data_json) {
        data_json = data_json.replaceAll("\\\\r\\\\n", "");
        data_json = data_json.replace("\"{", "{");
        data_json = data_json.replace("}\",", "},");
        data_json = data_json.replace("}\"", "}");
        return data_json;
    }

    private static boolean isInternet() {

        Runtime runtime = Runtime.getRuntime();
        try {
            Process ipProcess = runtime.exec("/system/bin/ping -c 1 8.8.8.8");
            int exitValue = ipProcess.waitFor();
            return (exitValue == 0);
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }

        return false;
    }

    public static Catelog getCatelog(SqlLiteDbHelper dbHelper, Catelog c) {
        String query = "Select name from cart where product_id = '" + c.getEntry_id() + "'";
        boolean isexist = dbHelper.isproductexistincart(query);
        if (isexist) {
            ArrayList<Catelog> productlist = new ArrayList<>();
            productlist = dbHelper.getAllcaretproduct();
            //String levelPatternGson = new Gson().toJson(productlist);
            //c = new Gson().fromJson(levelPatternGson,Catelog.class);

            for (int i = 0; i < productlist.size(); i++) {
                if (productlist.get(i).getEntry_id() == c.getEntry_id()) {

                    c = productlist.get(i);

                    break;

                }
            }
        }
        return c;
    }

    public static boolean isCatExist(SqlLiteDbHelper dbHelper, Catelog c) {
        String query = "Select name from cart where product_id = '" + c.getEntry_id() + "'";
        boolean isexist = dbHelper.isproductexistincart(query);
        return isexist;
    }


    public static void inserttocartWholesale(SqlLiteDbHelper dbHelper, Catelog c, String aliasname) {
        String query = "Select name from cart where product_id = '" + c.getEntry_id() + "'";
        boolean isexist = dbHelper.isproductexistincart(query);
        if (isexist) {
            dbHelper.updateRecord(c);
        } else {
            Log.e("prodeuc", "notexist");
            dbHelper.inserproduct(c, aliasname);
        }
    }

    public static void updateImg(SqlLiteDbHelper dbHelper, Catelog c) {
        String query = "Select name from cart where product_id = '" + c.getEntry_id() + "'";
        boolean isexist = dbHelper.isproductexistincart(query);
        if (isexist) {
            dbHelper.updateRecord(c);
        }
    }

    public static void inserttocart(SqlLiteDbHelper dbHelper, Catelog c, String aliasname) {
        String query = "Select name from cart where product_id = '" + c.getEntry_id() + "'";
        boolean isexist = dbHelper.isproductexistincart(query);
        if (isexist) {
            ArrayList<Catelog> productlist = new ArrayList<>();
            productlist = dbHelper.getAllcaretproduct();
            //String levelPatternGson = new Gson().toJson(productlist);
            //c = new Gson().fromJson(levelPatternGson,Catelog.class);

            for (int i = 0; i < productlist.size(); i++) {
                if (productlist.get(i).getEntry_id() == c.getEntry_id()) {

                    c.setMrp(productlist.get(i).getMrp());
                    /*Gson gson = new Gson();
                    TypeToken<ArrayList<String>> token = new TypeToken<ArrayList<String>>() {
                    };
                    ArrayList<String> pb = gson.fromJson(productlist.get(i).getImg_url(), token.getType());
                    c.setImageArr(pb);
                    c.setImg_url(productlist.get(i).getImg_url());*/
                    break;
                }
            }

            Log.e("prodeuc", "exist");
            dbHelper.updateRecord(c);
        } else {
            Log.e("prodeuc", "notexist");
            dbHelper.inserproduct(c, aliasname);
        }
    }

    public static boolean isanothercateror(SqlLiteDbHelper dbHelper, Supplier c) {
        String query = "Select * from cart";
        boolean isexist = dbHelper.isproductexistincart(query);
        if (isexist) {
            Log.e("prodeuc", "exist");
            String query1 = "Select name from cart where caterer_id = '" + c.getAlias_name() + "'";
            boolean isexist1 = dbHelper.isproductexistincart(query1);
            if (!isexist1) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public static boolean isanothercateror(SqlLiteDbHelper dbHelper, SupplierNew c) {
        String query = "Select * from cart";
        boolean isexist = dbHelper.isproductexistincart(query);
        if (isexist) {
            Log.e("prodeuc", "exist");
            String query1 = "Select name from cart where caterer_id = '" + c.getUsername() + "'";
            boolean isexist1 = dbHelper.isproductexistincart(query1);
            if (!isexist1) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }
}
