package com.cab.digicafe;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cab.digicafe.Activities.ImagePreviewActivity;
import com.cab.digicafe.Adapter.ChititemAdapter;
import com.cab.digicafe.Adapter.ImgPrevAdapter;
import com.cab.digicafe.Helper.Inad;
import com.cab.digicafe.Model.Catelog;
import com.cab.digicafe.Model.Chit;
import com.cab.digicafe.Model.ChitDetails;
import com.cab.digicafe.Model.Chitproducts;
import com.cab.digicafe.Model.Dental;
import com.cab.digicafe.Model.ModelFile;
import com.cab.digicafe.MyCustomClass.JsonObjParse;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link DetailsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link DetailsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DetailsFragment extends Fragment implements ChititemAdapter.OnItemClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private View view;
    ChititemAdapter adapter;
    ArrayList<Chitproducts> productlist;
    private static RecyclerView recyclerView;
    public static ChitDetails chitinfoobj;
    public static String chititemcount;
    public static String chititemprice;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    TextView txt_qty;
    TextView txt_total_price, tv_remark;
    private OnFragmentInteractionListener mListener;
    public static Chit item_detail;
    String currency = "";
    @BindView(R.id.rv_selectfile)
    RecyclerView rv_selectfile;
    Dental dental = new Dental();
    @BindView(R.id.llCountAndPrice)
    LinearLayout llCountAndPrice;

    @BindView(R.id.rv_selectfile_shade)
    RecyclerView rv_selectfile_shade;

    @BindView(R.id.rv_selectfile_tray)
    RecyclerView rv_selectfile_tray;

    @BindView(R.id.rv_selectfile_model)
    RecyclerView rv_selectfile_model;

    @BindView(R.id.rv_selectfile_bite)
    RecyclerView rv_selectfile_bite;

    @BindView(R.id.rv_selectfile_temp)
    RecyclerView rv_selectfile_temp;

    @BindView(R.id.llDentalImage)
    LinearLayout llDentalImage;

    TextView tvEmptyBlck;

    ImgPrevAdapter imgPrevAdapter;


    public DetailsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment DetailsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DetailsFragment newInstance(String param1, String param2) {
        Log.e("testparam2", param1 + "sessionstring");
        Log.e("testparam2", param2 + "sessionstring");
        DetailsFragment fragment = new DetailsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        Log.e("chitcountvalue", param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
            Log.e("testparam2", mParam1 + "sessionstring");
            Log.e("testparam2", mParam2 + "sessionstring");
            // chitinfoobj = (ChitDetails) getArguments().getSerializable("chitdetail");
        }
    }

    ArrayList<ModelFile> al_selet = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_details, container, false);

        ButterKnife.bind(this, view);
        //currency = SharedPrefUserDetail.getString(getActivity(), SharedPrefUserDetail.symbol_native,"") + " ";
        currency = item_detail.getSymbol_native() + " ";


        if (item_detail.getPurpose().equalsIgnoreCase("survey")) {
            llCountAndPrice.setVisibility(View.GONE);
        }

        al_selet = new ArrayList<>();
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        tvEmptyBlck = (TextView) view.findViewById(R.id.tvEmptyBlck);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));//Linear Items
        txt_qty = (TextView) view.findViewById(R.id.txt_qty);
        txt_total_price = (TextView) view.findViewById(R.id.txt_price);
        tv_remark = (TextView) view.findViewById(R.id.tv_remark);

        tv_remark.setText(item_detail.getHeader_note());

        if (item_detail.getHeader_note() != null && !item_detail.getHeader_note().equals("")) {
            tv_remark.setText("Note:\n" + item_detail.getHeader_note());
            tv_remark.setVisibility(View.VISIBLE);
        } else {
            tv_remark.setVisibility(View.GONE);
        }
        // Log.e("chitcountvalue",mParam1);
        //   txt_qty.setText(mParam1);

        //need to add currency symbol

        Double itemprice = Double.parseDouble(chititemcount);
        int itempriceval = Integer.valueOf(itemprice.intValue());

        txt_qty.setText("# " + itempriceval);
        DecimalFormat twoDForm = new DecimalFormat("#.##");
        Double formatedprice = 0.00;

        try {
            // change api key
            //    Double price = Double.parseDouble(chit_obj.getTotal_chit_item_value()); // Make use of autoboxing.  It's also easier to read.
            Double price = Double.parseDouble(chititemprice);
            formatedprice = Double.valueOf(twoDForm.format(price));

        } catch (NumberFormatException e) {
            // p did not contain a valid double
        }
        //need to add currency symbol
        //txt_total_price.setText(currency+String.format("%.2f", formatedprice)+"");
        try {
            JSONObject jo_summary = new JSONObject(item_detail.getFooter_note());

            Double totalcentralgst = Double.valueOf(jo_summary.getString("CGST"));
            Double totalstategst = Double.valueOf(jo_summary.getString("SGST"));
            Double vat = 0.0;
            if (jo_summary.has("VAT")) vat = Double.valueOf(jo_summary.getString("VAT"));


            Double total = Double.valueOf(jo_summary.getString("TOTALBILL"));

            total = total + totalcentralgst + totalstategst + vat;
            txt_total_price.setText(currency + Inad.getCurrencyDecimal(total, getActivity()));
            //txt_total_price.setText(currency+ Inad.getCurrencyDecimal(formatedprice,getActivity()));

        } catch (Exception e) {
            e.printStackTrace();
        }

        String dentalObj = JsonObjParse.getValueEmpty(item_detail.getFooter_note(), "dental_obj");

        if (!dentalObj.isEmpty()) {
            llDentalImage.setVisibility(View.VISIBLE);
            dental = new Gson().fromJson(dentalObj, Dental.class);
            Log.d("dental", dental.getName());
        } else {
            llDentalImage.setVisibility(View.GONE);
        }

        productlist = new ArrayList<>();
        productlist = chitinfoobj.getContent();
        adapter = new ChititemAdapter(productlist, getContext(), this, chitinfoobj.getCurrencyHeaderArr().getSymbol_native(), chitinfoobj.getCurrencyHeaderArr().getDecimal_digits());

        recyclerView.setAdapter(adapter);// set adapter on recyclerview

        recyclerView.setNestedScrollingEnabled(false);


        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        rv_selectfile.setLayoutManager(mLayoutManager);
        imgPrevAdapter = new ImgPrevAdapter(al_selet, getActivity(), new ImgPrevAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int pos) {
                String imgarray = new Gson().toJson(al_selet);
                Intent intent = new Intent(getActivity(), ImagePreviewActivity.class);
                intent.putExtra("imgarray", imgarray);
                intent.putExtra("pos", pos);
                intent.putExtra("isdrag", false);
                intent.putExtra("img_title", "Service");
                startActivity(intent);
            }

            @Override
            public void onDelete(int pos) {
            }

            @Override
            public void onLongClick(View pos) {
            }
        }, false);
        rv_selectfile.setAdapter(imgPrevAdapter);
        //rv_selectfile.setNestedScrollingEnabled(false);

        LinearLayoutManager mLayoutManager1 = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        rv_selectfile_shade.setLayoutManager(mLayoutManager1);
        imgPrevAdapter = new ImgPrevAdapter(dental.getAl_selet(), getActivity(), new ImgPrevAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int pos) {

                String imgarray = new Gson().toJson(dental.getAl_selet());
                Intent intent = new Intent(getActivity(), ImagePreviewActivity.class);
                intent.putExtra("imgarray", imgarray);
                intent.putExtra("pos", pos);
                intent.putExtra("isdrag", false);
                intent.putExtra("img_title", "Shade");
                startActivity(intent);

            }

            @Override
            public void onDelete(int pos) {

            }

            @Override
            public void onLongClick(View pos) {

            }
        }, false);
        rv_selectfile_shade.setAdapter(imgPrevAdapter);

        LinearLayoutManager mLayoutManager2 = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        rv_selectfile_tray.setLayoutManager(mLayoutManager2);
        imgPrevAdapter = new ImgPrevAdapter(dental.getAl_selet_tray(), getActivity(), new ImgPrevAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int pos) {

                String imgarray = new Gson().toJson(dental.getAl_selet_tray());
                Intent intent = new Intent(getActivity(), ImagePreviewActivity.class);
                intent.putExtra("imgarray", imgarray);
                intent.putExtra("pos", pos);
                intent.putExtra("isdrag", false);
                intent.putExtra("img_title", "Tray");
                startActivity(intent);
            }

            @Override
            public void onDelete(int pos) {
            }

            @Override
            public void onLongClick(View pos) {
            }
        }, false);
        rv_selectfile_tray.setAdapter(imgPrevAdapter);

        LinearLayoutManager mLayoutManager3 = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        rv_selectfile_model.setLayoutManager(mLayoutManager3);
        imgPrevAdapter = new ImgPrevAdapter(dental.getAl_selet_model(), getActivity(), new ImgPrevAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int pos) {

                String imgarray = new Gson().toJson(dental.getAl_selet_model());
                Intent intent = new Intent(getActivity(), ImagePreviewActivity.class);
                intent.putExtra("imgarray", imgarray);
                intent.putExtra("pos", pos);
                intent.putExtra("isdrag", false);
                intent.putExtra("img_title", "Model");
                startActivity(intent);
            }

            @Override
            public void onDelete(int pos) {
            }

            @Override
            public void onLongClick(View pos) {
            }
        }, false);
        rv_selectfile_model.setAdapter(imgPrevAdapter);

        LinearLayoutManager mLayoutManager4 = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        rv_selectfile_bite.setLayoutManager(mLayoutManager4);
        imgPrevAdapter = new ImgPrevAdapter(dental.getAl_selet_bite(), getActivity(), new ImgPrevAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int pos) {

                String imgarray = new Gson().toJson(dental.getAl_selet_bite());
                Intent intent = new Intent(getActivity(), ImagePreviewActivity.class);
                intent.putExtra("imgarray", imgarray);
                intent.putExtra("pos", pos);
                intent.putExtra("isdrag", false);
                intent.putExtra("img_title", "Bite");
                startActivity(intent);
            }

            @Override
            public void onDelete(int pos) {
            }

            @Override
            public void onLongClick(View pos) {
            }
        }, false);
        rv_selectfile_bite.setAdapter(imgPrevAdapter);

        LinearLayoutManager mLayoutManager5 = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        rv_selectfile_temp.setLayoutManager(mLayoutManager5);
        imgPrevAdapter = new ImgPrevAdapter(dental.getAl_selet_temp(), getActivity(), new ImgPrevAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int pos) {

                String imgarray = new Gson().toJson(dental.getAl_selet_temp());
                Intent intent = new Intent(getActivity(), ImagePreviewActivity.class);
                intent.putExtra("imgarray", imgarray);
                intent.putExtra("pos", pos);
                intent.putExtra("isdrag", false);
                intent.putExtra("img_title", "Temp");
                startActivity(intent);
            }

            @Override
            public void onDelete(int pos) {
            }

            @Override
            public void onLongClick(View pos) {
            }
        }, false);
        rv_selectfile_temp.setAdapter(imgPrevAdapter);

        try {
            JSONObject jo_summary = new JSONObject(item_detail.getFooter_note());


            String img = jo_summary.getString("img");

            if (!img.equals("")) {
                Gson gson = new Gson();
                TypeToken<ArrayList<String>> token = new TypeToken<ArrayList<String>>() {
                };
                ArrayList<String> al_img = gson.fromJson(img, token.getType());

                for (int i = 0; i < al_img.size(); i++) {
                    al_selet.add(new ModelFile(al_img.get(i), false));
                }

                imgPrevAdapter.setItem(al_selet);
            } else {
                rv_selectfile.setVisibility(View.GONE);
                tvEmptyBlck.setVisibility(View.GONE);
            }


        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }


        return view;

    }

    //Setting recycler view
    private void setRecyclerView() {


    }

    public void setchitdetails(ChitDetails productdetailsobj) {
        Log.e("chitdetailsfragment", "chitdetailrefreshed");
        if (productdetailsobj != null) {
            productlist = productdetailsobj.getContent();
        }
        adapter.notifyDataSetChanged();
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            // throw new RuntimeException(context.toString()
            //     + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onItemClick(Catelog item) {

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


}
