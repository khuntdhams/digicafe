package com.cab.digicafe;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.cab.digicafe.Activities.MyAppBaseActivity;
import com.cab.digicafe.Adapter.CategoryAddEditListAdapter;
import com.cab.digicafe.Adapter.PlaceArrayAdapter;
import com.cab.digicafe.Dialogbox.CustomDialogClass;
import com.cab.digicafe.Helper.Inad;
import com.cab.digicafe.Helper.SessionManager;
import com.cab.digicafe.Model.ShareModel;
import com.cab.digicafe.Model.SupplierCategory;
import com.cab.digicafe.Rest.ApiClient;
import com.cab.digicafe.Rest.ApiInterface;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.lamudi.phonefield.PhoneInputLayout;
import com.xiaofeng.flowlayoutmanager.Alignment;
import com.xiaofeng.flowlayoutmanager.FlowLayoutManager;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignupActivity extends MyAppBaseActivity implements
        GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks {
    PhoneInputLayout phoneInputLayout;
    TextInputLayout txt_firstname;
    TextInputLayout txt_email, til_mobile;
    TextInputLayout txt_password;
    TextInputLayout txt_flatno;
    TextInputLayout txt_blockno;
    TextInputLayout txt_confirmpassword;
    TextInputLayout tilUnmBusiness, tilShoNm;
    Button signupbtn;
    String str_actype = "Personal";
    String str_firstname = "";
    String str_lastname = "";
    String str_flat = "";
    String str_block = "";
    String str_area = "";
    String str_mobile = "";
    String str_bussinsee_user_id = "";
    String str_latitude = "";
    String str_longtitude = "";
    String str_countrycode = "";
    String str_email = "";
    String str_password = "";
    String str_confirmpassword = "";
    String str_newsletter = "";
    String str_terms = "Y";
    SessionManager sessionManager;
    String spin_area = "";
    RadioButton rb_manual, rb_current, rbPersonal, rbBusiness;
    LinearLayout ll_google, ll_manual_loc;
    RadioGroup rg_loc;
    AutoCompleteTextView tv_google_loc;
    boolean isregister = false;
    RadioGroup rgAct;
    String from = "", unmfromaddshop;
    EditText etUnm, etRePwd, etPwd;
    TextView tvSelectCat;
    RecyclerView rvCat;
    LinearLayout llCategory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        signupbtn = (Button) findViewById(R.id.signup);
        llCategory = (LinearLayout) findViewById(R.id.llCategory);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        etUnm = (EditText) findViewById(R.id.etUnm);
        etRePwd = (EditText) findViewById(R.id.etRePwd);
        etPwd = (EditText) findViewById(R.id.etPwd);
        tvSelectCat = (TextView) findViewById(R.id.tvSelectCat);
        rvCat = (RecyclerView) findViewById(R.id.rvCat);

        llCategory.setVisibility(View.GONE);

        business_status = "Open";


        if (getIntent().getExtras() != null) {
            from = getIntent().getExtras().getString("from", "");
            unmfromaddshop = getIntent().getExtras().getString("username", "");
            etUnm.setText(unmfromaddshop);
            etPwd.setText(unmfromaddshop);
            etRePwd.setText(unmfromaddshop);
            llCategory.setVisibility(View.VISIBLE);
            setCategoryRv();
            etUnm.setSelection(etUnm.getText().length());
            business_status = "Closed";

            field_json_data = ShareModel.getI().userprofile.field_json_data;


            etPwd.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            etRePwd.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            //etPwd.setTransformationMethod(new DoNothingTransformation());
            //etRePwd.setTransformationMethod(new DoNothingTransformation());
        }


        //TelephonyManager tm = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
        //String countryCodeValue = tm.getNetworkCountryIso();
        //String mPhoneNumber = tm.getLine1Number();

        rgAct = (RadioGroup) findViewById(R.id.rgAct);
        rbPersonal = (RadioButton) findViewById(R.id.rbPersonal);
        rbBusiness = (RadioButton) findViewById(R.id.rbBusiness);

        txt_firstname = (TextInputLayout) findViewById(R.id.firstname);
        txt_email = (TextInputLayout) findViewById(R.id.email);
        til_mobile = (TextInputLayout) findViewById(R.id.til_mobile);
        txt_flatno = (TextInputLayout) findViewById(R.id.flatno);
        txt_blockno = (TextInputLayout) findViewById(R.id.blockno);

        txt_password = (TextInputLayout) findViewById(R.id.password);

        txt_confirmpassword = (TextInputLayout) findViewById(R.id.confirmpassword);
        tilUnmBusiness = (TextInputLayout) findViewById(R.id.tilUnmBusiness);
        tilShoNm = (TextInputLayout) findViewById(R.id.tilShoNm);
        phoneInputLayout = (PhoneInputLayout) findViewById(R.id.phone_input_layout);


        sessionManager = new SessionManager(this);
        phoneInputLayout.setHint(R.string.phone_hint);
        //phoneInputLayout.getEditText().setText(mPhoneNumber);
        //phoneInputLayout.setDefaultCountry(countryCodeValue);
        Spinner spinner = (Spinner) findViewById(R.id.spinner);


        /*final ArrayList planets = new ArrayList();
        planets.add(Html.fromHtml("Select"));
        String serviceareas = sessionManager.getservicearea();
        String[] planetslist = serviceareas.split("\\|");
        //String[] planetslist = serviceareas.split(",");
        Log.e("arraylength", "" + sessionManager.getservicearea());
        List<String> wordList = Arrays.asList(planetslist);
        for (String e : wordList) {
            planets.add(e.trim());
        }
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item, planets);

        // ArrayAdapter adapter = ArrayAdapter.createFromResource(this, R.array.planets, R.layout.spinner_item);
        dataAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                // your code here
                if (position > 0) {
                    spin_area = planets.get(position).toString();

                } else

                {
                    spin_area = "";

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });*/

        usersignin();


        signupbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Boolean isvalid = true;


                txt_firstname.setError(null);
                txt_email.setError(null);
                phoneInputLayout.setError(null);
                tilUnmBusiness.setError(null);
                tilShoNm.setError(null);

                str_block = txt_blockno.getEditText().getText().toString().trim();
                str_flat = txt_flatno.getEditText().getText().toString().trim();

                str_area = "";
                if (rb_manual.isChecked()) {

                    if (!spin_area.equals("")) {

                        if (!str_block.trim().equals("")) {
                            str_block = " , " + str_block;
                        }
                        if (!str_flat.trim().equals("")) {
                            str_flat = " , " + str_flat;
                        }

                        str_area = spin_area + str_block + str_flat;

                    }
                } else {
                    str_area = tv_google_loc.getText().toString();
                }


                str_firstname = txt_firstname.getEditText().getText().toString();

                //str_mobile = phoneInputLayout.getEditText().getText().toString();
                str_mobile = til_mobile.getEditText().getText().toString();
                str_bussinsee_user_id = tilUnmBusiness.getEditText().getText().toString().trim();

                str_countrycode = phoneInputLayout.getPhoneNumber();
                str_countrycode = str_countrycode.replace(str_mobile, "");
                str_email = txt_email.getEditText().getText().toString();
                str_password = txt_password.getEditText().getText().toString();
                str_confirmpassword = txt_confirmpassword.getEditText().getText().toString();


                Log.e("password", str_password);
                Log.e("password", str_confirmpassword);


                if (isregister) {

                    if (str_firstname.length() == 0) {
                        isvalid = false;
                        txt_firstname.setError("Please Enter your name");
                    }

                    if (str_email.length() == 0) {

                        isvalid = false;
                        txt_email.setError("Please Enter your email");
                    } else {
                        if (!Apputil.isValidEmail(str_email)) {
                            isvalid = false;
                            txt_email.setError("Please Enter Valid email");
                        }
                    }
                    if (str_area.equals("")) {
                        isvalid = false;
                        Toast.makeText(SignupActivity.this, "AddAddress must be required!", Toast.LENGTH_LONG).show();

                    }


                    final String u_name = str_mobile + "@" + sessionManager.getAggregator_ID() + ".cr";
                    Login(u_name, str_password);


                } else {


                    txt_confirmpassword.setError(null);

                    str_firstname = "Temp";
                    if (from.equals("addshop")) {
                        //str_firstname = str_bussinsee_user_id;
                        str_firstname = tilShoNm.getEditText().getText().toString();
                    }


                    str_email = "Temp@gmail.com";


                    if (!StringUtils.isAlphanumeric(str_bussinsee_user_id) && str_actype.equals("Business")) {
                        isvalid = false;
                        tilUnmBusiness.setError("Username must be alphanumeric required");
                    }

                    if (str_actype.equals("Business") && (str_bussinsee_user_id.length() < 6 || str_bussinsee_user_id.length() > 12)) {
                        isvalid = false;
                        tilUnmBusiness.setError("Username must be 6 to 12 character required");
                    }


                    if (str_bussinsee_user_id.length() == 0 && str_actype.equals("Business")) {
                        isvalid = false;
                        tilUnmBusiness.setError("Please Enter Username");
                    }

                    if (str_firstname.isEmpty()) {
                        isvalid = false;
                        tilShoNm.setError("Please Enter Shop Name");
                    }

                    if (str_mobile.length() <= 9) {
                        isvalid = false;
                        phoneInputLayout.setError("Please Enter Valid Mobile Number");
                        til_mobile.setError("Please Enter Valid Mobile Number");
                    }


                    if (str_mobile.length() == 0) {
                        isvalid = false;
                        phoneInputLayout.setError("Please Enter your Mobile Number");
                        til_mobile.setError("Please Enter your Mobile Number");
                    }


                    if (TextUtils.isEmpty(str_password)) {
                        isvalid = false;
                        txt_password.setError("Please Enter Password");
                    }


                    if (TextUtils.isEmpty(str_confirmpassword)) {
                        isvalid = false;
                        txt_confirmpassword.setError("Please Enter Confirm Password");
                    } else if (!str_password.equalsIgnoreCase(str_confirmpassword)) {
                        isvalid = false;
                        txt_confirmpassword.setError("Password and Confirm Password should be same");
                    }

                    if (isvalid) {

                        Inad.hasActiveInternetConnection(SignupActivity.this, new Inad.Netcallback() {
                            @Override
                            public void netcall(boolean isnet) {
                                if (isnet) {
                                    userregistration();
                                } else {
                                    new CustomDialogClass(SignupActivity.this, new CustomDialogClass.OnDialogClickListener() {
                                        @Override
                                        public void onDialogImageRunClick(int positon) {
                                            if (positon == 0) {

                                            } else if (positon == 1) {

                                            }
                                        }
                                    }, "Alert\nInternet connection is required!").show();

                                }
                            }
                        });

                    }


                }


            }
        });

        locationinitialized();

        rgAct.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {

                switch (i) {
                    case R.id.rbPersonal:
                        str_actype = "Personal";


                        tilUnmBusiness.setVisibility(View.GONE);
                        break;

                    case R.id.rbBusiness:
                        str_actype = "Business";


                        tilUnmBusiness.setVisibility(View.VISIBLE);

                        break;
                }
            }
        });

        if (from.equalsIgnoreCase("addshop")) {
            tilShoNm.setVisibility(View.VISIBLE);
            rbBusiness.setChecked(true);
        } else {
            tilShoNm.setVisibility(View.GONE);


            if (getString(R.string.app_name_condition).equalsIgnoreCase(getString(R.string.b1_digimart))) {
                rbBusiness.setChecked(true); // digimart
            } else {
                rbPersonal.setChecked(true); // digicafe, texvalley, omantex
            }
        }
    }

    String u_name = "";

    public void userregistration() {

        dialog = new ProgressDialog(this);
        if (dialog != null && !dialog.isShowing()) {
            dialog.setMessage("Signing Up ...");
            dialog.show();
        }

        ApiInterface apiService =
                ApiClient.getClient(SignupActivity.this).create(ApiInterface.class);
        //str_mobile = str_mobile+"@"+sessionManager.getAggregator_ID()+".cr";

        //u_name = str_mobile + "@" + sessionManager.getAggregator_ID() + ".cr";
        if (str_actype.equals("Personal")) {
            u_name = str_mobile + "@" + sessionManager.getAggregator_ID() + ".cr";
        } else {

            u_name = str_bussinsee_user_id;
        }

        JSONObject joFieldPass = new JSONObject();


        // for personal / business
        Call call = apiService.getUserRegistration(str_actype, str_firstname, str_lastname, str_latitude, str_longtitude, str_area, str_mobile, str_email, u_name, str_password, str_confirmpassword, str_newsletter, str_terms, str_countrycode, joFieldPass);

        if (from.equals("addshop")) {
            // for add nw
            currency_code = ShareModel.getI().userprofile.getCurrency_code();
            currency_code_id = ShareModel.getI().userprofile.getCurrency_code_id();

            try {
                JSONObject joField = new JSONObject(field_json_data);

                joField.put("username", str_bussinsee_user_id);

                field_json_data = joField.toString();

            } catch (JSONException e) {
                e.printStackTrace();
            }

            call = apiService.setRegForAddNw(str_actype, str_firstname, str_lastname, str_latitude, str_longtitude, str_area, str_mobile, str_email, u_name, str_password, str_confirmpassword, str_newsletter, str_terms, str_countrycode, currency_code, currency_code_id, stringBuilder.toString(), business_status, field_json_data);

        } else {

        }

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                dialog.dismiss();

                int statusCode = response.code();
                Log.e("response_message", "" + statusCode);

                if (response.isSuccessful()) {
                   /* isregister = true;
                    findViewById(R.id.ll_before).setVisibility(View.GONE);
                    findViewById(R.id.ll_after).setVisibility(View.VISIBLE);
*/
                    if (from.equals("addshop")) {
                        Intent returnIntent = new Intent();
                        returnIntent.putExtra("username", str_bussinsee_user_id);
                        setResult(Activity.RESULT_OK, returnIntent);
                        finish();
                    } else {
                        Intent i = new Intent(SignupActivity.this, LoginActivity.class);
                        if (str_actype.equals("Personal")) {
                            i.putExtra("username", str_mobile);
                        } else {
                            i.putExtra("username", str_bussinsee_user_id);
                        }


                        i.putExtra("isfromsignup", true);
                        startActivity(i);
                        finish();
                    }


                    Toast.makeText(SignupActivity.this, "Register Successfully..", Toast.LENGTH_LONG).show();


                   /* Intent i = new Intent(SignupActivity.this, LoginActivity.class);
                    i.putExtra("username", str_mobile);
                    startActivity(i);*/
                } else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Log.e("response_message", jObjError.toString());

                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        JSONObject jObjErrordetails = jObjErrorresponse.getJSONObject("errordetail");
                        String username = jObjErrordetails.getString("username");

                        Inad.alerter("Alert", "Username already register!", SignupActivity.this);


                        //Toast.makeText(SignupActivity.this, jObjErrordetails.toString(), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(SignupActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }

                //  List<Movie> movies = response.body().getResults();
                //  recyclerView.setAdapter(new MoviesAdapter(movies, R.layout.list_item_movie, getApplicationContext()));
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // Log error here since request failed
                // Log.e("error message", t.toString());
                dialog.dismiss();

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void locationinitialized() {
        findViewById(R.id.iv_clear).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv_google_loc.setText("");
            }
        });
        rb_manual = (RadioButton) findViewById(R.id.rb_manual);
        rb_current = (RadioButton) findViewById(R.id.rb_current);
        rg_loc = (RadioGroup) findViewById(R.id.rg_loc);
        ll_google = (LinearLayout) findViewById(R.id.ll_google);
        ll_manual_loc = (LinearLayout) findViewById(R.id.ll_manual_loc);
        tv_google_loc = (AutoCompleteTextView) findViewById(R.id.tv_google_loc);
        tv_google_loc.setThreshold(1);
        tv_google_loc.setOnItemClickListener(mAutocompleteClickListener);


        tv_google_loc.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count > 0) {
                    findViewById(R.id.iv_clear).setVisibility(View.VISIBLE);
                } else {
                    findViewById(R.id.iv_clear).setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Places.GEO_DATA_API)
                .enableAutoManage(this, GOOGLE_API_CLIENT_ID, this)
                .addConnectionCallbacks(this)
                .build();


        mPlaceArrayAdapter = new PlaceArrayAdapter(this, android.R.layout.simple_list_item_1,
                BOUNDS_MOUNTAIN_VIEW, null);
        tv_google_loc.setAdapter(mPlaceArrayAdapter);


        rg_loc.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                str_area = "";
                switch (checkedId) {
                    case R.id.rb_manual:
                        ll_google.setVisibility(View.GONE);
                        ll_manual_loc.setVisibility(View.VISIBLE);
                        break;
                    case R.id.rb_current:
                        ll_manual_loc.setVisibility(View.GONE);
                        ll_google.setVisibility(View.VISIBLE);
                        break;

                }


            }
        });

        if (getString(R.string.isgoogleplace).equals("1")) {

        } else {
            rb_manual.setChecked(true);
            rb_current.setVisibility(View.GONE);
        }


    }

    private AdapterView.OnItemClickListener mAutocompleteClickListener
            = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            final PlaceArrayAdapter.PlaceAutocomplete item = mPlaceArrayAdapter.getItem(position);
            final String placeId = String.valueOf(item.placeId);
            //Log.i(LOG_TAG, "Selected: " + item.description);
            PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                    .getPlaceById(mGoogleApiClient, placeId);
            placeResult.setResultCallback(mUpdatePlaceDetailsCallback);
            //Log.i(LOG_TAG, "Fetching details for ID: " + item.placeId);
        }
    };


    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback
            = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {

                return;
            }
            // Selecting the first object buffer.
            final Place place = places.get(0);
            CharSequence attributions = places.getAttributions();


            //tv_google_loc.setText(Html.fromHtml(place.getAddress() + ""));

        }
    };

    @Override
    public void onConnected(Bundle bundle) {
        mPlaceArrayAdapter.setGoogleApiClient(mGoogleApiClient);


    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {


        Toast.makeText(this,
                "Google Places API connection failed with error code:" +
                        connectionResult.getErrorCode(),
                Toast.LENGTH_LONG).show();
    }

    @Override
    public void onConnectionSuspended(int i) {
        mPlaceArrayAdapter.setGoogleApiClient(null);
    }


    private GoogleApiClient mGoogleApiClient;
    private PlaceArrayAdapter mPlaceArrayAdapter;
    private static final LatLngBounds BOUNDS_MOUNTAIN_VIEW = new LatLngBounds(
            new LatLng(37.398160, -122.180831), new LatLng(37.430610, -121.972090));
    private static final int GOOGLE_API_CLIENT_ID = 0;


    public void Login(String u_nm, String pwd) {

        ApiInterface apiService =
                ApiClient.getClient2(SignupActivity.this).create(ApiInterface.class);


        Call call = apiService.getUserLogin(u_nm, pwd);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {

                if (response.isSuccessful()) {

                    try {


                       /* String cookie = response.headers().get("Set-Cookie");
                        String[] cookies = cookie.split(";");
                        sessionManager.create_Usersession(cookies[0]);
                        setprofile();*/
                    } catch (Exception e) {

                    }


                } else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Toast.makeText(SignupActivity.this, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(SignupActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }

            }

            @Override
            public void onFailure(Call call, Throwable t) {

            }
        });
    }


    private ProgressDialog dialog;

    public void usersignin() {


        ApiInterface apiService =
                ApiClient.getClient(SignupActivity.this).create(ApiInterface.class);


        Call call;

        call = apiService.getagregatorProfile(sessionManager.getAggregator_ID());

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                int statusCode = response.code();
                //  String cookie = response.;
                //  Log.d("test", cookie+ " ");

                if (response.isSuccessful()) {

                    try {
                        String a = new Gson().toJson(response.body());
                        JSONObject Jsonresponse = new JSONObject(a);
                        JSONObject Jsonresponseinfo = Jsonresponse.getJSONObject("response");


                        Log.e("test", " trdds" + Jsonresponse.toString());
                        JSONArray aggregatorobj = Jsonresponseinfo.getJSONArray("data");
                        JSONObject aggregator = aggregatorobj.getJSONObject(0);
                        String aggregatorbridgeid = aggregator.getString("bridge_id");
                        String isactive = aggregator.getString("business_status");
                        sessionManager.setAggregatorprofile(aggregatorbridgeid);
                        Log.e("aggregatorbridgeid", aggregatorbridgeid);

                        if (aggregator.has("company_detail_long")) {
                            String servicearea = aggregator.getString("company_detail_long");
                            sessionManager.setservicearea(servicearea);
                        }


                        planets = new ArrayList();

                        planets.add("Select");
                        String serviceareas = sessionManager.getservicearea();
                        String[] planetslist = serviceareas.split("\\|");
                        Log.e("arraylength", "" + sessionManager.getservicearea());
                        List<String> wordList = Arrays.asList(planetslist);
                        for (String e : wordList) {
                            planets.add(e.trim());
                        }
                        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(SignupActivity.this, R.layout.spinner_item, planets);

                        // ArrayAdapter adapter = ArrayAdapter.createFromResource(this, R.array.planets, R.layout.spinner_item);
                        dataAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
                        spinner.setAdapter(dataAdapter);

                        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                                // your code here
                                if (position > 0) {
                                    spin_area = planets.get(position).toString();

                                } else {
                                    spin_area = "";

                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parentView) {
                                // your code here
                            }

                        });


                    } catch (Exception e) {
                        Log.e("test", " trdds" + e.getLocalizedMessage());

                    }
                } else {
                    Log.e("test", " trdds1");

                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Toast.makeText(SignupActivity.this, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        // Toast.makeText(CatelogActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }

                //  List<Movie> movies = response.body().getResults();
                //  recyclerView.setAdapter(new MoviesAdapter(movies, R.layout.list_item_movie, getApplicationContext()));
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // Log error here since request failed
                Log.e("error message", t.toString());
            }
        });
    }

    ArrayList planets;
    Spinner spinner;


    ArrayList<SupplierCategory> alCategory = new ArrayList<>();
    CategoryAddEditListAdapter categoryAddEditListAdapter;

    boolean isLinear = false;
    String business_status = "", currency_code = "", field_json_data = "", currency_code_id = "";
    StringBuilder stringBuilder = new StringBuilder();

    public void setCategoryRv() {

        alCategory = new Gson().fromJson(ShareModel.getI().alCategory, new TypeToken<ArrayList<SupplierCategory>>() {
        }.getType());


      /*  String[] separated = category.split(" ");

        for (int i = 0; i < alCategory.size(); i++) {

            for(int j=0;j<separated.length;j++){

                if (separated[j].equalsIgnoreCase(alCategory.get(i).getPricelistCategoryName())) {
                    alCategory.get(i).setChecked(true);
                    break;
                }
                else {
                    alCategory.get(i).setChecked(false);
                }

            }
        }
*/

        if (alCategory.size() > 0 && alCategory.get(0).getPricelistCategoryName().equalsIgnoreCase("All"))
            alCategory.remove(0);

        categoryAddEditListAdapter = new CategoryAddEditListAdapter(alCategory, this, new CategoryAddEditListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(SupplierCategory item) {
                stringBuilder = new StringBuilder();
                for (int i = 0; i < alCategory.size(); i++) {
                    if (alCategory.get(i).isChecked()) {
                        if (stringBuilder.toString().isEmpty()) {
                            stringBuilder.append("" + alCategory.get(i).getPricelistCategoryName());
                        } else {
                            stringBuilder.append(" " + alCategory.get(i).getPricelistCategoryName());
                        }
                    }

                }
                tvSelectCat.setText("Select Category : " + stringBuilder + "");
            }
        });

        if (isLinear) {
            rvCat.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        } else {
            FlowLayoutManager layoutManager = new FlowLayoutManager();
            layoutManager.setAutoMeasureEnabled(true);
            layoutManager.setAlignment(Alignment.LEFT);
            rvCat.setLayoutManager(layoutManager);
        }
        //

        rvCat.setItemAnimator(new DefaultItemAnimator());
        rvCat.setAdapter(categoryAddEditListAdapter);
        rvCat.setNestedScrollingEnabled(false);

    }

}
