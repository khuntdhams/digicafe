package com.cab.digicafe.PrintView;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.andremion.counterfab.CounterFab;
import com.cab.digicafe.BaseActivity;
import com.cab.digicafe.Helper.Inad;
import com.cab.digicafe.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PrintOTPActivity extends BaseActivity {


    @BindView(R.id.counter_fab)
    CounterFab counterFab;

    @BindView(R.id.et_f_search)
    EditText et_f_search;

    @BindView(R.id.rl_search_btn)
    RelativeLayout rl_search_btn;


    public static boolean isback = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_search);
        getLayoutInflater().inflate(R.layout.activity_search, frameLayout);

        isback = false;

        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Print");

        counterFab.setVisibility(View.GONE);


        et_f_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 4) {
                    rl_search_btn.setVisibility(View.VISIBLE);
                } else {

                    //rl_search_btn.setVisibility(View.GONE);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        rl_search_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int length = et_f_search.getText().toString().trim().length();
                if (length == 4) {
                    Intent i = new Intent(PrintOTPActivity.this, PrintlistActivity.class);
                    i.putExtra("otp", et_f_search.getText().toString().trim());
                    startActivity(i);
                } else {
                    Inad.alerter("Alert", "Enter minimum 4 digit to search..!", PrintOTPActivity.this);

                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
           /* AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.AlertDialogCustom));
            builder
                    .setMessage(getString(R.string.quit))
                    .setTitle("")
                    .setCancelable(false)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            finish();
                            Intent intent = new Intent(Intent.ACTION_MAIN);
                            intent.addCategory(Intent.CATEGORY_HOME);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        }
                    })
                    .setNegativeButton("No", null)

                    .show();*/

            Inad.exitalert(this);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isback) {
            et_f_search.setText("");
        }
    }
}
