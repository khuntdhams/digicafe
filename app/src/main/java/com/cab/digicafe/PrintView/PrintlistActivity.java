package com.cab.digicafe.PrintView;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cab.digicafe.Activities.SearchActivity;
import com.cab.digicafe.Adapter.MyTaskAdapter;
import com.cab.digicafe.ChitlogActivity;
import com.cab.digicafe.Database.SettingDB;
import com.cab.digicafe.Database.SettingModel;
import com.cab.digicafe.DetailsFragment;
import com.cab.digicafe.Fragments.SummaryDetailFragmnet;
import com.cab.digicafe.Helper.Inad;
import com.cab.digicafe.Helper.RecyclerItemTouchHelper3;
import com.cab.digicafe.Helper.SessionManager;
import com.cab.digicafe.Model.Chit;
import com.cab.digicafe.Model.ChitDetails;
import com.cab.digicafe.Model.Chitproducts;
import com.cab.digicafe.MyCustomClass.GetProductDetail;
import com.cab.digicafe.MyCustomClass.PrintReceipt;
import com.cab.digicafe.R;
import com.cab.digicafe.Rest.ApiClient;
import com.cab.digicafe.Rest.ApiInterface;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Headers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PrintlistActivity extends PrintReceipt implements MyTaskAdapter.OnItemClickListener, RecyclerItemTouchHelper3.RecyclerItemTouchHelperListener, SwipyRefreshLayout.OnRefreshListener {

    @BindView(R.id.tv_nodata)
    TextView tv_nodata;

    @BindView(R.id.rv_search)
    RecyclerView rv_search;

    @BindView(R.id.rl_pb)
    RelativeLayout rl_pb;

    @BindView(R.id.et_f_search)
    EditText et_f_search;

    @BindView(R.id.rl_search_btn)
    RelativeLayout rl_search_btn;

    @BindView(R.id.rl_invoicesearch)
    RelativeLayout rl_invoicesearch;


    MyTaskAdapter mAdapter;
    List<Chit> orderlist = new ArrayList<>();
    SessionManager sessionManager;
    String otp = "";
    boolean issearchalltask = false;
    SettingModel sm = new SettingModel();

    @Override
    public void onTouchSwipe(boolean isSwipeEnable) {
        mSwipyRefreshLayout.setEnabled(isSwipeEnable);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_printlist);
        ButterKnife.bind(this);

        SettingDB db = new SettingDB(this);

        List<SettingModel> list_setting = db.GetItems();
        sm = new SettingModel();
        sm = list_setting.get(0);

        if (sm.getIssearchalltask().equals("1")) {
            issearchalltask = true; // by default
        } else {
            issearchalltask = false;
        }


        rl_invoicesearch.setVisibility(View.GONE);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mSwipyRefreshLayout = (SwipyRefreshLayout) findViewById(R.id.swipyrefreshlayout);
        mSwipyRefreshLayout.setDirection(SwipyRefreshLayoutDirection.BOTH);
        mSwipyRefreshLayout.setOnRefreshListener(this);


        sessionManager = new SessionManager(this);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        rv_search.setLayoutManager(mLayoutManager);
        //recyclerView.setItemAnimator(new DefaultItemAnimator());

        mAdapter = new MyTaskAdapter(orderlist, this, this, "search", false);
        if (sm.getIsprintdisplaydata().equals("1")) rv_search.setAdapter(mAdapter);
        rv_search.setHasFixedSize(true);
        new ItemTouchHelper(new RecyclerItemTouchHelper3(0, 0, this)).attachToRecyclerView(rv_search);

        Bundle ii = getIntent().getExtras();

        otp = ii.getString("otp", "");
        getSupportActionBar().setTitle(otp);


        et_f_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 4) {
                    rl_search_btn.setVisibility(View.VISIBLE);
                } else {

                    //rl_search_btn.setVisibility(View.GONE);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        rl_search_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int length = et_f_search.getText().toString().trim().length();
                if (length == 4) {
                    for (int i = 0; i < orderlist.size(); i++) {
                        String mobile = orderlist.get(i).getContact_number();
                        mobile = mobile.substring(mobile.length() - 4);
                        if (mobile.equalsIgnoreCase(et_f_search.getText().toString())) {

                            rl_invoicesearch.setVisibility(View.GONE);
                            rv_search.setVisibility(View.VISIBLE);
                            tv_nodata.setVisibility(View.GONE);
                            List<Chit> temp_orderlist = new ArrayList<>();
                            temp_orderlist.add(orderlist.get(i));
                            chitprint = temp_orderlist.get(i);
                            mAdapter = new MyTaskAdapter(temp_orderlist, PrintlistActivity.this, PrintlistActivity.this, "search", false);
                            if (sm.getIsprintdisplaydata().equals("1"))
                                rv_search.setAdapter(mAdapter);
                            rv_search.setHasFixedSize(true);
                            getp_detail();
                            break;
                        } else {
                            Inad.alerter("Alert", "Contact number can not found..!", PrintlistActivity.this);

                        }
                    }
                } else {
                    Inad.alerter("Alert", "Enter last 4 digit of contact number to search..!", PrintlistActivity.this);

                }
            }
        });


        load();

    }


    // rv
    @Override
    public void onItemClick(Chit item) {

        Intent i = new Intent(this, ChitlogActivity.class);
        i.putExtra("chit_hash_id", item.getChit_hash_id());
        i.putExtra("chit_id", item.getChit_id() + "");
        i.putExtra("transtactionid", item.getChit_name() + "");
        i.putExtra("productqty", item.getTotal_chit_item_value() + "");
        i.putExtra("productprice", item.getChit_item_count() + "");
        i.putExtra("purpose", item.getPurpose());
        i.putExtra("Refid", item.getRef_id());
        i.putExtra("Caseid", item.getCase_id());
        DetailsFragment.chititemcount = "" + item.getChit_item_count();
        DetailsFragment.chititemprice = "" + item.getTotal_chit_item_value();
        SummaryDetailFragmnet.item = item;
        DetailsFragment.item_detail = item;
        //startActivity(i);

        String BILL = "";


        // print();

    }

    @Override
    public void onItemLongClick(Chit item, int pos) {

    }

    // swipe
    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {

        if (viewHolder instanceof MyTaskAdapter.MyViewHolder) {


            final Chit deletedItem = orderlist.get(viewHolder.getAdapterPosition());
            final int deletedIndex = viewHolder.getAdapterPosition();
            mAdapter.removeItem(viewHolder.getAdapterPosition());

            String actionid = String.valueOf(deletedItem.getChit_id());
            actionid = actionid + ",";

            String actionCode = "";

            String msg = "";


            if (direction == ItemTouchHelper.RIGHT) {
                actionCode = "5";
            }

            final String f_actionid = actionid;
            final String f_actionCode = actionCode;

            assignarchived(sessionManager.getsession(), actionid, sessionManager.getE_bridgeid(), msg, new MyCallback() {
                @Override
                public void callbackCall(List<Chit> filteredModelList) {

                }

                @Override
                public void callback_remove() {

                    removearchived(sessionManager.getsession(), f_actionid, true, f_actionCode);
                }
            });


        }

    }


    public List<Chit> searchMytask(String usersession, String q, String filter, final MyCallback listener) {

        final List<Chit> filteredModelList = new ArrayList<>();

        ApiInterface apiService =
                ApiClient.getClient(this).create(ApiInterface.class);
        Call call = null;

        if (issearchalltask) {
            call = apiService.searchAlltask(usersession, q, filter, pagecount);
        } else {
            call = apiService.searchMytask(usersession, q, filter, pagecount);
        }


        rl_pb.setVisibility(View.VISIBLE);

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                mSwipyRefreshLayout.setRefreshing(false);
                rl_pb.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    try {
                        String a = new Gson().toJson(response.body());

                        JSONObject jObjRes = new JSONObject(a);
                        JSONObject jObjdata = jObjRes.getJSONObject("response");
                        JSONObject jObjResponse = jObjdata.getJSONObject("data");
                        String totalRecord = jObjResponse.getString("totalRecord");
                        JSONArray jObjcontacts = jObjResponse.getJSONArray("content");


                        String displayEndRecord = jObjResponse.getString("displayEndRecord");
                        if (totalRecord.equals(displayEndRecord)) {
                            isdataavailable = false;
                        } else {
                            isdataavailable = true;
                        }

                        for (int i = 0; i < jObjcontacts.length(); i++) {
                            if (jObjcontacts.get(i) instanceof JSONObject) {
                                JSONObject jsnObj = (JSONObject) jObjcontacts.get(i);
                                Chit obj = new Gson().fromJson(jsnObj.toString(), Chit.class);

                                filteredModelList.add(obj);
                                //filteredModelList.add(obj);
                            }
                        }

                        listener.callbackCall(filteredModelList);

                        setcte(null, "", totalRecord);

                    } catch (Exception e) {
                        listener.callback_remove();
                        Toast.makeText(PrintlistActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                } else {
                    listener.callback_remove();

                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Toast.makeText(PrintlistActivity.this, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(PrintlistActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }


            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // Log error here since request failed  mSwipyRefreshLayout.setRefreshing(false);
                mSwipyRefreshLayout.setRefreshing(false);
                rl_pb.setVisibility(View.GONE);
                listener.callback_remove();
                Log.e("error message", t.toString());
            }
        });
        return filteredModelList;

    }


    public void setcte(List<Chit> orderlist, String tab, String tt) {

        int newtask = 0, closetask = 0;


        try {
            /*for (int i = 0; i < orderlist.size(); i++) {
                String t_status = orderlist.get(i).getTransaction_status();


                if (t_status.equalsIgnoreCase("ACTIVE") || t_status.equalsIgnoreCase("ACCEPTED") || t_status.equalsIgnoreCase("HOLD") || t_status.equalsIgnoreCase("INPROGRESS")) {
                    newtask++;
                }

                if (t_status.equalsIgnoreCase("FINISHED") || t_status.equalsIgnoreCase("CLOSED") || t_status.equalsIgnoreCase("Completed") || t_status.equalsIgnoreCase("Withdrawn")) {
                    closetask++;
                }
            }
            if (tab.equals("new_all")) {
                openstatus.setText("Open (" + newtask + ")");
            } else {
                Close_.setText("Close (" + closetask + ")");
            }*/

            if (tab.equalsIgnoreCase("new_all")) {

                // openstatus.setText("Open [ " + tt + " ]");
                // Close_.setText("Close");
            } else {
                // openstatus.setText("Open");
                // Close_.setText("Close [ " + tt + " ]");
            }

        /*    if (tab.equalsIgnoreCase("new")) {

                rb_newtask.setText("Open (" + tt + ")");
            } else if (tab.equalsIgnoreCase("inprogress")) {
                rb_inprogtask.setText("Progress (" + tt + ")");
            } else if (tab.equalsIgnoreCase("close")) {
                rb_close.setText("Close (" + tt + ")");
            }*/


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    public void removearchived(final String usersession, String chitIds, final boolean isswipe, String actionCode) {

        ApiInterface apiService =
                ApiClient.getClient(PrintlistActivity.this).create(ApiInterface.class);
        JsonObject a1 = new JsonObject();
        try {

            a1.addProperty("chitIds", chitIds);
            a1.addProperty("actionCode", actionCode);


        } catch (JsonParseException e) {
            e.printStackTrace();
        }
        Call call = apiService.removechit(usersession, a1);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                int statusCode = response.code();

                if (response.isSuccessful()) {
                    try {
                        Log.e("response.body()", response.body().toString());
                        String a = new Gson().toJson(response.body());

                        JSONObject jo_res = new JSONObject(a);
                        String response_ = jo_res.getString("response");
                        JSONObject jo_txt = new JSONObject(response_);
                        String actionTxt = jo_txt.getString("actionTxt");


                        Toast.makeText(PrintlistActivity.this, actionTxt, Toast.LENGTH_LONG).show();


                    } catch (Exception e) {
                        Log.e("errortest", e.getMessage());

                    }

                } else {
                    Log.e("test", " trdds1");

                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Toast.makeText(PrintlistActivity.this, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(PrintlistActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }


                pagecount = 1;
                load();

            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // Log error here since request failed
                Log.e("error message", t.toString());
            }
        });
    }


    public void assignarchived(final String usersession, String chitIds, String employee_bridge_id, final String msg, final MyCallback listener) {

        ApiInterface apiService =
                ApiClient.getClient(PrintlistActivity.this).create(ApiInterface.class);
        JsonObject a1 = new JsonObject();
        try {

            a1.addProperty("chitIds", chitIds);
            a1.addProperty("employee_bridge_id", employee_bridge_id);


        } catch (JsonParseException e) {
            e.printStackTrace();
        }
        //   Toast.makeText(MyTaskActivity.this, employee_bridge_id, Toast.LENGTH_LONG).show();

        //Call call = apiService.removechit(usersession,chitIds+",","10");
        Call call = apiService.assigntask(usersession, a1);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                int statusCode = response.code();


                Headers headerList = response.headers();
                Log.d("testhistory", statusCode + " ");

                if (response.isSuccessful()) {
                    try {
                        Log.e("response.body()", response.body().toString());
                        String a = new Gson().toJson(response.body());

                        if (!msg.equalsIgnoreCase("")) {
                            Toast.makeText(PrintlistActivity.this, msg, Toast.LENGTH_LONG).show();
                        }

                        listener.callback_remove();


                    } catch (Exception e) {
                        Log.e("errortest", e.getMessage());

                    }

                } else {
                    Log.e("test", " trdds1");

                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Toast.makeText(PrintlistActivity.this, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(PrintlistActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }


            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // Log error here since request failed
                Log.e("error message", t.toString());
            }
        });
    }


    public void load() {
        searchMytask(sessionManager.getsession(), otp, "", new MyCallback() {
            @Override
            public void callbackCall(List<Chit> filteredModelList) {
                if (pagecount == 1) {
                    orderlist.clear();
                }

                if (filteredModelList != null && filteredModelList.size() == 0) {
                    getSupportActionBar().setTitle(otp + "");
                    rv_search.setVisibility(View.GONE);
                    tv_nodata.setVisibility(View.VISIBLE);
                    rl_invoicesearch.setVisibility(View.GONE);
                    final Handler handler = new Handler();
                    try {

                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                // Do something after 5s = 5000ms
                                SearchActivity.isback = true;
                                finish();
                            }
                        }, 2000);

                    } catch (Exception e) {
                        finish();
                    }


                } else if (filteredModelList != null && filteredModelList.size() > 1) {
                    rv_search.setVisibility(View.GONE);
                    tv_nodata.setVisibility(View.GONE);
                    rl_invoicesearch.setVisibility(View.VISIBLE);

                    Inad.alerter("Alert", "Duplicate token found, enter the last 4 digit of the phone number...!", PrintlistActivity.this);


                } else {
                    rv_search.setVisibility(View.VISIBLE);
                    tv_nodata.setVisibility(View.GONE);
                    rl_invoicesearch.setVisibility(View.GONE);
                }
                if (filteredModelList != null && filteredModelList.size() == 1) {
                    rl_pb.setVisibility(View.GONE);
                    chitprint = filteredModelList.get(0);
                    getp_detail();
                }

                orderlist.addAll(filteredModelList);
                mAdapter.setFilter(filteredModelList);
                int size = filteredModelList.size();
                getSupportActionBar().setTitle(otp + "");
                //getSupportActionBar().setTitle(otp+" [ "+size+" ]");


            }

            @Override
            public void callback_remove() {

                rv_search.setVisibility(View.GONE);
                tv_nodata.setVisibility(View.VISIBLE);
            }
        });
    }

    int pagecount = 1;
    SwipyRefreshLayout mSwipyRefreshLayout;
    boolean isdataavailable = true;
    int pagesize = 50;

    // load more
    @Override
    public void onRefresh(SwipyRefreshLayoutDirection direction) {

        mSwipyRefreshLayout.setRefreshing(false);
        /*if (direction == SwipyRefreshLayoutDirection.TOP) {
            isdataavailable = true;
            pagecount = 1;
            load();
        } else if (direction == SwipyRefreshLayoutDirection.BOTTOM) {
            if (isdataavailable) {
                pagecount++;
                load();
            } else {
                mSwipyRefreshLayout.setRefreshing(false);
                Toast.makeText(this, "No more data available", Toast.LENGTH_LONG).show();
            }

        }
*/

    }


    interface MyCallback {
        void callbackCall(List<Chit> filteredModelList);

        void callback_remove();
    }

    private MyCallback listener;

    @Override
    protected void onResume() {
        super.onResume();


    }

    @Override
    public void onBackPressed() {


        if (orderlist.size() == 2 && rv_search.getVisibility() == View.VISIBLE) {
            tv_nodata.setVisibility(View.GONE);
            rl_invoicesearch.setVisibility(View.VISIBLE);
            rv_search.setVisibility(View.GONE);
        } else {

            super.onBackPressed();
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

    }


    String address = "", symbol = "";

    public void getuserprofile() {


        ApiInterface apiService = ApiClient.getClient(PrintlistActivity.this).create(ApiInterface.class);
        Call call;

        call = apiService.getagregatorProfile(sessionManager.getAggregator_ID());

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {

                rl_pb.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    String a = new Gson().toJson(response.body());
                    Log.e("response", a);
                    try {
                        JSONObject jObjRes = new JSONObject(a);
                        JSONObject jObjdata = jObjRes.getJSONObject("response");
                        JSONArray aggregatorobj = jObjdata.getJSONArray("data");

                        JSONObject aggregator = aggregatorobj.getJSONObject(0);

                        String company_image = aggregator.getString("company_image");
                        String company_name = aggregator.getString("company_name");
                        String company_detail_short = aggregator.getString("company_detail_short");
                        String location = aggregator.getString("location");
                        String contact_no = aggregator.getString("contact_no");

                        symbol = aggregator.getString("symbol");
                        //address = "" + company_name + "\n" + location + "\n" + "Ph-" + contact_no;
                        address = "" + company_name;

                        if (chitprint != null && productlist != null && productlist.size() > 0) {

                            String status = chitprint.getTransaction_status();

                            if (status.equalsIgnoreCase("ACTIVE") || status.equalsIgnoreCase("ACCEPTED") || status.equalsIgnoreCase("INPROGRESS")) {

                                //new USBAdapter("");

                                PrintReceiptStart(sessionManager.getsession(), address, chitprint, productlist, new Mycall() {
                                    @Override
                                    public void printsuccess() {
                                        Toast.makeText(PrintlistActivity.this, "Success!", Toast.LENGTH_LONG).show();
                                        Inad.showmsg(tv_nodata, "Success!", PrintlistActivity.this);
                                        afterprint_finished(getIntent(), true);
                                    }

                                    @Override
                                    public void printfail() {
                                        finish();
                                        Inad.showmsg(tv_nodata, "Fail!", PrintlistActivity.this);
                                    }

                                    @Override
                                    public void assignarchived() {

                                    }
                                }, symbol);


                            } else {
                                Toast.makeText(PrintlistActivity.this, "Print not availble for " + status + " item!", Toast.LENGTH_LONG).show();
                                finish();
                            }


                        } else {
                            Toast.makeText(PrintlistActivity.this, "Wait while getting details...", Toast.LENGTH_LONG).show();
                        }

                    } catch (JSONException e) {

                    }

                }

                //  List<Movie> movies = response.body().getResults();
                //  recyclerView.setAdapter(new MoviesAdapter(movies, R.layout.list_item_movie, getApplicationContext()));
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // Log error here since request failed
                rl_pb.setVisibility(View.GONE);

            }
        });
    }

    public void getp_detail() {
        rl_pb.setVisibility(View.VISIBLE);
        GetProductDetail getdet = new GetProductDetail(0, this, chitprint.getChit_hash_id(), chitprint.getChit_id() + "", new GetProductDetail.Apicallback() {
            @Override
            public void onGetResponse(ChitDetails a, boolean issuccess, int p) {
                //catererList.get(p).setChitDetails(a);
                //notifyDataSetChanged();
                chitDetails = a;
                productlist = new ArrayList<>();
                productlist = chitDetails.getContent();

                getuserprofile();
            }

            @Override
            public void onUpdate(boolean isupdateforce, String ischeck) {
                rl_pb.setVisibility(View.GONE);
            }
        });

    }

    Chit chitprint = null;
    ChitDetails chitDetails;
    ArrayList<Chitproducts> productlist = new ArrayList<>();


}
