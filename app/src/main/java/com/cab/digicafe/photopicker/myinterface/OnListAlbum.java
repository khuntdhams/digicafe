package com.cab.digicafe.photopicker.myinterface;


import com.cab.digicafe.photopicker.model.ImageModel;

public interface OnListAlbum {
    void OnItemListAlbumClick(ImageModel item);
}
