package com.cab.digicafe.Helper;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.cab.digicafe.Activities.RequestApis.MyRequestCall;
import com.cab.digicafe.App;
import com.cab.digicafe.Database.SettingDB;
import com.cab.digicafe.Model.ShareModel;
import com.cab.digicafe.SplashActivity;

/**
 * Created by RajeshKumar on 1/31/2017.
 */

public class SessionManager {
    private static final String PREF_NAME = "digicafe";
    SharedPreferences.Editor editor;
    Context _context;
    int PRIVATE_MODE = 0;
    SharedPreferences pref;
    public String KEY_SESSION = "sessiontoken";
    public String ISLOGIN = "ISLOGIN";
    public String Aggregator_ID = "Aggregator_ID";
    public String SERVICEAREA = "servicearea";
    public String industrySupplier = "industrySupplier";
    public String KEY_CARTCOUNT = "cartcount";
    public String KEY_CARTPRODUCT = "cartproduct";
    public String KEY_CURRENTCATERER = "currentcaterer";
    public String KEY_CURRENTCATERERNAME = "currentcaterername";
    public String KEY_CURRENTCATERERSTATUS = "currentcatererstatus";
    public String KEY_CURRENTCOMPANYNAME = "currentcompanyname";

    public String KEY_CURRENTCATERERALIASNAME = "currentcatereraliasname";
    public String KEY_USERPROFILE = "userprofile";
    public String KEY_AGGREGATORPROFILE = "aggregatorid";
    public String KEY_EMP_BRIDGE_ID = "emp_bridge_id";
    public String KEY_NAME = "name";
    public String KEY_EMAIL = "email";
    public String KEY_IMAGE = "image";
    public String KEY_FBAUTH = "fbauthuid";
    public String KEY_MOBILE = "mobile";
    public String DEVICE_TOKEN = "device_token";
    public String UNIVERSITY = "university";
    public String HEADER = "HEADER";
    public String APPENDPHONEWITHAGGREGATOR = "AppendPhoneWithAggregate";
    public String PASSWD = "PASSWD";
    public String DATE = "DATE";
    public String TIME = "TIME";
    public String ADDRESS = "ADDRESS";
    public String CUST_NM = "CUST_NM";
    public String Temp_Phone = "Temp_Phone";
    public String dobForSurvey = "dobForSurvey";
    public String CurrentUser = "CurrentUser";
    public String isbusiness = "isbusiness";
    public String loginuser = "loginuser";
    public String MyCookie = "MyCookie";
    public String BUSINESSSTATUS = "shop";
    public String industry_id = "industry_id";
    public String building_id = "building_id";
    public String userwithbuild_id_and_ind_id = "userwithbuild_id_and_ind_id";
    public String KEY_COMPIMAGE = "KEY_COMPIMAGE";
    public String currency_code = "currency_code";
    public String ProfileApi_user = "ProfileApi/user";
    public String userId = "userId";
    public String userBridgeId = "userBridgeId";
    public String firstname = "firstname";

    // Constructor
    public SessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public SessionManager() {
        pref = App.getAppContext().getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void create_Userdetails(String name, String email, String image) {
        editor.putString(KEY_NAME, name);
        editor.putString(KEY_EMAIL, email);
        editor.putString(KEY_IMAGE, image);
        editor.commit();
    }

    public String getUserwithbuild_id_and_ind_id() {
        return pref.getString(userwithbuild_id_and_ind_id, "");
    }

    public void setUserwithbuild_id_and_ind_id(String user) {
        editor.putString(userwithbuild_id_and_ind_id, user);
        editor.commit();
    }

    public String getProfUset() {
        return pref.getString(ProfileApi_user, "");
    }

    public void setProfUser(String user) {
        editor.putString(ProfileApi_user, user);
        editor.commit();
    }

    public String getUserId() {
        return pref.getString(userId, "");
    }

    public void setUserId(String user) {
        editor.putString(userId, user);
        editor.commit();
    }

    public String getBridgeUserId() {
        return pref.getString(userBridgeId, "");
    }

    public void setUserBridgeId(String user) {
        editor.putString(userBridgeId, user);
        editor.commit();
    }

    public String getFstNm() {
        return pref.getString(firstname, "");
    }

    public void setFstNm(String user) {
        editor.putString(firstname, user);
        editor.commit();
    }

    public void create_Usersession(String session) {
        editor.putString(KEY_SESSION, session);


        editor.commit();
    }

    public String getIndustry_id() {
        return pref.getString(industry_id, "1");
    }

    public void setIndustry_id(String industry) {
        editor.putString(industry_id, industry);
        editor.commit();
    }

    public String getBuilding_id() {
        return pref.getString(building_id, "");
    }

    public void setBuilding_id(String building) {
        editor.putString(building_id, building);
        editor.commit();
    }

    public void setbus_status(String catererid) {
        editor.putString(BUSINESSSTATUS, catererid);
        editor.commit();
    }

    public String getbusi_status() {
        //return pref.getString(BUSINESSSTATUS,_context.getString(R.string.app_name));
        return pref.getString(BUSINESSSTATUS, "");
    }


    public void setFBUID(String fbuid) {
        editor.putString(KEY_FBAUTH, fbuid);
        editor.commit();
    }

    public void update_device_token(String token) {
        editor.putString(DEVICE_TOKEN, token);
        editor.commit();

    }

    public void setcurrentcaterer(String catererid) {
        editor.putString(KEY_CURRENTCATERER, catererid);
        editor.commit();
    }

    public String getcurrentcaterername() {
        return pref.getString(KEY_CURRENTCATERERNAME, "");
    }

    public void setcurrentcaterername(String catererid) {
        editor.putString(KEY_CURRENTCATERERNAME, catererid);
        editor.commit();
    }

    public String getcurrentu_nm() {
        return pref.getString(CurrentUser, "");
    }

    public void setcurrentuname(String catererid) {
        editor.putString(CurrentUser, catererid);
        editor.commit();
    }


    public String getcurrentcatererstatus() {
        return pref.getString(KEY_CURRENTCATERERSTATUS, "");
    }

    public void setcurrentcatererstatus(String catererid) {
        editor.putString(KEY_CURRENTCATERERSTATUS, catererid);
        editor.commit();
    }

    public String getIndustrySupplier() {
        return pref.getString(industrySupplier, "");
    }

    public void setIndustrySupplier(String industry) {
        editor.putString(industrySupplier, industry);
        editor.commit();
    }

    public String getservicearea() {
        return pref.getString(SERVICEAREA, "");
    }

    public void setservicearea(String areaname) {
        editor.putString(SERVICEAREA, areaname);
        editor.commit();
    }

    public String getcurrentcatereraliasname() {
        return pref.getString(KEY_CURRENTCATERERALIASNAME, "");
    }

    public void setcurrentcatereraliasname(String aliasname) {
        editor.putString(KEY_CURRENTCATERERALIASNAME, aliasname);
        editor.commit();
    }

    public String getcurrentcaterer() {
        return pref.getString(KEY_CURRENTCATERER, "");
    }

    public String getDEVICE_TOKEN() {
        return pref.getString(DEVICE_TOKEN, "");
    }

    public String getemail() {
        return pref.getString(KEY_EMAIL, "");
    }

    public String getFBUID() {
        return pref.getString(KEY_FBAUTH, "");
    }

    public String getimage() {
        return pref.getString(KEY_IMAGE, "");
    }

    public String getname() {
        return pref.getString(KEY_NAME, "");
    }


    public String getsession() {
        return pref.getString(KEY_SESSION, "");
    }

    public int getCartcount() {
        return pref.getInt(KEY_CARTCOUNT, 0);
    }

    public void setCartcount(int qty) {
        editor.putInt(KEY_CARTCOUNT, qty);
        editor.commit();
    }

    public void setCartproduct(String cartlist) {
        editor.putString(KEY_CARTPRODUCT, cartlist);
        editor.commit();
    }

    public String getCartproduct() {
        return pref.getString(KEY_CARTPRODUCT, "");
    }

    public void setMobile(String mobile) {
        editor.putString(KEY_MOBILE, mobile);
        editor.commit();
    }


    public String getAggregatorprofile() {
        return pref.getString(KEY_AGGREGATORPROFILE, "");
    }


    public void setAggregatorprofile(String aggregatorprofile) {
        editor.putString(KEY_AGGREGATORPROFILE, aggregatorprofile);
        editor.commit();
    }

    public String getE_bridgeid() {
        return pref.getString(KEY_EMP_BRIDGE_ID, "");
    }


    public void setE_bridgeid(String b_id) {
        editor.putString(KEY_EMP_BRIDGE_ID, b_id);
        editor.commit();
    }


    public String getCompImg() {
        return pref.getString(KEY_COMPIMAGE, "");
    }


    public void setCompImg(String aggregatorprofile) {
        editor.putString(KEY_COMPIMAGE, aggregatorprofile);
        editor.commit();
    }

    public String getUserprofile() {
        return pref.getString(KEY_USERPROFILE, "");
    }


    public void setuserprofile(String userprofile) // from bridge id of login detail
    {
        editor.putString(KEY_USERPROFILE, userprofile);
        editor.commit();
    }

    public String getMobile() {
        return pref.getString(KEY_MOBILE, "");
    }

    public void logoutUser(final Context context, final IlogOut ilogOut) {

        final MyRequestCall myRequestCall = new MyRequestCall();

        myRequestCall.checkTraction(true, 0, context, new MyRequestCall.CallRequest() {
            @Override
            public void onGetResponse(String response) {

                ilogOut.logOut();

                editor.clear();
                editor.commit();

                myRequestCall.clearTraction(context);

                SettingDB db = new SettingDB(context);
                db.deleteall();

                ShareModel.getI().field_json_data_of_bus_for_emp = "";
                ShareModel.getI().whoReferMe = "";


                Intent mainIntent = new Intent(context, SplashActivity.class);
                mainIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                context.startActivity(mainIntent);

            }
        });


    }


    public void setRegister(String mobile) {
        editor.putString("Register", mobile);
        editor.commit();
    }

    public String getRegister() {
        return pref.getString("Register", "");
    }


    public String getEmail() {
        return pref.getString(KEY_EMAIL, "");
    }

    public String getAggregator_ID() {
        return pref.getString(Aggregator_ID, "");
    }

    public void setAggregator_ID(String aggregator_id) {
        editor.putString(Aggregator_ID, aggregator_id);
        editor.commit();
    }


    public void setRemark(String catererid) {
        editor.putString(HEADER, catererid);
        editor.commit();
    }

    public String getRemark() {
        return pref.getString(HEADER, "");
    }

    public void setUserWithAggregator(String catererid) {
        editor.putString(APPENDPHONEWITHAGGREGATOR, catererid);
        editor.commit();
    }

    public String getUserWithAggregator() {
        return pref.getString(APPENDPHONEWITHAGGREGATOR, "");
    }

    public void setPasswd(String catererid) {
        editor.putString(PASSWD, catererid);
        editor.commit();
    }

    public String getPasswd() {
        return pref.getString(PASSWD, "");
    }


    public void setDate(String catererid) {
        editor.putString(DATE, catererid);
        editor.commit();
    }

    public String getDate() {
        return pref.getString(DATE, "");
    }

    public void setAddress(String catererid) {
        editor.putString(ADDRESS, catererid);
        editor.commit();
    }

    public String getAddress() {
        return pref.getString(ADDRESS, "");
    }

    ///

    public void setCustNmForProperty(String catererid) {
        editor.putString(CUST_NM, catererid);
        editor.commit();
    }

    public String getCustNmForProperty() {
        return pref.getString(CUST_NM, "");
    }

    ///

    public void setTime(String catererid) {
        editor.putString(TIME, catererid);
        editor.commit();
    }

    public String getTime() {
        return pref.getString(TIME, "");
    }

    public String getT_Phone() {
        return pref.getString(Temp_Phone, "");
    }

    public void setT_Phone(String catererid) {
        editor.putString(Temp_Phone, catererid);
        editor.commit();
    }

    public String getDobForSurvey() {
        return pref.getString(dobForSurvey, "");
    }

    public void setDobForSurvey(String catererid) {
        editor.putString(dobForSurvey, catererid);
        editor.commit();
    }


    public boolean getisBusinesslogic() {
        return pref.getBoolean(isbusiness, false);
    }

    public void setisBusinesslogic(boolean business) {
        editor.putBoolean(isbusiness, business);
        editor.commit();
    }


    public String getLoginuser() {
        return pref.getString(loginuser, "");
    }

    public void setTLoginuser(String catererid) {
        editor.putString(loginuser, catererid);
        editor.commit();
    }


    public String getKEY_CURRENTCOMPANYNAME() {
        return pref.getString(KEY_CURRENTCOMPANYNAME, "");

    }

    public void setKEY_CURRENTCOMPANYNAME(String key) {
        editor.putString(KEY_CURRENTCOMPANYNAME, key);
        editor.commit();
    }


    public String getCurrency_code() {
        return pref.getString(currency_code, "");
    }

    public void setCurrency_code(String building) {
        editor.putString(currency_code, building);
        editor.commit();
    }

    public boolean getIsLogin() {
        return pref.getBoolean(ISLOGIN, false);
    }


    public void setIsLogin(boolean isLogin) {
        editor.putBoolean(ISLOGIN, isLogin);
        editor.commit();
    }

    public String isEmployee = "isEmployee";

    public boolean getIsEmployess() {
        return pref.getBoolean(isEmployee, false);
    }

    public void setIsEmployee(boolean isLogin) {
        editor.putBoolean(isEmployee, isLogin);
        editor.commit();
    }

    public String isConsumer = "isConsumer";

    public boolean getIsConsumer() {
        return pref.getBoolean(isConsumer, false);
    }

    public void setIsConsumer(boolean isLogin) {
        editor.putBoolean(isConsumer, isLogin);
        editor.commit();
    }

    IlogOut ilogOut;

    public interface IlogOut {
        public void logOut();
    }


}
