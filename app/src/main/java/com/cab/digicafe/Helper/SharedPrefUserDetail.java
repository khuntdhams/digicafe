package com.cab.digicafe.Helper;

import android.content.Context;
import android.content.SharedPreferences;


public class SharedPrefUserDetail {

    public static final String PREF_NAME = "PREF_PROFILE";

    @SuppressWarnings("deprecation")
    public static final int MODE = Context.MODE_PRIVATE;


    public static final String EMAIL_VERIFY = "EMAIL_VERIFY";
    public static final String EMAIL = "EMAIL";
    public static final String UPDATEOPENDATE = "UPDATEOPENDATE";
    public static final String loginuser = "loginuser";
    public static final String purpose = "purpose";  // Invoice - shop , Request - supplier
    public static final String purpose_mysupplier = "purpose_mysupplier";
    public static final String Mycookie = "Mycookie";
    public static final String userwithagreeid = "userwithagreeid";
    public static final String isfromsupplier = "isfromsupplier";
    public static final String isfrommyaggregator = "isfrommyaggregator";
    public static final String currency_code = "currency_code"; // INR
    public static final String symbol_native = "symbol_native";
    public static final String currency_code_id = "currency_code_id";
    public static final String symbol = "symbol";  // Rs.
    public static final String decimal_digits = "decimal_digits";  // Rs.

    public static final String chit_currency_code = "chit_currency_code"; // INR
    public static final String chit_symbol_native = "chit_symbol_native";
    public static final String chit_currency_code_id = "chit_currency_code_id";
    public static final String chit_symbol = "chit_symbol";  // Rs.
    public static final String infoMainShop = "infoMainShop";  // Rs.

    public static final String field_json_data = "field_json_data";  // Rs.
    public static final String field_json_data_info = "field_json_data_info";  // Rs.
    public static final String business_type = "business_type";  // Rs.
    public static final String user_traction_list = "user_traction_list";  // Rs.


    public static final String gold_today_price_catalogue = "gold_today_price";  // Rs.
    public static final String silver_today_price_catalogue = "silver_today_price";  // Rs.
    public static final String metal_list = "metal_list";  // Rs.

    public static final String isMrp = "isMrp";
    public static final String isAppHyperlocalMode = "appMode"; // My Supplier - shows under shop direct
    public static final String delivery_charge = "";

    public static void setBoolean(Context context, String key, boolean value) {
        getEditor(context).putBoolean(key, value).commit();
    }

    public static boolean getBoolean(Context context, String key,
                                     boolean defValue) {
        return getPreferences(context).getBoolean(key, defValue);
    }

    public static void setInteger(Context context, String key, int value) {
        getEditor(context).putInt(key, value).commit();
    }

    public static int getInteger(Context context, String key, int defValue) {
        return getPreferences(context).getInt(key, defValue);
    }

    public static void setString(Context context, String key, String value) {
        getEditor(context).putString(key, value).commit();

    }

    public static String getString(Context context, String key, String defValue) {
        return getPreferences(context).getString(key, defValue);
    }

    public static void setFloat(Context context, String key, float value) {
        getEditor(context).putFloat(key, value).commit();
    }

    public static float getFloat(Context context, String key, float defValue) {
        return getPreferences(context).getFloat(key, defValue);
    }

    public static void setLong(Context context, String key, long value) {
        getEditor(context).putLong(key, value).commit();
    }

    public static long getLong(Context context, String key, long defValue) {
        return getPreferences(context).getLong(key, defValue);
    }

    public static SharedPreferences getPreferences(Context context) {
        return context.getSharedPreferences(PREF_NAME, MODE);
    }

    public static SharedPreferences.Editor getEditor(Context context) {
        return getPreferences(context).edit();
    }

    public static void clearlogin(Context context) {
        getPreferences(context).edit().clear().commit();
    }

}
