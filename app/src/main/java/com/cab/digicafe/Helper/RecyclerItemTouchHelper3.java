package com.cab.digicafe.Helper;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.View;

import com.cab.digicafe.Adapter.MyTaskAdapter;

public class RecyclerItemTouchHelper3 extends ItemTouchHelper.SimpleCallback {
    private RecyclerItemTouchHelperListener listener;
    private Paint p = new Paint();
    String tag = "";

    public RecyclerItemTouchHelper3(int dragDirs, int swipeDirs, RecyclerItemTouchHelperListener listener) {
        super(dragDirs, swipeDirs);
        this.listener = listener;
    }


    @Override
    public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
        // Set movement flags based on the layout manager

        MyTaskAdapter myTaskAdapter = new MyTaskAdapter();

        String c_tab = myTaskAdapter.tab;
        boolean isbusiness = myTaskAdapter.isbusiness;

        if (c_tab.equalsIgnoreCase("close_all")) {
            return makeMovementFlags(0, ItemTouchHelper.LEFT);
        } else if (c_tab.equalsIgnoreCase("close")) {
            return makeMovementFlags(0, ItemTouchHelper.LEFT);
        } else if (c_tab.equalsIgnoreCase("search")) {
            return makeMovementFlags(0, ItemTouchHelper.RIGHT);
        } else if (c_tab.equalsIgnoreCase("new_all") && isbusiness) {
            return makeMovementFlags(0, ItemTouchHelper.RIGHT);
        } else if (c_tab.equalsIgnoreCase("closedtask")) {
            return makeMovementFlags(0, ItemTouchHelper.RIGHT);
        } else {
            return makeMovementFlags(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT);
        }
    }


    @Override
    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
        return true;
    }

    @Override
    public void onSelectedChanged(RecyclerView.ViewHolder viewHolder, int actionState) {
        if (viewHolder != null) {
            final View foregroundView = ((MyTaskAdapter.MyViewHolder) viewHolder).viewForeground;

            getDefaultUIUtil().onSelected(foregroundView);
        }
    }

    @Override
    public void onChildDrawOver(Canvas c, RecyclerView recyclerView,
                                RecyclerView.ViewHolder viewHolder, float dX, float dY,
                                int actionState, boolean isCurrentlyActive) {
        final View foregroundView = ((MyTaskAdapter.MyViewHolder) viewHolder).viewForeground;

        if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {

            if (dX > 0) {
                ((MyTaskAdapter.MyViewHolder) viewHolder).view_background_left.setVisibility(View.VISIBLE);
            } else {
                ((MyTaskAdapter.MyViewHolder) viewHolder).view_background_left.setVisibility(View.GONE);
            }


        }

        Log.e("asOnChildDrawOver", actionState + "");

        getDefaultUIUtil().onDrawOver(c, recyclerView, foregroundView, dX, dY, actionState, isCurrentlyActive);


    }

    @Override
    public void clearView(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {

        final View foregroundView = ((MyTaskAdapter.MyViewHolder) viewHolder).viewForeground;
        getDefaultUIUtil().clearView(foregroundView);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                // Do something after 5s = 5000ms
                listener.onTouchSwipe(true);

            }
        }, 1000);
        // super.clearView(recyclerView, viewHolder);

    }

    @Override
    public void onChildDraw(Canvas c, RecyclerView recyclerView,
                            RecyclerView.ViewHolder viewHolder, float dX, float dY,
                            int actionState, boolean isCurrentlyActive) {

        listener.onTouchSwipe(false);
        final View foregroundView = ((MyTaskAdapter.MyViewHolder) viewHolder).viewForeground;

        if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {

            if (dX > 0) {
                ((MyTaskAdapter.MyViewHolder) viewHolder).view_background_left.setVisibility(View.VISIBLE);
            } else {
                ((MyTaskAdapter.MyViewHolder) viewHolder).view_background_left.setVisibility(View.GONE);
            }


        } else if (actionState == ItemTouchHelper.ANIMATION_TYPE_SWIPE_CANCEL) {
            Log.e("SWIPE_CANCEL", "");
        }

        Log.e("asOnChildDraw", actionState + "");

        getDefaultUIUtil().onDraw(c, recyclerView, foregroundView, dX, dY, actionState, isCurrentlyActive);


    }

    @Override
    public int getSwipeDirs(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
        return super.getSwipeDirs(recyclerView, viewHolder);
    }


    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {

        listener.onSwiped(viewHolder, direction, viewHolder.getAdapterPosition());

    }

    @Override
    public int convertToAbsoluteDirection(int flags, int layoutDirection) {
        return super.convertToAbsoluteDirection(flags, layoutDirection);
    }

    public interface RecyclerItemTouchHelperListener {
        void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position);

        void onTouchSwipe(boolean isSwipeEnable);
    }


}
