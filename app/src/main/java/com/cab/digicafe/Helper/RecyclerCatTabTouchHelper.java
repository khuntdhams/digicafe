package com.cab.digicafe.Helper;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;

import com.cab.digicafe.Adapter.CatalogTabAdapter;

public class RecyclerCatTabTouchHelper extends ItemTouchHelper.SimpleCallback {
    private RecyclerItemTouchHelperListener1 listener;
    private Paint p = new Paint();

    public RecyclerCatTabTouchHelper(int dragDirs, int swipeDirs, RecyclerItemTouchHelperListener1 listener) {
        super(dragDirs, swipeDirs);
        this.listener = listener;
    }

    @Override
    public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
        // Set movement flags based on the layout manager

        return makeMovementFlags(0, ItemTouchHelper.LEFT);

    }

    boolean isPagerSwipe = true;

    @Override
    public boolean isItemViewSwipeEnabled() {
        return isPagerSwipe;
    }


    @Override
    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
        return true;
    }

    @Override
    public void onSelectedChanged(RecyclerView.ViewHolder viewHolder, int actionState) {
        if (viewHolder != null) {
            final View foregroundView = ((CatalogTabAdapter.MyViewHolder) viewHolder).viewForeground;

            getDefaultUIUtil().onSelected(foregroundView);
        }
    }

    @Override
    public void onChildDrawOver(Canvas c, RecyclerView recyclerView,
                                RecyclerView.ViewHolder viewHolder, float dX, float dY,
                                int actionState, boolean isCurrentlyActive) {
        final View foregroundView = ((CatalogTabAdapter.MyViewHolder) viewHolder).viewForeground;

        if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {

            if (dX > 0) {
                //((CatalogTabAdapter.MyViewHolder) viewHolder).view_background_right.setVisibility(View.GONE);
            } else {
                ((CatalogTabAdapter.MyViewHolder) viewHolder).view_background_right.setVisibility(View.VISIBLE);
            }


        }

        getDefaultUIUtil().onDrawOver(c, recyclerView, foregroundView, dX, dY, actionState, isCurrentlyActive);


    }

    @Override
    public void clearView(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
        final View foregroundView = ((CatalogTabAdapter.MyViewHolder) viewHolder).viewForeground;
        getDefaultUIUtil().clearView(foregroundView);
    }

    @Override
    public void onChildDraw(Canvas c, RecyclerView recyclerView,
                            RecyclerView.ViewHolder viewHolder, float dX, float dY,
                            int actionState, boolean isCurrentlyActive) {


        final View foregroundView = ((CatalogTabAdapter.MyViewHolder) viewHolder).viewForeground;
        final View rlMediaView = ((CatalogTabAdapter.MyViewHolder) viewHolder).rlMediaView;
        final View rl_base_cat = ((CatalogTabAdapter.MyViewHolder) viewHolder).rl_base_cat;
        if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {

            if (dX > 0) {

                //((CatalogTabAdapter.MyViewHolder) viewHolder).view_background_right.setVisibility(View.GONE);
            } else {

                ((CatalogTabAdapter.MyViewHolder) viewHolder).view_background_right.setVisibility(View.VISIBLE);
            }


        }
       /* rlMediaView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                isPagerSwipe = false;
                return true;
            }
        });
        rl_base_cat.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                isPagerSwipe = true;
                return true;
            }
        });
*/
        getDefaultUIUtil().onDraw(c, recyclerView, foregroundView, dX, dY, actionState, isCurrentlyActive);


    }


    @Override
    public int getSwipeDirs(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
        return super.getSwipeDirs(recyclerView, viewHolder);
    }


    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
        listener.onSwiped(viewHolder, direction, viewHolder.getAdapterPosition());
    }

    @Override
    public int convertToAbsoluteDirection(int flags, int layoutDirection) {
        return super.convertToAbsoluteDirection(flags, layoutDirection);
    }

    public interface RecyclerItemTouchHelperListener1 {
        void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position);
    }
}
