package com.cab.digicafe.Helper;

import android.content.Context;
import android.graphics.Canvas;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.View;

import com.cab.digicafe.Adapter.master.CategoryUnderMasterListAdapter;
import com.cab.digicafe.R;

public class DragTouchTopBottomHelperForCategory extends ItemTouchHelper.SimpleCallback {
    private CategoryUnderMasterListAdapter imgPrevAdapter;
    Context context;

    public DragTouchTopBottomHelperForCategory(CategoryUnderMasterListAdapter imgPrevAdapter, Context context, TouchCall touchCall) {
        super(ItemTouchHelper.DOWN | ItemTouchHelper.UP, 0);
        this.imgPrevAdapter = imgPrevAdapter;
        this.touchCall = touchCall;
        this.context = context;
    }


    TouchCall touchCall;

    public interface TouchCall {
        public void callDrag(boolean isdrag);

        public void onScroll(int toScrollPos);

        public void onSwap(int from, int to);
    }


    int viewHolderPos = 0;
    int targetPos = 0;
    int prevTargetPos = -1;
    RecyclerView.ViewHolder prevTargetVH;


    @Override
    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {


        if (viewHolder.getAdapterPosition() != target.getAdapterPosition()) {
        }

        viewHolderPos = viewHolder.getAdapterPosition();
        targetPos = target.getAdapterPosition();

        if (prevTargetPos == -1) prevTargetPos = targetPos;


       /* if(targetPos==prevTargetPos){
            prevTargetVH = (CategoryUnderMasterListAdapter.MyViewHolder) target;
        }
        else {
            ((CategoryUnderMasterListAdapter.MyViewHolder) prevTargetVH).llDrag.setBackground(null);
            prevTargetVH = (CategoryUnderMasterListAdapter.MyViewHolder) target;
            prevTargetPos = targetPos;


        }
        ((CategoryUnderMasterListAdapter.MyViewHolder) target).llDrag.setBackground(ContextCompat.getDrawable(context, R.drawable.strokes_grn));
*/


        //imgPrevAdapter.swap(viewHolder.getAdapterPosition(), target.getAdapterPosition());
        return false;
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {

        imgPrevAdapter.remove(viewHolder.getAdapterPosition());
    }


    @Override
    public void clearView(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
        // final View foregroundView = ((CategoryUnderMasterListAdapter.MyViewHolder) viewHolder).itemView;
        // ((ImgPrevAdapter.MyViewHolder) viewHolder).tv_stroke.setVisibility(View.GONE);
        // getDefaultUIUtil().clearView(foregroundView);


        touchCall.callDrag(false);

        Log.e("pos viewHolderclr", viewHolder.getAdapterPosition() + "");

        final View foregroundView = ((CategoryUnderMasterListAdapter.MyViewHolder) viewHolder).itemView;
        // ((ImgPrevAdapter.MyViewHolder) viewHolder).tv_stroke.setVisibility(View.GONE);
        ((CategoryUnderMasterListAdapter.MyViewHolder) viewHolder).llDrag.setBackground(null);
        getDefaultUIUtil().clearView(foregroundView);

        //super.clearView(recyclerView, viewHolder);


        touchCall.onSwap(viewHolderPos, targetPos);
        //imgPrevAdapter.swap(viewHolderPos, targetPos);

    }

    @Override
    public void onChildDraw(Canvas c, RecyclerView recyclerView,
                            RecyclerView.ViewHolder viewHolder, float dX, float dY,
                            int actionState, boolean isCurrentlyActive) {

        final View v = ((CategoryUnderMasterListAdapter.MyViewHolder) viewHolder).itemView;

        if (actionState == ItemTouchHelper.ACTION_STATE_DRAG) {
            touchCall.callDrag(true);
            ((CategoryUnderMasterListAdapter.MyViewHolder) viewHolder).llDrag.setBackground(ContextCompat.getDrawable(context, R.drawable.strokes));
        }

        if (!isCurrentlyActive) {
            touchCall.callDrag(false);
            ((CategoryUnderMasterListAdapter.MyViewHolder) viewHolder).llDrag.setBackground(null);
        }


        getDefaultUIUtil().onDraw(c, recyclerView, v, dX, dY, actionState, isCurrentlyActive);


    }


    @Override
    public void onChildDrawOver(Canvas c, RecyclerView recyclerView,
                                RecyclerView.ViewHolder viewHolder, float dX, float dY,
                                int actionState, boolean isCurrentlyActive) {
        final View v = ((CategoryUnderMasterListAdapter.MyViewHolder) viewHolder).itemView;
        if (actionState == ItemTouchHelper.ACTION_STATE_DRAG) {

            Log.e("viewHolderX", dX + "");
            Log.e("viewHolderY", dY + "");


            touchCall.callDrag(true);
            ((CategoryUnderMasterListAdapter.MyViewHolder) viewHolder).llDrag.setBackground(ContextCompat.getDrawable(context, R.drawable.strokes));
        }


        if (!isCurrentlyActive) {
            touchCall.callDrag(false);
            ((CategoryUnderMasterListAdapter.MyViewHolder) viewHolder).llDrag.setBackground(null);
        }


        getDefaultUIUtil().onDrawOver(c, recyclerView, v, dX, dY, actionState, isCurrentlyActive);


    }


}
