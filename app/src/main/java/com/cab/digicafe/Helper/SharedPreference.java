package com.cab.digicafe.Helper;

import android.content.Context;
import android.content.SharedPreferences;

import com.cab.digicafe.Model.Catelog;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by crayond on 09/11/2017.
 */

public class SharedPreference {
    public static final String PREFS_NAME = "PRODUCT_APP";
    public static final String FAVORITES = "Product_Favorite";
    public static final String IS3DAYCOMPLETE = "IS3DAYCOMPLETE";

    public SharedPreference() {
        super();
    }

    // This four methods are used for maintaining favorites.
    public void saveFavorites(Context context, List<Catelog> favorites) {
        SharedPreferences settings;
        SharedPreferences.Editor editor;

        settings = context.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);
        editor = settings.edit();

        Gson gson = new Gson();
        String jsonFavorites = gson.toJson(favorites);

        editor.putString(FAVORITES, jsonFavorites);

        editor.commit();
    }

    public void addFavorite(Context context, Catelog product) {
        List<Catelog> favorites = getFavorites(context);
        if (favorites == null)
            favorites = new ArrayList<Catelog>();
        favorites.add(product);
        saveFavorites(context, favorites);
    }

    public void removeFavorite(Context context, Catelog product) {
        ArrayList<Catelog> favorites = getFavorites(context);
        if (favorites != null) {
            favorites.remove(product);
            saveFavorites(context, favorites);
        }
    }

    public ArrayList<Catelog> getFavorites(Context context) {
        SharedPreferences settings;
        List<Catelog> favorites;

        settings = context.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);

        if (settings.contains(FAVORITES)) {
            String jsonFavorites = settings.getString(FAVORITES, null);
            Gson gson = new Gson();
            Catelog[] favoriteItems = gson.fromJson(jsonFavorites,
                    Catelog[].class);

            favorites = Arrays.asList(favoriteItems);
            favorites = new ArrayList<Catelog>(favorites);
        } else
            return null;

        return (ArrayList<Catelog>) favorites;
    }
}
