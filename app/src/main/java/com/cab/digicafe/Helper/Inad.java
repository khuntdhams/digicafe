package com.cab.digicafe.Helper;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;

import com.cab.digicafe.Dialogbox.AnimDialogue;
import com.cab.digicafe.Dialogbox.CustomDialogClass;
import com.cab.digicafe.Dialogbox.DialogLoading;
import com.cab.digicafe.R;
import com.tapadoo.alerter.Alerter;

import java.net.HttpURLConnection;

import static android.content.Context.CONNECTIVITY_SERVICE;

/**
 * Created by Navneet on 10/7/2017.
 */
public class Inad {


    public static boolean isInternetOn(Context con) {


        // get Connectivity Manager object to check connection
        ConnectivityManager connec = (ConnectivityManager) con.getSystemService(CONNECTIVITY_SERVICE);

        // Check for network connections
        if (connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED) {

            // if connected with internet
            return true;

        } else if (
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.DISCONNECTED ||
                        connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.DISCONNECTED) {
            /*Alerter.create((Activity) con)
                    .setTitle("Internet Connection")
                    .setText("Internet connection is required!")
                    .show();*/

            return false;
        }

        return false;
    }


    public static void alerter(String title, String msg, Activity con) {
        Alerter.create(con)
                .setTitle(title)
                .setText(String.valueOf(Html.fromHtml(msg)))
                .setBackgroundColorRes(R.color.colorPrimary)
                .show();
    }

    public static void alerterInfo(String title, String msg, Activity con) {
        Alerter.create(con)
                .setTitle(title)
                .setText(String.valueOf(Html.fromHtml(msg)))
                .setBackgroundColorRes(R.color.gray_light)
                .show();
    }

    public static void alerterGrnInfo(String title, String msg, Activity con) {
        Alerter.create(con)
                .setTitle(title)
                .setText(String.valueOf(Html.fromHtml(msg)))
                .setBackgroundColorRes(R.color.green)
                .show();
    }

    public static void exitalert(final Activity con) {
        new CustomDialogClass(con, new CustomDialogClass.OnDialogClickListener() {
            @Override
            public void onDialogImageRunClick(int positon) {
                if (positon == 0) {

                } else if (positon == 1) {
                    con.finish();
                    Intent intent = new Intent(Intent.ACTION_MAIN);
                    intent.addCategory(Intent.CATEGORY_HOME);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    con.startActivity(intent);
                }
            }
        }, con.getString(R.string.quit)).show();
    }

    public static void showAnimDialogue(Context context, String title, String msg) {

        AnimDialogue commonConfirmDialogue = new AnimDialogue(context, new AnimDialogue.OnDialogClickListener() {
            @Override
            public void onDialogImageRunClick(int pos, String add) {

            }
        }, title, msg);

        commonConfirmDialogue.show();
    }


    public static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public static void showmsg(View v, String msg, Activity c) {
        //Snackbar.make(v, msg, Snackbar.LENGTH_LONG).setAction("",null).show();

       /* Snackbar snack = Snackbar.make(v, msg, Snackbar.LENGTH_LONG);
        View view = snack.getView();
        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams)view.getLayoutParams();
        params.gravity = Gravity.TOP;
        view.setLayoutParams(params);
        snack.show();*/

        Snackbar snack = Snackbar.make(c.findViewById(android.R.id.content), msg, Snackbar.LENGTH_LONG);
        View view = snack.getView();
        TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
        tv.setTextColor(ContextCompat.getColor(c, R.color.white));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            tv.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        } else {
            tv.setGravity(Gravity.CENTER);
        }
        tv.setText(msg);
        snack.show();


    }


    private static boolean isNetworkAvailable(Context c) {
        return ((ConnectivityManager) c.getSystemService(CONNECTIVITY_SERVICE)).getActiveNetworkInfo() != null;
    }

    static HttpURLConnection urlc = null;

    public static boolean hasActiveInternetConnection(final Context context, Netcallback netcall) {
        netcallback = netcall;

        if (isNetworkAvailable(context)) {
            try {

                netcallback.netcall(true);
                DialogLoading.dialogLoading_Dismiss();
                /*new AsyncTask<Void, Void, Void>() {
                    @Override
                    protected void onPostExecute(Void aVoid) {
                        super.onPostExecute(aVoid);

                        try {
                            DialogLoading.dialogLoading_Dismiss();
                            if (urlc.getResponseCode() == 200) {
                                netcallback.netcall(true);
                            }
                            else {
                                netcallback.netcall(false);
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    protected Void doInBackground(Void... voids) {


                        try {
                            urlc = (HttpURLConnection) new URL("http://www.google.com").openConnection();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        urlc.setRequestProperty("User-Agent", "Test");
                        urlc.setRequestProperty("Connection", "close");
                        urlc.setConnectTimeout(5000);
                        try {
                            urlc.connect();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        catch (Exception e) {
                            e.printStackTrace();
                        }

                        return null;
                    }
                }.execute();*/


            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            netcallback.netcall(false);
            return false;
        }
        return false;

    }


    public class MyCustomAsyncTask extends AsyncTask<Void, Void, Void> {
        private Context context;

        public MyCustomAsyncTask(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            // write show progress Dialog code here
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            // write service code here
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

        }
    }

    public interface Netcallback {
        public void netcall(boolean isnet);
    }


    static Netcallback netcallback;

    public static String getCurrencyDecimal(Double val, Context c) {

        String format = "";
        String decimal_digits = SharedPrefUserDetail.getString(c, SharedPrefUserDetail.decimal_digits, "");
        if (decimal_digits.equals("0")) {
            format = String.format("%.0f", val) + "";
        } else if (decimal_digits.equals("0.0")) {
            format = String.format("%.1f", val) + "";
        } else { // 0.00
            format = String.format("%.2f", val) + "";
        }
        return format;
    }


    public static void callIntent(TextView textView, Context context) {
        String callNo = "tel:" + textView.getText().toString().trim();
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse(callNo));
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            try {
                String[] PERMISSIONS = {Manifest.permission.CALL_PHONE};
                if (!hasPermissions(context, PERMISSIONS)) {
                    ActivityCompat.requestPermissions((Activity) context, PERMISSIONS, 22);
                } else {

                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return;
        }
        context.startActivity(callIntent);
    }

}
