package com.cab.digicafe.Helper;

import java.text.NumberFormat;

public class CurrencyCommaSeperator {


    public static String convertStringToComma(long val) {
        return NumberFormat.getNumberInstance().format(val);

    }

    public static String convertStringToCommaFraction(double val) {
        return NumberFormat.getNumberInstance().format(val);

    }


}
