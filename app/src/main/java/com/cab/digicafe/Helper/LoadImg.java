package com.cab.digicafe.Helper;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v4.widget.CircularProgressDrawable;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.cab.digicafe.R;

public class LoadImg {

    public void loadImg(final Context context, String url, final ImageView imageView) {

        CircularProgressDrawable circularProgressDrawable = new CircularProgressDrawable(context);
        circularProgressDrawable.setStrokeWidth(5f);
        circularProgressDrawable.setCenterRadius(30f);
        circularProgressDrawable.start();
        Drawable drawable = null;

        if (url.isEmpty()) drawable = ContextCompat.getDrawable(context, R.drawable.place);
        else if (url.contains(".jpg") || url.contains(".png")) drawable = circularProgressDrawable;

        Glide.with(context)
                .load(url).asBitmap()
                .placeholder(drawable)
                .error(drawable)
                .centerCrop().diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(new BitmapImageViewTarget(imageView) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                        //circularBitmapDrawable.setCircular(true);
                        circularBitmapDrawable.setCornerRadius(10);
                        imageView.setImageDrawable(circularBitmapDrawable);
                    }
                });
    }

    public void loadFitImg(final Context context, String url, final ImageView imageView) {

        CircularProgressDrawable circularProgressDrawable = new CircularProgressDrawable(context);
        circularProgressDrawable.setStrokeWidth(5f);
        circularProgressDrawable.setCenterRadius(30f);
        circularProgressDrawable.start();
        Drawable drawable = null;

        if (url.isEmpty()) drawable = ContextCompat.getDrawable(context, R.color.colorPrimary);
        else if (url.contains(".jpg") || url.contains(".png")) drawable = circularProgressDrawable;

        Glide.with(context)
                .load(url).asBitmap()
                .placeholder(drawable)
                .error(drawable)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(new BitmapImageViewTarget(imageView) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                        //circularBitmapDrawable.setCircular(true);
                        circularBitmapDrawable.setCornerRadius(10);
                        imageView.setImageDrawable(circularBitmapDrawable);
                    }
                });
    }

    public void loadFitImg(final Context context, final TextView imageView) {

        CircularProgressDrawable circularProgressDrawable = new CircularProgressDrawable(context);
        circularProgressDrawable.setStrokeWidth(5f);
        circularProgressDrawable.setCenterRadius(30f);
        circularProgressDrawable.start();
        imageView.setText("  ");
        imageView.setBackground(circularProgressDrawable);

    }


    public void loadCategoryImg(final Context context, String url, final ImageView imageView) {

        CircularProgressDrawable circularProgressDrawable = new CircularProgressDrawable(context);
        circularProgressDrawable.setStrokeWidth(5f);
        circularProgressDrawable.setCenterRadius(30f);
        circularProgressDrawable.start();
        Drawable drawable = null;


        if (url == null) drawable = ContextCompat.getDrawable(context, R.drawable.place);
        else if (url.isEmpty()) drawable = ContextCompat.getDrawable(context, R.drawable.place);
        else if (url.contains(".jpg") || url.contains(".png")) drawable = circularProgressDrawable;

        Glide.with(context)
                .load(url).asBitmap()
                .placeholder(drawable)
                .error(drawable)
                .fitCenter().diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(new BitmapImageViewTarget(imageView) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                        //circularBitmapDrawable.setCircular(true);
                        circularBitmapDrawable.setCornerRadius(0);
                        imageView.setImageDrawable(circularBitmapDrawable);
                    }
                });
    }
}
