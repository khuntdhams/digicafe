package com.cab.digicafe.Helper;

import android.content.Context;
import android.graphics.Canvas;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.View;

import com.cab.digicafe.Adapter.master.CategoryFlowListAdapter;
import com.cab.digicafe.R;

public class DragTouchTopBottomHelperForAllFlowCategory extends ItemTouchHelper.SimpleCallback {
    private CategoryFlowListAdapter imgPrevAdapter;
    Context context;

    public DragTouchTopBottomHelperForAllFlowCategory(CategoryFlowListAdapter imgPrevAdapter, Context context, TouchCall touchCall) {
        super(ItemTouchHelper.RIGHT | ItemTouchHelper.LEFT, 0);
        this.imgPrevAdapter = imgPrevAdapter;
        this.touchCall = touchCall;
        this.context = context;
    }

    TouchCall touchCall;

    public interface TouchCall {
        public void callDrag(boolean isdrag);
    }


    int viewHolderPos = 0;
    int targetPos = 0;

    @Override
    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
        isDrag = true;
        if (viewHolder.getAdapterPosition() != target.getAdapterPosition()) {
            isDrag = false;
        }
        viewHolderPos = viewHolder.getAdapterPosition();
        targetPos = target.getAdapterPosition();


        Log.e("pos viewHolder", viewHolder.getAdapterPosition() + "");
        Log.e("pos target", target.getAdapterPosition() + "");
        Log.e("pos isDrag", String.valueOf(isDrag));

        //imgPrevAdapter.swap(viewHolder.getAdapterPosition(), target.getAdapterPosition());
        return true;
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {

        imgPrevAdapter.remove(viewHolder.getAdapterPosition());
    }


    boolean isDrag = false;

    @Override
    public void clearView(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
        // final View foregroundView = ((CategoryFlowListAdapter.MyViewHolder) viewHolder).itemView;
        // ((ImgPrevAdapter.MyViewHolder) viewHolder).tv_stroke.setVisibility(View.GONE);
        // getDefaultUIUtil().clearView(foregroundView);

        if (isDrag) {
            isDrag = false;
            imgPrevAdapter.onSwap();
        }
        touchCall.callDrag(false);

        Log.e("pos viewHolderclr", viewHolder.getAdapterPosition() + "");

        final View foregroundView = ((CategoryFlowListAdapter.MyViewHolder) viewHolder).itemView;
        // ((ImgPrevAdapter.MyViewHolder) viewHolder).tv_stroke.setVisibility(View.GONE);
        ((CategoryFlowListAdapter.MyViewHolder) viewHolder).llDrag.setBackground(null);
        getDefaultUIUtil().clearView(foregroundView);

        //super.clearView(recyclerView, viewHolder);

        //imgPrevAdapter.swap(viewHolderPos, targetPos);

    }

    @Override
    public void onChildDraw(Canvas c, RecyclerView recyclerView,
                            RecyclerView.ViewHolder viewHolder, float dX, float dY,
                            int actionState, boolean isCurrentlyActive) {

        final View v = ((CategoryFlowListAdapter.MyViewHolder) viewHolder).itemView;
        if (actionState == ItemTouchHelper.ACTION_STATE_DRAG) {
            touchCall.callDrag(true);
            ((CategoryFlowListAdapter.MyViewHolder) viewHolder).llDrag.setBackground(ContextCompat.getDrawable(context, R.drawable.strokes));
        }

        if (!isCurrentlyActive) {
            touchCall.callDrag(false);
            ((CategoryFlowListAdapter.MyViewHolder) viewHolder).llDrag.setBackground(null);
        }

        getDefaultUIUtil().onDraw(c, recyclerView, v, dX, dY, actionState, isCurrentlyActive);


    }


    @Override
    public void onChildDrawOver(Canvas c, RecyclerView recyclerView,
                                RecyclerView.ViewHolder viewHolder, float dX, float dY,
                                int actionState, boolean isCurrentlyActive) {
        final View v = ((CategoryFlowListAdapter.MyViewHolder) viewHolder).itemView;
        if (actionState == ItemTouchHelper.ACTION_STATE_DRAG) {
            touchCall.callDrag(true);
            ((CategoryFlowListAdapter.MyViewHolder) viewHolder).llDrag.setBackground(ContextCompat.getDrawable(context, R.drawable.strokes));
        }


        if (!isCurrentlyActive) {
            touchCall.callDrag(false);
            ((CategoryFlowListAdapter.MyViewHolder) viewHolder).llDrag.setBackground(null);
        }


        getDefaultUIUtil().onDrawOver(c, recyclerView, v, dX, dY, actionState, isCurrentlyActive);


    }


}
