package com.cab.digicafe.Helper;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;

import com.cab.digicafe.Adapter.MyTaskAdapter;

public class RecyclerItemTouchHelper4 extends ItemTouchHelper.SimpleCallback {
    private RecyclerItemTouchHelperListener1 listener;
    private Paint p = new Paint();

    public RecyclerItemTouchHelper4(int dragDirs, int swipeDirs, RecyclerItemTouchHelperListener1 listener) {
        super(dragDirs, swipeDirs);
        this.listener = listener;
    }

    @Override
    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
        return true;
    }

    @Override
    public void onSelectedChanged(RecyclerView.ViewHolder viewHolder, int actionState) {
        if (viewHolder != null) {
            final View foregroundView = ((MyTaskAdapter.MyViewHolder) viewHolder).viewForeground;

            getDefaultUIUtil().onSelected(foregroundView);
        }
    }

    @Override
    public void onChildDrawOver(Canvas c, RecyclerView recyclerView,
                                RecyclerView.ViewHolder viewHolder, float dX, float dY,
                                int actionState, boolean isCurrentlyActive) {
        final View foregroundView = ((MyTaskAdapter.MyViewHolder) viewHolder).viewForeground;

        if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {

            if (dX > 0) {
                ((MyTaskAdapter.MyViewHolder) viewHolder).view_background_left.setVisibility(View.VISIBLE);
            } else {
                ((MyTaskAdapter.MyViewHolder) viewHolder).view_background_left.setVisibility(View.GONE);
            }


        }


        getDefaultUIUtil().onDrawOver(c, recyclerView, foregroundView, dX, dY, actionState, isCurrentlyActive);


    }

    @Override
    public void clearView(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
        final View foregroundView = ((MyTaskAdapter.MyViewHolder) viewHolder).viewForeground;
        getDefaultUIUtil().clearView(foregroundView);
    }

    @Override
    public void onChildDraw(Canvas c, RecyclerView recyclerView,
                            RecyclerView.ViewHolder viewHolder, float dX, float dY,
                            int actionState, boolean isCurrentlyActive) {

        final View foregroundView = ((MyTaskAdapter.MyViewHolder) viewHolder).viewForeground;

        if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {

            if (dX > 0) {
                ((MyTaskAdapter.MyViewHolder) viewHolder).view_background_left.setVisibility(View.VISIBLE);
            } else {
                ((MyTaskAdapter.MyViewHolder) viewHolder).view_background_left.setVisibility(View.GONE);
            }


        }

        getDefaultUIUtil().onDraw(c, recyclerView, foregroundView, dX, dY, actionState, isCurrentlyActive);


    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
        listener.onSwiped(viewHolder, direction, viewHolder.getAdapterPosition());
    }

    @Override
    public int convertToAbsoluteDirection(int flags, int layoutDirection) {
        return super.convertToAbsoluteDirection(flags, layoutDirection);
    }

    public interface RecyclerItemTouchHelperListener1 {
        void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position);
    }
}
