package com.cab.digicafe.Helper;

import android.graphics.Canvas;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;

import com.cab.digicafe.Adapter.MyRequsetAdapter;

public class Req_Issue_ItemTouchHelper extends ItemTouchHelper.SimpleCallback {
    private RecyclerItemTouchHelperListener listener;

    public Req_Issue_ItemTouchHelper(int dragDirs, int swipeDirs, RecyclerItemTouchHelperListener listener) {
        super(dragDirs, swipeDirs);
        this.listener = listener;
    }

    @Override
    public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
        // Set movement flags based on the layout manager

        MyRequsetAdapter myTaskAdapter = new MyRequsetAdapter();

        String c_tab = myTaskAdapter.tab;


        if (c_tab.equalsIgnoreCase("close_all")) {
            return makeMovementFlags(0, ItemTouchHelper.LEFT);
        } else if (c_tab.equalsIgnoreCase("new_all")) {
            return makeMovementFlags(0, ItemTouchHelper.RIGHT);
        } else {
            return makeMovementFlags(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT);
        }
    }


    @Override
    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
        return true;
    }

    @Override
    public void onSelectedChanged(RecyclerView.ViewHolder viewHolder, int actionState) {
        if (viewHolder != null) {
            final View foregroundView = ((MyRequsetAdapter.MyViewHolder) viewHolder).viewForeground;

            getDefaultUIUtil().onSelected(foregroundView);
        }
    }

    @Override
    public void onChildDrawOver(Canvas c, RecyclerView recyclerView,
                                RecyclerView.ViewHolder viewHolder, float dX, float dY,
                                int actionState, boolean isCurrentlyActive) {
        final View foregroundView = ((MyRequsetAdapter.MyViewHolder) viewHolder).viewForeground;

        if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {

            if (dX > 0) {
                ((MyRequsetAdapter.MyViewHolder) viewHolder).view_background_left.setVisibility(View.VISIBLE);
            } else {
                ((MyRequsetAdapter.MyViewHolder) viewHolder).view_background_left.setVisibility(View.GONE);
            }


        }


        getDefaultUIUtil().onDrawOver(c, recyclerView, foregroundView, dX, dY, actionState, isCurrentlyActive);
    }

    @Override
    public void clearView(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {

        final View foregroundView = ((MyRequsetAdapter.MyViewHolder) viewHolder).viewForeground;
        getDefaultUIUtil().clearView(foregroundView);


        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                // Do something after 5s = 5000ms
                listener.onTouchSwipe(true);

            }
        }, 1000);
    }

    @Override
    public void onChildDraw(Canvas c, RecyclerView recyclerView,
                            RecyclerView.ViewHolder viewHolder, float dX, float dY,
                            int actionState, boolean isCurrentlyActive) {
        listener.onTouchSwipe(false);
        final View foregroundView = ((MyRequsetAdapter.MyViewHolder) viewHolder).viewForeground;

        if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {

            if (dX > 0) {
                ((MyRequsetAdapter.MyViewHolder) viewHolder).view_background_left.setVisibility(View.VISIBLE);
            } else {
                ((MyRequsetAdapter.MyViewHolder) viewHolder).view_background_left.setVisibility(View.GONE);
            }


        }

        getDefaultUIUtil().onDraw(c, recyclerView, foregroundView, dX, dY, actionState, isCurrentlyActive);

    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
        listener.onSwiped(viewHolder, direction, viewHolder.getAdapterPosition());
    }

    @Override
    public int convertToAbsoluteDirection(int flags, int layoutDirection) {
        return super.convertToAbsoluteDirection(flags, layoutDirection);
    }

    public interface RecyclerItemTouchHelperListener {
        void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position);

        void onTouchSwipe(boolean isSwipeEnable);
    }
}
