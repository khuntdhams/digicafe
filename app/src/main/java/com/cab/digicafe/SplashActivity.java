package com.cab.digicafe;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.cab.digicafe.Activities.MyAppBaseActivity;
import com.cab.digicafe.Activities.MySupplierActivity;
import com.cab.digicafe.Activities.SearchActivity;
import com.cab.digicafe.Dialogbox.CustomDialogClass;
import com.cab.digicafe.Helper.Inad;
import com.cab.digicafe.Helper.SessionManager;
import com.cab.digicafe.Helper.SharedPrefUserDetail;
import com.cab.digicafe.Rest.ApiClient;
import com.cab.digicafe.Rest.ApiInterface;
import com.crashlytics.android.Crashlytics;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.PendingDynamicLinkData;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;

import io.fabric.sdk.android.Fabric;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.cab.digicafe.services.RefererDataReciever.REFERRER_DATA;

public class SplashActivity extends MyAppBaseActivity implements ConnectivityReceiver.ConnectivityReceiverListener {
    SessionManager sessionManager;
    FirebaseAnalytics mFirebaseAnalytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        //Apputil.report_bug(SplashActivity.this);

        setContentView(R.layout.activity_splash);
        sessionManager = new SessionManager(this);
        SharedPrefUserDetail.setString(this, SharedPrefUserDetail.isfrommyaggregator, "");

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);


        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("referrer")) {
            String ref = getIntent().getExtras().getString("referrer", "");
            showMsg(ref);
        }

        Intent intent = getIntent();
        Uri data = intent.getData();
        if (data != null) {
            try {
                Log.d("URI", data.toString());
                String ref = data.toString().substring(data.toString().lastIndexOf("/") + 1);
                SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
                sp.edit().putString(REFERRER_DATA, ref).apply();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (TextUtils.isEmpty(sessionManager.getsession())) {
            // sessionManager.setAggregator_ID(getString(R.string.Aggregator));

        } else {
            //  sessionManager.setAggregator_ID(sessionManager.getAggregator_ID());
        }

        sessionManager.setAggregator_ID(getString(R.string.Aggregator));


        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }




      /*  new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                if (!TextUtils.isEmpty(sessionManager.getsession()))
                {
                     // Create an Intent that will start the Menu-Activity.
                    Log.e("test", " trdds"+sessionManager.getsession());

                    Intent mainIntent = new Intent(SplashActivity.this,MainActivity.class);
                    mainIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                    mainIntent.putExtra("session",sessionManager.getsession());

                    startActivity(mainIntent);
                }else
                {
                     // Create an Intent that will start the Menu-Activity.
                    Intent mainIntent = new Intent(SplashActivity.this,LoginActivity.class);
                    startActivity(mainIntent);
                    finish();
                }

            }
        }, 1000);*/

        //throw new RuntimeException("This is a crash");


    }


    public void usersignin() {


        ApiInterface apiService =
                ApiClient.getClient(SplashActivity.this).create(ApiInterface.class);


        Call call;

        call = apiService.getagregatorProfile(sessionManager.getAggregator_ID());

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                int statusCode = response.code();
                //  String cookie = response.;
                //  Log.d("test", cookie+ " ");

                if (response.isSuccessful()) {

                    try {

                        String cookie = response.headers().get("Set-Cookie");
                        String[] cookies = cookie.split(";");
                        Log.d("test", cookies[0] + " ");

                    } catch (Exception e) {

                    }


                    try {
                        String a = new Gson().toJson(response.body());
                        JSONObject Jsonresponse = new JSONObject(a);
                        JSONObject Jsonresponseinfo = Jsonresponse.getJSONObject("response");


                        Log.e("test", " trdds" + Jsonresponse.toString());
                        JSONArray aggregatorobj = Jsonresponseinfo.getJSONArray("data");
                        JSONObject aggregator = aggregatorobj.getJSONObject(0);
                        String aggregatorbridgeid = aggregator.getString("bridge_id");
                        final String isactive = aggregator.getString("business_status");
                        company_name = aggregator.getString("company_name");
                        sessionManager.setKEY_CURRENTCOMPANYNAME(company_name);
                        sessionManager.setAggregatorprofile(aggregatorbridgeid);

                        if (aggregator.has("company_detail_long")) {
                            String servicearea = aggregator.getString("company_detail_long");
                            sessionManager.setservicearea(servicearea);
                        }


                        String priority_val = aggregator.getString("app_android_priority");

                        String versioncode = "";
                        if (aggregator.has("app_android_version_number")) {
                            versioncode = aggregator.getString("app_android_version_number");
                        }

                       /* String update_message = aggregator.getString("app_android_url");

                        int v_code = 0;
                        if (versioncode.equalsIgnoreCase("")) {

                        } else {
                            v_code = Integer.parseInt(versioncode);
                        }*/


                        //compareversioncode(v_code,priority_val,update_message,isactive);

                        FirebaseDynamicLinks.getInstance()
                                .getDynamicLink(getIntent())
                                .addOnSuccessListener(SplashActivity.this, new OnSuccessListener<PendingDynamicLinkData>() {


                                    @Override
                                    public void onSuccess(PendingDynamicLinkData pendingDynamicLinkData) {
                                        // Get deep link from result (may be null if no link is found)
                                        Uri deepLink = null;
                                        if (pendingDynamicLinkData != null) {


                                            try {
                                                deepLink = pendingDynamicLinkData.getLink();
                                                String referrer = deepLink.getQueryParameter("referrer");
                                                //showMsg(referrer);

                                                SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(SplashActivity.this);
                                                sp.edit().putString(REFERRER_DATA, referrer).apply();

                                /*String data = new Gson().toJson(pendingDynamicLinkData, PendingDynamicLinkData.class);
                                Log.e("okhttp", data);*/
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }

                                        navigatehomepage(isactive);
                                        // Handle the deep link. For example, open the linked
                                        // content, or apply promotional credit to the user's
                                        // account.

                                    }
                                })
                                .addOnFailureListener(SplashActivity.this, new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception e) {
                                                navigatehomepage(isactive);
                                            }
                                        }
                                );


                    } catch (Exception e) {
                        navigatehomepage("open");
                        Log.e("test", " trdds" + e.getLocalizedMessage());

                    }
                } else {
                    Log.e("test", " trdds1");

                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Toast.makeText(SplashActivity.this, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        // Toast.makeText(CatelogActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }

                //  List<Movie> movies = response.body().getResults();
                //  recyclerView.setAdapter(new MoviesAdapter(movies, R.layout.list_item_movie, getApplicationContext()));
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // Log error here since request failed
                Log.e("error message", t.toString());
            }
        });
    }


    public void compareversioncode(int currentversion, String priority_txt, String update_message, final String isactive) {

        int versionCode = 1;
        PackageInfo pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            versionCode = pInfo.versionCode;

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        if (versionCode < currentversion) {
            AlertDialog.Builder alert_build = new AlertDialog.Builder(SplashActivity.this);
            alert_build.setTitle(update_message);
            alert_build.setCancelable(false);
            alert_build.setPositiveButton(getResources().getString(R.string.update), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                    try {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                    } catch (android.content.ActivityNotFoundException anfe) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                    }
                }
            });
            if (priority_txt.equalsIgnoreCase("low")) {
                // sessionManager
                alert_build.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        navigatehomepage(isactive);

                    }
                });
            }
            AlertDialog alert_show = alert_build.create();
            alert_show.show();


        } else {
            navigatehomepage(isactive);
        }

    }

    public String currentdate() {
        Date curDate = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
        String DateToStr = format.format(curDate);
        return DateToStr;
    }

    public void printDifference(Date startDate, Date endDate) {
        //milliseconds
        long different = endDate.getTime() - startDate.getTime();

        System.out.println("startDate : " + startDate);
        System.out.println("endDate : " + endDate);
        System.out.println("different : " + different);

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;

        System.out.printf(
                "%d days, %d hours, %d minutes, %d seconds%n",
                elapsedDays, elapsedHours, elapsedMinutes, elapsedSeconds);
    }

    public void navigatehomepage(String isactive) {


        if (isactive.equalsIgnoreCase("open")) {
            if (!TextUtils.isEmpty(sessionManager.getsession())) {

                redirecthome();

            } else {
                // Create an Intent that will start the Menu-Activity.
                Intent mainIntent = new Intent(SplashActivity.this, LoginActivity.class);
                startActivity(mainIntent);
                finish();

            }

        } else {

            if (!TextUtils.isEmpty(sessionManager.getsession())) {

                redirecthome();

            } else {
                // Create an Intent that will start the Menu-Activity.

                Intent mainIntent = new Intent(SplashActivity.this, LoginActivity.class);
                startActivity(mainIntent);
                finish();

            }


        }
    }

    public void redirecthome() {

     /*   final SettingDB db = new SettingDB(SplashActivity.this);
        List<SettingModel> list_setting = db.GetItems();
        SettingModel sm = new SettingModel();*/
        //sm = list_setting.get(0);
        // Create an Intent that will start the Menu-Activity.
        //Log.e("test", " trdds" + sessionManager.getsession());


           /* if(sm.getIsalltask().equals("1"))
            {
                Intent mainIntent = new Intent(SplashActivity.this,AllTaskActivity.class);
                mainIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                mainIntent.putExtra("session",sessionManager.getsession());
                startActivity(mainIntent);
            }
            else if(sm.getIsmytask().equals("1"))
            {
                Intent mainIntent = new Intent(SplashActivity.this,MyTaskActivity.class);
                mainIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                mainIntent.putExtra("session",sessionManager.getsession());
                startActivity(mainIntent);
            }
            else {
                Intent mainIntent = new Intent(SplashActivity.this,MainActivity.class);
                mainIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                mainIntent.putExtra("session",sessionManager.getsession());
                startActivity(mainIntent);
            }*/

        if (sessionManager.getcurrentu_nm().contains("@") && sessionManager.getcurrentu_nm().contains(".br")) {  // E
            Intent mainIntent = new Intent(SplashActivity.this, SearchActivity.class);
            mainIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            mainIntent.putExtra("session", sessionManager.getsession());
            startActivity(mainIntent);
        } else if (sessionManager.getcurrentu_nm().matches("[0-9.]*")) {  // C
            Intent mainIntent = new Intent(SplashActivity.this, MySupplierActivity.class);
            mainIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            mainIntent.putExtra("session", sessionManager.getsession());
            startActivity(mainIntent);
        } else { // Business
            Intent mainIntent = new Intent(SplashActivity.this, SearchActivity.class);
            mainIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            mainIntent.putExtra("session", sessionManager.getsession());
            startActivity(mainIntent);
        }
    }

    String company_name = "";


    @Override
    protected void onPause() {
        super.onPause();
    }


    private void checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();

    }

    @Override
    protected void onResume() {
        super.onResume();


        App.getInstance().setConnectivityListener(this);

        Inad.hasActiveInternetConnection(this, new Inad.Netcallback() {
            @Override
            public void netcall(boolean isnet) {
                if (isnet) {
                    usersignin();
                } else {
                    new CustomDialogClass(SplashActivity.this, new CustomDialogClass.OnDialogClickListener() {
                        @Override
                        public void onDialogImageRunClick(int positon) {
                            if (positon == 0) {

                            } else if (positon == 1) {

                            }
                        }
                    }, "Alert\nInternet connection is required!").show();
                }
            }
        });

    }


    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }
}
