package com.cab.digicafe;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Geocoder;
import android.os.Bundle;
import android.location.Address;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.cab.digicafe.Activities.CatalougeTabActivity;
import com.cab.digicafe.Activities.EditCurrencyForBusinessActivity;
import com.cab.digicafe.Activities.MyAppBaseActivity;
import com.cab.digicafe.Activities.MySupplierActivity;
import com.cab.digicafe.Activities.SearchActivity;
import com.cab.digicafe.Adapter.AddressAdapter;
import com.cab.digicafe.Adapter.PlaceArrayAdapter;
import com.cab.digicafe.Database.SettingDB;
import com.cab.digicafe.Database.SettingModel;
import com.cab.digicafe.Dialogbox.SaveProfile;
import com.cab.digicafe.Helper.SessionManager;
import com.cab.digicafe.Helper.SharedPrefUserDetail;
import com.cab.digicafe.Model.AddAddress;
import com.cab.digicafe.Model.Metal;
import com.cab.digicafe.Model.ShareModel;
import com.cab.digicafe.Model.Userprofile;
import com.cab.digicafe.MyCustomClass.GPSTracker;
import com.cab.digicafe.MyCustomClass.ModelDevice;
import com.cab.digicafe.Rest.ApiClient;
import com.cab.digicafe.Rest.ApiInterface;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileActivity extends MyAppBaseActivity implements
        GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks, TextWatcher,
        AddressAdapter.OnItemClickListener {
    TextInputLayout txt_firstname;
    //TextInputLayout txt_lastname;
    TextInputLayout txt_email;
    TextInputLayout txt_mobile;
    CircleImageView userprofileimage;
    String str_firstname = "";
    String str_lastname = "";
    String str_mobile = "";
    String str_email = "";
    String sessionString;
    Button updatebtn;
    Button btnNext;
    Userprofile userprofileobj;
    SessionManager sessionManager;

    EditText et_fname, et_lname, et_phone, et_email;

    /*TextInputLayout txt_flatno;
    TextInputLayout txt_blockno;*/
    String str_area = "";
    SettingModel sm = new SettingModel();
    boolean isback = true;
    RecyclerView rvAddressList;

    AddressAdapter mAdapter;
    ArrayList<AddAddress> addressList = new ArrayList<>();
    ImageView ivAddAddress;

    JSONArray jArray = new JSONArray();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        findViewById(R.id.rl_pb).setVisibility(View.VISIBLE);

        sessionManager = new SessionManager(this);

        user = sessionManager.getcurrentu_nm();
        final SettingDB db = new SettingDB(this);

        try {
            List<SettingModel> list_setting = db.GetItems();

            sm = list_setting.get(0);
        } catch (Exception e) {
            e.printStackTrace();
        }


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Profile");
        txt_firstname = (TextInputLayout) findViewById(R.id.firstname);
        txt_email = (TextInputLayout) findViewById(R.id.email);
        //txt_lastname = (TextInputLayout)findViewById(R.id.lastname);
        txt_mobile = (TextInputLayout) findViewById(R.id.mobile);
        userprofileimage = (CircleImageView) findViewById(R.id.profilepic);
        et_fname = (EditText) findViewById(R.id.et_fname);
        et_lname = (EditText) findViewById(R.id.et_lname);
        et_phone = (EditText) findViewById(R.id.et_phone);
        et_email = (EditText) findViewById(R.id.et_email);

        rvAddressList = (RecyclerView) findViewById(R.id.rvAddressList);
        ivAddAddress = (ImageView) findViewById(R.id.ivAddAddress);
       /* txt_flatno = (TextInputLayout)findViewById(R.id.flatno);
        txt_blockno = (TextInputLayout)findViewById(R.id.blockno);*/

        updatebtn = (Button) findViewById(R.id.update);
        btnNext = (Button) findViewById(R.id.btnNext);

        Picasso.with(ProfileActivity.this)
                .load(R.mipmap.launcher)
                .placeholder(R.mipmap.launcher)   // optional
                .error(R.mipmap.launcher)      // optional
                .fit()                        // optional
                .into(userprofileimage);


        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                sessionString = null;
            } else {
                sessionString = extras.getString("session");
                isback = extras.getBoolean("isback", true);

                if (isback) {
                    ShareModel.getI().bus_setting_from = "profile";
                    getSupportActionBar().setDisplayHomeAsUpEnabled(true);

                } else {
                    isFromSignUp = true;
                    ShareModel.getI().bus_setting_from = "signup";
                    getSupportActionBar().setDisplayHomeAsUpEnabled(false);

                    if (sessionManager.getIsConsumer()) {

                        ShareModel.getI().isAddShopAuto = "ys";

                    }
                }

            }
        } else {
            sessionString = (String) savedInstanceState.getSerializable("session");
        }

        if (sessionManager.getisBusinesslogic()) {
            //updatebtn.setText("UPDATE AND NEXT");
            btnNext.setVisibility(View.VISIBLE);
        } else {
            btnNext.setVisibility(View.GONE);
            //updatebtn.setText("UPDATE");
        }
        locationinitialized();

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isTxtChanged) {
                    updatebtn.performClick();
                } else {
                    Intent i = new Intent(ProfileActivity.this, EditCurrencyForBusinessActivity.class);
                    i.putExtra("isFromSignUp", isFromSignUp);// All
                    startActivity(i);
                }


            }
        });

        updatebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Log.e("et", et_fname.getText().toString());

                String f_name = et_fname.getText().toString();
                String l_name = et_lname.getText().toString();
                String phone = et_phone.getText().toString();
                String email = et_email.getText().toString();


                str_area = tv_google_loc.getText().toString();

                for (int i = 0; i < addressList.size(); i++) {
                    JSONObject object = new JSONObject();

                    try {
                        object.put("addressType", addressList.get(i).getAddressType());
                        object.put("address", addressList.get(i).getAddress());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    jArray.put(object);
                }

                if (rb_manual.isChecked()) {

                    if (!spin_area.equals("")) {

                        /*str_block = txt_blockno.getEditText().getText().toString().trim();
                        str_flat = txt_flatno.getEditText().getText().toString().trim();


                        if (!str_block.trim().equals("")) {
                            str_block = " , " + str_block;
                        }
                        if (!str_flat.trim().equals("")) {
                            str_flat = " , " + str_flat;
                        }*/

                        str_area = spin_area;
                    }
                } else {

                }

                str_area = tv_google_loc.getText().toString();

                String Expn =
                        "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";


                if (TextUtils.isEmpty(f_name)) {
                    Toast.makeText(ProfileActivity.this, "Name is required!", Toast.LENGTH_LONG).show();
                } else if (TextUtils.isEmpty(phone)) {
                    Toast.makeText(ProfileActivity.this, "Mobile No. is required!", Toast.LENGTH_LONG).show();
                } else if (TextUtils.isEmpty(email)) {
                    Toast.makeText(ProfileActivity.this, "Email field is required!", Toast.LENGTH_LONG).show();
                } else if (email.matches(Expn) == false) {
                    Toast.makeText(ProfileActivity.this, "Email AddAddress Must be Valid required!", Toast.LENGTH_LONG).show();
                } /*else if (str_area.isEmpty()) {
                    Toast.makeText(ProfileActivity.this, "AddAddress field is required!", Toast.LENGTH_LONG).show();
                }*/ else if (phone.length() < 10) {
                    Toast.makeText(ProfileActivity.this, "Mobile number must be valid required!", Toast.LENGTH_LONG).show();
                } else {
                   /* if (industry_id.isEmpty()) {
                        Toast.makeText(ProfileActivity.this, "Select industry !", Toast.LENGTH_LONG).show();
                    } else if (building_id.isEmpty()) {
                        Toast.makeText(ProfileActivity.this, "Select Circle !", Toast.LENGTH_LONG).show();
                    } else {
                        findViewById(R.id.rl_pb).setVisibility(View.VISIBLE);
                        usersignin(f_name, email, phone);
                    }*/

                    findViewById(R.id.rl_pb).setVisibility(View.VISIBLE);
                    usersignin(f_name, email, phone);

                }
            }
        });


        spinner = (Spinner) findViewById(R.id.spinner);
        spinner_building = (Spinner) findViewById(R.id.spinner_building);
        spinner_industry = (Spinner) findViewById(R.id.spinner_industry);

        usersignin(false);

        //updateOnlyFieldJson();

        mAdapter = new AddressAdapter(addressList, this,false);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        rvAddressList.setLayoutManager(mLayoutManager);
        rvAddressList.setAdapter(mAdapter);

        ivAddAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addAddressDialog();
            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void userregistration(String f_nm_, String email_, String phone_) {

        /*String str_block = txt_blockno.getEditText().getText().toString().trim();
        String str_flat = txt_flatno.getEditText().getText().toString().trim();;*/

        String spin_area = str_area;
        //str_area = spin_area +" , "+ str_block +" , "+ str_flat;
        ApiInterface apiService =
                ApiClient.getClient2(ProfileActivity.this).create(ApiInterface.class);


        Map<String, RequestBody> stringRequestBodyHashMap = new HashMap<>();


        RequestBody sectionBody = RequestBody.create(MediaType.parse("multipart/form-data"), "userinfo");
        stringRequestBodyHashMap.put("section", sectionBody);

        RequestBody firstname = RequestBody.create(MediaType.parse("multipart/form-data"), f_nm_);
        stringRequestBodyHashMap.put("firstname", firstname);

        RequestBody location = RequestBody.create(MediaType.parse("multipart/form-data"), jArray.toString());
        stringRequestBodyHashMap.put("location", location);

        RequestBody contact_no = RequestBody.create(MediaType.parse("multipart/form-data"), phone_);
        stringRequestBodyHashMap.put("contact_no", contact_no);

        RequestBody email_id = RequestBody.create(MediaType.parse("multipart/form-data"), email_);
        stringRequestBodyHashMap.put("email_id", email_id);

        /*JSONObject joField = new JSONObject();
        try {
            joField.put("is_g_plus_sign", "yes");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        RequestBody field_json_data = RequestBody.create(MediaType.parse("multipart/form-data"), joField.toString());
        stringRequestBodyHashMap.put("field_json_data", field_json_data);*/


        Log.e("sessionString", sessionString);

        //Call call = apiService.updateuserprofile("userinfo", f_nm_, str_area, phone_, email_, joField.toString());
        Call call = apiService.updateuserprofile(stringRequestBodyHashMap);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                int statusCode = response.code();

                if (response.isSuccessful()) {
                    Log.e("sessionString", response.body().toString());
                    usersignin(true);
                    /*Intent i = new Intent(ProfileActivity.this, LoginActivity.class);
                    i.putExtra("username",str_mobile);
                    startActivity(i);*/
                } else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Log.e("response_message", jObjError.toString());

                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        JSONObject jObjErrordetails = jObjErrorresponse.getJSONObject("errordetail");
                        Toast.makeText(ProfileActivity.this, jObjErrordetails.toString(), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(ProfileActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }

                //  List<Movie> movies = response.body().getResults();
                //  recyclerView.setAdapter(new MoviesAdapter(movies, R.layout.list_item_movie, getApplicationContext()));
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // Log error here since request failed
                // Log.e("error message", t.toString());
            }
        });
    }

    public void getuserprofile(final String sessionvalue) {

        ApiInterface apiService =
                ApiClient.getClient(ProfileActivity.this).create(ApiInterface.class);
        Call call;
        Log.e("sessionString", sessionvalue);
        call = apiService.getProfile(sessionvalue, sessionManager.getAggregatorprofile());

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                int statusCode = response.code();

                if (response.isSuccessful()) {
                    String a = new Gson().toJson(response.body());
                    Log.e("response", a);
                    try {
                        JSONObject jObjRes = new JSONObject(a);
                        JSONObject jObjdata = jObjRes.getJSONObject("response");
                        JSONObject jObjResponse = jObjdata.getJSONObject("data");
                        Log.d("testuservalue", jObjResponse.toString() + " ");
                        userprofileobj = new Gson().fromJson(jObjResponse.toString(), Userprofile.class);

                        if (userprofileobj.getUsername().equalsIgnoreCase("")) {
                            getSupportActionBar().setTitle("Profile");
                        } else {
                            getSupportActionBar().setTitle(userprofileobj.getUsername() + "");
                        }


                        if (userprofileobj.getEmail_id().equalsIgnoreCase("Temp@gmail.com") || userprofileobj.getFirstname().equalsIgnoreCase("Temp")) {
                            txt_firstname.getEditText().setText("");
                        } else {
                            txt_firstname.getEditText().setText(userprofileobj.getFirstname());
                        }

                        txt_mobile.getEditText().setText(userprofileobj.getContact_no());


                        if (userprofileobj.getEmail_id().equalsIgnoreCase("Temp@gmail.com")) {
                            txt_email.getEditText().setText("");
                        } else {
                            txt_email.getEditText().setText(userprofileobj.getEmail_id());
                        }

                        String locatio_area = userprofileobj.getLocation();
                        Log.d("Location", locatio_area + "");

                        //AddAddress addAddress = new Gson().fromJson(locatio_area, AddAddress.class);

                        if (locatio_area != null && !locatio_area.isEmpty()) {
                            tv_google_loc.setText(locatio_area);
                            try {
                                JSONArray array = new JSONArray(locatio_area);
                                String type, address;
                                for (int i = 0; i < array.length(); i++) {
                                    JSONObject obj = array.getJSONObject(i);
                                    type = obj.getString("addressType");
                                    address = obj.getString("address");
                                    addressList.add(new AddAddress(type, address));
                                }
                            } catch (Exception e) {
                                isTxtChanged = true;
                                getcurrentlocationinfo();
                                e.printStackTrace();
                            }

                        } else {
                            locatio_area = "";
                        }

                        if (locatio_area.isEmpty()) {
                            getcurrentlocationinfo();
                        }

                        et_fname.addTextChangedListener(ProfileActivity.this);
                        et_email.addTextChangedListener(ProfileActivity.this);
                        tv_google_loc.addTextChangedListener(ProfileActivity.this);
                        et_phone.addTextChangedListener(ProfileActivity.this);

                        isTxtChanged = false;
                        if (userprofileobj.getEmail_id().equalsIgnoreCase("Temp@gmail.com")) {
                            isTxtChanged = true;
                        }

                        /*String locatio_area = userprofileobj.getLocation();

                        boolean ismanual = false;

                        if (locatio_area != null) {
                            String[] array = locatio_area.split(",");


                            if (array.length > 0) {
                                if (planets == null) planets = new ArrayList();

                                for (int i = 0; i < planets.size(); i++) {
                                    if (array[0].trim().equalsIgnoreCase(planets.get(i).toString())) {
                                        ismanual = true;
                                        spinner.setSelection(i);

                                        if (array.length > 1) {
                                            txt_blockno.getEditText().setText(array[1]);
                                        }
                                        if (array.length > 2) {
                                            txt_flatno.getEditText().setText(array[2]);
                                        }
                                        break;
                                    }
                                }
                            }
                        } else {
                            locatio_area = "";
                        }
                        if (ismanual) {
                            rb_manual.setChecked(true);
                        } else if (sm != null && sm.getIsgoogleplace().equals("1")) {
                            rb_current.setChecked(true);
                        } else {
                            rb_manual.setChecked(true);
                        }
                        tv_google_loc.setText(locatio_area);

                        if (!ismanual && locatio_area.isEmpty()) {
                            getcurrentlocationinfo();
                        }*/


                       /* txt_blockno.getEditText().setText(array[1]);
                        txt_flatno.getEditText().setText(array[2]);*/

                        //txt_blockno.getEditText().setText(userprofileobj.getEmail_id());
                        if (!TextUtils.isEmpty(userprofileobj.getProfileimage())) {
                           /* Picasso.with(ProfileActivity.this)
                                    .load(R.mipmap.logo)
                                    .placeholder(R.mipmap.logo)   // optional
                                    .error(R.mipmap.logo)      // optional
                                    .fit()                        // optional
                                    .into(userprofileimage);*/
                        }
                    } catch (JSONException e) {

                    }

                } else {
                    Log.e("test", " trdds1");

                    try {
                       /* JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Toast.makeText(MainActivity.this, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();*/
                    } catch (Exception e) {
                        // Toast.makeText(LoginActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }

                //  List<Movie> movies = response.body().getResults();
                //  recyclerView.setAdapter(new MoviesAdapter(movies, R.layout.list_item_movie, getApplicationContext()));
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // Log error here since request failed

            }
        });
    }

    public void usersignin(final String f_nm_, final String email_, final String phone_) {

        ApiInterface apiService =
                ApiClient.getClient2(ProfileActivity.this).create(ApiInterface.class);
        String u_name = sessionManager.getUserWithAggregator();
        String pwd = sessionManager.getPasswd();


        Call call = apiService.getUserLogin(u_name, pwd);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                int statusCode = response.code();

                if (response.isSuccessful()) {

                    try {
                        String cookie = response.headers().get("Set-Cookie");
                        String[] cookies = cookie.split(";");
                        sessionManager.create_Usersession(cookies[0]);
                    } catch (Exception e) {

                    }

                    userregistration(f_nm_, email_, phone_);

                } else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Toast.makeText(ProfileActivity.this, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(ProfileActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }

            }

            @Override
            public void onFailure(Call call, Throwable t) {

            }
        });
    }

    public void usersignin(final boolean isupdate) {


        ApiInterface apiService =
                ApiClient.getClient(ProfileActivity.this).create(ApiInterface.class);


        Call call;

        call = apiService.getagregatorProfile(sessionManager.getAggregator_ID());

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                int statusCode = response.code();
                //  String cookie = response.;
                //  Log.d("test", cookie+ " ");
                findViewById(R.id.rl_pb).setVisibility(View.GONE);

                if (response.isSuccessful()) {
                    try {
                        String a = new Gson().toJson(response.body());
                        JSONObject Jsonresponse = new JSONObject(a);
                        JSONObject Jsonresponseinfo = Jsonresponse.getJSONObject("response");


                        Log.e("test", " trdds" + Jsonresponse.toString());
                        JSONArray aggregatorobj = Jsonresponseinfo.getJSONArray("data");
                        JSONObject aggregator = aggregatorobj.getJSONObject(0);
                        String aggregatorbridgeid = aggregator.getString("bridge_id");
                        String isactive = aggregator.getString("business_status");
                        sessionManager.setAggregatorprofile(aggregatorbridgeid);
                        Log.e("aggregatorbridgeid", aggregatorbridgeid);

                        if (aggregator.has("company_detail_long")) {
                            String servicearea = aggregator.getString("company_detail_long");
                            sessionManager.setservicearea(servicearea);
                        }

                        if (isupdate) {


                            //setuserbuildid_and_indid(true);

                            if (sessionManager.getisBusinesslogic()) { //

                                //redirecttohome();

                                if (isTxtChanged) {
                                    isTxtChanged = false;
                                    btnNext.performClick();
                                } else {
                                    new SaveProfile(ProfileActivity.this, new SaveProfile.OnDialogClickListener() {
                                        @Override
                                        public void onDialogImageRunClick(int positon, String add) {

                                            if (positon == 0) {

                                            } else if (positon == 1) {
                                                redirecttohome();
                                                //finish();
                                            }
                                        }
                                    }, "Profile Updated Successfully !").show();
                                }


                            } else {
                                new SaveProfile(ProfileActivity.this, new SaveProfile.OnDialogClickListener() {
                                    @Override
                                    public void onDialogImageRunClick(int positon, String add) {

                                        if (positon == 0) {

                                        } else if (positon == 1) {

                                            redirecttohome();
                                            //finish();
                                        }
                                    }
                                }, "Profile Updated Successfully !").show();
                            }


                            // Toast.makeText(ProfileActivity.this, "Profile Updated Successfully!", Toast.LENGTH_LONG).show();
                        } else {
                            GetAddress();
                        /*    planets = new ArrayList();

                            planets.add("Select Industry");
                            String serviceareas = sessionManager.getservicearea();
                            String[] planetslist = serviceareas.split("\\|");
                            Log.e("arraylength", "" + sessionManager.getservicearea());
                            List<String> wordList = Arrays.asList(planetslist);
                            for (String e : wordList) {
                                planets.add(e.trim());
                            }
                            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(ProfileActivity.this, R.layout.spinner_item, planets);

                            // ArrayAdapter adapter = ArrayAdapter.createFromResource(this, R.array.planets, R.layout.spinner_item);
                            dataAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
                            spinner.setAdapter(dataAdapter);

                            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                                    // your code here
                                    if (position > 0) {
                                        spin_area = planets.get(position).toString();

                                    } else

                                    {
                                        spin_area = "";

                                    }
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parentView) {
                                    // your code here
                                }

                            });
*/
                        }

                    } catch (Exception e) {


                    }
                } else {


                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Toast.makeText(ProfileActivity.this, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        // Toast.makeText(CatelogActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }

                //  List<Movie> movies = response.body().getResults();
                //  recyclerView.setAdapter(new MoviesAdapter(movies, R.layout.list_item_movie, getApplicationContext()));
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                findViewById(R.id.rl_pb).setVisibility(View.GONE);
            }
        });
    }

    public void locationinitialized() {
        findViewById(R.id.iv_clear).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv_google_loc.setText("");
            }
        });

        rb_manual = (RadioButton) findViewById(R.id.rb_manual);
        rb_current = (RadioButton) findViewById(R.id.rb_current);
        rg_loc = (RadioGroup) findViewById(R.id.rg_loc);
        ll_google = (LinearLayout) findViewById(R.id.ll_google);
        ll_manual_loc = (LinearLayout) findViewById(R.id.ll_manual_loc);
        txt_flatno = (TextInputLayout) findViewById(R.id.flatno);
        txt_blockno = (TextInputLayout) findViewById(R.id.blockno);


        tv_google_loc = (AutoCompleteTextView) findViewById(R.id.tv_google_loc);
        tv_google_loc.setThreshold(1);
        tv_google_loc.setOnItemClickListener(mAutocompleteClickListener);


        tv_google_loc.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count > 0) {
                    findViewById(R.id.iv_clear).setVisibility(View.VISIBLE);
                } else {
                    findViewById(R.id.iv_clear).setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Places.GEO_DATA_API)
                .enableAutoManage(this, GOOGLE_API_CLIENT_ID, this)
                .addConnectionCallbacks(this)
                .build();


        mPlaceArrayAdapter = new PlaceArrayAdapter(this, android.R.layout.simple_list_item_1,
                BOUNDS_MOUNTAIN_VIEW, null);
        tv_google_loc.setAdapter(mPlaceArrayAdapter);


        rg_loc.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                str_area = "";
                switch (checkedId) {
                    case R.id.rb_manual:
                        ll_google.setVisibility(View.GONE);
                        ll_manual_loc.setVisibility(View.VISIBLE);
                        break;
                    case R.id.rb_current:
                        ll_manual_loc.setVisibility(View.GONE);
                        ll_google.setVisibility(View.VISIBLE);
                        break;
                }

            }
        });

        try {
            if (sm != null && sm.getIsgoogleplace().equals("1")) {

            } else {
                //rb_manual.setChecked(true);
                //rb_current.setVisibility(View.GONE);
            }
        } catch (Exception e) {

        }


    }

    AutoCompleteTextView tv_google_loc;
    private GoogleApiClient mGoogleApiClient;
    private PlaceArrayAdapter mPlaceArrayAdapter;
    private static final LatLngBounds BOUNDS_MOUNTAIN_VIEW = new LatLngBounds(
            new LatLng(37.398160, -122.180831), new LatLng(37.430610, -121.972090));
    private static final int GOOGLE_API_CLIENT_ID = 0;

    RadioButton rb_manual, rb_current;
    LinearLayout ll_google, ll_manual_loc;
    RadioGroup rg_loc;
    String spin_area = "";
    String str_block = "";
    String str_flat = "";
    TextInputLayout txt_flatno;
    TextInputLayout txt_blockno;
    Spinner spinner, spinner_building, spinner_industry;
    ArrayList planets;
    ArrayList<industrySuppliermodel> al_industry, al_building;

    @Override
    public void onConnected(Bundle bundle) {
        mPlaceArrayAdapter.setGoogleApiClient(mGoogleApiClient);


    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {


        Toast.makeText(this,
                "Google Places API connection failed with error code:" +
                        connectionResult.getErrorCode(),
                Toast.LENGTH_LONG).show();
    }

    @Override
    public void onConnectionSuspended(int i) {
        mPlaceArrayAdapter.setGoogleApiClient(null);
    }

    private AdapterView.OnItemClickListener mAutocompleteClickListener
            = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            final PlaceArrayAdapter.PlaceAutocomplete item = mPlaceArrayAdapter.getItem(position);
            final String placeId = String.valueOf(item.placeId);
            //Log.i(LOG_TAG, "Selected: " + item.description);
            PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                    .getPlaceById(mGoogleApiClient, placeId);
            placeResult.setResultCallback(mUpdatePlaceDetailsCallback);
            //Log.i(LOG_TAG, "Fetching details for ID: " + item.placeId);
        }
    };


    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {

                return;
            }
            // Selecting the first object buffer.
            final Place place = places.get(0);
            CharSequence attributions = places.getAttributions();


            //tv_google_loc.setText(Html.fromHtml(place.getAddress() + ""));

        }
    };

    @Override
    public void onPause() {
        super.onPause();
        mGoogleApiClient.stopAutoManage(this);
        mGoogleApiClient.disconnect();
    }

    private static final int REQUEST_LOCATION = 2;
    private static String[] PERMISSIONS_LOCATION = {
            android.Manifest.permission.ACCESS_COARSE_LOCATION,
            android.Manifest.permission.ACCESS_FINE_LOCATION
    };

    public void verifyLocationPermissions(Activity activity) {
        // Check if we have read or write permission
        int coarsePermission = ActivityCompat.checkSelfPermission(activity, android.Manifest.permission.ACCESS_COARSE_LOCATION);
        int finePermission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION);

        if (coarsePermission != PackageManager.PERMISSION_GRANTED || finePermission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_LOCATION,
                    REQUEST_LOCATION
            );
        }
        {
            getuserprofile(sessionString);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        verifyLocationPermissions(this);
        isTxtChanged = false;
    }

    public void getcurrentlocationinfo() {

        int coarsePermission = ActivityCompat.checkSelfPermission(ProfileActivity.this, android.Manifest.permission.ACCESS_COARSE_LOCATION);
        int finePermission = ActivityCompat.checkSelfPermission(ProfileActivity.this, Manifest.permission.ACCESS_FINE_LOCATION);

        if (coarsePermission != PackageManager.PERMISSION_GRANTED || finePermission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user

        }

        GPSTracker mGPS = new GPSTracker(this);


        if (mGPS.canGetLocation()) {
            mGPS.getLocation();

            Geocoder geocoder;
            List<Address> addresses = null;
            geocoder = new Geocoder(this, Locale.getDefault());

            try {
                addresses = geocoder.getFromLocation(mGPS.getLatitude(), mGPS.getLongitude(), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (addresses != null) {
                if (addresses.size() > 0) {
                    String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                    tv_google_loc.setText(address);
                    addressList.add(new AddAddress("Current Location", address));
                }

            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_LOCATION: {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    getuserprofile(sessionString);

                } else {
                    Toast.makeText(ProfileActivity.this, "Permission denied to get location!", Toast.LENGTH_SHORT).show();
                }
                return;
            }

        }
    }


    public void GetAddress() {


        ApiInterface apiService =
                ApiClient.getClient(this).create(ApiInterface.class);


        Call call;

        call = apiService.getaddress(sessionManager.getsession(), "");

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                int statusCode = response.code();
                //  String cookie = response.;
                //  Log.d("test", cookie+ " ");

                if (response.isSuccessful()) {


                    try {
                        String a = new Gson().toJson(response.body());
                        JSONObject Jsonresponse = new JSONObject(a);
                        JSONObject jo_response = Jsonresponse.getJSONObject("response");

                        String data = jo_response.getString("data");
                        sessionManager.setIndustrySupplier(data);

                        JSONObject jo_data = new JSONObject(data);
                        JSONArray ja_buildingArr = jo_data.getJSONArray("buildingArr");

                        al_building = new ArrayList();
                        al_building.add(new industrySuppliermodel("Select Circle", ""));
                        for (int i = 0; i < ja_buildingArr.length(); i++) {
                            JSONObject jo_res = ja_buildingArr.getJSONObject(i);
                            String building_name = jo_res.getString("building_name");
                            String building_id = jo_res.getString("building_id");
                            al_building.add(new industrySuppliermodel(building_name, building_id));
                        }

                        al_industry = new ArrayList();
                        al_industry.add(new industrySuppliermodel("Select Industry", ""));
                        JSONArray industryArr = jo_data.getJSONArray("industryArr");
                        for (int i = 0; i < industryArr.length(); i++) {
                            JSONObject jo_res = industryArr.getJSONObject(i);
                            String industry_name = jo_res.getString("industry_name");
                            industry_id = jo_res.getString("industry_id");
                            al_industry.add(new industrySuppliermodel(industry_name, industry_id));
                            break;
                        }

                        //setbuild();
                        //setindu();
                        //setuserbuildid_and_indid(false);

                    } catch (Exception e) {
                        Log.e("test", " trdds" + e.getLocalizedMessage());

                    }
                } else {

                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                        Toast.makeText(ProfileActivity.this, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        // Toast.makeText(CatelogActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }

                //  List<Movie> movies = response.body().getResults();
                //  recyclerView.setAdapter(new MoviesAdapter(movies, R.layout.list_item_movie, getApplicationContext()));
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // Log error here since request failed
                Log.e("error message", t.toString());
            }
        });
    }

    private class industrySuppliermodel {
        private String name;
        private String id;

        public industrySuppliermodel() {
        }

        public industrySuppliermodel(String name, String id) {
            this.name = name;
            this.id = id;
        }


        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        /**
         * Pay attention here, you have to override the toString method as the
         * ArrayAdapter will reads the toString of the given object for the name
         *
         * @return contact_name
         */
        @Override
        public String toString() {
            return name;
        }
    }

    ArrayAdapter<industrySuppliermodel> build_dataAdapter;

    public void setbuild() {
        build_dataAdapter = new ArrayAdapter<industrySuppliermodel>(ProfileActivity.this, R.layout.spinner_item, al_building);

        // ArrayAdapter adapter = ArrayAdapter.createFromResource(this, R.array.planets, R.layout.spinner_item);
        build_dataAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        spinner_building.setAdapter(build_dataAdapter);

        spinner_building.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                // your code here
                if (position > 0) {
                    spin_area = al_building.get(position).getName();
                    building_id = al_building.get(position).getId();

                } else {
                    // spin_area = "";
                    building_id = "";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });


    }

    ArrayAdapter<industrySuppliermodel> ind_dataAdapter;

    public void setindu() {
        ind_dataAdapter = new ArrayAdapter<industrySuppliermodel>(ProfileActivity.this, R.layout.spinner_item, al_industry);

        // ArrayAdapter adapter = ArrayAdapter.createFromResource(this, R.array.planets, R.layout.spinner_item);
        ind_dataAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        spinner_industry.setAdapter(ind_dataAdapter);

        spinner_industry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                // your code here
                if (position > 0) {
                    spin_area = al_industry.get(position).getName();
                    industry_id = al_industry.get(position).getId();

                } else {
                    // spin_area = "";
                    industry_id = "";

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });


    }

    String industry_id = "", building_id = "";


    @Override
    public void onBackPressed() {
        if (isback) {
            super.onBackPressed();
        } else {

        }


    }

    ArrayList<ModelDevice> al_user = new ArrayList<>();
    boolean isuseravailable = false;
    int userpos = 0;
    String user = "";

    public void setuserbuildid_and_indid(boolean isset) {

        user = sessionManager.getcurrentu_nm();
        final Gson gson = new Gson();

        String json = SharedPrefUserDetail.getString(this, SharedPrefUserDetail.loginuser, "");


        Type type = new TypeToken<ArrayList<ModelDevice>>() {
        }.getType();

        al_user = gson.fromJson(json, type);

        if (al_user == null) {
            al_user = new ArrayList<>();
        }

        if (isset)  // fst time
        {
            isuseravailable = false;
            for (int i = 0; i < al_user.size(); i++) {
                String alluser = al_user.get(i).getUsernm();
                if (user.equals(alluser)) {
                    isuseravailable = true;
                    userpos = i;
                    al_user.get(i).setBuild_id(building_id);
                    al_user.get(i).setInd_id(industry_id);
                    break;
                }
            }
            String str_array = gson.toJson(al_user);
            SharedPrefUserDetail.setString(this, SharedPrefUserDetail.loginuser, str_array);
            sessionManager.setTLoginuser(str_array);

            sessionManager.setIndustry_id(industry_id);
            sessionManager.setBuilding_id(building_id);


        } else {  // isget  // next time
            isuseravailable = false;
            for (int i = 0; i < al_user.size(); i++) {
                String alluser = al_user.get(i).getUsernm();
                if (user.equals(alluser)) {
                    isuseravailable = true;
                    userpos = i;

                    break;
                }
            }
            building_id = al_user.get(userpos).getBuild_id();
            //industry_id = al_user.get(userpos).getInd_id();

            boolean isind = false;
            for (int i = 0; i < al_industry.size(); i++) {
                String t_id = al_industry.get(i).getId();
                if (t_id.trim().equals(industry_id.trim())) {
                    isind = true;
                    spinner_industry.setSelection(i);
                    break;
                }
            }

            if (!isind && al_industry.size() > 0) spinner_industry.setSelection(1);
            spinner_industry.setEnabled(false);

            for (int i = 0; i < al_building.size(); i++) {
                String t_id = al_building.get(i).getId();
                if (t_id.trim().equals(building_id.trim())) {
                    spinner_building.setSelection(i);
                    break;
                }
            }
        }
     /*   sessionManager.setBuilding_id(building_id);
        sessionManager.setIndustry_id(industry_id);*/
    }

    public void redirecttohome() {

        if (user.contains("@") && user.contains(".br")) // Employee
        {
            sessionManager.setisBusinesslogic(false);
            Intent i = new Intent(ProfileActivity.this, SearchActivity.class);  // All
            i.putExtra("session", sessionManager.getsession());
            i.putExtra("isfromsignup", false);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);

        } else if (user.matches("[0-9.]*")) {  // consumer
            sessionManager.setisBusinesslogic(false);
        /*    Intent i = new Intent(ProfileActivity.this, MySupplierActivity.class);
            i.putExtra("session", sessionManager.getsession());
            i.putExtra("isfromsignup", false);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);*/
            if (isFromSignUp) {
                Intent i = new Intent(ProfileActivity.this, MySupplierActivity.class);
                i.putExtra("session", sessionManager.getsession());
                i.putExtra("isfromsignup", false);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
            } else {
                finish();
            }

        } else  // Business
        {
            /*Intent i = new Intent(ProfileActivity.this, SearchActivity.class);  // All
            i.putExtra("session", sessionManager.getsession());
            i.putExtra("isfromsignup", false);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            sessionManager.setisBusinesslogic(true);
            startActivity(i);*/

            Intent i = new Intent(ProfileActivity.this, CatalougeTabActivity.class);  // All
            startActivity(i);

        }

    }

    boolean isFromSignUp = false;


    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {

        isTxtChanged = true;
    }

    boolean isTxtChanged = false;


    public void addAddressDialog() {
        android.support.v7.app.AlertDialog.Builder builder =
                new android.support.v7.app.AlertDialog.Builder(this);
        View dialogView = LayoutInflater.from(this)
                .inflate(R.layout.address_dialog, null);
        builder.setView(dialogView);
        final EditText et_address_type = dialogView.findViewById(R.id.et_address_type);

        final AutoCompleteTextView tv_google_loc = dialogView.findViewById(R.id.tv_google_loc);
        final ImageView iv_clear = dialogView.findViewById(R.id.iv_clear);
        TextView tvCancel = dialogView.findViewById(R.id.tvCancel);
        TextView tvSave = dialogView.findViewById(R.id.tvSave);

        final android.support.v7.app.AlertDialog alertDialog = builder.create();

        tv_google_loc.setThreshold(1);
        tv_google_loc.setOnItemClickListener(mAutocompleteClickListener);


        tv_google_loc.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count > 0) {
                    iv_clear.setVisibility(View.VISIBLE);
                } else {
                    iv_clear.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        tv_google_loc.setAdapter(mPlaceArrayAdapter);

        iv_clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv_google_loc.setText("");
            }
        });

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                alertDialog.cancel();
            }
        });
        tvSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isTxtChanged = true;
                alertDialog.cancel();
                addressList.add(new AddAddress(et_address_type.getText().toString(), tv_google_loc.getText().toString()));
                mAdapter.notifyDataSetChanged();
            }
        });

        alertDialog.show();
    }

    @Override
    public void onItemClick(final int pos) {
        android.support.v7.app.AlertDialog.Builder builder =
                new android.support.v7.app.AlertDialog.Builder(this);
        View dialogView = LayoutInflater.from(this)
                .inflate(R.layout.address_dialog, null);
        builder.setView(dialogView);
        final EditText et_address_type = dialogView.findViewById(R.id.et_address_type);

        final AutoCompleteTextView tv_google_loc = dialogView.findViewById(R.id.tv_google_loc);
        final ImageView iv_clear = dialogView.findViewById(R.id.iv_clear);
        TextView tvCancel = dialogView.findViewById(R.id.tvCancel);
        TextView tvSave = dialogView.findViewById(R.id.tvSave);

        final android.support.v7.app.AlertDialog alertDialog = builder.create();

        tv_google_loc.setThreshold(1);
        tv_google_loc.setOnItemClickListener(mAutocompleteClickListener);

        et_address_type.setText(addressList.get(pos).getAddressType() + "");
        tv_google_loc.setText(addressList.get(pos).getAddress() + "");

        iv_clear.setVisibility(View.VISIBLE);
        tv_google_loc.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count > 0) {
                    iv_clear.setVisibility(View.VISIBLE);
                } else {
                    iv_clear.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        tv_google_loc.setAdapter(mPlaceArrayAdapter);

        iv_clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv_google_loc.setText("");
            }
        });

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                alertDialog.cancel();
                addressList.get(pos).setAddressType(addressList.get(pos).getAddressType());
                addressList.get(pos).setAddress(addressList.get(pos).getAddress());
                mAdapter.notifyDataSetChanged();
            }
        });
        tvSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.cancel();
                addressList.get(pos).setAddressType(et_address_type.getText().toString());
                addressList.get(pos).setAddress(tv_google_loc.getText().toString());
                mAdapter.notifyDataSetChanged();
                isTxtChanged = true;
            }
        });

        alertDialog.show();
    }
}