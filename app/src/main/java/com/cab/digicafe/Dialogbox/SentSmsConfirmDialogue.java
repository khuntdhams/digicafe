package com.cab.digicafe.Dialogbox;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cab.digicafe.MyCustomClass.PlayAnim;
import com.cab.digicafe.R;

import butterknife.BindView;
import butterknife.ButterKnife;


public class SentSmsConfirmDialogue extends AlertDialog implements
        View.OnClickListener {

    private final OnDialogClickListener listener;
    public Activity c;
    public Dialog d;


    @BindView(R.id.llAnimAddcat)
    LinearLayout llAnimAddcat;

    @BindView(R.id.tvAdd)
    TextView tvAdd;

    @BindView(R.id.tvCancel)
    TextView tvCancel;

    @BindView(R.id.tvCnfmTitle)
    TextView tvCnfmTitle;

    @BindView(R.id.rlHideCallReq)
    RelativeLayout rlHideCallReq;


    public SentSmsConfirmDialogue(Activity a, OnDialogClickListener listener) {
        super(a);
        // TODO Auto-generated constructor stub
        this.c = a;
        this.listener = listener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_sent_sms_cnfm_dialogue);
        ButterKnife.bind(this);

        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        getWindow().setAttributes(lp);

        tvAdd.setOnClickListener(this);
        tvCancel.setOnClickListener(this);
        rlHideCallReq.setOnClickListener(this);
        setCancelable(false);

        setOnKeyListener(new OnKeyListener() {

            @Override
            public boolean onKey(DialogInterface arg0, int keyCode,
                                 KeyEvent event) {
                // TODO Auto-generated method stub
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    dismissDialogue(0);
                }
                return true;
            }
        });


        rlHideCallReq.setOnClickListener(this);
        playAnim.slideUpRl(llAnimAddcat);
        tvCnfmTitle.setText("SMS");


    }


    public void dismissDialogue(int call) {
        playAnim.slideDownRl(llAnimAddcat);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                listener.onDialogImageRunClick(0, "");
                dismiss();

            }
        }, 700);
    }

    PlayAnim playAnim = new PlayAnim();

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvCancel:

                dismissDialogue(0);

                break;

            case R.id.rlHideCallReq:
                dismissDialogue(0);


                break;

            case R.id.tvAdd:


                break;
            default:
                break;
        }
    }

    public interface OnDialogClickListener {
        void onDialogImageRunClick(int pos, String add);
    }


}
