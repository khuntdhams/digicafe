package com.cab.digicafe.Dialogbox;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatRadioButton;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cab.digicafe.Fragments.CatalougeContent;
import com.cab.digicafe.R;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;


public class CatelogDetailDialogTab extends AlertDialog implements
        View.OnClickListener {
    private final OnDialogClickListener listener;
    public Activity c;
    public Dialog d;
    public TextView tv_msg;
    ImageView iv_close;
    CatalougeContent msg;
    AppCompatRadioButton rb_noblur, rb_innerblur, rb_normal, rb_outer, rb_solid;

    @BindView(R.id.tv_name)
    TextView tv_name;

    @BindView(R.id.tv_field)
    TextView tv_field;

    @BindView(R.id.ll_field)
    LinearLayout ll_field;

    @BindView(R.id.tv_tax)
    TextView tv_tax;

    @BindView(R.id.ll_tax)
    LinearLayout ll_tax;

    @BindView(R.id.tv_additional)
    TextView tv_additional;

    @BindView(R.id.ll_additional)
    LinearLayout ll_additional;

    /*  new CustomDialogClass(MainActivity.this, new CustomDialogClass.OnDialogClickListener() {
                    @Override
                    public void onDialogImageRunClick() {
                        Log.e("Ok","Ok");


                    }
                }).show(); */

    public CatelogDetailDialogTab(Activity a, OnDialogClickListener listener, CatalougeContent msg) {
        super(a);
        // TODO Auto-generated constructor stub
        this.c = a;
        this.listener = listener;
        this.msg = msg;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_catelog_detail);
        tv_msg = (TextView) findViewById(R.id.tv_msg);
        iv_close = (ImageView) findViewById(R.id.iv_close);

        ButterKnife.bind(this);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

      /*  WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(getWindow().getAttributes());
        int dialogWidth = lp.width;
        int dialogHeight = lp.height;

        if(dialogHeight > 200) {
            getWindow().setLayout(dialogWidth,300);
        }*/


        boolean istax = false, isadditional = false, isfield = false;


        try {
            JSONObject jo_add = new JSONObject(msg.getAdditionalJsonData());
            JSONObject jo_tax = new JSONObject(msg.getTaxJsonData());
            JSONObject jo_field = new JSONObject(msg.getFieldJsonData());


            if (msg.getAdditionalJsonData() != null && jo_add.length() > 0) {
                isadditional = true;
            }

            if (msg.getTaxJsonData() != null && jo_tax.length() > 0) {
                istax = true;
            }

            if (msg.getFieldJsonData() != null && jo_field.length() > 0) {
                isfield = true;
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            String TAX = "", Summary = "", CGST = "", IGST = "", SGST = "", Class = "", Grade = "", Origin = "", Size = "", Displaya = "";
            String tax = "", field = "", additional = "";
            if (msg.getAdditionalJsonData() != null && !msg.getAdditionalJsonData().equals("[]")) {
                JSONObject jo_summary = new JSONObject(msg.getAdditionalJsonData());


                if (jo_summary.has("TAX")) {
                    TAX = "Tax : " + jo_summary.getString("TAX") + "\n";
                }
                if (jo_summary.has("Summary")) {
                    Summary = "Summary :" + jo_summary.getString("Summary") + "\n";

                }
                if (jo_summary.has("Displaya")) {

                    Displaya = "Displaya : " + jo_summary.getString("Displaya") + " ";


                }

                additional = msg.getAdditionalJsonData();

                additional = additional.replaceAll("Summary", "<font color='#a4c639'><b>" + "Summary " + "</b></font>");
                //additional = additional.replaceAll("Summa","<font color='#a4c639'><b>" + "Summa " + "</b></font>");
                additional = additional.replaceAll("Displaya", "<br><br><font color='#a4c639'><b>" + "Displaya " + "</b></font>");

            }

            if (msg.getTaxJsonData() != null && !msg.getTaxJsonData().equals("[]")) {
                JSONObject jo_tax = new JSONObject(msg.getTaxJsonData());

                if (jo_tax.has("CGST")) {

                    CGST = "CGST : " + jo_tax.getString("CGST") + "";
                }
                if (jo_tax.has("SGST")) {

                    SGST = "SGST : " + jo_tax.getString("SGST") + " ; ";
                }

                if (jo_tax.has("IGST")) {

                    IGST = "IGST : " + jo_tax.getString("IGST") + " ; ";
                }

                tax = msg.getTaxJsonData();

            }
            //String tax_json_data = TAX + Summary;

            if (msg.getFieldJsonData() != null && !msg.getFieldJsonData().equals("[]")) {
                JSONObject jo__field = new JSONObject(msg.getFieldJsonData());

                if (jo__field.has("Class")) {

                    Class = "Class : " + jo__field.getString("Class") + " ; ";
                }
                if (jo__field.has("Grade")) {

                    Grade = "Grade : " + jo__field.getString("Grade") + " ; ";
                }
                if (jo__field.has("Origin")) {

                    Origin = "Origin : " + jo__field.getString("Origin") + "";
                }

                if (jo__field.has("Size")) {

                    Size = "Size : " + jo__field.getString("Size") + " ; ";
                }

                field = msg.getFieldJsonData();
            }

            String ms = "Name : " + msg.getName() + "\n" + TAX + Summary + SGST + CGST + Class + Grade + Origin;

            //tv_msg.setText(ms);


            tv_name.setSelected(true);
            tv_name.setText(msg.getName());


            //tax = SGST +IGST +CGST;
            tax = tax.replaceAll("[{}]", "");
            tax = tax.replaceAll("\"", "");
            tv_tax.setText(tax);

            //field = Size+Class+Grade+Origin;
            field = field.replaceAll("[{}]", "");
            field = field.replaceAll("\"", "");
            tv_field.setText(field);

            //additional = Displaya;
            additional = additional.replaceAll("[{}]", "");
            additional = additional.replaceAll("\"", "");
            tv_additional.setText(Html.fromHtml(additional));

            if (isadditional) {
                ll_additional.setVisibility(View.VISIBLE);
            } else {
                ll_additional.setVisibility(View.GONE);
            }

            if (isfield) {
                ll_field.setVisibility(View.VISIBLE);
            } else {
                ll_field.setVisibility(View.GONE);
            }


            if (istax) {
                ll_tax.setVisibility(View.VISIBLE);
            } else {
                ll_tax.setVisibility(View.GONE);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }


        iv_close.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_close:
                listener.onDialogImageRunClick(0);
                break;
            default:
                break;
        }
        dismiss();
    }

    public interface OnDialogClickListener {
        void onDialogImageRunClick(int pos);
    }

}
