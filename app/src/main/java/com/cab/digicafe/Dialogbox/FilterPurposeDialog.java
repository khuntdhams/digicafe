package com.cab.digicafe.Dialogbox;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatRadioButton;
import android.view.View;
import android.view.WindowManager;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.cab.digicafe.R;


public class FilterPurposeDialog extends AlertDialog implements
        View.OnClickListener {
    private final OnDialogClickListener listener;
    public Activity c;
    public Dialog d;
    public TextView txt_cancel, txt_ok;

    AppCompatRadioButton rb_all, rb_invoice, rb_request, rb_issue;
    RadioGroup rg_stock;
    boolean isall = false;

    /*  new CustomDialogClass(MainActivity.this, new CustomDialogClass.OnDialogClickListener() {
                    @Override
                    public void onDialogImageRunClick() {
                        Log.e("Ok","Ok");


                    }
                }).show(); */

    public FilterPurposeDialog(Activity a, OnDialogClickListener listener, String add, boolean isall) {
        super(a);
        // TODO Auto-generated constructor stub
        this.c = a;
        this.listener = listener;
        this.tag = add;
        this.isall = isall; // my supplier - false

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.filter_purposedialog);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        rb_all = (AppCompatRadioButton) findViewById(R.id.rb_all);
        rb_invoice = (AppCompatRadioButton) findViewById(R.id.rb_invoice);
        rb_request = (AppCompatRadioButton) findViewById(R.id.rb_request);
        rb_issue = (AppCompatRadioButton) findViewById(R.id.rb_issue);
        rg_stock = (RadioGroup) findViewById(R.id.rg_stock);
        txt_ok = (TextView) findViewById(R.id.txt_ok);
        txt_cancel = (TextView) findViewById(R.id.txt_cancel);

        setCancelable(false);


        if (isall) {
            rb_all.setVisibility(View.VISIBLE);
        } else {
            rb_all.setVisibility(View.GONE);
            rb_issue.setText("Service");
        }


        txt_cancel.setOnClickListener(this);
        txt_ok.setOnClickListener(this);


        switch (tag) {
            case "":
                rb_all.setChecked(true);
                break;
            case "invoice":
                rb_invoice.setChecked(true);
                break;
            case "request":
                rb_request.setChecked(true);
                break;
            case "service":
                rb_issue.setChecked(true);
                break;
            default:
                tag = "";
                rb_all.setChecked(true);
                break;
        }

        rg_stock.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                switch (checkedId) {
                    case R.id.rb_all:
                        tag = "";
                        break;
                    case R.id.rb_invoice:
                        tag = "invoice";
                        break;
                    case R.id.rb_request:
                        tag = "request";
                        break;
                    case R.id.rb_issue:
                        tag = "service";
                        break;
                    default:
                        break;
                }
            }
        });


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_cancel:
                listener.onDialogImageRunClick(0, tag);
                dismiss();
                break;
            case R.id.txt_ok:


                listener.onDialogImageRunClick(1, tag);
                dismiss();

                break;
            default:
                break;
        }

    }

    public interface OnDialogClickListener {
        void onDialogImageRunClick(int pos, String add);
    }

    String tag = "";
}
