package com.cab.digicafe.Dialogbox;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.WindowManager;
import android.widget.RadioButton;
import android.widget.TextView;

import com.cab.digicafe.R;


public class DeleteDialogClass extends AlertDialog implements View.OnClickListener {
    private final OnDialogClickListener listener;
    public Activity c;
    public Dialog d;
    public TextView txt_cancel, txt_ok;
    String msg = "";
    TextView tv_cancel, tv_done;
    RadioButton rb_delete, rb_avail;

    /*  new CustomDialogClass(MainActivity.this, new CustomDialogClass.OnDialogClickListener() {
                    @Override
                    public void onDialogImageRunClick() {
                        Log.e("Ok","Ok");


                    }
                }).show(); */

    public DeleteDialogClass(Activity a, OnDialogClickListener listener, String msg) {
        super(a);
        // TODO Auto-generated constructor stub
        this.c = a;
        this.listener = listener;
        this.msg = msg;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_delete_catalogue);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        txt_cancel = (TextView) findViewById(R.id.txt_cancel);
        txt_ok = (TextView) findViewById(R.id.txt_ok);
        rb_delete = (RadioButton) findViewById(R.id.rb_delete);
        rb_avail = (RadioButton) findViewById(R.id.rb_avail);


        if (msg.equalsIgnoreCase("yes")) {
            rb_avail.setText("AVAILABLE ?");
        } else {
            rb_avail.setText("UN AVAILABLE ?");
        }


        txt_cancel.setOnClickListener(this);
        txt_ok.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_cancel:
                listener.onDialogImageRunClick(0, false);
                dismiss();
                break;
            case R.id.txt_ok:

                boolean isdel = false;
                if (rb_delete.isChecked()) {
                    isdel = true;
                } else {
                    isdel = false;
                }
                listener.onDialogImageRunClick(1, isdel);
                dismiss();
                break;
            default:
                break;
        }

    }

    public interface OnDialogClickListener {
        void onDialogImageRunClick(int pos, boolean isdel);
    }

}
