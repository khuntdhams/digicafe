package com.cab.digicafe.Dialogbox;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;

import com.cab.digicafe.R;

import static android.content.Context.CONNECTIVITY_SERVICE;


public class DialogLoading {

    public static AlertDialog alertDialog, FieldDialog;
    private static DialogLoading mInstance;
    //public static Typeface typeface_Regular, typeface_Bold;

    public static DialogLoading getInstance() {
        if (mInstance == null) {
            mInstance = new DialogLoading();
        }
        return mInstance;
    }

    public static void dialogLoading(Context context) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dialogView = inflater.inflate(R.layout.dialog_progress, null);
        dialogBuilder.setView(dialogView);
        alertDialog = dialogBuilder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        if (!((Activity) context).isFinishing()) {
            try {
                alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                alertDialog.show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /*  public static void dialogFieldRequired(Context context, String s1, String s2, String s3) {
          //typeface_Regular = Typeface.createFromAsset(context.getAssets(), "fonts/OPENSANS-REGULAR_2.TTF");
          //typeface_Bold = Typeface.createFromAsset(context.getAssets(), "fonts/Quicksand-Bold.ttf");

          AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
          LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
          View dialogView = inflater.inflate(R.layout.dialog_error, null);
          dialogBuilder.setView(dialogView);
          TextView tvOA = (TextView) dialogView.findViewById(R.id.tvOA);
          TextView tvField = (TextView) dialogView.findViewById(R.id.tvField);
          TextView tvClose = (TextView) dialogView.findViewById(R.id.tvClose);

         *//* tvOA.setTypeface(typeface_Bold);
        tvField.setTypeface(typeface_Regular);
        tvClose.setTypeface(typeface_Bold);*//*

        tvOA.setText(s1);
        tvField.setText(s2);
        tvClose.setText(s3);

        tvClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (FieldDialog != null) {
                    if (FieldDialog.isShowing()) {
                        FieldDialog.dismiss();
                    }
                }
            }
        });

        FieldDialog = dialogBuilder.create();
        FieldDialog.setCanceledOnTouchOutside(true);
        if (!((Activity) context).isFinishing()) {
            try {
                FieldDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                FieldDialog.show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
*/
    public static void dialogLoading_Dismiss() {
        if (alertDialog != null) {
            if (alertDialog.isShowing()) {
                alertDialog.dismiss();
            }
        }
    }

    private static boolean isNetworkAvailable(Context c) {
        return ((ConnectivityManager) c.getSystemService(CONNECTIVITY_SERVICE)).getActiveNetworkInfo() != null;
    }

    public static boolean hasActiveInternetConnection(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static void alert(final Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Internet Alert");
        builder.setIcon(R.mipmap.launcher);
        builder.setMessage("You need to internet connection");
        builder.setPositiveButton("Open Setting", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                Intent intent = new Intent(android.provider.Settings.ACTION_SETTINGS);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                context.startActivity(intent);

            }
        });

        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }


}
