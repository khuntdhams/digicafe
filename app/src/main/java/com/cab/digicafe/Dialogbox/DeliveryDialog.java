package com.cab.digicafe.Dialogbox;

import android.app.Dialog;
import android.app.DialogFragment;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cab.digicafe.Adapter.AddressAdapter;
import com.cab.digicafe.Adapter.PlaceArrayAdapter;
import com.cab.digicafe.Database.SettingDB;
import com.cab.digicafe.Database.SettingModel;
import com.cab.digicafe.Helper.SessionManager;
import com.cab.digicafe.Helper.SharedPrefUserDetail;
import com.cab.digicafe.Model.AddAddress;
import com.cab.digicafe.Model.Userprofile;
import com.cab.digicafe.MyCustomClass.DatePicker;
import com.cab.digicafe.MyCustomClass.JsonObjParse;
import com.cab.digicafe.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DeliveryDialog extends AlertDialog implements
        View.OnClickListener, AddressAdapter.OnItemClickListener {
    private OnDialogClickListener listener;
    public AppCompatActivity c;
    public Dialog d;
    public TextView tv_date, tv_time, tv_done, txtx_add, tvPickUpOnly, tv_msg, txt_remark, txtCustNm;
    String add, date, time, remark, phone;
    EditText et_address, et_remark, et_phone, etCustNm, tv_dob;
    LinearLayout ll_google;
    RelativeLayout rl_address, rl_dob;
    ArrayList<AddAddress> addressList = new ArrayList<>();
    RecyclerView rvAddressList;
    AddressAdapter mAdapter;
    ImageView ivHideList, ivLocList;
    LinearLayout llAddressList;

    /*  new CustomDialogClass(MainActivity.this, new CustomDialogClass.OnDialogClickListener() {
                    @Override
                    public void onDialogImageRunClick() {
                        Log.e("Ok","Ok");


                    }
                }).show(); */

    @Override
    public void dismiss() {
        super.dismiss();

        mGoogleApiClient.stopAutoManage(c);
        mGoogleApiClient.disconnect();

    }

    public DeliveryDialog(AppCompatActivity a, OnDialogClickListener listener, String add, String time, String date, String remark, String phone) {
        super(a);
        // TODO Auto-generated constructor stub
        this.c = a;
        this.listener = listener;
        this.add = add;
        this.date = date;
        this.time = time;
        this.remark = remark;
        this.phone = phone;
        set_db = new SettingDB(a);
        List<SettingModel> list_setting = set_db.GetItems();
        sm = list_setting.get(0);
        field_json_data = SharedPrefUserDetail.getString(a, SharedPrefUserDetail.field_json_data, "");

    }

    String field_json_data = ""; // field_json_data - act
    SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_deliverydetail);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
        params.gravity = Gravity.CENTER;
        getWindow().setAttributes(params);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        tv_date = (TextView) findViewById(R.id.tv_date);
        txtCustNm = (TextView) findViewById(R.id.txtCustNm);
        tv_dob = (EditText) findViewById(R.id.tv_dob);
        txtx_add = (TextView) findViewById(R.id.txtx_add);
        tv_time = (TextView) findViewById(R.id.tv_time);
        et_address = (EditText) findViewById(R.id.et_address);
        et_remark = (EditText) findViewById(R.id.et_remark);
        et_phone = (EditText) findViewById(R.id.et_phone);
        etCustNm = (EditText) findViewById(R.id.etCustNm);
        tv_done = (TextView) findViewById(R.id.tv_done);
        tvPickUpOnly = (TextView) findViewById(R.id.tvPickUpOnly);
        tv_msg = (TextView) findViewById(R.id.tv_msg);
        txt_remark = (TextView) findViewById(R.id.txt_remark);
        ll_google = (LinearLayout) findViewById(R.id.ll_google);
        rl_address = (RelativeLayout) findViewById(R.id.rl_address);
        rl_dob = (RelativeLayout) findViewById(R.id.rl_dob);

        rvAddressList = (RecyclerView) findViewById(R.id.rvAddressList);
        ivHideList = (ImageView) findViewById(R.id.ivHideList);
        llAddressList = (LinearLayout) findViewById(R.id.llAddressList);
        ivLocList = (ImageView) findViewById(R.id.ivLocList);

        rl_dob.setVisibility(View.GONE);

        findViewById(R.id.iv_clear).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv_google_loc.setText("");
            }
        });

        tv_google_loc = (AutoCompleteTextView) findViewById(R.id.tv_google_loc);
        tv_google_loc.setThreshold(1);
        tv_google_loc.setOnItemClickListener(mAutocompleteClickListener);
        locationinitialized();
        //String expected_delivery_time =  DateTimeUtils.parseDateTime(time, "dd MMM yyyy hh:mm aaa", "yyyy/MM/dd kk:mm");

        SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy hh:mm aaa");
        try {
            if (!time.equals("")) {
                Date date_ = format.parse(time);


                SimpleDateFormat timeformat = new SimpleDateFormat("hh:mm aaa");
                String str_time = timeformat.format(date_);

                SimpleDateFormat dtformat = new SimpleDateFormat("dd MMM yyyy");

                String str_date = dtformat.format(date_);

                tv_time.setText(str_time);
                tv_date.setText(str_date);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!add.equals("")) {


        }
        et_address.setText(add);
        tv_google_loc.setText(add);
        et_remark.setText(remark);
        et_phone.setText(phone);


        tv_done.setOnClickListener(this);
        findViewById(R.id.iv_date).setOnClickListener(this);
        findViewById(R.id.iv_time).setOnClickListener(this);
        //findViewById(R.id.iv_address).setOnClickListener(this);
        findViewById(R.id.iv_close).setOnClickListener(this);


        if (sm.getIsdate().equals("1")) {
            findViewById(R.id.rl_date).setVisibility(View.VISIBLE);
            findViewById(R.id.rl_time).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.rl_date).setVisibility(View.GONE);
            findViewById(R.id.rl_time).setVisibility(View.GONE);
        }

        if (sm.getIsaddress().equals("1")) {
            findViewById(R.id.rl_address).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.rl_address).setVisibility(View.GONE);
        }

        if (sm.getIsremark().equals("1")) {
            findViewById(R.id.rl_remark).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.rl_remark).setVisibility(View.GONE);
        }

        rl_address.setVisibility(View.VISIBLE);
        tvPickUpOnly.setVisibility(View.GONE);

        String pick_up_only = JsonObjParse.getValueEmpty(field_json_data, "pick_up_only");
        if (pick_up_only.equalsIgnoreCase("Yes")) { // from business profile

            rl_address.setVisibility(View.GONE);
            tvPickUpOnly.setVisibility(View.VISIBLE);

            Animation animation = new AlphaAnimation(1, 0.3f);
            animation.setDuration(700);
            animation.setInterpolator(new LinearInterpolator());
            animation.setRepeatCount(Animation.INFINITE);
            animation.setRepeatMode(Animation.REVERSE);
            tvPickUpOnly.startAnimation(animation);
        }

        String service_type_of = JsonObjParse.getValueEmpty(field_json_data, "service_type_of");

        findViewById(R.id.rlCustNm).setVisibility(View.GONE);


        if (service_type_of.equalsIgnoreCase(c.getString(R.string.property))) {

            tv_msg.setText("Customer Details");
            txtx_add.setText("Customer AddAddress : ");
            findViewById(R.id.rl_date).setVisibility(View.GONE);
            findViewById(R.id.rl_time).setVisibility(View.GONE);

            findViewById(R.id.rlCustNm).setVisibility(View.VISIBLE);
        } else if (service_type_of.equalsIgnoreCase(c.getString(R.string.survey))) {
            findViewById(R.id.rl_address).setVisibility(View.GONE);
            rl_dob.setVisibility(View.VISIBLE);
            findViewById(R.id.rl_date).setVisibility(View.GONE);
            findViewById(R.id.rl_time).setVisibility(View.GONE);
            txt_remark.setText("Comment : ");
            et_remark.setHint("Enter comment");

            tv_msg.setText("Customer Details");
            txtx_add.setText("Customer AddAddress : ");
            findViewById(R.id.rl_remark).setVisibility(View.GONE);
            txtCustNm.setText("Name: ");
        }


        findViewById(R.id.rlCustNm).setVisibility(View.VISIBLE);
        sessionManager = new SessionManager(c);

        String userprofile = sessionManager.getUserprofile();
        try {
            Userprofile userProfileData = new Gson().fromJson(userprofile, Userprofile.class);

            if (sessionManager.getisBusinesslogic()) {
                etCustNm.setText(userProfileData.getFirstname());
            }
            if (sessionManager.getIsConsumer()) {
                etCustNm.setText(userProfileData.getFirstname());

            }

            if (sessionManager.getIsEmployess()) {
                etCustNm.setText("");
            }


            String locatio_area = userProfileData.getLocation();
            Log.d("Location", locatio_area + "");

            //AddAddress addAddress = new Gson().fromJson(locatio_area, AddAddress.class);

            if (locatio_area != null && !locatio_area.isEmpty()) {
                tv_google_loc.setText(locatio_area);
                try {
                    JSONArray array = new JSONArray(locatio_area);
                    String type, address;
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject obj = array.getJSONObject(i);
                        type = obj.getString("addressType");
                        address = obj.getString("address");
                        addressList.add(new AddAddress(type, address));
                    }
                    tv_google_loc.setText(addressList.get(0).getAddress());
                    mAdapter = new AddressAdapter(addressList, this, true);
                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(c);
                    rvAddressList.setLayoutManager(mLayoutManager);
                    rvAddressList.setAdapter(mAdapter);
                } catch (Exception e) {
                    e.printStackTrace();
                }


            } else {
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

        tv_dob.setText(sessionManager.getDobForSurvey() + "");

       /* rl_dob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDatePickerDialog();
            }
        });*/

        ivHideList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llAddressList.setVisibility(View.GONE);
            }
        });

        ivLocList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llAddressList.setVisibility(View.VISIBLE);
            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.iv_close:
                dismiss();
                break;
            case R.id.tv_done:
                String service_type_of = JsonObjParse.getValueEmpty(field_json_data, "service_type_of");
                sessionManager = new SessionManager(c);
                // if(service_type_of.equalsIgnoreCase(c.getString(R.string.property))&&sessionManager.getIsEmployess()&&etCustNm.getText().toString().trim().isEmpty())
                if (sessionManager.getIsEmployess() && etCustNm.getText().toString().trim().isEmpty()) {
                    etCustNm.setError("Enter customer name");
                    Toast.makeText(c, "Enter customer name", Toast.LENGTH_LONG).show();
                    return;
                }
                sessionManager.setCustNmForProperty(etCustNm.getText().toString().trim());

                if (service_type_of.equalsIgnoreCase(c.getString(R.string.survey)) && tv_dob.getText().toString().trim().isEmpty()) {
                    Toast.makeText(c, "Enter date of birth", Toast.LENGTH_LONG).show();
                    return;
                } else {
                    sessionManager.setDobForSurvey(tv_dob.getText().toString().trim());
                    listener.onDialogImageRunClick(0, tv_google_loc.getText().toString(), et_remark.getText().toString(), et_phone.getText().toString());
                }
                dismiss();
                break;
            case R.id.iv_date:
                listener.onDialogImageRunClick(1, "", "", "");
                break;
            case R.id.iv_time:
                listener.onDialogImageRunClick(2, "", "", "");
                break;
            case R.id.iv_address:
                listener.onDialogImageRunClick(3, "", "", "");
                break;
            default:
                break;
        }

        dismiss();

    }


    public interface OnDialogClickListener {
        void onDialogImageRunClick(int pos, String add, String remark, String phone);
    }


    public void locationinitialized() {

        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(c)
                    .addApi(Places.GEO_DATA_API)
                    .enableAutoManage(c, GOOGLE_API_CLIENT_ID, new GoogleApiClient.OnConnectionFailedListener() {
                        @Override
                        public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

                        }
                    })
                    .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                        @Override
                        public void onConnected(@Nullable Bundle bundle) {
                            mPlaceArrayAdapter.setGoogleApiClient(mGoogleApiClient);
                        }

                        @Override
                        public void onConnectionSuspended(int i) {
                            mPlaceArrayAdapter.setGoogleApiClient(null);
                        }
                    })
                    .build();
        }

        tv_google_loc.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count > 0) {
                    findViewById(R.id.iv_clear).setVisibility(View.VISIBLE);
                } else {
                    findViewById(R.id.iv_clear).setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        mPlaceArrayAdapter = new PlaceArrayAdapter(c, android.R.layout.simple_list_item_1,
                BOUNDS_MOUNTAIN_VIEW, null);

        tv_google_loc.setAdapter(mPlaceArrayAdapter);


    }


    private GoogleApiClient mGoogleApiClient;
    AutoCompleteTextView tv_google_loc;
    private PlaceArrayAdapter mPlaceArrayAdapter;
    private static final LatLngBounds BOUNDS_MOUNTAIN_VIEW = new LatLngBounds(
            new LatLng(37.398160, -122.180831), new LatLng(37.430610, -121.972090));
    private static final int GOOGLE_API_CLIENT_ID = 0;


    private AdapterView.OnItemClickListener mAutocompleteClickListener
            = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            final PlaceArrayAdapter.PlaceAutocomplete item = mPlaceArrayAdapter.getItem(position);
            final String placeId = String.valueOf(item.placeId);
            //Log.i(LOG_TAG, "Selected: " + item.description);
            PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                    .getPlaceById(mGoogleApiClient, placeId);
            placeResult.setResultCallback(mUpdatePlaceDetailsCallback);
            //Log.i(LOG_TAG, "Fetching details for ID: " + item.placeId);
        }
    };


    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback
            = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {

                return;
            }
            // Selecting the first object buffer.
            final Place place = places.get(0);
            CharSequence attributions = places.getAttributions();


            //tv_google_loc.setText(Html.fromHtml(place.getAddress() + ""));

        }
    };

    SettingDB set_db;
    SettingModel sm = new SettingModel();

    public void openDatePickerDialog() {

        dismiss();

        DatePicker mDatePicker = new DatePicker(c, new DatePicker.PickDob() {
            @Override
            public void onSelect(Date date) {
                show();
                tv_dob.setText(sessionManager.getDobForSurvey());
            }
        }, sessionManager.getDobForSurvey());
        mDatePicker.setStyle(DialogFragment.STYLE_NORMAL, R.style.CustomDialog);
        mDatePicker.show(c.getFragmentManager(), "Select date of birth");

    }

    @Override
    public void onItemClick(int pos) {
        llAddressList.setVisibility(View.GONE);
        tv_google_loc.setText(addressList.get(pos).getAddress());
    }

    @Override
    public void onBackPressed() {
        if (llAddressList.getVisibility() == View.VISIBLE) {
            llAddressList.setVisibility(View.GONE);
        }else{
            super.onBackPressed();
        }

    }
}
