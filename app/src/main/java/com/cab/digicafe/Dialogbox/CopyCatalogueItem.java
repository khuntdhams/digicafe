package com.cab.digicafe.Dialogbox;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.cab.digicafe.Activities.EditCatlogueCategoryActivity;
import com.cab.digicafe.Adapter.SelectFileAdapter;
import com.cab.digicafe.Database.SettingDB;
import com.cab.digicafe.Database.SettingModel;
import com.cab.digicafe.Fragments.CatalougeContent;
import com.cab.digicafe.Helper.Inad;
import com.cab.digicafe.Helper.SessionManager;
import com.cab.digicafe.Model.ModelFile;
import com.cab.digicafe.Model.ShareModel;
import com.cab.digicafe.Model.Userprofile;
import com.cab.digicafe.MyCustomClass.ApplicationUtil;
import com.cab.digicafe.MyCustomClass.Compressor;
import com.cab.digicafe.MyCustomClass.JsonObjParse;
import com.cab.digicafe.R;
import com.cab.digicafe.Rest.ApiClient;
import com.cab.digicafe.Rest.ApiInterface;
import com.cab.digicafe.Rest.Request.CatRequest;
import com.google.api.client.extensions.android.json.AndroidJsonFactory;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.services.vision.v1.Vision;
import com.google.api.services.vision.v1.VisionRequestInitializer;
import com.google.api.services.vision.v1.model.AnnotateImageRequest;
import com.google.api.services.vision.v1.model.BatchAnnotateImagesRequest;
import com.google.api.services.vision.v1.model.BatchAnnotateImagesResponse;
import com.google.api.services.vision.v1.model.EntityAnnotation;
import com.google.api.services.vision.v1.model.Feature;
import com.google.api.services.vision.v1.model.WebDetection;
import com.google.api.services.vision.v1.model.WebEntity;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CopyCatalogueItem extends AlertDialog implements
        View.OnClickListener, TextWatcher {
    public OnDialogClickListener listener;

   /* @Override
    public void onAddedImg(ArrayList<ModelFile> al_data) {
        ArrayList<ModelFile> al_temp_selet = new ArrayList<>();
        al_temp_selet.addAll(al_selet);
        selectFileAdapter.setItem(al_temp_selet);
        al_selet = new ArrayList<>();
        al_selet.addAll(al_temp_selet);

    }*/

    public interface OnDialogClickListener {
        void onDialogImageRunClick(int pos, String add);

        void onAddImg();

        void onSave();

        void onImgClick(List<ModelFile> data, int pos);
    }

    public Activity c;

    @BindView(R.id.tv_title)
    EditText tv_title;

    @BindView(R.id.et_mrp)
    EditText et_mrp;

    @BindView(R.id.et_dis_per)
    EditText et_dis_per;

    @BindView(R.id.et_dis_val)
    EditText et_dis_val;

    @BindView(R.id.et_cgst)  // Tax Field 1
            EditText et_cgst;

    @BindView(R.id.et_sgst)  // Tax Field 2
            EditText et_sgst;

    @BindView(R.id.tvTaxField1)
    TextView tvTaxField1;

    @BindView(R.id.tvTaxField2)
    TextView tvTaxField2;

    @BindView(R.id.llCgst)
    LinearLayout llCgst;

    @BindView(R.id.llSgst)
    LinearLayout llSgst;

    @BindView(R.id.et_offer)
    EditText et_offer;

    @BindView(R.id.et_xref)
    EditText et_xref;

    @BindView(R.id.tv_total)
    TextView tv_total;

    @BindView(R.id.iv_add)
    ImageView iv_add;

    @BindView(R.id.rl_upload)
    RelativeLayout rl_upload;

    @BindView(R.id.tv_progress)
    TextView tv_progress;

    @BindView(R.id.tv_calc_dis)
    TextView tv_calc_dis;

    @BindView(R.id.tv_calc_ctax)
    TextView tv_calc_ctax;

    @BindView(R.id.tv_calc_stax)
    TextView tv_calc_stax;

    @BindView(R.id.iv_saveas)
    ImageView iv_saveas;

    @BindView(R.id.iv_cancel)
    ImageView iv_cancel;


    @BindView(R.id.rv_selectfile)
    RecyclerView rv_selectfile;
    SelectFileAdapter selectFileAdapter;

    CatalougeContent obj;
    String str_model;
    SettingModel sm;
    String currency = "";
    @BindView(R.id.tv_p_cur)
    TextView tv_p_cur;

    @BindView(R.id.tv_dp_cur)
    TextView tv_dp_cur;

    @BindView(R.id.tv_cur1)
    TextView tv_cur1;

    @BindView(R.id.tv_cur2)
    TextView tv_cur2;


    public CopyCatalogueItem(Activity a, OnDialogClickListener listener, CatalougeContent item) {
        super(a);
        // TODO Auto-generated constructor stub
        this.c = a;
        this.listener = listener;
        this.obj = item;
        inits3();
        initvision();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_copycatalogue);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));


        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
        params.gravity = Gravity.CENTER;
        getWindow().setAttributes(params);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);


        ButterKnife.bind(this);

        if (ShareModel.getI().alCategory != null && !ShareModel.getI().alCategory.equalsIgnoreCase("[]")) {
            iv_saveas.setImageResource(R.drawable.next);
        } else {
            iv_saveas.setImageResource(R.mipmap.save);
        }

        //currency = SharedPrefUserDetail.getString(c, SharedPrefUserDetail.symbol_native, "") + " ";
        currency = obj.getSymbol_native();

        tv_p_cur.setText(currency);
        tv_dp_cur.setText(currency);


        sm = new SettingModel();
        final SettingDB db = new SettingDB(c);
        List<SettingModel> list_setting = db.GetItems();
        sm = list_setting.get(0);

        setCancelable(false);

        tv_title.setText(obj.getName());

        sessionManager = new SessionManager(c);
        String userprofile = sessionManager.getUserprofile();
        try {
            userProfileData = new Gson().fromJson(userprofile, Userprofile.class);
        } catch (Exception e) {
            e.printStackTrace();
        }


        try {
            String tax = obj.getTaxJsonData();
            if (tax != null) {
                //JSONObject jo_tax = new JSONObject(tax);
                //sgst = Double.valueOf(jo_tax.getString("SGST"));
                //cgst = Double.valueOf(jo_tax.getString("CGST"));
                setTax(obj.getTaxJsonData());
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

        setAdditional(obj.getAvailableStock());

        et_offer.setText(obj.getOffer());
        et_xref.setText(obj.getCrossReference());


        //setCancelable(false);
        iv_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (!isdetecting) {
                    listener.onAddImg();
                } else {
                    Toast.makeText(c, "Please wait while detecting images...", Toast.LENGTH_LONG).show();
                }

                //showPictureDialog();
            }
        });

        iv_saveas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String price = et_mrp.getText().toString().trim();
                String title = tv_title.getText().toString().trim();
                String dis_mod = et_dis_per.getText().toString().trim();
                String dis_val = et_dis_val.getText().toString().trim();


                String minPrice = etMinPriceRangeForQ.getText().toString().trim();
                String maxPrice = etMaxPriceRangeForQ.getText().toString().trim();

                int minPval = -2;
                int maxPval = -1;
                if (minPrice.length() > 0) {
                    minPval = Integer.parseInt(etMinPriceRangeForQ.getText().toString().trim());
                }
                if (maxPrice.length() > 0) {
                    maxPval = Integer.parseInt(etMaxPriceRangeForQ.getText().toString().trim());
                }


                if (title.isEmpty()) {
                    Toast.makeText(c, "Title Must Be Required!", Toast.LENGTH_LONG).show();
                } /*else if (price.isEmpty()) {
                    Toast.makeText(c, "Price Must Be Required!", Toast.LENGTH_LONG).show();

                } else if (dis_mod.isEmpty()) {
                    Toast.makeText(c, "Discount Percentage Must Be Required!", Toast.LENGTH_LONG).show();

                } else if (dis_val.isEmpty()) {
                    Toast.makeText(c, "Discount Value Must Be Required!", Toast.LENGTH_LONG).show();
                }*/ else {

                    if (price.isEmpty()) price = "0";
                    if (dis_mod.isEmpty()) dis_mod = "0";
                    if (dis_val.isEmpty()) dis_val = "0";


                    Double dis_price = 0.0;
                    Double dis_perc = 0.0;
                    Double mrp = 0.0;
                    mrp = Double.valueOf(price);

                    dis_price = Double.parseDouble(dis_val);
                    dis_perc = Double.parseDouble(dis_mod);


                    if (dis_price.doubleValue() > mrp.doubleValue()) {
                        Toast.makeText(c, "Discount Value Can Not Larger Than Mrp Value!", Toast.LENGTH_LONG).show();
                    } else if (dis_perc > 100) {
                        Toast.makeText(c, "Discount Percentage Must Be Valid Required!", Toast.LENGTH_LONG).show();
                    } else if (llWholesale.getVisibility() == View.VISIBLE && minPrice.isEmpty()) {
                        Toast.makeText(c, "Min. Price Must Be Required!", Toast.LENGTH_LONG).show();
                    } else if (llWholesale.getVisibility() == View.VISIBLE && maxPval != -1 && minPval == maxPval) {
                        Toast.makeText(c, "Price should not be same!", Toast.LENGTH_LONG).show();
                    } else if (llWholesale.getVisibility() == View.VISIBLE && maxPval != -1 && minPval > maxPval) {
                        Toast.makeText(c, "Please enter valid minimum price!", Toast.LENGTH_LONG).show();
                    } else {

                        if (!obj.getName().trim().equals(title)) {
                            if (al_selet.size() > 0) {
                                str_img = new String[al_selet.size()];
                                new ImageCompress().execute();
                            } else {
                                str_img = new String[0];
                                putdata();
                            }
                        } else {
                            Toast.makeText(c, "Name is already exist !", Toast.LENGTH_LONG).show();
                        }


                    }


                }


            }
        });


        LinearLayoutManager mLayoutManager = new LinearLayoutManager(c, LinearLayoutManager.HORIZONTAL, false);
        rv_selectfile.setLayoutManager(mLayoutManager);


        selectFileAdapter = new SelectFileAdapter(al_selet, c, new SelectFileAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(String item) {

            }

            @Override
            public void onDelete(int pos) {
                al_selet.remove(pos);
                selectFileAdapter.setItem(al_selet);


            }

            @Override
            public void onClickString(String pos) {
                tv_title.setText(pos);
            }

            @Override
            public void onImgClick(List<ModelFile> data, int pos) {
                if (isdetecting) {
                    Toast.makeText(c, "Please wait while detecting images...", Toast.LENGTH_SHORT).show();
                } else {
                    listener.onImgClick(data, pos);
                }


            }
        }, false, true, false);
        rv_selectfile.setAdapter(selectFileAdapter);

        //if(al_selet.size()==0)iv_upload.setVisibility(View.GONE);
        //else iv_upload.setVisibility(View.VISIBLE);


       /* iv_upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                str_img = new String[al_selet.size()];
                uploadimg();

            }
        });*/

        rl_upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        findViewById(R.id.rl_pb_cat).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        llPropertyInfoBlock.setVisibility(View.GONE);

        et_dis_per.addTextChangedListener(this);
        et_dis_val.addTextChangedListener(this);
        et_mrp.addTextChangedListener(this);
        et_cgst.addTextChangedListener(this);
        et_sgst.addTextChangedListener(this);

        etSqFt.addTextChangedListener(this);
        etInfoVal1.addTextChangedListener(this);
        etInfoVal2.addTextChangedListener(this);
        etInfoVal3.addTextChangedListener(this);
        etInfoVal4.addTextChangedListener(this);
        etInfoVal5.addTextChangedListener(this);


        //et_cgst.addTextChangedListener(new ValueTextWatcher(et_cgst, et_sgst, c));
        //et_sgst.addTextChangedListener(new ValueTextWatcher(et_sgst, et_cgst, c));

        et_cgst.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                ApplicationUtil.setFocusedId(v.getId());
            }
        });

        et_sgst.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                ApplicationUtil.setFocusedId(v.getId());
            }
        });


        /******************** Tax from Discount *************************/


        //tv_calc_ctax.setText(currency + String.format("%.2f", centralgst));
        tv_calc_ctax.setText(currency + Inad.getCurrencyDecimal(centralgst, c));
        //tv_calc_stax.setText(currency + String.format("%.2f", stategst));
        tv_calc_stax.setText(currency + Inad.getCurrencyDecimal(stategst, c));

        try {
            c_img = 0;
            JSONArray ja_img = new JSONArray(obj.getImageJsonData().toString());
            str_img = new String[ja_img.length()];
            for (int i = 0; i < ja_img.length(); i++) {
                str_img[i] = ja_img.get(i).toString();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (str_img.length > 0) {
            //downloadimg();
        }

        //c.startActivityForResult(intent, 0);


        try {
            c_img = 0;
            JSONArray ja_img = new JSONArray(obj.getImageJsonData().toString());
            str_img = new String[ja_img.length()];
            for (int i = 0; i < ja_img.length(); i++) {
                str_img[i] = ja_img.get(i).toString();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (str_img.length > 0) {
            downloadimg();
            Detection(0);
        }


        iv_cancel.setOnClickListener(this);

        tv_cur1.setText(currency);
        tv_cur2.setText(currency);

        tv_cur3.setText(currency);
        tv_cur4.setText(currency);
        tv_cur5.setText(currency);
        tv_cur6.setText(currency);
        tv_cur7.setText(currency);

        setWholeSale();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_cancel:
                listener.onDialogImageRunClick(0, "");
                dismiss();
                break;
            case R.id.txt_ok:
                listener.onDialogImageRunClick(1, "");
                dismiss();
                break;
            case R.id.iv_cancel:
                dismiss();
                break;
            default:
                break;
        }

    }


    ArrayList<ModelFile> al_selet = new ArrayList<>();
    int c_img = 0;
    String[] str_img = new String[]{};


    public String getdiscountmrp(Double mrpprice, Double discount_percentage, Double discounted_price) {
        Double discountprice = mrpprice;
        if (discount_percentage > 0) {
            if (mrpprice > 0) {
                discountprice = mrpprice - (mrpprice * (discount_percentage / 100));
            }
        } else if (discounted_price > 0) {
            discountprice = discounted_price;
        }

        return String.valueOf(discountprice);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        calc_tax_value();

    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    public void calc_tax_value() {
        try {
            String price = et_mrp.getText().toString().trim();

            Double mrp = 0.0;
            if (!price.isEmpty()) mrp = Double.valueOf(price);

            if (llPropertyFinancialBlock.getVisibility() == View.VISIBLE) {

                String sqFt = etSqFt.getText().toString().trim();
                if (!sqFt.isEmpty()) {
                    int sqFtVal = Integer.parseInt(sqFt);
                    mrp = mrp * sqFtVal;
                }

                String info1 = etInfoVal1.getText().toString().trim();
                if (!info1.isEmpty()) {
                    int info1Val = Integer.parseInt(info1);
                    mrp = mrp + info1Val;
                }

                String info2 = etInfoVal2.getText().toString().trim();
                if (!info2.isEmpty()) {
                    int info2Val = Integer.parseInt(info2);
                    mrp = mrp + info2Val;
                }

                String info3 = etInfoVal3.getText().toString().trim();
                if (!info3.isEmpty()) {
                    int info3Val = Integer.parseInt(info3);
                    mrp = mrp + info3Val;
                }

                String info4 = etInfoVal4.getText().toString().trim();
                if (!info4.isEmpty()) {
                    int info4Val = Integer.parseInt(info4);
                    mrp = mrp + info4Val;
                }

                String info5 = etInfoVal5.getText().toString().trim();
                if (!info5.isEmpty()) {
                    int info5Val = Integer.parseInt(info5);
                    mrp = mrp + info5Val;
                }
            }

            Double dis_price = 0.0;
            Double dis_perc = 0.0;

            String dis_val = et_dis_val.getText().toString();
            if (!dis_val.isEmpty()) dis_price = Double.parseDouble(et_dis_val.getText().toString());

            String dis_per = et_dis_per.getText().toString();
            if (!dis_per.isEmpty()) dis_perc = Double.parseDouble(et_dis_per.getText().toString());

            Double dis_mrp = Double.valueOf(getdiscountmrp(mrp, dis_perc, dis_price).toString());


            if (dis_mrp.doubleValue() != mrp.doubleValue()) {
                tv_calc_dis.setVisibility(View.VISIBLE);
                //tv_calc_dis.setText(currency + String.format("%.2f", dis_mrp));
                tv_calc_dis.setText(currency + Inad.getCurrencyDecimal(dis_mrp, c));
            } else {

                //tv_calc_dis.setVisibility(View.GONE);
            }
            //tv_calc_dis.setText(currency + String.format("%.2f", dis_mrp));
            tv_calc_dis.setText(currency + Inad.getCurrencyDecimal(dis_mrp, c));

            /******************** Tax from Discount *************************/

            Double sgst = 0.0;
            Double cgst = 0.0;
            String s_cgst = et_cgst.getText().toString();
            if (!s_cgst.isEmpty()) cgst = Double.parseDouble(et_cgst.getText().toString());

            String s_sgst = et_sgst.getText().toString();
            if (!s_sgst.isEmpty()) sgst = Double.parseDouble(et_sgst.getText().toString());

            Double stategst = (dis_mrp / 100.0f) * sgst;
            Double centralgst = (dis_mrp / 100.0f) * cgst;


            //tv_calc_ctax.setText(currency + String.format("%.2f", centralgst));
            tv_calc_ctax.setText(currency + Inad.getCurrencyDecimal(centralgst, c));
            //tv_calc_stax.setText(currency + String.format("%.2f", stategst));
            tv_calc_stax.setText(currency + Inad.getCurrencyDecimal(stategst, c));

            if (dis_mrp > 0) {
                Double total = dis_mrp + stategst + centralgst;
                //tv_total.setText(currency + String.format("%.2f", total) + "" + "");
                tv_total.setText(currency + Inad.getCurrencyDecimal(total, c) + "" + "");
            } else {
                Double total = mrp + stategst + centralgst;
                //tv_total.setText(currency + String.format("%.2f", total) + "" + "");
                tv_total.setText(currency + Inad.getCurrencyDecimal(total, c) + "" + "");
            }

        } catch (Exception e) {

        }


    }


    public void downloadimg() {

        rl_upload.setVisibility(View.VISIBLE);

        int cc = c_img + 1;
        int total = str_img.length;
        tv_progress.setText("Downloading... " + cc + "/" + total);

        File root = new File(Environment.getExternalStorageDirectory()
                + File.separator + c.getString(R.string.app_name) + File.separator);
        root.mkdirs();
        //String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        //final File localFile = File.createTempFile("images", getFileExtension(fileUri));


        String imgnm = str_img[c_img];
        //imgnm = imgnm.substring(imgnm.lastIndexOf("/") + 1);
        File file = new File(root, imgnm);
        String path = file.getAbsolutePath();

        /*if (file.exists()) {
            c_img++;
            al_selet.add(new ModelFile(path, false));
            if (c_img < str_img.length) {
                downloadimg();
            } else {
                rl_upload.setVisibility(View.GONE);
                c_img = 0;

                selectFileAdapter.setItem(al_selet);

            }
        } else {
            TransferObserver downloadObserver = transferUtility.download("cab-videofiles", imgnm, file);
            doenloadObserverListener(downloadObserver, path);
        }*/

        c_img++;
        al_selet.add(new ModelFile(imgnm, false));
        if (c_img < str_img.length) {
            downloadimg();
        } else {
            rl_upload.setVisibility(View.GONE);
            c_img = 0;
            selectFileAdapter.setItem(al_selet);

        }


    }

    public void onAddedImg(ArrayList<ModelFile> al_data) {
        ArrayList<ModelFile> al_temp_selet = al_data;
        al_temp_selet.addAll(al_selet);
        selectFileAdapter.setItem(al_temp_selet);
        al_selet = new ArrayList<>();
        al_selet.addAll(al_temp_selet);
        Detection(0);
    }

    public void onDragImg(ArrayList<ModelFile> al_data) {
        al_selet = al_data;
        selectFileAdapter.setItem(al_selet);

    }

    public void putdata() {
        findViewById(R.id.rl_pb_cat).setVisibility(View.VISIBLE);

        Gson gson = new GsonBuilder().create();
        String json = gson.toJson(obj);// obj is your object


        try {

            String price = et_mrp.getText().toString().trim();
            String dis_mod = et_dis_per.getText().toString().trim();
            String dis_val = et_dis_val.getText().toString().trim();
            String cgst = et_cgst.getText().toString().trim();
            String sgst = et_sgst.getText().toString().trim();
            String offer = et_offer.getText().toString().trim();
            String xref = et_xref.getText().toString().trim();
            String name = tv_title.getText().toString().trim();

            if (price.isEmpty()) price = "0";
            if (dis_mod.isEmpty()) dis_mod = "0";
            if (dis_val.isEmpty()) dis_val = "0";
            if (cgst.isEmpty()) cgst = "0";
            if (sgst.isEmpty()) sgst = "0";


            JSONObject jo = new JSONObject();
            JSONArray ja_new = new JSONArray();

            JSONArray ja_old = new JSONArray();


            JSONObject jsonObj = new JSONObject(json);


            jsonObj.put("name", name);
            jsonObj.put("price", Double.parseDouble(price));
            jsonObj.put("discount_percentage", Double.parseDouble(dis_mod));
            jsonObj.put("discounted_price", Double.parseDouble(dis_val));
            jsonObj.put("offer", offer);
            jsonObj.put("cross_reference", xref);
            jsonObj.put("copy_flag", "Yes");


            JSONObject joFielddata = new JSONObject();
            JSONObject jofield_json_data_f1 = new JSONObject();

            try {
                JSONArray jafield_json_data = new JSONArray();


                String f2 = tvTaxField1.getText().toString().trim();
                if (!f2.isEmpty()) {
                    String val = et_cgst.getText().toString().trim();
                    if (val.isEmpty()) val = "0";
                    jofield_json_data_f1.put(tvTaxField1.getText().toString().trim(), val);

                }

                String f1 = tvTaxField2.getText().toString().trim();
                if (!f1.isEmpty()) {
                    String val = et_sgst.getText().toString().trim();
                    if (val.isEmpty()) val = "0";
                    jofield_json_data_f1.put(tvTaxField2.getText().toString().trim(), val);

                }

                jafield_json_data.put(jofield_json_data_f1);


                joFielddata.put("Tax", jafield_json_data);


            } catch (JSONException e) {
                e.printStackTrace();
            }
            //JsonObject jo_field = new JsonObject();
            //jo_field.addProperty("CGST", Double.parseDouble(cgst));
            //jo_field.addProperty("SGST", Double.parseDouble(sgst));
            //jsonObj.put("field_json_data", jo_field);
            //jsonObj.put("tax_json_data", jo_field);

            //jsonObj.put("field_json_data", jofield_json_data_f1.toString()); // 22 dec
            jsonObj.put("tax_json_data", jofield_json_data_f1.toString());
            jsonObj.put("available_stock", etStockPos.getText().toString().trim());


            //jsonObj.put("field_json_data", joFielddata.toString());
            //jsonObj.put("tax_json_data", joFielddata.toString());

            JsonArray ja_img = new JsonArray();
            for (int i = 0; i < str_img.length; i++) {
                ja_img.add(str_img[i]);
            }

            jsonObj.put("image_json_data", ja_img);

            if (llWholesale.getVisibility() == View.VISIBLE) {
                JsonObject joWholesale = new JsonObject();
                joWholesale.addProperty("minQ", etMinQIFWholesale.getText().toString().trim());
                joWholesale.addProperty("minPrice", etMinPriceRangeForQ.getText().toString().trim());
                joWholesale.addProperty("maxPrice", etMaxPriceRangeForQ.getText().toString().trim());
                joWholesale.addProperty("minDay", etMinDeldays.getText().toString().trim());
                joWholesale.addProperty("maxDay", etMaxDeldays.getText().toString().trim());

                JsonObject jsonObject = new JsonObject();
                jsonObject.add("wholesale", joWholesale);

                jsonObj.put("internal_json_data", jsonObject);
            } else if (llPropertyFinancialBlock.getVisibility() == View.VISIBLE) {

                JsonObject joProperty = new JsonObject();

                joProperty.addProperty("proprty_sqft", etSqFt.getText().toString().trim());
                joProperty.addProperty("uds_sqft", etUds.getText().toString().trim()); // Undivided Share
                joProperty.addProperty("carpet_arae_sqft", etCarpetArea.getText().toString().trim()); // Undivided Share
                joProperty.addProperty("bed", etBr.getText().toString().trim()); // Undivided Share
                joProperty.addProperty("bath", etBath.getText().toString().trim()); // Undivided Share
                joProperty.addProperty("kitchen", etKitchen.getText().toString().trim()); // Undivided Share
                joProperty.addProperty("facing", ""); // Undivided Share


                JsonArray jaExtraInfo = new JsonArray();

                JsonObject joAdditionalInfo = new JsonObject();
                joAdditionalInfo.addProperty(etInfo1.getText().toString().trim(), etInfoVal1.getText().toString().trim());
                jaExtraInfo.add(joAdditionalInfo);

                joAdditionalInfo = new JsonObject();
                joAdditionalInfo.addProperty(etInfo2.getText().toString().trim(), etInfoVal2.getText().toString().trim());
                jaExtraInfo.add(joAdditionalInfo);

                joAdditionalInfo = new JsonObject();
                joAdditionalInfo.addProperty(etInfo3.getText().toString().trim(), etInfoVal3.getText().toString().trim());
                jaExtraInfo.add(joAdditionalInfo);

                joAdditionalInfo = new JsonObject();
                joAdditionalInfo.addProperty(etInfo4.getText().toString().trim(), etInfoVal4.getText().toString().trim());
                jaExtraInfo.add(joAdditionalInfo);

                joAdditionalInfo = new JsonObject();
                joAdditionalInfo.addProperty(etInfo5.getText().toString().trim(), etInfoVal5.getText().toString().trim());
                jaExtraInfo.add(joAdditionalInfo);
                joProperty.add("ExtraInfoPrice", jaExtraInfo);


                JsonObject jsonObject = new JsonObject();
                jsonObject.add("property", joProperty);

                jsonObj.put("internal_json_data", jsonObject);

            }


            obj = new Gson().fromJson(jsonObj.toString(), CatalougeContent.class);

            //jsonObj.put("field_json_data",new JSONObject());

           /* if(isadd)
            {
                jsonObj.put("flag", "add");
                ja_new.put(jsonObj);
                jo.put("newdata", ja_new);
                jo.put("olddata", ja_old);
            }
            else {
                jsonObj.put("flag", "edit");
                ja_old.put(jsonObj);
                jo.put("olddata", ja_old);
                jo.put("newdata", ja_new);
            }
*/

            jsonObj.put("flag", "add");
            ja_new.put(jsonObj);
            jo.put("newdata", ja_new);
            jo.put("olddata", ja_old);

            CatRequest catalougeContent = new CatRequest();

            catalougeContent = new Gson().fromJson(jo.toString(), CatRequest.class);

            //putData(catalougeContent);

            if (ShareModel.getI().alCategory != null && !ShareModel.getI().alCategory.equalsIgnoreCase("[]")) {
                findViewById(R.id.rl_pb_cat).setVisibility(View.GONE);
                ShareModel.getI().catRequest = catalougeContent;
                c.startActivityForResult(new Intent(c, EditCatlogueCategoryActivity.class), 22);
                dismiss();
            } else {
                putData(catalougeContent);
            }


        } catch (JSONException e) {
            e.printStackTrace();
            findViewById(R.id.rl_pb_cat).setVisibility(View.GONE);
        }


    }

    public void putData(CatRequest jo_cat) {
        sessionManager = new SessionManager(c);

        findViewById(R.id.rl_pb_cat).setVisibility(View.VISIBLE);

        ApiInterface apiService =
                ApiClient.getClient(c).create(ApiInterface.class);

        Call call = apiService.PutCatalogue(sessionManager.getsession(), jo_cat);

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                findViewById(R.id.rl_pb_cat).setVisibility(View.GONE);

                if (response.isSuccessful()) {
                    try {
                        dismiss();
                      /*  c.finish();
                        c.startActivity(getIntent().putExtra("CatalougeContent", json));
                       */

                        listener.onSave();


                    } catch (Exception e) {


                    }

                } else {

                    try {
                    } catch (Exception e) {
                        Toast.makeText(c, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }

            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // Log error here since request failed
                findViewById(R.id.rl_pb_cat).setVisibility(View.GONE);

            }
        });
    }

    SessionManager sessionManager;

    ArrayList<ModelFile> CompressItemSelect;

    public class ImageCompress extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            CompressItemSelect = new ArrayList();
            findViewById(R.id.rl_pb_cat).setVisibility(View.VISIBLE);

        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                for (int i = 0; i < al_selet.size(); i++) {
                    if (al_selet.get(i).isIsoffline()) {
                        File ImageFile = new File(al_selet.get(i).getImgpath());
                        File compressedImageFile = null;
                        String strFile = "";
                        try {
                            compressedImageFile = new Compressor(c).compressToFile(ImageFile);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        strFile = String.valueOf(compressedImageFile);

                        CompressItemSelect.add(new ModelFile(strFile, al_selet.get(i).isIsoffline()));
                    } else {
                        CompressItemSelect.add(new ModelFile(al_selet.get(i).getImgpath(), al_selet.get(i).isIsoffline()));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            findViewById(R.id.rl_pb_cat).setVisibility(View.GONE);
            al_selet = CompressItemSelect;
            uploadimg();
        }
    }

    AmazonS3 s3;
    TransferUtility transferUtility;


    public void inits3() {
        s3 = new AmazonS3Client(new BasicAWSCredentials(c.getString(R.string.accesskey), c.getString(R.string.secretkey)));
        transferUtility = new TransferUtility(s3, c.getApplicationContext());

    }

    String namegsxsax = "";

    public void uploadimg() {
        rl_upload.setVisibility(View.VISIBLE);
        int cc = c_img + 1;
        int total = al_selet.size();
        tv_progress.setText("Uploading " + cc + "/" + total);


        File file = new File(al_selet.get(c_img).getImgpath());
        namegsxsax = "IMG_" + System.currentTimeMillis() + ".jpg";

        if (al_selet.get(c_img).isIsoffline()) {
            TransferObserver transferObserver = transferUtility.upload("cab-videofiles", namegsxsax, file);
            transferObserverListener(transferObserver);
        } else {  // if already upload

            String imgnm = al_selet.get(c_img).getImgpath();
            imgnm = imgnm.substring(imgnm.lastIndexOf("/") + 1);

            str_img[c_img] = c.getString(R.string.baseaws) + imgnm;

            c_img++;
            if (c_img < al_selet.size()) {
                uploadimg();
            } else {
                rl_upload.setVisibility(View.GONE);
                c_img = 0;
                putdata();
            }
        }


    }

    public void transferObserverListener(final TransferObserver transferObserver) {

        transferObserver.setTransferListener(new TransferListener() {
            @Override
            public void onStateChanged(int id, TransferState state) {
                if (state.name().equals("COMPLETED")) {
                    //str_img.put(getString(R.string.baseaws)+namegsxsax);
                    str_img[c_img] = c.getString(R.string.baseaws) + namegsxsax;

                    c_img++;
                    if (c_img < al_selet.size()) {
                        uploadimg();
                    } else {
                        rl_upload.setVisibility(View.GONE);
                        c_img = 0;
                        putdata();

                      /*  Gson gson = new GsonBuilder().create();
                        String json = gson.toJson(obj);// obj is your object


                        try {

                            JSONObject jo = new JSONObject();
                            JSONArray ja_new = new JSONArray();
                            jo.put("newdata", ja_new);
                            JSONArray ja_old = new JSONArray();

                            JsonArray ja_img = new JsonArray();
                            for (int i=0;i<str_img.length;i++)
                            {
                                ja_img.add(str_img[i]);
                            }

                            JSONObject jsonObj = new JSONObject(json);
                            jsonObj.put("image_json_data", ja_img);


                            //jsonObj.put("field_json_data",new JSONObject());

                            ja_old.put(jsonObj);
                            jo.put("olddata", ja_old);


                            CatRequest catalougeContent = new CatRequest();

                            catalougeContent = new Gson().fromJson(jo.toString(), CatRequest.class);

                            putData(catalougeContent);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            findViewById(R.id.rl_pb_cat).setVisibility(View.GONE);
                        }*/
                    }
                } else if (state.name().equals("FAILED")) {
                    rl_upload.setVisibility(View.GONE);
                    Toast.makeText(c, "Failed Uploading !", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                try {
                    int percentage = (int) (bytesCurrent / bytesTotal * 100);


                    //mProgressDialog.setProgress(percentage);


                    //Log.e("percentage ", " : " + percentage);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(int id, Exception ex) {

                Log.e("error", "error");
            }

        });
    }

    Vision vision;

    public void initvision() {
        Vision.Builder visionBuilder = new Vision.Builder(
                new NetHttpTransport(),
                new AndroidJsonFactory(),
                null);

        visionBuilder.setVisionRequestInitializer(
                new VisionRequestInitializer("AIzaSyBljKEMCnXTENIltyt8_EzpIHRgKoNLbcI"));

        vision = visionBuilder.build();


    }

    private void Detection(final int pos) {
        AsyncTask.execute(new Runnable() {


            @Override
            public void run() {
                try {
                    //InputStream inputStream = new FileInputStream("");;


                    if (!sm.getIsvisiondetect().equals("1")) return;

                    isdetecting = true;
                    if (al_selet.get(pos).isIsdetect()) {

                        c.runOnUiThread(new Runnable() {
                            public void run() {

                                int c_p = pos;
                                c_p++;
                                if (c_p < al_selet.size()) {
                                    Detection(c_p);
                                } else {
                                    isdetecting = false;
                                    Toast.makeText(c, "Image Detected !", Toast.LENGTH_LONG).show();
                                }
                            }
                        });
                        return;
                    }
                    InputStream inputStream = null;
                    if (al_selet.get(pos).isIsoffline()) {
                        inputStream = new FileInputStream(al_selet.get(pos).getImgpath());
                    } else {
                        inputStream = new URL(al_selet.get(pos).getImgpath()).openStream();
                    }

                    byte[] photoData = org.apache.commons.io.IOUtils.toByteArray(inputStream);

                    com.google.api.services.vision.v1.model.Image inputImage = new com.google.api.services.vision.v1.model.Image();
                    inputImage.encodeContent(photoData);

                   /* Feature landFeature = new Feature();
                    landFeature.setType("LANDMARK_DETECTION");*/


                    Feature Webdetection = new Feature();
                    Webdetection.setType("WEB_DETECTION");
                    //Webdetection.setMaxResults(2);

                    Feature desiredFeature = new Feature();
                    desiredFeature.setType("TEXT_DETECTION");

                    Feature landdetect = new Feature();
                    landdetect.setType("LANDMARK_DETECTION");

                    Feature facedetect = new Feature();
                    facedetect.setType("FACE_DETECTION");


                    AnnotateImageRequest request = new AnnotateImageRequest();
                    request.setImage(inputImage);
                    request.setFeatures(Arrays.asList(Webdetection, desiredFeature, landdetect));

                    BatchAnnotateImagesRequest batchRequest = new BatchAnnotateImagesRequest();
                    batchRequest.setRequests(Arrays.asList(request));

                    BatchAnnotateImagesResponse batchResponse =
                            vision.images().annotate(batchRequest).execute();

                    ArrayList<ModelFile> al_detect = new ArrayList<>();

                    WebDetection web = batchResponse.getResponses().get(0).getWebDetection();
                    //TextAnnotation tax = batchResponse.getResponses().get(0).getFullTextAnnotation();
                    List<EntityAnnotation> tax = batchResponse.getResponses().get(0).getTextAnnotations();
                    List<EntityAnnotation> landmark = batchResponse.getResponses().get(0).getLandmarkAnnotations();

                    al_detect.add(new ModelFile(true, "Web Detect", pos));
                    if (web != null) {
                        List<WebEntity> webEntities = web.getWebEntities();
                        for (int a = 0; a < webEntities.size(); a++) {
                            String det_str = webEntities.get(a).getDescription();
                            al_detect.add(new ModelFile(false, det_str, pos));
                        }
                    }

                   /* if (tax != null) {
                        al_detect.add(new ModelFile(true, "Full Text Detect"));
                        al_detect.add(new ModelFile(false, tax.toString()));
                    }*/
                    al_detect.add(new ModelFile(true, "Text Detect", pos));
                    if (tax != null) {

                        for (int a = 0; a < tax.size(); a++) {
                            String det_str = tax.get(a).getDescription();
                            al_detect.add(new ModelFile(false, det_str, pos));
                        }
                    }

                    al_detect.add(new ModelFile(true, "LandMark Detect", pos));
                    if (landmark != null) {
                        for (int a = 0; a < landmark.size(); a++) {
                            String det_str = landmark.get(a).getDescription();
                            al_detect.add(new ModelFile(false, det_str, pos));
                        }
                    }


                    al_selet.get(pos).setAl_detect(al_detect);
                    //al_selet.get(pos).setDescription(description);
                    al_selet.get(pos).setIsdetect(true);


                    c.runOnUiThread(new Runnable() {
                        public void run() {
                            selectFileAdapter.setItem(al_selet);
                            int c_p = pos;
                            c_p++;
                            if (c_p < al_selet.size()) {
                                Detection(c_p);
                            } else {
                                isdetecting = false;
                                Toast.makeText(c, "Image Detected !", Toast.LENGTH_LONG).show();
                            }
                        }
                    });

                } catch (Exception e) {
                    Log.d("ERROR", e.getMessage());
                }
            }


        });
    }

    boolean isdetecting = false;

    Userprofile userProfileData;

    public void setTaxFromProfile() {
        String userprofile = sessionManager.getUserprofile();
        try {
            userProfileData = new Gson().fromJson(userprofile, Userprofile.class);
            setTax(userProfileData.field_json_data);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setTax(String field_json_data) throws JSONException {

        Double dis_price = 0.0;
        Double dis_perc = 0.0;

        if (!obj.getDiscountedPrice().equals(""))
            dis_price = Double.valueOf(obj.getDiscountedPrice());
        if (!obj.getDiscountPercentage().equals(""))
            dis_perc = Double.valueOf(obj.getDiscountPercentage());

        //et_dis_per.setText(String.format("%.2f", dis_perc) + "");
        et_dis_per.setText(Inad.getCurrencyDecimal(dis_perc, c) + "");
        //et_dis_val.setText(String.format("%.2f", dis_price) + "");
        et_dis_val.setText(Inad.getCurrencyDecimal(dis_price, c) + "");


        Double sgst = 0.0;
        Double cgst = 0.0;

        if (field_json_data != null) {

            String Tax = JsonObjParse.getValueEmpty(field_json_data, "Tax");


            if (!Tax.isEmpty()) {

                JSONObject joTax = new JSONObject(field_json_data);

                JSONArray arr = joTax.getJSONArray("Tax");

                JSONObject element;

                et_cgst.setText("");
                tvTaxField1.setText("");
                et_sgst.setText("");
                tvTaxField2.setText("");

                llCgst.setVisibility(View.GONE);
                llSgst.setVisibility(View.GONE);

                for (int i = 0; i < arr.length(); i++) {
                    element = arr.getJSONObject(i); // which for example will be Types,TotalPoints,ExpiringToday in the case of the first array(All_Details)

                    Iterator keys = element.keys();

                    while (keys.hasNext()) {
                        try {
                            String key = (String) keys.next();

                            String taxVal = JsonObjParse.getValueFromJsonObj(element, key);

                            if (et_cgst.getText().toString().isEmpty()) {
                                et_cgst.setText(taxVal);
                                tvTaxField1.setText(key);
                                llCgst.setVisibility(View.VISIBLE);
                                cgst = Double.valueOf(taxVal);


                            } else {
                                et_sgst.setText(taxVal);
                                tvTaxField2.setText(key);
                                llSgst.setVisibility(View.VISIBLE);
                                sgst = Double.valueOf(taxVal);

                            }

                            Log.e("key", key);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                /*  Charges  */
            } else {
                // Update
                et_cgst.setText("");
                tvTaxField1.setText("");
                et_sgst.setText("");
                tvTaxField2.setText("");

                llCgst.setVisibility(View.GONE);
                llSgst.setVisibility(View.GONE);


                JSONObject joTax = new JSONObject(field_json_data);

                Iterator keys = joTax.keys();

                while (keys.hasNext()) {
                    try {
                        String key = (String) keys.next();

                        String taxVal = JsonObjParse.getValueFromJsonObj(joTax, key);

                        if (et_cgst.getText().toString().isEmpty()) {
                            //sgst = Double.valueOf(jo_tax.getString("SGST"));
                            //cgst = Double.valueOf(jo_tax.getString("CGST"));
                            cgst = Double.valueOf(taxVal);
                            et_cgst.setText(taxVal);
                            tvTaxField1.setText(key);
                            llCgst.setVisibility(View.VISIBLE);

                        } else {
                            sgst = Double.valueOf(taxVal);
                            et_sgst.setText(taxVal);
                            tvTaxField2.setText(key);
                            llSgst.setVisibility(View.VISIBLE);
                        }

                        Log.e("key", key);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        //et_cgst.setText(String.format("%.2f", cgst) + "");
        et_cgst.setText(Inad.getCurrencyDecimal(cgst, c) + "");
        //et_sgst.setText(String.format("%.2f", sgst) + "");
        et_sgst.setText(Inad.getCurrencyDecimal(sgst, c) + "");

        Double mrp = 0.0;
        if (!obj.getPrice().isEmpty()) mrp = Double.valueOf(obj.getPrice());


        Double dis_mrp = Double.valueOf(getdiscountmrp(mrp, dis_perc, dis_price).toString());
        if (dis_mrp.doubleValue() != mrp.doubleValue()) {
            //tv_calc_dis.setText(currency + String.format("%.2f", dis_mrp));
            tv_calc_dis.setText(currency + Inad.getCurrencyDecimal(dis_mrp, c));
        } else {
            //tv_calc_dis.setVisibility(View.GONE);
        }
        //tv_calc_dis.setText(currency + String.format("%.2f", dis_mrp));
        tv_calc_dis.setText(currency + Inad.getCurrencyDecimal(dis_mrp, c));

        stategst = (dis_mrp / 100.0f) * sgst;
        centralgst = (dis_mrp / 100.0f) * cgst;


        //et_mrp.setText(String.format("%.2f", mrp) + "");
        et_mrp.setText(Inad.getCurrencyDecimal(mrp, c) + "");
        Double total = dis_mrp + stategst + centralgst;
        //tv_total.setText(currency + String.format("%.2f", total) + "" + "");
        tv_total.setText(currency + Inad.getCurrencyDecimal(total, c) + "" + "");


    }

    Double stategst = 0.0;
    Double centralgst = 0.0;

    @BindView(R.id.etStockPos)
    EditText etStockPos;

    @BindView(R.id.llStock)
    LinearLayout llStock;


    public void setAdditional(String availableStock) { // from user profile

        llStock.setVisibility(View.GONE);
        etStockPos.setText("");

        if (availableStock != null) {
            etStockPos.setText(availableStock + "");
        }

        if (userProfileData.field_json_data != null) {

            String show_stock_position = JsonObjParse.getValueEmpty(userProfileData.field_json_data, "show_stock_position");

            if (show_stock_position.equalsIgnoreCase("yes")) {
                llStock.setVisibility(View.VISIBLE);
            } else {
                llStock.setVisibility(View.GONE);
                etStockPos.setText("");
            }
        }

    }

    // Wholesale
    @BindView(R.id.llWholesale)
    LinearLayout llWholesale;

    @BindView(R.id.llDiscBlck)
    LinearLayout llDiscBlck;

    @BindView(R.id.llPrice)
    LinearLayout llPrice;

    @BindView(R.id.llPriceRange)
    LinearLayout llPriceRange;

    @BindView(R.id.llDeliveryRange)
    LinearLayout llDeliveryRange;

    @BindView(R.id.etMinQIFWholesale)
    EditText etMinQIFWholesale;

    @BindView(R.id.etMinPriceRangeForQ)
    EditText etMinPriceRangeForQ;

    @BindView(R.id.etMaxPriceRangeForQ)
    EditText etMaxPriceRangeForQ;

    @BindView(R.id.etMinDeldays)
    EditText etMinDeldays;

    @BindView(R.id.etMaxDeldays)
    EditText etMaxDeldays;

    public void checkWholeSale(String field_json_data) {

        // internal_json_data


        String service_type_of = JsonObjParse.getValueEmpty(field_json_data, "service_type_of");
        llDiscBlck.setVisibility(View.VISIBLE);
        llPrice.setVisibility(View.VISIBLE);
        llWholesale.setVisibility(View.GONE);
        llPriceRange.setVisibility(View.GONE);
        llDeliveryRange.setVisibility(View.GONE);

        if (service_type_of.equalsIgnoreCase(c.getString(R.string.wholesale))) {
            llWholesale.setVisibility(View.VISIBLE);
            llPriceRange.setVisibility(View.VISIBLE);
            llDeliveryRange.setVisibility(View.VISIBLE);
            llDiscBlck.setVisibility(View.GONE);
            llPrice.setVisibility(View.GONE);
        }

        /*new GetServiceTypeFromBusiUserProfile(this, sessionManager.getcurrentu_nm(), new GetServiceTypeFromBusiUserProfile.Apicallback() {
            @Override
            public void onGetResponse(String a, String response, String sms_emp) {

                String field_json_data = "";
                try {

                    JSONObject Jsonresponse = new JSONObject(response);
                    JSONObject Jsonresponseinfo = Jsonresponse.getJSONObject("response");
                    JSONArray jaData = Jsonresponseinfo.getJSONArray("data");
                    if (jaData.length() > 0) {
                        JSONObject data = jaData.getJSONObject(0);
                        field_json_data = JsonObjParse.getValueEmpty(data.toString(), "field_json_data");

                        String service_type_of = JsonObjParse.getValueEmpty(field_json_data, "service_type_of");
                        if (service_type_of.equalsIgnoreCase(getString(R.string.wholesale))) {

                        }

                    } else {

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
*/
    }

    public void setWholeSale() {

        // internal_json_data
        String wholesale = JsonObjParse.getValueEmpty(obj.getInternalJsonData(), "wholesale");
        String property = JsonObjParse.getValueEmpty(obj.getInternalJsonData(), "property");

        llDiscBlck.setVisibility(View.VISIBLE);
        llPrice.setVisibility(View.VISIBLE);
        llWholesale.setVisibility(View.GONE);
        llPriceRange.setVisibility(View.GONE);
        llDeliveryRange.setVisibility(View.GONE);

        llPropertyFinancialBlock.setVisibility(View.GONE);
        llPropertyInfoBlock.setVisibility(View.GONE);
        llNotProperty.setVisibility(View.VISIBLE);

        if (!wholesale.isEmpty()) {
            llWholesale.setVisibility(View.VISIBLE);
            llPriceRange.setVisibility(View.VISIBLE);
            llDeliveryRange.setVisibility(View.VISIBLE);
            llDiscBlck.setVisibility(View.GONE);
            llPrice.setVisibility(View.GONE);

            String minQ = JsonObjParse.getValueEmpty(wholesale, "minQ");
            etMinQIFWholesale.setText(minQ);

            String minPrice = JsonObjParse.getValueEmpty(wholesale, "minPrice");
            etMinPriceRangeForQ.setText(minPrice);

            String maxPrice = JsonObjParse.getValueEmpty(wholesale, "maxPrice");
            etMaxPriceRangeForQ.setText(maxPrice);

            String minDay = JsonObjParse.getValueEmpty(wholesale, "minDay");
            etMinDeldays.setText(minDay);

            String maxDay = JsonObjParse.getValueEmpty(wholesale, "maxDay");
            etMaxDeldays.setText(maxDay);


        } else if (!property.isEmpty()) {
            //llDiscBlck.setVisibility(View.GONE);
            llPropertyFinancialBlock.setVisibility(View.VISIBLE);
            llPropertyInfoBlock.setVisibility(View.VISIBLE);
            llNotProperty.setVisibility(View.GONE);


            String proprty_sqft = JsonObjParse.getValueEmpty(property, "proprty_sqft");
            etSqFt.setText(proprty_sqft);

            String uds_sqft = JsonObjParse.getValueEmpty(property, "uds_sqft");
            etUds.setText(uds_sqft);

            String carpet_arae_sqft = JsonObjParse.getValueEmpty(property, "carpet_arae_sqft");
            etCarpetArea.setText(carpet_arae_sqft);

            String bed = JsonObjParse.getValueEmpty(property, "bed");
            etBr.setText(bed);

            String bath = JsonObjParse.getValueEmpty(property, "bath");
            etBath.setText(bath);

            String kitchen = JsonObjParse.getValueEmpty(property, "kitchen");
            etKitchen.setText(kitchen);

            try {
                String ExtraInfoPrice = JsonObjParse.getValueEmpty(property, "ExtraInfoPrice");
                setExtraPropertyInfo(ExtraInfoPrice);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            calc_tax_value();


        }


    }

    // Property

    @BindView(R.id.llPropertyFinancialBlock)
    LinearLayout llPropertyFinancialBlock;

    @BindView(R.id.llPropertyInfoBlock)
    LinearLayout llPropertyInfoBlock;

    @BindView(R.id.llNotProperty)
    LinearLayout llNotProperty;

    @BindView(R.id.etSqFt)
    EditText etSqFt;
    @BindView(R.id.etBr)
    EditText etBr;
    @BindView(R.id.etUds)
    EditText etUds;
    @BindView(R.id.etCarpetArea)
    EditText etCarpetArea;
    @BindView(R.id.etBath)
    EditText etBath;
    @BindView(R.id.etKitchen)
    EditText etKitchen;

    @BindView(R.id.etInfo1)
    EditText etInfo1;
    @BindView(R.id.tv_cur3)
    TextView tv_cur3;
    @BindView(R.id.etInfoVal1)
    EditText etInfoVal1;
    @BindView(R.id.etInfo2)
    EditText etInfo2;
    @BindView(R.id.tv_cur4)
    TextView tv_cur4;
    @BindView(R.id.etInfoVal2)
    EditText etInfoVal2;
    @BindView(R.id.etInfo3)
    EditText etInfo3;
    @BindView(R.id.tv_cur5)
    TextView tv_cur5;
    @BindView(R.id.etInfoVal3)
    EditText etInfoVal3;
    @BindView(R.id.etInfo4)
    EditText etInfo4;
    @BindView(R.id.tv_cur6)
    TextView tv_cur6;
    @BindView(R.id.etInfoVal4)
    EditText etInfoVal4;
    @BindView(R.id.etInfo5)
    EditText etInfo5;
    @BindView(R.id.tv_cur7)
    TextView tv_cur7;
    @BindView(R.id.etInfoVal5)
    EditText etInfoVal5;

    /* public void setExtraPropertyInfo(String extraInfo) throws JSONException {
         if (extraInfo != null && !extraInfo.isEmpty()) {
             JSONObject joExtra = new JSONObject(extraInfo);
             Iterator keys = joExtra.keys();
             int i = 0;
             while (keys.hasNext()) {
                 try {
                     String key = (String) keys.next();
                     String info = JsonObjParse.getValueFromJsonObj(joExtra, key);
                     switch (i) {
                         case 0:
                             etInfo1.setText(key);
                             etInfoVal1.setText(info);
                             break;
                         case 1:
                             etInfo2.setText(key);
                             etInfoVal2.setText(info);
                             break;
                         case 2:
                             etInfo3.setText(key);
                             etInfoVal3.setText(info);
                             break;
                         case 3:
                             etInfo4.setText(key);
                             etInfoVal4.setText(info);
                             break;
                         case 4:
                             etInfo5.setText(key);
                             etInfoVal5.setText(info);
                             break;
                     }
                     i++;
                 } catch (Exception e) {
                     e.printStackTrace();
                 }
             }
         }
     }
 */
    public void setExtraPropertyInfo(String extraInfo) throws JSONException {
        if (extraInfo != null && !extraInfo.isEmpty()) {

            JSONArray jaExtraInfo = new JSONArray(extraInfo);

            for (int i = 0; i < jaExtraInfo.length(); i++) {
                JSONObject jsonObject = jaExtraInfo.getJSONObject(i);

                Iterator keys = jsonObject.keys();
                while (keys.hasNext()) {
                    try {
                        String key = (String) keys.next();
                        String info = JsonObjParse.getValueFromJsonObj(jsonObject, key);
                        switch (i) {
                            case 0:
                                etInfo1.setText(key);
                                etInfoVal1.setText(info);
                                break;
                            case 1:
                                etInfo2.setText(key);
                                etInfoVal2.setText(info);
                                break;
                            case 2:
                                etInfo3.setText(key);
                                etInfoVal3.setText(info);
                                break;
                            case 3:
                                etInfo4.setText(key);
                                etInfoVal4.setText(info);
                                break;
                            case 4:
                                etInfo5.setText(key);
                                etInfoVal5.setText(info);
                                break;
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }
        }
    }


}
