package com.cab.digicafe.Dialogbox;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatRadioButton;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.cab.digicafe.Helper.SessionManager;
import com.cab.digicafe.R;


public class AddAddress extends AlertDialog implements
        View.OnClickListener {
    private final OnDialogClickListener listener;
    public Activity c;
    public Dialog d;
    public TextView txt_cancel, txt_ok, tv_status;
    EditText et_address;
    String add;
    AppCompatRadioButton rb_noblur, rb_innerblur, rb_normal, rb_outer, rb_solid;

    /*  new CustomDialogClass(MainActivity.this, new CustomDialogClass.OnDialogClickListener() {
                    @Override
                    public void onDialogImageRunClick() {
                        Log.e("Ok","Ok");


                    }
                }).show(); */

    public AddAddress(Activity a, OnDialogClickListener listener, String add) {
        super(a);
        // TODO Auto-generated constructor stub
        this.c = a;
        this.listener = listener;
        this.add = add;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_box_address);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        et_address = (EditText) findViewById(R.id.et_address);
        txt_ok = (TextView) findViewById(R.id.txt_ok);
        txt_cancel = (TextView) findViewById(R.id.txt_cancel);
        tv_status = (TextView) findViewById(R.id.tv_status);

        tv_status.setVisibility(View.VISIBLE);

        SessionManager sm = new SessionManager();
        if (sm.getbusi_status().equals("Open")) {
            tv_status.setText("Close the shop ?");
        } else {
            tv_status.setText("Open the Shop ?");
        }


        //tv_status.setText("");

        txt_cancel.setOnClickListener(this);
        txt_ok.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_cancel:
                listener.onDialogImageRunClick(0, "");
                dismiss();
                break;
            case R.id.txt_ok:

                if (et_address.getText().toString().trim().equals(add)) {
                    listener.onDialogImageRunClick(1, "");
                    dismiss();
                } else {
                    Toast.makeText(c, "Invalid Password !", Toast.LENGTH_SHORT).show();
                }


                break;
            default:
                break;
        }

    }

    public interface OnDialogClickListener {
        void onDialogImageRunClick(int pos, String add);
    }


}
