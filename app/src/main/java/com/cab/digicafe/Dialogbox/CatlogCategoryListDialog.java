package com.cab.digicafe.Dialogbox;

import android.app.Activity;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.cab.digicafe.Adapter.EmployessListAdapter;
import com.cab.digicafe.Model.Employee;
import com.cab.digicafe.R;

import java.util.ArrayList;


public class CatlogCategoryListDialog extends AlertDialog implements
        View.OnClickListener {
    private final OnDialogClickListener listener;
    public Activity c;
    private RecyclerView recyclerView;
    private EmployessListAdapter mAdapter;
    ArrayList<Employee> alCat = new ArrayList<>();
    ProgressBar pb;
    Employee item;
    boolean isprofile = false;
    public TextView txt_cancel, txt_ok;
    /*  new CustomDialogClass(MainActivity.this, new CustomDialogClass.OnDialogClickListener() {
                    @Override
                    public void onDialogImageRunClick() {
                        Log.e("Ok","Ok");
                    }
                }).show(); */

    public CatlogCategoryListDialog(Activity a, OnDialogClickListener listener, ArrayList<Employee> alCat) {
        super(a);
        // TODO Auto-generated constructor stub
        this.c = a;
        this.listener = listener;
        this.alCat = alCat;
        this.isprofile = isprofile;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_employee);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));


        recyclerView = (RecyclerView) findViewById(R.id.rv_employeee);
        pb = (ProgressBar) findViewById(R.id.pb);
        txt_ok = (TextView) findViewById(R.id.txt_ok);
        txt_cancel = (TextView) findViewById(R.id.txt_cancel);


        setCancelable(true);
        pb.setVisibility(View.GONE);


        mAdapter = new EmployessListAdapter(alCat, c, new EmployessListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Employee ite) {

                item = ite;
                //dismiss();
                //listener.onDialogImageRunClick(0, item);
            }
        });

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(c);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        recyclerView.setNestedScrollingEnabled(false);

        //getEmployee();
        //mAdapter.setitem(alCat);
        txt_cancel.setOnClickListener(this);
        txt_ok.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.txt_cancel:
                listener.onDialogImageRunClick(0, null);
                dismiss();
                break;
            case R.id.txt_ok:
                if (item != null) {
                    dismiss();
                    listener.onDialogImageRunClick(1, item);
                } else {
                    Toast.makeText(c, "Select Employee!", Toast.LENGTH_SHORT).show();
                }

                break;

            default:
                break;
        }

    }

    public interface OnDialogClickListener {
        void onDialogImageRunClick(int pos, Employee add);
    }


}
