package com.cab.digicafe.Dialogbox;

import android.app.Activity;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.cab.digicafe.Adapter.EmployessListAdapter;
import com.cab.digicafe.Helper.SessionManager;
import com.cab.digicafe.Model.Employee;
import com.cab.digicafe.R;
import com.cab.digicafe.Rest.ApiClient;
import com.cab.digicafe.Rest.ApiInterface;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class EmployeeListDialog extends AlertDialog implements
        View.OnClickListener {
    private final OnDialogClickListener listener;
    public Activity c;
    private RecyclerView recyclerView;
    private EmployessListAdapter mAdapter;
    ArrayList<Employee> al_aggre = new ArrayList<>();
    ProgressBar pb;
    Employee item;
    boolean isprofile = false;
    public TextView txt_cancel, txt_ok, tv_empty;
    /*  new CustomDialogClass(MainActivity.this, new CustomDialogClass.OnDialogClickListener() {
                    @Override
                    public void onDialogImageRunClick() {
                        Log.e("Ok","Ok");
                    }
                }).show(); */

    public EmployeeListDialog(Activity a, OnDialogClickListener listener) {
        super(a);
        // TODO Auto-generated constructor stub
        this.c = a;
        this.listener = listener;

        this.isprofile = isprofile;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_employee);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));


        recyclerView = (RecyclerView) findViewById(R.id.rv_employeee);
        pb = (ProgressBar) findViewById(R.id.pb);
        txt_ok = (TextView) findViewById(R.id.txt_ok);
        txt_cancel = (TextView) findViewById(R.id.txt_cancel);
        tv_empty = (TextView) findViewById(R.id.tv_empty);
        tv_empty.setVisibility(View.GONE);

        setCancelable(false);
        pb.setVisibility(View.VISIBLE);


        mAdapter = new EmployessListAdapter(al_aggre, c, new EmployessListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Employee ite) {

                item = ite;
                //dismiss();
                //listener.onDialogImageRunClick(0, item);
            }
        });

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(c);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        recyclerView.setNestedScrollingEnabled(false);

        getEmployee();
        txt_cancel.setOnClickListener(this);
        txt_ok.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.txt_cancel:
                listener.onDialogImageRunClick(0, null);
                dismiss();
                break;
            case R.id.txt_ok:
                if (item != null) {
                    dismiss();
                    listener.onDialogImageRunClick(1, item);
                } else {
                    Toast.makeText(c, "Select Employee!", Toast.LENGTH_SHORT).show();
                }

                break;

            default:
                break;
        }

    }

    public interface OnDialogClickListener {
        void onDialogImageRunClick(int pos, Employee add);
    }

    String tag = "";
    SessionManager sessionManager;

    public void getEmployee() {
        sessionManager = new SessionManager(c);

        ApiInterface apiService = ApiClient.getClient(c).create(ApiInterface.class);

        Call call;
        call = apiService.getEmployee(sessionManager.getsession());

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {

                try {
                    pb.setVisibility(View.GONE);
                    if (response.isSuccessful()) {

                        String a = new Gson().toJson(response.body());
                        JSONObject jObjRes = new JSONObject(a);
                        JSONObject jObjdata = jObjRes.getJSONObject("response");
                        JSONObject jObjResponse = jObjdata.getJSONObject("data");
                        String totalRecord = jObjResponse.getString("totalRecord");
                        JSONArray jObjcontacts = jObjResponse.getJSONArray("content");

                        String displayEndRecord = jObjResponse.getString("displayEndRecord");
                        if (totalRecord.equals(displayEndRecord)) {
                            // isdataavailable = false;
                        } else {
                            // isdataavailable = true;
                        }


                        for (int i = 0; i < jObjcontacts.length(); i++) {
                            if (jObjcontacts.get(i) instanceof JSONObject) {
                                JSONObject jsnObj = (JSONObject) jObjcontacts.get(i);
                                Employee obj = new Gson().fromJson(jsnObj.toString(), Employee.class);
                                al_aggre.add(obj);
                            }
                        }

                        mAdapter.setitem(al_aggre);

                        if (al_aggre.size() == 0) {
                            tv_empty.setVisibility(View.VISIBLE);
                        } else {
                            tv_empty.setVisibility(View.GONE);
                        }

                    } else {
                        try {
                            JSONObject jObjError = new JSONObject(response.errorBody().string());
                            JSONObject jObjErrorresponse = jObjError.getJSONObject("response");
                            Toast.makeText(c, jObjErrorresponse.getString("errormsg"), Toast.LENGTH_LONG).show();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } catch (Exception e) {

                }


            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // Log error here since request failed
                try {
                    pb.setVisibility(View.GONE);
                } catch (Exception e) {

                }

            }
        });


    }


}
