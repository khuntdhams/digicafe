package com.cab.digicafe.Dialogbox;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cab.digicafe.MyCustomClass.PlayAnim;
import com.cab.digicafe.R;

import butterknife.BindView;
import butterknife.ButterKnife;


public class AnimDialogue extends AlertDialog implements
        View.OnClickListener {
    private final OnDialogClickListener listener;
    public Context c;
    public Dialog d;


    @BindView(R.id.llAnim)
    LinearLayout llAnim;

    @BindView(R.id.tvAdd)
    TextView tvAdd;

    @BindView(R.id.tvCancel)
    TextView tvCancel;


    @BindView(R.id.tvStatus)
    TextView tvStatus;

    @BindView(R.id.tvMsg)
    TextView tvMsg;

    @BindView(R.id.rlHide)
    RelativeLayout rlHide;


    String title = "", msg = "";

    public AnimDialogue(Context a, OnDialogClickListener listener, String title, String msg) {
        super(a);
        // TODO Auto-generated constructor stub
        this.c = a;
        this.listener = listener;
        this.title = title;
        this.msg = msg;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_anim_dialogue);
        ButterKnife.bind(this);

        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        getWindow().setAttributes(lp);

        tvAdd.setOnClickListener(this);
        tvCancel.setOnClickListener(this);
        rlHide.setOnClickListener(this);
        setCancelable(true);

        tvStatus.setText(title);
        tvStatus.setSelected(true);
        tvMsg.setText(msg);

        setOnKeyListener(new OnKeyListener() {

            @Override
            public boolean onKey(DialogInterface arg0, int keyCode,
                                 KeyEvent event) {
                // TODO Auto-generated method stub
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    dismissDialogue(0);
                }
                return true;
            }
        });

        rlHide.setOnClickListener(this);
        playAnim.slideDownRl(llAnim);

    }


    public void dismissDialogue(final int call) {
        playAnim.slideUpRl(llAnim);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                listener.onDialogImageRunClick(call, "");
                dismiss();

            }
        }, 700);

    }

    PlayAnim playAnim = new PlayAnim();

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvCancel:
                dismissDialogue(0);
                break;
            case R.id.tvAdd:
                dismissDialogue(1);
                break;
            case R.id.rlHide:
                dismissDialogue(1);
                break;
            default:
                break;
        }
    }

    public interface OnDialogClickListener {
        void onDialogImageRunClick(int pos, String add);
    }

}
