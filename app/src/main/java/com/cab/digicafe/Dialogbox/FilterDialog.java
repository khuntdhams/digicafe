package com.cab.digicafe.Dialogbox;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatRadioButton;
import android.view.View;
import android.view.WindowManager;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.cab.digicafe.R;


public class FilterDialog extends AlertDialog implements
        View.OnClickListener {
    private final OnDialogClickListener listener;
    public Activity c;
    public Dialog d;
    public TextView txt_cancel, txt_ok;

    AppCompatRadioButton rb_all, rb_avail, rb_notavail;
    RadioGroup rg_stock;

    /*  new CustomDialogClass(MainActivity.this, new CustomDialogClass.OnDialogClickListener() {
                    @Override
                    public void onDialogImageRunClick() {
                        Log.e("Ok","Ok");


                    }
                }).show(); */

    public FilterDialog(Activity a, OnDialogClickListener listener, String add) {
        super(a);
        // TODO Auto-generated constructor stub
        this.c = a;
        this.listener = listener;
        this.tag = add;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.filter_dialog);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        rb_all = (AppCompatRadioButton) findViewById(R.id.rb_all);
        rb_avail = (AppCompatRadioButton) findViewById(R.id.rb_avail);
        rb_notavail = (AppCompatRadioButton) findViewById(R.id.rb_notavail);
        rg_stock = (RadioGroup) findViewById(R.id.rg_stock);
        txt_ok = (TextView) findViewById(R.id.txt_ok);
        txt_cancel = (TextView) findViewById(R.id.txt_cancel);

        setCancelable(false);


        txt_cancel.setOnClickListener(this);
        txt_ok.setOnClickListener(this);


        switch (tag) {
            case "all":
                rb_all.setChecked(true);
                break;
            case "avail":
                rb_avail.setChecked(true);
                break;
            case "notavail":
                rb_notavail.setChecked(true);
                break;
            default:
                tag = "all";
                rb_all.setChecked(true);
                break;
        }

        rg_stock.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                switch (checkedId) {
                    case R.id.rb_all:
                        tag = "all";
                        break;
                    case R.id.rb_avail:
                        tag = "avail";
                        break;
                    case R.id.rb_notavail:
                        tag = "notavail";
                        break;
                    default:
                        break;
                }
            }
        });


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_cancel:
                listener.onDialogImageRunClick(0, tag);
                dismiss();
                break;
            case R.id.txt_ok:


                listener.onDialogImageRunClick(1, tag);
                dismiss();

                break;
            default:
                break;
        }

    }

    public interface OnDialogClickListener {
        void onDialogImageRunClick(int pos, String add);
    }

    String tag = "";
}
