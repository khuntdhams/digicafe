package com.cab.digicafe.Dialogbox;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.TextView;

import com.cab.digicafe.R;

import org.json.JSONObject;


public class RefundStatusDialog extends AlertDialog implements
        View.OnClickListener {
    private final OnDialogClickListener listener;
    public Activity c;
    public Dialog d;
    public TextView txt_ok, tv_msg;
    String msg = "";
    boolean iscapture = false;

    /*  new CustomDialogClass(MainActivity.this, new CustomDialogClass.OnDialogClickListener() {
                    @Override
                    public void onDialogImageRunClick() {
                        Log.e("Ok","Ok");


                    }
                }).show(); */

    public RefundStatusDialog(Activity a, OnDialogClickListener listener, String msg, boolean iscapture) {
        super(a);
        // TODO Auto-generated constructor stub
        this.c = a;
        this.listener = listener;
        this.msg = msg;
        this.iscapture = iscapture;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.refunddialog);

        txt_ok = (TextView) findViewById(R.id.txt_ok);
        tv_msg = (TextView) findViewById(R.id.tv_msg);


        setCancelable(false);


        try {

            JSONObject jo_refund = new JSONObject(msg);


            if (iscapture) {
                if (jo_refund.has("id")) {
                    String id = jo_refund.getString("id");
                    msg = "Paymnet id : " + id + "\nCapture Success !";
                }
            } else {
                if (jo_refund.has("id")) {
                    String id = jo_refund.getString("id");
                    msg = "Refund id : " + id + "\nRefund Success !";
                }

            }


        } catch (Exception e) {
            msg = "Description : \n" + msg.substring(msg.lastIndexOf(":") + 1);
            e.printStackTrace();
        }


        tv_msg.setText(msg);


        txt_ok.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {


        switch (v.getId()) {

            case R.id.txt_ok:
                listener.onDialogImageRunClick(1);

                dismiss();


                break;
            default:
                break;
        }

    }

    public interface OnDialogClickListener {
        void onDialogImageRunClick(int pos);
    }

}
