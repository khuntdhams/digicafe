package com.cab.digicafe.Dialogbox;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatRadioButton;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.cab.digicafe.R;


public class AddRefundDescription extends AlertDialog implements View.OnClickListener {
    private final OnDialogClickListener listener;
    public Activity c;
    public Dialog d;
    public TextView txt_refund;
    EditText et_msg;
    String msg = "";
    AppCompatRadioButton rb_noblur, rb_innerblur, rb_normal, rb_outer, rb_solid;

    /*  new CustomDialogClass(MainActivity.this, new CustomDialogClass.OnDialogClickListener() {
                    @Override
                    public void onDialogImageRunClick() {
                        Log.e("Ok","Ok");


                    }
                }).show(); */

    public AddRefundDescription(Activity a, OnDialogClickListener listener, String msg) {
        super(a);
        // TODO Auto-generated constructor stub
        this.c = a;
        this.listener = listener;
        this.msg = msg;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.refund_description);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);

        et_msg = (EditText) findViewById(R.id.et_msg);
        txt_refund = (TextView) findViewById(R.id.txt_refund);

        setCancelable(false);


        txt_refund.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.txt_refund:
                if (et_msg.getText().toString().trim().length() == 0) {
                    Toast.makeText(c, "Notes field is required !", Toast.LENGTH_LONG).show();
                } else {
                    listener.onDialogImageRunClick(1, et_msg.getText().toString().trim());
                    dismiss();
                }


                break;
            default:
                break;
        }

    }

    public interface OnDialogClickListener {
        void onDialogImageRunClick(int pos, String notes);
    }

}
