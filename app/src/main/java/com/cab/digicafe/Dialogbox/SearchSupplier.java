package com.cab.digicafe.Dialogbox;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.cab.digicafe.Model.ModelFile;
import com.cab.digicafe.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;


public class SearchSupplier extends AlertDialog implements
        View.OnClickListener {
    private final OnDialogClickListener listener;
    public Activity c;
    public Dialog d;
    public TextView txt_cancel, txt_ok, tv_msg, tv_status;
    EditText et_address;
    String add;
    Boolean isEdit = false;


    public SearchSupplier(Activity a, OnDialogClickListener listener, String add, Boolean isEdit) {
        super(a);
        // TODO Auto-generated constructor stub
        this.c = a;
        this.listener = listener;
        this.add = add;
        this.isEdit = isEdit;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_searchsupplier);

        ButterKnife.bind(this);

        setCancelable(false);

        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);

        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
        params.gravity = Gravity.CENTER;
        getWindow().setAttributes(params);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        et_address = (EditText) findViewById(R.id.et_address);
        txt_ok = (TextView) findViewById(R.id.txt_ok);
        txt_cancel = (TextView) findViewById(R.id.txt_cancel);
        tv_msg = (TextView) findViewById(R.id.tv_msg);
        tv_status = (TextView) findViewById(R.id.tv_status);

        if (isEdit) {
            et_address.setEnabled(false);
            tv_status.setText("Do you want to add into favourite?");
            et_address.setVisibility(View.GONE);
            txt_cancel.setText("NO");
            txt_ok.setText("YES");
        } else {
            et_address.setEnabled(true);
            tv_status.setText("Shop ID:");
            et_address.setVisibility(View.VISIBLE);
            txt_cancel.setText("CANCEL");
            txt_ok.setText("DONE");
        }

        et_address.setText(add);
        txt_cancel.setOnClickListener(this);
        txt_ok.setOnClickListener(this);


    }


    ArrayList<ModelFile> al_selet = new ArrayList<>();
    int c_img = 0;
    String[] str_img = new String[]{};


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_cancel:
                listener.onDialogImageRunClick(0, "");
                dismiss();
                break;


            case R.id.txt_ok:
                if (et_address.getText().toString().trim().isEmpty()) {
                    Toast.makeText(c, "Shop ID Field Is Required !", Toast.LENGTH_SHORT).show();
                } else {

                    findViewById(R.id.rl_pb_cat).setVisibility(View.VISIBLE);
                    listener.onDialogImageRunClick(1, et_address.getText().toString().trim());
                }
                break;
            default:
                break;
        }

    }

    public interface OnDialogClickListener {
        void onDialogImageRunClick(int pos, String add);

        void onImgClick(List<ModelFile> data, int pos);

        void onAddImg();
    }

    public void getsupplier(String errormsg) {
        findViewById(R.id.rl_pb_cat).setVisibility(View.GONE);
        tv_msg.setVisibility(View.VISIBLE);
        tv_msg.setText(errormsg);
    }


}
