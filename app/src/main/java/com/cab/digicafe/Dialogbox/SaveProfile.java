package com.cab.digicafe.Dialogbox;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatRadioButton;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.cab.digicafe.R;


public class SaveProfile extends AlertDialog implements
        View.OnClickListener {
    private final OnDialogClickListener listener;
    public Activity c;
    public Dialog d;
    public TextView txt_cancel, txt_ok;
    TextView tv_msg, tv_msg2;
    String add;
    AppCompatRadioButton rb_noblur, rb_innerblur, rb_normal, rb_outer, rb_solid;

    /*  new CustomDialogClass(MainActivity.this, new CustomDialogClass.OnDialogClickListener() {
                    @Override
                    public void onDialogImageRunClick() {
                        Log.e("Ok","Ok");


                    }
                }).show(); */

    public SaveProfile(Activity a, OnDialogClickListener listener, String add) {
        super(a);
        // TODO Auto-generated constructor stub
        this.c = a;
        this.listener = listener;
        this.add = add;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_box);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        tv_msg = (TextView) findViewById(R.id.tv_msg);
        tv_msg2 = (TextView) findViewById(R.id.tv_msg2);
        txt_ok = (TextView) findViewById(R.id.txt_ok);
        txt_cancel = (TextView) findViewById(R.id.txt_cancel);

        setCancelable(false);

        tv_msg2.setVisibility(View.GONE);

        txt_cancel.setOnClickListener(this);
        txt_ok.setOnClickListener(this);

        txt_ok.setText("Ok");

        txt_cancel.setVisibility(View.GONE);

        tv_msg.setText(add);

        if (add.equalsIgnoreCase("Yes")) {
            txt_ok.setText("Yes");
            txt_cancel.setText("No");
            txt_cancel.setVisibility(View.VISIBLE);
            add = "Available ?";
            tv_msg.setText(add);
        } else if (add.equalsIgnoreCase("No")) {
            txt_ok.setText("Yes");
            txt_cancel.setText("No");
            txt_cancel.setVisibility(View.VISIBLE);
            add = "Not Available ?";
            tv_msg.setText(add);
        }


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_cancel:
                listener.onDialogImageRunClick(0, "");
                dismiss();
                break;
            case R.id.txt_ok:
                listener.onDialogImageRunClick(1, "");
                dismiss();

                break;
            default:
                break;
        }

    }

    public interface OnDialogClickListener {
        void onDialogImageRunClick(int pos, String add);
    }

}
