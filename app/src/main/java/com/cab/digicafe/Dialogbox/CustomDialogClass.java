package com.cab.digicafe.Dialogbox;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatRadioButton;
import android.view.View;
import android.widget.TextView;

import com.cab.digicafe.Helper.Inad;
import com.cab.digicafe.R;


public class CustomDialogClass extends AlertDialog implements
        View.OnClickListener {
    private final OnDialogClickListener listener;
    public Activity c;
    public Dialog d;
    public TextView txt_cancel, txt_ok, tv_msg, tv_msg2;
    String msg = "";
    AppCompatRadioButton rb_noblur, rb_innerblur, rb_normal, rb_outer, rb_solid;

    /*  new CustomDialogClass(MainActivity.this, new CustomDialogClass.OnDialogClickListener() {
                    @Override
                    public void onDialogImageRunClick() {
                        Log.e("Ok","Ok");


                    }
                }).show(); */

    public CustomDialogClass(Activity a, OnDialogClickListener listener, String msg) {
        super(a);
        // TODO Auto-generated constructor stub
        this.c = a;
        this.listener = listener;
        this.msg = msg;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_box);
        txt_cancel = (TextView) findViewById(R.id.txt_cancel);
        txt_ok = (TextView) findViewById(R.id.txt_ok);
        tv_msg = (TextView) findViewById(R.id.tv_msg);
        tv_msg2 = (TextView) findViewById(R.id.tv_msg2);

        if (msg.equals("Alert\nInternet connection is required!")) {
            setCancelable(false);
            txt_ok.setText("Ok");
            txt_cancel.setVisibility(View.INVISIBLE);
            tv_msg.setText(msg);
        } else if (msg.equals("Empty Cart!") || msg.equals("Shop Closed!")) {
            setCancelable(false);
            txt_ok.setText("Ok");
            txt_cancel.setVisibility(View.INVISIBLE);
            tv_msg2.setText(msg);
        } else if (msg.equals("LOGOUT ?")) {
            setCancelable(true);
            tv_msg.setText(msg);
        } else if (msg.contains("You are moving out of")) {
            setCancelable(true);
            tv_msg.setText(msg);
            tv_msg2.setText("\nClearing Cart !");
        } else {
            setCancelable(true);
            tv_msg.setText(msg);
        }


        txt_cancel.setOnClickListener(this);
        txt_ok.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_cancel:
                listener.onDialogImageRunClick(0);
                dismiss();
                break;
            case R.id.txt_ok:
                listener.onDialogImageRunClick(1);
                if (msg.equals("Alert\nInternet connection is required!") && !Inad.isInternetOn(c)) {
                    dismiss();
                    Intent settingsIntent = new Intent(Settings.ACTION_SETTINGS);
                    c.startActivity(settingsIntent);
                } else {
                    dismiss();
                }

                break;
            default:
                break;
        }

    }

    public interface OnDialogClickListener {
        void onDialogImageRunClick(int pos);
    }

}
