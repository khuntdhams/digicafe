package com.cab.digicafe.Dialogbox;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatRadioButton;
import android.view.View;
import android.widget.TextView;

import com.cab.digicafe.Helper.SharedPrefUserDetail;
import com.cab.digicafe.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;


public class UpdateDialogClass extends AlertDialog implements
        View.OnClickListener {
    private final OnDialogClickListener listener;
    public Activity c;
    public Dialog d;
    public TextView txt_cancel, txt_ok, tv_msg, tv_msg2;
    String msg = "";
    boolean isupateforce = false;
    AppCompatRadioButton rb_noblur, rb_innerblur, rb_normal, rb_outer, rb_solid;

    /*  new CustomDialogClass(MainActivity.this, new CustomDialogClass.OnDialogClickListener() {
                    @Override
                    public void onDialogImageRunClick() {
                        Log.e("Ok","Ok");


                    }
                }).show(); */

    public UpdateDialogClass(Activity a, OnDialogClickListener listener, String msg, boolean isupateforce) {
        super(a);
        // TODO Auto-generated constructor stub
        this.c = a;
        this.listener = listener;
        this.msg = msg;
        this.isupateforce = isupateforce;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_box);
        txt_cancel = (TextView) findViewById(R.id.txt_cancel);
        txt_ok = (TextView) findViewById(R.id.txt_ok);
        tv_msg = (TextView) findViewById(R.id.tv_msg);
        tv_msg2 = (TextView) findViewById(R.id.tv_msg2);


        setCancelable(false);
        tv_msg.setText(msg);

        txt_cancel.setText("Cancel");
        txt_ok.setText("Update");

        if (isupateforce) {
            txt_cancel.setVisibility(View.GONE);
        } else {
            txt_cancel.setVisibility(View.VISIBLE);
        }


        txt_cancel.setOnClickListener(this);
        txt_ok.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String formattedDate = df.format(calendar.getTime());


        SharedPrefUserDetail.setString(c, SharedPrefUserDetail.UPDATEOPENDATE, formattedDate);


        switch (v.getId()) {
            case R.id.txt_cancel:
                listener.onDialogImageRunClick(0);
                dismiss();
                break;
            case R.id.txt_ok:
                listener.onDialogImageRunClick(1);

                dismiss();


                break;
            default:
                break;
        }

    }

    public interface OnDialogClickListener {
        void onDialogImageRunClick(int pos);
    }

}
