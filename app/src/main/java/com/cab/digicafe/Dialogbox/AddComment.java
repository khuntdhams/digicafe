package com.cab.digicafe.Dialogbox;

import android.app.Activity;
import android.app.Dialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatRadioButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.cab.digicafe.Adapter.SelectFileAdapter;
import com.cab.digicafe.Helper.DragTouchHelperIssue;
import com.cab.digicafe.Helper.SessionManager;
import com.cab.digicafe.Model.ModelFile;
import com.cab.digicafe.MyCustomClass.Compressor;
import com.cab.digicafe.R;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class AddComment extends AlertDialog implements
        View.OnClickListener {
    private final OnDialogClickListener listener;
    public Activity c;
    public Dialog d;
    public TextView txt_cancel, txt_ok, tv_status;
    EditText et_address;
    String add;
    ImageView iv_add;
    RecyclerView rv_selectfile;
    AppCompatRadioButton rb_noblur, rb_innerblur, rb_normal, rb_outer, rb_solid;
    SelectFileAdapter selectFileAdapter;

    @BindView(R.id.rl_upload)
    RelativeLayout rl_upload;

    @BindView(R.id.tv_progress)
    TextView tv_progress;


    /*  new CustomDialogClass(MainActivity.this, new CustomDialogClass.OnDialogClickListener() {
                    @Override
                    public void onDialogImageRunClick() {
                        Log.e("Ok","Ok");


                    }
                }).show(); */

    public AddComment(Activity a, OnDialogClickListener listener, String add) {
        super(a);
        // TODO Auto-generated constructor stub
        this.c = a;
        this.listener = listener;
        this.add = add;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_comment);

        ButterKnife.bind(this);

        setCancelable(false);

        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);

        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
        params.gravity = Gravity.CENTER;
        getWindow().setAttributes(params);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        et_address = (EditText) findViewById(R.id.et_address);
        txt_ok = (TextView) findViewById(R.id.txt_ok);
        txt_cancel = (TextView) findViewById(R.id.txt_cancel);
        tv_status = (TextView) findViewById(R.id.tv_status);
        rv_selectfile = (RecyclerView) findViewById(R.id.rv_selectfile);
        iv_add = (ImageView) findViewById(R.id.iv_add);


        txt_cancel.setOnClickListener(this);
        txt_ok.setOnClickListener(this);
        iv_add.setOnClickListener(this);


        LinearLayoutManager mLayoutManager = new LinearLayoutManager(c, LinearLayoutManager.HORIZONTAL, false);
        rv_selectfile.setLayoutManager(mLayoutManager);


        selectFileAdapter = new SelectFileAdapter(al_selet, c, new SelectFileAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(String item) {

            }

            @Override
            public void onDelete(int pos) {
                al_selet.remove(pos);
                selectFileAdapter.setItem(al_selet);


            }

            @Override
            public void onClickString(String pos) {

            }

            @Override
            public void onImgClick(List<ModelFile> data, int pos) {

                listener.onImgClick(data, pos);


            }
        }, false, false, true);

        rv_selectfile.setAdapter(selectFileAdapter);

        ItemTouchHelper.Callback callback = new DragTouchHelperIssue(selectFileAdapter);
        ItemTouchHelper helper = new ItemTouchHelper(callback);
        helper.attachToRecyclerView(rv_selectfile);


        inits3();


    }


    ArrayList<ModelFile> al_selet = new ArrayList<>();
    int c_img = 0;
    String[] str_img = new String[]{};


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_cancel:
                listener.onDialogImageRunClick(0, "");
                dismiss();
                break;

            case R.id.iv_add:
                listener.onAddImg();
                break;


            case R.id.txt_ok:
                if (et_address.getText().toString().trim().isEmpty()) {
                    Toast.makeText(c, "Comment Field Is Required !", Toast.LENGTH_SHORT).show();
                } else {
                    if (al_selet.size() == 0) {
                        SessionManager sessionManager = new SessionManager(c);
                        commentwithimg = et_address.getText().toString().trim() + ",;t " + sessionManager.getcurrentu_nm();
                        listener.onDialogImageRunClick(1, commentwithimg);
                        dismiss();
                    } else {

                        SessionManager sessionManager = new SessionManager(c);
                        commentwithimg = et_address.getText().toString().trim() + ",;t " + sessionManager.getcurrentu_nm();
                        str_img = new String[al_selet.size()];
                        new ImageCompress().execute();
                    }

                }
                break;
            default:
                break;
        }

    }

    public interface OnDialogClickListener {
        void onDialogImageRunClick(int pos, String add);

        void onImgClick(List<ModelFile> data, int pos);

        void onAddImg();
    }

    ArrayList<ModelFile> CompressItemSelect;

    public class ImageCompress extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            CompressItemSelect = new ArrayList();
            findViewById(R.id.rl_pb_cat).setVisibility(View.VISIBLE);

        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                for (int i = 0; i < al_selet.size(); i++) {
                    if (al_selet.get(i).isIsoffline()) {
                        File ImageFile = new File(al_selet.get(i).getImgpath());
                        File compressedImageFile = null;
                        String strFile = "";
                        try {
                            compressedImageFile = new Compressor(c).compressToFile(ImageFile);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        strFile = String.valueOf(compressedImageFile);

                        CompressItemSelect.add(new ModelFile(strFile, al_selet.get(i).isIsoffline()));
                    } else {
                        CompressItemSelect.add(new ModelFile(al_selet.get(i).getImgpath(), al_selet.get(i).isIsoffline()));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            findViewById(R.id.rl_pb_cat).setVisibility(View.GONE);
            al_selet = CompressItemSelect;
            uploadimg();
        }
    }

    AmazonS3 s3;
    TransferUtility transferUtility;


    public void inits3() {
        s3 = new AmazonS3Client(new BasicAWSCredentials(c.getString(R.string.accesskey), c.getString(R.string.secretkey)));
        transferUtility = new TransferUtility(s3, c.getApplicationContext());

    }

    String namegsxsax = "";

    public void uploadimg() {
        rl_upload.setVisibility(View.VISIBLE);
        int cc = c_img + 1;
        int total = CompressItemSelect.size();
        tv_progress.setText("Uploading " + cc + "/" + total);


        File file = new File(CompressItemSelect.get(c_img).getImgpath());
        namegsxsax = "REMARKIMG_" + System.currentTimeMillis() + ".jpg";


        TransferObserver transferObserver = transferUtility.upload("cab-videofiles", namegsxsax, file);
        transferObserverListener(transferObserver);


    }

    String commentwithimg = "";

    public void transferObserverListener(final TransferObserver transferObserver) {

        transferObserver.setTransferListener(new TransferListener() {
            @Override
            public void onStateChanged(int id, TransferState state) {
                if (state.name().equals("COMPLETED")) {
                    //str_img.put(getString(R.string.baseaws)+namegsxsax);
                    str_img[c_img] = c.getString(R.string.baseaws) + namegsxsax;

                    commentwithimg = commentwithimg + ",;t " + str_img[c_img];
                    c_img++;
                    if (c_img < al_selet.size()) {
                        uploadimg();
                    } else {
                        rl_upload.setVisibility(View.GONE);
                        c_img = 0;


                        listener.onDialogImageRunClick(1, commentwithimg);
                        dismiss();

                        //putdata();

                      /*  Gson gson = new GsonBuilder().create();
                        String json = gson.toJson(obj);// obj is your object


                        try {

                            JSONObject jo = new JSONObject();
                            JSONArray ja_new = new JSONArray();
                            jo.put("newdata", ja_new);
                            JSONArray ja_old = new JSONArray();

                            JsonArray ja_img = new JsonArray();
                            for (int i=0;i<str_img.length;i++)
                            {
                                ja_img.add(str_img[i]);
                            }

                            JSONObject jsonObj = new JSONObject(json);
                            jsonObj.put("image_json_data", ja_img);


                            //jsonObj.put("field_json_data",new JSONObject());

                            ja_old.put(jsonObj);
                            jo.put("olddata", ja_old);


                            CatRequest catalougeContent = new CatRequest();

                            catalougeContent = new Gson().fromJson(jo.toString(), CatRequest.class);

                            putData(catalougeContent);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            findViewById(R.id.rl_pb_cat).setVisibility(View.GONE);
                        }*/
                    }
                } else if (state.name().equals("FAILED")) {
                    rl_upload.setVisibility(View.GONE);
                    Toast.makeText(c, "Failed Uploading !", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                try {
                    int percentage = (int) (bytesCurrent / bytesTotal * 100);


                    //mProgressDialog.setProgress(percentage);


                    //Log.e("percentage ", " : " + percentage);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(int id, Exception ex) {

                Log.e("error", "error");
            }

        });
    }

    public void onAddedImg(ArrayList<ModelFile> al_data) {
        ArrayList<ModelFile> al_temp_selet = al_data;
        al_temp_selet.addAll(al_selet);
        selectFileAdapter.assignItem(al_temp_selet);
        al_selet = new ArrayList<>();
        al_selet = al_temp_selet;
    }


}
